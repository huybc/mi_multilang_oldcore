﻿using System;
using System.Web;
using System.Web.Caching;
using Mi.BO.Base.Account;
using Mi.BO.Base.Security;
using Mi.Common;
using Mi.Entity.Base.Security;

namespace Mi.Action
{
    public class Role
    {
        private static UserEntity GetUserByUsername(string username)
        {
            var cachedKey = string.Format("GetUserByUsername[{0}]", username);
            var data = HttpContext.Current.Cache[cachedKey] as UserEntity;
            if (data == null)
            {
                data = UserBo.GetUserByUsername(username);
                HttpContext.Current.Cache.Add(cachedKey, data, null, DateTime.Now.AddDays(1), TimeSpan.Zero,
                    CacheItemPriority.Default, null);
            }
            return data;
        }
        public static bool IsAdministrator(string username)
        {
            var res = false;
            if (string.IsNullOrEmpty(username)) return false;
            try
            {
                var user = GetUserByUsername(username);
                if (null != user)
                {
                    res = user.IsFullPermission;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return res;
        }
        public static bool IsRoleOnFullZone(string username)
        {
            var res = false;
            if (string.IsNullOrEmpty(username)) return false;
            try
            {
                var user = GetUserByUsername(username);
                if (null != user)
                {
                    res = user.IsFullZone;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return res;
        }

        //public static bool HasRoleOnGroup(GroupPermission groupPermissionId, string username)
        //{
        //    var res = false;
        //    if (string.IsNullOrEmpty(username)) return false;
        //    try
        //    {
        //        return PermissionBo.IsUserInGroupPermission(username, (int)groupPermissionId);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //    }
        //    return res;
        //}
        
        public static bool HasRoleOnPermission(EnumPermission permissionId, string username)
        {
            var res = false;
            if (string.IsNullOrEmpty(username)) return false;
            try
            {
                var usePermissions = PermissionBo.GetListByUsernameAndPermissionId(username, (int)permissionId);
                return null != usePermissions && usePermissions.Count > 0;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return res;
        }

        public static bool IsHasPermission(string username, EnumPermission permissionId, string listZoneId)
        {
            var userPermissions = PermissionBo.GetListByUsernameAndPermissionId(username, (int)permissionId);
            if (null != userPermissions && userPermissions.Count > 0)
            {
                listZoneId = "," + listZoneId + ",";
                var count = userPermissions.Count;
                for (var i = 0; i < count; i++)
                {
                    if (userPermissions[i].PermissionId == (int)permissionId &&
                        listZoneId.IndexOf("," + userPermissions[i].ZoneId + ",", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static bool HasRoleOnPermission(int zoneId, string username)
        {
            var res = false;
            if (string.IsNullOrEmpty(username)) return false;
            try
            {
                var userPermissions = PermissionBo.GetListByUsernameAndZoneId(username, zoneId);
                res = (null != userPermissions && userPermissions.Count > 0);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return res;
        }

        
    }
}
