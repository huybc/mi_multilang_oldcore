﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.Compilation;
using Mi.Common;
using Mi.Common.ChannelConfig;
using Mi.Action.Core;

namespace Mi.Action
{
    public class TemplateUtils
    {

        public static string LoadPage(string pagePath, NameValueCollection param)
        {
            //root directory
            pagePath = CmsChannelConfiguration.GetAppSetting(Constants.ROOT_DIRECTORY) + pagePath;

            var cachingDuration = CmsChannelConfiguration.GetAppSettingInInt32(Constants.CACHE_TEMPLATE_DURATION);

            var cacheName = PolicyProviderManager.Provider.GetAccountName() + ":Cached_Template_" + Regex.Replace(pagePath, "\\|/", "_") + CmsChannelConfiguration.GetAppSetting(Constants.CMS_VERSION);
            if (HttpContext.Current.Cache[cacheName] != null && cachingDuration > 0)
            {
                return HttpContext.Current.Cache[cacheName].ToString();
            }
            else
                try
                {
                    // get the compiled type of referenced path
                    var type = BuildManager.GetCompiledType(pagePath);

                    // if type is null, could not determine page type
                    if (type == null)
                        throw new ApplicationException("Page " + pagePath + " not found");

                    // cast page object (could also cast an interface instance as well)
                    // in this example, ASP220Page is a custom base page
                    var pageView = (PageBase)Activator.CreateInstance(type);
                    pageView.Params = param;

                    var writer = new StringWriter();
                    HttpContext.Current.Server.Execute(pagePath, writer, true);
                    var resultText = writer.ToString().Trim();
                    if (resultText != "" && cachingDuration > 0)
                    {
                        HttpContext.Current.Cache.Add(cacheName, resultText, null,
                                                      DateTime.Now.AddHours(cachingDuration), TimeSpan.Zero,
                                                      CacheItemPriority.High, null);
                    }
                    //if (CmsChannelConfiguration.GetAppSettingInInt32(Constants.CMS_SERVER_DEBUG_MODE) == 1)
                    //{
                    //    resultText += string.Format("<!--TemplateUrl:[{0}] Cached:[{1}]-->", pagePath, cachingDuration > 0);
                    //}


                    return RemoveWhitespaceFromHtml(resultText);
                }
                catch (Exception ex)
                {
                    if (CmsChannelConfiguration.GetAppSettingInInt32(Constants.CMS_SERVER_DEBUG_MODE) == 1)
                    {
                        return BuildServerErrorWithFormat("Page:" + pagePath, ex.Message + ex.InnerException);
                    }
                    else
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                        if (ex.InnerException != null)
                            Logger.WriteLog(Logger.LogType.Error, ex.InnerException.ToString());
                        return BuildServerErrorWithFormat("Lỗi:", ex.Message + ex.InnerException);
                    }
                }
        }

        public static string BuildServerErrorWithFormat(string title, string content)
        {
            return string.Format("<div class=\"IMSServerCodeErrorContent\"><span class=\"IMSServerCodeErrorTitle\">{0}</span><p>{1}</p></div>", title, content);
        }
        private static readonly Regex RegexBetweenTags = new Regex(@">(?! )\s+", RegexOptions.Compiled);

        private static readonly Regex RegexLineBreaks = new Regex(@"([\n\s])+?(?<= {2,})<", RegexOptions.Compiled);
        public static string RemoveWhitespaceFromHtml(string html)
        {
            html = RegexBetweenTags.Replace(html, ">");
            html = RegexLineBreaks.Replace(html, "<");
            return html.Trim();
        }
    }
}