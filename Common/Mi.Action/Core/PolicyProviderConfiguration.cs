﻿using System.Configuration;

namespace Mi.Action.Core
{
    public class PolicyProviderConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("providers")]
        public ProviderSettingsCollection Providers
        {
            get { return (ProviderSettingsCollection) base["providers"]; }
        }
        [ConfigurationProperty("default", DefaultValue = "WebPolicyProvider")]
        public string Default
        {
            get { return (string) base["default"]; }
            set { base["default"] = value; }
        }
    }
}
