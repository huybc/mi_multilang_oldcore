﻿using System;
using System.Collections.Generic;
using Mi.Entity.Base.Security;

namespace Mi.Action.Core
{
    public class AuthCookieData
    {
        #region private properties

        private string _userName = string.Empty;
        private string _action = string.Empty;
        private string _secretKey = string.Empty;
        private string _remoteAddr = string.Empty;
        private string _remoteHost = string.Empty;
        private string _referHost = string.Empty;
        private string _httpUserAgent = string.Empty;
        private string _authenticateId = string.Empty;
        private int _siteId = 0;
        private DateTime _createdDate = DateTime.Now;
        private DateTime _expiredDate = DateTime.Now;
        private EnumPermission _currentRole = EnumPermission.ArticleReporter;

        #endregion

        #region public properties

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public string Action
        {
            get { return _action; }
            set { _action = value; }
        }

        public string SecretKey
        {
            get { return _secretKey; }
            set { _secretKey = value; }
        }

        public string RemoteAddr
        {
            get { return _remoteAddr; }
            set { _remoteAddr = value; }
        }

        public string RemoteHost
        {
            get { return _remoteHost; }
            set { _remoteHost = value; }
        }

        public string ReferHost
        {
            get { return _referHost; }
            set { _referHost = value; }
        }

        public string HttpUserAgent
        {
            get { return _httpUserAgent; }
            set { _httpUserAgent = value; }
        }

        public string AuthenticateId
        {
            get { return _authenticateId; }
            set { _authenticateId = value; }
        }

        public int SiteId
        {
            get { return _siteId; }
            set { _siteId = value; }
        }

        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        public DateTime ExpiredDate
        {
            get { return _expiredDate; }
            set { _expiredDate = value; }
        }

        public EnumPermission CurrentRole
        {
            get { return _currentRole; }
            set { _currentRole = value; }
        }

        #endregion

        private readonly Dictionary<string, object> _extendedProperties = new Dictionary<string, object>();

        public object this[string propertyName]
        {
            get { return (_extendedProperties.ContainsKey(propertyName) ? _extendedProperties[propertyName] : null); }
            set
            {
                if (_extendedProperties.ContainsKey(propertyName))
                {
                    _extendedProperties[propertyName] = value;
                }
                else
                {
                    _extendedProperties.Add(propertyName, value);
                }
            }
        }
    }
}
