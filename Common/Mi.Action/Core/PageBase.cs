﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;

namespace Mi.Action.Core
{
    public class PageBase : Page
    {
        public NameValueCollection Params { get; set; }

        protected override void OnPreInit(EventArgs e)
        {
            EnableViewState = false;
            EnableViewStateMac = false;
            EnableEventValidation = false;
            base.OnPreInit(e);
        }
        public void PageLoad(object sender, EventArgs e)
        {
            HttpContext.Current.Response.AddHeader("Accept-Encoding", "gzip, deflate");
        }
    }
}
