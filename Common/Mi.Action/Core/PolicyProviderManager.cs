﻿using System;
using System.Configuration;
using System.Web.Configuration;
using Mi.Action.Core;

namespace Mi.Action.Core
{
    public  class PolicyProviderManager
    {
        public static PolicyProvider Provider
        {
            get { 
                return new PolicyProviderManager().DefaultProvider;
            }
        }
        public PolicyProvider DefaultProvider { get; protected set; }
        public PolicyProviderCollection Providers { get; protected set; }

        public PolicyProviderManager()
        {
            Initialize();
        }

        private void Initialize()
        {
            var configuration = ConfigurationManager.GetSection("CmsPolicyProvider") as PolicyProviderConfiguration;

            if (null == configuration)
            {
                throw new ConfigurationErrorsException("CmsPolicyProvider configuration section is not set correctly.");
            }

            Providers = new PolicyProviderCollection();

            ProvidersHelper.InstantiateProviders(configuration.Providers, Providers, typeof(PolicyProvider));

            Providers.SetReadOnly();

            DefaultProvider = Providers[configuration.Default];

            if (null == DefaultProvider)
            {
                throw new Exception("Default provider is not set.");
            }
        }
    }
}
