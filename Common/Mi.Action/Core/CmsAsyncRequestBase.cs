﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web;
using System.Web.SessionState;

namespace Mi.Action.Core
{
    public class AsynchOperation : IAsyncResult
    {
        private bool _completed;
        private Object _state;
        private AsyncCallback _callback;
        private HttpContext _context;

        bool IAsyncResult.IsCompleted { get { return _completed; } }
        WaitHandle IAsyncResult.AsyncWaitHandle { get { return null; } }
        Object IAsyncResult.AsyncState { get { return _state; } }
        bool IAsyncResult.CompletedSynchronously { get { return false; } }

        public AsynchOperation(AsyncCallback callback, HttpContext context, Object state)
        {
            _callback = callback;
            _context = context;
            _state = state;
            _completed = false;
        }

        public void StartAsyncWork()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(StartAsyncTask), null);
        }

        private void StartAsyncTask(Object workItemState)
        {

            _context.Response.Write("<p>Completion IsThreadPoolThread is " + Thread.CurrentThread.IsThreadPoolThread + "</p>\r\n");

            _context.Response.Write("Hello World from Async Handler!");
            _completed = true;
            _callback(this);
        }
    }

    public class CmsAsyncRequestBase : IHttpAsyncHandler, IRequiresSessionState
    {
        public delegate void ProcessRequestDelegate(HttpContext ctx);

        public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
        {
            context.Response.ContentType = "text/plain";
            //context.Response.AddHeader("Accept-Encoding", "gzip, deflate");
            var actionName = !string.IsNullOrEmpty(context.Request.Form["m"])
                                 ? context.Request.Form["m"].Trim().ToLower()
                                 : "file";
            if (string.IsNullOrEmpty(actionName))
            {
                context.Response.Status = "404 not found";
                context.Server.Transfer("/404.aspx");
                context.Response.End();
            }
            else
            {
                var action = GetAction(actionName);
                if (action != null)
                {
                    action.Do(context);
                }
            }

            var asynch = new AsynchOperation(cb, context, extraData);
            asynch.StartAsyncWork();
            return asynch;
        }


        // Fields
        private static readonly IDictionary<string, ActionBase> Dicts = new Dictionary<string, ActionBase>();

        protected static void RegisterAction(string actionName, ActionBase action)
        {
            Dicts[actionName] = action;
        }

        protected static ActionBase GetAction(string actionName)
        {
            ActionBase action;
            return Dicts.TryGetValue(actionName, out action) ? action : null;
        }

        public void EndProcessRequest(IAsyncResult result)
        {
        }

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
        }
    }
}
