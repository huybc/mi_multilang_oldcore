﻿using System;
using System.Web;
using Mi.Action.Core;
using Mi.Common;
using Mi.Entity.ErrorCode;

namespace Mi.Action
{
    public class ConvertResponseData
    {
        public static ResponseData CreateResponseData(WcfResponseData wcfResponseData, string templatePath)
        {
            return new ResponseData
            {
                Success = wcfResponseData.Success,
                Message = wcfResponseData.Message,
                Data = wcfResponseData.Data,
                TotalRow = wcfResponseData.TotalRow,
                ErrorCode = wcfResponseData.ErrorCode,
                Content = TemplateUtils.LoadPage(templatePath, HttpContext.Current.Request.Form)
            };
        }
        public static ResponseData CreateResponseData(WcfResponseData wcfResponseData)
        {
            return new ResponseData
            {
                Success = wcfResponseData.Success,
                Message = wcfResponseData.Message,
                Data = wcfResponseData.Data,
                TotalRow = wcfResponseData.TotalRow,
                ErrorCode = wcfResponseData.ErrorCode,
                Content = ""
            };
        }
        public static ResponseData CreateResponseData(Exception ex)
        {
            return new ResponseData
            {
                Success = false,
                Message = ex.Message,
                Data = "",
                TotalRow = 0,
                ErrorCode = (int)ErrorMapping.ErrorCodes.Exception,
                Content = ""
            };
        }
        public static ResponseData CreateResponseData(object returnData, int totalRow, string templatePath = "")
        {
            if (null != returnData)
            {
                return new ResponseData
                {
                    Success = true,
                    Message = "Success!",
                    Data = NewtonJson.Serialize(returnData),
                    TotalRow = totalRow,
                    ErrorCode = (int)ErrorMapping.ErrorCodes.Success,
                    Content =
                        string.IsNullOrEmpty(templatePath)
                            ? ""
                            : TemplateUtils.LoadPage(templatePath, HttpContext.Current.Request.Form)
                };
            }
            else
            {
                return new ResponseData
                {
                    Success = false,
                    Message = "Data not found!",
                    Data = "",
                    TotalRow = 0,
                    ErrorCode = (int)ErrorMapping.ErrorCodes.BusinessError,
                    Content =
                        string.IsNullOrEmpty(templatePath)
                            ? ""
                            : TemplateUtils.LoadPage(templatePath, HttpContext.Current.Request.Form)
                };
            }
        }

        #region For Mapping

        public static ResponseData CreateResponseData(WcfActionResponse wcfActionResponse, string templatePath = "")
        {
            return new ResponseData
                       {
                           Success = wcfActionResponse.Success,
                           Message = wcfActionResponse.Message,
                           Data = wcfActionResponse.Data,
                           TotalRow = 0,
                           ErrorCode = wcfActionResponse.ErrorCode,
                           Content =
                               string.IsNullOrEmpty(templatePath)
                                   ? ""
                                   : TemplateUtils.LoadPage(templatePath, HttpContext.Current.Request.Form)
                       };
        }

        #endregion
    }
}
