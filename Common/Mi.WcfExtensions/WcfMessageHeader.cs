﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Mi.WcfExtensions
{
    [DataContract]
    public class WcfMessageHeader
    {
        public static WcfMessageHeader CustomWcfHeader { get; set; }

        public const string MessageName = "WcfMessageHeader";
        public const string MessageNamespace = "http://www.Mi.net";

        public static WcfMessageHeader Current
        {
            get
            {
                WcfMessageHeader header;
                try
                {
                    if (OperationContext.Current != null)
                    {
                        header =
                            OperationContext.Current.IncomingMessageProperties.FirstOrDefault(f => f.Key == MessageName)
                                .Value
                                as
                                WcfMessageHeader ?? CustomWcfHeader;
                    }
                    else
                    {
                        header = CustomWcfHeader;
                    }
                }
                catch (Exception)
                {
                    header = CustomWcfHeader;
                }
                return header;
            }
        }

        [DataMember]
        public string Namespace { get; set; }
        [DataMember]
        public string SecretKey { get; set; }
        [DataMember]
        public string ClientUsername { get; set; }
    }
}
