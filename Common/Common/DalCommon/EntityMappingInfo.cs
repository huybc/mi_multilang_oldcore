﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Mi.Common.DalCommon
{
    [Serializable]
	public class EntityMappingInfo
    {
        public EntityMappingInfo()
		{
            Properties = new Dictionary<string, PropertyInfo>();
            ColumnNames = new Dictionary<string, string>();
		}

        public string CacheByProperty { get; set; }
        public int CacheTimeOutMultiplier { get; set; }
        public Dictionary<string, string> ColumnNames { private set; get; }

        public string ObjectType { get; set; }
        public string PrimaryKey { get; set; }
        public Dictionary<string, PropertyInfo> Properties { private set; get; }
        public string TableName { get; set; }
    }
}
