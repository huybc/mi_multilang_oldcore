﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.Redis;

namespace Mi.Common.RedisClientHelper
{
    public interface ICustomRedisClient
    {
        IRedisTransaction CreateTransaction();

        bool Push(string key, string value, bool tail);

        bool EnqueueItemOnList(string key, string value);

        string DequeueItemFromList(string key);

        bool Push(string key, List<string> listValue, bool tail);

        void AddItemToSet(string key, string value);

        void AddItemToList(string key, string value);

        bool AddItemToSortedSet(string key, string value, double score);

        bool TrimList(string key, int start, int end);

        bool Set<T>(string key, T t);
        bool Set<T>(string key, T t, DateTime expireAt);
        bool Set<T>(string key, T t, TimeSpan expireIn);

        bool SetNX(string key, string value);

        bool Set(string key, string value);

        bool Add(string key, string value);

        bool Add(string key, long value);

        bool Add(string key, string value, DateTime expireAt);

        bool Add(string key, string value, TimeSpan expireIn);

        bool Add<T>(string key, T value, TimeSpan expireIn);

        bool SetAdd(string key, string value);

        bool Expire(string key, int time);

        bool ExpireAt(string key, DateTime dateTime);

        bool ExpireIn(string key, TimeSpan timeSpan);

        bool ContainsKey(string key);

        string GetString(string key);

        bool Remove(string key);

        long RemoveElementFromList(string key, string element);

        byte[][] GetKeys(string[] keys);

        List<string> GetKeys(List<string> keys);

        List<T> GetValues<T>(List<string> keys);

        List<string> GetListRangeString(string key, int start, int end);

        List<string> GetRangeFromSortedList(string key, int startingFrom, int endingAt);

        long GetSortedSetCount(string key);

        long GetListCount(string key);

        long RemoveRangeFromSortedSet(string key, int minRank, int maxRank);

        /// <summary>
        /// The get range from sorted set
        /// Author:     ThanhDT
        /// Create Date: 3/20/2012 2:08 PM
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="fromRank">From rank.</param>
        /// <param name="toRank">To rank.</param>
        /// <returns></returns>
        List<string> GetRangeFromSortedSet(string key, int fromRank, int toRank);

        /// <summary>
        /// The get item index in sorted set
        /// Author:     ThanhDT
        /// Create Date: 3/23/2012 11:32 AM
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        long GetItemIndexInSortedSet(string key, string item);

        /// <summary>
        /// The get range from sorted set descending
        /// Author:     ThanhDT
        /// Create Date: 3/20/2012 2:08 PM
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="fromRank">From rank.</param>
        /// <param name="toRank">To rank.</param>
        /// <returns></returns>
        List<string> GetRangeFromSortedSetDesc(string key, int fromRank, int toRank);

        /// <summary>
        /// The get range from sorted set by highest score
        /// Author:     ThanhDT
        /// Create Date: 3/20/2012 2:08 PM
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="fromScore">From score.</param>
        /// <param name="toScore">To score.</param>
        /// <returns></returns>
        List<string> GetRangeFromSortedSetByHighestScore(string key, double fromScore, double toScore);

        /// <summary>
        /// The get range from sorted set by lowest score
        /// Author:     ThanhDT
        /// Create Date: 3/22/2012 9:03 AM
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="fromScore">From score.</param>
        /// <param name="toScore">To score.</param>
        /// <returns></returns>
        List<string> GetRangeFromSortedSetByLowestScore(string key, double fromScore, double toScore);

        IDictionary<string, double> GetAllWithScoresFromSortedSet(string key);

        List<string> GetAllItemsFromList(string key);

        List<string> GetSetMembersString(string key);

        IDisposable AcquireLock(string key);

        IDisposable AcquireLock(string key, TimeSpan timeout);

        long IncrementValue(string key);

        T Get<T>(string key);

        void SetWithLock(string key, string value);

        void SetAll<T>(IDictionary<string, T> dicts);

        bool Replace(string key, string value);

        void DisposeAll();

        void FlushDb();

        List<string> SearchKeys(string pattern);
    }
}
