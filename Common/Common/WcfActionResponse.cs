﻿using System;
using System.Runtime.Serialization;

namespace Mi.Common
{
    [DataContract]
    public class WcfActionResponse
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int ErrorCode { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public dynamic Data { get; set; }

        public WcfActionResponse()
        {
            Success = false;
            ErrorCode = 0;
            Message = "";
            Data = "";
        }

        #region create response methods
        public static WcfActionResponse CreateErrorResponse(int errorCode, string message)
        {
            return new WcfActionResponse
            {
                Success = false,
                Message = (string.IsNullOrEmpty(message) ? "Error!" : message),
                ErrorCode = errorCode, 
                Data = ""
            };
        }
        public static WcfActionResponse CreateErrorResponse(Exception ex)
        {
            return CreateErrorResponse(9998, ex.Message);
        }
        public static WcfActionResponse CreateErrorResponse(string message)
        {
            return CreateErrorResponse(9997, message);
        }
        public static WcfActionResponse CreateErrorResponseForInvalidRequest()
        {
            return CreateErrorResponse(9996, "Invalid request!");
        }

        public static WcfActionResponse CreateSuccessResponse(dynamic data, string message)
        {
            return new WcfActionResponse
            {
                Success = true,
                Message = string.IsNullOrEmpty(message) ? "Success!" : message,
                ErrorCode = 0,
                Data = data
            };
        }
        public static WcfActionResponse CreateSuccessResponse(string message)
        {
            return new WcfActionResponse
            {
                Success = true,
                Message = string.IsNullOrEmpty(message) ? "Success!" : message,
                ErrorCode = 0
            };
        }
        public static WcfActionResponse CreateSuccessResponse()
        {
            return CreateSuccessResponse("Success!");
        }
        #endregion
    }
}
