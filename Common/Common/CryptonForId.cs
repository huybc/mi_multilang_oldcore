﻿namespace Mi.Common
{
    public class CryptonForId
    {
        private const string DECRYPT_KEY = "decrypt_key";
        private static string CryptonKey
        {
            get
            {
                var key = "";//AppSettings.GetString("decrypt_key");
                if (string.IsNullOrEmpty(key)) key = "newcms";
                return key;
            }
        }
        public static int DecryptIdToInt(string encryptId)
        {
            return Utility.ConvertToInt(Crypton.DecryptByKey(encryptId, CryptonKey));
        }
        public static long DecryptIdToLong(string encryptId)
        {
            return Utility.ConvertToLong(Crypton.DecryptByKey(encryptId, CryptonKey));
        }
        public static string EncryptId(int id)
        {
            return Crypton.EncryptByKey(id.ToString(), CryptonKey);
        }
        public static string EncryptId(long id)
        {
            return id.ToString();
            return Crypton.EncryptByKey(id.ToString(), CryptonKey);
        }
    }
}
