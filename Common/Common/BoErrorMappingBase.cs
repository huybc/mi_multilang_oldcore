﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Mi.Common
{
    [DataContract]
    public abstract class BoErrorMappingBase<T> : DictionaryBase
    {
        public string this[T code]
        {
            get
            {
                return InnerHashtable.ContainsKey(code)
                           ? InnerHashtable[code].ToString()
                           : "Không tìm thấy thông tin ứng với mã lỗi";
            }
        }

        protected abstract void InitErrorMapping();

        protected BoErrorMappingBase()
        {
            #region General error

            InnerHashtable[ErrorCodeBase.Success] = "Xử lý thành công";
            InnerHashtable[ErrorCodeBase.UnknowError] = "Lỗi không xác định";
            InnerHashtable[ErrorCodeBase.Exception] = "Lỗi trong quá trình xử lý";
            InnerHashtable[ErrorCodeBase.BusinessError] = "Lỗi nghiệp vụ";
            InnerHashtable[ErrorCodeBase.InvalidRequest] = "Yêu cầu không hợp lệ";
            InnerHashtable[ErrorCodeBase.TimeOutSession] = "Phiên làm việc của bạn đã hết.";

            #endregion

            InitErrorMapping();
        }
        [DataContract]
        public enum ErrorCodeBase
        {
            #region General error

            [EnumMember]
            Success = 0,
            [EnumMember]
            UnknowError = 9999,
            [EnumMember]
            Exception = 9998,
            [EnumMember]
            BusinessError = 9997,
            [EnumMember]
            InvalidRequest = 9996,
            [EnumMember]
            TimeOutSession = -100,

            #endregion
        }
    }
}
