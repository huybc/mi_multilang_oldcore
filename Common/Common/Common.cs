﻿using System;
using System.IO;

namespace Mi.Common
{

    #region CommandNames

    public class CommandNames
    {
        public const string AddNew = "AddNew";
        public const string Approve = "Approve";
        public const string Cancel = "Cancel";
        public const string Clear = "Clear";
        public const string Collapse = "Expand/Collapse";
        public const string Delete = "Delete";
        public const string Edit = "Edit";
        public const string GoBacktoView = "GoBacktoView";
        public const string Preview = "Preview";
        public const string View = "View";
        public const string Reject = "Reject";
        public const string Remove = "Remove";
        public const string RowClick = "RowClick";
        public const string RowDblClick = "RowDblClick";
        public const string Save = "Save";
        public const string Reset = "Reset";
        public const string Search = "Search";
        public const string Stop = "Stop";
        public const string Submit = "Submit";
        public const string Upload = "Upload";
    }
    public class CommandConfigKey
    {
        public const string InfoConfig = "InfoConfig";
        public const string SourceConfig = "SourceConfig";
        public const string PageSetting = "PageSetting";
        public const string ZoneSetting = "ZoneSetting";
        
    }
    public class ActionNames
    {
        public const string AddNew = "Thêm mới";
        public const string Cancel = "Hủy";
        public const string Collapse = "Expand/Collapse";
        public const string Delete = "Xóa";
        public const string Edit = "Sửa";
        public const string Update = "Cập nhật";
        public const string GoBacktoView = "GoBacktoView";
        public const string Preview = "Preview";
        public const string View = "Xem";
        public const string Reject = "Reject";
        public const string Remove = "Remove";
        public const string RowClick = "RowClick";
        public const string RowDblClick = "RowDblClick";
        public const string Save = "Lưu";
        public const string Reset = "Làm lại";
        public const string Search = "Search";
        public const string Stop = "Stop";
        public const string Submit = "Submit";
        public const string Upload = "Upload";
    }

    #endregion

    #region FormNames

    public class UrlManager
    {

        #region Nested type: Administrator

        public class Admin
        {


            public const string ProjectBuildingView = "~/AdminCP/Pages/ProjectBuildingView.aspx";
            public const string ProjectBuildingEdit = "~/AdminCP/Pages/ProjectBuildingEdit.aspx";
            public const string NewsView = "~/AdminCP/Pages/NewsView.aspx";
            public const string NewsEdit = "~/AdminCP/Pages/NewsEdit.aspx";
            public const string CategoryServiceView = "~/AdminCP/Pages/CategoryServiceView.aspx";
            public const string CategoryServiceEdit = "~/AdminCP/Pages/CategoryServiceEdit.aspx";
            public const string CategoryNewsEdit = "~/AdminCP/Pages/CategoryNewsEdit.aspx";
            public const string CategoryNewsView = "~/AdminCP/Pages/CategoryNewsView.aspx";
            public const string CategoryProductView = "~/AdminCP/Pages/CategoryProductView.aspx";
            public const string CategoryProductEdit = "~/AdminCP/Pages/CategoryProductEdit.aspx";
            public const string GalleryAlbumView = "~/AdminCP/Pages/GalleryAlbumView.aspx";
            public const string GalleryAlbumEdit = "~/AdminCP/Pages/GalleryAlbumEdit.aspx";


            public const string CategoryAboutView = "~/AdminCP/Pages/CategoryAboutView.aspx";
            public const string CategoryAboutEdit = "~/AdminCP/Pages/CategoryAboutEdit.aspx";
            public const string CategoryCompanyEdit = "~/AdminCP/Pages/CategoryCompanyEdit.aspx";
            public const string CategoryCompanyView = "~/AdminCP/Pages/CategoryCompanyView.aspx";
            public const string AboutView = "~/AdminCP/Pages/AboutView.aspx";
            public const string AboutEdit = "~/AdminCP/Pages/AboutEdit.aspx";

            public const string RecruitmentView = "~/AdminCP/Pages/RecruitmentEdit.aspx";
            public const string RecruitmentEdit = "~/AdminCP/Pages/RecruitmentEdit.aspx";
            public const string CategoryRecruitmentView = "~/AdminCP/Pages/CategoryRecruitmentView.aspx";
            public const string CategoryRecruitmentEdit = "~/AdminCP/Pages/CategoryRecruitmentEdit.aspx";


            public const string CompanyChildEdit = "~/AdminCP/Pages/CompanyChildEdit.aspx";
            public const string CompanyChildView = "~/AdminCP/Pages/CompanyChildEdit.aspx";
            public const string SupportOnlineEdit = "~/AdminCP/Pages/SupportEdit.aspx";
            public const string ConfigsEdit = "~/AdminCP/Pages/ConfigsEdit.aspx";
            public const string Manufacturer = "~/AdminCP/Pages/Manufacturer.aspx";
            public const string AdsEdit = "~/AdminCP/Pages/AdsEdit.aspx";
            public const string AdsView = "~/AdminCP/Pages/AdsView.aspx";

            public const string ProductEdit = "~/AdminCP/Pages/ProductEdit.aspx";
            public const string ProductView = "~/AdminCP/Pages/ProductView.aspx";

            public const string PartnerEdit = "~/AdminCP/Pages/PartnerEdit.aspx";
            public const string PartnerView = "~/AdminCP/Pages/PartnerView.aspx";

            public const string ConfigEdit = "~/AdminCP/Pages/ConfigEdit.aspx";
            public const string ConfigView = "~/AdminCP/Pages/ConfigView.aspx";

            public const string SlideEdit = "~/AdminCP/Pages/SlideEdit.aspx";
            public const string SlideView = "~/AdminCP/Pages/SlideView.aspx";
            public const string VideoView = "~/AdminCP/Pages/VideoView.aspx";


        }

        #endregion
        public class Member
        {
            public const string Login = "~/Login.aspx";
            public const string Default = "~/Jobcodes.aspx";
        }

    }

    #endregion

    #region Session

    public class Session
    {




        public const string CURRENTID = "CURRENTID";
        public const string CURRENTSUPORTID = "CURRENTSUPORTID";
        public const string CURRENTBANNERID = "CURRENTBANNERID";
        public const string CURRENTOPTIONID = "CURRENTOPTIONID";
        public const string USER_ID = "USER_ID";
        //public const string CURRENTUSER = "CURRENTUSER";
    }

    #endregion

    #region ParameterName

    public class ParameterName
    {
        public const string ACID = "ACId";
        public const string ID = "ID";
        public const string CAMPAIGN_ID = "CId";
        public const string CATEGORY_ID = "CId";
        public const string CITY_ID = "CId";
        public const string DOC_ID = "DocId";
        public const string GROUP_ID = "GId";
        public const string KEY = "Key";
        public const string KEYWORD = "Keyword";
        public const string MANUFACTURER_ID = "MId";
        public const string MESSAGE = "msg";
        public const string MSG = "msg";
        public const string NATION_ID = "NId";
        public const string PAGE = "Page";
        public const string PAGES = "PageS";
        public const string PARENT_ID = "PId";
        public const string REDIRECT = "redi";
        public const string SITE_ID = "SId";
        public const string TYPE_ID = "TypeId";
        public const string URL = "Url";
        public const string USER_ID = "UId";
        public const string VIEW_ID = "ViewId";
    }

    #endregion

    #region Read File

    public class FileClass
    {
        public FileClass()
        {
        }

        public static string ReadFile(string FileName)
        {
            try
            {
                var FILENAME = System.Web.Hosting.HostingEnvironment.MapPath(FileName);
                if (FILENAME != null)
                {
                    StreamReader objStreamReader = File.OpenText(FILENAME);
                    String contents = objStreamReader.ReadToEnd();
                    return contents;
                }
            }
            catch (Exception ex)
            {

            }
            return "";
        }
    }

    #endregion
}