﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/HoangLongMaster.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="CMTravel._default1" %>

<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <%var slide = ConfigBo.AdvGetByType(2, lang).OrderBy(r => r.SortOrder).ToList(); %>
    <meta property="fb:app_id" content="<%=UIHelper.GetConfigByName("FBAppID") %>" />
    <meta property="og:url" content="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="<%=UIHelper.GetConfigByName("MetaTitle") %>" />
    <meta property="og:description" content="<%=UIHelper.GetConfigByName("MetaDescription") %>" />
    <meta property="og:image" content="<%=domainName %>/uploads/thumb/<%=slide[0].Thumb %>" />
    <style>
        .full-image-link img {
            width: 100%;
            height: auto;
        }

        .home-tin-tuc {
            margin-top: 25px;
        }

        .heading-ss-home h1 {
            font-size: 22px;
            font-family: "Trajan";
        }

        .heading-ss-home h6 {
            font-size: 14px;
            font-family: "Trajan";
        }

        .banner img {
            max-width: 100%;
            height: auto;
        }
        /*.image{
            width:347px;
            height:394px
        }*/
        .menu-right .item-link {
            padding: 8px;
            padding-bottom:0px;
            border-top: 1px solid #A9A9A9;
        }
        .list-ck .item-large, .list-ck .item {
            border-bottom:none;
        }
        .menu-right{
            padding-bottom:1px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%--<%
        var currentLanguage = Page.RouteData.Values["languageCode"].ToString();
        var lang = "";
        switch (currentLanguage)
        {
            case "vi":
                lang = "vi-VN";
                break;
            case "en":
                lang = "en-US";
                break;
        }

    %>--%>
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "Organization",
        "name": "HoangLong",
        "legalName" : "Phòng khám đa khoa Hoàng Long",
        "url": "<%=domainName %>",
        //"logo": "https://gianhadat.cenhomes.vn/content/images/cenhome-logo.png",
        "foundingDate": "<%=DateTime.Now.Year %>",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "<%=UIHelper.GetConfigByName("Address") %>",
            "addressLocality": "Hà Nội",
            "addressCountry": "VN"
        },
        "contactPoint": {
            "@type": "ContactPoint",
            "contactType": "customer support",
            "telephone": "[19008904]",
            "email": "<%=UIHelper.GetConfigByName("Email") %>"
        }
        <%--"sameAs": [ 
            "https://cenhomes.vn/",
            "https://cengroup.vn",
            "http://thamdinhgiatheky.vn/",
            "https://www.youtube.com/channel/UCwT0fGV1LDjE25PCX9pGkJA",
            "https://www.facebook.com/cenhomes.vn/"
        ],
        "potentialAction": {
            "@type": "SearchAction",
            "target": "https://gianhadat.cenhomes.vn/danh-sach-bat-dong-san?query={search_term_string}",
            "query-input": "required name=search_term_string"
        }--%>
    }
    </script>
    <div class="container">
        <section class="banner-home">
            <div class="row no-gutters">
                <div class="col-lg-8 col-md-8 col-sm-12 col-12">
                    <%//Get slide o day %>
                    <%var slide = ConfigBo.AdvGetByType(2, lang).OrderBy(r => r.SortOrder).ToList(); %>
                    <div id="carouselExampleIndicators" class="carousel slide slide-home" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <%for (int i = 0; i < slide.Count(); i++)
                                { %>
                            <li data-target="#carouselExampleIndicators" data-slide-to="<%=i %>" <%=i==0?"class=\"active\"":"" %>></li>
                            <%} %>
                        </ol>
                        <div class="carousel-inner">
                            <%for (int i = 0; i < slide.Count(); i++)
                                { %>
                            <div class="carousel-item <%=i==0?"active":"" %>">
                                <a href="<%=slide[i].Url %>">
                                    <img src="/uploads/<%=slide[i].Thumb %>" class="d-block w-100" alt="<%=domainName %>">
                                </a>
                            </div>
                            <%} %>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <i class="fas fa-chevron-circle-left"></i>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <i class="fas fa-chevron-circle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-12 box-right">
                    <div class="row no-gutters h-100">
                        <div class="col-12 res">
                            <a href="/<%=currentLanguage %>/dat-lich-kham" title="" class="link">
                                <img src="/Themes/images/res-ic.svg" class="img-fluid mr-3" />
                                <%--<%=UIHelper.MultiLanguageManual(lang,"ĐĂNG KÝ KHÁM","REGISTER") %>--%>
                                <%=Language.RegisterBanner %>
                            </a>
                        </div>
                        <div class="col-6 contact">
                            <a href="/<%=currentLanguage %>/lien-he" title="" class="link">
                                <img src="/Themes/images/contact-ic.svg" class="img-fluid mb-3" />
                                <div class="px-4">
                                    <%--<%=UIHelper.MultiLanguageManual(lang,"LIÊN HỆ","CONTACT") %>--%>
                                    <%=Language.Contact %>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 instruction">
                            <a href="/<%=currentLanguage %>/huong-dan-khach-hang/huong-dan-benh-nhan" title="" class="link">
                                <img src="/Themes/images/instruction-ic.svg" class="img-fluid mb-3" />
                                <div class="px-4">
                                    <%--<%=UIHelper.MultiLanguageManual(lang,"HƯỚNG DẪN","CUSTOMER") %>--%>
                                    <%=Language.CustomerBanner %>
                                </div>
                                <div class="px-4"><%=Language.TutorialBanner %></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="heading-ss-home d-none d-md-block">
            <div class="container">
                <h1>
                    <%=UIHelper.GetConfigByName_v1("TieuDeDong1",lang) %>
                </h1>
                <h6>
                    <%=UIHelper.GetConfigByName_v1("NoiDungDong1",lang) %>
                </h6>
            </div>
        </section>
        <section class="strong-point why-us d-none d-md-block">
            <div class="container">
                <h2 class="heading">
                    <%=UIHelper.GetConfigByName_v1("TieuDeSlider1",lang) %>
                </h2>
                <div class="row justify-content-center">
                    <%var stemps_1 = ConfigBo.AdvGetByType(3, lang).OrderBy(r => r.SortOrder); %>
                    <%--<%var regex_get_number = " /\\d+/g"; %>--%>
                    <%foreach (var item in stemps_1)
                        { %>
                    <%--<%var number = new String(item.Content.Where(Char.IsDigit).ToArray()); %>--%>
                    <%--<%var content_text = item.Content.Replace(number, "").TrimStart(); %>--%>
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="item">
                            <div class="icon">
                                <img src="/uploads/thumb/<%=item.Thumb %>" alt="<%=domainName %>" class="img-fluid" />
                            </div>
                            <div class="title">
                                <%var titles = UIHelper.CutContentByMinusCharactor(item.Name); %>

                                <span>
                                    <%if (titles.Count() >= 2)
                                        { %>
                                    <%foreach (var t in titles)
                                        {  %>
                                    <div><%=t.TrimStart().TrimEnd() %></div>
                                    <%} %>
                                    <%} %>
                                    <%else
                                        { %>
                                    <div><%=titles[0].TrimStart().TrimEnd() %></div>
                                    <%} %>
                                </span>
                            </div>
                            <div>
                                <%=item.Content %>
                            </div>
                        </div>
                    </div>
                    <%} %>
                </div>
            </div>
        </section>
        <section class="why-us">
            <div class="heading-ss-home">
                <div class="container">
                    <h2>
                        <%=UIHelper.GetConfigByName_v1("TieuDeSlider2",lang) %>
                    </h2>
                </div>
            </div>
            <div class="container">
                <div class="row py-5">
                    <%var stemp_2 = ConfigBo.AdvGetByType(4, lang).OrderBy(r => r.SortOrder).ToList(); %>
                    <%foreach (var item in stemp_2)
                        { %>
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="item">
                            <div class="icon">
                                <img src="/uploads/thumb/<%=item.Thumb %>" alt="<%=domainName %>" class="img-fluid" />
                            </div>
                            <div class="title">
                                <%var titles = UIHelper.CutContentByMinusCharactor(item.Name); %>

                                <span>
                                    <%if (titles.Count() >= 2)
                                        { %>
                                    <%foreach (var t in titles)
                                        {  %>
                                    <div><%=t.TrimStart().TrimEnd() %></div>
                                    <%} %>
                                    <%} %>
                                    <%else
                                        { %>
                                    <div><%=titles[0].TrimStart().TrimEnd() %></div>
                                    <%} %>
                                </span>
                            </div>
                            <div>
                                <%=item.Content %>
                            </div>
                        </div>
                    </div>
                    <%} %>
                </div>
            </div>
        </section>

        <section class="tech mb-3">
            <div class="container">
                <div class="heading">
                    <%=UIHelper.GetConfigByName_v1("TieuDeSlider3",lang) %>
                </div>
            </div>
            <div class="row justify-content-center px-2">
                <%var config_service_zone = ConfigBo.AdvGetByType(5, lang).OrderBy(r => r.SortOrder).ToList(); %>
                <%foreach (var item in config_service_zone)
                    {  %>
                <div class="col-md-4 col-sm-10 col-12 px-2">
                    <div class="item">
                        <div class="image">
                            <img src="/uploads/<%=item.Thumb %>" alt="<%=domainName %>" class="img-fluid" />
                        </div>
                        <div class="text">
                            <a href="<%=item.Url %>"><%=item.Name %></a>
                        </div>
                    </div>
                </div>
                <%} %>
        </section>

        <section class="news">
            <div class="heading-ss-home">
                <div class="container">
                    <h2><%=UIHelper.GetConfigByName_v1("TieuDeDongTinTuc",lang) %>
                    </h2>
                </div>
            </div>

            <div class="container-fluid">
                <%//Get 4 bai viet hot %>
                <%var total_rows_list_news = 0; %>
                <%var list_news_home = NewsBo.GetNewsDetailWithLanguage(14, lang, 1, 0, string.Empty, 1, 4, ref total_rows_list_news).ToList(); %>
                <div class="row py-4 justify-content-center">
                    <%--<div class="col-xl-5 col-lg-5 col-md-5 col-12">
                        <div class="item-news-large ">
                            <div class="image">
                                <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(list_news_home[0].Type) %>/<%=list_news_home[0].Alias %>/<%=list_news_home[0].Url %>.<%=list_news_home[0].NewsId %>.htm" title="">
                                    <img src="/uploads/<%=list_news_home[0].Avatar %>" class="img-fluid" alt="<%=list_news_home[0].Title %>"" /></a>
                            </div>
                            <h3 class="title">
                                <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(list_news_home[0].Type) %>/<%=list_news_home[0].Alias %>/<%=list_news_home[0].Url %>.<%=list_news_home[0].NewsId %>.htm" title=""><%=list_news_home[0].Title %></a>
                            </h3>
                            <div class="des">
                                <%=list_news_home[0].Sapo %>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-7 col-lg-7 col-md-7 col-12">
                        <%for (int i = 0; i < list_news_home.Count(); i++)
                            { %>
                        <%if (i > 0)
                            { %>
                        <div class="item-news">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                                    <div class="image">
                                        <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(list_news_home[i].Type) %>/<%=list_news_home[i].Alias %>/<%=list_news_home[i].Url %>.<%=list_news_home[i].NewsId %>.htm" title="">
                                            <img src="/uploads/thumb/<%=list_news_home[i].Avatar %>" class="img-fluid" alt="<%=list_news_home[i].Title %>" /></a>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-8 pl-0">
                                    <h3 class="title">
                                        <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(list_news_home[i].Type) %>/<%=list_news_home[i].Alias %>/<%=list_news_home[i].Url %>.<%=list_news_home[i].NewsId %>.htm" title=""><%=list_news_home[i].Title %></a>
                                    </h3>
                                    <div class="des">
                                        <%=list_news_home[i].Sapo %>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%} %>

                        <%} %>
                        <div class="mb-3">

                            <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(list_news_home[0].Type) %>/<%=list_news_home[0].Alias %>" class="btn view-more-news"><%=Language.More %></a>
                        </div>
                    </div>--%>

                    <div class="col-lg-8 col-md-8 col-sm-8 col-12 home-tin-tuc">
                    
                    <div class="list-ck">
                        <%//Lay danh sach tin o day, phan trang %>
                        <%var total = 0; %>
                        <%var pageIndex = 1; %>
                        <%var pageSize = 1; %>
                        <%var col_trai_to = "col-md-8 col-sm-12 col-12"; %>
                        <%var col_phai_to = "col-md-4 col-sm-12 col-12"; %>
                        <%var col_trai_nho = "col-md-4 col-sm-5 col-5"; %>
                        <%var col_phai_nho = "col-md-8 col-sm-7 col-7"; %>
                        <%var list_news_in_zone = NewsBo.GetNewsDetailWithLanguage(14, lang, 1, 0, "", pageIndex, pageSize, ref total).ToList(); %>
                        <%for (int i = 0; i < list_news_in_zone.Count(); i++)
                            {%>
                        <div class="<%=i==0?"item-large":"item" %>">
                            <div class="row">

                                <div class="<%=i==0?col_trai_to:col_trai_nho %>">
                                    <div class="image mb-3 mb-sm-0">
                                        <a class="full-image-link" href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(14) %>/<%=list_news_in_zone[i].Alias %>/<%=list_news_in_zone[i].Url %>.<%=list_news_in_zone[i].NewsId %>.htm" title="">
                                            <img src="/uploads/<%=i>0 ? ("/thumb/"+list_news_in_zone[i].Avatar) : list_news_in_zone[i].Avatar %>" class="img-fluid" alt="<%=list_news_in_zone[i].Title %>" /></a>
                                    </div>
                                </div>
                                <div class="<%=i==0?col_phai_to:col_phai_nho %>">
                                    <h3 class="title">
                                        <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(14) %>/<%=list_news_in_zone[i].Alias %>/<%=list_news_in_zone[i].Url %>.<%=list_news_in_zone[i].NewsId %>.htm" title=""><%=list_news_in_zone[i].Title %></a>
                                    </h3>
                                    <p class="des">
                                        <%=list_news_in_zone[i].Sapo %>
                                    </p>
                                </div>
                            </div>
                            <br />
                            <%--<div class="row">
                                <div class="<%=i==0?col_phai_to:col_phai_nho %>">
                                    <h3 class="title">
                                        <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(14) %>/<%=list_news_in_zone[i].Alias %>/<%=list_news_in_zone[i].Url %>.<%=list_news_in_zone[i].NewsId %>.htm" title=""><%=list_news_in_zone[i].Title %></a>
                                    </h3>
                                    <p class="des">
                                        <%=list_news_in_zone[i].Sapo %>
                                    </p>
                                </div>
                            </div>--%>
                        </div>
                        <%} %>
                    </div>

                </div>
                    <div class="col-md-4 col-sm-12 col-12 px-md-0 mt-md-5">

                                <div class="menu-right">
                                    <div class="heading">
                                        <%=Language.Interested %>
                                    </div>
                                    <%//Lay list 6 bai viet moi nhat %>
                                    <%var more_row = 0; %>
                                    <%var list_10_newest_news = NewsBo.GetNewsDetailWithLanguage(14, lang, 1, 0, string.Empty, 1, 6, ref more_row).Skip(1); %>
                                    <div class="list-link">
                                        <%foreach (var item in list_10_newest_news)
                                            { %>
                                        <div class="item-link" >
                                            <p class=""><a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(item.Type) %>/<%=item.Alias %>/<%=item.Url %>.<%=item.NewsId %>.htm" title=""><%=item.Title %></a></p>
                                            <%--<p class="des"><%=item.Sapo %></p>--%>
                                        </div>
                                        <%} %>
                                    </div>
                                </div>
                            </div>
                    <div class="mb-3">

                            <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(list_news_in_zone[0].Type) %>/<%=list_news_home[0].Alias %>" class="btn view-more-news"><%=Language.More %></a>
                        </div>
                </div>
            </div>
        </section>
        <section class="res">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <iframe width="100%" height="280" src="<%=UIHelper.GetConfigByName_v1("Videohome",lang).Replace("watch?v=","/embed/") %>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 pl-md-5">
                        <%var c = UIHelper.GetConfigByName_v1("TieuDeDongVideo", lang); %>
                        <%var line_videos = UIHelper.CutContentByMinusCharactor(c); %>
                        <div class="heading mt-4 mt-md-0">
                            <%=line_videos[0] %>
                            <div>
                                <%=line_videos[1] %>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" id="txtName" class="form-control" placeholder="<%=Language.Full_Name%>" />
                        </div>
                        <div class="form-group">
                            <input type="text" id="txtPhone" class="form-control" placeholder="<%=Language.Phone_Number%>" />
                        </div>
                        <div class="form-group">
                            <input type="text" id="txtAddress" class="form-control" placeholder="<%=Language.Address%>" />
                        </div>
                        <div class="form-group">
                            <a href="javascript:void(0)" id="dat-lien-he" class="btn"><%=Language.Register%></a>
                        </div>
                    </div>
                </div>
            </div>
   
    </section>

    <section class="reviews">
        <div class="container">
            <div class="heading">
                <%=UIHelper.GetConfigByName_v1("HomePageDongCuoi",lang) %>
            </div>
            <div id="slide-reviews" class="carousel slide slide-review" data-ride="carousel">
                <div class="carousel-inner">
                    <%//Lay bai viet hien thi trang chu cua danh muc cau chuyen khach hang %>
                    <%var total_cckh = 0; %>
                    <%var cau_chuyen_KH = NewsBo.GetNewsDetailWithLanguage(14, lang, 2, 5125, string.Empty, 1, 6, ref total_cckh).ToList(); %>
                    <%for (int i = 0; i < cau_chuyen_KH.Count(); i += 2)
                        { %>
                    <%if (i == 0 || i % 2 == 0)
                        { %>
                    <div class="carousel-item <%=i < 2 ? "active" : "" %>">
                        <div class="row justify-content-center">
                            <div class="col-lg-6 col-md-11 col-sm-12 col-12 pr-lg-4">
                                <div class="item-customer row no-gutters text-md-center text-lg-left ">
                                    <div class="image col-sm-5 col-12">
                                        <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(14) %>/<%=cau_chuyen_KH[i==0?0:i].Alias %>/<%=cau_chuyen_KH[i==0?0:i].Url %>.<%=cau_chuyen_KH[i==0?0:i].NewsId %>.htm">
                                            <img src="/uploads/thumb/<%=cau_chuyen_KH[i==0?0:i].Avatar %>" class="img-fluid" alt="<%=cau_chuyen_KH[i==0?0:i].Title %>" />
                                        </a>
                                        
                                    </div>
                                    <div class="col-sm-7 col-12 px-sm-3 align-self-md-center ">
                                        <div class="name">
                                            <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(14) %>/<%=cau_chuyen_KH[i==0?0:i].Alias %>/<%=cau_chuyen_KH[i==0?0:i].Url %>.<%=cau_chuyen_KH[i==0?0:i].NewsId %>.htm">
                                                <%=cau_chuyen_KH[i==0?0:i].Title %>
                                            </a>
                                            
                                        </div>
                                        <br />
                                        <div class="intro">
                                            <%=cau_chuyen_KH[i==0?0:i].Sapo %>
                                        </div>
                                        <div class="stars">
                                            <%--<i class="fas fa-star d-inline-block mr-2" style="color:#ffc107;font-size:20px;"></i>
                                            <i class="fas fa-star d-inline-block mr-2" style="color:#ffc107;font-size:20px;"></i>
                                            <i class="fas fa-star d-inline-block mr-2" style="color:#ffc107;font-size:20px;"></i>
                                            <i class="fas fa-star d-inline-block mr-2" style="color:#ffc107;font-size:20px;"></i>
                                            <i class="fas fa-star d-inline-block mr-2" style="color:#ffc107;font-size:20px;"></i>--%>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <%if (cau_chuyen_KH.Count() > i)
                                { %>
                            <div class="col-lg-6 col-md-11 col-sm-12 col-12 pr-lg-4">
                                <div class="item-customer row no-gutters">
                                    <div class="image col-sm-5 col-12">
                                        <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(14) %>/<%=cau_chuyen_KH[i==0?i+1:i+1].Alias %>/<%=cau_chuyen_KH[i==0?i+1:i+1].Url %>.<%=cau_chuyen_KH[i==0?i+1:i+1].NewsId %>.htm">
                                            <img src="/uploads/thumb/<%=cau_chuyen_KH[i==0?i+1:i+1].Avatar %>" class="img-fluid" alt="<%=cau_chuyen_KH[i==0?i+1:i+1].Title %>" />
                                        </a>
                                        <%--<img src="/uploads/thumb/<%=cau_chuyen_KH[i==0?i+1:i+1].Avatar %>" class="img-fluid" alt="<%=cau_chuyen_KH[i==0?i+1:i+1].Title %>" />--%>
                                    </div>
                                    <div class="col-sm-7 col-12 px-sm-3">
                                        <div class="name">
                                            <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(14) %>/<%=cau_chuyen_KH[i==0?i+1:i+1].Alias %>/<%=cau_chuyen_KH[i==0?i+1:i+1].Url %>.<%=cau_chuyen_KH[i==0?i+1:i+1].NewsId %>.htm">
                                                <%=cau_chuyen_KH[i==0?i+1:i+1].Title %>
                                            </a>
                                            <%--<%=cau_chuyen_KH[i==0?i+1:i+1].Title %>--%>
                                        </div>
                                        <br />
                                        <div class="intro">
                                            <%=cau_chuyen_KH[i==0?i+1:i+1].Sapo %>
                                        </div>
                                       <div class="stars">
                                            <%--<i class="fas fa-star d-inline-block mr-2" style="color:#ffc107;font-size:20px;"></i>
                                            <i class="fas fa-star d-inline-block mr-2" style="color:#ffc107;font-size:20px;"></i>
                                            <i class="fas fa-star d-inline-block mr-2" style="color:#ffc107;font-size:20px;"></i>
                                            <i class="fas fa-star d-inline-block mr-2" style="color:#ffc107;font-size:20px;"></i>
                                            <i class="fas fa-star d-inline-block mr-2" style="color:#ffc107;font-size:20px;"></i>--%>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <%} %>
                        </div>
                    </div>
                    <%} %>
                    <%} %>
                </div>
                <a class="carousel-control-prev" href="#slide-reviews" role="button" data-slide="prev">
                    <i class="fas fa-chevron-circle-left"></i>
                </a>
                <a class="carousel-control-next" href="#slide-reviews" role="button" data-slide="next">
                    <i class="fas fa-chevron-circle-right"></i>
                </a>
            </div>
            <div class="text-center">
               <%var news_zone = ZoneBo.GetZoneWithLanguageByType(14, lang).ToList(); %>
                            <%var news_zone_parent = news_zone.Where(r => r.ParentId == 0).SingleOrDefault(); %>
                            <%var news_zone_child = news_zone.Where(r => r.ParentId > 0); %>
                <%var cckh = news_zone_child.Where(r => r.Alias.Equals("cau-chuyen-khach-hang")).SingleOrDefault(); %>

                <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(cckh.Type) %>/<%=cckh.Alias %>"" class="btn view-more-news"><%=Language.More %></a>
            </div>
        </div>
    </section>
    </div>
    <%var isPopUp = ConfigBo.GetListConfigLanguageByConfigName("HienThiPopUp",lang); %>
    <%if (isPopUp.Equals("1"))
        { %> 
        <div class="modal" id="myModal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
          <%--<div class="modal-header">
            
          </div>--%>
          <div class="modal-body" style="padding:0;" >
              <%var banner = UIHelper.GetConfigByName("BannerPopUp"); %>
            <div class="banner">
                <img src="/uploads/<%=banner %>" alt="<%=domainName %>" />
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin:-15px;">
              <span aria-hidden="true">&times;</span>
            </button>
            </div>
          </div>
          <%--<div class="modal-footer">
            <button type="button" class="btn btn-primary">Save changes</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>--%>
      </div>
    </div>
            </div>
    <%} %>
    
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
    <script type="text/javascript">
        var api_url = "https://cmshoanglong.migroup.asia/api/customers/Group/Create";
        $(window).on('load', function () {
            //Kiem tra session
            var s = sessionStorage.getItem('show-popup');
            console.log(s);
            if (s != "show") {
                //Neu khong co session thi hien ra
                $('#myModal').modal('show');
                sessionStorage.setItem("show-popup", "show");
            }

        })
        $('#dat-lien-he').off('click').on('click', function () {
            var name = $('#txtName').val();
            var phoneNumber = $('#txtPhone').val();
            //var email = $('#txtEmail').val();
            var address = $('#txtAddress').val();
            var type = "lien-he";
            $.ajax({
                url: api_url,
                type: 'POST',
                data: {
                    TagId: 11,
                    FullName: name,
                    CompanyName: "",
                    Gender: 1,
                    Birthday: null,
                    Address: address,
                    Phone: phoneNumber,
                    Email: "email",
                    FacebookAddress: "fb.com",
                    ZaloAddress: "zalo.com",
                    Note: "KH tư vấn từ Website",
                    Province_Id: 128,
                    TagChildrenId: 5,
                    GoogleMapLink: "",
                    Ratio: 1
                },
                success: function (res) {
                    console.log('Thanh cong, bat thong API' + res);
                    
                }
            })
            R.Post({
                params: {
                    name: name,
                    address: address,
                    phoneNumber: phoneNumber,
                    //note: note,
                    type: type
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "save",
                success: function (res) {
                    if (res.Success) {
                        console.log(1);
                        alert("Cảm ơn Quý khách đã để lại thông tin! Chúng tôi sẽ liên hệ lại sớm!");
                        $('#txtName').val('');
                        $('#txtPhone').val('');
                        $('#txtAddress').val('');
                        R.Post({
                            params: {
                                name: name,
                                //email: email,
                                phoneNumber: phoneNumber,
                                //note: note,
                                type: type,
                                //ngaySinh: birthday,
                                //ngayKham: ngaykham,
                                //address: address,
                                //location: location,
                                //coSo: coSo

                            },
                            module: "ui-action",
                            ashx: 'modulerequest.ashx',
                            action: "send_mail",
                            success: function (res) {
                                console.log('Send mail Successful!');
                                $('#txtName').val('');
                                $('#txtPhoneNumber').val('');
                                $('#txtAddress').val('');
                            }
                        });
                    }
                    // $('.card-header').RLoadingModuleComplete();
                }
            });
            
        })

    </script>
</asp:Content>
