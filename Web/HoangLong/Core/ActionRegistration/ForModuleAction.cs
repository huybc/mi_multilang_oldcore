﻿using CMTravel.CMS.Modules.Authenticate.Actions;
using CMTravel.CMS.Modules.Config.Action;
using CMTravel.CMS.Modules.Customer.Action;
using CMTravel.CMS.Modules.FileManager.Action;
using CMTravel.CMS.Modules.News.Action;
using CMTravel.CMS.Modules.Service.Action;
using CMTravel.CMS.Modules.Slider.Action;
using CMTravel.CMS.Modules.Tag.Action;
using CMTravel.Core.RequestBase;
using CMTravel.CMS.Modules.Zone.Action;
using CMTravel.Pages.Action;
using CMTravel.CMS.Modules.Tour.Action;
using CMTravel.CMS.Modules.Location.Action;
//using CMTravel.Pages.Action;

namespace CMTravel.Core.ActionRegistration
{
    public class ForModuleAction : CmsAsyncRequestBase
    {
        static ForModuleAction()
        {
            var className = typeof(ForModuleAction).Name;
            RegisterAction(className, "tour", typeof(TourActions), true);
            RegisterAction(className, "location", typeof(LocationActions), true);
            RegisterAction(className, "fmanager", typeof(FileManagerActions), true);
            RegisterAction(className, "ui-action", typeof(UIActions), true);
            RegisterAction(className, "service", typeof(ServiceActions), true);
            RegisterAction(className, "authenticate", typeof(AuthenticateAction), true);
            RegisterAction(className, "customer", typeof(CustomerActions), true);
            RegisterAction(className, "zone", typeof(ZoneActions), true);
            //RegisterAction(className, "charge", typeof(ChargeActions), true);
            RegisterAction(className, "config", typeof(ConfigActions), true);
            RegisterAction(className, "adv", typeof(AdvActions), true);
            RegisterAction(className, "news", typeof(NewsActions), true);
         //   RegisterAction(className, "project", typeof(ProjectActions), true);
            //RegisterAction(className, "comment", typeof(CommentActions), true);
            //RegisterAction(className, "answer", typeof(AnswerActions), true);
            //RegisterAction(className, "profile", typeof(UserActions), true);
            //RegisterAction(className, "user", typeof(UserActions), true);
            RegisterAction(className, "tag", typeof(TagActions), true);
           // RegisterAction(className, "hotel", typeof(HotelActions), true);
            //RegisterAction(className, "livecms", typeof(LiveCmsActions), true);
            //RegisterAction(className, "media", typeof(MediaActions), true);
            //RegisterAction(className, "photo", typeof(PhotoActions), true);
            //RegisterAction(className, "hanwha", typeof(HanwhaActions), true);
            //RegisterAction(className, "shop", typeof(ShopActions), true);
            //RegisterAction(className, "experience", typeof(ExperienceActions), true);
            //   RegisterAction(className, "contact", typeof(ContactActions), true);

        }
    }
}