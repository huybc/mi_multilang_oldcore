﻿using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI;

namespace CMTravel.Core.Helper
{
    public abstract class BasePages : Page
    {
        protected BasePages()
        {
        }

        protected override void InitializeCulture()
        {
            try
            {
                var lang = Page.RouteData.Values["languageCode"].ToString();

                if (!string.IsNullOrEmpty(lang))
                {
                    string languageCode = null;
                    switch (lang)
                    {
                        case "vi":
                            languageCode = "vi-VN";
                            break;
                        case "en":
                            languageCode = "en-US";
                            break;
                        case "zh":
                            languageCode = "zh-CN";
                            break;
                        case "ko":
                            languageCode = "ko-KR";
                            break;
                    }
                    HttpContext.Current.Session["language"] = languageCode;
                }
                else
                {
                    HttpContext.Current.Session["language"] = "vi-VN";
                }

                //  }

                var selectedLanguage = HttpContext.Current.Session["language"].ToString();
                Page.UICulture = selectedLanguage;
                Page.Culture = selectedLanguage;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);

            }
            catch (Exception ex)
            {
                var langdefault = "vi-VN";
                string current = "";
                switch (langdefault)
                {
                    case "vi-VN":
                        current = "vi";
                        break;
                    case "en-US":
                        current = "en";
                        break;
                    case "zh-CN":
                        current = "zh";
                        break;
                    case "ko-KR":
                        current = "ko";
                        break;
                }
                Response.Redirect("/" + current);
            }

            // Base
            base.InitializeCulture();
        }


    }
}