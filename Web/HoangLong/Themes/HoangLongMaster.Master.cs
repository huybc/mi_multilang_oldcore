﻿using CMTravel.Core.Helper;
using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;
using Mi.Entity.Base.Zone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMTravel.Themes
{
    public partial class HoangLongMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public List<ZoneWithSimpleField> GetAllZoneWithTreeViewSimpleFields()
        {
            List<ZoneWithSimpleField> simplaFields = new List<ZoneWithSimpleField>();

            var zones = CacheObjectBase.GetInstance<ZoneCached>().GetAllZone((int)ZoneType.All, Current.LanguageJavaCode);
            foreach (var zone in zones)
            {
                simplaFields.Add(new ZoneWithSimpleField
                {
                    Id = zone.Id,
                    Name = zone.Name.Trim(),
                    ShortURL = zone.Url,
                    //   RealName = zone.Name.Trim(),
                });
            }
            return simplaFields;
        }
        public class ZoneWithSimpleField
        {
            public string Name { get; set; }
            public string ShortURL { get; set; }
            // public string RealName { get; set; }
            public int Id { get; set; }
            //  public int ZoneId { get { return this.Id; } }
        }
    }
}