
var swiper = new Swiper('.slide-customers .swiper-container', {
    direction: 'vertical',
    height: 600,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,//click pagination
    },
});

sliderAbout();
function sliderAbout() {
    var sliderAbout = new Swiper('.swiper-container.about', {
        slidesPerView: 4.5,
        spaceBetween: 0,
        loop: true,
        breakpoints: {
            320: {

            },
            567: {
                slidesPerView: 1.5,
                spaceBetween: 15,
            },
            767: {
                slidesPerView: 2.5,
                spaceBetween: 15,
            },
            991: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
        }

    });
}



$(window).scroll(function () {
    if ($(this).scrollTop() >= 30) {        // If page is scrolled more than 50px
        $('.header').addClass("fixed");    // Fade in the arrow
    } else {
        $('.header').removeClass("fixed");   // Else fade out the arrow
    }
});

$('.box-bottom .header-b').click(function () {
    $('.box-bottom').toggleClass('hidden');
})

function topFunction() {
    $('body,html').animate({
        scrollTop: 100                       // Scroll to top of body
    }, 500);
}
function topFunction_home() {
    $('body,html').animate({
        scrollTop: 0                       // Scroll to top of body
    }, 500);
}



jQuery(window).scroll(startCounter);
function startCounter() {
    var hT = jQuery('.strong-point').offset().top,
        hH = jQuery('.strong-point').outerHeight(),
        wH = jQuery(window).height();
    if (jQuery(window).scrollTop() > hT+hH-wH) {
        jQuery(window).off("scroll", startCounter);
        jQuery('.strong-point .item .number').each(function () {
            var $this = jQuery(this);
            jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
                duration: 4000,
                easing: 'swing',
                step: function () {
                    $this.text(Math.ceil(this.Counter) );
                }
            });
        });
    }
}
 

//2-1-2020
$('.header .nav-bottom .nav-item.sub').click(function () {
    $('.header .nav-bottom .nav-item.sub').removeClass("show");
    $(this).toggleClass('show');
    
})

$(document).click(function(event) {
    if (!$(event.target).closest(".header .nav-bottom .nav-item.sub").length) {
      $("body").find(".header .nav-bottom .nav-item.sub").removeClass("show");
    }
  });