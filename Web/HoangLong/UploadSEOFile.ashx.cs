﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMTravel
{
    /// <summary>
    /// Summary description for UploadSEOFile
    /// </summary>
    public class UploadSEOFile : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                string fname = context.Server.MapPath("~/" + file.FileName);
                file.SaveAs(fname);
            }
            context.Response.ContentType = "text/plain";
            context.Response.Write("File Uploaded Successfully!");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}