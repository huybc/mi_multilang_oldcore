﻿R.Zone = {
    Init: function () {
        R.Zone.RegisterEvents();
        this.ZoneId = 0;
        this.ZoneType = 0;
        this.ZoneNode = null;
        //----
        this.PageIndex = 1;
        this.PageSize = 30;
        this.NewsStatus = 1;
        this.startDate = null;
        this.endDate = null;
        this.ItemSelected = [];
        this.XNewsId = 0;
        this.minDate = moment("01/01/2014").format('DD/MM/YYYY');
        this.maxDate = moment("12/31/2050").format('DD/MM/YYYY');
        
        R.Zone.XNews();
    },
    RegisterEvents: function () {
        $('#zoneStatusdll,#zoneTypedll').fselect();
        $('#zoneStatusdll').off('change').on('change',
            function (e) {
                R.Zone.XNews();
            });

        $('#zoneAddBtn').off('click').on('click',
            function () {
                var type = $(this).attr('zone-type');
                location.href = '/cpanel/conf/zone/edit-' + type+ '.htm';
            });

        $('.zone-action .status').off('click').on('click', function (e) {
            var id = $(this).parent().attr('data-id');
            var status = $(this).attr('data');
            R.Zone.XUpdateStatus({
                status: status,
                id: id
            });
        });

        $('.zone-action ._edit').off('click').on('click', function (e) {
            var id = $(this).parent().attr('data-id');
            var type = $('#news-edit').attr('zone-type');
            location.href = '/cpanel/conf/zone/edit-'+type+'-'+id+'.htm';
            
        });
       
        $('#items .item i.icon-bin').off('click').on('click',
            function () {
                var $this = $(this);
                $.confirm({
                    title: 'Thông báo !',
                    type: 'red',
                    content: 'Xác nhận xóa',
                    animation: 'scaleY',
                    closeAnimation: 'scaleY',
                    buttons: {
                        yes: {
                            text: 'Xóa',
                            keys: ['ESC'],
                            action: function () {
                                $this.closest('.item').remove();
                            }
                        },
                        cancel: {
                            text: 'Đóng',
                            btnClass: 're-btn re-btn-default'
                        }
                    }

                });

            });



    },


   
    XUpdateStatus: function (data) {
        R.Post({
            params: data,
            module: "zone",
            ashx: 'modulerequest.ashx',
            action: "update_status",
            success: function (res) {
                if (res.Success) {
                    $.notify("Cập nhật thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.Zone.XNews();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }

        });
    },
    XNews: function () {

        var el = $('#zone-wrapper');
        $(el).RLoading();
        var data = {
            keyword: $('#_search-data').val(),
            zone: 0,
            pageindex: R.Zone.PageIndex,
            pagesize: R.Zone.PageSize,
            status: $('#zoneStatusdll').val(),
            from: R.Zone.startDate,
            to: R.Zone.endDate,
            type: $('#news-edit').attr('zone-type')
        }
        R.Post({
            params: data,
            module: "zone",
            ashx: 'modulerequest.ashx',
            action: "xsearch",
            success: function (res) {
                if (res.Success) {

                    $(el).find('.datatable-scroll._list').html(res.Content);

                    R.CMSMapZone(el);
                    $('.btn-group.btn-action').hide();


                    //pager
                    if ($('#zone-wrapper .datatable-scroll._list table').attr('page-info') != 'undefined') {
                        var pageInfo = $('#zone-wrapper .datatable-scroll._list table').attr('page-info').split('#');
                        var page = pageInfo[0];
                        var extant = pageInfo[1];
                        var totals = pageInfo[2];
                        if (parseInt(totals) < 0) {
                            $('#data-pager').hide();
                            return;
                        } else {
                            $('#data-pager').show();
                        }
                        var rowFrom = '';
                        if (R.Zone.PageIndex === 1) {
                            rowFrom = 'Từ 1 đến ' + extant;
                        } else {
                            rowFrom = 'Từ ' + (parseInt(R.Zone.PageIndex) * parseInt(R.Zone.PageSize) - parseInt(R.Zone.PageSize)) + ' đến ' + extant;
                        }

                        $('#rowInTotals').text(rowFrom + '/' + totals);
                        $('.ipagination').jqPagination({
                            current_page: R.Zone.PageIndex,
                            max_page: page,
                            paged: function (page) {
                                R.Zone.PageIndex = page;
                                R.Zone.XNews();
                            }
                        });
                    }

                    setTimeout(function () {
                        R.Zone.RegisterEvents();
                    },
                        300);
                    //R.ScrollAutoSize('#zone-wrapper .datatable-scroll', function () {
                    //    return $(window).height() - 92;
                    //}, function () {
                    //    return 'auto';
                    //}, {}, {}, {}, true);

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });
                }

                $(el).RLoadingComplete();
            }
        });
    }
 
}
$(function () {
    R.Zone.Init();
});