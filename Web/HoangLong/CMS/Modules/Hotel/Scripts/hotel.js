﻿R.Hotel = {
    Init: function () {
        R.Hotel.RegisterEvents();
        this.ZoneId = 0;
        this.ZoneType = 0;
        this.ZoneNode = null;
        //----
        this.PageIndex = 1;
        this.PageSize = 30;
        this.NewsStatus = 3;
        this.startDate = null;
        this.endDate = null;
        this.ItemSelected = [];
        this.XNewsId = 0;
        this.minDate = moment("01/01/2014").format('DD/MM/YYYY');
        this.maxDate = moment("12/31/2050").format('DD/MM/YYYY');

        if ($('#mini-game').length > 0) {
            R.Hotel.XNews();
            $('[data-popup="tooltip"]').tooltip();
        };


    },
    RegisterEvents: function () {
        $('#gallery-upload-file .r-queue-item .file_upload').off('click').on('click', function (e) {
            R.FileManager.Open(R.Hotel.AttackFile, 'multi');
        });
        $('#titletxt').bind("keypress keyup", function (event) {
            $('#urltxt').val(R.UnicodeToSeo($('#titletxt').val()));
        });

        $('.add-more-option').off('click').on('click',
            function () {
                var $this = $(this);
                var stepid = 'step-content' + Date.now();
                var stepCount = parseInt($('#items .item').length) + 1;
                $this.closest('.room-option').find('.rooms').append(R.Hotel.RoomOptionHtml({
                    id: stepid,
                    stepName: 'Bước ' + stepCount + ': ',
                    content: ''
                }));
                var editor = CKEDITOR.replace(stepid,
                    {
                        allowedContent: true,
                        toolbar: 'basic'
                    });
                editor.focusManager.focus();
               
                R.Hotel.RegisterEvents();

            });

        if ($('#mini-game').length > 0) {

            $('#news-save .btn-primary').off('click').on('click',
                function (e) {
                    var status = $(this).attr('status');
                    var oid = R.Hotel.XNewsId;
                    var title = $('#titletxt').val();
                    var sapo = $('#Sapo').val();
                    var address = $('#Address').val();

                    var zoneddl = $('#_zoneddl').val();
                    var url = $('#urltxt').val();
                    var titleSeo = $('#metaTitletxt').val();
                    var metakeyword = $('#metakeywordtxt').val();
                    var metadescription = $('#metadescriptiontxt').val();
                    var date = $('#datetxt').attr('data');
                    var content = CKEDITOR.instances['contentCkeditor'].getData();

                    var contents = [];
                    $('#items .item').each(function (i, v) {
                        var ckid = $(v).find('.editor').attr('id');
                        var image = $(v).find('img').attr('data-img');
                        var roomsOption = [];
                        $(v).find('.room').each(function (index, elr) {
                            var subCk = CKEDITOR.instances['' + $(elr).find('.editor').attr('id') + ''].getData();
                            var price = $(elr).find('.price').val();
                            var c_price = $(elr).find('.child-price').val();
                            roomsOption.push({
                                option: subCk,
                                price: price,
                                child_price: c_price
                            });

                        });

                        contents.push({
                            title: $(v).find('.title').val(),
                            image: typeof (image) != 'undefined' ? image : '',
                            content: CKEDITOR.instances['' + ckid + ''].getData(),
                            options: JSON.stringify(roomsOption)
                        });

                    });
                    var avatar = $('#gallery-upload-file .r-queue-item').attr('data');
                    var avatars = [];
                    $('#gallery-upload-file .r-queue-item.added').each(function (i, v) {
                        var item = $(v).attr('data');
                        if (typeof (item) != 'undefined' && item.length > 0) {
                            avatars.push({
                                num: i + 1,
                                url: item
                            });
                        }

                    });

                    var data = {
                        id: oid,
                        title: title,
                        sapo: sapo,
                        address: address,
                        //  author: author,
                        // source: source,
                        zone: zoneddl,
                        date: date,
                        content: content,
                        url: url,
                        //status:status,
                        metakeyword: metakeyword,
                        status: status,
                        metadescription: metadescription,
                        avatar: $('#attack-thumb img').attr('data-img'),
                        avatars: avatars.length > 0 ? JSON.stringify(avatars) : '',
                        metatitle: titleSeo,
                       
                        location: $('#groupDdl').val(),
                        rate: $('#rateDdl').val(),
                        contents: JSON.stringify(contents)

                    }
                    console.log(data);
                    R.Hotel.XSave(data);
                });


            $('#module-content .btn-action button._edit').off('click').on('click',
                function (e) {
                    var id = $('#module-content .datatable-scroll table tbody tr td input[type=checkbox]:checked')
                        .attr('id');

                    R.Hotel.XEdit(id);
                });

            $('#btn-new-post').off('click').on('click',
                function () {
                    R.Hotel.XEdit(0);
                });
            var delay = null;
            $('#news-sldebar .navigation li.sub-news-game').off('click').on('click',
                function () {

                    R.Hotel.ItemSelected = [];
                    $('#main-pnl').show('slow');
                    $('#sub-pnl').hide();
                    $('#news-sldebar .navigation li').removeClass('active');
                    $(this).addClass('active');
                    R.Hotel.startDate = R.Hotel.minDate;
                    R.Hotel.endDate = R.Hotel.maxDate;
                    R.Hotel.PageIndex = 1;
                    $('#date-filter span').html("Tất cả: " + R.Hotel.minDate + ' - ' + R.Hotel.maxDate);
                    R.Hotel.NewsStatus = $(this).attr('status');
                    if (delay != null) clearTimeout(delay);
                    delay = setTimeout(function () {
                        R.Hotel.XNews();
                    },
                        200);

                });
            $('#date-filter').off('apply.daterangepicker').on('apply.daterangepicker',
                function (ev, picker) {
                    R.Hotel.startDate = picker.startDate.format('YYYY-MM-DD');
                    R.Hotel.endDate = picker.endDate.format('YYYY-MM-DD');
                    R.Hotel.XNews();
                });
            $('#zone-filer-ddl').off('change').on('change',
                function (e) {
                    R.Hotel.XNews();
                });
            $('#zone-type-ddl').off('change').on('change',
                function (e) {
                    R.Hotel.XNews();
                });
            $('#btn-new-reload').off('click').on('click',
                function (e) {
                    R.Hotel.startDate = R.Hotel.minDate;
                    R.Hotel.endDate = R.Hotel.maxDate;
                    R.Hotel.PageIndex = 1;
                    $('#_search-data').val('');
                    $('#date-filter span').html("Tất cả: " + R.Hotel.minDate + ' - ' + R.Hotel.maxDate);
                    if (delay != null) clearTimeout(delay);
                    delay = setTimeout(function () {
                        R.Hotel.XNews();
                    },
                        200);
                });
            $('#module-content .btn-action button._view').off('click').on('click',
                function (e) {
                    $('#news-preview').RLoading();
                    var id = $('#module-content .datatable-scroll table tbody tr td input[type=checkbox]:checked')
                        .attr('id');
                    $('#news-preview').modal('show').off('shown.bs.modal').on('shown.bs.modal',
                        function () {
                            R.Post({
                                params: { id: id },
                                module: "news",
                                ashx: 'modulerequest.ashx',
                                action: "preview",
                                success: function (res) {
                                    if (res.Success) {
                                        $('#news-preview-content').html(res.Content).find('#body .detail img')
                                            .css({ 'max-width': '80%' }).parent().css({
                                                'text-align': 'center',
                                                'width': '100%',
                                                'display': 'inline-block'
                                            });
                                        $('#news-preview-content p img,#news-preview-content p iframe').parent()
                                            .css('text-align', 'center');
                                        $('#news-preview .modal-content').css('overflow-y', 'hidden');
                                        $('[data-popup="tooltip"]').tooltip();
                                        // check menu action in view

                                        if (R.Hotel.NewsStatus == 3) {
                                            $('.btn-group.btn-action ._unpublish').show("slow");
                                        } else {
                                            $('.btn-group.btn-action ._unpublish').hide("slow");
                                        }
                                        // bài lưu tạm
                                        if (R.Hotel.NewsStatus == 5) {
                                            // từ chối
                                            $('.btn-group.btn-action ._reject').show("slow");
                                        } else {
                                            // từ chối
                                            $('.btn-group.btn-action ._reject').hide("slow");
                                        }

                                        if (R.Hotel.NewsStatus == '6' || R.Hotel.NewsStatus == 6) {
                                            $('.btn-group.btn-action ._request_publish').hide("slow");
                                        } else {

                                            $('.btn-group.btn-action ._request_publish').show("slow");
                                        }

                                        R.ScrollAutoSize('#news-preview .container-news',
                                            function () {
                                                return $(window).height() - 10;
                                            },
                                            function () {
                                                return 'auto';
                                            },
                                            {});

                                        var $zoneItem = $('#news-preview').find('._zone');
                                        if ($zoneItem.length > 0 && $zoneItem != 'undefined') {
                                            var strHtml = '';
                                            var arrayId = $zoneItem.attr('data-id').split(',');
                                            for (var k = 0; k < arrayId.length; k++) {
                                                $(RAllZones).each(function (i, item) {
                                                    if (arrayId[k] == item.Id) {
                                                        if (strHtml.length == 0) {
                                                            strHtml = item.Name;
                                                        } else {
                                                            strHtml += ';' + item.Name;
                                                        }
                                                    }
                                                    return;
                                                });

                                            }
                                            $zoneItem.text(strHtml);
                                        }
                                        R.Post({
                                            params: { id: id },
                                            module: "news",
                                            ashx: 'modulerequest.ashx',
                                            action: "reject_log",
                                            success: function (res) {
                                                var str = '';
                                                if (res.Success) {
                                                    if (parseInt(res.TotalRow) > 0) {
                                                        $.each($.parseJSON(res.Data),
                                                            function (i, item) {
                                                                str += '<li><p><b>' +
                                                                    item.CreatedBy +
                                                                    '</b> - <time>' +
                                                                    item.Date +
                                                                    '</time></p><div id="log_content">' +
                                                                    item.Content +
                                                                    '</div></li>';
                                                            });
                                                        $('#reject-text')
                                                            .html('<div id="_log"><ul>' + str + '</ul></div>').show();

                                                    }
                                                }
                                            }
                                        });

                                        R.Hotel.RegisterEvents();
                                        $('#news-preview').RLoadingComplete();

                                    } else {
                                        alert(res.Message);
                                    }
                                }
                            });
                        });
                });
            $('#module-content .btn-action button.status').off('click').on('click',
                function (e) {
                    var status = $(this).attr('data-status');
                    var data = {
                        id: R.Hotel.ItemSelected.join(','),
                        status: status
                    }
                    $.confirm({
                        title: 'Thông báo',
                        content: 'Xác nhận yêu cầu ?',
                        buttons: {
                            confirm: {
                                text: 'Thực hiện',
                                action: function () {
                                    R.Hotel.XUpdateStatus(data);
                                }
                            },
                            cancel: {
                                text: 'Hủy',
                                action: function () {

                                }
                            },
                        }
                    });


                });

            $('#data-list .datatable-scroll tr td input[type=checkbox]').off('click').on('click',
                function () {

                    var id = $(this).attr('id');
                    if ($(this).is(':checked')) {
                        $(this).closest('tr').addClass('row-selected');
                        R.Hotel.ItemSelected.push(id);
                    } else {

                        var index = R.Hotel.ItemSelected.indexOf(id);
                        if (index > -1) {
                            R.Hotel.ItemSelected.splice(index, 1);
                        }
                        $(this).closest('tr').removeClass('row-selected');
                    }


                    if (R.Hotel.ItemSelected.length === 0) {
                        $('.btn-group.btn-action').slideUp();
                    }
                    if (R.Hotel.ItemSelected.length === 1) {
                        $('.btn-group.btn-action').slideDown("slow");
                        // $('.btn-group.btn-action ._delete').show("slow");
                        //$('.btn-group.btn-action ._view').show("slow");
                        $('.btn-group.btn-action ._edit').show("slow");


                    } else if (R.Hotel.ItemSelected.length > 1) {
                        $('.btn-group.btn-action').slideDown("slow");
                        //  $('.btn-group.btn-action ._delete').show("slow");
                        //$('.btn-group.btn-action ._view').hide();
                        $('.btn-group.btn-action ._edit').hide();
                        // bài đã duyệt
                        //if (R.Hotel.NewsStatus === '1' || R.Hotel.NewsStatus === 1) {
                        //    $('.btn-group.btn-action ._unpublish').show("slow");
                        //} else {
                        //    $('.btn-group.btn-action ._unpublish').hide("slow");
                        //}
                        // bài lưu tạm
                        //if (R.Hotel.NewsStatus === '4' || R.Hotel.NewsStatus === 4) {
                        //    $('.btn-group.btn-action ._publish').show("slow");
                        //} else {
                        //    $('.btn-group.btn-action ._publish').hide("slow");
                        //}


                    }

                    if (R.Hotel.NewsStatus == 5) {
                        $('.btn-group.btn-action ._delete').hide("slow");

                    } else {
                        $('.btn-group.btn-action ._delete').show("slow");
                    }
                    if (R.Hotel.NewsStatus == 5) {
                        $('.btn-group.btn-action ._publish').show("slow");
                    } else {
                        $('.btn-group.btn-action ._publish').hide("slow");
                    }
                    if (R.Hotel.NewsStatus == 3) {
                        $('.btn-group.btn-action ._unpublish').show("slow");
                    } else {
                        $('.btn-group.btn-action ._unpublish').hide("slow");
                    }
                    $('#rows').text(R.Hotel.ItemSelected.length);
                });
        }


        var delay = 0;
        $('#news-sldebar .navigation li.sub-item').off('click').on('click',
            function () {

                R.Hotel.ZoneType = parseInt($(this).attr('data-type'));
                $('#news-sldebar .navigation li').removeClass('active');
                $(this).addClass('active');
                if (delay != null) clearTimeout(delay);
                delay = setTimeout(function () {
                    R.Hotel.MainInit();
                },
                    200);

            });

        $('#btn-add-zone').off('click').on('click',
            function () {
                R.Hotel.Edit(0);

            });


        $('#items .item > i.icon-bin').off('click').on('click',
            function () {
                var $this = $(this);
                $.confirm({
                    title: 'Thông báo !',
                    type: 'red',
                    content: 'Xác nhận xóa',
                    animation: 'scaleY',
                    closeAnimation: 'scaleY',
                    buttons: {
                        yes: {
                            text: 'Xóa',
                            keys: ['ESC'],
                            action: function () {
                                $this.closest('.item').remove();
                            }
                        },
                        cancel: {
                            text: 'Đóng',
                            btnClass: 're-btn re-btn-default'
                        }
                    }

                });

            });
        $('#items .item .rooms .room i.icon-bin').off('click').on('click',
            function () {
                var $this = $(this);
                $.confirm({
                    title: 'Thông báo !',
                    type: 'red',
                    content: 'Xác nhận xóa',
                    animation: 'scaleY',
                    closeAnimation: 'scaleY',
                    buttons: {
                        yes: {
                            text: 'Xóa',
                            keys: ['ESC'],
                            action: function () {
                                $this.closest('.room').remove();
                            }
                        },
                        cancel: {
                            text: 'Đóng',
                            btnClass: 're-btn re-btn-default'
                        }
                    }

                });

            });


        $('#items .item > i.icon-bin').hover(function () {
            $(this).closest('.item').find('._field').css('opacity', .3)

        },
            function () {
                $(this).closest('.item').find('._field').css('opacity', 1)
            });

        $('#addItemBtn').off('click').on('click',
            function () {
                var stepid = 'step-content' + Date.now();
                var stepCount = parseInt($('#items .item').length) + 1;

                $('#items').append(R.Hotel.StepHtml({
                    id: stepid,
                    stepName: 'Bước ' + stepCount + ': ',
                    content: ''
                }));
                var editor = CKEDITOR.replace(stepid,
                    {
                        allowedContent: true,
                        toolbar: 'basic'
                    });
                editor.focusManager.focus();
                var subCk = CKEDITOR.replace('sub_' + stepid,
                    {
                        allowedContent: true,
                        toolbar: 'basic'
                    });
                R.Hotel.RegisterEvents();

            });



        $('#items .item .avatar').off('click').on('click',
            function () {
                var $this = $(this);

                R.Image.SingleUpload($this, function () {

                    //process
                }, function (response, status) {

                    //success
                    if (!response.success) {
                        $.confirm({
                            title: 'Thông báo !',
                            type: 'red',
                            content: response.messages,
                            animation: 'scaleY',
                            closeAnimation: 'scaleY',
                            buttons: {
                                cancel: {
                                    text: 'Đóng',
                                    btnClass: 're-btn re-btn-default'
                                },
                                yes: {
                                    isHidden: true, // hide the button
                                    keys: ['ESC'],
                                    action: function () {

                                    }
                                }
                            }

                        });
                        return;
                    }

                    if (response.success && response.error == 200) {
                        var img = response.path;
                        $this.closest('.avatar').find('img').attr({
                            'src': img + '?w=150&h=100&mode=crop',
                            'data-img': response.name
                        });
                    }


                });

            });

    },
    AttackFile: function (el) {
        if (typeof (el) != "undefined") {

            var temp = '<div class="col-md-3 r-queue-item"><i class="fa fa-picture-o"></i><p>Đăng ảnh</p><div class="file_upload ui-state-default"></div></div>';
            var countNotAdded = $('.gallery-upload-file .r-queue-item:not(.added)').length;
            if (el.length > countNotAdded) {
                var add = el.length - parseInt(countNotAdded);
                for (var j = 0; j < add; j++) {
                    $('#gallery-upload-file ._library').before(temp);
                }
            }

            if ($('.gallery-upload-file .r-queue-item').hasClass('added')) {
                //$.each(el, function (i, v) {
                //lần 2

                //  $('.gallery-upload-file .r-queue-item:not(.added):eq(' + i + ')').attr({ 'in': i }).attr({ 'data': v }).addClass('added').append('<div class="item"><img src="/' + StorageUrl + '/' + v + '"/></div><i class="fa fa-times"></i>');
                $('#gallery-upload-file .r-queue-item:not(.added)').each(function (i, v) {
                    if (typeof (el[i]) != 'undefined') {

                        if (!$(this).hasClass('added')) {
                            $(this).attr({ 'data': el[i] }).addClass('added').append('<div class="item"><img src="/' + StorageUrl + '/' + el[i] + '"/></div><i class="fa fa-times"></i>');
                        }

                    }
                });

            } else {
                $.each(el, function (i, v) {
                    // laanf 1
                    $('.gallery-upload-file .r-queue-item:eq(' + i + ')').attr({ 'data': v }).addClass('added').append('<div class="item"><img src="/' + StorageUrl + '/' + v + '"/></div><i class="fa fa-times"></i>');
                });
            }

            $('.gallery-upload-file .r-queue-item i.fa-times').off('click').on('click', function () {
                var $this = $(this);
                $this.closest('.r-queue-item').removeClass('added').find('div').show().removeAttr('data');
                $this.parent().find('.item').remove();
                $this.parent().find('i.fa-times').remove();

            });
            $("#gallery-upload-file").sortable();
            R.Hotel.RegisterEvents();
            return;

        }
    },
    StepHtml: function (data) {

        return ' <div class="item _add">' +
            '<i class="icon-bin"></i>' +
            '<div class="control-label col-lg-2 text-center _field">' +
            '<div class="avatar"  style="width: auto;height: auto">' +
            '(1500px*630px)<input accept="image/*" id="img_attack_' + data.id + '" type="file" name="files[]" style="display: none" />' +
            '  <label for="img_attack_' + data.id + '"><img  src="/CMS/Themes/Images/nothumb.JPG"/></label> </div></div><div class="col-lg-10 _field">' +
            '<input autocomplete="off" type="text" name="basic" class="form-control title" value="" tabindex="1" required="required" placeholder="Tối đa 255 ký tự">' +
            '<div class="content-tiem" style="padding-top: 10px">' +
            '<textarea class="editor" id=' + data.id + '></textarea>' +
            '</div></div>' +
            '<div class="form-group">' +
            '<label class="col-lg-2"> Option phòng <span class="text-danger">*</span></label>' +
            '<div class="col-lg-10 room-option">' +
            '<div class="rooms"><div class="room"><i class="icon-bin"></i>' +
            '<div class="row">' +
            '<div class="col-lg-9">' +
            '<div class="col-lg-12">Thông tin phòng</div>' +
            '<textarea class="editor" id="sub_' + data.id + '"></textarea>' +
            '</div>' +
            '<div class="col-lg-3">' +
            '<label class="control-label col-lg-12">Giá</label>' +
            '<input autocomplete="off" type="text" name="basic" class="form-control price" tabindex="1" required="required">' +
            '</div>' +
            '<div class="col-lg-3">' +
            '<label class="control-label col-lg-12">Giá trẻ em</label>' +
            '<input autocomplete="off" type="text" name="basic" class="form-control child-price" tabindex="1" required="required">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div style="padding-top: 20px">' +
            '<button type="button" class="btn btn-blue ibtn-xs add-more-option" style="float: right; clear: both"><i class="icon-add position-left"></i>Thêm lựa chọn</button></div></div></div></div></div>';




    },
    RoomOptionHtml: function (data) {

        return '<div class="room"><i class="icon-bin"></i>' +
            '<div class="row">' +
            '<div class="col-lg-9">' +
            '<div class="col-lg-12">Thông tin phòng</div>' +
            '<textarea class="editor" id=' + data.id + '> </textarea>' +
            '</div>' +
            '<div class="col-lg-3">' +
            '<label class="control-label col-lg-12">Giá</label>' +
            '<input autocomplete="off" type="text" name="basic" class="form-control  price" tabindex="1" required="required">' +
            '</div>' +
            '<div class="col-lg-3">' +
            '<label class="control-label col-lg-12">Giá trẻ em</label>' +
            '<input autocomplete="off" type="text" name="basic" class="form-control  child-price" tabindex="1" required="required">' +
            '</div>' +
            '</div>' +
            '</div>';




    },

    XUpdateStatus: function (data) {
        R.Post({
            params: data,
            module: "hotel",
            ashx: 'modulerequest.ashx',
            action: "update_status",
            success: function (res) {
                if (res.Success) {
                    $.notify("Cập nhật thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.Hotel.XNews();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }

        });
    },
    XNews: function () {

        var el = '#data-list';
        $(el).RLoading();
        var data = {
            keyword: $('#_search-data').val(),
            zone: $('#zone-filer-ddl').val(),
            pageindex: R.Hotel.PageIndex,
            pagesize: R.Hotel.PageSize,
            status: R.Hotel.NewsStatus,
            from: R.Hotel.startDate,
            to: R.Hotel.endDate,
            type: $('#zone-type-ddl').val()
        }
        R.Post({
            params: data,
            module: "hotel",
            ashx: 'modulerequest.ashx',
            action: "xsearch",
            success: function (res) {
                if (res.Success) {
                    R.Hotel.ItemSelected = [];
                    $(el).find('.datatable-scroll._list').html(res.Content);
                    $('.btn-group.btn-action').hide();
                    R.CMSMapZone('#data-list');

                    R.ScrollAutoSize('.datatable-scroll', function () {
                        return $(window).height() - 92;
                    }, function () {
                        return 'auto';
                    }, {}, {}, {}, true);
                    //pager
                    if ($('#data-list .datatable-scroll._list table').attr('page-info') != 'undefined') {
                        var pageInfo = $('#data-list .datatable-scroll._list table').attr('page-info').split('#');
                        var page = pageInfo[0];
                        var extant = pageInfo[1];
                        var totals = pageInfo[2];
                        if (parseInt(totals) < 0) {
                            $('#data-pager').hide();
                            return;
                        } else {
                            $('#data-pager').show();
                        }
                        var rowFrom = '';
                        if (R.Hotel.PageIndex === 1) {
                            rowFrom = 'Từ 1 đến ' + extant;
                        } else {
                            rowFrom = 'Từ ' + (parseInt(R.Hotel.PageIndex) * parseInt(R.Hotel.PageSize) - parseInt(R.Hotel.PageSize)) + ' đến ' + extant;
                        }

                        $('#rowInTotals').text(rowFrom + '/' + totals);
                        $('.ipagination').jqPagination({
                            current_page: R.Hotel.PageIndex,
                            max_page: page,
                            paged: function (page) {
                                R.Hotel.PageIndex = page;
                                R.Hotel.XNews();
                            }
                        });
                    }
                    //end pager



                    R.Hotel.RegisterEvents();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });
                }

                $(el).RLoadingComplete();
            }
        });
    },
    XEdit: function (id) {

        $('body').RModuleBlock();
        R.Post({
            params: {
                id: id
                // zoneType: R.Hotel.ZoneType
            },
            module: "hotel",
            ashx: 'modulerequest.ashx',
            action: "xedit",
            success: function (res) {

                if (res.Success) {

                    R.ShowOverlayFull({ content: res.Content }, function () {


                    }, function () {

                    });

                    R.Hotel.XNewsId = id;

                    $('#attack-thumb .avatar').off('click').on('click', function () {
                        R.Image.SingleUpload($('#attack-thumb .avatar'),
                            function () {
                                //process
                            }, function (response, status) {
                                $('#attack-thumb .avatar').RModuleUnBlock();
                                //success
                                if (!response.success) {
                                    $.confirm({
                                        title: 'Thông báo !',
                                        type: 'red',
                                        content: response.messages,
                                        animation: 'scaleY',
                                        closeAnimation: 'scaleY',
                                        buttons: {
                                            cancel: {
                                                text: 'Đóng',
                                                btnClass: 're-btn re-btn-default'
                                            },
                                            yes: {
                                                isHidden: true, // hide the button
                                                keys: ['ESC'],
                                                action: function () {

                                                }
                                            }
                                        }

                                    });
                                    return;
                                }

                                if (response.success && response.error == 200) {

                                    var img = response.path;
                                    $('#attack-thumb .avatar img').attr({
                                        'src': img + '?w=150&h=100&mode=crop',
                                        'data-img': response.name
                                    });
                                }

                            });

                    });
                    setTimeout(function () {
                        $('#_zoneddl').fselect({
                            dropDownWidth: 0,
                            autoResize: true
                        }); $('#groupDdl').fselect({
                            dropDownWidth: 0,
                            autoResize: true
                        });
                        if (id > 0) {
                            $('#_zoneddl').val($('#_zoneddl').attr('data'));
                            $('#_zoneddl').trigger('fselect:updated');

                            $('#groupDdl').val($('#groupDdl').attr('data'));
                            $('#groupDdl').trigger('fselect:updated');
                            $('#rateDdl').val($('#rateDdl').attr('data'));
                            $('#rateDdl').trigger('fselect:updated');
                            $('#datetxt').daterangepicker({
                                autoUpdateInput: false,
                                "singleDatePicker": true,
                                "timePicker": true,
                                "timePicker24Hour": true,
                                "timePickerSeconds": true,
                                //"autoApply": true,
                                "showCustomRangeLabel": false,
                                "alwaysShowCalendars": true,
                                "startDate": moment(),
                                "endDate": R.Hotel.maxDate,
                                "opens": "left",
                                locale: {
                                    format: 'DD/MM/YYYY h:mm A'
                                }
                            }, function (start, end, label) {
                                $('#datetxt').attr('data', start.format('YYYY-MM-DD h:mm A'));
                                $('#datetxt').val(start.format('DD-MM-YYYY h:mm A'));
                            });
                            var imageData = $('#gallery-upload-file').attr('data');
                            if (imageData.length > 0) {
                                var images = $.parseJSON(imageData);
                                var temp = '<div class="col-md-3 r-queue-item"><i class="fa fa-picture-o"></i><p>Đăng ảnh</p><div class="file_upload ui-state-default"></div></div>';
                                if (images.length > $('.gallery-upload-file .r-queue-item').length) {

                                    var add = images.length - $('.gallery-upload-file .r-queue-item').length;
                                    for (var j = 0; j < add; j++) {
                                        $('#gallery-upload-file ._library').before(temp);
                                    }
                                };
                                $.each(images, function (i, v) {
                                    $('#gallery-upload-file .r-queue-item:eq(' + i + ')').attr({ 'data': v.url }).addClass('added').append('<div class="item"><img src="/' + StorageUrl + '/' + v.url + '"/></div><i class="fa fa-times"></i>');
                                    $("#gallery-upload-file").sortable();
                                });
                            }


                            $('.gallery-upload-file .r-queue-item i.fa-times').off('click').on('click', function () {
                                var $this = $(this);
                                $this.closest('.r-queue-item').removeClass('added').find('div').show().removeAttr('data');
                                $this.parent().find('.item').remove();
                                $this.parent().find('i.fa-times').remove();
                            });


                        } else {
                            $('#datetxt').daterangepicker({
                                "singleDatePicker": true,
                                "timePicker": true,
                                "timePicker24Hour": true,
                                "timePickerSeconds": true,
                                //"autoApply": true,
                                "showCustomRangeLabel": false,
                                "alwaysShowCalendars": true,
                                "startDate": moment(),
                                "endDate": R.Hotel.maxDate,
                                "opens": "left",
                                locale: {
                                    format: 'DD/MM/YYYY h:mm A'
                                }
                            }, function (start, end, label) {
                                $('#datetxt').attr('data', start.format('YYYY-MM-DD h:mm A'));
                                $('#datetxt').val(start.format('DD-MM-YYYY h:mm A'));

                            });
                            $('#datetxt').attr('data', moment().format('YYYY-MM-DD h:mm A'));

                        }
                      //  CKEDITOR.replace('subDetail1', { toolbar: 'basic' });

                        autosize(document.querySelectorAll('textarea#Sapo'));
                        R.ScrollAutoSize('#zone-content', function () {
                            return $(window).height() - 10;
                        }, function () {
                            if (CKEDITOR.instances["contentCkeditor"]) {
                                CKEDITOR.instances["contentCkeditor"].destroy();
                            }
                            CKEDITOR.replace('contentCkeditor', { toolbar: 'iweb' });


                            $('#items .item').each(function (i, v) {
                                var ckeditorId = $(v).find('.editor').attr('id');
                                CKEDITOR.replace('' + ckeditorId + '', { toolbar: 'basic' });

                                $(v).find('.room').each(function (index, elr) {
                                    var ckeditorId = $(elr).find('.editor').attr('id');
                                    CKEDITOR.replace('' + ckeditorId + '', { toolbar: 'basic' });

                                });


                            });
                           
                            return 'auto';
                        }, {});
                    }, 200);
                    R.Hotel.RegisterEvents();
                    $('body').RModuleUnBlock();
                   

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }
        });
    },
    XSave: function (data) {
        console.log(data);
        $('#zone-edit').RModuleBlock();
        R.Post({
            params: data,
            module: "hotel",
            ashx: 'modulerequest.ashx',
            action: "xsave",
            success: function (res) {
                if (res.Success) {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.CloseOverlayFull();
                    // R.Hotel.NewsStatus = 4;
                    R.Hotel.XNews();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                $('#zone-edit').RModuleUnBlock();
                R.Hotel.RegisterEvents();
            }
        });
    },

}
$(function () {
    R.Hotel.Init();
});