﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMTravel.Core.Helper;
using Mi.BO.Base.Customer;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.Customer;
using Mi.Entity.Base.Zone;
using Mi.Entity.ErrorCode;
using Mi.Action;
using Mi.Action.Core;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;

namespace CMTravel.CMS.Modules.Customer.Action
{
    public class CustomerActions : ActionBase
    {
        protected override string ResponseContentType
        {
            get { return "text/plain; charset=utf-8"; }
        }
        protected override bool IsResponseDataDirectly
        {
            get { return false; }
        }

        protected override object ProcessAction(string functionName, HttpContext context)
        {
            var responseData = new ResponseData();
            if (!PolicyProviderManager.Provider.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {
                switch (functionName)
                {
                    case "":
                        responseData.Success = false;
                        responseData.Message = Constants.MESG_CAN_NOT_FOUND_ACTION;
                        break;

                    case "xsearch":
                        responseData = Customers();
                        break;

                    case "xsave":
                        responseData = SaveZoneNews();
                        break;
                    case "save":
                        responseData = SaveZoneNews2();
                        break;

                    case "update_status":
                        var status = GetQueryString.GetPost("status", 0);
                        responseData = ChangeStatus(status);
                        break;

                    case "xedit":
                        responseData = XEdit();

                        break;
                    case "search-export":
                        responseData = SearchExport();
                        break;
                    case "update-note":
                        responseData = UpdateNote();
                        break;
                    case "save-thong-tin":
                        responseData = SeveThongTinKhachHang();
                        break;
                }
            }

            return responseData;
        }

        private ResponseData SearchExport()
        {

            var responseData = new ResponseData();
            var startDate = GetQueryString.GetPost("start", DateTime.MinValue);
            var endDate = GetQueryString.GetPost("end", DateTime.MaxValue);
            var totalR = 0;
            var r = CustomerBo.Search(string.Empty, string.Empty, "dat-lich", 1, 1000, ref totalR).ToList();
            var r1 = CustomerBo.Search(string.Empty, string.Empty, "lien-he", 1, 1000, ref totalR).ToList();
            
            r = r.Where(d => d.NgayKham >= startDate && d.NgayKham <= endDate).ToList();
            r1 = r1.Where(d => d.NgayKham >= startDate && d.NgayKham <= endDate).ToList();
            //Xuat Excel
            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Đặt lịch khám");
            var workSheet2 = excel.Workbook.Worksheets.Add("Liên hệ");
            
            workSheet.TabColor = System.Drawing.Color.Black;
            workSheet.DefaultRowHeight = 12;
            //Header of table  
            //  
            workSheet.Row(1).Height = 20;
            workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Row(1).Style.Font.Bold = true;
            workSheet.Cells[1, 1].Value = "STT";
            workSheet.Cells[1, 2].Value = "Họ và tên";
            workSheet.Cells[1, 3].Value = "Điện thoại";
            workSheet.Cells[1, 4].Value = "Email";
            workSheet.Cells[1, 5].Value = "Ngày sinh";
            workSheet.Cells[1, 6].Value = "Ngày khám";
            workSheet.Cells[1, 7].Value = "Ghi chú";
            //Body of table  
            //  
            int recordIndex = 2;
            foreach (var item in r)
            {
                workSheet.Cells[recordIndex, 1].Value = (recordIndex - 1).ToString();
                workSheet.Cells[recordIndex, 2].Value = item.FullName;
                workSheet.Cells[recordIndex, 3].Value = item.Mobile;
                workSheet.Cells[recordIndex, 4].Value = item.Email;
                workSheet.Cells[recordIndex, 5].Value = item.NgaySinh.ToString("dd/MM/yyyy");
                workSheet.Cells[recordIndex, 6].Value = item.NgayKham.ToString("dd/MM/yyyy");
                workSheet.Cells[recordIndex, 7].Value = item.Note;
                recordIndex++;
            }
            workSheet.Column(1).AutoFit();
            workSheet.Column(2).AutoFit();
            workSheet.Column(3).AutoFit();
            workSheet.Column(4).AutoFit();
            workSheet.Column(5).AutoFit();
            workSheet.Column(6).AutoFit();
            workSheet.Column(7).AutoFit();
            //Worksheet2
            workSheet2.TabColor = System.Drawing.Color.Black;
            workSheet2.DefaultRowHeight = 12;
            //Header of table  
            //  
            workSheet2.Row(1).Height = 20;
            workSheet2.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet2.Row(1).Style.Font.Bold = true;
            workSheet2.Cells[1, 1].Value = "STT";
            workSheet2.Cells[1, 2].Value = "Họ và tên";
            workSheet2.Cells[1, 3].Value = "Điện thoại";
            workSheet2.Cells[1, 4].Value = "Email";
            workSheet2.Cells[1, 5].Value = "Ngày sinh";
            workSheet2.Cells[1, 6].Value = "Ngày khám";
            workSheet2.Cells[1, 7].Value = "Ghi chú";
            //Body of table  
            //  
            int recordIndex1 = 2;
            foreach (var item in r1)
            {
                workSheet2.Cells[recordIndex, 1].Value = (recordIndex1 - 1).ToString();
                workSheet2.Cells[recordIndex, 2].Value = item.FullName;
                workSheet2.Cells[recordIndex, 3].Value = item.Mobile;
                workSheet2.Cells[recordIndex, 4].Value = item.Email;
                workSheet2.Cells[recordIndex, 5].Value = item.NgaySinh.ToString("dd/MM/yyyy");
                workSheet2.Cells[recordIndex, 6].Value = item.NgayKham.ToString("dd/MM/yyyy");
                workSheet2.Cells[recordIndex, 7].Value = item.Note;
                recordIndex++;
            }
            workSheet2.Column(1).AutoFit();
            workSheet2.Column(2).AutoFit();
            workSheet2.Column(3).AutoFit();
            workSheet2.Column(4).AutoFit();
            workSheet2.Column(5).AutoFit();
            workSheet2.Column(6).AutoFit();
            workSheet2.Column(7).AutoFit();
            string excelName = "DanhSachDatLich.xlsx";
            excel.SaveAs(new FileInfo(HttpContext.Current.Server.MapPath("~/Export/" + excelName)));
            responseData.Success = true;
            responseData.Data = "/Export/" + excelName;
            return responseData;
        }
        private ResponseData XEdit()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Customer\\Template\\Edit.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData GetListZone()
        {
            ResponseData responseData;
            var rs = new List<ZoneResult>();
            var key = GetQueryString.GetPost("keyword", string.Empty);
            var status = GetQueryString.GetPost("status", -1);
            var type = GetQueryString.GetPost("type", 0);
            var objs = ZoneBo.ZoneSearch(key, status, type);
            foreach (var obj in objs)
            {
                rs.Add(new ZoneResult
                {
                    id = obj.Id + "",
                    parent = obj.ParentId > 0 ? obj.ParentId + "" : "#",
                    text = obj.Name.Replace("+ ", "")
                });
            }
            responseData = ConvertResponseData.CreateResponseData(rs, rs.Count(), "");




            return responseData;
        }
        private ResponseData UpdateNote()
        {
            ResponseData responseData;
            
            var id = GetQueryString.GetPost("id", 0);
            var note = GetQueryString.GetPost("note", string.Empty);

            var result = CustomerBo.Update_NoteNhanVien(id, note);
            responseData = ConvertResponseData.CreateResponseData(CustomerBo.Update_NoteNhanVien(id, note));

            responseData.Success = true;
            responseData.Message = "Cập nhật thành công !";
            return responseData;
        }
        //private ResponseData Customers()
        //{
        //    var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Customer\\Template\\List.aspx");
        //    responseData.Success = true;
        //    return responseData;
        //}

        private ResponseData Customers()
        {
            ResponseData responseData = new ResponseData();
            try
            {
                var type = GetQueryString.GetPost("type", string.Empty);
                var templatePath = string.Empty;
                switch (type)
                {
                    case "lien-he":
                        templatePath = "\\CMS\\Modules\\Customer\\Template\\List.aspx";
                        break;
                    case "dat-lich":
                        templatePath = "\\CMS\\Modules\\Customer\\Template\\DatLich.aspx";
                        break;
                }



                responseData = ConvertResponseData.CreateResponseData("", 1, templatePath);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return responseData;
        }










        private ResponseData SaveZoneNews()
        {
            var responseData = new ResponseData();

            if (!Policy.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {

                var id = GetQueryString.GetPost("id", 0);
                var name = GetQueryString.GetPost("name", string.Empty);
                var email = GetQueryString.GetPost("email", string.Empty);
                var phone = GetQueryString.GetPost("phone", string.Empty);
                var address = GetQueryString.GetPost("address", string.Empty);
                //  var sortOrder = GetQueryString.GetPost("sort", 0);
                var company = GetQueryString.GetPost("company", string.Empty);
                var type = GetQueryString.GetPost("type", string.Empty);
                var service = GetQueryString.GetPost("service", string.Empty);
                var status = GetQueryString.GetPost("status", string.Empty);
                var contact = GetQueryString.GetPost("contact", string.Empty);
                var note = GetQueryString.GetPost("note", string.Empty);
                var ngaySinh = GetQueryString.GetPost("ngaySinh", DateTime.MinValue);
                var ngayKham = GetQueryString.GetPost("ngayKham", DateTime.MaxValue);

                if (!string.IsNullOrEmpty(name))
                {

                    var obj = new CustomerEntity
                    {
                        Id = id,
                        FullName = name,
                        Email = email,
                        Mobile = phone,
                        Address = address,
                        Firm = company,
                        Type = type,
                        Service = service,
                        Status = status,
                        Contactperson = contact,
                        Note = note,
                        NgaySinh = ngaySinh,
                        NgayKham = ngayKham
                    };
                    int outId = 0;
                    if (id > 0)
                    {
                        responseData = ConvertResponseData.CreateResponseData(CustomerBo.Update(obj, ref outId));

                        responseData.Success = true;
                        responseData.Message = "Cập nhật thành công !";
                    }
                    else
                    {
                        responseData = ConvertResponseData.CreateResponseData(CustomerBo.Create(obj, ref outId));

                        if (outId > 0)
                        {
                            responseData.Success = true;
                        }
                        responseData.Message = "Thêm mới thành công !";
                    }
                }
                else
                {
                    responseData.Success = false;
                    responseData.Message = "Invalid zone's name";
                }
            }
            return responseData;
        }

        private ResponseData SaveZoneNews2()
        {
            var responseData = new ResponseData();

            if (!Policy.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {

                var id = GetQueryString.GetPost("id", 0);
                var name = GetQueryString.GetPost("name", string.Empty).GetText();
                var email = GetQueryString.GetPost("email", string.Empty).GetText();
                var phone = GetQueryString.GetPost("phone", string.Empty).GetText();
                var address = GetQueryString.GetPost("address", string.Empty).GetText();
                //  var sortOrder = GetQueryString.GetPost("sort", 0);
                var company = GetQueryString.GetPost("company", string.Empty).GetText();
                var type = GetQueryString.GetPost("type", string.Empty).GetText();
                var service = GetQueryString.GetPost("service", string.Empty).GetText();
                var status = GetQueryString.GetPost("status", "Potential").GetText();
                var contact = GetQueryString.GetPost("contact", string.Empty).GetText();
                var note = GetQueryString.GetPost("note", string.Empty).GetText();

                if (!string.IsNullOrEmpty(name))
                {

                    var obj = new CustomerEntity
                    {
                        Id = id,
                        FullName = name,
                        Email = email,
                        Mobile = phone,
                        Address = address,
                        Firm = company,
                        Type = type,
                        Service = service,
                        Status = status,
                        Contactperson = contact,
                        Note = note
                    };
                    int outId = 0;
                    if (id > 0)
                    {
                        responseData = ConvertResponseData.CreateResponseData(CustomerBo.Update(obj, ref outId));

                        responseData.Success = true;
                        responseData.Message = "Cập nhật thành công !";
                    }
                    else
                    {
                        responseData = ConvertResponseData.CreateResponseData(CustomerBo.Create(obj, ref outId));

                        if (outId > 0)
                        {
                            responseData.Success = true;
                        }
                        responseData.Message = "Thêm mới thành công !";
                    }
                }
                else
                {
                    responseData.Success = false;
                    responseData.Message = "Invalid zone's name";
                }
            }
            return responseData;
        }



        public ResponseData ChangeStatus(int status)
        {
            var username = Policy.GetAccountName();

            var listNewsId = GetQueryString.GetPost("id", "");
            ResponseData responseData = null;
            var listNewsIdUpdated = "";

            var newsIds = listNewsId.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
            {
                responseData = ConvertResponseData.CreateResponseData(ZoneBo.ChangeStatus((int)id, username, status));
                if (responseData.Success)
                {
                    listNewsIdUpdated += ";" + id;

                }
                var objsNews = ZoneBo.GetZoneById(id.ToInt32Return0());
                if (objsNews != null)
                {
                    if ((int)ZoneStatus.Publish == status)
                    {
                        XMLDAL.UpdateNode(objsNews.ShortUrl, id.ToInt32Return0());

                    }

                }
            }

            if (!string.IsNullOrEmpty(listNewsIdUpdated))
            {
                responseData = ResponseData.CreateSuccessResponseData(listNewsIdUpdated.Remove(0, 1), 1, "");
            }
            return responseData;
        }
        public class ZoneResult
        {
            public string id { get; set; }
            public string parent { get; set; }
            public string text { get; set; }
            public string path { get; set; }
        }
        private ResponseData SeveThongTinKhachHang()
        {
            var responseData = new ResponseData();
            string TinNhan = GetQueryString.GetPost("TinNhan", string.Empty);
            string Name = GetQueryString.GetPost("Name", string.Empty);
            string Gmail = GetQueryString.GetPost("Gmail", string.Empty);
            string Phone = GetQueryString.GetPost("Phone", string.Empty);
            string address = GetQueryString.GetPost("address", string.Empty);
            string note = GetQueryString.GetPost("note", string.Empty);
            string type = GetQueryString.GetPost("type", string.Empty);
            string service = GetQueryString.GetPost("service", string.Empty);
            var NguoiLon = GetQueryString.GetPost("nguoilon", 0);
            var TreEm = GetQueryString.GetPost("treem", 0);
            var ngaySinh = GetQueryString.GetPost("ngaySinh", DateTime.MinValue);
            var ngayKham = GetQueryString.GetPost("ngayKham", DateTime.MaxValue);
            //var location = GetQueryString.GetPost("service", string.Empty);
            var coSo = GetQueryString.GetPost("coSo", 0);
            if (!string.IsNullOrEmpty(Name) && !string.IsNullOrEmpty(Phone))
            {
                var obj = new CustomerEntity
                {
                    FullName = Name,
                    Email = Gmail,
                    Mobile = Phone,
                    Note = TinNhan,
                    Type = type,
                    Service = service,
                    NgaySinh = ngaySinh,
                    NgayKham = ngayKham,
                    Address = address,
                    location = note,
                    CoSoKham = coSo,
                    NguoiLon = NguoiLon,
                    TreEm = TreEm
                };
                int outId = 0;

                responseData = ConvertResponseData.CreateResponseData(CustomerBo.Create(obj, ref outId));

                if (outId > 0)
                    responseData.Success = true;
                responseData.Message = "thư đã được gửi thành công ,Chúng tôi sẽ liên hệ tới bạn sớm nhất !";
                responseData.Data = outId;
            }
            else
            {
                responseData.Success = false;
            }
            return responseData;
        }
    }
}