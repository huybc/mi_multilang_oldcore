﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMTravel.Core.Helper;
using Mi.BO.Base.News;
using Mi.Common;
using Mi.Entity.Base.News;

namespace CMTravel.Pages
{
    public partial class HL_BaiVietDetail : BasePages
    {
        public NewsDetailWithLanguageEntity obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var newsId = Utility.ConvertToInt(Page.RouteData.Values["id"]);
                if (newsId > 0)
                {
                    obj = NewsBo.GetNewsDetailWithLanguageById(newsId, Current.LanguageJavaCode);
                    Page.Title = !string.IsNullOrEmpty(obj.MetaTitle) ? obj.MetaTitle : obj.Title;
                    Page.MetaKeywords = obj.MetaKeyword;
                    Page.MetaDescription = obj.MetaDescription;
                }
                else
                {
                    obj = new NewsDetailWithLanguageEntity();

                }
            }
        }
    }
}