﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMTravel.Core.Helper;
using Resources;

namespace CMTravel.Pages
{
    public partial class Contact : BasePages
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = Language.Contact;
        }
    }
}