﻿using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;
using Mi.Entity.Base.Tag;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMTravel.Core.Helper;
using Mi.BO.Base.News;
using Mi.Common;
using Mi.Entity.Base.News;

namespace CMTravel.Pages
{
    public partial class BlogDetail : BasePages
    {
        public NewsEntity obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var newsId = Utility.ConvertToLong(Page.RouteData.Values["id"]);
                if (newsId > 0)
                {
                    obj = NewsBo.GetNewsDetailById(newsId, 1);
                    Page.Title = !string.IsNullOrEmpty(obj.TitleSeo) ? obj.TitleSeo : obj.Title;
                    Page.MetaKeywords = obj.MetaKeyword;
                    Page.MetaDescription = obj.MetaDescription;
                }
                else
                {
                    obj = new NewsEntity();

                }
            }
        }
        protected List<TagNewsWithTagInfoEntity> GetTags(long newsId)
        {
            var tags = CacheObjectBase.GetInstance<TagCached>().GetTagNewsByNewsId(newsId);
            return tags;
        }
    }
}