﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMTravel.Core.Helper;
using Mi.BO.Base.News;
using Mi.BO.Base.Zone;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Zone;

namespace CMTravel.Pages
{
    public partial class BlogCategories : BasePages
    {

        public ZoneEntity zoneObj;
        public List<NewsEntity> news;
        public int totalRows = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsCallback)
            {
                var alias = Page.RouteData.Values["zoneName"];
                if (alias != null)
                {
                    zoneObj = ZoneBo.GetZoneByAlias(alias.ToString(), Current.LanguageJavaCode);
                    if (zoneObj != null)
                    {
                        Page.Title = zoneObj.Name;
                        Page.MetaDescription = zoneObj.MetaDescription;
                        Page.MetaKeywords = zoneObj.MetaKeyword;

                        var pageIndex = Request.QueryString["page"].ToInt32Return0();
                        news = NewsBo.SearchByShortUrl(alias.ToString(), Current.LanguageJavaCode, pageIndex == 0 ? 1 : pageIndex, 5, ref totalRows).ToList();
                        UIPager.TotalItems = totalRows;
                    }
                    else
                    {
                        news = new List<NewsEntity>();
                    }

                }
                else
                {
                    zoneObj = new ZoneEntity();
                    news = new List<NewsEntity>();
                }
            }
        }
    }
}