﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/CMTravel.Master" AutoEventWireup="true" CodeBehind="Location.aspx.cs" Inherits="CMTravel.Pages.Location" %>

<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Pages/Controls/Sub/UIPager.ascx" TagPrefix="uc1" TagName="UIPager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <% var currentLanguageJavaCode = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
        var unitMoney = Current.UnitMoney;
        var priceContact = Language.Contact;
        var priceLable = Language.Price;
    %>

    <div class="hidden" id="zoneAlias"></div>
    <section class="banner-vmb mb-4">
        <img src="/uploads/<%=locationObj!=null?locationObj.Avatar:"" %>" class="w-100" />
        <div class="ovelay">
            <div class="container align-self-center">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="heading">
                            <h1 class="top">
                                <%=locationObj!=null?locationObj.Name:"" %>
                            </h1>
                            <div class="bot m-2 col-lg-8 col-md-8 col-sm-12 col-12">
                                <%=locationObj!=null?locationObj.Content:"" %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class=" container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <section class="ss-name pb-2">
                    <div class="left">
                        <h3 class="tit mb-4"></h3>
                        <div class="font-weight-bold h5">
                            Địa điểm:<%=locationObj!=null?locationObj.Name:"" %>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12">
                <div class="block-filter mb-3">

                    <%
                        var locations = ZoneBo.GetLocationByLanguage(currentLanguageJavaCode, true);
                    %>
                    <%
                        var locaHots = ZoneBo.GetZoneByAlias("viet-nam", currentLanguageJavaCode);
                        if (locaHots != null)
                        {
                            var locationVN = locations.Where(l => l.ParentId == locaHots.Id).ToList();

                    %>
                    <div class="item">
                        <div class="heading mb-3">
                            <asp:Literal runat="server" Text="<%$ Resources:Language,LocalLocation %>"></asp:Literal>
                        </div>
                        <div class="option star">
                            <% foreach (var item in locationVN)
                                { %>
                            <div class="label-checkbox pl-0 mb-3">
                                <a class="" href="/<%=currentLanguage %>/location/<%= item.Url %>" title=""><%= item.Name %></a>
                            </div>

                            <% } %>
                        </div>
                    </div>
                    <% } %>
                    <%
                        var locationQt = ZoneBo.GetZoneByAlias("quoc-te", currentLanguageJavaCode);
                        if (locationQt != null)
                        {
                            var locationsQt = locations.Where(l => l.ParentId == locationQt.Id).ToList();

                    %>
                    <div class="item">
                        <div class="heading mb-3">
                            <asp:Literal runat="server" Text="<%$ Resources:Language,InternationalLocation %>"></asp:Literal>

                        </div>
                        <div class="option star">

                            <% foreach (var item in locationsQt)
                                { %>
                            <div class="label-checkbox pl-0 mb-3">
                                <a class="" href="/<%=currentLanguage %>/location/<%= item.Url %>" title=""><%= item.Name %></a>
                            </div>

                            <% } %>
                        </div>
                    </div>
                    <% } %>
                </div>
            </div>
            <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                <div class="list-result">
                 
                    <%foreach (var tour in tours)
                        { %>
                    <div class="item mb-4">
                        <div class="row no-gutters">
                            <div class="col-lg-5 col-md-5 col-sm-12 col-12 ">
                                <div class="image">
                                    <a href="/<%=currentLanguage %>/tour/<%=tour.Url %>.<%=tour.Id %>.htm" title="">
                                        <img src="/uploads/thumb/<%=tour.Avatar %>" alt="" class="w-100 h-100" /></a>
                                </div>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-12 col-12 py-3 px-2">
                                <h2 class="title">
                                    <a href="/<%=currentLanguage %>/tour/<%=tour.Url %>.<%=tour.Id %>.htm"><%=tour.Title %></a>
                                </h2>
                                <div class="des">
                                    <%=Utility.SubWordInString(tour.Sapo.ToString(),30) %>
                                </div>
                                <div class="item-sub">
                                    <%=tour.SubTitle %>
                                    <label>Mã:</label>
                                    <span><%=tour.Code %></span>
                                    <label>Phương tiện:</label>
                                    <% if (!string.IsNullOrEmpty(tour.Transfers))
                                        {
                                            var trans = tour.Transfers.Split(';');
                                            foreach (var t in trans)
                                            {
                                    %>
                                    <i class="fas <%=t %> ml-2"></i>
                                    <% }
                                        }%>
                                </div>
                                <div class="price ">
                                    <% if (tour.MinPrice > 0)
                                       { %>
                                        <span class="label mr-1">Chỉ từ</span>
                                        <span class="price"><%=UIHelper.FormatCurrency(unitMoney,tour.MinPrice,true) %></span>
                                    <% }
                                       else
                                       { %>
                                        <div class="ml-auto "><%=priceLable %>: <label class="text-danger"><%=priceContact  %></label></div>
                                    <% } %>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    <%} %>
                    <% if (!tours.Any())
                        { %>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                        <asp:Literal runat="server" Text="<%$ Resources:Language,NoData %>"></asp:Literal>
                    </div>
                    <% } %>
                </div>
                <div class="page-container text-center float-right ">
                    <uc1:UIPager runat="server" ID="UIPager" PageSize="20" PageShow="5" />
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
</asp:Content>
