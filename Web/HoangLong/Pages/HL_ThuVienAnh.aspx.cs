﻿using CMTravel.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMTravel.Pages
{
    public partial class HL_ThuVienAnh : BasePages
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Page.Title = "Thư viện ảnh và video";
                Page.MetaDescription = UIHelper.GetConfigByName("MetaDescription");
                Page.MetaKeywords = UIHelper.GetConfigByName("MetaKeyword");
            }
        }
    }
}