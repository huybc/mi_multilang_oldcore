﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/HoangLongMaster.Master" AutoEventWireup="true" CodeBehind="HL_DatLichKham.aspx.cs" Inherits="CMTravel.Pages.HL_DatLichKham" %>

<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style>
        #loading {
            background-color: white;
            position: fixed;
            display: block;
            top: 0;
            bottom: 0;
            z-index: 1000000;
            opacity: 0.8;
            width: 100%;
            height: 100%;
            text-align: center;
        }

            #loading img {
                margin: auto;
                display: block;
                top: calc(50% - 100px);
                left: calc(50% - 10px);
                position: absolute;
                z-index: 999999;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%--<%
        var currentLanguage = Page.RouteData.Values["languageCode"].ToString();
        ///var currentType = Page.RouteData.Values["type"].ToString();
        //var type = "";
        var lang = "";
        switch (currentLanguage)
        {
            case "vi":
                lang = "vi-VN";
                break;
            case "en":
                lang = "en-US";
                break;
        }
    %>--%>
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <div class="container">
        <section class="bg-dat-lich">


            <div class="row ">
                <div class="col-lg-11 col-md-12 col-sm-12 col-12">
                    <div class="row justify-content-center">
                        <div class="col-sm-8 col-12 offset-md-3 text-center mb-4">
                            <h4 class="text-uppercase mb-2"><%=UIHelper.GetConfigByName_v1("TieuDeDatLich",lang) %></h4>
                            <div><%=UIHelper.GetConfigByName_v1("NoiDungeDatLich_1",lang) %></div>
                            <div><%=UIHelper.GetConfigByName_v1("NoiDungeDatLich_2",lang) %></div>
                        </div>
                    </div>
                    <form class="form-dat-lich">
                        <div class="form-group row justify-content-center">
                            <label for="" class="col-lg-3 col-sm-3 col-form-label require"><%=Language.Full_Name %></label>
                            <div class="col-sm-8 col-12">
                                <input type="text" class="form-control" id="txtName" placeholder="<%=Language.Full_Name %>">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <label for="" class="col-lg-3 col-sm-3 col-form-label require"><%=Language.Date_Of_Birth %></label>
                            <div class="col-sm-8 col-12">
                                <input type="text" class="form-control dpk" id="txtBirthday" placeholder="24/07/1998">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <label for="" class="col-lg-3 col-sm-3 col-form-label require"><%=Language.Phone_Number %></label>
                            <div class="col-sm-8 col-12">
                                <input type="text" class="form-control" id="txtPhoneNumber" placeholder="0833336299">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <label for="" class=" col-lg-3 col-sm-3 col-form-label require">
                                <%=Language.Address %>
                            </label>

                            <div class="col-sm-8 col-12">
                                <input type="text" class="form-control" id="txtAddress" placeholder="<%=Language.Address %>">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <label for="" class=" col-lg-3 col-sm-3 col-form-label require">
                                <%=Language.Location %>
                            </label>

                            <div class="col-sm-8 col-12">
                                <select name="" class="form-control" id="cbxLocation">
                                    <option value="65">Hà Nội</option>
                                    <option value="66">Hồ Chí Minh</option>
                                    <option value="67">An Giang</option>
                                    <option value="68">Bà Rịa - Vũng Tàu</option>
                                    <option value="69">Bắc Giang</option>
                                    <option value="70">Bắc Kạn</option>
                                    <option value="71">Bạc Liêu</option>
                                    <option value="72">Bắc Ninh</option>
                                    <option value="73">Bến Tre</option>
                                    <option value="74">Bình Định</option>
                                    <option value="75">Bình Dương</option>
                                    <option value="76">Bình Phước</option>
                                    <option value="77">Bình Thuận</option>
                                    <option value="78">Cà Mau</option>
                                    <option value="79">Cao Bằng</option>
                                    <option value="80">Đắk Lắk</option>
                                    <option value="81">Đắk Nông</option>
                                    <option value="82">Điện Biên</option>
                                    <option value="83">Đồng Nai</option>
                                    <option value="84">Đồng Tháp</option>
                                    <option value="85">Gia Lai</option>
                                    <option value="86">Hà Giang</option>
                                    <option value="87">Hà Nam</option>
                                    <option value="88">Hà Tĩnh</option>
                                    <option value="89">Hải Dương</option>
                                    <option value="90">Hậu Giang</option>
                                    <option value="91">Hòa Bình</option>
                                    <option value="92">Hưng Yên</option>
                                    <option value="93">Khánh Hòa</option>
                                    <option value="94">Kiên Giang</option>
                                    <option value="95">Kon Tum</option>
                                    <option value="96">Lai Châu</option>
                                    <option value="97">Lâm Đồng</option>
                                    <option value="98">Lạng Sơn</option>
                                    <option value="99">Lào Cai</option>
                                    <option value="100">Long An</option>
                                    <option value="101">Nam Định</option>
                                    <option value="102">Nghệ An</option>
                                    <option value="103">Ninh Bình</option>
                                    <option value="104">Ninh Thuận</option>
                                    <option value="105">Phú Thọ</option>
                                    <option value="106">Quảng Bình</option>
                                    <option value="107">Quảng Nam</option>
                                    <option value="108">Quảng Ngãi</option>
                                    <option value="109">Quảng Ninh</option>
                                    <option value="110">Quảng Trị</option>
                                    <option value="111">Sóc Trăng</option>
                                    <option value="112">Sơn La</option>
                                    <option value="113">Tây Ninh</option>
                                    <option value="114">Thái Bình</option>
                                    <option value="115">Thái Nguyên</option>
                                    <option value="116">Thanh Hóa</option>
                                    <option value="117">Thừa Thiên Huế</option>
                                    <option value="118">Tiền Giang</option>
                                    <option value="119">Trà Vinh</option>
                                    <option value="120">Tuyên Quang</option>
                                    <option value="121">Vĩnh Long</option>
                                    <option value="122">Vĩnh Phúc</option>
                                    <option value="123">Yên Bái</option>
                                    <option value="124">Phú Yên</option>
                                    <option value="125">Cần Thơ</option>
                                    <option value="126">Đà Nẵng</option>
                                    <option value="127">Hải Phòng</option>
                                    <option value="128">Chưa phân loại</option>
                                </select>

                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <label for="" class="col-lg-3 col-sm-3 col-form-label require"><%=Language.Medical_day %></label>
                            <div class="col-sm-8 col-12">
                                <input type="text" class="form-control dpk" id="txtNgayKham" placeholder="24/04/2019">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <label for="" class="col-lg-3 col-sm-3 col-form-label require"><%="Cơ sở" %></label>
                            <div class="col-sm-8 col-12">
                                <%//Lay thong tin cac co so %>
                                <%var list_co_so = ZoneBo.GetZoneWithLanguageByType(18, lang).Where(r => r.ParentId > 0).ToList(); %>
                                <% %>
                                <select id="cbx_CoSo" class="form-control">
                                    <%--<%foreach (var item in list_co_so)
                                        { %> 
                                        <option value="<%=item.Id %>"><%=item.Content %></option>
                                    <%} %>--%>
                                    <%for (int i = 0; i < list_co_so.Count(); i++)
                                        { %>
                                    <%var item = list_co_so[0]; %>
                                    <%if (item != null && i == 0)
                                        { %>
                                    <option value="<%=item.Id %>"><%=item.Content %></option>
                                    <%} %>
                                    <%} %>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <label for="" class="col-lg-3 col-sm-3 col-form-label ">Email</label>
                            <div class="col-sm-8 col-12">
                                <input type="text" class="form-control" id="txtEmail" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <label for="" class="col-lg-3 col-sm-3 col-form-label align-self-start"><%=Language.Note %></label>
                            <div class="col-sm-8 col-12">
                                <textarea type="text" rows="6" class="form-control" id="txtNote" placeholder="<%=Language.Note_Detail %>"></textarea>
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <div class="col-sm-8 col-12 offset-md-3 text-center mt-4">
                                <a href="javascript:void(0)" id="dat-lich-kham" data-lang="<%=lang %>" role="button" class="btn btn-dat-lich mx-auto"><%=Language.Send %></a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </section>
    </div>
    <div id="loading" style="display: none">
        <img src="/Themes/images/Spinner-1s-200px.gif" alt="Loading..." />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.dpk').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                //minYear: 1901,
                //maxYear: parseInt(moment().format('YYYY'), 10),
                locale: {
                    "format": "DD/MM/YYYY"
                }
                //}, function (start, end, label) {
                //    var years = moment().diff(start, 'years');
                //    alert("You are " + years + " years old!");
            });
        });
    </script>
    <script>
        $(document).ajaxStart(function () {
            $("#loading").show();
        });
        $(document).ajaxStop(function () {
            $("#loading").hide();
        });
    </script>
    <script type="text/javascript">
        var api_url = "https://cmshoanglong.migroup.asia/api/customers/Group/Create";
        //var api_url = "http://localhost:57406/api/customers/Group/Create";
        $('#dat-lich-kham').off('click').on('click', function () {
            var name = $('#txtName').val();
            var birthday = moment($('#txtBirthday').val(), "DD/MM/YYYY").format("YYYY-MM-DD");
            var ngaykham = moment($('#txtNgayKham').val(), "DD/MM/YYYY").format("YYYY-MM-DD");
            var phoneNumber = $('#txtPhoneNumber').val();
            var address = $('#txtAddress').val();
            var coSo = $('#cbx_CoSo').val();
            var coSoKham = $('#cbx_CoSo option:selected').text();
            var location = $('#cbxLocation').val();
            var email = $('#txtEmail').val();
            var note = $('#txtNote').val();
            var type = "dat-lich";
            var lang = $(this).data('lang');
            console.log(name, birthday, ngaykham, phoneNumber, address, email, note, type);

            R.Post({
                params: {
                    name: name,
                    email: email,
                    phoneNumber: phoneNumber,
                    note: note,
                    type: type,
                    ngaySinh: birthday,
                    ngayKham: ngaykham,
                    address: address,
                    location: location,
                    coSo: coSo

                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "save",
                success: function (res) {
                    if (res.Success) {
                        var id_result = res.Data;
                        console.log(res.Data);
                        $.ajax({
                            url: api_url,
                            type: 'POST',
                            data: {
                                TagId: 12,
                                FullName: name,
                                CompanyName: coSoKham,
                                Gender: 1,
                                Birthday: birthday,
                                INS_DTTM: ngaykham,
                                Address: address,
                                Phone: phoneNumber,
                                Email: email,
                                FacebookAddress: "fb.com",
                                ZaloAddress: "zalo.com",
                                Note: note,
                                Province_Id: location,
                                TagChildrenId: 5,
                                GoogleMapLink: "",
                                Ratio: 1
                            },
                            success: function (res) {
                                console.log('Thanh cong, bat thong API' + res);

                            }
                        })
                        R.Post({
                            params: {
                                name: name,
                                email: email,
                                phoneNumber: phoneNumber,
                                note: note,
                                type: type,
                                ngaySinh: birthday,
                                ngayKham: ngaykham,
                                address: address,
                                location: location,
                                coSo: coSo

                            },
                            module: "ui-action",
                            ashx: 'modulerequest.ashx',
                            action: "send_mail",
                            success: function (res) {
                                console.log('Send mail Successful!');
                                var link = "/" + lang.split("-")[0] + "/xac-nhan-dat-lich?id=" + id_result;
                                window.location.replace(link);
                            }
                        });
                        //alert("Cảm ơn Quý khách đã để lại thông tin! Chúng tôi sẽ liên hệ lại sớm!");

                    }
                    // $('.card-header').RLoadingModuleComplete();
                }
            });
        })
    </script>
</asp:Content>
