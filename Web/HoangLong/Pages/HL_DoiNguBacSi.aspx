﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/HoangLongMaster.Master" AutoEventWireup="true" CodeBehind="HL_DoiNguBacSi.aspx.cs" Inherits="CMTravel.Pages.HL_DoiNguBacSi" %>
<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%--<%
        var currentLanguage = Page.RouteData.Values["languageCode"].ToString();
        var lang = "";
        switch (currentLanguage)
        {
            case "vi":
                lang = "vi-VN";
                break;
            case "en":
                lang = "en-US";
                break;
        }

    %>--%>
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
        %>
    <div class="container">
        <section class="res bg-doi-ngu py-4" style="">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 pl-md-5">
                    <div class="heading mt-4 mt-md-0" style="text-shadow: 0 3px 5px #3c3c3c;">
                        <%=Language.Register %>
                        <div>
                            <%=Language.Call %>: 19008904
                        </div>
                    </div>
                        <div class="form-group">
                            <input type="text" id="txtName" class="form-control" placeholder="<%=Language.Full_Name%>" />
                        </div>
                        <div class="form-group">
                            <input type="text" id="txtPhone" class="form-control" placeholder="<%=Language.Phone_Number%>" />
                        </div>
                        <div class="form-group">
                            <input type="text" id="txtAddress" class="form-control" placeholder="<%=Language.Address%>" />
                        </div>
                        <div class="form-group">
                            <a href="javascript:void(0)" id="dat-lien-he" class="btn"><%=Language.Register%></a>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    </div>
    

    <section class="heading-ss-home py-5 " style="background:#fff;color: #0A1E34;">
        <div class="container">
            <h2>
                <%=UIHelper.GetConfigByName_v1("TieuDeBacSi",lang) %>
            </h2>
            <div style="text-transform: none;font-size: 18px;">
                <%=UIHelper.GetConfigByName_v1("NoiDungBacSi",lang) %>
            </div>
            
        </div>
        
    </section>
    <div class="container">
        <div class="row">
            <div class="col-12 append-div">
                <%//Lay cac thong tin can thiet %>
                <%var zone_doi_ngu_bac_si = ZoneBo.GetZoneWithLanguageByType(3, lang).Where(r => r.ParentId == 0).SingleOrDefault(); %>
                <%var pageIndex = 1; %>
                <%var pageSize = 10; %>
                <%var totalR = 0; %>
                <%var list_bac_si = NewsBo.GetNewsDetailWithLanguage(3, lang, 2, zone_doi_ngu_bac_si.Id, string.Empty, pageIndex, pageSize, ref totalR); %>
                <%foreach (var item in list_bac_si)
                    { %> 
                <%var extras = new BacSiEntity(); %>
                <%if (item.Extras != null)
                    { %> 
                    <%extras = Newtonsoft.Json.JsonConvert.DeserializeObject<BacSiEntity>(item.Extras); %>
                <%} %>
                    <div class="item-doctor">
                    <div class="row no-gutters">
                        <div class="col-md-3 col-sm-5 col-12">
                            <img src="/uploads/thumb/<%=item.Avatar %>" class="img-fluid w-100 avatar" alt="<%=item.Title %>"/>
                        </div>
                        <div class="col-md-4 col-sm-7 col-12 pl-sm-0">
                            <ul class="p-3 py-md-5  list font-weight-bold">
                                <li class="mb-3 mb-md-4 d-flex">
                                    <img src="/themes/images/ic-bs-1.svg" class="img-fluid mr-4"/>
                                    <span class="h4 align-self-center mb-0 title"><%=item.Title %></span>
                                </li>
                                <li class="mb-3 mb-md-4 d-flex">
                                    <img src="/themes/images/ic-bs-2.svg" class="img-fluid mr-3"/>
                                    <span class="align-self-center hoc-vi"><%=extras.HocVi %></span>
                                </li>
                                <li class="mb-3 mb-md-4 d-flex">
                                    <img src="/themes/images/ic-bs-3.svg" class="img-fluid mr-3"/>
                                    <span class=" align-self-center chuyen-khoa"><%=extras.ChuyenKhoa %></span>
                                </li>
                                <li class="mb-3 mb-md-4 d-flex">
                                    <img src="/themes/images/ic-bs-4.svg" class="img-fluid  ml-1 mr-3"/>
                                    <span class="align-self-center chuyen-mon"><%=extras.ChuyenMon %></span>
                                </li>
                                <%--<li class="mb-3 mb-md-4 d-flex">
                                    <img src="/themes/images/ic-bs-5.svg" class="img-fluid  ml-1 mr-3"/>
                                    <span class=" align-self-center "><a href="" class="lich-kham"><%=extras.LichKham %></a></span>
                                </li>--%>
                            </ul>
                        </div>
                        <div class="col-md-5 col-sm-12 col-12">
                           <div class="px-3 py-md-5 ">
                            <div class="mb-4">
                                <span class="color-000">
                                    <%=item.Sapo %>
                                </span>
                                <a href="/<%=currentLanguage %>/doi-ngu-bac-si/<%=zone_doi_ngu_bac_si.Alias %>/<%=item.Url %>.<%=item.NewsId %>.htm" class="xem-them"><%=UIHelper.MultiLanguageManual(lang,"Xem thêm...","More...") %>&gt;&gt;&gt;</a>
                            </div>
                            <div class="text-right">
                                <a href="/<%=currentLanguage %>/doi-ngu-bac-si/<%=zone_doi_ngu_bac_si.Alias %>/<%=item.Url %>.<%=item.NewsId %>.htm" class="btn btn-dat-lich xem-them"><i class="far fa-calendar-alt mr-2" ></i><%=UIHelper.MultiLanguageManual(lang,"Đăng ký khám","Register") %></a>
                            </div>
                           </div>
                           
                        </div>
                    </div>
                </div>    
                <%} %>

                <!--
                    var pageNow = pageIndex; %>
                        var totalPage = (int)Math.Ceiling((double)total / (double)pageSize); %>
                        <span class="page-number mr-3 show-page" data-page="=pageNow %>">=UIHelper.MultiLanguageManual(lang,"Trang","Page") %> <span class="page-now">=pageNow %></span>/=totalPage %></span>
                        <a href="javascript:void(0)" id="xem-them-btn" data-size="=pageSize %>" data-index="=pageIndex %>" data-type="=zone_tar.Type %>" data-zone="=zone_tar.Id %>" data-lang="=lang %>" data-txttype="=currentType %>" class="btn mr-3" role="button">=UIHelper.MultiLanguageManual(lang,"Xem thêm","More") %></a>
                        <a href="javascripts:void(0);" class="btn " onclick="topFunction()" role="button">=UIHelper.MultiLanguageManual(lang,"Về đầu trang","Page Up") %></a>
                    -->
                <div class="text-center mb-5">
                    <a href="javascript:void(0)" data-size="<%=pageSize %>" 
                        data-index="<%=pageIndex %>" 
                        data-type="<%=zone_doi_ngu_bac_si.Type %>"
                        data-zone="<%=zone_doi_ngu_bac_si.Id %>"
                        data-lang="<%=lang %>"
                        data-txttype="doi-ngu-bac-si"
                        id="xem-them-btn"
                        style="color: #1C95D2;font-weight: bold;"><%=Language.More %>&gt;&gt;&gt;</a>
                </div>
                

            </div>
        </div>
        
    </div>
    <div class="container">
        <section class="banner-bs">
        <div class="container">
            <div class="text-center">
                <h2 class="mb-2"><%=UIHelper.GetConfigByName_v1("TitleBacSi_2",lang) %>
                </h2>
                <div class="h4 font-weight-normal mb-4">
                    <%=UIHelper.GetConfigByName_v1("NoiDungBacSi_2",lang) %>
                </div>
                <div>
                    <a href="/<%=currentLanguage %>/dat-lich-kham" class="btn"><%=Language.Register %></a>
                </div>
            </div>
        </div>
    </section>
    </div>
    

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
    <script type="text/javascript">
        $('#xem-them-btn').off('click').on('click', function () {
            var zone_type = $(this).data('type');
            var lang = $(this).data('lang');
            var hot = 2;
            var zone_id = $(this).data('zone');
            var search = "";
            var pageIndex = $(this).data('index')+1;
            var pageSize = $(this).data('size');
            var currentType = $(this).data('txttype');

            R.Post({
                params: {
                    type : zone_type,
                    lang : lang,
                    hot : hot,
                    zone_id: zone_id,
                    search: search,
                    pageIndex: pageIndex,
                    pageSize: pageSize
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "load-trang",
                success: function (res) {
                    if (res.Success) {
                        var result = res.Data;
                        console.log(result);
                        var htm = "";
                        
                        result.forEach(function (e) {
                            var extras = JSON.parse(e.Extras);
                            var link = "/" + lang.split('-')[0] + "/" + currentType + "/" + e.Alias + "/" + e.Url + "." + e.NewsId + ".htm";
                            /*
                             Tu tin viet kieu moi xem nao
                             */
                            $('.item-doctor').last().clone().insertAfter('.item-doctor:last');
                            var el = $('.item-doctor').last();
                            el.find('.avatar').attr("src", '/uploads/thumb/' + e.Avatar + '');
                            //el.find('.avatar').attr("alt", e.Title);
                            el.find('.title').text(e.Title);
                            el.find('.hoc-vi').text(extras.HocVi);
                            el.find('.chuyen-khoa').text(extras.ChuyenKhoa);
                            el.find('.chuyen-mon').text(extras.ChuyenMon);
                            el.find('.lich-kham').text(extras.LichKham);
                            el.find('.xem-them').attr("href", link);
                            el.find('.color-000').html(e.Sapo);
                            
                        });
                        //$('.list-ck').append(htm);
                        //Xu ly hau ky
                        if (result.length > 0) {
                            $('#xem-them-btn').data('index', pageIndex);
                            var index = $('#xem-them-btn').data('index');
                            $('.page-now').html(index);
                        }
                        
                    }

                }, error: function () {
                    //$('#contact').RLoadingModuleComplete();
                }
            });
        })
        $('#dat-lien-he').off('click').on('click', function () {
            var name = $('#txtName').val();
            var phoneNumber = $('#txtPhoneNumber').val();
            //var email = $('#txtEmail').val();
            var address = $('#txtAddress').val();
            var type = "lien-he";
            R.Post({
                params: {
                    name: name,
                    address: address,
                    phoneNumber: phoneNumber,
                    //note: note,
                    type: type
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "save",
                success: function (res) {
                    if (res.Success) {
                        console.log(1);
                        alert("Cảm ơn Quý khách đã để lại thông tin! Chúng tôi sẽ liên hệ lại sớm!");
                        R.Post({
                            params: {
                                name: name,
                                //email: email,
                                phoneNumber: phoneNumber,
                                //note: note,
                                type: type,
                                //ngaySinh: birthday,
                                //ngayKham: ngaykham,
                                //address: address,
                                //location: location,
                                //coSo: coSo

                            },
                            module: "ui-action",
                            ashx: 'modulerequest.ashx',
                            action: "send_mail",
                            success: function (res) {
                                console.log('Send mail Successful!');
                                $('#txtName').val('');
                                $('#txtPhoneNumber').val('');
                                $('#txtAddress').val('');
                            }
                        });
                    }
                    // $('.card-header').RLoadingModuleComplete();
                }
            });
        })
    </script>
</asp:Content>
