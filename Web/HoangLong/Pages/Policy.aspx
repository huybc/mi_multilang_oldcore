﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/CMTravel.Master" AutoEventWireup="true" CodeBehind="Policy.aspx.cs" Inherits="CMTravel.Pages.Policy" %>
<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Pages/Controls/SideBarRight.ascx" TagPrefix="uc1" TagName="SideBarRight" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <div class="blog-detail">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12">
                    <section class="breadcrumb breadcrumb-blog-detail" style="max-width: 100%">
                        <div class="breadcrumb-item parent"><a href="/" title=""><%=Language.Policy %></a></div>
                    </section>
                    <div class="d-flex mb-4">
                        <div class="share d-flex ml-auto">
                            <span class="mr-2 align-self-center">Chia sẻ</span>
                            <a class="mr-2" href="">
                                <img src="/Themes/imgs/fb-ic.png" /></a>
                            <a class="mr-2" href="">
                                <img src="/Themes/imgs/gg-ic.png" /></a>
                        </div>
                    </div>

                    <section class="content-blog">
                        <div class="description">
                        </div>

                        <div class="newsbody">
                            <%=UIHelper.GetConfigByName("Terms") %>
                        </div>
                    </section>


                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <uc1:SideBarRight runat="server" id="SideBarRight" />
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
</asp:Content>
