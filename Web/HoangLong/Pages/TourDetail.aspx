﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/CMTravel.Master" AutoEventWireup="true" CodeBehind="TourDetail.aspx.cs" Inherits="CMTravel.Pages.TourDetail" %>

<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="System.Web.Script.Serialization" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Pages/Controls/TourStick.ascx" TagPrefix="uc1" TagName="TourStick" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <meta property="fb:app_id" content="<%=UIHelper.GetConfigByName("FBID") %>" />
    <meta property="og:url" content="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" />
    <meta property="og:type" content="article" />
    <%if (obj != null)
        { %>
    <meta property="og:title" content="<%= obj.Title %>" />
    <meta property="og:description" content="<%= obj.Sapo.GetText()%>" />
    <meta property="og:image" content="<%=domainName %>/Uploads/<%= obj.Avatar %>" />
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">

    <%
        var priceContact = Language.Contact;
        var priceLable = Language.Price;
        var currentLanguageJavaCode = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
        var currentUnit = Current.UnitMoney;
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        var items = TourDepartureBo.GetByTourIdCurentTime(obj.Id).ToArray();
        var serialization = new JavaScriptSerializer();

        var tours = new List<TourBasicEntity>();
        var zone = new ZoneEntity();
        if (!string.IsNullOrEmpty(obj.ZoneIds))
        {
            var ids = obj.ZoneIds.Split(',');
            if (ids.Any())
            {
                int totalRows = 0;
                zone = ZoneBo.GetZoneById(ids[0].ToInt32Return0());
                var tourResults = NewsBo.SearchByShortUrlTourV2(zone != null ? zone.Alias : "", Current.LanguageJavaCode, 18, 1, 9, ref totalRows).Where(it => it.Id != obj.Id).ToList();
                if (tourResults.Any())
                {
                    tours.AddRange(tourResults);
                }

            }


        }

    %>
    <script type="application/ld+json">
				            {  
                                    "@context":"http://schema.org",
                                    "@type":"Article",
                                    "mainEntityOfPage":{  
                                        "@type":"WebPage",
                                        "@id":"<%= HttpContext.Current.Request.Url.AbsoluteUri %>"
                                    },
                                   "headline":"<%= !string.IsNullOrEmpty(obj.TitleSeo) ? obj.TitleSeo : obj.Title %>",
                                    "image":{  
                                        "@type":"ImageObject",
                                        "url":"<%= domainName + "/uploads/" + obj.Avatar %>",
                                        "height":600,
                                        "width":800
                                    },
                                    "datePublished":"<%= obj.DistributionDate %>",
                                    "dateModified":"<%= obj.LastModifiedDate %>",
                                    "author":{  
                                        "@type":"Person",
                                        "name":"rbland"
                                    },
                                    "publisher":{  
                                        "@type":"Organization",
                                        "name":"rbland.vn",
                                        "logo":{  
                                            "@type":"ImageObject",
                                            "url":"<%= domainName + "/uploads/" + UIHelper.GetConfigByName("Logo") %>",
                                            "width":35,
                                            "height":34
                                        }
                                    },
                                    "description":"<%= obj.Sapo %>"
                            }
    </script>
    <uc1:TourStick runat="server" ID="TourStick" />
    <div class="blog-detail tour-detail bg-F8F8F8 pt-4">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="content-blog d-lg-flex mb-4 border-0">
                        <h1 class="title align-self-center mb-3"><%= obj.Title %></h1>
                        <div class="share d-flex ml-auto mb-3">
                            <span class="mr-2 align-self-center">Chia sẻ</span>
                            <a class="align-self-center mr-2" href="">
                                <img src="/Themes/imgs/fb-ic.png" /></a>
                            <a class=" align-self-center mr-2" href="">
                                <img src="/Themes/imgs/gg-ic.png" /></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-7 col-sm-7 col-12">
                    <section class="slide-tour mb-3">
                        <div class="swiper-container">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <!-- Slides -->
                                <%
                                    var arraySlide = new List<AvatarArray>();
                                    if (!string.IsNullOrEmpty(obj.AvatarArray))
                                        arraySlide = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AvatarArray>>(obj.AvatarArray);

                                %>
                                <%
                                    if (arraySlide.Any())
                                    {
                                        foreach (var item in arraySlide)
                                        { %>
                                <div class="swiper-slide">
                                    <div class="item-slide">
                                        <div class="image">
                                            <img src="/Uploads/<%= item.url %>" alt="" class="w-100 h-100" />
                                        </div>

                                    </div>
                                </div>

                                <% }
                                    }
                                    else
                                    { %>
                                <div class="swiper-slide">
                                    <div class="item-slide">
                                        <div class="image">
                                            <img src="/Uploads/<%= obj.Avatar %>" alt="" class="w-100 h-100" />
                                        </div>

                                    </div>
                                </div>
                                <% } %>
                            </div>
                        </div>
                    </section>
                    <section class="slide-tour-thumb ">
                        <div class="swiper-container thumb">
                            <div class="swiper-wrapper">
                                <% foreach (var item in arraySlide)
                                    { %>
                                <div class="swiper-slide">
                                    <div class="item-slide">
                                        <div class="image">
                                            <img src="/Uploads/<%= item.url %>" alt="" class="w-100 h-100" />
                                        </div>

                                    </div>
                                </div>
                                <% } %>
                            </div>
                        </div>
                    </section>
                    <section class="info-basic">
                        <div class="item">
                            <i class="fas fa-map-marker-alt mr-1"></i>
                            <span class="format-location" data-id="<%= obj.ListLocationId %>"><%= obj.ListLocationId %></span>
                        </div>
                        <div class="item">
                            <i class="fas fa-map-marker-alt mr-1"></i>
                            <%= obj.SubTitle %>
                        </div>
                        <div class="item">
                            <span><%= Language.Transfer %>:</span>
                            <% if (!string.IsNullOrEmpty(obj.Transfers))
                                {
                                    var trans = obj.Transfers.Split(';');
                                    foreach (var t in trans)
                                    {
                            %>
                            <i class="fas <%= t %> ml-2"></i>
                            <% }
                                } %>
                        </div>
                        <div class="item">
                            <%= Language.TourCode %>: <span id="tourCode" style="color: #9f224e; font-weight: bold"><%= obj.Code %></span>
                        </div>
                    </section>
                    <section class="info-detail" id="info-detail">
                        <div class="description ss-text text-bold">
                            <%= obj.Sapo %>
                        </div>
                        <div class="ss-text">
                            <%= obj.Body %>
                        </div>
                        <div class="departure-table" id="departure-table">
                            <div style="padding: 10px; color: #9F224E">
                                <i class="far fa-calendar-alt"></i>
                                <b>
                                    <asp:Literal runat="server" Text="<%$ Resources:Language,Schedule %>"></asp:Literal>
                                </b>
                            </div>
                            <table class="table">

                                <thead>
                                    <tr>
                                        <th><%= Language.StartDate %></th>
                                        <th><%= Language.EndDate %></th>
                                        <th><%= Language.Status %></th>
                                        <th><%= Language.Price %></th>
                                    </tr>
                                </thead>
                                <%

                                    if (items.Any())
                                    {
                                        for (int i = 0; i < items.Count(); i++)
                                        {
                                %>
                                <tr>
                                    <td><%= UIHelper.GetOnlyCurentDate(items[i].StartDate, Current.LanguageJavaCode) %></td>
                                    <td><%= UIHelper.GetOnlyCurentDate(items[i].EndDate, Current.LanguageJavaCode) %></td>
                                    <td><%=GetTourStatus(items[i].Status.ToInt32Return0()) %></td>
                                    <td><%= UIHelper.FormatCurrency(currentUnit, items[i].Price, true) %></td>
                                </tr>
                                <% }
                                    }
                                    else
                                    {
                                %>
                                <tfoot>
                                    <tr>
                                        <td colspan="4" class="text-center">
                                            <asp:Literal runat="server" Text="<%$ Resources:Language,NoData%>"></asp:Literal>
                                            <section class="book-tour mb-4 text-left">
                                                <p style="color: #9F224E"><%=Language.PriceContact %></p>
                                                <p class="hotline">
                                                    <label><i class="fas fa-mobile-alt"></i></label>
                                                    <a href="tel:<%=UIHelper.GetConfigByName("HotLine").Replace("-","") %>"><b><%=UIHelper.GetConfigByName("HotLine").Replace("-","") %></b></a>
                                                </p>
                                                <p class="email">
                                                    <i class="fas fa-phone-volume"></i>
                                                    <b><%=UIHelper.GetConfigByName("Mobile") %></b>
                                                </p>
                                                <p class="email">
                                                    <label><i class="far fa-envelope"></i></label>
                                                    <b><%=UIHelper.GetConfigByName("Email") %></b>
                                                </p>
                                                <p class="email">
                                                    <label><i class="fas fa-map-marker-alt"></i></label>
                                                    <b><%=UIHelper.GetConfigByName("Address") %></b>
                                                </p>
                                            </section>
                                        </td>
                                    </tr>
                                </tfoot>
                                <% } %>
                            </table>

                        </div>
                        <div class="entry-footer container">
                            <div class="row">
                                <div class="text-right">
                                    <div class="fb-like" data-href="<%= HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="comment-facebook" style="width: 100%">
                                    <div class="fb-comments" data-href="<%= HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="100%" data-numposts="5"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-5 col-sm-5 col-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <%if (!items.Any())
                                { %>
                            <section class="book-tour mb-4">
                                <p class="header text-center"><b><%=Language.BookingTour %></b></p>
                                <p style="color: #9F224E"><%=Language.PriceContact %></p>
                                <p class="hotline">
                                    <label><i class="fas fa-mobile-alt"></i></label>
                                    <a href="tel:<%=UIHelper.GetConfigByName("HotLine").Replace("-","") %>"><b><%=UIHelper.GetConfigByName("HotLine").Replace("-","") %></b></a>
                                </p>
                                <p class="email">
                                    <i class="fas fa-phone-volume"></i>
                                    <b><%=UIHelper.GetConfigByName("Mobile") %></b>
                                </p>
                                <p class="email">
                                    <label><i class="far fa-envelope"></i></label>
                                    <b><%=UIHelper.GetConfigByName("Email") %></b>
                                </p>
                                <p class="email">
                                    <label><i class="fas fa-map-marker-alt"></i></label>
                                    <b><%=UIHelper.GetConfigByName("Address") %></b>
                                </p>
                            </section>
                            <% }
                                else

                                { %>
                            <section class="book-tour mb-4">
                                <div class="heading"><%= Language.Booking %></div>
                                <div class="form-group row" style="margin-top: 20px">
                                    <label class="col-xl-6 col-12 col-form-label">
                                        <%= Language.DepartureSchedule %>:
                                    </label>
                                    <div class="col-xl-6 col-12">
                                        <select id="slNgayKhoiHanh" class="select form-control">
                                            <option value=''>- <%= Language.Select %> -</option>
                                            <%
                                                if (items.Any())
                                                {
                                                    for (int i = 0; i < items.Count(); i++)
                                                    {
                                            %>
                                            <option value='<%= string.Format("{0},{1},{2},{3}", items[i].Price, items[i].ChildPrice, UIHelper.GetOnlyCurentDate(items[i].StartDate,"en-US"),  UIHelper.GetOnlyCurentDate(items[i].EndDate,"en-US")) %>'><%= UIHelper.GetOnlyCurentDate(items[i].StartDate, currentLanguageJavaCode,false) %></option>
                                            <% }
                                                } %>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row justify-content-end price">
                                    <div class="col-xl-4 col-md-7 col-sm-6 col-4 align-self-center amount">
                                        <span class="font-weight-bold sluong" id="slNguoiLon" data-sl="1">1</span> <span id="lnglon"><%= Language.Adulthood %></span>
                                    </div>
                                    <div class="col-xl-4 col-md-5 col-sm-6 col-4 align-self-center cost">
                                        x <span class="priceNguoiLon" id="priceNguoiLon" data-gia="0"></span>
                                    </div>
                                    <div class="btn-group col-xl-4 col-md-5 col-sm-6 col-4 align-self-center">
                                        <button type="button " class="btn btn-secondary minus">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-secondary plus">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="form-group row justify-content-end price">
                                    <div class="col-xl-4 col-md-7 col-sm-6 col-4 align-self-center amount">
                                        <span class="font-weight-bold sluong" id="slTreEm" data-sl="1">1</span><span id="ltreep"> <%= Language.Children %></span>
                                    </div>
                                    <div class="col-xl-4 col-md-5 col-sm-6 col-4 align-self-center cost giaTreEm">
                                        x <span class="priceTreEm" id="priceTreEm" data-gia="0"></span>
                                    </div>
                                    <div class="btn-group col-xl-4 col-md-5 col-sm-6 col-4 align-self-center ">
                                        <button type="button " class="btn btn-secondary minus">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-secondary plus">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <i style="font-size: 10px; color: #9f224e">(<%=Language.NotePriceChildren %>.)</i>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xl-6 col-md-6 col-sm-4 col-6 align-self-center amount">
                                        <%= Language.Total %>
                                    </div>
                                    <div class="col-xl-6 col-md-6 col-sm-8 col-6 align-self-center text-right sum">
                                        <span id="sumPrice"></span>
                                    </div>

                                </div>
                                <button class="btn btn-book mb-2 col-12" data-toggle="modal" id="btnSetTour" data-target=""><%= Language.BookingNow %></button>
                                <div class="text-center color-FF0000 mb-3">
                                    <%= Language.CallSupport %> <a href="tel:<%= UIHelper.GetConfigByName("HotLine") %>"><%= UIHelper.GetConfigByName("HotLine") %></a>
                                </div>
                            </section>
                            <% } %>
                            <ul class="list-group menu-right-tour mb-4">

                                <li class="list-group-item">
                                    <a href="#info-detail" title="">
                                        <img src="/Themes/imgs/list-on-a-notebook-stroke-symbol.png" class="mr-3" />
                                        <%=Language.TotalProcess %></a>
                                </li>
                                <li class="list-group-item">
                                    <a href="#departure-table" title="">
                                        <img src="/Themes/imgs/airplane.png" class="mr-3" /><%=Language.Schedule %></a>
                                </li>
                                <%--<li class="list-group-item">
                            <a href="" title="">
                                <img src="/Themes/imgs/wedding-planning.png" class="mr-3" />
                                ĐIỀU KHOẢN & QUY ĐỊNH
                            </a>
                        </li>--%>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <%-- <div class="ads">
                            <a href="" title="">
                                <img src="/uploads/<%=UIHelper.GetConfigByName("AdvHotelDetail") %>" class="img-fluid mb-3" alt=""></a>
                        </div>--%>
                        <div class="blog-ss-right ">
                            <div class="heading ">
                                <div><%= Language.HotTours %></div>
                            </div>
                            <%
                                int totalHotRows = 0;
                                var topView = NewsBo.GetHotTours(18, Current.LanguageJavaCode, 1, 1, 12, ref totalHotRows).ToList();
                                for (int i = 0; i < topView.Count(); i++)
                                {%>

                            <%  

                                if (i == 0)
                                { %>

                            <div class="item-left mb-3">
                                <div class="image">
                                    <a href="/<%=currentLanguage %>/tour/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="<%=topView[i].Title %>">
                                        <img src="/Uploads/thumb/<%=topView[i].Avatar %>" alt="" /></a>
                                </div>
                                <h2 class="title">
                                    <a href="/<%=currentLanguage %>/tour/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title=""><%=topView[i].Title %></a>
                                </h2>
                                <div class="detail">
                                    <%=Utility.SubWordInString(topView[i].Sapo,40) %>
                                </div>

                            </div>
                            <%} %>
                            <%if (i > 0)
                                { %>
                            <div class="item-right d-flex">
                                <div class="image">
                                    <a href="/<%=currentLanguage %>/tour/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="<%=topView[i].Title %>">
                                        <img src="/Uploads/thumb/<%=topView[i].Avatar %>" alt="<%=topView[i].Title %>" /></a>
                                </div>
                                <div class="text">
                                    <h2 class="title">
                                        <a href="/<%=currentLanguage %>/tour/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="<%=topView[i].Title %>"><%=topView[i].Title %></a>
                                    </h2>
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class=""><%=topView[i].SubTitle %></span>
                                    </div>
                                    <% if ( topView[i].MinPrice > 0)
                                       { %>
                                        <div class="cost ml-auto" style="font-weight: bold"><%= UIHelper.FormatCurrency(currentUnit,  topView[i].MinPrice, true) %></div>
                                    <% }
                                       else
                                       { %>
                                        <div class="ml-auto "><%=priceLable %>: <label class="text-danger"><%=priceContact  %></label></div>
                                    <% } %>
                                   
                                </div>

                            </div>
                            <%} %>
                            <%} %>
                        </div>
                    </div>
                </div>
            </div>
            <section class="row list-combo">
                <div class="col-md-12 col-sm-12 col-12">
                    <div class="row tour-cate">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                            <section class="ss-name">
                                <div class="left">
                                    <h3 class="tit"><%=Language.ArticleRelation %>
                                    </h3>
                                    <%--<div>Làm việc cả năm. Đến lúc tưởng thưởng</div>--%>
                                </div>
                                <div class="right">
                                    <a href="/<%=currentLanguage %>/tour/<%= zone!=null?zone.Alias:"" %>" class="font-italic color-0072ff" title=""><%=Language.ViewAll %></a>
                                </div>
                            </section>

                            <section class="list row">
                                <%foreach (var tour in tours)
                                    { %>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="item item-tour hot">
                                        <div class="image">
                                            <a href="/<%=currentLanguage %>/tour/<%=tour.Url %>.<%=tour.Id %>.htm" title="<%=tour.Title %>">
                                                <img src="/uploads/<%=tour.Avatar %>" alt="" /></a>
                                            <div class="flag">
                                            </div>
                                            <div class="text format-location" data-id="<%=tour.ListZoneId %>"><%=tour.ListZoneId %></div>

                                        </div>
                                        <div class="content px-3 ">
                                            <div class="tit">
                                                <a href="/<%=currentLanguage %>/tour/<%=tour.Url %>.<%=tour.Id %>.htm" title="<%=tour.Title %>"><%=tour.Title %></a>
                                            </div>

                                            <div class="d-flex mb-2">
                                                <div class="time ">
                                                    <i class="far fa-clock mr-2"></i>
                                                    <span class=""><%=tour.SubTitle %></span>
                                                </div>
                                                <% if (tour.MinPrice > 0)
                                                   { %>
                                                    <div class="cost ml-auto"><%= UIHelper.FormatCurrency(currentUnit, tour.MinPrice, true) %></div>
                                                <% }
                                                   else
                                                   { %>
                                                    <div class="ml-auto "><%=priceLable %>: <label class="text-danger"><%=priceContact  %></label></div>
                                                <% } %>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <%} %>
                                <%if (!tours.Any())
                                    {
                                %>
                                <p class="text-center">
                                    <asp:Literal runat="server" Text="<%$ Resources:Language,NoData %>"></asp:Literal>
                                </p>
                                <% } %>
                            </section>

                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg modal-book-tour" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" id="exampleModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><%=Language.BookingTour %></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="example-email-input" class="col-md-2 col-sm-12 col-12 col-form-label">
                            <%=Language.FullName %><i style="color: red">(*)</i>:</label>
                        <div class="col-md-4 col-sm-12 col-12">
                            <input class="form-control" type="email" id="txtName" autocomplete="off">
                        </div>
                        <label for="example-email-input" class="col-md-2 col-sm-12 col-12 col-form-label">
                            <%=Language.Birthday %>:</label>
                        <div class="col-md-4 col-sm-12 col-12">
                            <input class="form-control" type="email" id="txtBirthday" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-email-input" class="col-md-2 col-sm-12 col-12 col-form-label"><%=Language.Mobile %><i style="color: red">(*)</i>:</label>
                        <div class="col-md-4 col-sm-12 col-12">
                            <input class="form-control" type="email" id="txtPhone" autocomplete="off">
                        </div>
                        <label for="example-email-input" class="col-md-2 col-sm-12 col-12 col-form-label">
                            <%=Language.Address %>:</label>
                        <div class="col-md-4 col-sm-12 col-12">
                            <input class="form-control" type="email" id="txtAddress" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-email-input" class="col-md-2 col-sm-12 col-12 col-form-label"><%=Language.Email %>:</label>
                        <div class="col-md-4 col-sm-12 col-12">
                            <input class="form-control" type="email" id="txtEmail" autocomplete="off">
                        </div>
                        <label for="example-email-input" class="col-md-2 col-sm-12 col-12 col-form-label">
                            <%=Language.Sex %>:</label>
                        <div class="col-md-4 col-sm-12 col-12">
                            <select class="select form-control" id="txtGender">
                                <option>- <%=Language.Select %> -</option>
                                <option value="<%=Language.Made %>"><%=Language.Made %></option>
                                <option value="<%=Language.Fmade %>"><%=Language.Fmade %></option>
                                <option class="<%=Language.PreferNotToSay %>"><%=Language.PreferNotToSay %></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-email-input" class="col-md-2 col-sm-12 col-12 col-form-label">
                            <%=Language.Note %>:</label>
                        <div class="col-md-10 col-sm-12 col-12">
                            <textarea class="form-control" type="email" rows="4" id="txtNote"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th><%=Language.TourCode %></th>
                                    <th><%=Language.DepartureSchedule %></th>
                                    <th><%=Language.Quantity %></th>
                                    <th><%=Language.Total %></th>
                                </tr>
                            </thead>
                            <tbody id="modal-tour-detail">
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 col-sm-12 col-12 text-center">
                            <button id="commit-customer" class="btn btn-book"><%=Language.Submit %></button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ignismyModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
                </div>
                <div class="modal-body">
                    <div class="thank-you-pop">
                        <img src="/Themes/imgs/Green-Round-Tick.png" alt="">
                        <div class="iheader"><%=Language.ThankYou %>!</div><p><%=Language.ReplyRequest %></p>
                    </div>

                </div>

            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
    <script src="/Pages/Script/DatTour.js"></script>
    <script type="text/javascript ">
        function sliderGalleryThumb() {
            var sliderGalleryThumb = new Swiper('.slide-tour-thumb .thumb', {
                slidesPerView: 4,
                spaceBetween: 15,
                autoPlay: true,
                loop: true,
                centeredSlides: true,
                slideToClickedSlide: true,
                breakpoints: {
                    320: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    567: {
                        slidesPerView: 3,
                    },
                    767: {
                        slidesPerView: 4,
                        spaceBetween: 15,
                    },
                    991: {
                        slidesPerView: 5,
                        spaceBetween: 20,
                    },
                }


            });
            var sliderGallery = new Swiper('.slide-tour .swiper-container', {
                slidesPerView: 1,
                spaceBetween: 0,
                autoPlay: true,
                loop: true,
                thumbs: {
                    swiper: sliderGalleryThumb
                }
            });
        }
        $(document).ready(function () {
            $(function () {
                $('#newsTicker5').breakingNews({
                    effect: 'typography'
                });
            })
            $('input[name="date"]').daterangepicker({
                singleDatePicker: true,
            });

            sliderGalleryThumb();

        });

    </script>
</asp:Content>
