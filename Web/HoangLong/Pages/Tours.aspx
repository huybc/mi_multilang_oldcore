﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/CMTravel.Master" AutoEventWireup="true" CodeBehind="Tours.aspx.cs" Inherits="CMTravel.Pages.Tours" %>

<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Pages/Controls/Banner.ascx" TagPrefix="uc1" TagName="Banner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
    <style>
        .local-tour-name { margin-top: 6%; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <% var currentLanguageJavaCode = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
        var currentUnit = Current.UnitMoney;
        var priceContact = Language.Contact;
        var priceLable = Language.Price;
    %>
    <uc1:Banner runat="server" ID="Banner" />
    <section class="list-combo list-tour container">
        <%
            int totalRows1 = 0;
            //var tours = NewsBo.SearchByShortUrl(zone.ShortUrl, 1, 10, ref totalRows);
            var hotTours = NewsBo.GetHotTours(18, currentLanguageJavaCode, 1, 1, 12, ref totalRows1);
        %>
        <div class="row tour-cate">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <section class="ss-name">
                    <div class="left">
                        <h3 class="tit">
                            <a href="/<%=currentLanguage %>/hot-tour.htm">
                                <asp:Literal runat="server" Text="<%$ Resources:Language,HotTours %>"></asp:Literal>
                            </a>
                        </h3>
                        <%--<div>Làm việc cả năm. Đến lúc tưởng thưởng</div>--%>
                    </div>
                </section>
                <section class="list row">
                    <% foreach (var tour in hotTours)
                        { %>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour hot">
                            <div class="image">
                                <a href="/<%=currentLanguage %>/tour/<%=tour.Url %>.<%=tour.Id %>.htm" title="<%=tour.Title %>">
                                    <img src="/Uploads/thumb/<%=tour.Avatar %>?v=1.0" alt="<%=tour.Title %>" /></a>
                                <div class="flag text">
                                    <div class="format-location" data-id="<%=tour.ListZoneId %>"></div>

                                </div>
                            </div>
                            <div class="content px-3 ">
                                <h2 class="tit">
                                    <a href="/<%=currentLanguage %>/tour/<%=tour.Url %>.<%=tour.Id %>.htm" title="<%=tour.Title %>"><%=tour.Title %></a>
                                </h2>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class=""><%=tour.SubTitle %></span>
                                    </div>
                                    <% if (tour.MinPrice > 0)
                                       { %>
                                        <div class="cost ml-auto"><%= UIHelper.FormatCurrency(currentUnit, tour.MinPrice, true) %></div>
                                    <% }
                                       else
                                       { %>
                                        <div class="ml-auto "><%=priceLable %>: <label class="text-danger"><%=priceContact  %></label></div>
                                    <% } %>
                                </div>
                            </div>

                        </div>
                    </div>
                    <% } %>
                    <% if (!hotTours.Any())
                        { %>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                        <asp:Literal runat="server" Text="<%$ Resources:Language,NoData %>"></asp:Literal>
                    </div>
                    <% } %>
                </section>
            </div>
        </div>
    </section>
    <section class="list-combo list-tour container">
        <%
            var zoneTours = ZoneBo.GetAllZone((int)ZoneType.Tour, currentLanguageJavaCode).Where(it => it.Status == (int)ZoneStatus.Publish).OrderBy(it => it.SortOrder);
            foreach (var zone in zoneTours)
            {
                int totalRows = 0;
                var tours = NewsBo.SearchByShortUrlTourV2(zone.Url, currentLanguageJavaCode, 18, 1, 12, ref totalRows).OrderByDescending(r => r.Id);
        %>
        <div class="row tour-cate">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <section class="ss-name">
                    <div class="left">
                        <h3 class="tit"><a href="/<%=currentLanguage %>/tour-category/<%=zone.Url %>"><%=zone.Name %></a></h3>
                        <div><%=zone.Content %></div>
                    </div>
                    <%-- <div class="right">
                        <a href="/tour/<%=zone.ShortUrl %>" class="font-italic color-0072ff" title="<%=zone.Name %>">Xem tất cả</a>
                    </div>--%>
                </section>
                <section class="list row">
                    <% foreach (var tour in tours)
                        { %>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour hot">
                            <div class="image">
                                <a href="/<%=currentLanguage %>/tour/<%=tour.Url %>.<%=tour.Id %>.htm" title="">
                                    <img src="/Uploads/<%=tour.Avatar %>?v=1.0" alt="<%=tour.Title %>" /></a>
                                <div class="flag text">
                                    <div class="format-location" data-id="<%=tour.ListZoneId %>">n/a</div>

                                </div>
                                <%--<div class="text">Vietjet</div>--%>
                                <%-- <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>--%>
                            </div>
                            <div class="content px-3 ">
                                <h2 class="tit">
                                    <a href="/<%=currentLanguage %>/tour/<%=tour.Url %>.<%=tour.Id %>.htm" title="<%=tour.Title %>"><%=tour.Title %></a>
                                </h2>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class=""><%=tour.SubTitle %></span>
                                    </div>
                                    <% if (tour.MinPrice > 0)
                                       { %>
                                        <div class="cost ml-auto"><%= UIHelper.FormatCurrency(currentUnit, tour.MinPrice, true) %></div>
                                    <% }
                                       else
                                       { %>
                                        <div class="ml-auto "><%=priceLable %>: <label class="text-danger"><%=priceContact  %></label></div>
                                    <% } %>
                                </div>
                            </div>

                        </div>
                    </div>
                    <% } %>
                </section>
            </div>
        </div>
        <%    
            } %>
    </section>
    <section class="suggest py-5">
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
</asp:Content>
