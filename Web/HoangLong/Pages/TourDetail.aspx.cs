﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMTravel.Core.Helper;
using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;
using Mi.BO.Base.News;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Tag;
using Resources;

namespace CMTravel.Pages
{
    public partial class TourDetail : BasePages
    {
        public NewsEntity obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var newsId = Utility.ConvertToLong(Page.RouteData.Values["id"]);
                if (newsId > 0)
                {
                    obj = NewsBo.GetNewsDetailById(newsId, 18);
                    Page.Title = !string.IsNullOrEmpty(obj.TitleSeo) ? obj.TitleSeo : obj.Title;
                    Page.MetaKeywords = obj.MetaKeyword;
                    Page.MetaDescription = obj.MetaDescription;
                }
                else
                {
                    obj = new NewsEntity();

                }
            }
        }
        protected string GetTags(long newsId)
        {
            var tags = CacheObjectBase.GetInstance<TagCached>().GetTagNewsByNewsId(newsId);
            string _strTag = "";

            foreach (var tag in tags)
            {
                if (tag.TagMode == (int)EnumTagType.ForNews)
                {
                    _strTag += "<a title=\"" + tag.Name + "\" href=\"/tag/" + tag.Url + ".htm\"><span class=\"arrow\"></span>" +
                               tag.Name + "</a>";

                }
            }
            return _strTag;
        }
        protected string GetTourStatus(int status)
        {
         
            string _strStatus = "";

            switch (status)
            {
                case 1:
                    _strStatus = Language.Available;
                    break;
                case 2:
                    _strStatus = Language.BookedUp;
                    break;
                case 3:
                    _strStatus = Language.Contact;
                    break;
                default:
                    _strStatus = "n/a";
                    break;
            }
            return _strStatus;
        }
        public class AvatarArray
        {
            public int num { get; set; }
            public string url { get; set; }
        }
    }
}