﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mi.Action;
using Mi.Action.Core;
using Mi.BO.Base.Customer;
using Mi.BO.Base.News;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.Customer;
using Mi.Entity.Base.Zone;
using Mi.Entity.ErrorCode;
using CMTravel.CMS.Modules.Service.Action;
using CMTravel.Core.Helper;
using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;
using Resources;

namespace CMTravel.Pages.Action
{
    public class UIActions : ActionBase
    {
        protected override string ResponseContentType
        {
            get { return "text/plain; charset=utf-8"; }
        }
        protected override bool IsResponseDataDirectly
        {
            get { return false; }
        }

        protected override object ProcessAction(string functionName, HttpContext context)
        {
            var responseData = new ResponseData();
            switch (functionName)
            {

                case "save":
                    responseData = AddCustomer();
                    break;
                case "faq-detail":
                    responseData = FAQDetail();
                    break;
                case "get-more-tour":
                    responseData = GetMoreTour();
                    break;
                case "filter-tour":
                    responseData = FilterTour();
                    break;
                case "send_mail":
                    responseData = SendMail();
                    break;
                case "load-trang":
                    responseData = LoadTrang();
                    break;
                case "load-chu-cai":
                    responseData = LoadChuCai();
                    break;
                case "chart-customer":
                    responseData = ChartCustomer();
                    break;
                case "chart-co-so":
                    responseData = ChartCoSo();
                    break;
                case "chart-theo-tuoi":
                    responseData = ChartTheoTuoi();
                    break;
            }

            return responseData;
        }
        private ResponseData SendMail()
        {
            var responseData = new ResponseData();
            string name = GetQueryString.GetPost("name", string.Empty);
            string email = GetQueryString.GetPost("email", string.Empty);
            string phoneNumber = GetQueryString.GetPost("phoneNumber", string.Empty);
            string address = GetQueryString.GetPost("address", string.Empty);
            string note = GetQueryString.GetPost("note", string.Empty);
            string type = GetQueryString.GetPost("type", string.Empty);
            var ngaySinh = GetQueryString.GetPost("ngaySinh", DateTime.MinValue);
            var ngayKham = GetQueryString.GetPost("ngayKham", DateTime.MaxValue);
            var location = GetQueryString.GetPost("location", string.Empty);
            var coSo = GetQueryString.GetPost("coSo", 0);
            if (name.Length <= 0)
            {
                responseData.Success = false;
                return responseData;
            }
            //if (email.Length <= 0)
            //{
            //    responseData.Success = false;
            //    return responseData;
            //}

            try
            {
                //Lay ten co so
                var coSo_ten = ZoneBo.GetZoneWithLanguageByType(18, "vi-VN").Where(r => r.Id == coSo).SingleOrDefault();
                var kind = "";
                switch (type) {
                    case "lien-he":
                        kind = "Liên hệ";
                        break;
                    case "dat-lich":
                        kind = "Đặt lịch khám";
                        break;
                }
                string content = "<h3>Thông tin khách hàng " +kind+ "</h3><br/>";
                content += "<div class=\"root\"><div class=\"\" style=\"border-bottom: 1px solid #d3d3d3\">" +
                           //Ve lai
                           "<strong>Họ tên: </strong> " + name + "<br/>" +
                           "<strong>Ngày sinh: </strong> " + ngaySinh.ToString("dd/MM/yyyy") + "<br/>" +
                           "<strong>Điện thoại: </strong> " + phoneNumber + "<br/>" +
                           "<strong>Địa chỉ: </strong> " + address + "<br/>" +
                           "<strong>Tỉnh/ Thành phố: </strong> " + location + "<br/>" +
                           "<strong>Email: </strong> " + email + "<br/>" +
                           "<strong>Ngày khám: </strong> " + ngayKham.ToString("dd/MM/yyyy") + "<br/>" +
                           "<strong>Cơ sở khám: </strong> " + coSo_ten.lang_name + "<br/>" +
                           "</div>";



                string ReceiverEmail = "";
                //var config = CacheObjectBase.GetInstance<ConfigCached>().GetByConfigName("EmailReceived", Current.LanguageJavaCode);
                var config = CacheObjectBase.GetInstance<ConfigCached>().GetByConfigName("EmailReceived", "vi-VN");
                //var config = ConfigBo.GetListConfigLanguageByConfigName("EmailReceived", "vi-VN");
                if (config != null)
                {
                    ReceiverEmail = config.ConfigValue;
                }
                var mail_arr = ReceiverEmail.Split(',');
                foreach (var item in mail_arr) {
                    var isSent = EmailHelper.SendEmail("Thông tin khách hàng", "noreply.hoanglongclinic@gmail.com", item, "Thông tin khách hàng" + ". ", content, true);
                }
                //var isSent2 = EmailHelper.SendEmail("ChangMyTravel", "noreply.changmytravel@gmail.com", ReceiverEmail, "[ChangMyTravel] Thông báo có đơn hàng mới", content2, true);
                responseData.Success = true;
            }
            catch (Exception ex)
            {
                throw ex;

            }
            return responseData;
        }
        private ResponseData FAQDetail()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\Pages\\FAQDetail.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData LoadTrang()
        {
            /*
             type : zone_type,
                    lang : lang,
                    hot : hot,
                    zone_id: zone_id,
                    search: search,
                    pageIndex: pageIndex,
                    pageSize: pageSize
             */

            var responseData = new ResponseData();
            var type = GetQueryString.GetPost("type", 0);
            var lang = GetQueryString.GetPost("lang", string.Empty);
            var hot = GetQueryString.GetPost("hot", 0);
            var zone_id = GetQueryString.GetPost("zone_id", 0);
            var search = GetQueryString.GetPost("search", string.Empty);
            var pageIndex = GetQueryString.GetPost("pageIndex", 0);
            var pageSize = GetQueryString.GetPost("pageSize", 0);
            var isTagSearch = GetQueryString.GetPost("isTagSearch", 0);
            var total_row = 0;
            if(isTagSearch == 0)
                responseData.Data = NewsBo.GetNewsDetailWithLanguage(type, lang, hot, zone_id, search, pageIndex, pageSize, ref total_row);
            if(isTagSearch == 1)
                responseData.Data = NewsBo.GetNewsDetailWithLanguageSameTag(type, lang, hot, zone_id, search, pageIndex, pageSize, ref total_row);
            //var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\Pages\\FAQDetail.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData LoadChuCai()
        {
            /*
             type : zone_type,
                    lang : lang,
                    hot : hot,
                    zone_id: zone_id,
                    search: search,
                    pageIndex: pageIndex,
                    pageSize: pageSize
             */

            var responseData = new ResponseData();
            //var type = GetQueryString.GetPost("type", 0);
            var lang = GetQueryString.GetPost("lang", string.Empty);
            //var hot = GetQueryString.GetPost("hot", 0);
            var zone_id = GetQueryString.GetPost("zone_id", 0);
            var search = GetQueryString.GetPost("search", string.Empty);
            var pageIndex = GetQueryString.GetPost("pageIndex", 0);
            var pageSize = GetQueryString.GetPost("pageSize", 0);
            var total_row = 0;
            responseData.Data = NewsBo.GetNewsWithZoneParentAndLanguage(zone_id, lang, search, pageIndex, pageSize, ref total_row);
            //var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\Pages\\FAQDetail.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData ChartCustomer()
        {
            /*
             type : zone_type,
                    lang : lang,
                    hot : hot,
                    zone_id: zone_id,
                    search: search,
                    pageIndex: pageIndex,
                    pageSize: pageSize
             */

            var responseData = new ResponseData();
            //var type = GetQueryString.GetPost("type", 0);
            var year = GetQueryString.GetPost("year", DateTime.Now.Year);
            responseData.Data = CustomerBo.CustomerByMonthChart(year);
            //var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\Pages\\FAQDetail.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData ChartCoSo()
        {
            var responseData = new ResponseData();
            var start = GetQueryString.GetPost("start", DateTime.MinValue);
            var end = GetQueryString.GetPost("end", DateTime.MinValue);
            responseData.Data = CustomerBo.CustomerByCoSoChart(start,end);
            responseData.Success = true;
            return responseData;
        }
        private ResponseData ChartTheoTuoi()
        {
            var responseData = new ResponseData();
            var start = GetQueryString.GetPost("start", DateTime.MinValue);
            var end = GetQueryString.GetPost("end", DateTime.MinValue);
            responseData.Data = CustomerBo.CustomerByTuoi(start, end);
            responseData.Success = true;
            return responseData;
        }
        private ResponseData GetMoreTour()
        {
            var shortUrl = GetQueryString.GetPost("shortUrl", string.Empty);
            var pageIndex = GetQueryString.GetPost("pageIndex", 2);
            int pageSize = 10;
            var totalRow = 0;
            var result = NewsBo.SearchByShortUrlTourV2(shortUrl, Current.LanguageJavaCode, 18, pageIndex, pageSize, ref totalRow);

            var responseData = ConvertResponseData.CreateResponseData(result, 0, "");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData FilterTour()
        {
            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\Pages\\FilterTour.aspx");
            responseData.Success = true;
            return responseData;
        }

        private ResponseData AddCustomer()
        {

            /*
              var name = $('#txtName').val();
            var phoneNumber = $('#txtPhoneNumber').val();
            var email = $('#txtEmail').val();
            var note = $('#txtNote').val();
            var type = "lien-he";
             */
            var responseData = new ResponseData();
            string name = GetQueryString.GetPost("name", string.Empty);
            string email = GetQueryString.GetPost("email", string.Empty);
            string phoneNumber = GetQueryString.GetPost("phoneNumber", string.Empty);
            string address = GetQueryString.GetPost("address", string.Empty);
            string note = GetQueryString.GetPost("note", string.Empty);
            string type = GetQueryString.GetPost("type", string.Empty);
            var ngaySinh = GetQueryString.GetPost("ngaySinh", DateTime.MinValue);
            var ngayKham = GetQueryString.GetPost("ngayKham", DateTime.MaxValue);
            var location = GetQueryString.GetPost("location", string.Empty);
            var coSo = GetQueryString.GetPost("coSo", 0);
            if (!string.IsNullOrEmpty(name))
            {
                var obj = new CustomerEntity
                {
                    FullName = name,
                    Email = email,
                    Mobile = phoneNumber,
                    Note = note,
                    Type = type,
                    NgaySinh = ngaySinh,
                    NgayKham = ngayKham,
                    Address = address,
                    location = location,
                    CoSoKham = coSo
                };
                int outId = 0;

                responseData = ConvertResponseData.CreateResponseData(CustomerBo.Create(obj, ref outId));

                if (outId > 0)
                //{
                //    string ReceiverEmail = "";
                //    var config = CacheObjectBase.GetInstance<ConfigCached>().GetByConfigName("EmailReceived", Current.LanguageJavaCode);
                //    if (config != null)
                //    {
                //        ReceiverEmail = config.ConfigValue;
                //    }
                   

                //    string content = "Thông tin khách hàng:<br/>";
                //    content += "<b>Tên</b>:" + name + ", <b>Điện thoại</b>:" + phoneNumber + ", <b>Email</b>: " + email + "<br/>, <b>Ghi chú</b>:" + note + "<br/>";
                //    //content += "<br/> UTM:<br/>";
                //    //  content += "Source:" + utm_source + ", Medium:" + utm_medium + ", Campaign:" + utm_campaign + "";
                //    var isSent2 = EmailHelper.SendEmail("ChangMyTravel", "noreply.changmytravel@gmail.com", ReceiverEmail, "[ChangMyTravel] Thông báo có thư mới", content, true);

                //}
                responseData.Success = true;
                responseData.Message = "Thêm mới thành công !";
                responseData.Data = outId;

            }
            else
            {
                responseData.Success = false;
                responseData.Message = "Error";
            }
            return responseData;
        }


    }
}