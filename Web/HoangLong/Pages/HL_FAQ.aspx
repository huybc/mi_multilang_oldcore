﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/HoangLongMaster.Master" AutoEventWireup="true" CodeBehind="HL_FAQ.aspx.cs" Inherits="CMTravel.Pages.HL_FAQ" %>

<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <%//Lay toan bo zone cua thong tin suc khoe a-z %>
    <%var kh_zone = ZoneBo.GetZoneWithLanguageByType(13, lang); %>
    <%var kh_zone_parents = kh_zone.Where(r => r.ParentId ==0).SingleOrDefault(); %>
    <%var a_to_z_zone = kh_zone.Where(r => r.Alias.Equals("thong-tin-suc-khoe-a-z")).SingleOrDefault(); %>
    <%var a_to_z_zone_child = kh_zone.Where(r => r.ParentId == a_to_z_zone.Id).OrderBy(r=>r.SortOrder).ToList();%>
    <div class="container  py-4">

        <div class="row mb-4">
            <div class="col-md-8 col-sm-12 col-12 align-self-md-center">
                <div class="h-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="javascript:void(0)"><%=kh_zone_parents.lang_name %></a></li>
                            <li class="breadcrumb-item active"><%=a_to_z_zone.lang_name %></li>
                        </ol>
                    </nav>

                </div>
            </div>

            <div class="col-md-4 col-sm-12 col-12">
                <div class="form-group form-search-faq mb-0">
                    <input type="text" class="form-control pl-5" placeholder="<%=Language.Search %>">
                    <i class="fas fa-search" aria-hidden="true"></i>
                </div>
            </div>
        </div>

        <div class="mb-4  border-bottom">
            <h4 class="text-uppercase" style="color: #367572;"><%=UIHelper.GetConfigByName("TieuDeAToZ") %></h4>
            <p><%=UIHelper.GetConfigByName("NoiDungAToZ") %></p>
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-12">
                <div class="card card-aphabe">
                    <div class="card-header">
                        <%=UIHelper.GetConfigByName("BangChuCai") %>
                    </div>
                    <div class="card-body p-3">
                        <div class="mobile-search-aphabe d-table text-center " data-zone="<%=a_to_z_zone.Id %>" data-lang="<%=lang %>" data-txttype="<%=UIHelper.GetTypeUrlByHoangLongProject(13) %>" data-shortlang="<%=currentLanguage %>">
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">A</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">B</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">C</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">D</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">E</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">F</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">G</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">H</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">I</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">J</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">K</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">L</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">M</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">N</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">O</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">P</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">Q</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">R</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">S</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">T</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">U</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">V</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">W</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">X</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">Y</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">Z</a>
                            <a href="javascript:void(0)" class="btn btn-default chu-cai-dau">#</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-lg-5 col-md-6 col-sm-8 col-12">
                <div class="px-md-3">
                    <h2 style="color: #367572; text-decoration: underline; margin-bottom: 1rem;">
                        <a href="javascript:void(0)"><span class="choose-letter">A</span>
                        </a>
                    </h2>
                    <ul class="list-ques">
                        <%//Lay toan bo bai viet chu A %>
                        <%var totalR = 0; %>
                        <%var list_news_a = NewsBo.GetNewsWithZoneParentAndLanguage(a_to_z_zone.Id, lang, "A", 1, 9, ref totalR); %>
                        <%foreach (var item in list_news_a)
                            { %>
                        <li>
                            <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(13) %>/<%=item.Alias %>/<%=item.Url %>.<%=item.NewsId %>.htm"><%=item.Title %>
                            </a>
                        </li>
                        <%} %>
                    </ul>
                </div>
            </div>
            <%var banner_quang_cao = ConfigBo.AdvGetByType(9, lang).FirstOrDefault(); %>
            <div class="col-xl-4 col-lg-4 col-md-2 col-sm-12">
                <div class="qc-left">
                    <a href="<%=banner_quang_cao.Url %>">
                        <img src="/uploads/<%=banner_quang_cao.Thumb %>" class="img-fluid" alt="<%=domainName %>" /></a>
                </div>
            </div>
        </div>
        <div class="my-4 border-bottom">
        </div>

        <div class="row">

            <%foreach (var item in a_to_z_zone_child)
                { %>
            <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="item-list-faqs">
                    <div class="topic">
                        <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(13) %>/<%=item.Alias %>" title=""><%=item.Name %></a>
                    </div>
                    <%//Lay danh sach bai viet thuoc zone tren %>
                    <%var t = 0; %>
                    <%var list_news = NewsBo.GetNewsDetailWithLanguage(13, lang, 2, item.Id, "", 1, 4, ref t); %>


                    <ul class="list">
                        <%foreach (var i in list_news)
                            { %>
                        <li>
                            <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(13) %>/<%=i.Alias %>/<%=i.Url %>.<%=i.NewsId %>.htm" title=""><%=i.Title %></a>
                        </li>
                        <%} %>
                    </ul>
                    <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(13) %>/<%=item.Alias %>" class=" btn btn-view-more btn-sm" title=""><%=Language.More %></a>
                </div>
            </div>
            <%} %>

            <%--<div class="col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="item-list-faqs">
                    <div class="topic">
                        <a href="" title="">Box Types and Materials</a>
                    </div>
                    <ul class="list">
                        <li>
                            <a href="" title="">Can I get a sample box?</a>
                        </li>
                        <li>
                            <a href="" title="">What's the difference between Kraft, Standard White, and
                                Dreamcoat™?</a>
                        </li>
                        <li>
                            <a href="" title="">Can you print boxes with a satin-like finish? </a>
                        </li>
                        <li>
                            <a href="" title="">Can you print foils, metallics, or white inks? </a>
                        </li>
                        <li>
                            <a href="" title="">Are there print quality differences between your Kraft and
                                White material
                                options?</a>
                        </li>
                    </ul>
                    <a href="" class=" btn btn-view-more btn-sm">Xem thêm</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="item-list-faqs">
                    <div class="topic">
                        <a href="" title="">Box Types and Materials</a>
                    </div>
                    <ul class="list">
                        <li>
                            <a href="" title="">Can I get a sample box?</a>
                        </li>
                        <li>
                            <a href="" title="">What's the difference between Kraft, Standard White, and
                                Dreamcoat™?</a>
                        </li>
                        <li>
                            <a href="" title="">Can you print boxes with a satin-like finish? </a>
                        </li>
                        <li>
                            <a href="" title="">Can you print foils, metallics, or white inks? </a>
                        </li>
                        <li>
                            <a href="" title="">Are there print quality differences between your Kraft and
                                White material
                                options?</a>
                        </li>
                    </ul>
                    <a href="" class=" btn btn-view-more btn-sm">Xem thêm</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="item-list-faqs">
                    <div class="topic">
                        <a href="" title="">Box Types and Materials</a>
                    </div>
                    <ul class="list">
                        <li>
                            <a href="" title="">Can I get a sample box?</a>
                        </li>
                        <li>
                            <a href="" title="">What's the difference between Kraft, Standard White, and
                                Dreamcoat™?</a>
                        </li>
                        <li>
                            <a href="" title="">Can you print boxes with a satin-like finish? </a>
                        </li>
                        <li>
                            <a href="" title="">Can you print foils, metallics, or white inks? </a>
                        </li>
                        <li>
                            <a href="" title="">Are there print quality differences between your Kraft and
                                White material
                                options?</a>
                        </li>
                    </ul>
                    <a href="" class=" btn btn-view-more btn-sm">Xem thêm</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="item-list-faqs">
                    <div class="topic">
                        <a href="" title="">Box Types and Materials</a>
                    </div>
                    <ul class="list">
                        <li>
                            <a href="" title="">Can I get a sample box?</a>
                        </li>
                        <li>
                            <a href="" title="">What's the difference between Kraft, Standard White, and
                                Dreamcoat™?</a>
                        </li>
                        <li>
                            <a href="" title="">Can you print boxes with a satin-like finish? </a>
                        </li>
                        <li>
                            <a href="" title="">Can you print foils, metallics, or white inks? </a>
                        </li>
                        <li>
                            <a href="" title="">Are there print quality differences between your Kraft and
                                White material
                                options?</a>
                        </li>
                    </ul>
                    <a href="" class=" btn btn-view-more btn-sm">Xem thêm</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="item-list-faqs">
                    <div class="topic">
                        <a href="" title="">Box Types and Materials</a>
                    </div>
                    <ul class="list">
                        <li>
                            <a href="" title="">Can I get a sample box?</a>
                        </li>
                        <li>
                            <a href="" title="">What's the difference between Kraft, Standard White, and
                                Dreamcoat™?</a>
                        </li>
                        <li>
                            <a href="" title="">Can you print boxes with a satin-like finish? </a>
                        </li>
                        <li>
                            <a href="" title="">Can you print foils, metallics, or white inks? </a>
                        </li>
                        <li>
                            <a href="" title="">Are there print quality differences between your Kraft and
                                White material
                                options?</a>
                        </li>
                    </ul>
                    <a href="" class=" btn btn-view-more btn-sm">Xem thêm</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="item-list-faqs">
                    <div class="topic">
                        <a href="" title="">Box Types and Materials</a>
                    </div>
                    <ul class="list">
                        <li>
                            <a href="" title="">Can I get a sample box?</a>
                        </li>
                        <li>
                            <a href="" title="">What's the difference between Kraft, Standard White, and
                                Dreamcoat™?</a>
                        </li>
                        <li>
                            <a href="" title="">Can you print boxes with a satin-like finish? </a>
                        </li>
                        <li>
                            <a href="" title="">Can you print foils, metallics, or white inks? </a>
                        </li>
                        <li>
                            <a href="" title="">Are there print quality differences between your Kraft and
                                White material
                                options?</a>
                        </li>
                    </ul>
                    <a href="" class=" btn btn-view-more btn-sm">Xem thêm</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="item-list-faqs">
                    <div class="topic">
                        <a href="" title="">Box Types and Materials</a>
                    </div>
                    <ul class="list">
                        <li>
                            <a href="" title="">Can I get a sample box?</a>
                        </li>
                        <li>
                            <a href="" title="">What's the difference between Kraft, Standard White, and
                                Dreamcoat™?</a>
                        </li>
                        <li>
                            <a href="" title="">Can you print boxes with a satin-like finish? </a>
                        </li>
                        <li>
                            <a href="" title="">Can you print foils, metallics, or white inks? </a>
                        </li>
                        <li>
                            <a href="" title="">Are there print quality differences between your Kraft and
                                White material
                                options?</a>
                        </li>
                    </ul>
                    <a href="" class=" btn btn-view-more btn-sm">Xem thêm</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="item-list-faqs">
                    <div class="topic">
                        <a href="" title="">Box Types and Materials</a>
                    </div>
                    <ul class="list">
                        <li>
                            <a href="" title="">Can I get a sample box?</a>
                        </li>
                        <li>
                            <a href="" title="">What's the difference between Kraft, Standard White, and
                                Dreamcoat™?</a>
                        </li>
                        <li>
                            <a href="" title="">Can you print boxes with a satin-like finish? </a>
                        </li>
                        <li>
                            <a href="" title="">Can you print foils, metallics, or white inks? </a>
                        </li>
                        <li>
                            <a href="" title="">Are there print quality differences between your Kraft and
                                White material
                                options?</a>
                        </li>
                    </ul>
                    <a href="" class=" btn btn-view-more btn-sm">Xem thêm</a>
                </div>
            </div>--%>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
    <script>
        $('.chu-cai-dau').off('click').on('click', function () {
            var search = $(this).text();
            var lang = $(this).parent().data('lang');
            var zone_id = $(this).parent().data('zone');
            var pageIndex = 1;
            var pageSize = 9;
            var txtType = $(this).parent().data('txttype');
            var sLang = $(this).parent().data('shortlang');
            //Lay du lieu tu database
            R.Post({
                params: {
                    lang: lang,
                    zone_id: zone_id,
                    search: search,
                    pageIndex: pageIndex,
                    pageSize: pageSize
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "load-chu-cai",
                success: function (res) {
                    if (res.Success) {
                        console.log(res.Data);
                        var result = res.Data
                        $('.choose-letter').text(search);
                        var htm = '';
                        $('.list-ques').html('')
                        for (var o = 0; o < result.length; o++) {
                            var link = '/' + sLang + '/' + txtType + '/' + result[o].Alias + '/' + result[o].Url + '.' + result[o].NewsId + '.htm';
                            htm += '<li>';
                            htm += '<a href="' + link + '">' + result[o].Title + '</a>'
                            htm += '</li>';
                        }
                        $('.list-ques').html(htm);
                    }
                    // $('.card-header').RLoadingModuleComplete();
                }
            });
        })
    </script>
</asp:Content>
