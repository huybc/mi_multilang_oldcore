﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/HoangLongMaster.Master" AutoEventWireup="true" CodeBehind="HL_BaiVietDetail.aspx.cs" Inherits="CMTravel.Pages.HL_BaiVietDetail" %>

<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        var s_id = Page.RouteData.Values["id"].ToString();
        var id = int.Parse(s_id);
        var newsDetail = NewsBo.GetNewsDetailWithLanguageById(id, lang);
        var delete_html = @"<[^>]*>";
        //var sapo = obj.Sapo.Replace(delete_html, "");
        var sapo = Regex.Replace(obj.Sapo, delete_html, "");
        %>
    <style>
        .content img {
            max-width: 100%;
            height: auto;
        }
        .content li{
            list-style-type:disc;
            list-style-position:inside;
        }
    </style>
    <meta property="fb:app_id" content="1426300910760607" />
    <meta property="og:url" content="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" />
    <meta property="og:type" content="article" />
    <%if (obj != null)
      { %>
        <meta property="og:title" content="<%= obj.Title %>" />
        <meta property="og:description" content="<%=sapo%>" />
        <meta property="og:image" content="<%=domainName %>/uploads/<%= obj.Avatar %>" />
    <%} %>
    <link rel="canonical" href="<%=newsDetail.Title %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%
        var delete_html = @"<[^>]*>";
        //var sapo = obj.Sapo.Replace(delete_html, "");
        var sapo = Regex.Replace(obj.Sapo, delete_html, "");
        %>
    <%//Lay ra cac thong tin can thiet %>
    <%
        //var currentLanguage = Page.RouteData.Values["languageCode"].ToString();
        //var lang = "";
        //switch (currentLanguage)
        //{
        //    case "vi":
        //        lang = "vi-VN";
        //        break;
        //    case "en":
        //        lang = "en-US";
        //        break;
        //}
        
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
   
        var s_id = Page.RouteData.Values["id"].ToString();
        var id = int.Parse(s_id);
        var zone_ali = Page.RouteData.Values["zone"].ToString();
    %>
    <%var newsDetail = NewsBo.GetNewsDetailWithLanguageById(id, lang); %>
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <script type="application/ld+json">
				            {  
                                    "@context":"http://schema.org",
                                    "@type":"Article",
                                    "mainEntityOfPage":{  
                                        "@type":"WebPage",
                                        "@id":"<%=HttpContext.Current.Request.Url.AbsoluteUri %>"
                                    },
                                   "headline":"<%=!string.IsNullOrEmpty(obj.MetaTitle)?obj.MetaTitle:obj.Title%>",
                                    "image":{  
                                        "@type":"ImageObject",
                                        "url":"<%=domainName+"/uploads/"+obj.Avatar %>",
                                        "height":600,
                                        "width":800
                                    },
                                    "datePublished":"<%=obj.CreatedDate %>",
                                    "dateModified":"<%=obj.CreatedDate %>",
                                    "author":{  
                                        "@type":"Person",
                                        "name":"rbland"
                                    },
                                    "publisher":{  
                                        "@type":"Organization",
                                        "name":"rbland.vn",
                                        "logo":{  
                                            "@type":"ImageObject",
                                            "url":"<%=domainName+"/uploads/"+UIHelper.GetConfigByName("LogoTop")%>",
                                            "width":35,
                                            "height":34
                                        }
                                    },
                                    "description":"<%=sapo %>"
                            }
    </script>
    <%//Lay danh sach zone thuoc type kia va parent Id > 0 %>

    <%var zone_list = ZoneBo.GetZoneWithLanguageByType(newsDetail.Type, lang).ToList(); %>
    <%var zone_parent = zone_list.Where(r => r.ParentId == 0).SingleOrDefault(); %>
    <%var zone_list_by_news = zone_list.Where(r => r.ParentId == zone_parent.Id).ToList(); %>
    <%//Cai nay de danh cho may thang a-z %>
    <%var sitemap_a_z = ""; %>
    <%var a_z_id = 0; %>
    <%var zone_a_z = zone_list_by_news.Where(r => r.Alias.Equals("thong-tin-suc-khoe-a-z")).SingleOrDefault(); %>
    <%if (zone_a_z != null) { %> 
        <% a_z_id = zone_a_z.Id; %>
    <%} %>
    
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4 col-12">
                    <div class="menu-left">
                        <%foreach (var item in zone_list_by_news)
                            { %>
                        <%if(item.Alias.Equals("thong-tin-suc-khoe-a-z")){ %> 
                            <div class="item">
                            <a href="/<%=currentLanguage %>/<%=item.Alias %>" title=""><%=item.lang_name %></a>
                        </div>
                        <%} else{ %> 
                            <div class="item">
                            <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(zone_parent.Type) %>/<%=item.Alias %>" title=""><%=item.lang_name %></a>
                        </div>
                        <%} %>
                        <%} %>
                    </div>
                    <%var banner_quang_cao = ConfigBo.AdvGetByType(9, lang).ToList(); %>
                    <%foreach (var item in banner_quang_cao)
                        { %> 
                        <div class="qc-left">
                        <a href="<%=item.Url %>">
                            <img src="/uploads/<%=item.Thumb %>" class="img-fluid" alt="<%=domainName %>" /></a>
                    </div>
                    <%} %>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-hoanglong-2">
                            <li class="breadcrumb-item " aria-current="page"><%=zone_parent.lang_name %></li>
                            <li class="breadcrumb-item active" aria-current="page"><%=zone_list_by_news.Where(r => r.Id == newsDetail.ZoneId).SingleOrDefault()!=null?zone_list_by_news.Where(r => r.Id == newsDetail.ZoneId).SingleOrDefault().lang_name:(zone_ali.Length==1?zone_a_z.lang_name:"") %></li>
                        </ol>
                    </nav>
                    <div class="news-detail">
                        <div class="row">
                            <div class="col-md-8 col-sm-12 col-12">
                                <div class="d-md-flex">
                                    <%var count_char_title = newsDetail.Title.Length; %>
                                    <%--<%var add_style = "font-size:20px"; %>--%>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left" style="padding-left:0px">
                                        <h1 class="title" style="<%=count_char_title > 20 ? "font-size:20px": "" %>; text-transform:none;">
                                            <%=newsDetail.Title %>
                                        </h1>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="group-btn-top ml-auto">
                                            <a href="/<%=currentLanguage %>/lien-he" title="" class="btn btn-red mr-2"><i class="fas fa-phone-volume mr-2"></i><%=Language.Contact %></a>
                                            <a href="/<%=currentLanguage %>/dat-lich-kham" title="" class="btn btn-red"><i class="far fa-calendar-alt mr-2"></i><%=Language.Register %></a>
                                        </div>
                                    </div>

                                </div>
                                <%--<div class="time mt-2 mb-4">
                                    <%=newsDetail.CreatedDate.ToString("dd/MM/yyyy") %>
                                </div>--%>
                                <div class="content mb-4 ">
                                    <%=newsDetail.Body %>
                                </div>
                                <div class="facebook-like-share">
                                    <div class="fb-like" data-href="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="" data-layout="standard" data-action="like" data-size="small" data-share="true"></div>
                                    <div class="fb-comments" data-href="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="" data-numposts="10"></div>
                                </div>
                                <br />
                                <div class="news-more-bt mb-4">
                                    <div class="text-uppercase font-weight-bold mb-2">
                                        <%=Language.More %>
                                    </div>
                                    <ul class="list pl-4">
                                        <%//Lay ra danh sach 5 bai viet co cung chuyen muc moi nhat %>
                                        <%var more_row = 0; %>
                                        <%var list_more_news = NewsBo.GetNewsDetailWithLanguage(newsDetail.Type, lang, 2, newsDetail.ZoneId, string.Empty, 1, 5, ref more_row); %>
                                        <%foreach (var item in list_more_news)
                                            { %>
                                        <li><a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(item.Type).Equals(string.Empty)?item.Alias:UIHelper.GetTypeUrlByHoangLongProject(item.Type) %>/<%=item.Alias %>/<%=item.Url %>.<%=item.NewsId %>.htm" title=""><%=item.Title %></a></li>
                                        <%} %>
                                    </ul>
                                </div>
                                <div class="list-tags mb-4">
                                    <label class="d-inline-block mr-3"><%=Language.Tag %></label>
                                    <%var tags = newsDetail.Tags.Split(','); %>
                                    <%foreach (var item in tags)
                                        { %>
                                    <div class="item">
                                        <a href="/<%=currentLanguage %>/tim-kiem-tag?search=<%=item.TrimStart() %>"><%=item.TrimStart() %></a>
                                        <%--<%=item %>--%>
                                    </div>
                                    <%} %>
                                </div>

                            </div>
                            <div class="col-md-4 col-sm-12 col-12 px-md-0 mt-md-5">

                                <div class="menu-right">
                                    <div class="heading">
                                        <%=Language.Interested %>
                                    </div>
                                    <%//Lay list 6 bai viet moi nhat %>
                                    <%var list_10_newest_news = NewsBo.GetNewsDetailWithLanguage(14, lang, 2, newsDetail.ZoneId, string.Empty, 1, 10, ref more_row); %>
                                    <div class="list-link">
                                        <%foreach (var item in list_10_newest_news)
                                            { %>
                                        <div class="item-link" >
                                            <p class=""><a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(item.Type) %>/<%=item.Alias %>/<%=item.Url %>.<%=item.NewsId %>.htm" title=""><%=item.Title %></a></p>
                                            <%--<p class="des"><%=item.Sapo %></p>--%>
                                        </div>
                                        <%} %>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
</asp:Content>
