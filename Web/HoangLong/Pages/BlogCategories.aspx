﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/CMTravel.Master" AutoEventWireup="true" CodeBehind="BlogCategories.aspx.cs" Inherits="CMTravel.Pages.BlogCategories" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Register Src="~/Pages/Controls/Sub/UIPager.ascx" TagPrefix="uc1" TagName="UIPager" %>
<%@ Register Src="~/Pages/Controls/TourStick.ascx" TagPrefix="uc1" TagName="TourStick" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <% var currentLanguageJavaCode = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;%>
    <uc1:TourStick runat="server" ID="TourStick" />
    <section class="">

        <div class="container">
            <div class="ss-name">
                <div class="left">
                    <h3 class="tit">
                        <%=zoneObj!=null?zoneObj.Name:"" %></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="blog-ss-1">
                        <div class="slide-blog-cate swiper-container">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <!-- Slides -->
                                <%
                                    var newsTop5 = news.Take(3).ToList();

                                    for (int i = 0; i < newsTop5.Count(); i++)
                                    {
                                %>
                                <div class="swiper-slide">
                                    <div class="item mb-3">
                                        <a href="/<%=currentLanguage %>/blog/<%=newsTop5[i].Url %>.<%=newsTop5[i].Id %>.htm" title="<%=newsTop5[i].Title %>">
                                            <img style="display: inline-block; width: auto; height: 450px" src="/Uploads/<%=newsTop5[i].Avatar %>" alt="<%=newsTop5[i].Title %>" /></a><%--Uploads/<%=news[i].Avatar %--%>
                                        <h2 class="title">
                                            <a href="/<%=currentLanguage %>/blog/<%=newsTop5[i].Url %>.<%=newsTop5[i].Id %>.htm" title=""><%=newsTop5[i].Title %></a>

                                        </h2>
                                    </div>
                                </div>

                                <%} %>
                            </div>

                            <!-- If we need navigation buttons -->
                            <div class="swiper-button-prev"><i class="fas fa-chevron-left"></i></div>
                            <div class="swiper-button-next"><i class="fas fa-chevron-right"></i></div>
                        </div>

                    </div>
                    <div class="list-news">

                        <%
                            var newsSkip5 = news.Skip(3).ToList();
                            foreach (var item in newsSkip5)
                            { %>
                        <div class="item py-4 row">
                            <div class="image align-self-center col-lg-4 col-md-5 col-sm-12">
                                <a title="/<%=currentLanguage %>/blog/<%=item.Url %>.<%=item.Id %>.htm" href="<%=item.Url%>">
                                    <img src="/Uploads/thumb/<%=item.Avatar %>" class="img-fluid">
                                </a>
                            </div>
                            <div class="content col-lg-8 col-md-7 col-sm-12">
                                <h3 class="title"><a title="" href="/<%=currentLanguage %>/blog/<%=item.Url %>.<%=item.Id %>.htm"><%=item.Title %></a></h3>
                                <div class="time">
                                    <i class="far fa-clock mr-2"></i><%=UIHelper.GetOnlyCurentDate(item.CreatedDate,currentLanguageJavaCode) %>
                                </div>
                                <div>
                                    <%= Utility.SubWordInString(item.Sapo.GetText(),40) %>
                                </div>
                            </div>
                        </div>
                        <%} %>
                        <nav class="my-5">
                            <ul class="pagination justify-content-center">
                                <uc1:UIPager runat="server" ID="UIPager" PageSize="20" />
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div class="blog-ss-right ">
                        <div class="heading ">
                            <div>Tin đọc nhiều</div>
                        </div>
                        <%var topView = NewsBo.GetMostedViewNews(Current.LanguageJavaCode);
                            for (int i = 0; i < topView.Count(); i++)
                            {%>

                        <%  

                            if (i == 0)
                            { %>

                        <div class="item-left mb-3">
                            <div class="image">
                                <a href="/<%=currentLanguage %>/blog/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="">
                                    <img src="/Uploads/<%=topView[i].Avatar %>" alt="" /></a>
                            </div>
                            <h2 class="title">
                                <a href="/<%=currentLanguage %>/blog/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title=""><%=topView[i].Title %></a>
                            </h2>
                            <div class="detail">
                                <%=Utility.SubWordInString(topView[i].Sapo.GetText(),40) %>
                            </div>

                        </div>
                        <%} %>
                        <%if (i > 0)
                            { %>
                        <div class="item-right d-flex">
                            <div class="image">
                                <a href="/<%=currentLanguage %>/blog/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="">
                                    <img src="/Uploads/thumb/<%=topView[i].Avatar %>" alt="" /></a>
                            </div>
                            <div class="text">
                                <h2 class="title">
                                    <a href="/<%=currentLanguage %>/blog/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title=""><%=topView[i].Title %></a>
                                </h2>
                                <div class="time">
                                    <%=UIHelper.GetOnlyCurentDate(topView[i].CreatedDate,currentLanguageJavaCode) %>
                                </div>
                            </div>

                        </div>
                        <%} %>
                        <%} %>
                    </div>
                    <div class="blog-ss-right ">
                        <div class="heading ">
                            Video
                        </div>
                        <%var mediaList = NewsBo.GetTop4VideoNews(currentLanguageJavaCode);
                            foreach (var item in mediaList)
                            {

                        %>
                        <% string data = item.Body;
                            string reg = "\"(h.*?)\"";
                            data = Regex.Matches(data, reg)[0].ToString().Replace("\"", "").Replace("\"", "").Replace(@"watch?v=", @"embed\");
                        %>
                        <iframe width="100%" height="100%" class="mb-3" src="<%=data %>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
