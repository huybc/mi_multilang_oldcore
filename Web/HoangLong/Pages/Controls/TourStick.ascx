﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TourStick.ascx.cs" Inherits="CMTravel.Pages.Controls.TourStick" %>
<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Resources" %>
<section class="trendding trendding-tuor-detail ">
    <div class="container ">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breaking-news-ticker border-new bn-effect-typography bn-direction-ltr" id="newsTicker5">
                    <div class="bn-label  "><%=Language.HotTours %></div>
                    <div class="bn-news">
                        <%
                            int totalRows1 = 0;
                            //var tours = NewsBo.SearchByShortUrl(zone.ShortUrl, 1, 10, ref totalRows);
                            var hotTours = NewsBo.GetHotTours(18, Current.LanguageJavaCode, 1, 1, 6, ref totalRows1);
                        %>
                        <ul>
                            <% foreach (var tour in hotTours)
                               { %>
                            <li> <a href="/<%=Current.Language %>/tour/<%=tour.Url %>.<%=tour.Id %>.htm" title="<%=tour.Title %>"><%=tour.Title %></a> - <%= UIHelper.FormatCurrency(Current.UnitMoney, tour.MinPrice,true) %></li>
                            <% } %>
                        </ul>
                    </div>
                    <div class="bn-controls">
                        <button><span class="bn-arrow bn-prev"></span></button>
                        <button><span class="bn-arrow bn-next"></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>