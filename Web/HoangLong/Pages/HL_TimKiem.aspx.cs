﻿using CMTravel.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMTravel.Pages
{
    public partial class HL_TimKiem : BasePages
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Page.Title = "Tìm kiếm";
                Page.MetaDescription = UIHelper.GetConfigByName("MetaDescription");
                Page.MetaKeywords = UIHelper.GetConfigByName("MetaKeyword");
            }
        }
    }
}