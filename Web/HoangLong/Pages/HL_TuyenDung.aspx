﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/HoangLongMaster.Master" AutoEventWireup="true" CodeBehind="HL_TuyenDung.aspx.cs" Inherits="CMTravel.Pages.HL_TuyenDung" %>

<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%--<%
        //var currentLanguage = Current.Language;
        var currentLanguage = Page.RouteData.Values["languageCode"].ToString();
        var lang = "";
        switch (currentLanguage)
        {
            case "vi":
                lang = "vi-VN";
                break;
            case "en":
                lang = "en-US";
                break;
        }
    %>--%>
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <%//Lay thong tin can thiet slider %>
    <%var slider = ConfigBo.AdvGetByType(8, lang).ToList(); %>
    <div class="container">
        <div class="bd-example">
            <div id="slide-td" class="carousel slide slide-td" data-ride="carousel">
                <ol class="carousel-indicators">
                    <%for (int i = 0; i < slider.Count(); i++)
                        { %>
                    <li data-target="#slide-td" data-slide-to="<%=i %>" class="<%=i==0?"active":"" %>"></li>
                    <%} %>

                    <%--            <li data-target="#slide-td" data-slide-to="1"></li>
            <li data-target="#slide-td" data-slide-to="2"></li>--%>
                </ol>
                <div class="carousel-inner">
                    <%for (int i = 0; i < slider.Count(); i++)
                        { %>
                    <div class="carousel-item <%=i==0?"active":"" %>">
                        <img src="/uploads/<%=slider[i].Thumb %>" class="d-block w-100" alt="<%=domainName %>">
                        <div class="carousel-caption d-none d-md-block">
                            <h1><%=slider[i].Name %></h1>
                            <p><%=slider[i].Content %></p>
                        </div>
                    </div>
                    <%} %>

                    <%--                <div class="carousel-item">
                    <img src="images/change/slide-td-01.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h1>LOREM IPSUM</h1>
                        <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="images/change/slide-td-01.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h1>LOREM IPSUM</h1>
                        <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged</p>
                    </div>
                </div>--%>
                </div>
                <a class="carousel-control-prev" href="#slide-td" role="button" data-slide="prev">
                    <i class="fas fa-chevron-circle-left" aria-hidden="true" style="color: #fff;"></i>
                </a>
                <a class="carousel-control-next" href="#slide-td" role="button" data-slide="next">
                    <i class="fas fa-chevron-circle-right" aria-hidden="true" style="color: #fff;"></i>
                </a>
            </div>
        </div>
    </div>


    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-md-8 col-sm-12 col-12 ">
                            <h6 class="text-uppercase "><%=UIHelper.GetConfigByName_v1("TieuDeTuyenDung",lang) %>
                            </h6>
                            <hr class="mt-0" />
                            <p class="mb-md-5">
                                <%=UIHelper.GetConfigByName_v1("NoiDungTuyenDung",lang) %>
                            </p>
                            <div class="row">
                                <div class="col-md-5 col-sm-5 col-12 pr-md-4 ">
                                    <h6 class="text-uppercase"><%=Language.Newest_Recruiment %></h6>
                                    <hr class="mt-0" />
                                    <%//Lay cac thong tin can thiet %>
                                    <%var list_danh_muc = ZoneBo.GetZoneWithLanguageByType(19, lang); %>
                                    <%var list_danh_muc_parent = list_danh_muc.Where(r => r.ParentId == 0).SingleOrDefault(); %>
                                    <%var list_danh_muc_child = list_danh_muc.Where(r => r.ParentId > 0).ToList(); %>
                                    <%//Lay danh sach bai viet thuoc cac danh muc tuyen dung  %>
                                    <%var totalR = 0; %>
                                    <%var danh_sach_tin_tuyen_dung = NewsBo.GetNewsDetailWithLanguage(19, lang, 2, 0, string.Empty, 1, 5, ref totalR); %>
                                    <ul class="pl-3" style="color: #73ABA6; list-style-type: disc; list-style-position: inside;">
                                        <%foreach (var item in danh_sach_tin_tuyen_dung)
                                            { %>
                                        <li class="mb-2">
                                            <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(19) %>/<%=item.Alias %>/<%=item.Url %>.<%=item.NewsId %>.htm" title="" class="color-0A1E34"><%=item.Title %></a>
                                        </li>
                                        <%} %>
                                    </ul>
                                    <%--<div class="my-4">
                                        <a href="" class="btn-dat-lich px-3 font-weight-normal text-center" style="font-size: medium;">TÌM KIẾM VỊ TRÍ TUYỂN DỤNG</a>
                                    </div>--%>
                                </div>
                                <div class="col-md-7 col-sm-7 col-12">
                                    <div class="row">
                                        <%foreach (var item in list_danh_muc_child)
                                            { %>
                                        <div class="col-sm-6 col-12 pl-md-0">
                                            <h6 class="text-uppercase "><%=item.Name %>
                                            </h6>
                                            <hr class="mt-0" />
                                            <div class="border-radius-10">
                                                <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(19) %>/<%=item.Alias %>" title="">
                                                    <img src="/uploads/<%=item.Avatar %>" class="img-fluid" alt="<%=item.lang_name %>" /></a>
                                            </div>
                                        </div>
                                        <%} %>
                                    </div>
                                </div>


                            </div>




                        </div>
                        <div class="col-md-4 col-sm-12 col-12  ">
                            <div class="menu-right">
                                <h6 class="h6 text-uppercase"><%=UIHelper.MultiLanguageManual(lang,"Tin mới nhất","Newest News") %>
                                </h6>
                                <%//Lay list 3 bai viet moi nhat %>
                                <%var totalR_news = 0; %>
                                <%var list_3_news = NewsBo.GetNewsDetailWithLanguage(14, lang, 2, 0, string.Empty, 1, 3, ref totalR_news); %>
                                <div class="list-link">
                                    <%foreach (var item in list_3_news)
                                    { %>
                                    <div class="item-link">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-4 pr-2">
                                                <div class="image">
                                                    <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(19) %>/<%=item.Alias %>/<%=item.Url %>.<%=item.NewsId %>.htm" title="">
                                                        <img src="/uploads/thumb/<%=item.Avatar %>" class="img-fluid" alt="<%=item.Title %>" /></a>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-8 pl-0">
                                                <h6 class="title">
                                                    <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(19) %>/<%=item.Alias %>/<%=item.Url %>.<%=item.NewsId %>.htm" title=""><%=item.Title %></a>
                                                </h6>

                                                <%--<div class="time-vd">
                                                    <%=item.CreatedDate.ToString("dd/MM/yyyy") %>
                                                </div>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <%} %>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <section class="res">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <iframe width="100%" height="280" src="<%=UIHelper.GetConfigByName_v1("Videohome",lang).Replace("watch?v=","/embed/") %>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 pl-md-5">
                        <%var c = UIHelper.GetConfigByName_v1("TieuDeDongVideo", lang); %>
                        <%var line_videos = UIHelper.CutContentByMinusCharactor(c); %>
                        <div class="heading mt-4 mt-md-0">
                            <%=line_videos[0] %>
                            <div>
                                <%=line_videos[1] %>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" id="txtName" class="form-control" placeholder="<%=UIHelper.MultiLanguageManual(lang,"Họ và tên","Full name") %>" />
                        </div>
                        <div class="form-group">
                            <input type="text" id="txtPhone" class="form-control" placeholder="<%=UIHelper.MultiLanguageManual(lang,"Số điện thoại","Phone number") %>" />
                        </div>
                        <div class="form-group">
                            <input type="text" id="txtAddress" class="form-control" placeholder="<%=UIHelper.MultiLanguageManual(lang,"Địa chỉ","Address") %>" />
                        </div>
                        <div class="form-group">
                            <a href="javascript:void(0)" id="dat-lien-he" class="btn"><%=UIHelper.MultiLanguageManual(lang,"Đăng ký","Register") %></a>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
    <script type="text/javascript">
        $('#dat-lien-he').off('click').on('click', function () {
            var name = $('#txtName').val();
            var phoneNumber = $('#txtPhoneNumber').val();
            //var email = $('#txtEmail').val();
            var address = $('#txtAddress').val();
            var type = "lien-he";
            R.Post({
                params: {
                    name: name,
                    address: address,
                    phoneNumber: phoneNumber,
                    //note: note,
                    type: type
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "save",
                success: function (res) {
                    if (res.Success) {
                        console.log(1);
                        alert("Cảm ơn Quý khách đã để lại thông tin! Chúng tôi sẽ liên hệ lại sớm!");
                    }
                    // $('.card-header').RLoadingModuleComplete();
                }
            });
        })
    </script>
</asp:Content>
