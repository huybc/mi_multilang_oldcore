﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMTravel.Core.Helper;
using Mi.BO.Base.News;
using Mi.BO.Base.Zone;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Zone;

namespace CMTravel.Pages
{
    public partial class TourByCategory : BasePages
    {
        public ZoneEntity zoneObj;
        public List<TourBasicEntity> tours;
        public int totalRows = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsCallback)
            {
                var location = Page.RouteData.Values["zoneName"];
                if (location != null)
                {
                    zoneObj = ZoneBo.GetZoneByAlias(location.ToString(), Current.LanguageJavaCode);
                    if (zoneObj != null)
                    {
                        Page.Title = zoneObj.Name;
                        Page.MetaDescription = zoneObj.MetaDescription;
                        Page.MetaKeywords = zoneObj.MetaKeyword;

                        var pageIndex = Request.QueryString["page"].ToInt32Return0();

                        tours = NewsBo.SearchByShortUrlTourV2(zoneObj != null ? zoneObj.Url : "", Current.LanguageJavaCode, 18, pageIndex == 0 ? 1 : pageIndex, UIPager.PageSize, ref totalRows).ToList();
                        UIPager.TotalItems = totalRows;
                    }
                    else
                    {
                        tours = new List<TourBasicEntity>();
                    }

                }
                else
                {
                    zoneObj = new ZoneEntity();
                    tours = new List<TourBasicEntity>();
                }
            }
        }
    }
}