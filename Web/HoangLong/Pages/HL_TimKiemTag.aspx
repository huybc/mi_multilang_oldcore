﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/HoangLongMaster.Master" AutoEventWireup="true" CodeBehind="HL_TimKiemTag.aspx.cs" Inherits="CMTravel.Pages.HL_TimKiemTag" %>
<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%--<%
        var currentLanguage = Page.RouteData.Values["languageCode"].ToString();
       
        //var type = "";
        var lang = "";
        switch (currentLanguage)
        {
            case "vi":
                lang = "vi-VN";
                break;
            case "en":
                lang = "en-US";
                break;
        }


    %>--%>
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;

        %>
    <%// Lay ra cac thong tin can thiet %>

    <%--<% var currentType = Page.RouteData.Values["type"].ToString();
        var zone_alias = Page.RouteData.Values["zone"].ToString();
        var zone_tar = ZoneBo.GetZoneByAlias(zone_alias, lang);
        %>--%>
    <%var search = Request.QueryString["search"].ToString(); %>
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4 col-12">
                    <%--<%//Lay danh sach cac Zone co type nhu vay va co parent ID %>
                    <%var list_zone_with_same_tree = ZoneBo.GetZoneWithLanguageByType(zone_tar.Type, lang); %>
                    <%var list_zone_with_same_parent = list_zone_with_same_tree.Where(r => r.ParentId > 0); %>
                    <%var zone_parent = list_zone_with_same_tree.Where(r => r.ParentId == 0).SingleOrDefault(); %>
                    <div class="menu-left">
                        <%foreach (var item in list_zone_with_same_parent)
                            {  %>
                        <div class="item">
                            <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(zone_tar.Type) %>/<%=item.Alias %>" title=""><%=item.lang_name %></a>
                        </div>
                        <%} %>
                    </div>--%>
                    <%var banner_quang_cao = ConfigBo.AdvGetByType(9, lang).ToList(); %>
                    <%foreach (var item in banner_quang_cao)
                        { %> 
                        <div class="qc-left">
                        <a href="<%=item.Url %>">
                            <%string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority); %>
                            <img src="/uploads/<%=item.Thumb %>" alt="<%=domainName %>" class="img-fluid" /></a>
                    </div>
                    <%} %>

                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-hoanglong-2">
                            <li class="breadcrumb-item" aria-current="page"><%=Language.Home %></li>
                            <li class="breadcrumb-item active" aria-current="page"><%=Language.Search %></li>
                        </ol>
                    </nav>
                    <hr />
                    <div class="list-ck">
                        <%//Lay danh sach tin o day, phan trang %>
                        <%var total = 0; %>
                        <%var pageIndex = 1; %>
                        <%var pageSize = 10; %>
                        <%var col_trai_to = "col-md-8 col-sm-7 col-12"; %>
                        <%var col_phai_to = "col-md-4 col-sm-5 col-12"; %>
                        <%var col_trai_nho = "col-md-4 col-sm-5 col-5"; %>
                        <%var col_phai_nho = "col-md-8 col-sm-7 col-7"; %>
                        <%var list_news_in_zone = NewsBo.GetNewsDetailWithLanguageSameTag(0, lang, 2, 0, search, pageIndex, pageSize, ref total).ToList(); %>
                        
                        <%for (int i = 0; i < list_news_in_zone.Count(); i++)
                            {%>
                        <div class="<%=i==0?"item-large":"item" %>">
                            <div class="row">

                                <div class="<%=i==0?col_trai_to:col_trai_nho %>">
                                    <div class="image mb-3 mb-sm-0">
                                        <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(list_news_in_zone[i].Type) %>/<%=list_news_in_zone[i].Alias %>/<%=list_news_in_zone[i].Url %>.<%=list_news_in_zone[i].NewsId %>.htm" title="">
                                            <img src="/uploads/<%=i>0 ? ("/thumb/"+list_news_in_zone[i].Avatar) : list_news_in_zone[i].Avatar %>" class="img-fluid" alt="<%=list_news_in_zone[i].Title %>" /></a>
                                    </div>
                                </div>
                                <div class="<%=i==0?col_phai_to:col_phai_nho %>">
                                    <h3 class="title">
                                        <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(list_news_in_zone[i].Type) %>/<%=list_news_in_zone[i].Alias %>/<%=list_news_in_zone[i].Url %>.<%=list_news_in_zone[i].NewsId %>.htm" title=""><%=list_news_in_zone[i].Title %></a>
                                    </h3>
                                    <p class="des">
                                        <%=list_news_in_zone[i].Sapo %>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <%} %>
                    </div>
                    <div class="pani">
                        <%var pageNow = pageIndex; %>
                        <%var totalPage = (int)Math.Ceiling((double)total / (double)pageSize); %>
                        <span class="page-number mr-3 show-page" data-page="<%=pageNow %>"><%=Language.Page %> <span class="page-now"><%=pageNow %></span>/<%=totalPage %></span>
                        <a href="javascript:void(0)" id="xem-them-btn" data-size="<%=pageSize %>" data-index="<%=pageIndex %>" data-type="<%=0 %>" data-search="<%=search %>" data-zone="<%=0 %>" data-lang="<%=lang %>" data-txttype="<%="tim-kiem" %>" class="btn mr-3" role="button"><%=Language.More %></a>
                        <a href="javascripts:void(0);" class="btn " onclick="topFunction()" role="button"><%=Language.Page_Up %></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
    <script type="text/javascript">
        $('#xem-them-btn').off('click').on('click', function () {
            var zone_type = $(this).data('type');
            var lang = $(this).data('lang');
            var hot = 2;
            var zone_id = $(this).data('zone');
            var search = $(this).data('search');
            var pageIndex = $(this).data('index')+1;
            var pageSize = $(this).data('size');
            var currentType = $(this).data('txttype');
            R.Post({
                params: {
                    type : zone_type,
                    lang : lang,
                    hot : hot,
                    zone_id: zone_id,
                    search: search,
                    pageIndex: pageIndex,
                    pageSize: pageSize,
                    isTagSearch: 1,
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "load-trang",
                success: function (res) {
                    if (res.Success) {
                        var result = res.Data;
                        var htm = "";
                        
                        result.forEach(function (e) {
                            
                            var link = "/" + lang.split('-')[0] + "/" + currentType + "/" + e.Alias + "/" + e.Url + "." + e.NewsId + ".htm"
                            htm += '<div class="item">';
                            htm += '<div class="row">';
                            //trai
                            htm += '<div class="col-md-4 col-sm-5 col-5">';
                            htm += '<div class="image mb-3 mb-sm-0">';
                            htm += '<a href="' + link + '" title="">'
                            htm += '<img src="/uploads/thumb/'+e.Avatar+'" class="img-fluid" alt="" />'
                            htm += '</a>'
                            htm += '</div>';
                            htm += '</div>';
                            //phai
                            htm += '<div class="col-md-8 col-sm-7 col-7">'
                            htm += '<h3 class="title">'
                            htm += '<a href="'+link+'" title="">'+e.Title+'</a>'
                            htm += '</h3>'
                            htm += '<p class="des"> '+e.Sapo+' </p>'
                            htm += '</div>'
                            //
                            htm += '</div>';
                            htm += '</div>';
                        });
                        $('.list-ck').append(htm);
                        //Xu ly hau ky
                        if (result.length > 0) {
                            $('#xem-them-btn').data('index', pageIndex);
                            var index = $('#xem-them-btn').data('index');
                            $('.page-now').html(index);
                        }
                        
                    }

                }, error: function () {
                    //$('#contact').RLoadingModuleComplete();
                }
            });
        })
    </script>
</asp:Content>
