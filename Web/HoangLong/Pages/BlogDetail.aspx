﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/CMTravel.Master" AutoEventWireup="true" CodeBehind="BlogDetail.aspx.cs" Inherits="CMTravel.Pages.BlogDetail" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Pages/Controls/SideBarRight.ascx" TagPrefix="uc1" TagName="SideBarRight" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <meta property="fb:app_id" content="<%=UIHelper.GetConfigByName("FBID") %>" />
    <meta property="og:url" content="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" />
    <meta property="og:type" content="article" />
    <%if (obj != null)
        { %>
    <meta property="og:title" content="<%= obj.Title %>" />
    <meta property="og:description" content="<%= obj.Sapo.GetText()%>" />
    <meta property="og:image" content="<%=domainName %>/Uploads/<%= obj.Avatar %>" />
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <script type="application/ld+json">
				            {  
                                    "@context":"http://schema.org",
                                    "@type":"Article",
                                    "mainEntityOfPage":{  
                                        "@type":"WebPage",
                                        "@id":"<%=HttpContext.Current.Request.Url.AbsoluteUri %>"
                                    },
                                   "headline":"<%=!string.IsNullOrEmpty(obj.TitleSeo)?obj.TitleSeo:obj.Title%>",
                                    "image":{  
                                        "@type":"ImageObject",
                                        "url":"<%=domainName+"/uploads/"+obj.Avatar %>",
                                        "height":600,
                                        "width":800
                                    },
                                    "datePublished":"<%=obj.DistributionDate %>",
                                    "dateModified":"<%=obj.LastModifiedDate %>",
                                    "author":{  
                                        "@type":"Person",
                                        "name":"repair"
                                    },
                                    "publisher":{  
                                        "@type":"Organization",
                                        "name":"Repair.vn",
                                        "logo":{  
                                            "@type":"ImageObject",
                                            "url":"<%=domainName+"/uploads/"+UIHelper.GetConfigByName("Logo")%>",
                                            "width":35,
                                            "height":34
                                        }
                                    },
                                    "description":"<%=obj.Sapo.GetText() %>"
                            }
    </script>
    <%
        var currentLanguage = Current.Language;
        var currentLanguageJava = Current.LanguageJavaCode;
        var news = new List<TourBasicEntity>();
        var zone = new ZoneEntity();

        if (!string.IsNullOrEmpty(obj.ZoneIds))
        {
            var ids = obj.ZoneIds.Split(';');
            if (ids.Any())
            {
                int totalRows = 0;
                zone = ZoneBo.GetZoneById(ids[0].ToInt32Return0(),currentLanguageJava);
                if (zone != null)
                {
                    news.AddRange(NewsBo.SearchByShortUrlNewsV2(zone.Alias, Current.LanguageJavaCode, 1, 1, 9, ref totalRows).Where(it => it.Id != obj.Id));

                }
               
            }


        }
    %>
    <div class="blog-detail">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12">
                    <section class="breadcrumb breadcrumb-blog-detail" style="max-width: 100%">
                        <div class="breadcrumb-item parent"><a href="/<%=currentLanguage %>/blog.htm" title="">Blog</a></div>
                        <div class="breadcrumb-item"><a href="/<%=currentLanguage %>/blog/<%=zone!=null?zone.Alias:"" %>" title=""><%=zone!=null?zone.Name:"" %></a></div>
                    </section>
                    <div class="d-flex mb-4">
                        <div class="share d-flex ml-auto">
                            <span class="mr-2 align-self-center">Chia sẻ</span>
                            <a class="mr-2" href="">
                                <img src="/Themes/imgs/fb-ic.png" /></a>
                            <a class="mr-2" href="">
                                <img src="/Themes/imgs/gg-ic.png" /></a>
                        </div>
                    </div>
                    <section class="content-blog">
                        <h1 class="title"><%=obj.Title %></h1>
                        <div class="time align-self-center">
                            <%=UIHelper.GetOnlyCurentDate(obj.DistributionDate,currentLanguageJava) %>
                        </div>
                        <div class="description">
                            <%=obj.Sapo %>
                        </div>
                        <div class="newsbody">
                            <%=obj.Body %>
                        </div>

                        <p class="text-right">
                            <%=obj.Author %>
                        </p>
                    </section>

                    <section class="blog-detail-tag d-inline-block">
                        <div class="heading">
                            Tag
                        </div>
                        <%
                            var tags = GetTags(obj.Id);
                            foreach (var item in tags)
                            {
                        %>
                        <div class="item-tag">
                            <%=item.Name %>
                        </div>
                        <%} %>


                        <%--<div class="item-tag">
                            phượt thủ
                        </div>
                        <div class="item-tag">
                            bún chả
                        </div>--%>
                    </section>
                    <div class="entry-footer container">
                        <div class="row">
                            <div class="text-right">
                                <div class="fb-like" data-href="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="comment-facebook" style="width: 100%">
                                <div class="fb-comments" data-href="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="100%" data-numposts="5"></div>
                            </div>
                        </div>
                    </div>
                    <section class="list-news">
                        <div class="heading ">
                            <div href="" title=""><%=Language.ArticleRelation %></div>
                        </div>
                        <%
                            foreach (var item in news)
                            {%>
                        <div class="item py-4 row">
                            <div class="image align-self-center col-lg-4 col-md-5 col-sm-12">
                                <a title="<%=item.Title %>" href="/<%=currentLanguage %>/blog/<%=item.Url %>.<%=item.Id %>.htm">
                                    <img src="/Uploads/thumb/<%=item.Avatar %>" class="img-fluid">
                                </a>
                            </div>
                            <div class="content col-lg-8 col-md-7 col-sm-12">
                                <h3 class="title"><a class="tit" title="<%=item.Title %>" href="/<%=currentLanguage %>/blog/<%=item.Url %>.<%=item.Id %>.htm"><%=item.Title %></a></h3>
                                <div class="time">
                                    <i class="far fa-clock mr-2"></i><%=UIHelper.GetOnlyCurentDate(item.DistributionDate,currentLanguageJava) %>
                                </div>
                                <div>
                                    <%=Utility.SubWordInString(item.Sapo.GetText(), 40)%>
                                </div>
                            </div>
                        </div>
                        <%} %>
                    </section>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <uc1:SideBarRight runat="server" ID="SideBarRight" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
