﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/CMTravel.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="CMTravel.Pages.Contact" %>

<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Resources" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <section class="banner-vmb mb-4">
        <img src="/Themes/imgs/bg-lien-he.jpg" class="w-100" />
        <%--<div class="ovelay">
            <div class="container align-self-center">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="heading">
                            <div class="top text-center">
                                Liên hệ
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>--%>
    </section>
    <section class="contact" id="contact">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-12">
                <div class="box">
                    <div class="row">
                        <div class="col-md-7 col-sm-12 col-12 left">
                            <h4 class="heading"><%=Language.Contact %>
                            </h4>
                            <div class="form-group row">
                                <label for="example-text-input" class=" col-md-3 col-sm-12 col-12 col-form-label"><%=Language.FullName %>:</label>
                                <div class="col-md-9 col-sm-12 col-12">
                                    <input class="form-control" type="text" id="txtName">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-search-input" class="col-md-3 col-sm-12 col-12 col-form-label"><%=Language.Mobile %>:</label>
                                <div class="col-md-9 col-sm-12 col-12">
                                    <input class="form-control" type="tel" id="txtPhoneNumber">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-md-3 col-sm-12 col-12 col-form-label"><%=Language.Email %>:</label>
                                <div class="col-md-9 col-sm-12 col-12">
                                    <input class="form-control" type="email" id="txtEmail">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-md-3 col-sm-12 col-12 col-form-label"><%=Language.Note %>:</label>
                                <div class="col-md-9 col-sm-12 col-12">
                                    <textarea class="form-control" type="email" rows="4" id="txtNote"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3 col-sm-12 col-12 "></div>
                                <div class="col-md-9 col-sm-12 col-12">
                                    <button class="btn btn-book" id="btnSendContact"><%=Language.Send %></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-12 d-flex  right">
                            <div class="align-self-center">
                                <div class="text-uppercase mb-3">
                                    <asp:Literal runat="server" Text="<%$ Resources:Language,CallNow %>"></asp:Literal>
                                </div>
                                <div class="h3 font-weight-bold mb-4">
                                    <%=UIHelper.GetConfigByName("HotLine").Replace("-","") %>
                                </div>
                                <div class="text-uppercase font-weight-bold mb-3">
                                    <%=UIHelper.GetConfigByName("CompanyName") %>
                                </div>
                                <ul class="info-company">
                                    <li>
                                        <asp:Literal runat="server" Text="<%$ Resources:Language,Address %>"></asp:Literal>: <%=UIHelper.GetConfigByName("Address") %>
            
                                    </li>
                                    <li>
                                        <asp:Literal runat="server" Text="<%$ Resources:Language,Hotline %>"></asp:Literal>: <%=UIHelper.GetConfigByName("HotLine").Replace("-","") %></li>
                                    <li>
                                        <asp:Literal runat="server" Text="<%$ Resources:Language,Email %>"></asp:Literal>: <%=UIHelper.GetConfigByName("Email") %></li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="w-100 mt-5">
        <%=UIHelper.GetConfigByName("Map") %>
    </section>
    <div class="modal fade" id="ignismyModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
                </div>
                <div class="modal-body">
                    <div class="thank-you-pop">
                        <img src="/Themes/imgs/Green-Round-Tick.png" alt="">
                        <div class="iheader"><%=Language.ThankYou %>!</div>
                        <%-- <p>Your submission is received and we will contact you soon</p>--%>
                        <p><%=Language.ReplyRequest %></p>
                    </div>

                </div>

            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
    <script>
        $(function() {
            $('#btnSendContact').off('click').on('click', function () {
                var obj = {
                    name: $('#txtName').val(),
                    phoneNumber: $('#txtPhoneNumber').val(),
                    email: $('#txtEmail').val(),
                    note: $('#txtNote').val(),
                }
                var error = 0;
                $('input[type=text]').removeClass('_error')
                if ($('#txtName').val().length <= 0) {
                    error++;
                    $('#txtName').addClass('_error')
                }
                if ($('#txtPhoneNumber').val().length <= 0) {
                    $('#txtPhoneNumber').addClass('_error')
                    error++;
                }
                if (error > 0) {
                    return false;
                }
                $('#contact').RLoadingModule();
                R.Post({
                    params: obj,
                    module: "ui-action",
                    ashx: 'modulerequest.ashx',
                    action: "save",
                    success: function (res) {
                        if (res.Success) {
                            $('#contact').RLoadingModuleComplete();
                            $('#ignismyModal').modal('show').on('hidden.bs.modal', function (e) {
                                location.reload();
                            })
                        }

                    }, error: function () {
                        $('#contact').RLoadingModuleComplete();
                    }
                });
            });
        })
    </script>
</asp:Content>

