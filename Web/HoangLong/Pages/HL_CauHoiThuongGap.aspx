﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/HoangLongMaster.Master" AutoEventWireup="true" CodeBehind="HL_CauHoiThuongGap.aspx.cs" Inherits="CMTravel.Pages.HL_CauHoiThuongGap" %>

<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%--<%
        var currentLanguage = Page.RouteData.Values["languageCode"].ToString();
        ///var currentType = Page.RouteData.Values["type"].ToString();
        //var type = "";
        var lang = "";
        switch (currentLanguage)
        {
            case "vi":
                lang = "vi-VN";
                break;
            case "en":
                lang = "en-US";
                break;
        }


    %>--%>
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <div class="container">
        <div class="bd-example mb-5">
            <div id="slide-td" class="carousel slide slide-td" data-ride="carousel">
                <%//Lay du lieu can thiet %>
                <%var slider = ConfigBo.AdvGetByType(7, lang).ToList(); %>
                <ol class="carousel-indicators">
                    <%for (int i = 0; i < slider.Count(); i++)
                        { %>
                    <li data-target="#slide-td" data-slide-to="<%=i %>" class="<%=i==0?"active":"" %>"></li>
                    <%} %>
                </ol>
                <div class="carousel-inner">
                    <%for (int i = 0; i < slider.Count(); i++)
                        { %>
                    <div class="carousel-item <%=i==0?"active":"" %>">
                        <img src="/uploads/<%=slider[i].Thumb %>" class="d-block w-100" alt="<%=domainName %>">
                        <div class="carousel-caption d-none d-md-block">
                            <h1><%=slider[i].Name %></h1>
                            <p><%=slider[i].Content %></p>
                        </div>
                    </div>

                    <%} %>
                </div>
                <a class="carousel-control-prev" href="#slide-td" role="button" data-slide="prev">
                    <i class="fas fa-chevron-circle-left" aria-hidden="true" style="color: #fff;"></i>
                </a>
                <a class="carousel-control-next" href="#slide-td" role="button" data-slide="next">
                    <i class="fas fa-chevron-circle-right" aria-hidden="true" style="color: #fff;"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="container mb-5">
        <div class="row">
            <div class="col-12">
                <form class="form-group mb-3 row">
                    <div class="form-group input-group-lg col-md-10 col-sm-9 col-8 ">
                        <input type="text" class="form-control " placeholder="<%=Language.Search %>">
                    </div>
                    <div class=" mb-2 col-md-2 col-sm-3 col-4 pl-0 ">
                        <button type="submit" class="btn btn-dat-lich mb-2 px-2 py-2 px-md-4 w-100" style="font-weight: 400;"><%=Language.Search %></button>
                    </div>

                </form>
            </div>
        </div>
        <div class="accordion-faq" id="accordionExample">
            <%//Lay cac du lieu can thiet phan cau hoi %>
            <%var zone_khach_hang = ZoneBo.GetZoneWithLanguageByType(13, lang); %>
            <%var zone_cau_hoi = zone_khach_hang.Where(r => r.ParentId > 0 && r.Alias.Equals("thong-tin-suc-khoe-a-z")).SingleOrDefault(); %>
            <%var total_row = 0; %>
            <%var list_cau_hoi = NewsBo.GetNewsDetailWithLanguage(13, lang, 2, zone_cau_hoi.Id, "", 1, 1000, ref total_row).ToList(); %>
            <%for (int i = 0; i < list_cau_hoi.Count(); i++)
                { %>
            <div class="card border-0 mb-4">
                <div class="card-header" id="heading<%=i %>" data-toggle="collapse" data-target="#collapse<%=i %>"
                    aria-expanded="<%=i==0?"true":"false" %>" aria-controls="collapse<%=i %>">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button">
                            <%=list_cau_hoi[i].Sapo %>
                        </button>
                    </h2>
                </div>

                <div id="collapse<%=i %>" class="collapse <%=i==0?"show":"" %>" aria-labelledby="heading<%=i %>"
                    data-parent="#accordionExample">
                    <div class="card-body px-0">
                        <%=list_cau_hoi[i].Body %>
                    </div>
                </div>
            </div>
            <%} %>
        </div>
    </div>

    <section>
        <div class="container">
            <div class="heading-right heading-right-2 mb-4">
                <%=UIHelper.GetConfigByName_v1("BaiVietLienQuan",lang) %>
            </div>
            <%var zone_bai_huong_dan = zone_khach_hang.Where(r => r.ParentId == 0).SingleOrDefault(); %>
            <%var total_row_2 = 0; %>
            <%var pageIndex = 1; %>
            <%var pageSize = 10; %>
            <%var list_bai_huong_dan = NewsBo.GetNewsDetailWithLanguage(zone_bai_huong_dan.Type, lang, 2, zone_bai_huong_dan.Id, string.Empty, pageIndex, pageSize, ref total_row_2); %>
            <%foreach (var item in list_bai_huong_dan)
                { %>
            <div class="item-news pb-3 border-bottom">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                        <div class="image mb-3 mb-md-0">
                            <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(13) %>/<%=item.Alias %>/<%=item.Url %>.<%=item.NewsId %>.htm" class="link" title="">
                                <img src="/uploads/thumb/<%=item.Avatar %>" class="img-fluid avatar" alt="<%=item.Title %>" /></a>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-12  ">
                        <h3 class="title mb-4">
                            <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(13) %>/<%=item.Alias %>/<%=item.Url %>.<%=item.NewsId %>.htm" class="link link-title" title=""><%=item.Title %></a>
                        </h3>
                        <div class="des">
                            <%=item.Sapo %>
                        </div>

                    </div>
                </div>
            </div>
            <%} %>
            <div class="pani my-4">
                <a href="javascript:void(0)" id="xem-them-btn" data-type="<%=zone_bai_huong_dan.Type %>" data-lang="<%=lang %>" data-zone="<%=zone_bai_huong_dan.Id %>" data-index="<%=pageIndex %>" data-size="<%=pageSize %>" data-txttype="<%=zone_bai_huong_dan.Alias %>" class="btn mr-3" role="button"><%=Language.More %></a>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
    <script type="text/javascript">
        $('#xem-them-btn').off('click').on('click', function () {
            var zone_type = $(this).data('type');
            var lang = $(this).data('lang');
            var hot = 2;
            var zone_id = $(this).data('zone');
            var search = "";
            var pageIndex = $(this).data('index') + 1;
            var pageSize = $(this).data('size');
            var currentType = $(this).data('txttype');

            R.Post({
                params: {
                    type: zone_type,
                    lang: lang,
                    hot: hot,
                    zone_id: zone_id,
                    search: search,
                    pageIndex: pageIndex,
                    pageSize: pageSize
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "load-trang",
                success: function (res) {
                    if (res.Success) {
                        var result = res.Data;
                        console.log(result);
                        var htm = "";

                        result.forEach(function (e) {
                            var extras = JSON.parse(e.Extras);
                            var link = "/" + lang.split('-')[0] + "/" + currentType + "/" + e.Alias + "/" + e.Url + "." + e.NewsId + ".htm";
                            /*
                             Tu tin viet kieu moi xem nao
                             */
                            $('.item-news').last().clone().insertAfter('.item-news:last');
                            var el = $('.item-news').last();
                            el.find('.avatar').attr("src", '/uploads/thumb/' + e.Avatar + '');
                            el.find('.link-title').text(e.Title);
                            el.find('.link').attr("href", link);
                            el.find('.des').html(e.Sapo);

                        });
                        //$('.list-ck').append(htm);
                        //Xu ly hau ky
                        if (result.length > 0) {
                            $('#xem-them-btn').data('index', pageIndex);
                            var index = $('#xem-them-btn').data('index');
                            $('.page-now').html(index);
                        }

                    }

                }, error: function () {
                    //$('#contact').RLoadingModuleComplete();
                }
            });
        })
    </script>
</asp:Content>
