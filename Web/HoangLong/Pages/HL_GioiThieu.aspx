﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/HoangLongMaster.Master" AutoEventWireup="true" CodeBehind="HL_GioiThieu.aspx.cs" Inherits="CMTravel.Pages.HL_GioiThieu" %>

<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%--<%
        var currentLanguage = Page.RouteData.Values["languageCode"].ToString();
        var lang = "";
        switch (currentLanguage)
        {
            case "vi":
                lang = "vi-VN";
                break;
            case "en":
                lang = "en-US";
                break;
        }

    %>--%>
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        %>
    <div class="container py-5">
        <div class="row">
            <div class="col-lg-9 col-md-8 col-sm-12 col-12 pr-md-4">
                <%//Lay danh sach Zone Gioi thieu %>
                <%var about_zone = ZoneBo.GetZoneWithLanguageByType(1, lang); %>
                <%var about_zone_parent = about_zone.Where(r => r.ParentId == 0).SingleOrDefault(); %>
                <%var about_zone_child = about_zone.Where(r => r.ParentId > 0 && r.Alias != "doi-ngu-bac-si").ToList(); %>
                <%for (int i = 0; i < about_zone_child.Count(); i++)
                    {  %>
                <div class="list-intro <%=i==0?"mt-2":"" %>">
                    <h3 class="heading"><%=about_zone_child[i].lang_name %></h3>
                    <%//Lay danh sach bai viet trong zone %>
                    <%var tor = 0; %>
                    <%var list_news = NewsBo.GetNewsDetailWithLanguage(1, lang, 2, about_zone_child[i].Id, string.Empty, 1, 4, ref tor); %>
                    <%foreach (var item in list_news)
                        { %>
                    <div class="item-intro">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-12">
                                <div class="image mb-3 mb-sm-0">
                                    <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(item.Type) %>/<%=item.Alias %>/<%=item.Url %>.<%=item.NewsId %>.htm">
                                        <img src="/uploads/thumb/<%=item.Avatar %>" class="img-fluid" alt="<%=item.Title %>" /></a>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-8 col-12 pl-sm-1">
                                <h4 class="title">
                                    <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(item.Type) %>/<%=item.Alias %>/<%=item.Url %>.<%=item.NewsId %>.htm" title=""><%=item.Title %></a>
                                </h4>
                                <div class="des">
                                    <%=item.Sapo %>
                                </div>
                                <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(item.Type) %>/<%=item.Alias %>/<%=item.Url %>.<%=item.NewsId %>.htm" class="color-1C95D2" title=""><%=Language.More %></a>
                            </div>
                        </div>
                    </div>
                    <%} %>
                </div>
                <%} %>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-12 col-12">
                <div class="mb-4">
                    <div class="heading-right">
                        <%=Language.Event %>
                    </div>
                    <div class="qc">
                        <b><%=UIHelper.GetConfigByName_v1("TieuDeUuDai",lang) %></b>
                        <p class="font-weight-light">
                            <%=UIHelper.GetConfigByName_v1("NoiDungUuDai",lang) %>
                        </p>
                        <img src="/uploads/<%=UIHelper.GetConfigByName_v1("HinhAnhUuDai",lang) %>" alt="<%=domainName %>" class="img-fluid w-100" />
                    </div>
                </div>

                <div class="heading-right">
                    Video
                </div>
                <%//Lay ra cac bai viet video %>
                <%var zone_gallary = ZoneBo.GetZoneWithLanguageByType(17, lang); %>
                <%var zone_video = zone_gallary.Where(r => r.ParentId > 0 && r.lang_name.Equals("VIDEO")).SingleOrDefault(); %>
                <%var totR = 0; %>
                <%var list_video = NewsBo.GetNewsDetailWithLanguage(17, lang, 2, zone_video.Id, "", 1, 3, ref totR); %>
                <%foreach (var item in list_video)
                    { %>
                <% string data = item.Body;
                            string reg = "\"(h.*?)\"";
                            data = Regex.Matches(data, reg)[0].ToString().Replace("\"", "").Replace("\"", "").Replace(@"watch?v=", @"embed\");
                        %>
                <div class="item-video mb-4">
                    <h6><%=item.Title %></h6>
                    <iframe width="100%" height="200px" class="mb-1" src="<%=data %>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                    <div class="time-vd pl-2">
                        <%=item.CreatedDate.ToString("dd/MM/yyyy") %>
                    </div>
                </div>
                <%} %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
</asp:Content>
