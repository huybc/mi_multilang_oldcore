﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/CMTravel.Master" AutoEventWireup="true" CodeBehind="Booking.aspx.cs" Inherits="CMTravel.Pages.Booking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <section class="banner-vmb mb-4">
        <img src="/Themes/imgs/bg-vmb-search.jpg" class="w-100" />
        <div class="ovelay">
            <div class="container align-self-center">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="heading">
                            <div class="top">
                                Đặt vé máy bay quốc nội & quốc tế
                            </div>
                            <div class="bot m-2">
                                Hơn 3000 tour Quốc tế và trong nước
                            </div>
                        </div>
                        <div class="search-box mb-2">
                            <div class="row no-gutters">
                                <div class="gr-select">
                                    <div class="row no-gutters mb-2">
                                        <div class="col px-1">
                                            <div class="input-departure dropdown">
                                                <input class="form-control input-search-box dropdown-toggle" data-toggle="dropdown" placeholder="" />
                                                <div class="input-icon">
                                                    <img src="/Themes/imgs/input-departure.png" class=" mr-2" />
                                                    <div class="align-self-center">Khởi hành:</div>
                                                </div>
                                                <div class="suggest-select dropdown-menu">
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Đà Lạt</span>
                                                        <span class="color-66666 small align-self-center ml-auto">138 KS</span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Hải Phòng</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Đà Nẵng</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Hồ Chí Minh</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Đà Lạt</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>

                                                </div>
                                            </div>


                                        </div>
                                        <div class="col px-1">
                                            <div class="input-departure dropdown">
                                                <input class="form-control input-search-box pl-80 dropdown-toggle " data-toggle="dropdown" placeholder="" />
                                                <div class="input-icon">
                                                    <img src="/Themes/imgs/input-departure.png" class=" mr-2" />
                                                    <div class="align-self-center">Đến:</div>
                                                </div>
                                                <div class="suggest-select dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Đà Lạt</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Hải Phòng</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Đà Nẵng</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Hồ Chí Minh</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Đà Lạt</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="col px-1">
                                            <div class="input-departure">
                                                <input class="form-control input-search-box pl-100" name="date" placeholder="" />
                                                <div class="input-icon">
                                                    <img src="/Themes/imgs/cal.png" class=" mr-2" />
                                                    <div class="align-self-center">Ngày đi:</div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col px-1">
                                            <div class="input-departure">
                                                <input class="form-control input-search-box pl-100" name="date" placeholder="" />
                                                <div class="input-icon">
                                                    <img src="/Themes/imgs/cal.png" class=" mr-2" />
                                                    <div class="align-self-center">
                                                        Ngày về:
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row no-gutters">
                                        <div class="col px-1">
                                            <div class="input-departure dropdown">
                                                <input class="form-control input-search-box pl-100 dropdown-toggle" type="button" data-toggle="dropdown" placeholder="" value="01" />
                                                <div class="input-icon">
                                                    <img src="/Themes/imgs/nguoilon.png" class=" mr-2" />
                                                    <div class="align-self-center">
                                                        Người lớn:
                                                    </div>
                                                </div>
                                                <div class="suggest-select w-100 h-auto dropdown-menu">
                                                    <ul class="list-group">
                                                        <li class="list-group-item">
                                                            <span class="font-weight-bold mr-2">1</span>Người lớn
                                                            <div class="btn-group ml-auto">
                                                                <button type="button" class="btn btn-sm"><i class="fa fa-minus"></i></button>
                                                                <button type="button" class="btn  btn-sm"><i class="fa fa-plus"></i></button>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <span class="font-weight-bold mr-2">1</span>Trẻ em
                                                            <div class="btn-group ml-auto">
                                                                <button type="button" class="btn btn-sm"><i class="fa fa-minus"></i></button>
                                                                <button type="button" class="btn  btn-sm"><i class="fa fa-plus"></i></button>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <span class="font-weight-bold mr-2">1</span>Phòng
                                                            <div class="btn-group ml-auto">
                                                                <button type="button" class="btn btn-sm"><i class="fa fa-minus"></i></button>
                                                                <button type="button" class="btn  btn-sm"><i class="fa fa-plus"></i></button>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <div class="text-right">
                                                        <button type="button" class="btn  btn-sm mt-2">Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col px-2 align-self-center">
                                            <a href=""><i class="fas fa-plus-square  mr-2"></i>Thêm trẻ em</a>
                                        </div>
                                        <div class="col px-1 pb-0">
                                        </div>
                                        <div class="col px-1 pb-0">
                                        </div>
                                    </div>

                                </div>

                                <div class="gr-btn px-1 ">
                                    <a class="btn btn-warning text-uppercase d-flex w-100 h-100 justify-content-center " target="_blank" href="http://booking.vintraveljsc.com/" style="background: #fe8c00;"><span class="align-self-center">Tìm kiếm</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="font-italic  px-1">
                            <span class="" style="color: #fe8c00;">Các điểm đến đang hot:</span> Đà Nẵng, Phú Quốc, Tam Đảo, Nha Trang, Hạ Long
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="list-combo container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <section class="ss-name">
                    <div class="left">
                        <h3 class="tit">Combo vé máy bay + khách sạn</h3>
                        <div>Làm việc cả năm. Đến lúc tưởng thưởng</div>
                    </div>
                    <div class="right">
                        <select class="form-control select-place">
                            <option>Đà Nẵng</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </section>
                <section class="list row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>

                            </div>
                            <div class="content">
                                <div class="tit">
                                    <a href="" title="">Khu nghỉ dưỡng Melia Đà Nẵng</a>
                                </div>
                                <div class="detail">
                                    Combo 3N2Đ + Vé máy bay khứ hồi + Ăn sáng
                                </div>
                                <div class="cost">
                                    5.500.000 <sup>VNĐ / người</sup>
                                </div>
                            </div>
                            <div class="date ">
                                <i class="far fa-clock mr-2"></i>
                                <span class="">01/08/2019 </span>
                                <i class="fas fa-arrow-right mx-2"></i>
                                <span>05/08/2019</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>

                            </div>
                            <div class="content">
                                <div class="tit">
                                    <a href="" title="">Khu nghỉ dưỡng Melia Đà Nẵng</a>
                                </div>
                                <div class="detail">
                                    Combo 3N2Đ + Vé máy bay khứ hồi + Ăn sáng
                                </div>
                                <div class="cost">
                                    5.500.000 <sup>VNĐ / người</sup>
                                </div>
                            </div>
                            <div class="date ">
                                <i class="far fa-clock mr-2"></i>
                                <span class="">01/08/2019 </span>
                                <i class="fas fa-arrow-right mx-2"></i>
                                <span>05/08/2019</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>

                            </div>
                            <div class="content">
                                <div class="tit">
                                    <a href="" title="">Khu nghỉ dưỡng Melia Đà Nẵng</a>
                                </div>
                                <div class="detail">
                                    Combo 3N2Đ + Vé máy bay khứ hồi + Ăn sáng
                                </div>
                                <div class="cost">
                                    5.500.000 <sup>VNĐ / người</sup>
                                </div>
                            </div>
                            <div class="date ">
                                <i class="far fa-clock mr-2"></i>
                                <span class="">01/08/2019 </span>
                                <i class="fas fa-arrow-right mx-2"></i>
                                <span>05/08/2019</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>

                            </div>
                            <div class="content">
                                <div class="tit">
                                    <a href="" title="">Khu nghỉ dưỡng Melia Đà Nẵng</a>
                                </div>
                                <div class="detail">
                                    Combo 3N2Đ + Vé máy bay khứ hồi + Ăn sáng
                                </div>
                                <div class="cost">
                                    5.500.000 <sup>VNĐ / người</sup>
                                </div>
                            </div>
                            <div class="date ">
                                <i class="far fa-clock mr-2"></i>
                                <span class="">01/08/2019 </span>
                                <i class="fas fa-arrow-right mx-2"></i>
                                <span>05/08/2019</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>

                            </div>
                            <div class="content">
                                <div class="tit">
                                    <a href="" title="">Khu nghỉ dưỡng Melia Đà Nẵng</a>
                                </div>
                                <div class="detail">
                                    Combo 3N2Đ + Vé máy bay khứ hồi + Ăn sáng
                                </div>
                                <div class="cost">
                                    5.500.000 <sup>VNĐ / người</sup>
                                </div>
                            </div>
                            <div class="date ">
                                <i class="far fa-clock mr-2"></i>
                                <span class="">01/08/2019 </span>
                                <i class="fas fa-arrow-right mx-2"></i>
                                <span>05/08/2019</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>

                            </div>
                            <div class="content">
                                <div class="tit">
                                    <a href="" title="">Khu nghỉ dưỡng Melia Đà Nẵng</a>
                                </div>
                                <div class="detail">
                                    Combo 3N2Đ + Vé máy bay khứ hồi + Ăn sáng
                                </div>
                                <div class="cost">
                                    5.500.000 <sup>VNĐ / người</sup>
                                </div>
                            </div>
                            <div class="date ">
                                <i class="far fa-clock mr-2"></i>
                                <span class="">01/08/2019 </span>
                                <i class="fas fa-arrow-right mx-2"></i>
                                <span>05/08/2019</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>

                            </div>
                            <div class="content">
                                <div class="tit">
                                    <a href="" title="">Khu nghỉ dưỡng Melia Đà Nẵng</a>
                                </div>
                                <div class="detail">
                                    Combo 3N2Đ + Vé máy bay khứ hồi + Ăn sáng
                                </div>
                                <div class="cost">
                                    5.500.000 <sup>VNĐ / người</sup>
                                </div>
                            </div>
                            <div class="date ">
                                <i class="far fa-clock mr-2"></i>
                                <span class="">01/08/2019 </span>
                                <i class="fas fa-arrow-right mx-2"></i>
                                <span>05/08/2019</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>

                            </div>
                            <div class="content">
                                <div class="tit">
                                    <a href="" title="">Khu nghỉ dưỡng Melia Đà Nẵng</a>
                                </div>
                                <div class="detail">
                                    Combo 3N2Đ + Vé máy bay khứ hồi + Ăn sáng
                                </div>
                                <div class="cost">
                                    5.500.000 <sup>VNĐ / người</sup>
                                </div>
                            </div>
                            <div class="date ">
                                <i class="far fa-clock mr-2"></i>
                                <span class="">01/08/2019 </span>
                                <i class="fas fa-arrow-right mx-2"></i>
                                <span>05/08/2019</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>

                            </div>
                            <div class="content">
                                <div class="tit">
                                    <a href="" title="">Khu nghỉ dưỡng Melia Đà Nẵng</a>
                                </div>
                                <div class="detail">
                                    Combo 3N2Đ + Vé máy bay khứ hồi + Ăn sáng
                                </div>
                                <div class="cost">
                                    5.500.000 <sup>VNĐ / người</sup>
                                </div>
                            </div>
                            <div class="date ">
                                <i class="far fa-clock mr-2"></i>
                                <span class="">01/08/2019 </span>
                                <i class="fas fa-arrow-right mx-2"></i>
                                <span>05/08/2019</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>

                            </div>
                            <div class="content">
                                <div class="tit">
                                    <a href="" title="">Khu nghỉ dưỡng Melia Đà Nẵng</a>
                                </div>
                                <div class="detail">
                                    Combo 3N2Đ + Vé máy bay khứ hồi + Ăn sáng
                                </div>
                                <div class="cost">
                                    5.500.000 <sup>VNĐ / người</sup>
                                </div>
                            </div>
                            <div class="date ">
                                <i class="far fa-clock mr-2"></i>
                                <span class="">01/08/2019 </span>
                                <i class="fas fa-arrow-right mx-2"></i>
                                <span>05/08/2019</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>

                            </div>
                            <div class="content">
                                <div class="tit">
                                    <a href="" title="">Khu nghỉ dưỡng Melia Đà Nẵng</a>
                                </div>
                                <div class="detail">
                                    Combo 3N2Đ + Vé máy bay khứ hồi + Ăn sáng
                                </div>
                                <div class="cost">
                                    5.500.000 <sup>VNĐ / người</sup>
                                </div>
                            </div>
                            <div class="date ">
                                <i class="far fa-clock mr-2"></i>
                                <span class="">01/08/2019 </span>
                                <i class="fas fa-arrow-right mx-2"></i>
                                <span>05/08/2019</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>

                            </div>
                            <div class="content">
                                <div class="tit">
                                    <a href="" title="">Khu nghỉ dưỡng Melia Đà Nẵng</a>
                                </div>
                                <div class="detail">
                                    Combo 3N2Đ + Vé máy bay khứ hồi + Ăn sáng
                                </div>
                                <div class="cost">
                                    5.500.000 <sup>VNĐ / người</sup>
                                </div>
                            </div>
                            <div class="date ">
                                <i class="far fa-clock mr-2"></i>
                                <span class="">01/08/2019 </span>
                                <i class="fas fa-arrow-right mx-2"></i>
                                <span>05/08/2019</span>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="text-center mb-5">
                    <button class="btn btn-warning btn-view-more m-auto">Xem thêm <i class="fas fa-angle-double-down ml-2"></i></button>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
</asp:Content>
