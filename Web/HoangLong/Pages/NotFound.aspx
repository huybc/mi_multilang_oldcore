﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/CMTravel.Master" AutoEventWireup="true" CodeBehind="NotFound.aspx.cs" Inherits="CMTravel.Pages.NotFound" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <style>
        .Error404{ text-align: center;color: #fff;line-height: 60px;color: #333}
        .Error404 h1{font-size: 120px}
    </style>
    <div class="container" style="padding-top: 50px">
        <div class="Error404">
            <div class="number">
                <img src="/Themes/imgs/not-found.png"/>
            </div>
            <%--<h1>404</h1>--%>
            <h3>Lỗi không tìm thấy trang</h3>
            <p>Có vẻ như trang các bạn tìm kiếm không có sẵn. Bạn có thể thử lại !!</p>

            <a href="/" class="btn btn-success">Trang chủ</a>
        </div>
    </div>
</asp:Content>

