﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/CMTravel.Master" AutoEventWireup="true" CodeBehind="Blog.aspx.cs" Inherits="CMTravel.Pages.Blog" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="CMTravel.Core.Helper" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Resources" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <% var currentLanguageJavaCode = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;%>
    <% var result = ZoneBo.GetListZoneByParentId(-1, (int)Mi.Entity.Base.Zone.ZoneType.News, currentLanguageJavaCode);
        //Tim kiem top 3 bai viet moi nhat 
        // var topNewsest = NewsBo.GetTopNewestNews().ToList();

    %>
    <%--<section class="blog-ss-1">
        <div class="container">
            <div class="row">
                <%
                    //var top3 = NewsBo.GetTop3IsHot().OrderBy(r => r.CreatedDate).Take(3).ToList();

                    var top3 = new List<NewsEntity>();
                    for(int i = 0; i < 3; i++)
                    {
                        string name = "FocusNews" + (i+1).ToString();
                        var id = ConfigBo.GetByConfigName(name).ConfigValue.Split('.')[1];
                        var newDetail = NewsBo.GetNewsDetailById(long.Parse(id));
                        top3.Add(newDetail);
                    }
                %>
                <%for (int i = 0; i < top3.Count(); i++)
                    { %>
                <%var mediaAlias = ZoneBo.GetZoneByNewsId(top3[i].Id).First().ShortUrl; %>
                <% if (i == 0)
                    { %>

                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="item mb-3">
                        <a href="" title="">
                            <img src="/Uploads/thumb/thumb/<%=top3[i].Avatar %>" alt="" /></a>
                        <h2 class="title">
                            <a href="/<%=currentLanguage %>/blog/<%=mediaAlias %>/<%=top3[i].Url %>.<%=top3[i].Id %>.htm" title=""><%=top3[i].Title %></a>

                        </h2>
                    </div>
                </div>

                <%} %>
                <%if (i == 1)
                    { %> 
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div class="item mb-3 item-right">
                        <a href="" title="">
                            <img src="/Uploads/thumb/thumb/<%=top3[i].Avatar %>" alt="" /></a>
                        <h2 class="title">
                            <a href="/<%=currentLanguage %>/blog/<%=mediaAlias %>/<%=top3[i].Url %>.<%=top3[i].Id %>.htm" title=""><%=top3[i].Title %></a>

                        </h2>
                    </div>
                <% }%>
                <%if (i == 2)
                    {%> 
                        <div class="item mb-3 item-right">
                        <a href="" title="">
                            <img src="/Uploads/thumb/thumb/<%=top3[i].Avatar %>" alt="" /></a>
                        <h2 class="title">
                            <a href="/<%=currentLanguage %>/blog/<%=mediaAlias %>/<%=top3[i].Url %>.<%=top3[i].Id %>.htm" title=""><%=top3[i].Title %></a>

                        </h2>
                    </div>
                </div>
                        <%} %>
                
                <%} %>
                    

            </div>
        </div>
    </section>--%>
    <section class="media">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="heading text-uppercase">
                        <div >Media</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <%var mediaList = NewsBo.GetTop4VideoNews(currentLanguageJavaCode);
                    foreach (var item in mediaList)
                    {
                        var mediaAlias = ZoneBo.GetZoneByNewsId(item.Id).First();
                %>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item">
                        <% string data = item.Body;
                            string reg = "\"(h.*?)\"";
                            data = Regex.Matches(data, reg)[0].ToString().Replace("\"", "").Replace("\"", "").Replace(@"watch?v=", @"embed\");
                        %>
                        <iframe width="100%" height="100%" src="<%=data %>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <div class="text">
                            <h3 class="title">
                                <a class="tit" href="/<%=currentLanguage %>/blog/<%=mediaAlias %>/<%=item.Url %>.<%=item.Id %>" title="<%=item.Title %>"><%=item.Title %></a>
                            </h3>
                            <div class="date">
                                <span class="acc"><%=item.Author %></span>
                                <span class=""><%=UIHelper.GetOnlyCurentDate(item.CreatedDate,currentLanguageJavaCode) %></span>
                            </div>
                        </div>
                    </div>
                </div>
                <%}%>
            </div>
        </div>
    </section>
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12">
                    <%foreach (var item in result)
                        { %>

                    <div class="blog-ss-2">
                        <%--<div class="heading">
                            <a href="blog-category.html" title="">Điểm đến</a>
                        </div>--%>
                        <%if (item.SortOrder == 1)
                            {
                                var news = NewsBo.GetByZoneId(item.Id, currentLanguageJavaCode, 5).ToList();
                        %>
                        <div class="wp">
                            <div class="heading">
                                <a href="/<%=currentLanguage %>/blog/<%=item.Url %>" title=""><%=item.Name %></a>
                            </div>
                            <div class="row">
                                <%for (int i = 0; i < news.Count(); i++)
                                    { %>
                                <%if (i == 0)
                                    { %>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="item-left">
                                        <div class="image">
                                            <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>">
                                                <img src="/Uploads/thumb/<%=news[i].Avatar %>" alt="<%=news[i].Title %>" /></a>
                                        </div>
                                        <h2 class="title">
                                            <a  class="tit" href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                        </h2>
                                        <div class="detail">
                                            <%=Utility.SubWordInString(news[i].Sapo.GetText(),40) %>
                                        </div>

                                    </div>
                                </div>
                                <%} %>
                                <%if (i == 1)
                                    { %>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="item-right d-flex">
                                        <div class="image">
                                            <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>">
                                                <img src="/Uploads/thumb/<%=news[i].Avatar %>" alt="<%=news[i].Title %>" /></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="title">
                                                <a class="tit" href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>"><%=news[i].Title %></a>
                                            </h2>
                                            <div class="time">
                                                <%=UIHelper.GetOnlyCurentDate(news[i].CreatedDate,currentLanguageJavaCode) %>
                                            </div>
                                        </div>

                                    </div>
                                    <%} %>
                                    <%if (i > 1 && i < news.Count() - 1)
                                        { %>
                                    <div class="item-right d-flex">
                                        <div class="image">
                                            <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                                <img src="/Uploads/thumb/<%=news[i].Avatar %>" alt="" /></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="title">
                                                <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                            </h2>
                                            <div class="time">
                                                <%=UIHelper.GetOnlyCurentDate(news[i].CreatedDate,currentLanguageJavaCode) %>
                                            </div>
                                        </div>

                                    </div>
                                    <%} %>
                                    <%if (i == news.Count() - 1)
                                        { %>
                                    <div class="item-right d-flex">
                                        <div class="image">
                                            <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                                <img src="/Uploads/thumb/thumb/<%=news[i].Avatar %>" alt="" /></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="title">
                                                <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                            </h2>
                                            <div class="time">
                                                <%=UIHelper.GetOnlyCurentDate(news[i].CreatedDate,currentLanguageJavaCode) %>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <%} %>
                                <%} %>
                            </div>

                        </div>
                        <%} %>
                    </div>


                    <div class="blog-ss-2 mb-4">
                        <%if (item.SortOrder == 2)
                            {
                                var news = NewsBo.GetByZoneId(item.Id, currentLanguageJavaCode, 5).ToList();
                        %>
                        <div class="wp mb-3">
                            <div class="heading">
                                <a class="" title="<%=item.Name %>" href="/<%=currentLanguage %>/blog/<%=item.Url %>"><%=item.Name %></a>
                            </div>
                            <%for (int i = 0; i < news.Count(); i++)
                                { %>
                            <%if (i == 0)
                                {%>
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="item-left">
                                        <div class="image">
                                            <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>">
                                                <img src="/Uploads/thumb/<%=news[i].Avatar %>" alt="<%=news[i].Title %>" /></a>
                                        </div>
                                        <h2 class="title">
                                            <a class="tit" href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>"><%=news[i].Title %></a>
                                        </h2>

                                    </div>
                                </div>

                                <%} %>
                                <%if (i == 1)
                                    { %>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="item-left">
                                        <div class="image">
                                            <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>">
                                                <img src="/Uploads/thumb/<%=news[i].Avatar %>" alt="<%=news[i].Title %>" /></a>
                                        </div>
                                        <h2 class="title">
                                            <a class="tit" href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>"><%=news[i].Title %></a>
                                        </h2>

                                    </div>
                                </div>
                                <%} %>
                                <%} %>
                            </div>

                        </div>
                        <div class="slide-blog">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <div class="swiper-container">
                                        <!-- Additional required wrapper -->
                                        <div class="swiper-wrapper">
                                            <!-- Slides -->
                                            <%for (int i = 2; i < news.Count(); i++)
                                                { %>
                                            <div class="swiper-slide">
                                                <div class="item-slide">
                                                    <div class="image">
                                                        <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>">
                                                            <img src="/Uploads/thumb/<%=news[i].Avatar %>" alt="<%=news[i].Title %>" class="w-100 h-100" /></a>
                                                    </div>
                                                    <h3 class="title">
                                                        <a class="tit" href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>"><%=news[i].Title %>
                                                        </a>
                                                    </h3>
                                                </div>
                                            </div>

                                            <%} %>
                                        </div>

                                        <!-- If we need navigation buttons -->
                                        <div class="swiper-button-prev"><i class="fas fa-chevron-left"></i></div>
                                        <div class="swiper-button-next"><i class="fas fa-chevron-right"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%} %>
                    </div>
                    <%if (item.SortOrder == 3)
                        {
                            var news = NewsBo.GetByZoneId(item.Id, currentLanguageJavaCode, 5).ToList();
                    %>

                    <div class="row accordion">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">

                            <div class="blog-ss-right mb-3">
                                <%--<p>abc</p>--%>
                                <div class="heading-2">
                                    <a href="/<%=currentLanguage %>/blog/<%=item.ShortUrl %>" title="<%=item.Name %>"><%=item.Name %></a>
                                </div>
                                <%for (int i = 0; i < news.Count(); i++)
                                    { %>
                                <%if (i == 0)
                                    { %>
                                <div class="item-left mb-3">
                                    <div class="image">
                                        <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>">
                                            <img src="/Uploads/thumb/<%=news[i].Avatar %>" alt="<%=news[i].Title %>" /></a>
                                    </div>
                                    <h2 class="title">
                                        <a class="tit" href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>"><%=news[i].Title %></a>
                                    </h2>
                                    <div class="detail">
                                        <% =Utility.SubWordInString(news[i].Sapo.GetText(),40) %>
                                    </div>

                                </div>
                                <%} %>
                                <%if (i > 0)
                                    { %>
                                <div class="item-right d-flex">
                                    <div class="image">
                                        <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>">
                                            <img src="/Uploads/thumb/<%=news[i].Avatar %>" alt="<%=news[i].Title %>" /></a>
                                    </div>
                                    <div class="text">
                                        <h2 class="title">
                                            <a class="tit" href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>"><%=news[i].Title %></a>
                                        </h2>
                                        <div class="time">
                                            <%=UIHelper.GetOnlyCurentDate(news[i].CreatedDate,currentLanguageJavaCode) %>
                                        </div>
                                    </div>

                                </div>
                                <%} %>
                                <%} %>
                            </div>

                        </div>
                        <%} %>
                        <%if (item.SortOrder == 4)
                            {
                                var news = NewsBo.GetByZoneId(item.Id, currentLanguageJavaCode, 5).ToList();
                        %>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">

                            <div class="blog-ss-right mb-3">

                                <%--<p>abc</p>--%>
                                <div class="heading-2">
                                    <a href="/<%=currentLanguage %>/blog/<%=item.Url %>" title=""><%=item.Name %></a>
                                </div>
                                <%for (int i = 0; i < news.Count(); i++)
                                    { %>
                                <%if (i == 0)
                                    { %>
                                <div class="item-left mb-3">
                                    <div class="image">
                                        <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>">
                                            <img src="/Uploads/thumb/<%=news[i].Avatar %>" alt="<%=news[i].Title %>" /></a>
                                    </div>
                                    <h2 class="title">
                                        <a class="tit" href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>"><%=news[i].Title %></a>
                                    </h2>
                                    <div class="detail">
                                        <% =Utility.SubWordInString(news[i].Sapo.GetText(),40) %>
                                    </div>

                                </div>
                                <%} %>
                                <%if (i > 0)
                                    { %>
                                <div class="item-right d-flex">
                                    <div class="image">
                                        <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>">
                                            <img src="/Uploads/thumb/<%=news[i].Avatar %>" alt="<%=news[i].Title %>" /></a>
                                    </div>
                                    <div class="text">
                                        <h2 class="title">
                                            <a class="tit" href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>"><%=news[i].Title %></a>
                                        </h2>
                                        <div class="time">
                                            <%=UIHelper.GetOnlyCurentDate(news[i].CreatedDate,currentLanguageJavaCode) %>
                                        </div>
                                    </div>

                                </div>
                                <%} %>
                                <%} %>
                            </div>

                        </div>

                    </div>
                    <%} %>
                    <div class="blog-ss-2">
                        <%--<div class="heading">
                            <a href="blog-category.html" title="">Điểm đến</a>
                        </div>--%>
                        <%if (item.SortOrder >= 5)
                            {
                                var news = NewsBo.GetByZoneId(item.Id, currentLanguageJavaCode, 5).ToList();
                        %>
                        <div class="wp">
                            <div class="heading">
                                <a href="/<%=currentLanguage %>/blog/<%=item.Url %>" title="<%=item.Name %>"><%=item.Name %></a>
                            </div>
                            <div class="row">
                                <%for (int i = 0; i < news.Count(); i++)
                                    { %>
                                <%if (i == 0)
                                    { %>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="item-left">
                                        <div class="image">
                                            <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>">
                                                <img src="/Uploads/thumb/<%=news[i].Avatar %>" alt="" /></a>
                                        </div>
                                        <h2 class="title">
                                            <a class="tit" href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>"><%=news[i].Title %></a>
                                        </h2>
                                        <div class="detail">
                                            <%=Utility.SubWordInString(news[i].Sapo.GetText().ToString(),40) %>
                                        </div>

                                    </div>
                                </div>
                                <%} %>
                                <%if (i == 1)
                                    { %>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="item-right d-flex">
                                        <div class="image">
                                            <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>">
                                                <img src="/Uploads/thumb/<%=news[i].Avatar %>" alt=<%=news[i].Title %>"" /></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="title">
                                                <a class="tit" href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>"><%=news[i].Title %></a>
                                            </h2>
                                            <div class="time">
                                                <%=UIHelper.GetOnlyCurentDate(news[i].CreatedDate,currentLanguageJavaCode) %>
                                            </div>
                                        </div>

                                    </div>
                                    <%} %>
                                    <%if (i > 1 && i < news.Count() - 1)
                                        { %>
                                    <div class="item-right d-flex">
                                        <div class="image">
                                            <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>">
                                                <img src="/Uploads/thumb/<%=news[i].Avatar %>" alt="<%=news[i].Title %>" /></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="title">
                                                <a class="tit" href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                            </h2>
                                            <div class="time">
                                                <%=UIHelper.GetOnlyCurentDate(news[i].CreatedDate,currentLanguageJavaCode) %>
                                            </div>
                                        </div>

                                    </div>
                                    <%} %>
                                    <%if (i == news.Count() - 1)
                                        { %>
                                    <div class="item-right d-flex">
                                        <div class="image">
                                            <a href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>">
                                                <img src="/Uploads/thumb/<%=news[i].Avatar %>" alt="<%=news[i].Title %>" /></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="title">
                                                <a class="tit" href="/<%=currentLanguage %>/blog/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="<%=news[i].Title %>"><%=news[i].Title %></a>
                                            </h2>
                                            <div class="time">
                                                <%=UIHelper.GetOnlyCurentDate(news[i].CreatedDate,currentLanguageJavaCode) %>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <%} %>
                                <%} %>
                            </div>

                        </div>
                        <%} %>
                    </div>
                    <%} %>
                </div>


                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div class="blog-ss-right mb-3">
                        <div class="heading">
                            <div title=""><%=Language.MostRead %></div>
                        </div>
                        <%var topView = NewsBo.GetMostedViewNews(currentLanguageJavaCode);
                            for (int i = 0; i < topView.Count(); i++)
                            {%>

                        <%  

                            if (i == 0)
                            { %>

                        <div class="item-left mb-3">
                            <div class="image">
                                <a href="/<%=currentLanguage %>/blog/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="<%=topView[i].Title %>">
                                    <img src="/Uploads/thumb/<%=topView[i].Avatar %>" alt="<%=topView[i].Title %>" /></a>
                            </div>
                            <h2 class="title">
                                <a class="tit" href="/<%=currentLanguage %>/blog/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="<%=topView[i].Title %>"><%=topView[i].Title %></a>
                            </h2>
                            <div class="detail">
                                <%=Utility.SubWordInString(topView[i].Sapo.GetText(),40) %>
                            </div>

                        </div>
                        <%} %>
                        <%if (i > 0)
                            { %>
                        <div class="item-right d-flex">
                            <div class="image">
                                <a href="/<%=currentLanguage %>/blog/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="<%=topView[i].Title %>">
                                    <img src="/Uploads/thumb/<%=topView[i].Avatar %>" alt="<%=topView[i].Title %>" /></a>
                            </div>
                            <div class="text">
                                <h2 class="title">
                                    <a class="tit" href="/<%=currentLanguage %>/blog/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="<%=topView[i].Title %>"><%=topView[i].Title %></a>
                                </h2>
                                <div class="time">
                                    <%=UIHelper.GetOnlyCurentDate(topView[i].CreatedDate,currentLanguageJavaCode) %>
                                </div>
                            </div>

                        </div>
                        <%} %>
                        <%} %>
                    </div>

                </div>
            </div>
        </div>
    </section>

</asp:Content>

