//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Language {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Language() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Language", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Địa chỉ.
        /// </summary>
        internal static string Address {
            get {
                return ResourceManager.GetString("Address", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đặt lịch cho người khác.
        /// </summary>
        internal static string Another_Booking {
            get {
                return ResourceManager.GetString("Another Booking", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Trở về trang chủ.
        /// </summary>
        internal static string BackHome {
            get {
                return ResourceManager.GetString("BackHome", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gọi ngay.
        /// </summary>
        internal static string Call {
            get {
                return ResourceManager.GetString("Call", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Liên hệ.
        /// </summary>
        internal static string Contact {
            get {
                return ResourceManager.GetString("Contact", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hướng dẫn.
        /// </summary>
        internal static string CustomerBanner {
            get {
                return ResourceManager.GetString("CustomerBanner", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ngày tháng năm sinh.
        /// </summary>
        internal static string Date_Of_Birth {
            get {
                return ResourceManager.GetString("Date Of Birth", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đội ngũ bác sĩ.
        /// </summary>
        internal static string Doctor_Team {
            get {
                return ResourceManager.GetString("Doctor Team", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ưu đãi.
        /// </summary>
        internal static string Event {
            get {
                return ResourceManager.GetString("Event", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Họ và tên.
        /// </summary>
        internal static string Full_Name {
            get {
                return ResourceManager.GetString("Full Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thư viện ảnh.
        /// </summary>
        internal static string Gallery {
            get {
                return ResourceManager.GetString("Gallery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bài viết này có hữu ích cho bạn không?.
        /// </summary>
        internal static string Helpful {
            get {
                return ResourceManager.GetString("Helpful", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Trang chủ.
        /// </summary>
        internal static string Home {
            get {
                return ResourceManager.GetString("Home", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ngày khám.
        /// </summary>
        internal static string HospitalDate {
            get {
                return ResourceManager.GetString("HospitalDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hotline.
        /// </summary>
        internal static string Hotline {
            get {
                return ResourceManager.GetString("Hotline", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Có thể bạn quan tâm.
        /// </summary>
        internal static string Interested {
            get {
                return ResourceManager.GetString("Interested", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ngôn ngữ.
        /// </summary>
        internal static string Lang {
            get {
                return ResourceManager.GetString("Lang", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thích.
        /// </summary>
        internal static string Like {
            get {
                return ResourceManager.GetString("Like", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tỉnh / Thành phố.
        /// </summary>
        internal static string Location {
            get {
                return ResourceManager.GetString("Location", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ngày khám bệnh.
        /// </summary>
        internal static string Medical_day {
            get {
                return ResourceManager.GetString("Medical day", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chat với chúng tôi.
        /// </summary>
        internal static string Message_for_us {
            get {
                return ResourceManager.GetString("Message for us", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Xem thêm.
        /// </summary>
        internal static string More {
            get {
                return ResourceManager.GetString("More", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vị trí tuyển dụng mới nhất.
        /// </summary>
        internal static string Newest_Recruiment {
            get {
                return ResourceManager.GetString("Newest Recruiment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lời nhắn.
        /// </summary>
        internal static string Note {
            get {
                return ResourceManager.GetString("Note", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nội dung lời nhắn.
        /// </summary>
        internal static string Note_Detail {
            get {
                return ResourceManager.GetString("Note Detail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đội ngũ bác sĩ.
        /// </summary>
        internal static string Our_Doctor {
            get {
                return ResourceManager.GetString("Our Doctor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Trang.
        /// </summary>
        internal static string Page {
            get {
                return ResourceManager.GetString("Page", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Về đầu trang.
        /// </summary>
        internal static string Page_Up {
            get {
                return ResourceManager.GetString("Page Up", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Số điện thoại.
        /// </summary>
        internal static string Phone_Number {
            get {
                return ResourceManager.GetString("Phone Number", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Trụ sở.
        /// </summary>
        internal static string Place {
            get {
                return ResourceManager.GetString("Place", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tuyển dụng.
        /// </summary>
        internal static string Recruitment {
            get {
                return ResourceManager.GetString("Recruitment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đăng ký tư vấn.
        /// </summary>
        internal static string Register {
            get {
                return ResourceManager.GetString("Register", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đăng ký khám.
        /// </summary>
        internal static string RegisterBanner {
            get {
                return ResourceManager.GetString("RegisterBanner", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tìm kiếm.
        /// </summary>
        internal static string Search {
            get {
                return ResourceManager.GetString("Search", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gửi.
        /// </summary>
        internal static string Send {
            get {
                return ResourceManager.GetString("Send", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chia sẻ.
        /// </summary>
        internal static string Share {
            get {
                return ResourceManager.GetString("Share", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chủ đề.
        /// </summary>
        internal static string Tag {
            get {
                return ResourceManager.GetString("Tag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cám ơn quý khách đã sử dụng dịch vụ của Hoàng Long.
        /// </summary>
        internal static string ThankForCustomer {
            get {
                return ResourceManager.GetString("ThankForCustomer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Khách hàng.
        /// </summary>
        internal static string TutorialBanner {
            get {
                return ResourceManager.GetString("TutorialBanner", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thời gian làm việc.
        /// </summary>
        internal static string Working_Time {
            get {
                return ResourceManager.GetString("Working Time", resourceCulture);
            }
        }
    }
}
