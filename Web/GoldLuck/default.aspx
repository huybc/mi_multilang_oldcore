﻿



<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/GoldHome.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="GoldLuck._default1" %>

<%@ Import Namespace="GoldLuck.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
     <div class="  bg-fff">
        <div class="container px-0 "> 
            <div class="row no-gutters"> 
                <div class="col-lg-12 col-md-12  col-sm-12 col-12 px-xl-3">
                    <div class="bd-example">
                        <div id="slide-home" class="carousel slide" data-ride="carousel">
                          <ol class="carousel-indicators">
                            <li data-target="#slide-home" data-slide-to="0" class="active"></li>
                            <li data-target="#slide-home" data-slide-to="1"></li>
                            <li data-target="#slide-home" data-slide-to="2"></li>
                          </ol>
                          <div class="carousel-inner">
                            <div class="carousel-item active">
                              <img src="images/change/slide-home-1.jpg" class="d-block w-100" alt="...">
                              <div class="carousel-caption ">
                                  <div class="lp-3">
                                    SUMMER COLLECTION
                                  </div>
                                <h2>Nội thất mạ vàng</h2>
                                <p>The cold weather has arrived. It’s time to sit by the fire and let your imagination go dressed in wool sweaters, warm trousers and leotards.</p>
                                <a class="btn btn-outline-light text-uppercase px-5 mb-3" href="">
                                    Xem thêm
                                </a>
                              </div>
                            </div>
                            <div class="carousel-item">
                              <img src="images/change/slide-home-1.jpg" class="d-block w-100" alt="...">
                              <div class="carousel-caption  ">
                                <div class="lp-3">
                                    SUMMER COLLECTION
                                  </div>
                                <h2>Nội thất mạ vàng</h2>
                                <p>The cold weather has arrived. It’s time to sit by the fire and let your imagination go dressed in wool sweaters, warm trousers and leotards.</p>
                                <a class="btn btn-outline-light text-uppercase px-5 mb-3" href="">
                                    Xem thêm
                                </a>
                              </div>
                            </div>
                            <div class="carousel-item">
                              <img src="images/change/slide-home-1.jpg" class="d-block w-100" alt="...">
                              <div class="carousel-caption ">
                                <div class="lp-3">
                                    SUMMER COLLECTION
                                  </div>
                                <h2>Nội thất mạ vàng</h2>
                                <p>The cold weather has arrived. It’s time to sit by the fire and let your imagination go dressed in wool sweaters, warm trousers and leotards.</p>
                                <a class="btn btn-outline-light text-uppercase px-5 mb-3" href="">
                                    Xem thêm
                                </a>
                              </div>
                            </div>
                          </div>
                          
                        </div>
                      </div>

                </div>
            </div> 
        </div>
    </div>

    <!--Sp mới-->
    <section class="list-product-home py-3 py-lg-5 mb-xl-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5 col-12">
                    <h2 class="heading-home">SẢN PHẨM MỚI</h2>
                    <div class="color-52575C text-center mb-4">
                        Autem neglegentur in duo, ex aperiam fabulas mei, exerci menandri explicari ut mei. Eam cibo et
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag">
                                HOT
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag bg-C6832B">
                                -10%
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag bg-C6832B">
                                -10%
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag bg-C6832B">
                                -10%
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag">
                                HOT
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag bg-C6832B">
                                -10%
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag">
                                HOT
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag bg-C6832B">
                                -10%
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!--Banner-->
    <section class="banner-home">
        <div class="container">
            <a href="">
                <img src="images/change/banner-1.jpg" class="img-fluid w-100"/>
            </a>
        </div>
    </section>

     <!--Sp yêu thích-->
     <section class="list-product-home py-3 py-lg-4 ">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5 col-12">
                    <h2 class="heading-home">SẢN phẩm Yêu thích</h2>
                    <div class="color-52575C text-center mb-4">
                        Autem neglegentur in duo, ex aperiam fabulas mei, exerci menandri explicari ut mei. Eam cibo et
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag">
                                HOT
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag bg-C6832B">
                                -10%
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag bg-C6832B">
                                -10%
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag bg-C6832B">
                                -10%
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag">
                                HOT
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag bg-C6832B">
                                -10%
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag">
                                HOT
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag bg-C6832B">
                                -10%
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="text-center">
                <a href="" role="button" class="btn btn-outline-viewmore  px-5">Xem thêm +</a>
            </div>
        </div>
    </section>
    <!--banner category-->
    <section class="banner-cate py-3">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="img-thumb-banner">
                        <a href="">
                            <img src="images/change/img-thumb-1.jpg" class="img-fluid" alt=""/>
                        </a>
                        <div class="text">
                            <div class="text-uppercase ">
                                Fall/Winter 2020 Collection
                            </div>
                            <div class="heading">
                              <a href="">Nội thất Mạ vàng</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="img-thumb-banner">
                        <a href="">
                            <img src="images/change/img-thumb-2.jpg" class="img-fluid" alt=""/>
                        </a>
                        <div class="text">
                            <div class="text-uppercase ">
                                Fall/Winter 2020 Collection
                            </div>
                            <div class="heading">
                              <a href="">Quà tặng Cao cấp</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--Sp theo yc-->
    <section class="list-product-home py-3 py-lg-4  ">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5 col-12">
                    <h2 class="heading-home">SẢN phẩm theo yêu cầu</h2>
                    <div class="color-52575C text-center mb-4">
                        Autem neglegentur in duo, ex aperiam fabulas mei, exerci menandri explicari ut mei. Eam cibo et
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag">
                                HOT
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag bg-C6832B">
                                -10%
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag bg-C6832B">
                                -10%
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                            <div class="tag bg-C6832B">
                                -10%
                            </div>
                            <div class="overlay">

                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div> 

            </div>
            <div class="text-center">
                <a href="" role="button" class="btn btn-outline-viewmore  px-5">Xem thêm +</a>
            </div>
        </div>
    </section>
    <!--Tin tức-->
    <section class="list-product-home py-3 py-lg-4  bg-f2f2f2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5 col-12">
                    <h2 class="heading-home">Tin tức</h2>
                    <div class="color-52575C text-center mb-4">
                        Autem neglegentur in duo, ex aperiam fabulas mei, exerci menandri explicari ut mei. Eam cibo et
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="item-news">
                        <div class="image position-relative">
                            <a href="tour-detail.html"><img src="images/change/img-news-01.jpg" class="img-fluid w-100" alt=""></a>
                            <div class="tag">
                                <a href="" title="">10 - 02 - 2020</a>
                            </div>
                        </div>
                        <div class="py-4 px-3">
                            <h4 class="title">
                                <a href="tour-detail.html" title="">My Son Holy Land and Hoi An Ancient Town</a>
                            </h4> 
                            <div class="des">
                                Autem neglegentur in duo, ex aperiam fabulas mei, exerci menandri explicari uọc 
                                exerci menandri   
                                <a href="" class="color-C6832B text-underline ml-1 nowrap ">Đọc tiếp</a>
                            </div>
                            <div class="text-right">
                                
                            </div>
                        </div>
                        <div>

                        </div>
                    </div>
                </div>  
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="item-news">
                        <div class="image position-relative">
                            <a href="tour-detail.html"><img src="images/change/img-news-01.jpg" class="img-fluid w-100" alt=""></a>
                            <div class="tag">
                                <a href="" title="">10 - 02 - 2020</a>
                            </div>
                        </div>
                        <div class="py-4 px-3">
                            <h4 class="title">
                                <a href="tour-detail.html" title="">My Son Holy Land and Hoi An Ancient Town</a>
                            </h4> 
                            <div class="des">
                                Autem neglegentur in duo, ex aperiam fabulas mei, exerci menandri explicari uọc 
                                exerci menandri   
                                <a href="" class="color-C6832B text-underline ml-1 nowrap ">Đọc tiếp</a>
                            </div>
                            <div class="text-right">
                                
                            </div>
                        </div>
                        <div>

                        </div>
                    </div>
                </div>  
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="item-news">
                        <div class="image position-relative">
                            <a href="tour-detail.html"><img src="images/change/img-news-01.jpg" class="img-fluid w-100" alt=""></a>
                            <div class="tag">
                                <a href="" title="">10 - 02 - 2020</a>
                            </div>
                        </div>
                        <div class="py-4 px-3">
                            <h4 class="title">
                                <a href="tour-detail.html" title="">My Son Holy Land and Hoi An Ancient Town</a>
                            </h4> 
                            <div class="des">
                                Autem neglegentur in duo, ex aperiam fabulas mei, exerci menandri explicari uọc 
                                exerci menandri   
                                <a href="" class="color-C6832B text-underline ml-1 nowrap ">Đọc tiếp</a>
                            </div>
                            <div class="text-right">
                                
                            </div>
                        </div>
                        <div>

                        </div>
                    </div>
                </div>  
            </div>
             
        </div>
    </section>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
