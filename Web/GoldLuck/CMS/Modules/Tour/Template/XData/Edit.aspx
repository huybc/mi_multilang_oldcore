﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="GoldLuck.CMS.Modules.Tour.Template.XData.Edit" %>

<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<%@ Import Namespace="Mi.Common.ChannelConfig" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="GoldLuck.Core.Helper" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>

<style>
    .rooms { }
    .rooms .room { padding-left: 10px; margin-top: 20px; position: relative }
    .form-control.title { border: 1px solid #2196f3; }

    .gallery-upload-file .r-queue-item div.file_upload { width: 100px; height: 100px; display: block; position: absolute; top: 0px; left: 0px; cursor: pointer; overflow: hidden; opacity: 0; }
    .gallery-upload-file .r-queue-item div.added { opacity: 1; display: none; }
    .gallery-upload-file .r-queue-item i.fa-times { background: #cccccc none repeat scroll 0 0; border: 1px solid #ddd; border-radius: 11px; color: #b1253a; padding: 0 2px; position: absolute; right: 6px; top: 3px; z-index: 999; }
    .gallery-upload-file .r-queue-item, .gallery-upload-file ._library { position: relative; float: left; width: 100px; height: 100px; border-radius: 3px; border: 2px dashed #e1e1e1; background: #FFFFFF; margin: 10px; color: #666666; font-size: 12px; position: relative; text-align: center; overflow: hidden; }
    .gallery-upload-file .r-queue-item i.fa-picture-o, .gallery-upload-file ._library i.fa-folder-open { font-size: 50px; cursor: pointer; display: block; margin: 9px 14px 3px 12px; color: #cccccc; }
    .notice-upload-image { background: #dcebf5; border: 1px dashed #bdd0dc; padding: 8px 0px; text-align: center; color: #436c87; margin-bottom: 4px; font-size: 12px; }
    .gallery-upload-file .r-queue-item img { width: 100px; cursor: pointer; display: block; overflow: hidden; width: 100px; }
    .gallery-upload-file .r-queue-item:hover img { opacity: .4; }
    .r-queue-item._library > p { font-size: 11px; }
    #gallery-upload-file .r-queue-item .file_upload { z-index: 99; }
    #gallery-upload-file .r-queue-item .item { background: #fff none repeat scroll 0 0; height: 100%; width: 100%; z-index: 999; position: absolute; top: 0; left: 0; right: 0; }
</style>
<div class="tabbable" id="news-id">
    <div class=" _body" id="_body">
        <div class="col-lg-9" style="padding-left: 0;">
            <div id="zone-content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <fieldset class="content-group">
                            <legend class="text-semibold">
                                <i class="icon-file-text2 position-left"></i>
                                Thông tin dự án
                            </legend>
                        </fieldset>
                        <fieldset class="content-group">
                            <div class="form-group">
                                <label class="col-lg-2">Tiêu đề<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input autocomplete="off" type="text" name="basic" id="titletxt" class="form-control" value="<%=_obj.NewsInfo.Title %>" tabindex="1" required="required" placeholder="Tối ưu ở [60-100] ký tự.">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2">Url<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input autocomplete="off" type="text" name="basic" id="urltxt" class="form-control" required="required" value="<%=_obj.NewsInfo.Url %>" placeholder="Tùy chỉnh đường dẫn tin bài">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Mô tả<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <textarea id="Sapo" class="form-control" rows="3" placeholder="Nội dung ngắn gọn" tabindex="2"><%=_obj.NewsInfo.Sapo %></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2">Thông tin<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <div class="col-lg-6">
                                        <label class="control-label col-lg-12">Mã tour</label>
                                        <input autocomplete="off" type="text" name="basic" id="codeTxt" class="form-control" value="<%=_obj.NewsInfo.Code %>" placeholder="T0001" tabindex="1" required="required">
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="control-label col-lg-12">Thời gian</label>
                                        <input autocomplete="off" type="text" name="basic" id="subTitleTxt" placeholder="Ex: 2 ngày 3 đêm" class="form-control" value="<%=_obj.NewsInfo.SubTitle %>" tabindex="1" required="required">
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="control-label col-lg-12">Địa điểm</label>
                                        <select data-placeholder="Chọn địa điểm" class="select" id="locationDll" multiple="multiple" style="width: 100%" tabindex="4" data="<%=_obj.NewsInfo.Locations %>">
                                            <%
                                                var locations = ZoneBo.GetAllZoneWithTreeView(false, (int)ZoneType.Location).ToList().Where(it => it.Status == 1);
                                                foreach (var lo in locations)
                                                {
                                            %>
                                            <option value="<%=lo.Id %>" data-parent="<%=lo.ParentId %>"><%=lo.Name %></option>
                                            <% } %>
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="control-label col-lg-12">Phương tiện</label>
                                        <select data-placeholder="Phương tiện" class="select" id="TransfersDdl" multiple="multiple" tabindex="4" data="<%=_obj.NewsInfo.Transfers %>">
                                            <option value="fa-bus">Ô tô</option>
                                            <option value="fa-plane">Máy bay</option>
                                            <option value="fa-train">Tàu hỏa</option>
                                            <option value="fa-ship">Tàu thủy</option>
                                        </select>
                                        <%--<input autocomplete="off" type="text" name="basic" id="TransferTxt" class="form-control" value="<%=_obj.NewsInfo.Transfers %>" tabindex="1" required="required">--%>
                                    </div>

                                </div>
                            </div>

                            <!-- Input group -->
                            <div class="form-group">
                                <label class="control-label col-lg-2">Ảnh<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <div class="notice-upload-image">
                                        <p>
                                            <b style="font-size: 14px;">Đăng nhiều ảnh để công cụ tìm kiếm dễ thấy bạn hơn!</b><br />
                                            <i style="font-size: 11px; padding-top: 5px; color: #666;">Kéo ảnh lên vị trí đầu tiên để chọn làm Thumbnail.</i>
                                        </p>
                                    </div>
                                    <div class="row gallery-upload-file" id="gallery-upload-file" data='<%=_obj.NewsInfo.AvatarArray %>'>
                                        <div class="col-md-3 r-queue-item">
                                            <i class="fa fa-picture-o"></i>
                                            <p>Đăng ảnh</p>
                                            <div class="file_upload ui-state-default"></div>
                                        </div>
                                        <div class="col-md-3 r-queue-item">
                                            <i class="fa fa-picture-o"></i>
                                            <p>Đăng ảnh</p>
                                            <div class="file_upload ui-state-default"></div>
                                        </div>
                                        <div class="col-md-3 r-queue-item">
                                            <i class="fa fa-picture-o"></i>
                                            <p>Đăng ảnh</p>
                                            <div class="file_upload ui-state-default"></div>

                                        </div>
                                        <div class="col-md-3 r-queue-item">
                                            <i class="fa fa-picture-o"></i>
                                            <p>Đăng ảnh</p>
                                            <div class="file_upload ui-state-default"></div>

                                        </div>
                                        <div class="col-md-3 r-queue-item">
                                            <i class="fa fa-picture-o"></i>
                                            <p>Đăng ảnh</p>
                                            <div class="file_upload ui-state-default"></div>

                                        </div>
                                        <div class="col-md-3  _library">
                                            <i class="fa fa-folder-open"></i>
                                            <p>[Thư viện ảnh]</p>
                                        </div>

                                    </div>
                                    <%--<p style="padding: 20px 5px 10px 5px;">Đăng nhiều hình thật nhanh bằng cách kéo và thả hình vào khung này hoặc nhấn nút phía trên</p>--%>
                                </div>
                            </div>
                            <!-- /input group -->

                            <div class="form-group">
                                <label class="control-label col-lg-2">Chi tiết<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <%--<button type="button" id="add-media"><i class="fa fa-camera"></i>Thêm media</button>--%>
                                    <textarea id="contentCkeditor" class="editor" rows="8" style="height: 500px" tabindex="3"><%=_obj.NewsInfo.Body %></textarea>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="content-group">
                            <legend class="text-bold">Lịch khởi hành</legend>

                            <div class="form-group">

                                <div id="items">
                                    <%
                                        var items = TourDepartureBo.GetByTourId(_obj.NewsInfo.Id).ToArray();
                                        if (items.Any())
                                        {
                                            for (int i = 0; i < items.Count(); i++)
                                            {
                                    %>
                                    <div class="item">
                                        <i class="icon-bin"></i>
                                        <label class="control-label col-lg-2 text-right">#1</label>
                                        <div class="col-lg-10">
                                            <div class="col-lg-3">
                                                <label class="control-label col-lg-12">Từ ngày</label>
                                                <input autocomplete="off" type="text" name="basic start-date" value="<%= UIHelper.GetOnlyDate(items[i].StartDate) %>" class="form-control start-date" tabindex="1" required="required">
                                            </div>
                                            <div class="col-lg-3">
                                                <label class="control-label col-lg-12">Đến ngày</label>
                                                <input autocomplete="off" type="text" name="basic" class="form-control end-date" value="<%= UIHelper.GetOnlyDate(items[i].EndDate) %>" tabindex="1" required="required">
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="control-label col-lg-12">Giá</label>
                                                <input autocomplete="off" type="text" name="basic" class="form-control price input-price" value="<%= UIHelper.FormatCurrency("VND", items[i].Price,false) %>" tabindex="1" required="required">
                                            </div>
                                            <div class="col-lg-3">
                                                <label class="control-label col-lg-12">Giá trẻ em</label>
                                                <input autocomplete="off" type="text" name="basic" class="form-control child-price input-price" value="<%= UIHelper.FormatCurrency("VND", items[i].ChildPrice,false) %>" tabindex="1" required="required">
                                            </div>
                                            <div class="col-lg-3">
                                                <label class="control-label col-lg-12">Trạng thái</label>
                                                <select data-placeholder="Select your state" class="select ddl-status" style="width: 100%" tabindex="4" data="<%=items[i].Status %>">
                                                    <option value="">-- Chọn --</option>
                                                    <option value='1'>Còn chỗ</option>
                                                    <option value='2'>Hết chỗ</option>
                                                    <option value='3'>Liên hệ</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                    <% }
                                        }
                                        else
                                        { %>
                                    <div class="item">
                                        <label class="control-label col-lg-2 text-right">#1</label>
                                        <div class="col-lg-10">
                                            <div class="col-lg-3">
                                                <label class="control-label col-lg-12">Từ ngày</label>
                                                <input autocomplete="off" type="text" name="basic start-date" class="form-control start-date" tabindex="1" required="required">
                                            </div>
                                            <div class="col-lg-3">
                                                <label class="control-label col-lg-12">Đến ngày</label>
                                                <input autocomplete="off" type="text" name="basic" class="form-control end-date" tabindex="1" required="required">
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="control-label col-lg-12">Giá</label>
                                                <input autocomplete="off" type="text" name="basic" class="form-control price" value="" tabindex="1" required="required">
                                            </div>
                                            <div class="col-lg-3">
                                                <label class="control-label col-lg-12">Giá trẻ em</label>
                                                <input autocomplete="off" type="text" name="basic" class="form-control child-price" tabindex="1" required="required">
                                            </div>
                                            <div class="col-lg-3">
                                                <label class="control-label col-lg-12">Trạng thái</label>
                                                <select data-placeholder="Select your state" class="select ddl-status" style="width: 100%" tabindex="4" data="<%=_obj.NewsInfo.GroupId %>">
                                                    <option value="">-- Chọn --</option>
                                                    <option value='1'>Còn chỗ</option>
                                                    <option value='2'>Hết chỗ</option>
                                                    <option value='3'>Liên hệ</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>

                                    <% } %>
                                </div>
                                <label class="control-label col-lg-2">&nbsp;<span class="text-danger"></span></label>
                                <div class="col-lg-10">
                                    <div style="padding-top: 20px">
                                        <button type="button" class="btn btn-blue ibtn-xs" id="addItemBtn"><i class="icon-add position-left"></i>Thêm</button>
                                    </div>

                                </div>


                            </div>
                            <%--    <div class="form-group">
                            <label class="control-label col-lg-2">Tags<span class="text-danger"></span></label>
                            <div class="col-lg-10 tags">
                                <textarea id="Tagstext" data-type="<%=(int) EnumTagType.ForNews %>" preset="<%=GetTags((int)EnumTagType.ForNews) %>" placeholder="Tag: sửa chữa, thay thế, phụ tùng..." class="tag-editor-hidden-src"></textarea>
                            </div>
                        </div>--%>
                        </fieldset>
                        <fieldset class="content-group">
                            <legend class="text-bold">SEO <i class="icon-stats-growth"></i></legend>
                            <%--    <div class="form-group">
                            <label class="control-label col-lg-2">Url<span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <input autocomplete="off" type="text" name="basic" id="url" class="form-control" required="required" value="<%=_obj.NewsInfo.Url %>" placeholder="Đường dẫn tối ưu cho bài viết">
                            </div>
                        </div>--%>

                            <div class="form-group">
                                <label class="col-lg-2">Meta title<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input autocomplete="off" type="text" name="basic" id="metaTitletxt" class="form-control" value="<%=_obj.NewsInfo.TitleSeo %>" tabindex="1" required="required" placeholder="Khai báo từ khóa với công cụ tìm kiếm">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2">Meta Keyword<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input autocomplete="off" type="text" name="maximum_characters" id="metakeywordtxt" class="form-control" value="<%=_obj.NewsInfo.MetaKeyword %>" placeholder="Khai báo các từ khóa với bộ máy tìm kiếm.">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2">Meta Description<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <textarea name="maximum_characters" class="form-control" id="metadescriptiontxt" placeholder="Mô tả bài viết hiển thị trên công cụ tìm kiếm. Độ dài tối ưu độ dài khoảng [140-160] ký tự"><%=_obj.NewsInfo.MetaDescription %></textarea>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3" style="padding-right: 0">
            <div id="sidebar">
                <div id="sidebar-content">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="form-group fselect" style="display: inline-block; width: 100%">
                                <p>
                                    <label>Ngôn ngữ</label>
                                </p>
                                <select data-placeholder="Select your state" class="select" id="languageDdl" tabindex="4" data="<%=_obj.NewsInfo.LanguageCode %>">
                                    <%
                                        var languages = ConfigBo.GetAllLanguage();
                                        foreach (var lg in languages)
                                        {
                                    %>
                                    <option value='<%=lg.Code %>'><%=lg.Name %></option>
                                    <% } %>
                                </select>
                            </div>
                            <%-- <div class="col-lg-12 col-md-12 col-sm-12">--%>
                            <div class="form-group">
                                <label>Danh mục</label>
                                <select data-placeholder="Select your state" class="select"  id="_zoneddl" style="width: 100%" tabindex="4" data='<%=_obj.NewsInfo.ZoneIds %>'>
                                    <option value="">-- Chọn --</option>
                                    <asp:repeater runat="server" id="ZoneRpt">
                                            <ItemTemplate>
                                                <option value='<%#Eval("Id") %>' data-parent="<%#Eval("ParentId") %>"><%#Eval("Name") %></option>
                                            </ItemTemplate>
                                        </asp:repeater>
                                    <option value='4532' data-parent="0">Has#Tag</option>
                                </select>
                            </div>
                            <%-- <div class="form-group">
                                <label>Nhóm sản phẩm</label>
                                <select data-placeholder="Select your state" class="select" id="groupDdl" style="width: 100%" tabindex="4" data="<%=_obj.NewsInfo.GroupId %>">
                                    <option value="">-- Chọn --</option>
                                    <asp:repeater runat="server" id="groupRpt">
                                            <ItemTemplate>
                                                <option value='<%#Eval("Id") %>' data-parent="<%#Eval("ParentId") %>"><%#Eval("Name") %></option>
                                            </ItemTemplate>
                                        </asp:repeater>
                                    <option value='4532' data-parent="0">Has#Tag</option>
                                </select>
                            </div>--%>
                            <%-- <div class="form-group">
                                <label>Ngày đăng</label>
                                <input autocomplete="off" type="text" class="form-control" id="datetxt" placeholder="" tabindex="10" value="<%=String.Format("{0:dd/MM/yyyy HH:mm:ss tt}", _obj.DistributionDate) %>" data="<%=String.Format("{0:yyyy/MM/dd HH:mm:ss tt}", _obj.DistributionDate) %>">
                            </div>--%>
                            <div class="form-group text-left">
                                <label class="checkbox-inline checkbox-left">
                                    <input <%=_obj.NewsInfo.IsHot?"checked":"" %> type="checkbox" tabindex="8" id="hotcb">
                                    Tour hot
								
                                </label>
                                <label class="checkbox-inline checkbox-left" style="display: none">
                                    <input <%=_obj.NewsInfo.IsVideo?"checked":"" %> type="checkbox" tabindex="8" id="videocb">
                                    Tin video
								
                                </label>
                                <label class="checkbox-inline checkbox-left"  style="display: none">
                                    <input type="checkbox" tabindex="9" id="commentcb" <%=_obj.NewsInfo.IsAllowComment?"checked":"" %>>Đóng bình luận
								
                                </label>
                            </div>
                            <%-- <div class="form-group">
                                            <label>Tài liệu đính kèm</label>
                                            <a href="#">[Đính kèm]</a>

                                        </div>--%>
                            <div class="form-group" id="attack-thumb">
                                <div>
                                    <label>Ảnh đại diện</label>
                                </div>
                                <%--<a id="attack-thumb" href="#">
                                    <img data="<%=_obj.Avatar %>" src="<%= UIHelper.Thumb_W(70,_obj.Avatar) %>" width="70" /></a>--%>
                                <div class="avatar">
                                    <input accept="image/*" id="fileSingleupload" type="file" name="files[]" style="display: none" />
                                    <label for="fileSingleupload">
                                        <%if (_obj.NewsInfo.Id > 0)
                                            {%>
                                        <img data-img="<%=_obj.NewsInfo.Avatar %>" src="/<%=!string.IsNullOrEmpty(_obj.NewsInfo.Avatar)? CmsChannelConfiguration.GetAppSetting("StorageUrl")+"/"+_obj.NewsInfo.Avatar:"CMS/Themes/Images/no-thumbnai.png" %>" />
                                        <%}
                                            else
                                            {%>
                                        <img src="/CMS/Themes/Images/no-thumbnai.png" width="60" />
                                        <% } %>
                                    </label>
                                </div>
                            </div>

                            <div class="text-right" id="news-save">
                                <button type="button" status="<%=(int)NewsStatus.Temporary %>" class="btn btn-default ibtn-xs btn-primary"><i class="icon-floppy-disk position-left"></i>Lưu</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button type="button" status="<%=(int)NewsStatus.Published %>" class="btn btn-default ibtn-xs btn-primary"><i class="icon-paperplane position-left"></i>Đăng bài</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="button" class="btn btn-default ibtn-xs" id="IMSFullOverlayClose"><i class="icon-x position-left"></i>Đóng</button>
                            </div>
                            <%--</div>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

