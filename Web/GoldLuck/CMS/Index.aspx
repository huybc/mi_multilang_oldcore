﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS/Master/ICMS.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="GoldLuck.CMS.Index" %>

<%@ Import Namespace="GoldLuck.Core.Helper" %>
<%@ Import Namespace="GoldLuck.Core.Helper" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="HeaderCph">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css" />
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">--%>

</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MainCph">
    <div class="content" style="margin-top: 30px">
        <div class="row">
            <div class="col-md-4">
                <div class="card text-white bg-danger mb-3">
                    <div class="card-header text-center">
                        <h4>Tổng số</h4>
                    </div>
                    <div class="card-body text-center">
                        <h1 id="card-t-ct"></h1>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-white bg-primary mb-3">
                    <div class="card-header text-center">
                        <h4 id="card-1">Dịch vụ</h4>
                    </div>
                    <div class="card-body text-center">
                        <h1 id="card-1-ct"></h1>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-white bg-success mb-3">
                    <div class="card-header text-center">
                        <h4 id="card-2">Sản phẩm</h4>
                    </div>
                    <div class="card-body text-center">
                        <h1 id="card-2-ct"></h1>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-lg-12">
                <!-- Traffic sources -->
                <div class="panel panel-flat">
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-8">
                                <label>Chọn khoảng thời gian: </label>
                                <input type="text" id="thoi-gian-exprot" class="form-control" />
                            </div>
                            <div class="col-md-4">
                                <br />
                                <button class="btn btn-default" id="loc-thoi-gian" type="button">Lọc thời gian</button>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /traffic sources -->
            </div>


        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Thống kê đăng đặt lịch</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-8">
                                <label>Chọn năm: </label>
                                <select id="cbx_year_picker">
                                    <option value="2019">2020</option>
                                    <option value="2020" selected>2021</option>
                                    <option value="2021">2022</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-md-12" id="chart1-container">
                                    <canvas id="myChart" width="200" height="200"></canvas>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Thống kê đăng ký đặt tour</div>
                    <div class="panel-body">
                        <div class="row">
                            <%--<div class="col-md-8">
                                <label>Chọn khoảng thời gian: </label>
                                <input type="text" class="thoi-gian-search" class="form-control" />
                                
                            </div>
                            <div class="row">--%>
                            <div class="col-md-12" id="chart2-container">
                                <canvas id="myChart1" width="200" height="200"></canvas>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Thống kê khách theo tuổi</div>
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-12" id="chart3-container">
                                <canvas id="myChart2" width="200" height="200"></canvas>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Upload XML SiteMap</div>
                    <div class="panel-body">
                        <div class="row">
                            <label>Chọn file XML SiteMap (Có thể tạo tự động tại <a href="https://www.xml-sitemaps.com/">đây</a>)</label>
                            <input type="file" accept="text/xml" id="upload-xml-sitemap" />
                        </div>
                        <br />
                        <div class="row">
                            <button class="btn btn-success" type="button" id="btn-upload-sitemap">Upload file </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Export Excel lịch khám</div>
                    <div class="panel-body">

                        <div class="row">
                            <button type="button" class="btn btn-danger" id="btnExport">Xuất Excel</button>
                        </div>
                        <br />
                        <br />
                        <br />
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="FooterCph">
    <!-- Theme JS files -->
    <script type="text/javascript" src="/CMS/Themes/js/plugins/visualization/d3/d3.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/pickers/daterangepicker.js"></script>

    <script type="text/javascript" src="/CMS/Themes/js/core/app.js"></script>
    <%=UIHelper.GetConfigByName("DashbroadAnalytics") %>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#thoi-gian-exprot').daterangepicker({
                //singleDatePicker: false,
                //showDropdowns: true,
                minYear: 1901,
                //maxYear: parseInt(moment().format('YYYY'), 10),
                locale: {
                    "format": "DD/MM/YYYY"
                }
                //}, function (start, end, label) {
                //    var years = moment().diff(start, 'years');
                //    alert("You are " + years + " years old!");
            });
            $('.thoi-gian-search').daterangepicker({
                //singleDatePicker: false,
                //showDropdowns: true,
                minYear: 1901,
                //maxYear: parseInt(moment().format('YYYY'), 10),
                locale: {
                    "format": "DD/MM/YYYY"
                }
                //}, function (start, end, label) {
                //    var years = moment().diff(start, 'years');
                //    alert("You are " + years + " years old!");
            });
            var dl = [];
            var year = 2020;
            R.Post({
                params: {
                    year: year
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "chart-customer",
                success: function (res) {
                    if (res.Success) {
                        //alert(res.Data);
                        console.log(res.Data);
                        var result = res.Data;
                        var l = [];
                        var d = [];
                        for (var i = 0; i < result.length; i++) {
                            var string_label = 'T' + result[i].month + '-' + result[i].year;
                            l.push(string_label);
                            d.push(result[i].tong_so);
                        }

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var chart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                                labels: l,
                                datasets: [{
                                    label: "Lượt đăng ký khám",
                                    data: d,
                                    backgroundColor: ['rgba(255, 99, 132, 0.2)'],
                                    borderColor: ['rgba(255, 99, 132, 1)'],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    } else {
                        $.notify(res.Message, {
                            autoHideDelay: 3000, className: "error",
                            globalPosition: 'right top'
                        });

                    }
                }
            })
            R.Post({
                params: {

                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "chart-co-so",
                success: function (res) {
                    if (res.Success) {
                        //alert(res.Data);
                        console.log(res.Data);
                        var result = res.Data;
                        var l = [];
                        var d = [];
                        var total = 0;
                        var hl1 = 0;
                        var hl2 = 0;
                        for (var i = 0; i < result.length; i++) {

                            l.push(result[i].Name);
                            d.push(result[i].total_kh);
                            total += result[i].total_kh;
                            if (i == 0)
                                hl1 = result[i].total_kh
                            if (i == 1)
                                hl2 = result[i].total_kh
                        }
                        var backgroundColor = [
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ];
                        var borderColor = [
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ];
                        //Fill ra tong so
                        $('#card-t-ct').text(total);
                        $('#card-1-ct').text(hl1);
                        $('#card-2-ct').text(hl2);
                        var ctx = document.getElementById('myChart1').getContext('2d');
                        ctx.canvas.width = 300;
                        ctx.canvas.height = 300;
                        var chart = new Chart(ctx, {
                            type: 'pie',

                            data: {
                                labels: l,
                                datasets: [{
                                    label: "Số lượng bệnh nhân",
                                    data: d,
                                    backgroundColor: backgroundColor,
                                    borderColor: borderColor,
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    } else {
                        $.notify(res.Message, {
                            autoHideDelay: 3000, className: "error",
                            globalPosition: 'right top'
                        });

                    }
                }
            })
            R.Post({
                params: {

                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "chart-theo-tuoi",
                success: function (res) {
                    if (res.Success) {
                        //alert(res.Data);
                        console.log(res.Data);
                        var result = res.Data;
                        var l = ['0 đến 10', '11 đến 25', '26 đến 40', '41 đến 60', 'Ngoài 60'];
                        var d = [result.z1, result.z2, result.z3, result.z4, result.z5];

                        var backgroundColor = [
                            "#2ecc71",
                            "#3498db",
                            "#95a5a6",
                            "#9b59b6",
                            "#f1c40f",
                            "#e74c3c",
                            "#34495e"
                        ];
                        //var borderColor = [
                        //    'rgba(255, 206, 86, 1)',
                        //    'rgba(75, 192, 192, 1)',
                        //    'rgba(153, 102, 255, 1)',
                        //    'rgba(113, 102, 255, 1)',
                        //    'rgba(173, 102, 255, 1)',
                        //    'rgba(255, 159, 64, 1)'
                        //];
                        var ctx = document.getElementById('myChart2').getContext('2d');
                        ctx.canvas.width = 300;
                        ctx.canvas.height = 300;
                        var chart = new Chart(ctx, {
                            type: 'pie',

                            data: {
                                labels: l,
                                datasets: [{
                                    label: "Số lượng bệnh nhân",
                                    data: d,
                                    backgroundColor: backgroundColor,
                                    //borderColor: borderColor,
                                    //borderWidth: 1
                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    } else {
                        $.notify(res.Message, {
                            autoHideDelay: 3000, className: "error",
                            globalPosition: 'right top'
                        });

                    }
                }
            })
        });
    </script>
    <script>
        gapi.analytics.ready(function () {

            // Step 3: Authorize the user.


            gapi.analytics.auth.authorize({
                container: 'auth-button',
                clientid: '436774269610-duljhdnm0r6jcoiacvam3ee7sd2hbnr7.apps.googleusercontent.com'
            });

            // Step 4: Create the view selector.

            var viewSelector = new gapi.analytics.ViewSelector({
                container: 'view-selector'
            });

            // Step 5: Create the timeline chart.

            var timeline = new gapi.analytics.googleCharts.DataChart({
                reportType: 'ga',
                query: {
                    'dimensions': 'ga:date',
                    'metrics': 'ga:sessions',
                    'start-date': '30daysAgo',
                    'end-date': 'yesterday',
                },
                chart: {
                    type: 'LINE',
                    container: 'timeline'
                }
            });

            // Step 6: Hook up the components to work together.

            gapi.analytics.auth.on('success', function (response) {
                viewSelector.execute();
            });

            viewSelector.on('change', function (ids) {
                var newIds = {
                    query: {
                        ids: ids
                    }
                }
                timeline.set(newIds).execute();
            });
        });
    </script>
    <%--<script type="text/javascript" src="/CMS/Themes/js/pages/dashboard.js"></script>--%>
    <script>
        $("#btn-upload-sitemap").click(function (evt) {
            var fileUpload = $("#upload-xml-sitemap").get(0);
            var files = fileUpload.files;

            var data = new FormData();
            for (var i = 0; i < files.length; i++) {
                data.append(files[i].name, files[i]);
            }

            $.ajax({
                url: "/UploadSEOFile.ashx",
                type: "POST",
                data: data,
                contentType: false,
                processData: false,
                success: function (result) { alert(result); },
                error: function (err) {
                    alert(err.statusText)
                }
            });

            evt.preventDefault();
        });
        $('#btnExport').off('click').on('click', function () {
            var time = $('#thoi-gian-exprot').val().split('-');
            var start = moment(time[0].trim(), "DD/MM/YYYY").format("YYYY-MM-DD");
            var end = moment(time[1].trim(), "DD/MM/YYYY").format("YYYY-MM-DD");
            //alert(start + end);
            R.Post({
                params: {
                    start: start,
                    end: end
                },
                module: "customer",
                ashx: 'modulerequest.ashx',
                action: "search-export",
                success: function (res) {
                    if (res.Success) {
                        //alert(res.Data);
                        window.open(res.Data, "_blank");
                    } else {
                        $.notify(res.Message, {
                            autoHideDelay: 3000, className: "error",
                            globalPosition: 'right top'
                        });

                    }
                }
            })
        });
        $('#loc-thoi-gian').off('click').on('click', function () {
            //alert(1);
            //Reset ChartJS
            $('#myChart1').remove();
            $('#myChart2').remove();
            $('#chart2-container').append('<canvas id="myChart1" width="200" height="200"></canvas>')
            $('#chart3-container').append('<canvas id="myChart2" width="200" height="200"></canvas>')
            var time = $('#thoi-gian-exprot').val().split('-');
            var start = moment(time[0].trim(), "DD/MM/YYYY").format("YYYY-MM-DD");
            var end = moment(time[1].trim(), "DD/MM/YYYY").format("YYYY-MM-DD");
            R.Post({
                params: {
                    start: start,
                    end: end
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "chart-co-so",
                success: function (res) {
                    if (res.Success) {
                        //alert(res.Data);
                        console.log(res.Data);
                        var result = res.Data;
                        var l = [];
                        var d = [];
                        var total = 0;
                        var hl1 = 0;
                        var hl2 = 0;
                        for (var i = 0; i < result.length; i++) {

                            l.push(result[i].Name);
                            d.push(result[i].total_kh);
                            total += result[i].total_kh;
                            if (i == 0)
                                hl1 = result[i].total_kh
                            if (i == 1)
                                hl2 = result[i].total_kh
                        }
                        var backgroundColor = [
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ];
                        var borderColor = [
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ];
                        //Fill ra tong so
                        $('#card-t-ct').text(total);
                        $('#card-1-ct').text(hl1);
                        $('#card-2-ct').text(hl2);
                        var ctx = document.getElementById('myChart1').getContext('2d');
                        ctx.canvas.width = 300;
                        ctx.canvas.height = 300;
                        var chart = new Chart(ctx, {
                            type: 'pie',

                            data: {
                                labels: l,
                                datasets: [{
                                    label: "Số lượng bệnh nhân",
                                    data: d,
                                    backgroundColor: backgroundColor,
                                    borderColor: borderColor,
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    } else {
                        $.notify(res.Message, {
                            autoHideDelay: 3000, className: "error",
                            globalPosition: 'right top'
                        });

                    }
                }
            })
            R.Post({
                params: {
                    start: start,
                    end: end
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "chart-theo-tuoi",
                success: function (res) {
                    if (res.Success) {
                        //alert(res.Data);
                        console.log(res.Data);
                        var result = res.Data;
                        var l = ['0 đến 10', '11 đến 25', '26 đến 40', '41 đến 60', 'Ngoài 60'];
                        var d = [result.z1, result.z2, result.z3, result.z4, result.z5];

                        var backgroundColor = [
                            "#2ecc71",
                            "#3498db",
                            "#95a5a6",
                            "#9b59b6",
                            "#f1c40f",
                            "#e74c3c",
                            "#34495e"
                        ];
                        //var borderColor = [
                        //    'rgba(255, 206, 86, 1)',
                        //    'rgba(75, 192, 192, 1)',
                        //    'rgba(153, 102, 255, 1)',
                        //    'rgba(113, 102, 255, 1)',
                        //    'rgba(173, 102, 255, 1)',
                        //    'rgba(255, 159, 64, 1)'
                        //];
                        var ctx = document.getElementById('myChart2').getContext('2d');
                        ctx.canvas.width = 300;
                        ctx.canvas.height = 300;
                        var chart = new Chart(ctx, {
                            type: 'pie',

                            data: {
                                labels: l,
                                datasets: [{
                                    label: "Số lượng bệnh nhân",
                                    data: d,
                                    backgroundColor: backgroundColor,
                                    //borderColor: borderColor,
                                    //borderWidth: 1
                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    } else {
                        $.notify(res.Message, {
                            autoHideDelay: 3000, className: "error",
                            globalPosition: 'right top'
                        });

                    }
                }
            })
        });
        $('#cbx_year_picker').off('change').on('change', function () {
            var year = $(this).val();
            $('#myChart').remove();
            $('#chart1-container').append('<canvas id="myChart" width="200" height="200"></canvas>');
            R.Post({
                params: {
                    year: year
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "chart-customer",
                success: function (res) {
                    if (res.Success) {
                        //alert(res.Data);
                        console.log(res.Data);
                        var result = res.Data;
                        var l = [];
                        var d = [];
                        for (var i = 0; i < result.length; i++) {
                            var string_label = result[i].month + '-' + result[i].year;
                            l.push(string_label);
                            d.push(result[i].tong_so);
                        }

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var chart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                                labels: l,
                                datasets: [{
                                    label: "Lượt đăng ký khám",
                                    data: d,
                                    backgroundColor: ['rgba(255, 99, 132, 0.2)'],
                                    borderColor: ['rgba(255, 99, 132, 1)'],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    } else {
                        $.notify(res.Message, {
                            autoHideDelay: 3000, className: "error",
                            globalPosition: 'right top'
                        });

                    }
                }
            })
        })
    </script>
</asp:Content>


