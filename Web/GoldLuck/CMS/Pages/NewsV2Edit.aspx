﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS/Master/ICMS.Master" AutoEventWireup="true" CodeBehind="NewsV2Edit.aspx.cs" Inherits="GoldLuck.CMS.Pages.NewsV2Edit" %>

<%@ Import Namespace="GoldLuck.Core.Helper" %>
<%@ Import Namespace="Mi.Common.ChannelConfig" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Newtonsoft.Json" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderCph" runat="server">
    <%--<link href="/CMS/Themes/js/comfirm/jquery-confirm.min.css" rel="stylesheet" />
    <link href="/CMS/Themes/js/fselect/styles.css" rel="stylesheet" />
    <link href="/CMS/Modules/Zone/Styles/main.css" rel="stylesheet" />
    <link href="/CMS/Modules/News/Styles/main.css" rel="stylesheet" />--%>
    <link href="/CMS/Themes/js/comfirm/jquery-confirm.min.css" rel="stylesheet" />
    <link href="/CMS/Themes/js/fselect/styles.css" rel="stylesheet" />
    <link href="/CMS/Themes/js/ftag/ftag.css" rel="stylesheet" />
    <link href="/CMS/Modules/Zone/Styles/main.css" rel="stylesheet" />
    <link href="/CMS/Modules/News/Styles/main.css" rel="stylesheet" />
    <link href="/CMS/Themes/js/treegrid/jquery.treegrid.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainCph" runat="server">
    <%
        var id = int.Parse(Page.RouteData.Values["id"].ToString());
        var news_languages = new List<NewsWithLanguageEntity>();
        if (id > 0)
        {
            news_languages = NewsBo.GetNewsInLanguage(id, "all").ToList();

        }
        else
        {
            news_languages = null;
        }
    %>
    <%if (_obj.ExpertId != 0) 
        {%>  
    <div class="page-container">
        <div class="page-content">
            <!-- Main sidebar -->
            <div class="sidebar sidebar-main" id="cms-sidebar">
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <!-- Main -->
                            <li class="navigation-header"><span>Bài viết</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li class="active n-item" status="3"><a href="javascript:void(0)"><i class="icon-magazine position-left"></i><span>Bài đã duyệt</span></a></li>
                            <li status="2" class="n-item"><a href="javascript:void(0)"><i class="icon-file-presentation"></i>Bài chờ duyệt</a></li>
                            <li status="6" class="n-item"><a href="javascript:void(0)"><i class="icon-file-minus2"></i>Bài bị từ chối</a></li>
                            <li status="1" class="n-item"><a href="javascript:void(0)"><i class="icon-floppy-disk"></i>Bài lưu tạm</a></li>
                            <li status="5" class="n-item"><a href="javascript:void(0)"><i class="icon-file-download position-left"></i><span>Bài bị gỡ</span></a></li>
                            <li status="4" class="n-item"><a href="javascript:void(0)"><i class="icon-bin"></i>Bài bị xóa</a></li>

                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->
            </div>
            <!-- /main sidebar -->
            <!-- Main content -->

            <div class="content-wrapper" style="background: #fff;">
                <div class="col-lg-10">
                    <div id="zone-edit" zone-id='' zone-type="">

                        <div class="row">
                            <div class="panel-body">
                                <div class="tabbable nav-tabs-vertical nav-tabs-left" style="display: table; width: 100%">
                                    <ul class="nav nav-tabs nav-tabs-highlight" style="">
                                        <li class="active info-tab"><a href="#left-tab1" data-toggle="tab"><i class="icon-info3 position-left"></i>Thông tin</a></li>
                                        <li class="language-tab"><a href="#left-tab2" data-toggle="tab"><i class="icon-puzzle4 position-left"></i>Ngôn ngữ</a></li>
                                    </ul>

                                    <div class="tab-content" style="display: table-cell">
                                        <div class="tab-pane active has-padding fselect" id="left-tab1">
                                            <div class="col-lg-12">
                                                <div class="panel panel-flat">
                                                    <div class="panel-body">
                                                        <div id="icroll">
                                                            <fieldset class="content-group">
                                                                <legend class="text-semibold">
                                                                    <i class="icon-file-text2 position-left"></i>
                                                                    Đăng bài viết
                                                                </legend>
                                                            </fieldset>
                                                            <fieldset class="content-group">
                                                                <div class="form-group">
                                                                    <label class="col-lg-2">Tiêu đề<span class="text-danger">*</span></label>
                                                                    <div class="col-lg-10">
                                                                        <input type="text" name="basic" id="titletxt" class="form-control" value="<%=_obj.NewsInfo.Title %>" tabindex="1" required="required" placeholder="Tối ưu ở [60-100] ký tự.">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Chuyên mục</label>
                                                                    <select data-placeholder="Select your state" class="select" id="zoneddl" style="width: 100%" tabindex="4" data="<%=_obj.NewsInfo.ZoneIds %>">
                                                                        <option value="">- Chọn -</option>
                                                                        <asp:Repeater runat="server" ID="ZoneRpt">
                                                                            <ItemTemplate>
                                                                                <option <%#Eval("Id").ToString().Equals(_obj.NewsInfo.ZoneIds) ? "selected" : "" %> value='<%#Eval("Id") %>' data-parent="<%#Eval("ParentId") %>"><%#Eval("Name") %></option>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-lg-2">Url<span class="text-danger">*</span></label>
                                                                    <div class="col-lg-10">
                                                                        <input type="text" name="basic" id="Urltxt" value="<%=_obj.NewsInfo.Url %>" class="form-control" tabindex="1" required="required" placeholder="link bài viết.">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group text-right">
                                                                    <%--<label class="checkbox-inline checkbox-left">
                                        <input <%=_obj.NewsInfo.IsHot?"checked":"" %> type="checkbox" tabindex="8" id="hotcb">
                                        Nổi bật
								
                                    </label>--%>
                                                                    <label class="checkbox-inline checkbox-left">
                                                                        <input <%=_obj.NewsInfo.IsVideo ? "checked" : "" %> type="checkbox" tabindex="8" id="videocb">
                                                                        Tin video
								
                                                                    </label>
                                                                    <label class="checkbox-inline checkbox-left">
                                                                        <input <%=_obj.NewsInfo.IsHot ? "checked" : "" %> type="checkbox" tabindex="8" id="hotcb">
                                                                        Tin hot
                                                                    </label>

                                                                </div>
                                                                <%--hiển thị trang home--%>
                                                                <div class="form-group">
                                                                    <label>hiển thị trang chủ</label>
                                                                    <select data-placeholder="Select your state" class="select" id="zoneddl2" style="width: 100%" tabindex="4" data="<%=_obj.NewsInfo.ZoneIds %>">
                                                                        <option value="">- Chọn -</option>
                                                                        <asp:Repeater runat="server" ID="ZoneRpt2">
                                                                            <ItemTemplate>
                                                                                <option <%#Eval("Id").ToString().Equals(_obj.NewsInfo.ZoneIds) ? "selected" : "" %> value='<%#Eval("Id") %>' data-parent="<%#Eval("ParentId") %>"><%#Eval("Name") %></option>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </select>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Ảnh đính kèm:</label>
                                                                </div>
                                                                <div class="form-group" id="attack-news-thumb">
                                                                    <div class="row">
                                                                        <div class="col-lg-12 col-md-12">
                                                                            <div class="avatar2">
                                                                                <div class="col-md-3">
                                                                                    <label>Avatar vuông</label>
                                                                                </div>
                                                                                <input accept="image/*" id="fileSingleupload" type="file" name="files[]" style="display: none" />
                                                                                <label for="fileSingleupload">
                                                                                    <%if (_obj.NewsInfo.Id > 0)
                                                                                        {%>
                                                                                    <img class="parseImg" width="250" data-img="<%=_obj.NewsInfo.Avatar %>" src="/<%=!string.IsNullOrEmpty(_obj.NewsInfo.Avatar)? CmsChannelConfiguration.GetAppSetting("StorageUrl")+"/"+_obj.NewsInfo.Avatar:"CMS/Themes/Images/no-thumbnai.png" %>" />
                                                                                    <%}
                                                                                        else
                                                                                        {%>
                                                                                    <img class="parseImg" src="/CMS/Themes/Images/no-thumbnai.png" width="250" />
                                                                                    <% } %>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-12 col-md-12">
                                                                            <div class="avatar3">
                                                                                <div class="col-md-3">
                                                                                    <label>Avatar tròn</label>
                                                                                </div>
                                                                                <input accept="image/*" id="fileSingleupload-avatar-tron" type="file" name="files[]" style="display: none" />
                                                                                <label for="fileSingleupload-avatar-tron">
                                                                                    <%if (_obj.NewsInfo.Id > 0)
                                                                                        {%>
                                                                                    <img class="parseImg" width="250" data-img="<%=_obj.NewsInfo.AvatarTron %>" src="/<%=!string.IsNullOrEmpty(_obj.NewsInfo.AvatarTron)? CmsChannelConfiguration.GetAppSetting("StorageUrl")+"/"+_obj.NewsInfo.AvatarTron:"CMS/Themes/Images/no-thumbnai.png" %>" />
                                                                                    <%}
                                                                                        else
                                                                                        {%>
                                                                                    <img class="parseImg" src="/CMS/Themes/Images/no-thumbnai.png" width="250" />
                                                                                    <% } %>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-12 col-md-12">
                                                                            <div class="banner">
                                                                                <div class="col-md-3">
                                                                                    <label>Banner</label>
                                                                                </div>
                                                                                <input accept="image/*" id="fileSingleupload-banner" type="file" name="files[]" style="display: none" />
                                                                                <label for="fileSingleupload-banner">
                                                                                    <%if (_obj.NewsInfo.Id > 0)
                                                                                        {%>
                                                                                    <img class="parseImg" width="250" data-img="<%=_obj.NewsInfo.Banner %>" src="/<%=!string.IsNullOrEmpty(_obj.NewsInfo.Banner)? CmsChannelConfiguration.GetAppSetting("StorageUrl")+"/"+_obj.NewsInfo.Banner:"CMS/Themes/Images/no-thumbnai.png" %>" />
                                                                                    <%}
                                                                                        else
                                                                                        {%>
                                                                                    <img class="parseImg" src="/CMS/Themes/Images/no-thumbnai.png" width="250" />
                                                                                    <% } %>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>






                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-lg-2">Tags<span class="text-danger"></span></label>
                                                                    <div class="col-lg-10 tags">
                                                                        <input type="text" class="form-control" id="Tagstext" />
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                            <%--<fieldset class="content-group">
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <button class="btn btn-success" type="button" id="add_bac_si" data-toggle="collapse" data-target="#extras-bac-si" aria-expanded="false" aria-controls="collapseExample">Extras Bác sĩ</button>
                                                                    </div>
                                                                </div>
                                                                <br />

                                                                <div class="row collapse" id="extras-bac-si">
                                                                    <div class="col-md-3">
                                                                        <input type="text" class="form-control" id="txtGia_HocVi" placeholder="Học vị" />
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <input type="text" class="form-control" id="txtGia_ChuyenKhoa" placeholder="Chuyên khoa" />
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <input type="text" class="form-control" id="txtGia_SubChuyenKhoa" placeholder="Chuyên môn" />
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <input type="text" class="form-control" id="txtGia_LichKham" placeholder="Lịch khám" />
                                                                    </div>
                                                                </div>
                                                            </fieldset>--%>
                                                            <fieldset class="content-group">
                                                                <legend class="text-bold">SEO <i class="icon-stats-growth"></i></legend>
                                                                <div class="form-group">
                                                                    <label class="col-lg-2">Tiêu đề SEO<span class="text-danger">*</span></label>
                                                                    <div class="col-lg-10">
                                                                        <input type="text" name="basic" id="titleSeotxt" class="form-control" required="required" value="<%=_obj.NewsInfo.TitleSeo %>" placeholder="Tối ưu ở [60-100] ký tự.">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-lg-2">Meta Keyword<span class="text-danger">*</span></label>
                                                                    <div class="col-lg-10">
                                                                        <input type="text" name="maximum_characters" id="metakeywordtxt" class="form-control" value="<%=_obj.NewsInfo.MetaKeyword %>" placeholder="Khai báo các từ khóa với bộ máy tìm kiếm.">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-lg-2">Meta Description<span class="text-danger">*</span></label>
                                                                    <div class="col-lg-10">
                                                                        <textarea name="maximum_characters" class="form-control" id="metadescriptiontxt" placeholder="Mô tả khi tìm kiếm google tối ưu độ dài khoảng [140-160] ký tự"><%=_obj.NewsInfo.MetaDescription %></textarea>
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                            <div class="text-right">

                                                                <button type="button" status="<% =_obj.NewsInfo.Status%>" class="btn btn-default ibtn-xs btn-primary" id="news-save-temp"><i class="icon-floppy-disk position-left"></i>Lưu</button>&nbsp;&nbsp;
                                                                 <button type="button" class="btn btn-default ibtn-xs btn-primary" id="news-save-request"><i class="icon-paperplane position-left"></i>Xuất bản</button>&nbsp;&nbsp;
                                                                <button type="button" class="btn btn-default ibtn-xs" id="IMSFullOverlayClose"><i class="icon-x position-left"></i>Đóng</button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="tab-pane has-padding fselect" id="left-tab2">
                                            <div class="form-group fselect" style="display: inline-block; width: 100%">
                                                <p>
                                                    <label>Ngôn ngữ</label>
                                                </p>
                                                <%//Giai quyet: Mac dinh tieng Viet la dau tien, bidn ra bai tieng Viet truoc roi sua sau %>
                                                <%var vn_langs = new NewsWithLanguageEntity();
                                                    if (news_languages != null)
                                                    {%>
                                                <% vn_langs = news_languages.Where(r => r.LanguageCode.Equals("vi-VN")).FirstOrDefault(); %>
                                                <%} %>

                                                <select data-placeholder="Select your state" class="select" id="languageDdl" tabindex="4" data="">
                                                    <%
                                                        var languages = ConfigBo.GetAllLanguage();
                                                        foreach (var lg in languages)
                                                        {
                                                    %>
                                                    <option value='<%=lg.Code.TrimEnd() %>' <%=lg.Code.Equals("vi-VN") ? "selected" : "" %>><%=lg.Name %></option>
                                                    <% } %>
                                                </select>
                                            </div>
                                            <%if (vn_langs != null)
                                                { %>
                                            <div class="form-group">
                                                <label>Tên <span class="text-danger">*</span></label>
                                                <input type="text" autocomplete="off" name="basic" id="nameLanguageTxt" class="form-control" value="<%=vn_langs.Title %>" tabindex="1" required="required" placeholder="Tên danh mục theo ngôn ngữ">
                                            </div>
                                            <div class="form-group">
                                                <label>Mô tả:</label>
                                                <textarea rows="2" cols="5" class="form-control" id="contentTxt" placeholder="Enter your message here"><%=vn_langs.Sapo %></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Chi tiết:</label>
                                                <textarea rows="2" cols="5" class="form-control" id="detailCkeditor" placeholder="Enter your message here"><%=vn_langs.Body %></textarea>
                                            </div>
                                            <fieldset class="content-group">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <button class="btn btn-success" type="button" id="add_bac_si" data-toggle="collapse" data-target="#extras-bac-si" aria-expanded="false" aria-controls="collapseExample">Giá Spa/Yoga/sản phẩm</button>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="btn btn-success" type="button" id="add_Gia-Phong" data-toggle="collapse" data-target="#extras-bac-si1" aria-expanded="false" aria-controls="collapseExample">Giá Phòng</button>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="btn btn-success" type="button" id="add_Gia-tour" data-toggle="collapse" data-target="#extras-bac-si2" aria-expanded="false" aria-controls="collapseExample">Giá tour</button>
                                                    </div>
                                                </div>
                                                <br />
                                                <%var Gia = new DonGiaEntity(); %>
                                                <%if (!string.IsNullOrEmpty(vn_langs.Extras))
                                                    { %>
                                                <%Gia = Newtonsoft.Json.JsonConvert.DeserializeObject<DonGiaEntity>(vn_langs.Extras); %>
                                                <%} %>
                                                <div class="row collapse" id="extras-bac-si">
                                                    <div class="col-md-3">
                                                        <input type="number" class="form-control" id="txtGia_HocVi" value="<%=Gia.HocVi %>" placeholder="Giá gốc" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="number" class="form-control" id="txtGia_KhuyenMai" value="<%=Gia.KhuyenMai %>" placeholder="Giá khuyến mãi" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="number" class="form-control" id="txtGia_SKU" value="<%=Gia.KhuyenMai %>" placeholder="SKU (Với sản phẩm)" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="number" class="form-control" id="txtGia_Category" value="<%=Gia.KhuyenMai %>" placeholder="Category (Với sản phẩm)" />
                                                    </div>
                                                </div>

                                                <div class="row collapse" id="extras-bac-si2">
                                                    <div class="col-md-3">
                                                        <input type="number" class="form-control" id="txtGia_tour" value="<%=Gia.HocVitour%>" placeholder="Gia tour" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="txtTG_Ngay" value="<%=Gia.SoNgay %>" placeholder="Thời gian tour" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="txtTG_Location" value="<%=Gia.Location %>" placeholder="Điểm đến" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="txtTG_Vehicle" value="<%=Gia.Vehicle %>" placeholder="Phương tiện" />
                                                    </div>
                                                </div>

                                                <div class="row collapse" id="extras-bac-si1">
                                                    <div class="col-md-3">
                                                        <input type="number" class="form-control" id="txtGia_HocViPhong" value="<%=Gia.HocViPhong %>" placeholder="Giá gốc" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="number" class="form-control" id="txtGia_KhuyenMaiPhong" value="<%=Gia.KhuyenMaiPhong %>" placeholder="Giá khuyến mãi" />
                                                    </div>
                                                    <br />
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="txtGia_DichVu1" value="<%=Gia.DichVu1 %>" placeholder="Dịch vụ đi kèm 1" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="txtGia_DichVu2" value="<%=Gia.DichVu2 %>" placeholder="Dịch vụ đi kèm 2" />
                                                    </div>
                                                    <br />
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="txtGia_SoPhong" value="<%=Gia.SoPhong %>" placeholder="Số phòng Phòng" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="txtGia_SoNguoi" value="<%=Gia.SoNguoi %>" placeholder="Số người" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="txtGia_DichVu3" value="<%=Gia.DichVu3 %>" placeholder="Dịch vụ đi kèm 3" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="txtGia_DichVu4" value="<%=Gia.DichVu4 %>" placeholder="Dịch vụ đi kèm 4" />
                                                    </div>
                                                </div>
                                            </fieldset>

                                            <%--<div class="form-group">
                                                <label>Tags<span class="text-danger">*</span></label>
                                                <input type="text" autocomplete="off" name="basic" id="tagsLangTxt" class="form-control" value="<%=vn_langs.Tags %>" tabindex="1" required="required" placeholder="Tag">
                                            </div>

                                            <div class="form-group">
                                                <label>Meta title<span class="text-danger">*</span></label>
                                                <input type="text" autocomplete="off" name="basic" id="metaTitletxt" class="form-control" value="<%=vn_langs.MetaTitle %>" tabindex="1" required="required" placeholder="Khai báo từ khóa vs công cụ tìm kiếm">
                                            </div>
                                            <div class="form-group">
                                                <label>Meta keyword:</label>
                                                <input class="form-control" id="metaKeywordTxt" placeholder="Metakeyword, từ khóa đểo seo danh mục" value="<%=vn_langs.MetaKeyword %>" />
                                            </div>
                                            <div class="form-group">
                                                <label>Meta description:</label>
                                                <textarea rows="2" cols="5" class="form-control" id="metaDescriptionTxt" placeholder="MetaDescription.. mô tả trang."><%=vn_langs.MetaDescription %></textarea>
                                            </div>--%>
                                            <%}
                                                else
                                                { %>
                                            <div class="form-group">
                                                <label>Tên <span class="text-danger">*</span></label>
                                                <input type="text" autocomplete="off" name="basic" id="nameLanguageTxt" class="form-control" value="" tabindex="1" required="required" placeholder="Tên danh mục theo ngôn ngữ">
                                            </div>
                                            <div class="form-group">
                                                <label>Mô tả:</label>
                                                <textarea rows="2" cols="5" class="form-control" id="contentTxt" placeholder="Enter your message here"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Chi tiết:</label>
                                                <textarea rows="2" cols="5" class="form-control" id="detailCkeditor" placeholder="Enter your message here"></textarea>
                                            </div>
                                            <fieldset class="content-group">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <button class="btn btn-success" type="button" id="add_bac_si" data-toggle="collapse" data-target="#extras-bac-si" aria-expanded="false" aria-controls="collapseExample">Giá Spa/Yoga/sản phẩm</button>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="btn btn-success" type="button" id="add_Gia-Phong" data-toggle="collapse" data-target="#extras-bac-si1" aria-expanded="false" aria-controls="collapseExample">Giá Phòng</button>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="btn btn-success" type="button" id="add_Gia-tour" data-toggle="collapse" data-target="#extras-bac-si2" aria-expanded="false" aria-controls="collapseExample">Giá tour</button>
                                                    </div>
                                                </div>
                                                <br />
                                                <%var Gia = new DonGiaEntity(); %>

                                                <div class="row collapse" id="extras-bac-si">
                                                    <div class="col-md-3">
                                                        <input type="number" class="form-control" id="txtGia_HocVi" value="<%=Gia.HocVi%>" placeholder="Giá Gốc" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="number" class="form-control" id="txtGia_KhuyenMai" value="<%=Gia.KhuyenMai %>" placeholder="Khuyến mãi" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="number" class="form-control" id="txtGia_SKU" value="<%=Gia.KhuyenMai %>" placeholder="SKU (Với sản phẩm)" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="number" class="form-control" id="txtGia_Category" value="<%=Gia.KhuyenMai %>" placeholder="Category (Với sản phẩm)" />
                                                    </div>
                                                </div>

                                                <div class="row collapse" id="extras-bac-si2">
                                                    <div class="col-md-3">
                                                        <input type="number" class="form-control" id="txtGia_tour" value="<%=Gia.HocVitour%>" placeholder="Giá tour" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="txtTG_Ngay" value="<%=Gia.SoNgay %>" placeholder="Thời gian tour" />
                                                    </div>

                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="txtTG_Location" value="<%=Gia.Location %>" placeholder="Điểm đến" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="txtTG_Vehicle" value="<%=Gia.Vehicle %>" placeholder="Phương tiện" />

                                                    </div>

                                                    <div class="row collapse" id="extras-bac-si1">
                                                        <div class="col-md-3">
                                                            <input type="number" class="form-control" id="txtGia_HocViPhong" value="<%=Gia.HocViPhong %>" placeholder="Giá gốc" />
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="number" class="form-control" id="txtGia_KhuyenMaiPhong" value="<%=Gia.KhuyenMaiPhong %>" placeholder="Giá khuyến mãi" />
                                                        </div>
                                                        <br />
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control" id="txtGia_DichVu1" value="<%=Gia.DichVu1 %>" placeholder="Dịch vụ đi kèm 1" />
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control" id="txtGia_DichVu2" value="<%=Gia.DichVu2 %>" placeholder="Dịch vụ đi kèm 2" />
                                                        </div>
                                                        <br />
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control" id="txtGia_SoPhong" value="<%=Gia.SoPhong %>" placeholder="Số phòng Phòng" />
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control" id="txtGia_SoNguoi" value="<%=Gia.SoNguoi %>" placeholder="Số người" />
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control" id="txtGia_DichVu3" value="<%=Gia.DichVu3 %>" placeholder="Dịch vụ đi kèm 3" />
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control" id="txtGia_DichVu4" value="<%=Gia.DichVu4 %>" placeholder="Dịch vụ đi kèm 4" />
                                                        </div>
                                                    </div>
                                            </fieldset>

                                            <%}%>
                                            <%// Tao ra 1 nut them %>
                                            <div class="form-group">
                                                <label>Tags<span class="text-danger">*</span></label>
                                                <input type="text" autocomplete="off" name="basic" id="tagsLangTxt" class="form-control" value="" tabindex="1" required="required" placeholder="Tag">
                                            </div>
                                            <div class="form-group">
                                                <label>Meta title<span class="text-danger">*</span></label>
                                                <input type="text" autocomplete="off" name="basic" id="metaTitletxt" class="form-control" value="" tabindex="1" required="required" placeholder="Khai báo từ khóa vs công cụ tìm kiếm">
                                            </div>
                                            <div class="form-group">
                                                <label>Meta keyword:</label>
                                                <input class="form-control" id="metaKeywordTxt" placeholder="Metakeyword, từ khóa đểo seo danh mục" value="" />
                                            </div>
                                            <div class="form-group">
                                                <label>Meta description:</label>
                                                <textarea rows="2" cols="5" class="form-control" id="metaDescriptionTxt" placeholder="MetaDescription.. mô tả trang."></textarea>
                                            </div>

                                            <div class="_binding_doan" id="binding">
                                                <%var parts = NewsBo.GetPartInNews(id, vn_langs == null ? "vi-VN" : vn_langs.LanguageCode); %>
                                                <%foreach (var item in parts)
                                                    { %>
                                                <div class="item _add" data-random-id="" data-id="<%=item.Id %>">
                                                    <a href="javascript:void(0)" class="xoa-doan"><i class="icon-bin"></i></a>
                                                    <div class="control-label col-lg-2 text-center _field">
                                                        <div class="avatar" style="width: auto; height: auto">
                                                            (1500px*630px)<input accept="image/*" class="upload_anh" id="" type="file" name="files[]" style="display: none" />
                                                            <label for="img_attack_ random_id + ">
                                                                <img id="" class="append_img" src="/CMS/Themes/Images/nothumb.JPG" /></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-10 _field">
                                                        <input type="text" name="basic" class="form-control title" value="<%=item.Title %>" tabindex="1" required="required" placeholder="Tối đa 255 ký tự">
                                                        <div class="content-tiem" style="padding-top: 10px">
                                                            <textarea class="editor" id="editor<%=item.Id %> "><%=item.Content %></textarea>
                                                        </div>

                                                    </div>

                                                </div>
                                                <%} %>
                                            </div>
                                            <div class="">
                                                <button class="btn btn-danger" type="button" id="them-doan">Thêm đoạn mới</button>
                                            </div>


                                            <div class="form-group text-center">
                                                <button type="button" id="zoneInLanguageSaveBtn" class="btn ibtn-xs btn-primary "><i class="icon-floppy-disk position-left"></i>Lưu</button>
                                            </div>
                                            <div class="table-responsive" id="zones-language">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Ngôn ngữ</th>
                                                            <th>Tên</th>
                                                            <th class="text-right">Chức năng</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="zone-language">
                                                        <%if (news_languages != null)
                                                            { %>

                                                        <%foreach (var lag in news_languages)
                                                            {%>

                                                        <tr data="<%=lag.LanguageCode %>">
                                                            <td><%=lag.LanguageCode %></td>
                                                            <td><%=lag.Title %></td>
                                                            <td class="text-right" data-id="<%=lag.Id %>">
                                                                <a class="_edit_lang" data-lcode="<%=lag.LanguageCode %>" style="padding-right: 20px" href="javascript:void(0)"><i class="icon-pencil7"></i></a>
                                                                <a class="_delete_lang" data-lcode="<%=lag.Id %>" href="javascript:void(0)"><i class="icon-bin"></i></a>
                                                            </td>
                                                        </tr>
                                                        <%}%>
                                                        <%if (!news_languages.Any())
                                                            {%>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="3">Dữ liệu trống
                                                                </td>
                                                            </tr>
                                                        </tfoot>
                                                        <%}%>
                                                        <%}%>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Page content -->

            <!-- /main content -->

            <!-- /page content -->
        </div>
    </div>
    <%} %>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterCph" runat="server">
    <script src="/CMS/Themes/js/ckeditor4.2/ckeditor.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/ckeditor4.2/config.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/textcount/textcounter.min.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/jquery.jqpagination.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/plugins/ui/moment/moment_locales.min.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/plugins/pickers/daterangepicker.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/plugins/pickers/anytime.min.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/ftag/ftag.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/fselect/fselect.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/comfirm/jquery-confirm.js?v=<%=UIHelper.Version %>"></script>
    <%--<script src="/CMS/Modules/Zone/Scripts/zone.js?v=<%=UIHelper.Version %>"></script>--%>
    <script src="/CMS/Modules/News/Scripts/main.js?v=<%=UIHelper.Version %>"></script>
    <%--File manager--%>
    <script>
        var RAllZones = <%= JsonConvert.SerializeObject(GetAllZoneWithTreeViewSimpleFields())%>;
    </script>
    <script src="/CMS/Themes/js/jsTree/dist/jstree.min.js"></script>
    <script src="/CMS/Modules/FileManager/Scripts/image.upload.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Modules/FileManager/Scripts/image.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Modules/FileManager/fmanagers.v1.js"></script>
    <script type="text/javascript">
        /*
         return ' <div class="item _add">' +
            '<i class="icon-bin"></i>' +
            '<div class="control-label col-lg-2 text-center _field">' +
            '<div class="avatar"  style="width: auto;height: auto">' +
            '(1500px*630px)<input accept="image/*" id="img_attack_' + data.id + '" type="file" name="files[]" style="display: none" />' +
            '  <label for="img_attack_' + data.id + '"><img  src="/CMS/Themes/Images/nothumb.JPG"/></label> </div></div><div class="col-lg-10 _field">' +
            '<input type="text" name="basic" class="form-control title" value="" tabindex="1" required="required" placeholder="Tối đa 255 ký tự">' +
            '<div class="content-tiem" style="padding-top: 10px">' +
            '<textarea class="editor" id=' + data.id + '></textarea>' +
            '</div></div></div>';
         */


    </script>
</asp:Content>
