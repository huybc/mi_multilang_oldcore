﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS/Master/ICMS.Master" AutoEventWireup="true" CodeBehind="ZoneEdit.aspx.cs" Inherits="GoldLuck.CMS.Pages.ZoneEdit" %>

<%@ Import Namespace="GoldLuck.Core.Helper" %>
<%@ Import Namespace="Mi.Common.ChannelConfig" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Register Src="~/CMS/Modules/Zone/MenuLeft.ascx" TagPrefix="uc1" TagName="MenuLeft" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderCph" runat="server">
    <link href="/CMS/Themes/js/comfirm/jquery-confirm.min.css" rel="stylesheet" />
    <link href="/CMS/Themes/js/fselect/styles.css" rel="stylesheet" />
    <link href="/CMS/Modules/Zone/Styles/main.css" rel="stylesheet" />
    <link href="/CMS/Modules/News/Styles/main.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainCph" runat="server">
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">
            <!-- Main sidebar -->
            <uc1:MenuLeft runat="server" ID="MenuLeft" />
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper" style="background: #fff;">
                <%
                    var zoneType = Page.RouteData.Values["zoneType"] != null ? Page.RouteData.Values["zoneType"] : "";
                %>
                <div class="col-lg-10">
                    <div id="zone-edit" zone-id='<%=obj.Id %>' zone-type="<%=zoneType %>">

                        <div class="row">
                            <div class="panel-body">
                                <div class="tabbable nav-tabs-vertical nav-tabs-left" style="display: table; width: 100%">
                                    <ul class="nav nav-tabs nav-tabs-highlight" style="">
                                        <li class="active info-tab"><a href="#left-tab1" data-toggle="tab"><i class="icon-info3 position-left"></i>Thông tin</a></li>
                                        <li class="language-tab"><a href="#left-tab2" data-toggle="tab"><i class="icon-puzzle4 position-left"></i>Ngôn ngữ</a></li>
                                    </ul>

                                    <div class="tab-content" style="display: table-cell">
                                        <div class="tab-pane active has-padding fselect" id="left-tab1">
                                            <div class="form-group">
                                                <label>Tên:</label>
                                                <input type="text" autocomplete="off" class="form-control" id="zoneNameTxt" placeholder="Tên danh mục" value="<%= obj.Name %>">
                                            </div>

                                            <div class="form-group " style="width: 100%; display: inline-block">
                                                <p style="margin-bottom: 0">
                                                    <label>Danh mục cha:</label>

                                                </p>
                                                <select data-placeholder="Select your state" class="select" id="_zoneddl" style="width: 100%" tabindex="4" data="<%=obj.ParentId %>">
                                                    <option value="">- Chọn -</option>
                                                    <asp:Repeater runat="server" ID="ZoneRpt">
                                                        <ItemTemplate>
                                                            <option value='<%#Eval("Id") %>' data-parent="<%#Eval("ParentId") %>"><%#Eval("Name") %></option>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </select>
                                            </div>
                                            <% if (zoneType.ToInt32Return0() != (int)ZoneType.Blog)
                                                { %>
                                            <div class="form-group text-left" id="isShowHomaPageEl">
                                                <label class="checkbox-inline checkbox-left">
                                                    <input <%= obj.IsShowHomePage ? "checked" : "" %> type="checkbox" tabindex="8" id="isShowHomaPage">
                                                    Hiển thị trang chủ</label>

                                            </div>
                                            <% } %>


                                            <% if (zoneType.ToInt32Return0() == (int)ZoneType.Location)
                                                { %>

                                            <div class="form-group">
                                                <label>Lưới hiển thị (3 cột) <a style="font-size: 10px" target="_blank" href="/Themes/imgs/ti-le.png">Xem ví dụ</a></label>
                                                <select data-placeholder="Select your state" class="select" id="colDdl" tabindex="4" data="<%=obj.Col %>">
                                                    <option value=''>- Chọn -</option>
                                                    <option value='col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12'>Tỉ lệ 1/3</option>
                                                    <option value='col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12'>Tỉ lệ 2/3</option>
                                                    <option value='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12'>Tỉ lệ 1.5/3 (chia đôi)</option>
                                                </select>
                                            </div>

                                            <% } %>
                                            <div class="form-group" id="attack-files">
                                                <div class="row">
                                                    <div class="col-lg-3" id="attack-thumb">
                                                        <div>
                                                            <label>Ảnh đại diện <i style="font-size: 10px">(1920 * 500 px)</i></label>
                                                        </div>
                                                        <div class="avatar">
                                                            <input accept="image/*" id="fileSingleUploadThumb" type="file" name="files[]" style="display: none" />
                                                            <label for="fileSingleUploadThumb">
                                                                <%if (obj.Id > 0)
                                                                    {%>
                                                                <img data-img="<%=obj.Avatar %>" src="/<%=!string.IsNullOrEmpty(obj.Avatar)? CmsChannelConfiguration.GetAppSetting("StorageUrl")+"/"+obj.Avatar:"CMS/Themes/Images/no-thumbnai.png" %>" />
                                                                <%}
                                                                    else
                                                                    {%>
                                                                <img src="/CMS/Themes/Images/no-thumbnai.png" width="60" />
                                                                <% } %>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <% if (zoneType.ToInt32Return0() == (int)ZoneType.Location)
                                                        { %>
                                                    <div class="col-lg-6" id="attack-icon">
                                                        <div>
                                                            <label>Banner</label>
                                                        </div>
                                                        <div class="avatar">
                                                            <input accept="image/*" id="fileSingleUploadIcon" type="file" name="files[]" style="display: none" />
                                                            <label for="fileSingleUploadIcon">
                                                                <%if (obj.Id > 0)
                                                                    {%>
                                                                <img style="width: 100%" data-img="<%=obj.Banner %>" src="/<%=!string.IsNullOrEmpty(obj.Banner)? CmsChannelConfiguration.GetAppSetting("StorageUrl")+"/"+obj.Banner:"CMS/Themes/Images/no-thumbnai.png" %>" />
                                                                <%}
                                                                    else
                                                                    {%>
                                                                <img src="/CMS/Themes/Images/no-thumbnai.png" width="60" />
                                                                <% } %>
                                                            </label>
                                                        </div>
                                                        <p>Kích thước banner theo lưới hiển thị (đơn vị px).<br />
                                                            <strong>Tỉ lệ 1/3</strong> : 370x190
                                                            <br />
                                                            <strong>Tỉ lệ 2/3</strong>: 755x190
                                                            <br />
                                                            <strong>Tỉ lệ 1.5/3 - chia đôi</strong>: 562x190</p>
                                                    </div>
                                                    <% } %>
                                                </div>

                                            </div>
                                            <div class="form-group">
                                                <label>Thứ tự:</label>
                                                <input type="text" autocomplete="off" class="form-control" id="sortZoneTxt" placeholder="Thứ tự" value="<%= obj.SortOrder %>">
                                            </div>
                                            <div class="form-group text-center">
                                                <button type="button" id="zoneSaveBtn" class="btn ibtn-xs btn-primary "><i class="icon-floppy-disk position-left"></i><%=obj.Id>0?"Cập nhật":"Lưu" %></button>
                                            </div>
                                        </div>
                                        <div class="tab-pane has-padding fselect" id="left-tab2">
                                            <div class="form-group fselect" style="display: inline-block; width: 100%">
                                                <p>
                                                    <label>Ngôn ngữ</label>
                                                </p>
                                                <select data-placeholder="Select your state" class="select" id="languageDdl" tabindex="4" data="<%=obj.ParentId %>">
                                                    <%
                                                        var languages = ConfigBo.GetAllLanguage();
                                                        foreach (var lg in languages)
                                                        {
                                                    %>
                                                    <option value='<%=lg.Code %>'><%=lg.Name %></option>
                                                    <% } %>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Tên <span class="text-danger">*</span></label>
                                                <input type="text" autocomplete="off" name="basic" id="nameLanguageTxt" class="form-control" value="<%=obj.MetaTitle %>" tabindex="1" required="required" placeholder="Tên danh mục theo ngôn ngữ">
                                            </div>
                                            <div class="form-group">
                                                <label>Mô tả:</label>
                                                <textarea rows="2" cols="5" class="form-control" id="contentTxt" placeholder="Enter your message here"><%= obj.Content %></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Meta title<span class="text-danger">*</span></label>
                                                <input type="text" autocomplete="off" name="basic" id="metaTitletxt" class="form-control" value="<%=obj.MetaTitle %>" tabindex="1" required="required" placeholder="Khai báo từ khóa vs công cụ tìm kiếm">
                                            </div>
                                            <div class="form-group">
                                                <label>Meta keyword:</label>
                                                <input class="form-control" id="metaKeywordTxt" placeholder="Metakeyword, từ khóa đểo seo danh mục" value="<%=obj.MetaKeyword %>" />
                                            </div>
                                            <div class="form-group">
                                                <label>Meta description:</label>
                                                <textarea rows="2" cols="5" class="form-control" id="metaDescriptionTxt" placeholder="MetaDescription.. mô tả trang."><%=obj.MetaDescription %></textarea>
                                            </div>

                                            <div class="form-group text-center">
                                                <button type="button" id="zoneInLanguageSaveBtn" class="btn ibtn-xs btn-primary "><i class="icon-floppy-disk position-left"></i>Lưu</button>
                                            </div>
                                            <div class="table-responsive" id="zones-language">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Ngôn ngữ</th>
                                                            <th>Tên</th>
                                                            <th class="text-right">Chức năng</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="zone-language">
                                                        <% var lags = ZoneBo.GetAllZoneLanguage(obj.Id);
                                                            foreach (var lag in lags)
                                                            {


                                                        %>
                                                        <tr data="<%=lag.LanguageCode %>">
                                                            <td><%=lag.LanguageName %></td>
                                                            <td><%=lag.Name %></td>
                                                            <td class="text-right" data-id="<%=lag.Id %>">
                                                                <a class="_edit" style="padding-right: 20px" href="javascript:void(0)"><i class="icon-pencil7"></i></a>
                                                                <a class="_delete" href="javascript:void(0)"><i class="icon-bin"></i></a>
                                                            </td>
                                                        </tr>
                                                        <% } %>
                                                    </tbody>
                                                    <% if (!lags.Any())
                                                        { %>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="3">Dữ liệu trống
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                    <% } %>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterCph" runat="server">
    <script src="/CMS/Themes/js/ckeditor4.2/ckeditor.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/ckeditor4.2/config.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/textcount/textcounter.min.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/jquery.jqpagination.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/plugins/ui/moment/moment_locales.min.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/plugins/pickers/daterangepicker.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/plugins/pickers/anytime.min.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/ftag/ftag.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/fselect/fselect.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/comfirm/jquery-confirm.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Modules/Zone/Scripts/zoneEdit.js?v=<%=UIHelper.Version %>"></script>
    <script src="/Libs/smoothscroll.js"></script>
    <%--File manager--%>

    <script src="/CMS/Modules/FileManager/Scripts/image.upload.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Modules/FileManager/Scripts/image.js?v=<%=UIHelper.Version %>"></script>

</asp:Content>

