﻿var feditor;
CKEDITOR.plugins.add('filemanager', {
    icons: 'filemanager',
    hidpi: true,
    init: function (editor) {
        editor.ui.addButton && editor.ui.addButton('filemanager', {
            label: 'Chèn ảnh',
            command: 'filemanager',
            toolbar: 'filemanager'
        });
        editor.addCommand('filemanager',
         {
             exec: function (editor) {
                 R.Image.Dialog(function (op) {
                     try {
                         if (op.multi) {
                             $.each(op.data, function (i, v) {
                                 if (op.upload) {
                                     editor.insertHtml('<img style="max-width:90%" src="'+ v + '?w=718"/>');
                                 } else {
                                     editor.insertHtml('<img style="max-width:90%" src="' + v + '"/>');
                                 }
                             });
                         }
                         if (!op.multi) {
                             editor.insertHtml('<img style="max-width:90%" src="' + op.data + '?w=718"/>');
                         }
                     } catch (el) {
                     }
                 });
             }
         });
    }
});
function receiveData(op) {

}

