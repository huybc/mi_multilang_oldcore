﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/GoldHome.Master" AutoEventWireup="true" CodeBehind="GioHang.aspx.cs" Inherits="GoldLuck.Pages.GioHang" %>
<%@ Import Namespace="GoldLuck.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    
    <div class="container my-3">
        <div  >
            <nav aria-label="breadcrumb" class="align-self-center">
                <ol class="breadcrumb jan-breadcrumb mb-2">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Giỏ hàng</li>
                </ol>
            </nav> 
        </div>
    </div>

    <section class="list-product-cart">
        <div class="container">
            <div class="row"> 
                <div class="col-xl-9 col-lg-9 col-md-8 col-sm-12 col-12"> 
                    <h5 class="font-weight-bold mb-3">Giỏ hàng</h5>
                    <section class="cart-table  mb-4"> 
                        <div class="item-cart-page">
                            <div class="row  ">
                                <div class="col-lg-1 col-md-2 col-sm-4 col-3 ">
                                    <img src="images/change/img-sp-new.jpg" class="img-fluid w-100 mb-3" alt=""/>
                                </div>
                                <div class="col-lg-6 col-md-10 col-sm-8 col-9">
                                    <h4 class="title">
                                        <a href="">Chậu cây mạ vàng NK080 (Màu vàng)</a>
                                    </h4>
                                    <div class="price">
                                        200.000
                                    </div>
                                </div>
                               <div class="col-lg-4 col-md-10 col-sm-9 col-10 ">
                                <div class="group-amount d-flex mb-3  justify-content-end">
                                    <button class="btn " type="button">
                                        <i class="fas fa-minus" aria-hidden="true"></i>
                                    </button>
                                  <input type="text" class="form-control " value="1">
                                      <button class="btn " type="button">
                                          <i class="fas fa-plus" aria-hidden="true"></i>
                                      </button>
                                </div>
                               </div>
                               <div class="col-lg-1 col-md-2 col-sm-3 col-2 text-right pl-0">
                                <button class="btn " type="button">
                                    <img src="images/times-icon.svg" class="img-fluid"/>
                                </button>
                               </div>
                            </div>
                        </div>
                        <div class="item-cart-page">
                            <div class="row  ">
                                <div class="col-lg-1 col-md-2 col-sm-4 col-3 ">
                                    <img src="images/change/img-sp-new.jpg" class="img-fluid w-100 mb-3" alt=""/>
                                </div>
                                <div class="col-lg-6 col-md-10 col-sm-8 col-9">
                                    <h4 class="title">
                                        <a href="">Chậu cây mạ vàng NK080 (Màu vàng)</a>
                                    </h4>
                                    <div class="price">
                                        200.000
                                    </div>
                                </div>
                               <div class="col-lg-4 col-md-10 col-sm-9 col-10 ">
                                <div class="group-amount d-flex mb-3  justify-content-end">
                                    <button class="btn " type="button">
                                        <i class="fas fa-minus" aria-hidden="true"></i>
                                    </button>
                                  <input type="text" class="form-control " value="1">
                                      <button class="btn " type="button">
                                          <i class="fas fa-plus" aria-hidden="true"></i>
                                      </button>
                                </div>
                               </div>
                               <div class="col-lg-1 col-md-2 col-sm-3 col-2 text-right pl-0">
                                <button class="btn " type="button">
                                    <img src="images/times-icon.svg" class="img-fluid"/>
                                </button>
                               </div>
                            </div>
                        </div>
                         
                    </section>
                    <section class="infor-customer">
                        <form>
                            <div class="heading">
                                Thông tin khách hàng
                            </div>
                            <div>
                                <label class="label-checkbox d-inline-block mr-3">
                                    Nam 
                                    <input type="checkbox" name="radio" checked/>
                                    <span class="checkmark" style="border-radius: 50%;"></span>
                                </label>
                                <label class="label-checkbox d-inline-block">
                                    Nữ
                                    <input type="checkbox" name="radio" />
                                    <span class="checkmark"  style="border-radius: 50%;"></span>
                                </label>
                            </div>
                            <div class="form-row mb-4">
                                <div class="col-sm-6 col-12">
                                    <input type="text" class="form-control mb-3" placeholder="Họ và tên *">
                                </div>
                                <div class="col-sm-6 col-12">
                                    <input type="text" class="form-control mb-3" placeholder="Số điện thoại *">
                                </div>
                                <div class="col-sm-12 col-12">
                                    <textarea rows="4" type="text" class="form-control mb-3"
                                        placeholder="Ghi chú"></textarea>
                                </div>
                            </div>
                            <div class="heading">
                                Chọn địa chỉ và thời gian nhận hàng
                            </div>
                            <div class="form-row ">
                                <div class="col-sm-6 col-12">
                                    <select class="form-control mb-3">
                                        <option hidden>Chọn Tỉnh/Thành</option>
                                        <option>Hồ Chí Minh</option>
                                    </select>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <select class="form-control mb-3">
                                        <option hidden>Chọn Quận/Huyện</option>
                                        <option>Tân Phú</option>
                                    </select>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <select class="form-control mb-3">
                                        <option hidden>Chọn Phường/Xã</option>
                                        <option>Hồ Chí Minh</option>
                                    </select>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <input type="text" class="form-control mb-3" placeholder="Số nhà">
                                </div>
    
                            </div>
                            <div class="mb-3 d-flex flex-wrap">
                                <label class="label-checkbox w-50  ">
                                    Thanh toán tiền mặt khi nhận hàng
                                    <input type="checkbox" >
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox w-50">
                                    Chuyển qua tài khoản ngân hàng
                                   
                                    <input type="checkbox"  >
                                    <span class="checkmark"></span>
                                </label>  
                            </div> 

                            <div class="mb-5">
                                <div class="heading">
                                 Thông tin chuyển khoản
                                </div>
                                <div class="mb-2">
                                    •<b> Ngân hàng :</b> Vietcombank - CN Thăng Long, Hà Nội
                                </div>
                                <div class="mb-2">
                                    • <b>Tên tài khoản: </b>Nguyễn Hoàng Nam
                                </div>
                                <div class="mb-2">
                                    • STK: <b>0491.0015.888</b>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-12">
                    <div class="total-pay">
                        <div class="d-flex ">
                            <label class="mr-2 font-weight-600">Tổng tiền:</label>
                            <div class="ml-auto">10 000 đ</div>
                        </div>
                        <div class="d-flex ">
                            <label class="mr-2 font-weight-600">Shipping:</label>
                            <div class="ml-auto">1 000 đ</div>
                        </div>
                        <div class="d-flex mb-2">
                            <label class="mr-2 font-weight-600">Giảm giá:</label>
                            <div class="ml-auto">1 000 đ</div>
                        </div>
                        <div class="border-top mb-3"></div>
                        <div class="d-flex">
                            <label class="mr-2 font-weight-600">Thanh toán:</label>
                            <div class="ml-auto color-C6832B font-weight-600">8 000đ</div>
                        </div>
                        <button class="btn btn-save w-100 my-2" data-toggle="modal" data-target="#modal-xn">Đặt hàng</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
