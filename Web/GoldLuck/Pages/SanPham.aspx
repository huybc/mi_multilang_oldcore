﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/GoldHome.Master" AutoEventWireup="true" CodeBehind="SanPham.aspx.cs" Inherits="GoldLuck.Pages.SanPham" %>
<%@ Import Namespace="GoldLuck.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
     <div class="container mt-4">
        <div class="row flex-md-row-reverse">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12">
                <div class="list-menu-right">
                    <div class="heading">
                        LỌC THEO GIÁ
                    </div>
                    <div class="list">
                        <label class="label-checkbox  ">
                            Dưới 1 triệu
                            <input type="checkbox" name="radio" />
                            <span class="checkmark"  style="border-radius: 50%;"></span>
                        </label>
                        <label class="label-checkbox  ">
                            Từ 1 triệu đến 2 triệu
                            <input type="checkbox" name="radio" />
                            <span class="checkmark"  style="border-radius: 50%;"></span>
                        </label>
                        <label class="label-checkbox  ">
                            Từ 3 triệu đến 5 triệu
                            <input type="checkbox" name="radio" />
                            <span class="checkmark"  style="border-radius: 50%;"></span>
                        </label>
                        <label class="label-checkbox  ">
                            Từ 5 triệu đến 10 triệu
                            <input type="checkbox" name="radio" />
                            <span class="checkmark"  style="border-radius: 50%;"></span>
                        </label>
                        <label class="label-checkbox  ">
                            Trên 10 triệu
                            <input type="checkbox" name="radio" />
                            <span class="checkmark"  style="border-radius: 50%;"></span>
                        </label>
                    </div>
                    
                </div>
                <div class="list-menu-right">
                    <div class="heading">
                        DANH MỤC
                    </div>
                    <div class="list">
                        <label class="label-checkbox  ">
                            Lifestyle (0)
                            <input type="checkbox" name="radio" />
                            <span class="checkmark"  style="border-radius: 50%;"></span>
                        </label>
                        <label class="label-checkbox  ">
                            Accessories (4)
                            <input type="checkbox" name="radio" />
                            <span class="checkmark"  style="border-radius: 50%;"></span>
                        </label>
                        <label class="label-checkbox  ">
                            Bags (4)
                            <input type="checkbox" name="radio" />
                            <span class="checkmark"  style="border-radius: 50%;"></span>
                        </label>
                        <label class="label-checkbox  ">
                            Basics (4)
                            <input type="checkbox" name="radio" />
                            <span class="checkmark"  style="border-radius: 50%;"></span>
                        </label>
                        <label class="label-checkbox  ">
                            Casual (12)
                            <input type="checkbox" name="radio" />
                            <span class="checkmark"  style="border-radius: 50%;"></span>
                        </label>
                        <label class="label-checkbox  ">
                            Children (8)
                            <input type="checkbox" name="radio" />
                            <span class="checkmark"  style="border-radius: 50%;"></span>
                        </label>
                        <label class="label-checkbox  ">
                            Clothes (8)
                            <input type="checkbox" name="radio" />
                            <span class="checkmark"  style="border-radius: 50%;"></span>
                        </label>
                    </div>
                    
                </div>
                <div class="list-menu-right">
                    <div class="heading">
                        Sản phẩm mới
                    </div>
                    <div class="list">
                        <div class="item-cart px-0 ">
                            <div class="image-product">
                                <img src="images/change/img-sp-new.jpg" class="" />
                            </div>
                            <div class="text pl-3 py-1"> 
                                <h6 class="name-product mb-2 font-weight-light   "><a href="">Bàn trà titan nảy mầm MC0036</a></h6>
                                <div class="price color-C6832B">200.000 vnđ</div> 
                            </div> 
                        </div>
                        <div class="item-cart px-0 ">
                            <div class="image-product">
                                <img src="images/change/img-sp-new.jpg" class="" />
                            </div>
                            <div class="text pl-3 py-1"> 
                                <h6 class="name-product mb-2 font-weight-light  "><a href="">Bàn trà titan nảy mầm MC0036</a></h6>
                                <div class="price color-C6832B">200.000 vnđ</div> 
                            </div> 
                        </div>
                        <div class="item-cart px-0 ">
                            <div class="image-product">
                                <img src="images/change/img-sp-new.jpg" class="" />
                            </div>
                            <div class="text pl-3 py-1"> 
                                <h6 class="name-product mb-2 font-weight-light   "><a href="">Bàn trà titan nảy mầm MC0036</a></h6>
                                <div class="price color-C6832B">200.000 vnđ</div> 
                            </div> 
                        </div> 
                        <div class="item-cart px-0 ">
                            <div class="image-product">
                                <img src="images/change/img-sp-new.jpg" class="" />
                            </div>
                            <div class="text pl-3 py-1"> 
                                <h6 class="name-product mb-2 font-weight-light  "><a href="">Bàn trà titan nảy mầm MC0036</a></h6>
                                <div class="price color-C6832B">200.000 vnđ</div> 
                            </div> 
                        </div>
                        <div class="item-cart px-0 ">
                            <div class="image-product">
                                <img src="images/change/img-sp-new.jpg" class="" />
                            </div>
                            <div class="text pl-3 py-1"> 
                                <h6 class="name-product mb-2 font-weight-light  "><a href="">Bàn trà titan nảy mầm MC0036</a></h6>
                                <div class="price color-C6832B">200.000 vnđ</div> 
                            </div> 
                        </div>
                    </div>
                    
                </div>
                <div class="list-menu-right">
                    <div class="heading">
                        TAGS
                    </div>
                    <div class="list tags ">
                        <a class="" href="">
                            BAGS
                        </a>
                        <a class="" href="">
                            CLOTHES
                        </a>
                        <a class="" href="">
                            DECORATION
                        </a>
                        <a class="" href="">
                            FLOWERS
                        </a>
                        <a class="" href="">
                            LIFESTYLE
                        </a>
                        <a class="" href="">
                            LOOK MINIMAL
                        </a>
                        <a class="" href="">
                            BAGS
                        </a>
                        <a class="" href="">
                            CLOTHES
                        </a>
                        <a class="" href="">
                            DECORATION
                        </a>
                        <a class="" href="">
                            FLOWERS
                        </a>
                        <a class="" href="">
                            LIFESTYLE
                        </a>
                        <a class="" href="">
                            LOOK MINIMAL
                        </a>
                    </div>
                    
                </div>
                
            </div>
            <div class="col-xl-9 col-lg-9 col-md-8 col-sm-12 col-12 mb-5">
                <nav aria-label="breadcrumb ">
                    <ol class="breadcrumb jan-breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li> 
                        <li class="breadcrumb-item active" aria-current="page">Sản phẩm</li>
                    </ol>
                </nav>

                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md- col-sm-6 col-6">
                        <div class="item-product">
                            <div class="image position-relative"> 
                                    <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                                <div class="tag">
                                    HOT
                                </div>
                                <div class="overlay">
    
                                </div>
                                <div class="group-btn">
                                    <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                    <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                                </div>
                            </div>
                            <div class="py-3">
                                <div class="cate">
                                    <a href="" title="">Nội thất</a>
                                </div>
                                <h5 class="title">
                                    <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                                </h5>
                                <div class="price  ">
                                    <div class="new color-C6832B font-weight-bold mr-3">
                                        200.000 vnđ
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md- col-sm-6 col-6">
                        <div class="item-product">
                            <div class="image position-relative"> 
                                    <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                                <div class="tag">
                                    HOT
                                </div>
                                <div class="overlay">
    
                                </div>
                                <div class="group-btn">
                                    <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                    <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                                </div>
                            </div>
                            <div class="py-3">
                                <div class="cate">
                                    <a href="" title="">Nội thất</a>
                                </div>
                                <h5 class="title">
                                    <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                                </h5>
                                <div class="price  ">
                                    <div class="new color-C6832B font-weight-bold mr-3">
                                        200.000 vnđ
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md- col-sm-6 col-6">
                        <div class="item-product">
                            <div class="image position-relative"> 
                                    <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                                <div class="tag">
                                    HOT
                                </div>
                                <div class="overlay">
    
                                </div>
                                <div class="group-btn">
                                    <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                    <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                                </div>
                            </div>
                            <div class="py-3">
                                <div class="cate">
                                    <a href="" title="">Nội thất</a>
                                </div>
                                <h5 class="title">
                                    <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                                </h5>
                                <div class="price  ">
                                    <div class="new color-C6832B font-weight-bold mr-3">
                                        200.000 vnđ
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md- col-sm-6 col-6">
                        <div class="item-product">
                            <div class="image position-relative"> 
                                    <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                                <div class="tag">
                                    HOT
                                </div>
                                <div class="overlay">
    
                                </div>
                                <div class="group-btn">
                                    <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                    <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                                </div>
                            </div>
                            <div class="py-3">
                                <div class="cate">
                                    <a href="" title="">Nội thất</a>
                                </div>
                                <h5 class="title">
                                    <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                                </h5>
                                <div class="price  ">
                                    <div class="new color-C6832B font-weight-bold mr-3">
                                        200.000 vnđ
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md- col-sm-6 col-6">
                        <div class="item-product">
                            <div class="image position-relative"> 
                                    <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                                <div class="tag">
                                    HOT
                                </div>
                                <div class="overlay">
    
                                </div>
                                <div class="group-btn">
                                    <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                    <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                                </div>
                            </div>
                            <div class="py-3">
                                <div class="cate">
                                    <a href="" title="">Nội thất</a>
                                </div>
                                <h5 class="title">
                                    <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                                </h5>
                                <div class="price  ">
                                    <div class="new color-C6832B font-weight-bold mr-3">
                                        200.000 vnđ
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md- col-sm-6 col-6">
                        <div class="item-product">
                            <div class="image position-relative"> 
                                    <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                                <div class="tag">
                                    HOT
                                </div>
                                <div class="overlay">
    
                                </div>
                                <div class="group-btn">
                                    <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                    <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                                </div>
                            </div>
                            <div class="py-3">
                                <div class="cate">
                                    <a href="" title="">Nội thất</a>
                                </div>
                                <h5 class="title">
                                    <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                                </h5>
                                <div class="price  ">
                                    <div class="new color-C6832B font-weight-bold mr-3">
                                        200.000 vnđ
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md- col-sm-6 col-6">
                        <div class="item-product">
                            <div class="image position-relative"> 
                                    <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                                <div class="tag bg-C6832B">
                                    -10%
                                </div>
                                <div class="overlay">
    
                                </div>
                                <div class="group-btn">
                                    <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                    <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                                </div>
                            </div>
                            <div class="py-3">
                                <div class="cate">
                                    <a href="" title="">Nội thất</a>
                                </div>
                                <h5 class="title">
                                    <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                                </h5>
                                <div class="price  ">
                                    <div class="new color-C6832B font-weight-bold mr-3">
                                        200.000 vnđ
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md- col-sm-6 col-6">
                        <div class="item-product">
                            <div class="image position-relative"> 
                                    <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                                <div class="tag">
                                    HOT
                                </div>
                                <div class="overlay">
    
                                </div>
                                <div class="group-btn">
                                    <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                    <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                                </div>
                            </div>
                            <div class="py-3">
                                <div class="cate">
                                    <a href="" title="">Nội thất</a>
                                </div>
                                <h5 class="title">
                                    <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                                </h5>
                                <div class="price  ">
                                    <div class="new color-C6832B font-weight-bold mr-3">
                                        200.000 vnđ
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md- col-sm-6 col-6">
                        <div class="item-product">
                            <div class="image position-relative"> 
                                    <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                                <div class="tag">
                                    HOT
                                </div>
                                <div class="overlay">
    
                                </div>
                                <div class="group-btn">
                                    <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                    <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                                </div>
                            </div>
                            <div class="py-3">
                                <div class="cate">
                                    <a href="" title="">Nội thất</a>
                                </div>
                                <h5 class="title">
                                    <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                                </h5>
                                <div class="price  ">
                                    <div class="new color-C6832B font-weight-bold mr-3">
                                        200.000 vnđ
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md- col-sm-6 col-6">
                        <div class="item-product">
                            <div class="image position-relative"> 
                                    <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                                <div class="tag bg-C6832B">
                                    -10%
                                </div>
                                <div class="overlay">
    
                                </div>
                                <div class="group-btn">
                                    <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                    <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                                </div>
                            </div>
                            <div class="py-3">
                                <div class="cate">
                                    <a href="" title="">Nội thất</a>
                                </div>
                                <h5 class="title">
                                    <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                                </h5>
                                <div class="price  ">
                                    <div class="new color-C6832B font-weight-bold mr-3">
                                        200.000 vnđ
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md- col-sm-6 col-6">
                        <div class="item-product">
                            <div class="image position-relative"> 
                                    <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                                <div class="tag">
                                    HOT
                                </div>
                                <div class="overlay">
    
                                </div>
                                <div class="group-btn">
                                    <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                    <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                                </div>
                            </div>
                            <div class="py-3">
                                <div class="cate">
                                    <a href="" title="">Nội thất</a>
                                </div>
                                <h5 class="title">
                                    <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                                </h5>
                                <div class="price  ">
                                    <div class="new color-C6832B font-weight-bold mr-3">
                                        200.000 vnđ
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md- col-sm-6 col-6">
                        <div class="item-product">
                            <div class="image position-relative"> 
                                    <img src="images/change/product-01.jpg" class="img-fluid w-100" alt=""> 
                                <div class="tag">
                                    HOT
                                </div>
                                <div class="overlay">
    
                                </div>
                                <div class="group-btn">
                                    <button class="btn btn-product mr-2"><img src="images/heart-icon.svg" class="img-fluid "></button>
                                    <button class="btn btn-product"><img src="images/cart-icon.svg" class="img-fluid"></button>
                                </div>
                            </div>
                            <div class="py-3">
                                <div class="cate">
                                    <a href="" title="">Nội thất</a>
                                </div>
                                <h5 class="title">
                                    <a href="" class="">Bàn trà titan nảy mầm MC0036</a>
                                </h5>
                                <div class="price  ">
                                    <div class="new color-C6832B font-weight-bold mr-3">
                                        200.000 vnđ
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                     
                     
    
                </div>
                <div class="jan-pagination text-right mb-4 ">
                    <button class="btn btn-pagi " title=""><i class="fas fa-chevron-left "></i></button>
                    <select class="select-page mr-2">
                        <option>1</option>
                    </select>
                    <span class="color-828282">/10</span>
                    <button class="btn btn-pagi " title=""><i class="fas fa-chevron-right "></i></button>
                </div>
                 
            </div>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
