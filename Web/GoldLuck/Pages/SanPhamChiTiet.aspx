﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/GoldHome.Master" AutoEventWireup="true" CodeBehind="SanPhamChiTiet.aspx.cs" Inherits="GoldLuck.Pages.SanPhamChiTiet" %>
<%@ Import Namespace="GoldLuck.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    
    <div class="container my-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb jan-breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Sàn gỗ</a></li>
                <li class="breadcrumb-item active" aria-current="page">Sàn gỗ công nghiệp</li>
            </ol>
        </nav>
    </div>

    <section class="product-detail">
        <div class="container">
            <div class="row  ">
                <div class="col-xl-5 col-lg-6 col-md-6 col-sm-12 col-12 mb-3">
                    <div class="row h-100">
                        <div class="col-xl-2 col-md-3 col-sm-3 col-12  d-none d-md-block ">
                            <div class="swiper-container gallery-thumbs ">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide" style="background-image:url(images/change/product-detail-slide.jpg)"></div>
                                    <div class="swiper-slide" style="background-image:url(images/change/product-detail-slide.jpg)"></div>
                                    <div class="swiper-slide" style="background-image:url(images/change/product-detail-slide.jpg)"></div>
                                    <div class="swiper-slide" style="background-image:url(images/change/product-detail-slide.jpg)"></div>
                                    <div class="swiper-slide" style="background-image:url(images/change/product-detail-slide.jpg)"></div>
                                </div>
                            </div> 
                        </div>
                        <div class="col-xl-10 col-md-9 col-sm-9 col-12">
                            <div class="swiper-container gallery-top">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide" style=" ">
                                        <div class="swiper-zoom-container">
                                            <img src="images/change/product-detail-slide.jpg" class="img-fluid">
                                        </div>
                                    </div>
                                    <div class="swiper-slide" style=" ">
                                        <div class="swiper-zoom-container">
                                            <img src="images/change/product-detail-slide.jpg" class="img-fluid">
                                        </div>
                                    </div>
                                    <div class="swiper-slide" style=" ">
                                        <div class="swiper-zoom-container">
                                            <img src="images/change/product-detail-slide.jpg" class="img-fluid">
                                        </div>
                                    </div>
                                    <div class="swiper-slide" style=" ">
                                        <div class="swiper-zoom-container">
                                            <img src="images/change/product-detail-slide.jpg" class="img-fluid">
                                        </div>
                                    </div>
                                    <div class="swiper-slide" style=" ">
                                        <div class="swiper-zoom-container">
                                            <img src="images/change/product-detail-slide.jpg" class="img-fluid">
                                        </div>
                                    </div>
                    
                                </div>
                    
                            </div>
                        </div>
                    </div>
                    
                      
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12"> 
                    <h1 class="title mb-3">
                        <a href="">Chậu cây mạ vàng NK080 (Màu vàng)</a>
                    </h1>
                    <div class="price mb-2">
                        <div class="new">
                            200.000 vnd
                        </div>
                        <div class="old">
                            200.000 vnd
                        </div> 
                    </div>
                    <div class="status">
                        <div>Tình trạng: <span class="color-C6832B">Còn hàng</span></div>
                    </div>
                    
                    <div class="des color-828282 mb-2  ">
                        Lorem proin gravida nibh enean sonauris hime sollicitudin enean , lom himenaeos lorem ean consertquat estruda cono pero.
                    </div>
                    <div class="size mb-3">
                        <div>Kích thước: <span  <span class="color-C6832B">20x34x30</span></div> 
                    </div>
                    <div class="row   "> 
                        <div class="col-xl-3 col-lg-4 col-md-5 col-12">
                            <div class="input-amount mb-3">
                                <button class="btn "><i class="fas fa-minus" aria-hidden="true"></i></button>
                                <input type="text" class=" mx-2" id=" " value="1" placeholder="">
                                <button class="btn "><i class="fas fa-plus" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-5 col-md-9 col-9">
                            <button class="btn btn-add text-uppercase mb-3">Add to cart</button>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-3 col-3">
                            <button class="btn btn-wish  mb-3">
                                <img src="images/heart-icon.svg" class="img-fluid"/>
                            </button>
                        </div>
                    </div> 
                    <div class="border-bottom my-3"></div>
                    <div class="mb-3 color-828282">
                        <div class="mb-2">
                            <span class="color-000 font-weight-600 ">Xuất xứ:</span> N/A
                         </div>
                         <div class="mb-2">
                            <span class="color-000 font-weight-600 ">Thư mục:</span> Nội thất
                         </div>
                         <div class="mb-2">
                             <span class="color-000 font-weight-600 mr-3">Share:</span> 
                                     <a href=""><img src="images/Facebook.svg" class="img-fluid mr-3" style="height: 15px;"/> </a>  
                                     <a href=""><img src="images/Instagram.svg" class="img-fluid mr-3" style="height: 15px;"/> </a> 
                                     <a href=""> <img src="images/Zalo.svg" class="img-fluid mr-3" style="height: 15px; "/> </a>  
                                     <a href=""><img src="images/Email.svg" class="img-fluid " style=" height: 15px; "/> </a> 
                             </div> 
                         </div>
                </div>
                </div>
             
        </div>
    </section>

    <div class="pt-2 my-4" style="background: #F5F7F2; ">
            <div class="container">
                <ul class="nav nav-pills nav-fill-rolux mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-1" role="tab"
                            aria-controls="pills-home" aria-selected="true">MÔ TẢ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-2" role="tab"
                            aria-controls="pills-profile" aria-selected="false">CÔNG DỤNG</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container" > 
            <div class="tab-content tab-content-rolux mb-5" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-1" role="tabpanel" >

                    <div class="h4 mb-3">Mô tả</div>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nisi enim, egestas non metus tempus, tincidunt maximus dolor. Nunc vitae odio sem. Aenean finibus pharetra purus, ac iaculis lorem auctor ut. Integer lobortis scelerisque lorem, a mollis metus pellentesque volutpat. Maecenas malesuada vestibulum turpis sollicitudin rhoncus. Pellentesque sollicitudin arcu eros, nec vestibulum metus consectetur et. Phasellus at auctor tortor. Nullam malesuada, elit sed malesuada viverra, arcu turpis maximus leo.


                    </p>
                    <p>
                        Sed scelerisque tristique iaculis. Aenean congue vitae nisi sed volutpat. Curabitur pretium turpis tellus, id mattis metus tincidunt sed. Duis vitae sollicitudin nibh. Vestibulum iaculis ipsum vitae nisi gravida, et maximus elit aliquam. Nam quam neque, lacinia quis auctor et, pellentesque sed purus. Lorem ipsum dolor sit amet
                    </p> 
                   
                </div>
                <div class="tab-pane fade" id="pills-2" role="tabpanel"  > 
                    <div class="h4 mb-3">Mô tả</div>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nisi enim, egestas non metus tempus, tincidunt maximus dolor. Nunc vitae odio sem. Aenean finibus pharetra purus, ac iaculis lorem auctor ut. Integer lobortis scelerisque lorem, a mollis metus pellentesque volutpat. Maecenas malesuada vestibulum turpis sollicitudin rhoncus. Pellentesque sollicitudin arcu eros, nec vestibulum metus consectetur et. Phasellus at auctor tortor. Nullam malesuada, elit sed malesuada viverra, arcu turpis maximus leo.


                    </p>
                    <p>
                        Sed scelerisque tristique iaculis. Aenean congue vitae nisi sed volutpat. Curabitur pretium turpis tellus, id mattis metus tincidunt sed. Duis vitae sollicitudin nibh. Vestibulum iaculis ipsum vitae nisi gravida, et maximus elit aliquam. Nam quam neque, lacinia quis auctor et, pellentesque sed purus. Lorem ipsum dolor sit amet
                    </p> 
                    
                </div>
                

            </div>
            <div class="text-center mb-5">
                <button   role="button" class="btn btn-outline-viewmore px-5" onclick="expanddetail()">Xem thêm +</button>
            </div>
        </div>

    <div class="container mb-5">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-12">
                 <!--Comment fb--> 
                
                 <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0"></script>
                 <div class="fb-comments" data-href="https://www.facebook.com/Rolux-Gold-107151547543683/" data-numposts="5" data-width="100%"></div>
            </div>
        </div>
    </div>
 
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>

