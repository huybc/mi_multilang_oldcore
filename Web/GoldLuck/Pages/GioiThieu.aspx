﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/GoldHome.Master" AutoEventWireup="true" CodeBehind="GioiThieu.aspx.cs" Inherits="GoldLuck.Pages.GioiThieu" %>

<%@ Import Namespace="GoldLuck.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
     <div class="container">
        <nav aria-label="breadcrumb ">
            <ol class="breadcrumb jan-breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li> 
                <li class="breadcrumb-item active" aria-current="page">Giới thiệu</li>
            </ol>
        </nav>
        <h4 class="mb-4">Giới thiệu</h4>
    </div>
    <section class="about-ss-1 mb-4">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-xl-6 col-lg-6 col-md-7 col-sm-12 col-12">
                    <div class="color-333 lp-3 mb-2">
                        VỀ CHÚNG TÔI
                    </div>
                    <h1 class="color-C6832B mb-3">GOLUX GOLD</h1>
                    <div class="color-828282">
                        <p>
                            We believe in a world where you have total freedom to be you, without judgement. To experiment. To express yourself. To be brave and grab life as the extraordinary adventure it is. So we make sure everyone has an equal chance to discover all the amazing things they’re capable of – no matter who they are, where they’re from or what looks they like to boss.

                        </p>
                        <p>
                            Our audience (AKA you) is wonderfully unique. And we do everything we can to help you find your fit, offering our Ciloe Brands in more than 30 sizes – and we’re committed to providing all sizes at the same price
                        </p>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/dATjPiJv0l0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>

        </div>
    </section>

    <!--Tin tức-->
    <section class="list-product-home py-3 py-lg-4  ">
        <div class="container">
            <div class="row justify-content-center"> 
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="item-news" style="background: none;box-shadow: 0 0 0;" >
                        <div class="image  ">
                            <a href="tour-detail.html"><img src="/Themes/images/change/about-news-1.jpg" class="img-fluid w-100" alt=""></a> 
                        </div>
                        <div class="py-4  ">
                            <h4 class="  h5">
                                <a href="tour-detail.html" title="">Quà tặng cao cấp</a>
                            </h4> 
                            <div class="des color-828282">
                                Exercitation photo booth stumptown tote bag Banksy, elit small batch freegan sed. Craft beer elit 
                                
                            </div>
                            <div class="text-right">
                                
                            </div>
                        </div>
                        <div>

                        </div>
                    </div>
                </div>   
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="item-news" style="background: none;box-shadow: 0 0 0;" >
                        <div class="image  ">
                            <a href="tour-detail.html"><img src="/Themes/images/change/about-news-2.jpg" class="img-fluid w-100" alt=""></a> 
                        </div>
                        <div class="py-4  ">
                            <h4 class="  h5">
                                <a href="tour-detail.html" title="">Mạ vàng 24k</a>
                            </h4> 
                            <div class="des color-828282">
                                Exercitation photo booth stumptown tote bag Banksy, elit small batch freegan sed. Craft beer elit 
                                
                            </div>
                            <div class="text-right">
                                
                            </div>
                        </div>
                        <div>

                        </div>
                    </div>
                </div>   
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="item-news" style="background: none;box-shadow: 0 0 0;" >
                        <div class="image  ">
                            <a href="tour-detail.html"><img src="/Themes/images/change/about-news-3.jpg" class="img-fluid w-100" alt=""></a> 
                        </div>
                        <div class="py-4  ">
                            <h4 class="  h5">
                                <a href="tour-detail.html" title="">Nội thất mạ vàng</a>
                            </h4> 
                            <div class="des color-828282">
                                Exercitation photo booth stumptown tote bag Banksy, elit small batch freegan sed. Craft beer elit 
                                
                            </div>
                            <div class="text-right">
                                
                            </div>
                        </div>
                        <div>

                        </div>
                    </div>
                </div>   
            </div>
             
        </div>
    </section>

    <section class="review py-3 py-lg-5">
        <div class="container">
            <div class="text-center mb-4">
                <h4 class="color-C6832B h4">ROLUX GOLD</h4>
                <div class="h3">
                    Khách hàng nói gì về chúng tôi
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-8 col-md-9 col-sm-11 col-11">
                    <div class="swiper-container">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper ">
                            <!-- Slides -->
                            <div class="swiper-slide">
                               <div class="item-customer">
                                <div class="avatar mb-4">
                                    <img src="/Themes/images/change/avatar-client.png" class="img-fluid"/>
                                </div>
                                <div class="mb-3">
                                    We believe in a world where you have total freedom to be you, without judgement. To experiment. To express yourself. 
                                </div>
                                <div class="author mb-2">
                                    Vũ Tuấn Ngọc
                                </div>
                                <div class="text-uppercase  ">
                                    Nhà thiết kế
                                </div>
                               </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-customer">
                                 <div class="avatar mb-4">
                                     <img src="/Themes/images/change/avatar-client.png" class="img-fluid"/>
                                 </div>
                                 <div class="mb-3">
                                     We believe in a world where you have total freedom to be you, without judgement. To experiment. To express yourself. 
                                 </div>
                                 <div class="author mb-2">
                                     Vũ Tuấn Ngọc
                                 </div>
                                 <div class="text-uppercase  ">
                                     Nhà thiết kế
                                 </div>
                                </div>
                             </div>
                             <div class="swiper-slide">
                                <div class="item-customer">
                                 <div class="avatar mb-4">
                                     <img src="/Themes/images/change/avatar-client.png" class="img-fluid"/>
                                 </div>
                                 <div class="mb-3">
                                     We believe in a world where you have total freedom to be you, without judgement. To experiment. To express yourself. 
                                 </div>
                                 <div class="author mb-2">
                                     Vũ Tuấn Ngọc
                                 </div>
                                 <div class="text-uppercase  ">
                                     Nhà thiết kế
                                 </div>
                                </div>
                             </div>
                        </div> 
                    
                      
                    </div>
                      <!-- If we need navigation buttons -->
                      <div class="swiper-button-prev"><i class="fas fa-chevron-left"></i></div>
                      <div class="swiper-button-next"><i class="fas fa-chevron-right"></i></div> 
                </div>
            </div>
        </div>
    </section>

    <section class="clients">
        <div class="container">
            <div class="swiper-container">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <div class="swiper-slide">
                        <img src="/Themes/images/change/about-client-1.png" class="img-fluid" alt=""/>
                    </div>
                    <div class="swiper-slide">
                        <img src="/Themes/images/change/about-client-2.png" class="img-fluid" alt=""/>
                    </div>
                    <div class="swiper-slide">
                        <img src="/Themes/images/change/about-client-3.png" class="img-fluid" alt=""/>
                    </div>
                    <div class="swiper-slide">
                        <img src="/Themes/images/change/about-client-4.png" class="img-fluid" alt=""/>
                    </div>
                    <div class="swiper-slide">          
                        <img src="/Themes/images/change/about-client-5.png" class="img-fluid" alt=""/>
                    </div>
                    <div class="swiper-slide">
                        <img src="/Themes/images/change/about-client-6.png" class="img-fluid" alt=""/>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
