﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/GoldHome.Master" AutoEventWireup="true" CodeBehind="LienHe.aspx.cs" Inherits="GoldLuck.Pages.LienHe" %>
<%@ Import Namespace="GoldLuck.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    
    <section class="form-contact-2 pb-5">
        <div class="container">
            <nav aria-label="breadcrumb ">
                <ol class="breadcrumb jan-breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li> 
                    <li class="breadcrumb-item active" aria-current="page">Liên hệc</li>
                </ol>
            </nav>
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-6 col-md-10 col-sm-12 col-12">
                    <h4 class="mb-3">Liên hệ</h4>
                    <div class=" h5 mb-3 font-weight-600">
                        Địa chỉ
                    </div>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.9082162551294!2d105.79755631440683!3d20.996315994262105!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac90c18a545f%3A0x5e0afd41f42165d!2zQ8O0bmcgdHkgVG5oaCDEkOG6p3UgVMawIHbDoCBUaHVvbmcgTeG6oWkgRW50ZXJidXkgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1587224570925!5m2!1svi!2s" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

                    <div class="w-50 my-3 ">
                        <div class="color-C6832B h5 mb-3 "><a href="">Trụ sở Rolux Gold </a></div>
                        <div class="d-flex mb-2">
                            <div class="icon mr-2">
                                <img src="/Themes/images/icon-store-1.svg" class="img-fluid">
                            </div>
                            <div class="text">
                                114 Khuất Duy Tiến - P.Nhân Chính - Q.Thanh Xuân
                            </div>
                        </div> 
                        <div class="d-flex mb-2">
                            <div class="icon mr-2">
                                <img src="/Themes/images/icon-store-3.svg" class="img-fluid">
                            </div>
                            <div class="text color-C6832B  ">
                                024.777.66666 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-10 col-sm-12 col-12">
                    <form class="form-contact-home mt-4">
                        <div class="  font-weight-bold h5  mb-4 color-333 text-center">Hãy liên hệ với chúng tôi </div>
                        <div class="form-row">
                            <div class="form-group col-12">
                                <input type="text" class="form-control" id=" " placeholder="Họ tên *">
                            </div>
                            
                        </div> 
                        <div class="form-row">
                            <div class="form-group col-12">
                                <input type="text" class="form-control" id=" " placeholder="Email *">
                            </div>
                            
                        </div> 
                        <div class="form-row">
                            <div class="form-group col-12">
                                <input type="text" class="form-control" id=" " placeholder="SĐT *">
                            </div>
                            
                        </div>  
                        
                        <div class="form-row mb-3">
                            <div class="form-group col-md-12">
                                <textarea type="text" rows="5" class="form-control" id=" " placeholder="Tin nhắn *"></textarea>
                            </div>
                        </div>
                        <div class="form-row justify-content-end">
                            <div class="form-group col-md-3 col-4   ">
                                <button class="btn btn-submit w-100" style="border-radius: 0;">Gửi</button>
                            </div>
                        </div>
                    
                    </form>
                </div>
            </div>
        </div>
    </section>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
