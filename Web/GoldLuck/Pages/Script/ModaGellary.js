﻿var pageId = 1;
modal = {
    init: function () {
        modal.registerEvent();
        modal.loadAnhByDanhmuc();
    },
    registerEvent: function () {
        modal.loadDataMd();
        modal.loadAnhByDanhmuc();
    },
    loadDataMd: function () {
        $(".immd").off('click').on('click', function () {
            var idAnh = $(this).data('id');
            var lang = $(this).data('lang');
            $('#modal-slide-imge').modal("show");

            R.Post({
                params: {
                    Id: idAnh,
                    lang: lang,
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "get-new-byid",

                success: function (res) {
                    var regex_get_picture = new RegExp(/\/[^'"/<>]+"/g);
                    var ltobjax =  res.Data.Body;
                    var anh = ltobjax.match(regex_get_picture);
                    var htm = "";
                    anh.forEach(function (element) {
                        var can = element.lastIndexOf("?");
                        var chuoidung = element.slice(0, can)
                        htm += '<div class="swiper-slide swiper-slide-visible ">' +
                            ' <div class="image">' +
                            ' <img src="/themes/images/change' + chuoidung + '" class="img-fluid   " alt="" />' +
                            ' </div>' +
                            ' </div>';

                    })
                    $('._binAnhById').html(htm);
                    //$('.myslide').html(res.Data.Body);
                    //$('.myslide>p').addClass('swiper-wrapper');
                    //$('.myslide-preview>p>img').addClass('my-img-slide');
                    $('._Bin1>div>div>img').addClass('swiper-slide');
                    $('._Bin2>div>div>img').addClass('slide-swipe-bt');

                    var galleryThumbs = new Swiper('.gallery-thumbs', {
                        slidesPerView: 5,
                        freeMode: true,
                        watchSlidesVisibility: true,
                        watchSlidesProgress: true,
                    });
                    var galleryTop = new Swiper('.gallery-top', {
                        navigation: {
                            nextEl: '.swiper-button-next',
                            prevEl: '.swiper-button-prev',
                        },
                        thumbs: {
                            swiper: galleryThumbs
                        }
                    });
                   
                }
            });
        });
    },
    loadAnhByDanhmuc: function () {
        $('#pills-home-tab').on('click', function () {
            $('#BinAnh').html("")
            var pageIndex = 1;
            var pageSize = 6;
            var lang = $(this).data('lang');
            R.Post({
                params: {
                    lang: lang,
                    type: 11,
                    pageIndex: pageIndex,
                    pageSize: pageSize,
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "load-san-pham",
                success: function (res) {
                    var htm = "";
                    for (var i = 0; i < res.Data.length; i++) {
                        htm +=
                            ' <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 px-2">' +
                            '  <a href="javascript:void(0);">' +
                            '<img src="/Themes/images/change/' + res.Data[i].Avatar + ' " class="img-fluid immd" data-id="' + res.Data[i].NewsId + '" data-lang="' + lang + '" alt="" />' +
                            '   </a>' +
                            ' </div >' +
                            ' </div >';
                    }

                    $('#BinAnh').html(htm);
                    if (htm === '') {
                        switch (lang) {
                            case "vi-VN":
                                htm += '<small>thư viện ảnh trống !</small>'
                                break;
                            case "ko-KR":
                                htm += '<small>  빈 사진 갤러리 !</small>'
                               
                                break;
                            case "en-US":
                                htm += '<small> Blank photo gallery !</small>'
                               

                                break;
                            case "zh-CN":
                                htm += '<small> 空白圖片庫 !</small>'
                               
                                break;
                        }

                        $('#sp').html(htm);
                    }
                    modal.loadDataMd();
                }

            })
        })
        $('#pills-profile-tab2').on('click', function () {

            $('#BinAnh2').html("")
            var pageIndex = 1;
            var pageSize = 6;
            var lang = $(this).data('lang');
            R.Post({
                params: {
                    lang: lang,
                    type: 11,
                    pageIndex: pageIndex,
                    pageSize: pageSize,
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "load-san-pham",
                success: function (res) {
                    var htm = "";
                    if (res.Data.length > 0) {
                        for (var i = 0; i < res.Data.length; i++) {

                            if (res.Data[i].MetaTitle === "MT1") {

                                htm += ' <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 px-2">' +
                                    '  <a href="javascript:void(0);">' +
                                    '<img src="/Themes/images/change/' + res.Data[i].Avatar + ' " class="img-fluid immd" data-id="' + res.Data[i].NewsId + '" data-lang="' + lang + '" alt="" />' +
                                    '   </a>' +
                                    ' </div >' +
                                    ' </div >';
                            }
                        }
                        $('#BinAnh2').html(htm);
                    }
                    else{
                        switch (lang) {
                            case "vi-VN":
                                htm += '<small>thư viện ảnh trống !</small>'
                                break;
                            case "ko-KR":
                                htm += '<small>  빈 사진 갤러리 !</small>'

                                break;
                            case "en-US":
                                htm += '<small> Blank photo gallery !</small>'
                                break;
                            case "zh-CN":
                                htm += '<small> 空白圖片庫 !</small>'

                                break;
                        }

                        $('#sp2').html(htm);
                    }
                    modal.loadDataMd();
                }
            })
        })
        $('#pills-profile-tab3').on('click', function () {

            $('#BinAnh3').html("")
            var pageIndex = 1;
            var pageSize = 6;
            var lang = $(this).data('lang');
            R.Post({
                params: {
                    lang: lang,
                    type: 11,
                    pageIndex: pageIndex,
                    pageSize: pageSize,
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "load-san-pham",
                success: function (res) {
                    var htm = "";
                    if (res.Data.length > 0) {
                        for (var i = 0; i < res.Data.length; i++) {

                            if (res.Data[i].MetaTitle === "MT2") {

                                htm += ' <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 px-2">' +
                                    '  <a href="javascript:void(0);">' +
                                    '<img src="/Themes/images/change/' + res.Data[i].Avatar + ' " class="img-fluid immd" data-id="' + res.Data[i].NewsId + '" data-lang="' + lang + '" alt="" />' +
                                    '   </a>' +
                                    ' </div >' +
                                    ' </div >';
                            }
                        }
                        $('#BinAnh3').html(htm);
                    }
                    else{
                        switch (lang) {
                            case "vi-VN":
                                htm += '<small>thư viện ảnh trống !</small>'
                                break;
                            case "ko-KR":
                                htm += '<small>  빈 사진 갤러리 !</small>'

                                break;
                            case "en-US":
                                htm += '<small> Blank photo gallery !</small>'


                                break;
                            case "zh-CN":
                                htm += '<small> 空白圖片庫 !</small>'

                                break;
                        }

                        $('#sp3').html(htm);
                    }
                    modal.loadDataMd();
                }
            })
        })
        $('#pills-profile-tab4').on('click', function () {

            $('#BinAnh').html("")
            var pageIndex = 1;
            var pageSize = 6;
            var lang = $(this).data('lang');
            R.Post({
                params: {
                    lang: lang,
                    type: 11,
                    pageIndex: pageIndex,
                    pageSize: pageSize,
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "load-san-pham",
                success: function (res) {
                    var htm = "";
                    if (res.Data.length > 0) {
                        for (var i = 0; i < res.Data.length; i++) {

                            if (res.Data[i].MetaTitle === "MT4") {

                                htm += ' <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 px-2">' +
                                    '  <a href="javascript:void(0);">' +
                                    '<img src="/Themes/images/change/' + res.Data[i].Avatar + ' " class="img-fluid immd" data-id="' + res.Data[i].NewsId + '" data-lang="' + lang + '" alt="" />' +
                                    '   </a>' +
                                    ' </div >' +
                                    ' </div >';
                            }
                        }
                        $('#BinAnh').html(htm);
                    }
                    if (htm === '') {
                        switch (lang) {
                            case "vi-VN":
                                htm += '<small>thư viện ảnh trống !</small>'
                                break;
                            case "ko-KR":
                                htm += '<small>  빈 사진 갤러리 !</small>'

                                break;
                            case "en-US":
                                htm += '<small> Blank photo gallery !</small>'


                                break;
                            case "zh-CN":
                                htm += '<small> 空白圖片庫 !</small>'

                                break;
                        }

                        $('#sp4').html(htm);
                    }
                    modal.loadDataMd();
                }
            })
        })

        $("#view_more_image1").off('click').on('click', function () {
            pageId += 1;
            var pageSize = 6;
            var lang = $(this).data('lang');
            R.Post({
                params: {
                    lang: lang,
                    type: 11,
                    pageIndex: pageId,
                    pageSize: pageSize,
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "load-san-pham",
                success: function (res) {
                    var htm = '';
                    if (res.Data.length > 0) {
                        for (var i = 0; i < res.Data.length; i++) {
                            htm +=
                                ' <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 px-2">' +
                                '  <a href="javascript:void(0);">' +
                                '<img src="/Themes/images/change/' + res.Data[i].Avatar + ' " class="img-fluid immd" data-id="' + res.Data[i].NewsId + '" data-lang="' + lang + '" alt="" />' +
                                '   </a>' +
                                ' </div >' +
                                ' </div >';
                        }

                        $('#BinAnh').append(htm);
                    }
                    else{
                        switch (lang) {
                            case "vi-VN":
                                htm += '<small>thư viện ảnh trống !</small>'
                                break;
                            case "ko-KR":
                                htm += '<small>  빈 사진 갤러리 !</small>'

                                break;
                            case "en-US":
                                htm += '<small> Blank photo gallery !</small>'


                                break;
                            case "zh-CN":
                                htm += '<small> 空白圖片庫 !</small>'

                                break;
                        }

                        $('#sp').html(htm);
                    }
                    modal.loadDataMd();
                }
            })
        })
        $("#view_more_image2").off('click').on('click', function () {
            pageId += 1;
            var pageSize = 6;
            var lang = $(this).data('lang');
            R.Post({
                params: {
                    lang: lang,
                    type: 11,
                    pageIndex: pageId,
                    pageSize: pageSize,
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "load-san-pham",
                success: function (res) {
                    var htm = "";
                    if (res.Data.length > 0) {
                        for (var i = 0; i < res.Data.length; i++) {
                            if (res.Data[i].MetaTitle === "MT1") {
                                htm +=
                                    ' <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 px-2">' +
                                    '  <a href="javascript:void(0);">' +
                                    '<img src="/Themes/images/change/' + res.Data[i].Avatar + ' " class="img-fluid immd" data-id="' + res.Data[i].NewsId + '" data-lang="' + lang + '" alt="" />' +
                                    '   </a>' +
                                    ' </div >' +
                                    ' </div >';
                            }
                        }

                        $('#BinAnh2').append(htm);
                    }
                    else {
                        switch (lang) {
                            case "vi-VN":
                                htm += '<small>thư viện ảnh trống !</small>'
                                break;
                            case "ko-KR":
                                htm += '<small>  빈 사진 갤러리 !</small>'

                                break;
                            case "en-US":
                                htm += '<small> Blank photo gallery !</small>'


                                break;
                            case "zh-CN":
                                htm += '<small> 空白圖片庫 !</small>'

                                break;
                        }

                        $('#sp2').html(htm);
                    }
                    modal.loadDataMd();
                }
            })
        })
        $("#view_more_image3").off('click').on('click', function () {
            pageId += 1;
            var pageSize = 6;
            var lang = $(this).data('lang');
            R.Post({
                params: {
                    lang: lang,
                    type: 11,
                    pageIndex: pageId,
                    pageSize: pageSize,
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "load-san-pham",
                success: function (res) {
                    var htm = '';
                    if (res.Data.length > 0) {
                        for (var i = 0; i < res.Data.length; i++) {
                            if (res.Data[i].MetaTitle === "MT2") {
                                htm +=
                                    ' <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 px-2">' +
                                    '  <a href="javascript:void(0);">' +
                                    '<img src="/Themes/images/change/' + res.Data[i].Avatar + ' " class="img-fluid immd" data-id="' + res.Data[i].NewsId + '" data-lang="' + lang + '" alt="" />' +
                                    '   </a>' +
                                    ' </div >' +
                                    ' </div >';
                            }
                        }

                        $('#BinAnh3').append(htm);
                    }
                    else if (htm === '') {
                        switch (lang) {
                            case "vi-VN":
                                htm += '<small>thư viện ảnh trống !</small>'
                                break;
                            case "ko-KR":
                                htm += '<small>  빈 사진 갤러리 !</small>'

                                break;
                            case "en-US":
                                htm += '<small> Blank photo gallery !</small>'


                                break;
                            case "zh-CN":
                                htm += '<small> 空白圖片庫 !</small>'

                                break;
                        }

                        $('#sp3').html(htm);
                    }
                    modal.loadDataMd();
                }
            })
        })
        $("#view_more_image4").off('click').on('click', function () {
            pageId += 1;
            var pageSize = 6;
            var lang = $(this).data('lang');
            R.Post({
                params: {
                    lang: lang,
                    type: 11,
                    pageIndex: pageId,
                    pageSize: pageSize,
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "load-san-pham",
                success: function (res) {
                    var htm = '';
                    if (res.Data.length > 0) {
                        for (var i = 0; i < res.Data.length; i++) {
                            if (res.Data[i].MetaTitle === "MT3") {
                                htm +=
                                    ' <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 px-2">' +
                                    '  <a href="javascript:void(0);">' +
                                    '<img src="/Themes/images/change/' + res.Data[i].Avatar + ' " class="img-fluid immd" data-id="' + res.Data[i].NewsId + '" data-lang="' + lang + '" alt="" />' +
                                    '   </a>' +
                                    ' </div >' +
                                    ' </div >';
                            }
                        }

                        $('#BinAnh4').append(htm);
                    }
                    else{
                        switch (lang) {
                            case "vi-VN":
                                htm += '<small>thư viện ảnh trống !</small>'
                                break;
                            case "ko-KR":
                                htm += '<small>  빈 사진 갤러리 !</small>'

                                break;
                            case "en-US":
                                htm += '<small> Blank photo gallery !</small>'


                                break;
                            case "zh-CN":
                                htm += '<small> 空白圖片庫 !</small>'

                                break;
                        }

                        $('#sp4').html(htm);
                    }
                    modal.loadDataMd();
                }
            })
        })
    }

}
modal.init();
