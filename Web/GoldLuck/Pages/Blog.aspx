﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/GoldHome.Master" AutoEventWireup="true" CodeBehind="Blog.aspx.cs" Inherits="GoldLuck.Pages.Blog" %>
<%@ Import Namespace="GoldLuck.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <div class="container mt-4">
        <div class="row flex-md-row-reverse">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12">
                <div class="list-menu-right">
                    <div class="heading">
                         Danh mục
                    </div>
                    <ul class="list">
                        <li><a href="" title="">Kiến thức sản phẩm</a></li>
                        <li><a href="" title="">Vật phẩm phong thủy</a></li>
                        <li><a href="" title="">Quà tặng cao cấp</a></li>
                        <li><a href="" title="">Nội thất - Ngoại thất</a></li> 
                    </ul>
                </div>
                <div class="list-menu-right">
                    <div class="heading ">
                         Khuyến mại
                    </div>
                    <div class="list lastest-news">
                        <div class="item large">
                            <div class="row">
                                <div class="col-12 ">
                                    <div class="image mb-3">
                                        <a href="" title=""><img src="images/change/blog-km-1.jpg" class="img-fluid w-100"
                                                alt=""></a>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <h6 class="title">
                                        <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội golden ...</a>
                                    </h6>
                                    <div class="des ">
                                        Choáng ngợp với độ xa hoa trên các thiết bị nội thất mạ vàng
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xl-3 col-md-4 col-sm-3 col-3 pr-0">
                                    <div class="image">
                                        <a href="" title=""><img src="images/change/lastest-img.jpg" class="img-fluid"
                                                alt=""></a>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-md-8 col-sm-9 col-9">
                                    <h6 class="title">
                                        <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng</a>
                                    </h6>
                                    <div class="time ">
                                        10/9/2019 10:46
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xl-3 col-md-4 col-sm-3 col-3 pr-0">
                                    <div class="image">
                                        <a href="" title=""><img src="images/change/lastest-img.jpg" class="img-fluid"
                                                alt=""></a>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-md-8 col-sm-9 col-9">
                                    <h6 class="title">
                                        <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng</a>
                                    </h6>
                                    <div class="time ">
                                        10/9/2019 10:46
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="list-menu-right">
                    <div class="heading  ">
                        Mới cập nhật
                    </div>
                    <div class="list lastest-news">
                        <div class="item large">
                            <div class="row">
                                <div class="col-12 ">
                                    <div class="image mb-3">
                                        <a href="" title=""><img src="images/change/blog-km-1.jpg" class="img-fluid w-100"
                                                alt=""></a>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <h6 class="title">
                                        <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội golden ...</a>
                                    </h6>
                                    <div class="des ">
                                        Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội golden ...
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xl-3 col-md-4 col-sm-3 col-3 pr-0">
                                    <div class="image">
                                        <a href="" title=""><img src="images/change/lastest-img.jpg" class="img-fluid"
                                                alt=""></a>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-md-8 col-sm-9 col-9">
                                    <h6 class="title">
                                        <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội golden ...</a>
                                    </h6>
                                    <div class="time ">
                                        10/9/2019 10:46
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xl-3 col-md-4 col-sm-3 col-3 pr-0">
                                    <div class="image">
                                        <a href="" title=""><img src="images/change/lastest-img.jpg" class="img-fluid"
                                                alt=""></a>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-md-8 col-sm-9 col-9">
                                    <h6 class="title">
                                        <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội golden ...</a>
                                    </h6>
                                    <div class="time ">
                                        10/9/2019 10:46
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xl-3 col-md-4 col-sm-3 col-3 pr-0">
                                    <div class="image">
                                        <a href="" title=""><img src="images/change/lastest-img.jpg" class="img-fluid"
                                                alt=""></a>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-md-8 col-sm-9 col-9">
                                    <h6 class="title">
                                        <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội golden ...</a>
                                    </h6>
                                    <div class="time ">
                                        10/9/2019 10:46
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xl-3 col-md-4 col-sm-3 col-3 pr-0">
                                    <div class="image">
                                        <a href="" title=""><img src="images/change/lastest-img.jpg" class="img-fluid"
                                                alt=""></a>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-md-8 col-sm-9 col-9">
                                    <h6 class="title">
                                        <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội golden ...</a>
                                    </h6>
                                    <div class="time ">
                                        10/9/2019 10:46
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-9 col-md-8 col-sm-12 col-12">
                <div class="heading pb-2    d-flex">
                    <div class="h5 font-weight-600 mb-0 ">
                        Kiến thức sản phẩm
                    </div>
                </div>
                <div class="slide-blog-cate swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <div class="swiper-slide">
                            <div class="item mb-3">
                                <a href="" title=""><img src="images/change/img-blog.jpg" alt=""  class="img-fluid w-100"/></a>
                                <h2 class="title">
                                    <a href="" title="">Choáng ngợp với độ xa hoa trên các thiết bị nội thất mạ vàng</a>

                                </h2>
                            </div>
                        </div> 
                        <div class="swiper-slide">
                            <div class="item mb-3">
                                <a href="" title=""><img src="images/change/img-blog.jpg" alt=""  class="img-fluid w-100"/></a>
                                <h2 class="title">
                                    <a href="" title="">Choáng ngợp với độ xa hoa trên các thiết bị nội thất mạ vàng</a>

                                </h2>
                            </div>
                        </div> 
                    </div>

                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev"><i class="fas fa-chevron-left"></i></div>
                    <div class="swiper-button-next"><i class="fas fa-chevron-right"></i></div>
                </div>


                <div class="list-blog-cate-2">
                    <div class="item item-blog-km">
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-12">
                                <div class="image mb-3 mb-sm-0">
                                    <a href="" title="">
                                        <img src="images/change/image-blog-cate-2.jpg" class="img-fluid w-100" alt=""/>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-8 col-sm-8  col-12">
                                <h5 class="title">
                                    Choáng ngợp với độ xa hoa trên các thiết bị nội thất mạ vàng
                                </h5>
                                <div class="time">
                                    10/9/2019  10:46
                                </div>
                                <div class="des">
                                    Choáng ngợp với độ xa hoa trên các thiết bị nội thất mạ vàng
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item item-blog-km">
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-12">
                                <div class="image mb-3 mb-sm-0">
                                    <a href="" title="">
                                        <img src="images/change/image-blog-cate-2.jpg" class="img-fluid w-100" alt=""/>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-8 col-sm-8  col-12">
                                <h5 class="title">
                                    Choáng ngợp với độ xa hoa trên các thiết bị nội thất mạ vàng
                                </h5>
                                <div class="time">
                                    10/9/2019  10:46
                                </div>
                                <div class="des">
                                    Choáng ngợp với độ xa hoa trên các thiết bị nội thất mạ vàng
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item item-blog-km">
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-12">
                                <div class="image mb-3 mb-sm-0">
                                    <a href="" title="">
                                        <img src="images/change/image-blog-cate-2.jpg" class="img-fluid w-100" alt=""/>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-8 col-sm-8  col-12">
                                <h5 class="title">
                                    Choáng ngợp với độ xa hoa trên các thiết bị nội thất mạ vàng
                                </h5>
                                <div class="time">
                                    10/9/2019  10:46
                                </div>
                                <div class="des">
                                    Choáng ngợp với độ xa hoa trên các thiết bị nội thất mạ vàng
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item item-blog-km">
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-12">
                                <div class="image mb-3 mb-sm-0">
                                    <a href="" title="">
                                        <img src="images/change/image-blog-cate-2.jpg" class="img-fluid w-100" alt=""/>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-8 col-sm-8  col-12">
                                <h5 class="title">
                                    Choáng ngợp với độ xa hoa trên các thiết bị nội thất mạ vàng
                                </h5>
                                <div class="time">
                                    10/9/2019  10:46
                                </div>
                                <div class="des">
                                    Choáng ngợp với độ xa hoa trên các thiết bị nội thất mạ vàng
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item item-blog-km">
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-12">
                                <div class="image mb-3 mb-sm-0">
                                    <a href="" title="">
                                        <img src="images/change/image-blog-cate-2.jpg" class="img-fluid w-100" alt=""/>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-8 col-sm-8  col-12">
                                <h5 class="title">
                                    Choáng ngợp với độ xa hoa trên các thiết bị nội thất mạ vàng
                                </h5>
                                <div class="time">
                                    10/9/2019  10:46
                                </div>
                                <div class="des">
                                    Choáng ngợp với độ xa hoa trên các thiết bị nội thất mạ vàng
                                </div>
                            </div>
                        </div>
                    </div>
                     

                </div>
                <div class="jan-pagination text-right mb-4 ">
                    <button class="btn btn-pagi " title=""><i class="fas fa-chevron-left "></i></button>
                    <select class="select-page mr-2">
                        <option>1</option>
                    </select>
                    <span class="color-828282">/10</span>
                    <button class="btn btn-pagi " title=""><i class="fas fa-chevron-right "></i></button>
                </div>


               
            </div>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
