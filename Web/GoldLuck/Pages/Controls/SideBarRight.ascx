﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SideBarRight.ascx.cs" Inherits="GoldLuck.Pages.Controls.SideBarRight" %>
<%@ Import Namespace="GoldLuck.Core.Helper" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Resources" %>
<% var currentLanguageJavaCode = Current.LanguageJavaCode;
    var currentLanguage = Current.Language;%>
<div class="blog-ss-right ">
    <div class="heading ">
        <div><%=Language.MostRead %></div>
    </div>
    <%var topView = NewsBo.GetMostedViewNews(Current.LanguageJavaCode);
        for (int i = 0; i < topView.Count(); i++)
        {%>

    <%  

        if (i == 0)
        { %>

    <div class="item-left mb-3">
        <div class="image">
            <a href="/<%=currentLanguage %>/blog/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="">
                <img src="/Uploads/<%=topView[i].Avatar %>" alt="" /></a>
        </div>
        <h2 class="title">
            <a href="/<%=currentLanguage %>/blog/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title=""><%=topView[i].Title %></a>
        </h2>
        <div class="detail">
            <%=Utility.SubWordInString(topView[i].Sapo.GetText(),40) %>
        </div>

    </div>
    <%} %>
    <%if (i > 0)
        { %>
    <div class="item-right d-flex">
        <div class="image">
            <a href="/<%=currentLanguage %>/blog/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="">
                <img src="/Uploads/thumb/<%=topView[i].Avatar %>" alt="" /></a>
        </div>
        <div class="text">
            <h2 class="title">
                <a href="/<%=currentLanguage %>/blog/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title=""><%=topView[i].Title %></a>
            </h2>
            <div class="time">
                <%=UIHelper.GetOnlyCurentDate(topView[i].CreatedDate,currentLanguageJavaCode) %>
            </div>
        </div>

    </div>
    <%} %>
    <%} %>
</div>
<div class="blog-ss-right ">
    <div class="heading ">
        Video
    </div>
    <%var mediaList = NewsBo.GetTop4VideoNews(currentLanguageJavaCode);
        foreach (var item in mediaList)
        {

    %>
    <% string data = item.Body;
        string reg = "\"(h.*?)\"";
        data = Regex.Matches(data, reg)[0].ToString().Replace("\"", "").Replace("\"", "").Replace(@"watch?v=", @"embed\");
    %>
    <iframe width="100%" height="100%" class="mb-3" src="<%=data %>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <%}%>


    <%-- <iframe width="100%" height="100%" class="mb-3" src="https://www.youtube.com/embed/YlGsuRAUdG8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                        <iframe width="100%" height="100%" class="mb-3" src="https://www.youtube.com/embed/YlGsuRAUdG8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                        <iframe width="100%" height="100%" class="mb-3" src="https://www.youtube.com/embed/YlGsuRAUdG8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>--%>
</div>
