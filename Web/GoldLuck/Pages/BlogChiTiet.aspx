﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/GoldHome.Master" AutoEventWireup="true" CodeBehind="BlogChiTiet.aspx.cs" Inherits="GoldLuck.Pages.BlogChiTiet" %>
<%@ Import Namespace="GoldLuck.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
     <div class="container mt-4">
        <div class="row flex-md-row-reverse">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12">
                <div class="list-menu-right">
                    <div class="heading">
                         Danh mục
                    </div>
                    <ul class="list">
                        <li><a href="" title="">Kiến thức sản phẩm</a></li>
                        <li><a href="" title="">Vật phẩm phong thủy</a></li>
                        <li><a href="" title="">Quà tặng cao cấp</a></li>
                        <li><a href="" title="">Nội thất - Ngoại thất</a></li> 
                    </ul>
                </div>
                <div class="list-menu-right">
                    <div class="heading ">
                         Khuyến mại
                    </div>
                    <div class="list lastest-news">
                        <div class="item large">
                            <div class="row">
                                <div class="col-12 ">
                                    <div class="image mb-3">
                                        <a href="" title=""><img src="images/change/blog-km-1.jpg" class="img-fluid w-100"
                                                alt=""></a>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <h6 class="title">
                                        <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội golden ...</a>
                                    </h6>
                                    <div class="des ">
                                        Choáng ngợp với độ xa hoa trên các thiết bị nội thất mạ vàng
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xl-3 col-md-4 col-sm-3 col-3 pr-0">
                                    <div class="image">
                                        <a href="" title=""><img src="images/change/lastest-img.jpg" class="img-fluid"
                                                alt=""></a>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-md-8 col-sm-9 col-9">
                                    <h6 class="title">
                                        <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng</a>
                                    </h6>
                                    <div class="time ">
                                        10/9/2019 10:46
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xl-3 col-md-4 col-sm-3 col-3 pr-0">
                                    <div class="image">
                                        <a href="" title=""><img src="images/change/lastest-img.jpg" class="img-fluid"
                                                alt=""></a>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-md-8 col-sm-9 col-9">
                                    <h6 class="title">
                                        <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng</a>
                                    </h6>
                                    <div class="time ">
                                        10/9/2019 10:46
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="list-menu-right">
                    <div class="heading  ">
                        Mới cập nhật
                    </div>
                    <div class="list lastest-news">
                        <div class="item large">
                            <div class="row">
                                <div class="col-12 ">
                                    <div class="image mb-3">
                                        <a href="" title=""><img src="images/change/blog-km-1.jpg" class="img-fluid w-100"
                                                alt=""></a>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <h6 class="title">
                                        <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội golden ...</a>
                                    </h6>
                                    <div class="des ">
                                        Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội golden ...
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xl-3 col-md-4 col-sm-3 col-3 pr-0">
                                    <div class="image">
                                        <a href="" title=""><img src="images/change/lastest-img.jpg" class="img-fluid"
                                                alt=""></a>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-md-8 col-sm-9 col-9">
                                    <h6 class="title">
                                        <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội golden ...</a>
                                    </h6>
                                    <div class="time ">
                                        10/9/2019 10:46
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xl-3 col-md-4 col-sm-3 col-3 pr-0">
                                    <div class="image">
                                        <a href="" title=""><img src="images/change/lastest-img.jpg" class="img-fluid"
                                                alt=""></a>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-md-8 col-sm-9 col-9">
                                    <h6 class="title">
                                        <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội golden ...</a>
                                    </h6>
                                    <div class="time ">
                                        10/9/2019 10:46
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xl-3 col-md-4 col-sm-3 col-3 pr-0">
                                    <div class="image">
                                        <a href="" title=""><img src="images/change/lastest-img.jpg" class="img-fluid"
                                                alt=""></a>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-md-8 col-sm-9 col-9">
                                    <h6 class="title">
                                        <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội golden ...</a>
                                    </h6>
                                    <div class="time ">
                                        10/9/2019 10:46
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xl-3 col-md-4 col-sm-3 col-3 pr-0">
                                    <div class="image">
                                        <a href="" title=""><img src="images/change/lastest-img.jpg" class="img-fluid"
                                                alt=""></a>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-md-8 col-sm-9 col-9">
                                    <h6 class="title">
                                        <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội golden ...</a>
                                    </h6>
                                    <div class="time ">
                                        10/9/2019 10:46
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-9 col-md-8 col-sm-12 col-12 mb-5">
                <nav aria-label="breadcrumb ">
                    <ol class="breadcrumb jan-breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li> 
                        <li class="breadcrumb-item active" aria-current="page">Tin tức</li>
                    </ol>
                </nav>
                <div class="blog-content">
                    <h1 class="title">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội golden ...</h1>
                    <div class="d-flex mb-4">
                        <div class="author color-828282 mr-4">
                            <div class="avt align-self-center">
                                <img src="images/change/avt-author.png" class="img-fluid" alt=""/>
                            </div>
                            <div class="align-self-center">
                                S.Rogers
                            </div>
                        </div>
                        <div class="time mb-0">
                            10/9/2019  10:46
                        </div>
                    </div>
                    <div class="detail color-4f4f4f">
                        <img src="images/change/img-blog.jpg" class="img-fluid w-100 mb-4" alt=""/>
                        <p>
                            Nói về Golden Bay, nếu ta dừng lại ở chất lượng phục vụ 5 sao với những dịch vụ nghỉ dưỡng đẳng cấp, thì đó quả là một thiếu sót lớn, bởi điểm lưu trú này còn tuyệt vời hơn thế. Nằm ngay tại điểm gặp gỡ giữa dòng sông Hàn, biển Đông và bán đảo Sơn Trà, Golden Bay là địa điểm hiếm hoi mà bạn có thể trải nghiệm nét đẹp quyến rũ của thiên nhiên Đà Nẵng.
                        </p>
                        <p class="font-weight-600">
                            Đến các đồ dùng nhỏ bé đều được dát vàng 24K
                        </p>
                        <p>
                            Golden Bay sở hữu phong cách thiết kế hòa quyện độc đáo giữa đường nét hiện đại của phương Tây và màu sắc bí ẩn đậm chất Á Đông. Khách sạn này sẽ khiến mọi du khách phải choáng váng với phần nội thất dát vàng 24K đẳng cấp và sang chảnh hàng đầu.
                        </p>


                    </div>
                    
                    <div class="d-flex facebook-btn justify-content-end my-4">
                        <div class="small mr-3"><img src="images/eye.svg" class="img-fluid mr-2"/>120 lượt xem</div>
                        <button class="btn mr-2"><i class="fas fa-thumbs-up mr-1"></i>Thích <span>1.4K</span></button>
                        <button href="" class="btn">Chia sẻ</button>
                    </div>
                    <!--Bài viết liên quan-->
                    <div class="my-3">
                        <div class="row mb-3  ">
                            <div class="col-12">
                                <a href="" class="h5 font-weight-bold ">Bài viết liên quan</a>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-xl-3 col-md-6 col-sm-6 col-6">
                                <div class="item-blog-km">
                                    <div class="image mb-3">
                                        <a href=""><img src="images/change/bai-viet-lq.jpg" class="img-fluid" alt="" /></a>
                                    </div>
                                    <div class="">
                                        <h5 class="title h6 font-weight-bold">
                                            <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội</a>
                                        </h5>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 col-sm-6 col-6">
                                <div class="item-blog-km">
                                    <div class="image mb-3">
                                        <a href=""><img src="images/change/bai-viet-lq.jpg" class="img-fluid" alt="" /></a>
                                    </div>
                                    <div class="">
                                        <h5 class="title h6 font-weight-bold">
                                            <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội</a>
                                        </h5>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 col-sm-6 col-6">
                                <div class="item-blog-km">
                                    <div class="image mb-3">
                                        <a href=""><img src="images/change/bai-viet-lq.jpg" class="img-fluid" alt="" /></a>
                                    </div>
                                    <div class="">
                                        <h5 class="title h6 font-weight-bold">
                                            <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội</a>
                                        </h5>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 col-sm-6 col-6">
                                <div class="item-blog-km">
                                    <div class="image mb-3">
                                        <a href=""><img src="images/change/bai-viet-lq.jpg" class="img-fluid" alt="" /></a>
                                    </div>
                                    <div class="">
                                        <h5 class="title h6 font-weight-bold">
                                            <a href="" title="">Nội thất cao cấp 7 sao mạ vàng ròng của khách sạn hà nội</a>
                                        </h5>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Tag-->
                    <div class="tags">
                        <label class="">Từ khóa:</label>
                        <a href="" class="item btn-link color-C6832B">mạ vàng</a>
                        <a href="" class="item btn-link color-C6832B">nội thất</a>
                        <a href="" class="item btn-link color-C6832B">quà tặng</a>
                        <a href="" class="item btn-link color-C6832B">vàng</a> 

                    </div>
                   
                </div>
                 <!--Comment fb--> 
                
                 <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0"></script>
                 <div class="fb-comments" data-href="https://www.facebook.com/Rolux-Gold-107151547543683/" data-numposts="5" data-width="100%"></div>
            </div>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
