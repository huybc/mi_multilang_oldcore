﻿using System;
using System.IO.Compression;
using System.Web;
using System.Web.Routing;

namespace GoldLuck
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {

            RegisterRoutes(RouteTable.Routes);
        }
        public Global()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            //  this.PostReleaseRequestState += new EventHandler(Global_PostReleaseRequestState);
        }
        public static void RegisterRoutes(RouteCollection routes)
        {
            //vintravel
            //cms

            routes.MapPageRoute("Index", "{languageCode}/.htm", "~/default.aspx");
            routes.MapPageRoute("gioi-thieu", "{languageCode}/gioi-thieu.htm", "~/Pages/GioiThieu.aspx");
            routes.MapPageRoute("Blog", "{languageCode}/tin-tuc.htm", "~/Pages/Blog.aspx");
            routes.MapPageRoute("San-pham", "{languageCode}/San-pham.htm", "~/Pages/SanPham.aspx");
            routes.MapPageRoute("blog-detail-remaster", "{languageCode}/blog-chi-tiet/{alias}/{url}.{id}.htm", "~/Pages/BlogChiTiet.aspx");
            routes.MapPageRoute("lien-he", "{languageCode}/lien-he.htm", "~/Pages/LienHe.aspx");
            routes.MapPageRoute("Gio-hang", "{languageCode}/gio-hang.htm", "~/Pages/GioHang.aspx");
            routes.MapPageRoute("spa-chi-tiet", "{languageCode}/spa-chi-tiet/{alias}/{id}.htm", "~/Pages/SanPhamChiTiet.aspx");


            //routes.MapPageRoute("chi-tiet-lich-trinh", "{languageCode}/tour/{alias}.{id}.htm", "~/Pages/tour-detail.aspx");
            //routes.MapPageRoute("Bo-suu-tap", "{languageCode}/Thu-vien-anh/{url}/{id}.htm", "~/Pages/gallery.aspx");
            //routes.MapPageRoute("chi-tiet-san-pham", "{languageCode}/san-pham/{alias}.{id}.htm", "~/Pages/product-detail.aspx");
            //routes.MapPageRoute("nha-hang", "{languageCode}/nha-hang.htm", "~/Pages/restaurent.aspx");
            //routes.MapPageRoute("Our-Spa", "{languageCode}/spa/{url}/{id}.htm", "~/Pages/Spa.aspx");
            //routes.MapPageRoute("Yoga", "{languageCode}/yoga/{url}/{id}.htm", "~/Pages/Yoga.aspx");
            //routes.MapPageRoute("spa-remaster", "{languageCode}/spa.htm", "~/Pages/Spa.aspx");
            //routes.MapPageRoute("khach-san-remaster", "{languageCode}/khach-san/{alias}.{id}.htm", "~/Pages/room.aspx");
            //routes.MapPageRoute("khach-san", "{languageCode}/khach-san.htm", "~/Pages/room.aspx");
            //routes.MapPageRoute("dich-vu-chung-remaster", "{languageCode}/{zoneAlias}/{url}.{id}.htm", "~/Pages/spa-deltail.aspx");
            //routes.MapPageRoute("yoga-remaster", "{languageCode}/yoga.htm", "~/Pages/Yoga.aspx");
            //routes.MapPageRoute("room-remaster", "{languageCode}/room/{alias}/{id}.htm", "~/Pages/room.aspx");
            //routes.MapPageRoute("blog-remaster", "{languageCode}/blog", "~/Pages/blog.aspx");
            //routes.MapPageRoute("blog-list-remaster", "{languageCode}/{alias}.b{id}.htm", "~/Pages/blog-list.aspx");
            //routes.MapPageRoute("tuyen-dung", "{languageCode}/tuyen-dung", "~/Pages/HL_TuyenDung.aspx");
            //routes.MapPageRoute("thu-vien-anh", "{languageCode}/thu-vien-anh", "~/Pages/HL_ThuVienAnh.aspx");
            //routes.MapPageRoute("tim-kiem", "{languageCode}/{tim-kiem}", "~/Pages/HL_TimKiem.aspx");
            //{languageCode}/{tim-kiem-1}.html -=> HL_TimKiem1.aspx
            //{languageCode}/{type}/{Url}.{id}.html -=> HL_ChiTiet.aspx
            //var extras = "";
            //switch/case
            //routes.MapPageRoute("tim-kiem-tag", "{languageCode}/tim-kiem-tag", "~/Pages/HL_TimKiemTag.aspx");
            //routes.MapPageRoute("thu-vien-anh-detail", "{languageCode}/thu-vien-anh/{url}.{id}.htm", "~/Pages/HL_ThuVienAnhDetail.aspx");
            //routes.MapPageRoute("baiviet-detail", "{languageCode}/{type}/{zone}/{url}.{id}.htm", "~/Pages/HL_BaiVietDetail.aspx");
            //routes.MapPageRoute("danhmuc-chuyenkhoa", "{languageCode}/{type}/{zone}", "~/Pages/HL_DSBaiVietChuyenKhoa.aspx");
            //routes.MapPageRoute("cau-hoi-thuong-gap", "{languageCode}/thong-tin-suc-khoe-a-z", "~/Pages/HL_FAQ.aspx");
            //routes.MapPageRoute("lien-he", "{languageCode}/lien-he", "~/Pages/HL_LienHe.aspx");
            //routes.MapPageRoute("dat-lich-kham", "{languageCode}/dat-lich-kham", "~/Pages/HL_DatLichKham.aspx");
            //routes.MapPageRoute("xac-nhan-dat-lich", "{languageCode}/xac-nhan-dat-lich", "~/Pages/HL_XacNhanDatLich.aspx");


            //routes.MapPageRoute("travel-handbook", "{languageCode}/travel-handbook.htm", "~/Pages/TravelHandbook.aspx");
            ////routes.MapPageRoute("blog", "{languageCode}/blog.htm", "~/Pages/Blog.aspx");
            //routes.MapPageRoute("HotTour", "{languageCode}/hot-tour.htm", "~/Pages/HotTour.aspx");
            //routes.MapPageRoute("about", "{languageCode}/about.htm", "~/Pages/About.aspx");
            //routes.MapPageRoute("terms", "{languageCode}/terms.htm", "~/Pages/terms.aspx");
            //routes.MapPageRoute("policy", "{languageCode}/policy.htm", "~/Pages/Policy.aspx");
            //routes.MapPageRoute("tour", "{languageCode}/tour", "~/Pages/Tours.aspx");
            //routes.MapPageRoute("locationByZone", "{languageCode}/location/{locationName}", "~/Pages/Location.aspx");
            //routes.MapPageRoute("tourDetail", "{languageCode}/tour/{title}.{id}.htm", "~/Pages/TourDetail.aspx");

            //routes.MapPageRoute("blog-detail", "{languageCode}/blog/{title}.{id}.htm", "~/Pages/BlogDetail.aspx");
            //routes.MapPageRoute("blog-categories", "{languageCode}/blog/{zoneName}", "~/Pages/BlogCategories.aspx");
            //routes.MapPageRoute("blog-categories-page", "{languageCode}/blog/{zoneName}/{page}", "~/Pages/BlogCategories.aspx");

            //routes.MapPageRoute("contact", "{languageCode}/contact.htm", "~/Pages/Contact.aspx");
            //routes.MapPageRoute("voucher", "{languageCode}/voucher.htm", "~/Pages/UuDai.aspx");

            //routes.MapPageRoute("Filter", "filter/{locationId}/{startDate}.{minPrice}.{maxPrice}", "~/Pages/FilterTours.aspx");

            //routes.MapPageRoute("FilterTour", "filter/{locationId}/{startDate}.{minPrice}.{maxPrice}", "~/Pages/FilterTours.aspx");
            //routes.MapPageRoute("FilterTour_param", "tour-filter", "~/Pages/FilterTours.aspx");
            //routes.MapPageRoute("FilterHotel", "hotel-filter/{locationId}/{minPrice}.{maxPrice}", "~/Pages/FilterHotel.aspx");
            //routes.MapPageRoute("FilterHotel_param", "hotel-filter", "~/Pages/FilterHotel.aspx");
            ////routes.MapPageRoute("blog-detail", "blog.htm", "~/Pages/Blog.aspx");
            //routes.MapPageRoute("blog", "blog.htm", "~/Pages/Blog.aspx");

            //    routes.MapPageRoute("NotFound", "{languageCode}/not-found.htm", "~/Pages/NotFound.aspx");


            //cms
            routes.MapPageRoute("CMSHotel", "cpanel/hotel.htm", "~/CMS/Pages/Hotel.aspx");
            routes.MapPageRoute("Configs", "cpanel/configs.htm", "~/CMS/Pages/configs.aspx");
            routes.MapPageRoute("projects", "cpanel/projects.htm", "~/CMS/Pages/Projects.aspx");
            routes.MapPageRoute("CMStour", "cpanel/tour.htm", "~/CMS/Pages/Tour.aspx");
            routes.MapPageRoute("customers", "cpanel/customers.htm", "~/CMS/Pages/Customers.aspx");
            routes.MapPageRoute("Dashbroad", "cpanel/dashbroad.htm", "~/CMS/Index.aspx");
            routes.MapPageRoute("Products", "cpanel/products", "~/CMS/Pages/Products.aspx");
            routes.MapPageRoute("ProductsAdd", "cpanel/product/add", "~/CMS/Pages/ProductEdit.aspx");
            routes.MapPageRoute("ProductsEdit", "cpanel/product/edit", "~/CMS/Pages/ProductEdit.aspx");
            routes.MapPageRoute("NewsAdd", "cpanel/news/add", "~/CMS/Pages/NewsEdit.aspx");
            routes.MapPageRoute("NewsEdit", "cpanel/news/edit", "~/CMS/Pages/NewsEdit.aspx");
            //routes.MapPageRoute("CMSNews", "cpanel/projects.htm", "~/CMS/Pages/Game.aspx");
            routes.MapPageRoute("CMSNewsV2", "cpanel/newsv2.htm", "~/CMS/Pages/NewsV2.aspx");
            routes.MapPageRoute("Game", "cpanel/services.htm", "~/CMS/Pages/Services.aspx");
            routes.MapPageRoute("Media", "cpanel/media", "~/CMS/Pages/Media.aspx");
            routes.MapPageRoute("CMSPhoto", "cpanel/photo", "~/CMS/Pages/Photo.aspx");
            routes.MapPageRoute("Sliders", "cpanel/sliders-{type}.htm", "~/CMS/Pages/Sliders.aspx");
            routes.MapPageRoute("Sliders-landing-page", "cpanel/laningPage-{landing}-{type}.htm", "~/CMS/Pages/Slider_LandingPage.aspx");
            routes.MapPageRoute("Sliders2", "cpanel/sliders.htm", "~/CMS/Pages/Sliders.aspx");
            routes.MapPageRoute("login", "cpanel/login.htm", "~/CMS/Login.aspx");
            routes.MapPageRoute("login1", "cpanel.htm", "~/CMS/Login.aspx");

            routes.MapPageRoute("zone", "cpanel/zone-{zoneType}.htm", "~/CMS/Pages/Zone.aspx");

            routes.MapPageRoute("zoneEdit2", "cpanel/conf/zone/edit-{zoneType}-{Id}.htm", "~/CMS/Pages/ZoneEdit.aspx");
            routes.MapPageRoute("zoneEdit", "cpanel/conf/zone/edit-{zoneType}.htm", "~/CMS/Pages/ZoneEdit.aspx");

            routes.MapPageRoute("location", "cpanel/location.htm", "~/CMS/Pages/Location.aspx");
            routes.MapPageRoute("newsEditV2", "cpanel/conf/news/edit-{id}.htm", "~/CMS/Pages/NewsV2Edit.aspx");
            //routes.MapPageRoute("StystemUser", "system-user", "~/Pages/StystemUser.aspx");
            // Client
            //routes.MapPageRoute("ListNews2", "sp/{productType}.htm", "~/Pages/ProductType.aspx");
            //routes.MapPageRoute("NewsByTag", "tag/{tagName}.htm", "~/Pages/NewsByTag.aspx");
            //routes.MapPageRoute("Contact", "lien-he.htm", "~/Pages/Contact.aspx");
            //routes.MapPageRoute("hoi-dap", "hoi-dap.htm", "~/Pages/FAQ.aspx");
            //routes.MapPageRoute("Recruitment", "tuyen-dung.htm", "~/Pages/Recruitment.aspx");
            //routes.MapPageRoute("doi-tac", "doi-tac.htm", "~/Pages/CoOperater.aspx");
            //routes.MapPageRoute("ShareHolders", "co-dong.htm", "~/Pages/ShareHolders.aspx");
            //routes.MapPageRoute("About", "gioi-thieu.htm", "~/Pages/About.aspx");
            //routes.MapPageRoute("Terms", "dieu-khoan-su-dung.htm", "~/Pages/Terms.aspx");
            //routes.MapPageRoute("Privacy", "bao-mat.htm", "~/Pages/Privacy.aspx");

            //routes.MapPageRoute("ListNews", "tin-tuc.htm", "~/Pages/ListNews.aspx");

            //routes.MapPageRoute("TuVan", "tin-tuc.htm", "~/Pages/ListNews.aspx");
            //routes.MapPageRoute("Project", "du-an.htm", "~/Pages/ListProjects.aspx");

            //routes.MapPageRoute("ProjectDetail", "du-an/{zoneName}/{title}.{id}.htm", "~/Pages/ProjectDetail.aspx");

            //routes.MapPageRoute("Articles4", "phan-tich-nhan-dinh/{zoneName}", "~/Pages/Articles.aspx");
            //routes.MapPageRoute("ListProjects", "du-an.htm", "~/Pages/Articles.aspx");
            //routes.MapPageRoute("NotFound", "not-found.htm", "~/Pages/NotFound.aspx");

            //routes.MapPageRoute("NewsDetail", "{title}-{id}.htm", "~/Pages/NewsDetail.aspx");
            //routes.MapPageRoute("NewsDetail2", "{zoneName}/{title}.{id}.htm", "~/Pages/NewsDetail.aspx");
            //routes.MapPageRoute("Articles1", "{zoneName}", "~/Pages/Articles.aspx");
            //routes.MapPageRoute("Articles2", "du-an/{zoneName}", "~/Pages/Articles.aspx");
            //routes.MapPageRoute("Articles3", "tu-van/{zoneName}", "~/Pages/Articles.aspx");

            //routes.MapPageRoute("_log.gif", "_log.gif", "~/log.aspx");
        }
        private void Global_PostReleaseRequestState(object sender, EventArgs e)
        {
            string contentType = Response.ContentType; // Get the content type.

            // Compress only html and stylesheet documents.
            if (contentType == "text/html" || contentType == "text/css")
            {
                // Get the Accept-Encoding header value to know whether zipping is supported by the browser or not.
                string acceptEncoding = Request.Headers["Accept-Encoding"];

                if (!string.IsNullOrEmpty(acceptEncoding))
                {
                    // If gzip is supported then gzip it else if deflate compression is supported then compress in that technique.
                    if (acceptEncoding.Contains("gzip"))
                    {
                        // Compress and set Content-Encoding header for the browser to indicate that the document is zipped.
                        Response.Filter = new GZipStream(Response.Filter, CompressionMode.Compress);
                        Response.AppendHeader("Content-Encoding", "gzip");
                    }
                    else if (acceptEncoding.Contains("deflate"))
                    {
                        // Compress and set Content-Encoding header for the browser to indicate that the document is zipped.
                        Response.Filter = new DeflateStream(Response.Filter, CompressionMode.Compress);
                        Response.AppendHeader("Content-Encoding", "deflate");
                    }
                }
            }
        }
        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            var path = HttpContext.Current.Request.Url.AbsolutePath;
            HttpContext context = ((HttpApplication)sender).Context;
            if (path.ToLower().Contains("/templates/") || path.ToLower().Contains("/template/"))
            {
                context.Response.Write("Access denied");
                context.Response.End();
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = Server.GetLastError().GetBaseException();
            //  Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}