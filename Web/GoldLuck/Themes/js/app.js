new WOW().init();
 
function sliderclients() {
    var slidercate = new Swiper('.clients .swiper-container', {
        slidesPerView: 6,
        spaceBetween: 20,
        loop: true,
        autoplay:true,
        breakpoints: {
            320: {
                slidesPerView: 2.5,
                spaceBetween: 10,
            },
            767: {
                slidesPerView: 3,
            },
            991: {
                slidesPerView: 4,
            },
            1023: {
                slidesPerView: 5,
            },
            1365: {
                slidesPerView: 6,
            },
        },

    });
}
sliderclients();  


function topFunction() {
    $('body,html').animate({
        scrollTop: 0                       // Scroll to top of body
    }, 500);
}

function expand(){
    $(".content-cate-intro").toggleClass("expand");
}

function expanddetail(){
    $(".tab-content-rolux").toggleClass("expand");
}

$(".list-menu-cate .list-group-item:first-child").click(function(){
    $(this).closest(".list-menu-cate").toggleClass("un-expand");
})


$(window).scroll(function () {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('.header').addClass("fixed");    // Fade in the arrow
     //   $(".list-menu-cate").addClass("un-expand");
       
    } else {
        $('.header').removeClass("fixed");   // Else fade out the arrow
        
    }
});



function slidereivew() {
    var slidereivew = new Swiper('.review .swiper-container', {
        slidesPerView: 1,
        spaceBetween: -1,
        loop: true,
        autoplay:true, 
        navigation: {
            nextEl: '.review .swiper-button-next',
            prevEl: '.review .swiper-button-prev',
        },

    });
}
slidereivew();

function spvx(){
    $(".menu-fix .spvx").toggleClass("show");
}




  $(".select-local .dropdown-item").click(function(){
      var text=$(this).text();
      $(this).closest(".select-local").find(".dropdown-toggle").text(text);
  })

  $(".list-menu-right .heading").click(function(){
    $(this).closest(".list-menu-right").toggleClass("expand");
})

$(".star-rating i").click(function(){
    $(this).parent().find(".star-rating i").removeClass("checked");
    $(this).addClass("checked");
    $(this).prevAll().addClass("checked");
    $(this).nextAll().removeClass("checked");
    
  });
 
  

function slideCate(){
    
  var sliderBlogcate = new Swiper('.slide-blog-cate.swiper-container', {
    slidesPerView: 1,
    spaceBetween: 15,
    loop: true,
    speed: 600,
    autoplay: true,
    navigation: {
        nextEl: '.slide-blog-cate .swiper-button-next',
        prevEl: '.slide-blog-cate .swiper-button-prev',
    },

});
}
slideCate(); 

 

$(".tab-flash-sale .link-tab-flash-sale").click(function(){
    $(".tab-flash-sale .link-tab-flash-sale").removeClass("active");
    $(this).addClass("active");
})

function sidevideo() {
    var sidevideo = new Swiper('.slide-video .swiper-container', {
        slidesPerView: 4,
        spaceBetween: 20,
        autoplay:true,
        loop: true,
        breakpoints: {
            320: {
                slidesPerView: 1.2,
            },
            567: {
                slidesPerView: 1.5,
            },
            767: {
                slidesPerView: 2.3,
            },
            991: {
                slidesPerView: 3,
            },
            1023: {
                slidesPerView: 3.5,
            },
            1365: {
                slidesPerView: 3.5,
            },
        },

    });
}

sidevideo() ;


function expandProductDes(){
    $(".product-description").toggleClass("expand");
}

 

 

$(".btn-x").click(function(){
    $(".btn-x").removeClass("active");
    $(this).toggleClass("active");
});

function slidethumbhome(){
    var galleryTop3 = new Swiper('.slide-banner-home .gallery-top-home', {
        spaceBetween: 10,
        autoplay:true,
         
      });
    
  }

  slidethumbhome();

 
 
    var galleryThumbsproduct = new Swiper('.product-detail .gallery-thumbs', {
        spaceBetween: 15,
        slidesPerView: 4,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        direction: 'vertical',
        breakpoints: {
              320: {
                  slidesPerView: 3,
              },
              567: {
                  slidesPerView: 3,
              },
              767: {
                  slidesPerView: 3,
              },
              991: {
                  slidesPerView: 3,
                  spaceBetween: 20,
              }, 
              1365: {
                  slidesPerView: 4,
                  spaceBetween: 15,
              },
          },
         
      });
          var galleryTopproduct = new Swiper('.product-detail .gallery-top', {
          spaceBetween: 10,
              zoom: {
              maxRatio: 3,
          },
          thumbs: {
              swiper: galleryThumbsproduct
          }
          });
 