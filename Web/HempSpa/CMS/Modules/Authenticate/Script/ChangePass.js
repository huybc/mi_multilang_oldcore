﻿R.ChangePass = {
    Init: function () {
        R.ChangePass.RegisterEvents();
       
        R.ChangePass.Id = 0;
    },
    RegisterEvents: function () {
        $('#btn-change-pass').off('click').on('click', function () {


            $('#changePass').modal(
                {
                    show: true,
                    backdrop: 'static',
                    keyboard: false
                }).on('show.bs.modal',
                    function () {
                    }).off('shown.bs.modal').on('shown.bs.modal',
                        function () {
                         //   R.ChangePass.ChangePassword('#changePass', R.ChangePass.Id);
                        }).off('hide.bs.modal').on('hide.bs.modal',
                            function () {
                                R.ChangePass.Id = 0;
                                $('#changePass .modal-body').html('');
                            }).on('loaded.bs.modal',
                                function () {

                                });


        });
        $('#btnChangeSave').off('click').on('click', function () {

            $('#changePass').RModuleBlock();
            R.Post({
                timeout: R.requestTimeout,
                module: "authenticate",
                params: {
                    oldPass: $('#oldPassword').val(),
                    newPass: $('#newPassword').val()
                },
                ashx: 'modulerequest.epi',
                action: "change_pass",
                success: function (res) {
                    if (res.Success) {
                        //R.PNotify('success', res.Message)
                        alert('Thành công');
                        $('#changePass').modal('hide');
                    } else {
                      //  R.PNotify('warn', res.Message);
                      alert('Lỗi');
                    }
                    $('#changePass').RModuleUnBlock();
                },
                error: function (e) {
                    $(el).RModuleUnBlock();
                    R.NotifytPopup('error', e.statusText + " [code:" + e.status + "]");
                }
            });
        });


    },


}

$(function () {

    R.ChangePass.Init();
});