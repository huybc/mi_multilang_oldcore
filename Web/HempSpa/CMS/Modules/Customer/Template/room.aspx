﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="room.aspx.cs" Inherits="HempSpa.CMS.Modules.Customer.Template.room" %>

<%@ Import Namespace="HempSpa.Core.Helper" %>
<%@ Import Namespace="Mi.BO.Base.Customer" %>
<%@ Import Namespace="Mi.Common" %>

<%

    var TotalRows = 0;
    string pageInfo = string.Empty;

    string name = GetQueryString.GetPost("Name", string.Empty);
    string phone = GetQueryString.GetPost("Phone", string.Empty);
    string type = GetQueryString.GetPost("type", string.Empty);

    var pageIndex = GetQueryString.GetPost("pageindex", 1);
    var pagesize = GetQueryString.GetPost("pagesize", 10);

    var list_dat_lich = CustomerBo.Search(name, phone, type, pageIndex, pagesize, ref TotalRows);




    var extant = TotalRows - (pagesize * pageIndex);
    var totalPages = Math.Ceiling((decimal)((double)TotalRows / pagesize));
    var ex = 0;

    if (extant < pagesize && pageIndex == totalPages)
    {
        ex = extant + (pagesize * pageIndex);
    }
    else
    {
        ex = pagesize * pageIndex;

    }
    pageInfo = totalPages + "#" + (TotalRows < pagesize ? TotalRows : ex) + "#" + TotalRows;
%>

<table class="table datatable-show-all dataTable no-footer" total-rows="<%=TotalRows%>" page-info="<%=pageInfo%>">
    <tbody>
        <%foreach (var item in list_dat_lich)
            { %>
        <tr role="row">
            <td class="w50">
                <div>
                    <label class="_hot">
                        <i class="icon-pencil7 new-edit position-static" data="<%=item.Id %>"></i>#<%=item.Id %>
                    </label>
                </div>
            </td>

            <td>
                <p>
                    <b><%=item.FullName %></b>
                </p>
                <p>
                    <label>Điện thoại:</label><%=item.Mobile %>
                </p>
                <p>
                    <label>Mail:</label><%=item.Email %>
                </p>
                <p>
                    <label>Ngày tạo:</label><time><%= UIHelper.GetLongDate(item.CreatedDate) %></time>
                </p>
            </td>
             <td>
                  <p>
                    <label>Người lớn:</label> <%=item.NguoiLon %>
                </p>
                <p>
                    <label>Trẻ em:</label> <%=item.TreEm %>
                </p>
                <p>
                    <label>Ngày bắt đầu: </label> <%=item.NgaySinh.ToString("dd/MM/yyyy") %>
                </p>
                <p>
                    <label>Ngày kết thúc: </label> <%=item.NgayKham.ToString("dd/MM/yyyy") %>
                </p>
            </td>
            <td>
                <p>
                    <label>Ghi chú:</label><%=item.Note %>
                </p>

            </td>
            <td>
                <label>Note nhân viên:</label><textarea data-id="<%=item.Id %>" class="form-control txtNoteNhanVien" style="vertical-align: top;" cols="50" rows="3" disabled><%=item.NoteNhanVien %></textarea>
            </td>


        </tr>
        <%} %>
    </tbody>
</table>
<div style="float: left; padding-top: 20px"><%= UIHelper.FormatCurrency("VND", TotalRows,false) %> bản ghi.</div>
