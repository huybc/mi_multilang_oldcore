﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuLeft.ascx.cs" Inherits="HempSpa.CMS.Modules.Zone.MenuLeft" %>
<div class="sidebar sidebar-main" id="cms-sidebar">
    <!-- Main navigation -->
    <div class="sidebar-category sidebar-category-visible">
        <div class="category-content no-padding">
            <ul class="navigation navigation-main navigation-accordion">
                <li class="navigation-header"><span>Danh mục</span> <i class="icon-menu" title="Main pages"></i></li>
                <li data="3" class="n-item"><a href="/cpanel/zone-3.htm"><i class="icon-file-presentation"></i>Blog</a></li>
                <li data="5" class="n-item"><a href="/cpanel/zone-5.htm"><i class="icon-file-presentation"></i>Giới thiệu</a></li>
                <li data="6" class="n-item"><a href="/cpanel/zone-6.htm"><i class="icon-file-presentation"></i>Liên hệ</a></li>
                <li data="11" class="n-item"><a href="/cpanel/zone-11.htm"><i class="icon-file-presentation"></i>Thư viện ảnh</a></li>
                <li data="12" class="n-item"><a href="/cpanel/zone-12.htm"><i class="icon-file-presentation"></i>Danh mục dịch vụ</a></li>

                <%--<li data="3" class="n-item"><a href="/cpanel/zone-3.htm"><i class="icon-file-presentation"></i>Blog công nghệ</a></li>
                <li data="4" class="n-item"><a href="/cpanel/zone-4.htm"><i class="icon-file-presentation"></i>Báo giá</a></li>
                <li data="5" class="n-item"><a href="/cpanel/zone-5.htm"><i class="icon-file-presentation"></i>Video</a></li>
                <li data="6" class="n-item"><a href="/cpanel/zone-6.htm"><i class="icon-file-presentation"></i>Tuyển dụng</a></li>
                <li data="7" class="n-item"><a href="/cpanel/zone-7.htm"><i class="icon-file-presentation"></i>Dịch vụ</a></li>
                <li data="8" class="n-item"><a href="/cpanel/zone-8.htm"><i class="icon-file-presentation"></i>HD Khách hàng</a></li>--%>
                <%--<li data="13" class="n-item"><a href="/cpanel/zone-13.htm"><i class="icon-file-presentation"></i>Hướng dẫn KH</a></li>
                <li data="17" class="n-item"><a href="/cpanel/zone-17.htm"><i class="icon-file-presentation"></i>Thư viện ảnh và video</a></li>
                <li data="18" class="n-item"><a href="/cpanel/zone-18.htm"><i class="icon-file-presentation"></i>Hệ thống</a></li>
                <li data="19" class="n-item"><a href="/cpanel/zone-19.htm"><i class="icon-file-presentation"></i>Tuyển dụng</a></li>
                <li data="20" class="n-item"><a href="/cpanel/zone-20.htm"><i class="icon-file-presentation"></i>Tra cứu</a></li>--%>
                <%--<li data="19" class="n-item"><a href="/cpanel/zone-19.htm"><i class="icon-file-presentation"></i>Khách sạn</a></li>--%>
            </ul>
        </div>
    </div>
    <!-- /main navigation -->
</div>
