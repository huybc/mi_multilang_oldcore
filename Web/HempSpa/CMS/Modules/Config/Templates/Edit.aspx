﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="HempSpa.CMS.Modules.Config.Templates.Edit" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<div class="tabbable" id="" style="width: 800px">
    <div class=" _body" id="_body">
        <div id="config-content" data="<%=obj.ConfigValueType %>" data-key="<%=obj.ConfigName %>">
            <div class=" panel-flat">
                <div class="panel-title" style="height: 46px; padding: 10px 0 4px 0">
                    <div class="col-md-12 text-right">
                        <button type="button" id="ConfigSaveBtn" class="btn ibtn-xs btn-primary "><i class="icon-floppy-disk position-left"></i>Lưu</button>
                    </div>
                </div>
                <div class="panel-body" style="padding-top: 0px">
                    <div id="icroll">
                        <div class="form-group">
                            <label class="col-lg-2">Tên cấu hình</label>
                            <div class="col-lg-10">
                                <b><%=obj.ConfigLabel %></b> [<%=obj.ConfigName %>]
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-lg-2">Url trang</label>
                            <div class="col-lg-10">
                                <input type="text"value="<%=obj.ConfigInitValue %>" id="_configinvalue"/>
                            </div>
                        </div>
                        <% var configs = ConfigBo.GetListConfigLanguageByConfigId(obj.Id);
                            int i = 0;
                            foreach (var cf in configs)
                            {
                                i++;
                        %>
                        <div class="form-group item-in-language" data-id="<%=cf.Id %>" data-language="<%=cf.LanguageCode.Trim()%>" data-language-name="<%=cf.Name%>">
                            <label class="control-label">Ngôn ngữ: <b><%=cf.Name %></b></label>
                            <div class="typeContent" style="display: none">
                                <textarea id="detailCkeditor<%=i %>" rows="10" class="editor form-control" tabindex="3"><%=cf.Content %></textarea>
                            </div>
                            <%if (obj.ConfigValueType == 4)
                                { %>
                                    <div class="typeImage" style="display: none">
                                        <img width="100px" id="attack-thumb<%= i %>" src="/<%= !string.IsNullOrEmpty(cf.Content) ? "uploads/" + cf.Content : "/CMS/Themes/images/no-thumbnai.png" %>" /><br />
                                        <br />
                                        &nbsp; <a href="javascript:void(0)" id="attackFile<%= i %>">
                                            <label for="fileSingleupload<%= i %>">[Đính kèm]</label>
                                            <input accept="image/*" id="fileSingleupload<%= i %>" multiple type="file" name="files[]" style="display: none" /></a>
                                    </div>
                            <% } %>
                        </div>
                        <%     
                            } %>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

