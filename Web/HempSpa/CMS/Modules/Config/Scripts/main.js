﻿R.Config = {
    Init: function () {
        this.GroupKey = 'all';

        R.Config.Search(R.Config.GroupKey);
        R.Config.RegisterEvents();
    },
    RegisterEvents: function () {
        $('table tr.row-item input, table tr.row-item textarea').off('keypress').on('keypress', function () {
            $(this).parents('tr').addClass('edit');
        });
        $('table tr.row-item input[type=checkbox]').off('click').on('click', function () {
            $(this).parents('tr').addClass('edit');
        });
        $('table tr.row-item a.attack-img').off('click').on('click', function () {
            var $this = $(this);
            R.Image.SingleUpload($this,
                function () {
                    //process
                }, function (response, status) {
                    $this.RModuleUnBlock();
                    //success
                    if (!response.success) {
                        $.confirm({
                            title: 'Thông báo !',
                            type: 'red',
                            content: response.messages,
                            animation: 'scaleY',
                            closeAnimation: 'scaleY',
                            buttons: {
                                cancel: {
                                    text: 'Đóng',
                                    btnClass: 're-btn re-btn-default'
                                },
                                yes: {
                                    isHidden: true, // hide the button
                                    keys: ['ESC'],
                                    action: function () {

                                    }
                                }
                            }

                        });
                        return;
                    }

                    if (response.success && response.error == 200) {
                        var img = StorageUrl + response.path;
                        //$('#attack-thumb .avatar img').attr({
                        //    'src': img + '?w=150&h=100&mode=crop',
                        //    'data-img': response.path
                        //});
                        $this.parents('tr').addClass('edit');
                        $this.parents('tr').find('.imgs').append('<div class="i-item"><img data="' + response.path + '" src="' + img + '?w=300" /><input type="text" placeholder="ex: http://example.com"  class="link"/><i>X</i></div>');
                        R.Config.RegisterEvents();
                    }

                });


            setTimeout(function () {
                $("table tr.row-item .imgs").sortable({
                    change: function (event, ui) {

                    },
                    update: function (event, ui) {


                    }
                });
            }, 200);
        });
        $('#config-wrapper tr td i.edit').off('click').on('click', function (e) {
            var key = $(this).parents('tr').attr('data');
            R.Config.Edit(key);
        });

        $('table tr.row-item .i-item i').off('click').on('click', function (e) {
            var $this = $(this);
            $.confirm({
                title: 'Xác nhận',
                content: 'Xóa ?',
                buttons: {
                    confirm: {
                        text: 'Xóa',
                        action: function () {
                            $this.parents('.i-item').remove();
                        }
                    },
                    cancel: {
                        text: 'Hủy',
                        action: function () {

                        }
                    },
                }
            });

        });
        $('#config-sidebar ul li.item').off('click').on('click', function (e) {
            if ($(this).hasClass('active')) return;
            $('#config-sidebar ul li').removeClass('active');
            $(this).addClass('active');
            var key = $(this).attr('data');
            R.Config.Search(key);
        });

        $('.typeImage input[type=file]').off('click').on('click', function (e) {
            var $this = $(this).parent().attr('id');
            $this = $('#' + $this);
            R.Image.SingleUpload($this,
                function () {
                    //process
                }, function (response, status) {
                    $this.RModuleUnBlock();
                    //success
                    if (!response.success) {
                        $.confirm({
                            title: 'Thông báo !',
                            type: 'red',
                            content: response.messages,
                            animation: 'scaleY',
                            closeAnimation: 'scaleY',
                            buttons: {
                                cancel: {
                                    text: 'Đóng',
                                    btnClass: 're-btn re-btn-default'
                                },
                                yes: {
                                    isHidden: true, // hide the button
                                    keys: ['ESC'],
                                    action: function () {

                                    }
                                }
                            }

                        });
                        return;
                    }

                    if (response.success && response.error == 200) {
                        var img = response.path;

                        $this.closest('.typeImage').find('img').attr('src', response.path).attr('data', response.name);
                    }

                });
        });
        $('#save-temp').off('click').on('click', function (e) {
            var data = [];

            $('tr.row-item.edit').each(function (i, v) {
                var poster = [];
                var $items = $(v).find('.i-item');

                for (var x = 0; x < $items.length; x++) {
                    var img = $('tr.row-item.edit:eq(' + i + ')').find('.i-item:eq(' + x + ')').children('img').attr('data');
                    var url = $('tr.row-item.edit:eq(' + i + ')').find('.i-item:eq(' + x + ')').children('input[type=text]').val();
                    poster.push({ 'img': img, 'url': url });

                }

                var obj = {
                    id: $(v).attr('data'),
                    order: $(v).find('.rownumber').val(),
                    label: $(v).find('.name').val(),
                    total: $(v).find('.totalrow').val(),
                    isenable: $(v).find('.isaenable').is(':checked'),
                    poster: JSON.stringify(poster)
                };
                data.push(obj);
            });

            R.Config.UpdatePageSetting(data);

        });
        $('#save-Zone').off('click').on('click', function (e) {
            var data = [];
            $('tr.row-item.edit').each(function (i, v) {
                var obj = {
                    id: $(v).attr('data'),
                    order: $(v).find('.rownumber').val(),
                    total: $(v).find('.totalrow').val(),
                    isenable: $(v).find('.isaenable').is(':checked')
                };
                data.push(obj);
            });
            console.log(data.join('-'))
            R.Config.UpdateZoneSetting(data);
        });
        $('#ConfigSaveBtn').off('click').on('click', function (e) {
            R.Config.SetValue();
        });

        R.ScrollAutoSize('#config-wrapper', function () {
            return $(window).height() - 80;
        }, function () {

        }, {});
    },

    SetValue: function () {

        var config_key = $('#config-content').attr('data-key');
        var type = $('#config-content').attr('data');
        var configinvalue = $('#_configinvalue').val();
        console.log(configinvalue)
        $('.item-in-language').each(function (i, v) {

            $('#_body').RModuleBlock();

            var value = '';
            if (parseInt(type) === 4) {
                value = $(v).find('img').attr('data');
            }
            if (parseInt(type) === 7) {
                var ckid = $(v).find('textarea').attr('id');
                value = CKEDITOR.instances[ckid].getData();
            }
            if (parseInt(type) === 5 || parseInt(type) === 6) {

                value = $(v).find('textarea').val();
            }
          
            R.Post({
                params: {
                    id: $(v).attr('data-id'),
                    language_code: $(v).attr('data-language'),
                    config_key: config_key,
                    value: value,
                    ConfigInitValue: configinvalue
                },
                module: "config",
                ashx: 'modulerequest.ashx',
                action: "save",
                success: function (res) {
                    if (res.Success) {
                        $.notify("Lưu thành công ngôn ngữ " + $(v).attr('data-language-name'), {
                            autoHideDelay: 2000, className: "success",
                            globalPosition: 'right top'
                        });

                    } else {
                        $.notify(res.Message, {
                            autoHideDelay: 3000, className: "error",
                            globalPosition: 'right top'
                        });

                    }
                    R.Config.RegisterEvents();
                    $('#_body').RModuleUnBlock();
                }

            });

        });


    },
    Edit: function (key) {
        $('#config-main').RModuleBlock();
        R.Post({
            params: { key: key },
            module: "config",
            ashx: 'modulerequest.ashx',
            action: "edit",
            success: function (res) {
                if (res.Success) {
                    R.ShowOverlay(res.Content, function () {


                    }, function () {

                    });

                    var type = $('#config-content').attr('data');
                    //Images = 4,
                    //[EnumMember]
                    //Code = 5,
                    //[EnumMember]
                    //Text = 6,
                    //[EnumMember]
                    //Content = 7,

                    //$('#typeddl').fselect({
                    //    dropDownWidth: 0,
                    //    autoResize: true
                    //});
                    if (type === '7') {
                        $('.item-in-language textarea').each(function (i, v) {
                            var ckid = $(v).attr('id');
                            CKEDITOR.replace(ckid, { toolbar: 'CosmeticV1', height: 450 });
                        })
                        $('.typeContent').show();
                    }
                    if (type === '5' || type === '6') {
                        autosize($('textarea'));
                        $('.typeContent').show();
                    }
                    if (type === '4') {
                        $('.typeImage').show();
                    } else {
                        $('.typeContent').show();
                    }

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                R.Config.RegisterEvents();
                $('#config-main').RModuleUnBlock();
            }

        });
    },
    Search: function (key) {
        $('#config-main').RModuleBlock();
        R.Post({
            params: { page: key },
            module: "config",
            ashx: 'modulerequest.ashx',
            action: "search",
            success: function (res) {
                if (res.Success) {
                    $('#config-wrapper table').html(res.Content);
                    R.ScrollAutoSize('#config-wrapper', function () {
                        return $(window).height() - 92;
                    }, function () {
                        return 'auto';
                    }, {}, {}, {}, true);
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                R.Config.RegisterEvents();
                $('#config-main').RModuleUnBlock();
            }

        });
    },
    UpdatePageSetting: function (data) {
        $('#config-main').RModuleBlock();
        R.Post({
            params: { json: JSON.stringify(data) },
            module: "config",
            ashx: 'modulerequest.ashx',
            action: "page_setting",
            success: function (res) {
                if (res.Success) {
                    $.notify("Cập nhật thành công !", {
                        autoHideDelay: 2000, className: "success",
                        globalPosition: 'right top'
                    });
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                R.Config.RegisterEvents();
                $('#config-main').RModuleUnBlock();
            }

        });
    },
    UpdateZoneSetting: function (data) {
        $('#config-main').RModuleBlock();
        R.Post({
            params: { json: JSON.stringify(data) },
            module: "zone",
            ashx: 'modulerequest.ashx',
            action: "zone_setting",
            success: function (res) {
                if (res.Success) {
                    $.notify("Cập nhật thành công !", {
                        autoHideDelay: 2000, className: "success",
                        globalPosition: 'right top'
                    });
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                R.Config.RegisterEvents();
                $('#config-main').RModuleUnBlock();
            }

        });
    },


}

$(function () {
    R.Config.Init();
})