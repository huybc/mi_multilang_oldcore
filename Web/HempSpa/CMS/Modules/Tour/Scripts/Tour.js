﻿R.Tour = {
    Init: function () {
        R.Tour.RegisterEvents();
        this.ZoneId = 0;
        this.ZoneType = 0;
        this.ZoneNode = null;
        //----
        this.PageIndex = 1;
        this.PageSize = 30;
        this.NewsStatus = 3;
        this.startDate = null;
        this.endDate = null;
        this.ItemSelected = [];
        this.XNewsId = 0;
        this.minDate = moment("01/01/2014").format('DD/MM/YYYY');
        this.maxDate = moment("12/31/2050").format('DD/MM/YYYY');

        $('#language-type-ddl').fselect({
            dropDownWidth: 0,
            autoResize: true
        });
        if ($('#mini-game').length > 0) {
            R.Tour.XNews();
            $('[data-popup="tooltip"]').tooltip();
        };


    },
    RegisterEvents: function () {
        $('#gallery-upload-file .r-queue-item .file_upload').off('click').on('click', function (e) {
            R.FileManager.Open(R.Tour.AttackFile, 'multi');
        });
        $('#titletxt').bind("keypress keyup", function (event) {
            $('#urltxt').val(R.UnicodeToSeo($('#titletxt').val()));
        });
        if ($('#mini-game').length > 0) {
            var delay = null;
            $('#news-save .btn-primary').off('click').on('click',
                function (e) {
                    var status = $(this).attr('status');
                    var oid = R.Tour.XNewsId;
                    var title = $('#titletxt').val();
                    // var sapo = $('#Sapo').val();
                    var zoneddl = $('#_zoneddl').val();
                    var url = $('#urltxt').val();
                    var titleSeo = $('#metaTitletxt').val();
                    var metakeyword = $('#metakeywordtxt').val();
                    var metadescription = $('#metadescriptiontxt').val();
                    var date = $('#datetxt').attr('data');
                    var content = CKEDITOR.instances['contentCkeditor'].getData();
                    var Sapo = CKEDITOR.instances['Sapo'].getData();
                    var hotcb = $('#hotcb').is(":checked");
                    var avatars = [];
                    $('#gallery-upload-file .r-queue-item.added').each(function (i, v) {
                        var item = $(v).attr('data');
                        if (typeof (item) != 'undefined' && item.length > 0) {
                            avatars.push({
                                num: i + 1,
                                url: item
                            });
                        }

                    });

                    var contents = [];
                    $('#items .item').each(function (i, v) {
                        var startDate = $(v).find('.start-date').val();
                        if (startDate.length > 0) {
                            startDate = R.FormatDate(startDate)
                        }
                        var endDate = $(v).find('.end-date').val();
                        if (endDate.length > 0) {
                            endDate = R.FormatDate(endDate)
                        }
                        var price = $(v).find('.price').val();
                        var childPrice = $(v).find('.child-price').val();
                        var status = $(v).find('.ddl-status').val();
                        if (price.length > 0 && startDate.length > 0 && endDate.length > 0) {
                            contents.push({
                                startDate: startDate,
                                endDate: endDate,
                                price: price.replace(/\./g, ''),
                                child_price: childPrice.length > 0 ? childPrice.replace(/\./g, '') : 0,
                                status: status
                            });

                        }

                    });

                    var data = {
                        id: oid,
                        title: title,
                        sapo: Sapo,
                        //  author: author,
                        // source: source,
                        zone: zoneddl,
                        date: date,
                        content: content,
                        hot: hotcb,
                        url: url,
                        //status:status,
                        metakeyword: metakeyword,
                        status: status,
                        metadescription: metadescription,
                        avatar: $('#attack-thumb img').attr('data-img'),
                        avatars: avatars.length > 0 ? JSON.stringify(avatars) : '',
                        metatitle: titleSeo,
                        subtile: $('#subTitleTxt').val(),
                        location: $('#locationDll').val() != null ? $('#locationDll').val().join(';') : '',
                        language_code: $('#languageDdl').val(),
                        transfers: $('#TransfersDdl').val() != null ? $('#TransfersDdl').val().join(';') : '',
                        code: $('#codeTxt').val(),
                        groupId: $('#groupDdl').val(),
                        contents: JSON.stringify(contents)

                    }
                    $('#IMSFullOverlayWrapper').RModuleBlock();
                    delay = setTimeout(function () {
                        R.Tour.XSave(data);
                    },
                        200);

                });


            $('#module-content .btn-action button._edit').off('click').on('click',
                function (e) {
                    var id = $('#module-content .datatable-scroll table tbody tr td input[type=checkbox]:checked')
                        .attr('id');

                    R.Tour.XEdit(id);
                });

            $('#btn-new-post').off('click').on('click',
                function () {
                    R.Tour.XEdit(0);
                });

            $('#news-sldebar .navigation li.sub-news-game').off('click').on('click',
                function () {

                    R.Tour.ItemSelected = [];
                    $('#main-pnl').show('slow');
                    $('#sub-pnl').hide();
                    $('#news-sldebar .navigation li').removeClass('active');
                    $(this).addClass('active');
                    R.Tour.startDate = R.Tour.minDate;
                    R.Tour.endDate = R.Tour.maxDate;
                    R.Tour.PageIndex = 1;
                    $('#date-filter span').html("Tất cả: " + R.Tour.minDate + ' - ' + R.Tour.maxDate);
                    R.Tour.NewsStatus = $(this).attr('status');
                    if (delay != null) clearTimeout(delay);
                    delay = setTimeout(function () {
                        R.Tour.XNews();
                    },
                        200);

                });
            $('#date-filter').off('apply.daterangepicker').on('apply.daterangepicker',
                function (ev, picker) {
                    R.Tour.startDate = picker.startDate.format('YYYY-MM-DD');
                    R.Tour.endDate = picker.endDate.format('YYYY-MM-DD');
                    R.Tour.XNews();
                });
            $('#zone-filer-ddl').off('change').on('change',
                function (e) {
                    R.Tour.XNews();
                });
            $('#zone-type-ddl').off('change').on('change',
                function (e) {
                    R.Tour.XNews();
                });
            $('#language-type-ddl').off('change').on('change',
                function (e) {
                    R.Tour.XNews();
                });
            $('#btn-new-reload').off('click').on('click',
                function (e) {
                    R.Tour.startDate = R.Tour.minDate;
                    R.Tour.endDate = R.Tour.maxDate;
                    R.Tour.PageIndex = 1;
                    $('#_search-data').val('');
                    $('#date-filter span').html("Tất cả: " + R.Tour.minDate + ' - ' + R.Tour.maxDate);
                    if (delay != null) clearTimeout(delay);
                    delay = setTimeout(function () {
                        R.Tour.XNews();
                    },
                        200);
                });
            $('#module-content .btn-action button._view').off('click').on('click',
                function (e) {
                    $('#news-preview').RLoading();
                    var id = $('#module-content .datatable-scroll table tbody tr td input[type=checkbox]:checked')
                        .attr('id');
                    $('#news-preview').modal('show').off('shown.bs.modal').on('shown.bs.modal',
                        function () {
                            R.Post({
                                params: { id: id },
                                module: "news",
                                ashx: 'modulerequest.ashx',
                                action: "preview",
                                success: function (res) {
                                    if (res.Success) {
                                        $('#news-preview-content').html(res.Content).find('#body .detail img')
                                            .css({ 'max-width': '80%' }).parent().css({
                                                'text-align': 'center',
                                                'width': '100%',
                                                'display': 'inline-block'
                                            });
                                        $('#news-preview-content p img,#news-preview-content p iframe').parent()
                                            .css('text-align', 'center');
                                        $('#news-preview .modal-content').css('overflow-y', 'hidden');
                                        $('[data-popup="tooltip"]').tooltip();
                                        // check menu action in view

                                        if (R.Tour.NewsStatus == 3) {
                                            $('.btn-group.btn-action ._unpublish').show("slow");
                                        } else {
                                            $('.btn-group.btn-action ._unpublish').hide("slow");
                                        }
                                        // bài lưu tạm
                                        if (R.Tour.NewsStatus == 5) {
                                            // từ chối
                                            $('.btn-group.btn-action ._reject').show("slow");
                                        } else {
                                            // từ chối
                                            $('.btn-group.btn-action ._reject').hide("slow");
                                        }

                                        if (R.Tour.NewsStatus == '6' || R.Tour.NewsStatus == 6) {
                                            $('.btn-group.btn-action ._request_publish').hide("slow");
                                        } else {

                                            $('.btn-group.btn-action ._request_publish').show("slow");
                                        }

                                        R.ScrollAutoSize('#news-preview .container-news',
                                            function () {
                                                return $(window).height() - 10;
                                            },
                                            function () {
                                                return 'auto';
                                            },
                                            {});

                                        var $zoneItem = $('#news-preview').find('._zone');
                                        if ($zoneItem.length > 0 && $zoneItem != 'undefined') {
                                            var strHtml = '';
                                            var arrayId = $zoneItem.attr('data-id').split(',');
                                            for (var k = 0; k < arrayId.length; k++) {
                                                $(RAllZones).each(function (i, item) {
                                                    if (arrayId[k] == item.Id) {
                                                        if (strHtml.length == 0) {
                                                            strHtml = item.Name;
                                                        } else {
                                                            strHtml += ';' + item.Name;
                                                        }
                                                    }
                                                    return;
                                                });

                                            }
                                            $zoneItem.text(strHtml);
                                        }
                                        R.Post({
                                            params: { id: id },
                                            module: "news",
                                            ashx: 'modulerequest.ashx',
                                            action: "reject_log",
                                            success: function (res) {
                                                var str = '';
                                                if (res.Success) {
                                                    if (parseInt(res.TotalRow) > 0) {
                                                        $.each($.parseJSON(res.Data),
                                                            function (i, item) {
                                                                str += '<li><p><b>' +
                                                                    item.CreatedBy +
                                                                    '</b> - <time>' +
                                                                    item.Date +
                                                                    '</time></p><div id="log_content">' +
                                                                    item.Content +
                                                                    '</div></li>';
                                                            });
                                                        $('#reject-text')
                                                            .html('<div id="_log"><ul>' + str + '</ul></div>').show();

                                                    }
                                                }
                                            }
                                        });

                                        R.Tour.RegisterEvents();
                                        $('#news-preview').RLoadingComplete();

                                    } else {
                                        alert(res.Message);
                                    }
                                }
                            });
                        });
                });
            $('#module-content .btn-action button.status').off('click').on('click',
                function (e) {
                    var status = $(this).attr('data-status');
                    var data = {
                        id: R.Tour.ItemSelected.join(','),
                        status: status
                    }
                    $.confirm({
                        title: 'Thông báo',
                        content: 'Xác nhận yêu cầu ?',
                        buttons: {
                            confirm: {
                                text: 'Thực hiện',
                                action: function () {
                                    R.Tour.XUpdateStatus(data);
                                }
                            },
                            cancel: {
                                text: 'Hủy',
                                action: function () {

                                }
                            },
                        }
                    });


                });

            $('#data-list .datatable-scroll tr td input[type=checkbox]').off('click').on('click',
                function () {

                    var id = $(this).attr('id');
                    if ($(this).is(':checked')) {
                        $(this).closest('tr').addClass('row-selected');
                        R.Tour.ItemSelected.push(id);
                    } else {

                        var index = R.Tour.ItemSelected.indexOf(id);
                        if (index > -1) {
                            R.Tour.ItemSelected.splice(index, 1);
                        }
                        $(this).closest('tr').removeClass('row-selected');
                    }


                    if (R.Tour.ItemSelected.length === 0) {
                        $('.btn-group.btn-action').slideUp();
                    }
                    if (R.Tour.ItemSelected.length === 1) {
                        $('.btn-group.btn-action').slideDown("slow");
                        // $('.btn-group.btn-action ._delete').show("slow");
                        //$('.btn-group.btn-action ._view').show("slow");
                        $('.btn-group.btn-action ._edit').show("slow");


                    } else if (R.Tour.ItemSelected.length > 1) {
                        $('.btn-group.btn-action').slideDown("slow");
                        //  $('.btn-group.btn-action ._delete').show("slow");
                        //$('.btn-group.btn-action ._view').hide();
                        $('.btn-group.btn-action ._edit').hide();
                        // bài đã duyệt
                        //if (R.Tour.NewsStatus === '1' || R.Tour.NewsStatus === 1) {
                        //    $('.btn-group.btn-action ._unpublish').show("slow");
                        //} else {
                        //    $('.btn-group.btn-action ._unpublish').hide("slow");
                        //}
                        // bài lưu tạm
                        //if (R.Tour.NewsStatus === '4' || R.Tour.NewsStatus === 4) {
                        //    $('.btn-group.btn-action ._publish').show("slow");
                        //} else {
                        //    $('.btn-group.btn-action ._publish').hide("slow");
                        //}


                    }

                    if (R.Tour.NewsStatus == 5) {
                        $('.btn-group.btn-action ._delete').hide("slow");

                    } else {
                        $('.btn-group.btn-action ._delete').show("slow");
                    }
                    if (R.Tour.NewsStatus == 5) {
                        $('.btn-group.btn-action ._publish').show("slow");
                    } else {
                        $('.btn-group.btn-action ._publish').hide("slow");
                    }
                    if (R.Tour.NewsStatus == 3) {
                        $('.btn-group.btn-action ._unpublish').show("slow");
                    } else {
                        $('.btn-group.btn-action ._unpublish').hide("slow");
                    }
                    $('#rows').text(R.Tour.ItemSelected.length);
                });
        }



        $('#news-sldebar .navigation li.sub-item').off('click').on('click',
            function () {

                R.Tour.ZoneType = parseInt($(this).attr('data-type'));
                $('#news-sldebar .navigation li').removeClass('active');
                $(this).addClass('active');
                if (delay != null) clearTimeout(delay);
                delay = setTimeout(function () {
                    R.Tour.MainInit();
                },
                    200);

            });

        $('#btn-add-zone').off('click').on('click',
            function () {
                R.Tour.Edit(0);
            });


        $('#items .item i.icon-bin').off('click').on('click',
            function () {
                var $this = $(this);
                $.confirm({
                    title: 'Thông báo !',
                    type: 'red',
                    content: 'Xác nhận xóa',
                    animation: 'scaleY',
                    closeAnimation: 'scaleY',
                    buttons: {
                        yes: {
                            text: 'Xóa',
                            keys: ['ESC'],
                            action: function () {
                                $this.closest('.item').remove();
                            }
                        },
                        cancel: {
                            text: 'Đóng',
                            btnClass: 're-btn re-btn-default'
                        }
                    }

                });

            });


        $('#items .item i.icon-bin').hover(function () {
            $(this).closest('.item').find('._field').css('opacity', .3)

        },
            function () {
                $(this).closest('.item').find('._field').css('opacity', 1)
            });

        $('#addItemBtn').off('click').on('click',
            function () {

                $('#items').append(R.Tour.StepHtml());
                $('.start-date,.end-date').pickadate({
                    editable: true,
                    format: 'dd/mm/yyyy',
                    formatSubmit: 'yyyy/mm/dd',
                    altFormat: "dd/MM/yyyy"
                });
                $('.ddl-status').fselect({
                    dropDownWidth: 0,
                    autoResize: true
                });
                $('.input-price').maskMoney({
                    precision: 0,
                    thousands: ".",
                    decimal: ",",
                    allowZero: true,
                    allowNegative: true
                    //suffix:'vnđ'
                });
                R.Tour.RegisterEvents();

            });



        $('#items .item .avatar').off('click').on('click',
            function () {
                var $this = $(this);

                R.Image.SingleUpload($this, function () {

                    //process
                }, function (response, status) {

                    //success
                    if (!response.success) {
                        $.confirm({
                            title: 'Thông báo !',
                            type: 'red',
                            content: response.messages,
                            animation: 'scaleY',
                            closeAnimation: 'scaleY',
                            buttons: {
                                cancel: {
                                    text: 'Đóng',
                                    btnClass: 're-btn re-btn-default'
                                },
                                yes: {
                                    isHidden: true, // hide the button
                                    keys: ['ESC'],
                                    action: function () {

                                    }
                                }
                            }

                        });
                        return;
                    }

                    if (response.success && response.error == 200) {
                        var img = response.path;
                        $this.closest('.avatar').find('img').attr({
                            'src': img + '?w=150&h=100&mode=crop',
                            'data-img': response.name
                        });
                    }


                });

            });

    },
    AttackFile: function (el) {
        if (typeof (el) != "undefined") {

            var temp = '<div class="col-md-3 r-queue-item"><i class="fa fa-picture-o"></i><p>Đăng ảnh</p><div class="file_upload ui-state-default"></div></div>';
            var countNotAdded = $('.gallery-upload-file .r-queue-item:not(.added)').length;
            if (el.length > countNotAdded) {
                var add = el.length - parseInt(countNotAdded);
                for (var j = 0; j < add; j++) {
                    $('#gallery-upload-file ._library').before(temp);
                }
            }

            if ($('.gallery-upload-file .r-queue-item').hasClass('added')) {
                //$.each(el, function (i, v) {
                //lần 2

                //  $('.gallery-upload-file .r-queue-item:not(.added):eq(' + i + ')').attr({ 'in': i }).attr({ 'data': v }).addClass('added').append('<div class="item"><img src="/' + StorageUrl + '/' + v + '"/></div><i class="fa fa-times"></i>');
                $('#gallery-upload-file .r-queue-item:not(.added)').each(function (i, v) {
                    if (typeof (el[i]) != 'undefined') {

                        if (!$(this).hasClass('added')) {
                            $(this).attr({ 'data': el[i] }).addClass('added').append('<div class="item"><img src="/' + StorageUrl + '/' + el[i] + '"/></div><i class="fa fa-times"></i>');
                        }

                    }
                });

            } else {
                $.each(el, function (i, v) {
                    // laanf 1
                    $('.gallery-upload-file .r-queue-item:eq(' + i + ')').attr({ 'data': v }).addClass('added').append('<div class="item"><img src="/' + StorageUrl + '/' + v + '"/></div><i class="fa fa-times"></i>');
                });
            }

            $('.gallery-upload-file .r-queue-item i.fa-times').off('click').on('click', function () {
                var $this = $(this);
                $this.closest('.r-queue-item').removeClass('added').find('div').show().removeAttr('data');
                $this.parent().find('.item').remove();
                $this.parent().find('i.fa-times').remove();

            });
            $("#gallery-upload-file").sortable();
            R.Tour.RegisterEvents();
            return;

        }
    },
    StepHtml: function (data) {

        return ' <div class="item _add">' +
            '<i class="icon-bin"></i>' +
            '<div class="item">' +
            '<label class="control-label col-lg-2 text-right">#1</label>' +
            '<div class="col-lg-10">' +
            '<div class="col-lg-3">' +
            '<label class="control-label col-lg-12">Từ ngày</label>' +
            '<input autocomplete="off" type="text" name="basic start-date" class="form-control start-date"  tabindex="1" required="required"></div>' +
            '<div class="col-lg-3" ><label class="control-label col-lg-12">Đến ngày</label>' +
            '<input autocomplete="off" type="text" name="basic" class="form-control end-date" tabindex="1" required="required"></div>' +
            '<div class="col-lg-3">' +
            '<label class="control-label col-lg-12">Giá</label>' +
            '<input autocomplete="off" type="text" name="basic" class="form-control price input-price" value="" tabindex="1" required="required">' +
            '</div>' +
            '<div class="col-lg-3">' +
            '<label class="control-label col-lg-12">Giá trẻ em</label>' +
            '<input autocomplete="off" type="text" name="basic" class="form-control child-price input-price" value="" tabindex="1" required="required">' +
            '</div>' +
            '<div class="col-lg-3">' +
            '<label class="control-label col-lg-12">Trạng thái</label>' +
            '<select data-placeholder="Select your state" class="select ddl-status" style="width: 100%" tabindex="4" data="<%=_obj.NewsInfo.GroupId %>">' +
            '<option value="1">Còn chỗ</option><option value ="2"> Hết chỗ</option >' +
            '<option value="3">Liên hệ</option>' + '</select></div>' + '</div></div></div>';




    },

    XUpdateStatus: function (data) {
        R.Post({
            params: data,
            module: "tour",
            ashx: 'modulerequest.ashx',
            action: "update_status",
            success: function (res) {
                if (res.Success) {
                    $.notify("Cập nhật thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.Tour.XNews();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }

        });
    },
    XNews: function () {

        var el = '#data-list';
        $(el).RLoading();
        var data = {
            keyword: $('#_search-data').val(),
            zone: $('#zone-filer-ddl').val(),
            language_code: $('#language-type-ddl').val(),
            pageindex: R.Tour.PageIndex,
            pagesize: R.Tour.PageSize,
            status: R.Tour.NewsStatus,
            from: R.Tour.startDate,
            to: R.Tour.endDate,
            type: $('#zone-type-ddl').val()
        }
        R.Post({
            params: data,
            module: "tour",
            ashx: 'modulerequest.ashx',
            action: "xsearch",
            success: function (res) {
                if (res.Success) {
                    R.Tour.ItemSelected = [];
                    $(el).find('.datatable-scroll._list').html(res.Content);
                    $('.btn-group.btn-action').hide();
                    R.CMSMapZone('#data-list');

                    R.ScrollAutoSize('.datatable-scroll', function () {
                        return $(window).height() - 92;
                    }, function () {
                        return 'auto';
                    }, {}, {}, {}, true);
                    //pager
                    if ($('#data-list .datatable-scroll._list table').attr('page-info') != 'undefined') {
                        var pageInfo = $('#data-list .datatable-scroll._list table').attr('page-info').split('#');
                        var page = pageInfo[0];
                        var extant = pageInfo[1];
                        var totals = pageInfo[2];
                        if (parseInt(totals) < 0) {
                            $('#data-pager').hide();
                            return;
                        } else {
                            $('#data-pager').show();
                        }
                        var rowFrom = '';
                        if (R.Tour.PageIndex === 1) {
                            rowFrom = 'Từ 1 đến ' + extant;
                        } else {
                            rowFrom = 'Từ ' + (parseInt(R.Tour.PageIndex) * parseInt(R.Tour.PageSize) - parseInt(R.Tour.PageSize)) + ' đến ' + extant;
                        }

                        $('#rowInTotals').text(rowFrom + '/' + totals);
                        $('.ipagination').jqPagination({
                            current_page: R.Tour.PageIndex,
                            max_page: page,
                            paged: function (page) {
                                R.Tour.PageIndex = page;
                                R.Tour.XNews();
                            }
                        });
                    }
                    //end pager



                    R.Tour.RegisterEvents();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });
                }

                $(el).RLoadingComplete();
            }
        });
    },
    XEdit: function (id) {

        $('body').RModuleBlock();
        R.Post({
            params: {
                id: id
                // zoneType: R.Tour.ZoneType
            },
            module: "tour",
            ashx: 'modulerequest.ashx',
            action: "xedit",
            success: function (res) {

                if (res.Success) {
                    //   $('#zone-form-edit').html(res.Content);

                    R.ShowOverlayFull({ content: res.Content }, function () {


                    }, function () {

                    });

                    R.Tour.XNewsId = id;
                    $('.input-price').maskMoney({
                        precision: 0,
                        thousands: ".",
                        decimal: ",",
                        allowZero: true,
                        allowNegative: true
                        //suffix:'vnđ'
                    });
                    $('#attack-thumb .avatar').off('click').on('click', function () {
                        R.Image.SingleUpload($('#attack-thumb .avatar'),
                            function () {
                                //process
                            }, function (response, status) {
                                $('#attack-thumb .avatar').RModuleUnBlock();
                                //success
                                if (!response.success) {
                                    $.confirm({
                                        title: 'Thông báo !',
                                        type: 'red',
                                        content: response.messages,
                                        animation: 'scaleY',
                                        closeAnimation: 'scaleY',
                                        buttons: {
                                            cancel: {
                                                text: 'Đóng',
                                                btnClass: 're-btn re-btn-default'
                                            },
                                            yes: {
                                                isHidden: true, // hide the button
                                                keys: ['ESC'],
                                                action: function () {

                                                }
                                            }
                                        }

                                    });
                                    return;
                                }

                                if (response.success && response.error == 200) {

                                    var img = response.path;
                                    $('#attack-thumb .avatar img').attr({
                                        'src': img + '?w=150&h=100&mode=crop',
                                        'data-img': response.name
                                    });
                                }

                            });

                    });
                    setTimeout(function () {
                        $('#_zoneddl').fselect({
                            // multiple: true,
                            dropDownWidth: 0,
                            autoResize: true
                        });
                        $('#locationDll').fselect({
                            multiple: true,
                            dropDownWidth: 0,
                            autoResize: true
                        });
                        $('.ddl-status').fselect({
                            dropDownWidth: 0,
                            autoResize: true
                        });
                        $('#groupDdl').fselect({
                            dropDownWidth: 0,
                            autoResize: true
                        });
                        $('#languageDdl').fselect({
                            dropDownWidth: 0,
                            autoResize: true
                        });
                        $('#TransfersDdl').fselect({
                            multiple: true,
                            dropDownWidth: 0,
                            autoResize: true
                        });
                        if (id > 0) {
                            $('.start-date,.end-date').pickadate({
                                editable: true,
                                format: 'dd/mm/yyyy',
                                formatSubmit: 'yyyy/mm/dd',
                                altFormat: "dd/MM/yyyy"
                            });

                            $('#items .item').each(function (i, v) {
                                var sl = $(v).find('select');
                                sl.val(sl.attr('data'));
                                $('#items .item select').trigger('fselect:updated');
                            });

                            //$('#_zoneddl').val($('#_zoneddl').attr('data').split(';'));
                            $('#_zoneddl').val($('#_zoneddl').attr('data'));
                            $('#languageDdl').val($('#languageDdl').attr('data'));
                            $('#locationDll').val($('#locationDll').attr('data').split(';'));
                            $('#groupDdl').val($('#groupDdl').attr('data'));
                            $('#TransfersDdl').val($('#TransfersDdl').attr('data').split(';'));
                            $('#_zoneddl,#languageDdl,#locationDll,#groupDdl,#TransfersDdl').trigger('fselect:updated');

                            var imageData = $('#gallery-upload-file').attr('data');
                            if (typeof (imageData) != 'undefined') {
                                if (imageData.length > 0) {


                                    var images = $.parseJSON(imageData);
                                    var temp = '<div class="col-md-3 r-queue-item"><i class="fa fa-picture-o"></i><p>Đăng ảnh</p><div class="file_upload ui-state-default"></div></div>';
                                    if (images.length > $('.gallery-upload-file .r-queue-item').length) {

                                        var add = images.length - $('.gallery-upload-file .r-queue-item').length;
                                        for (var j = 0; j < add; j++) {
                                            $('#gallery-upload-file ._library').before(temp);
                                        }
                                    };
                                    $.each(images, function (i, v) {
                                        $('#gallery-upload-file .r-queue-item:eq(' + i + ')').attr({ 'data': v.url }).addClass('added').append('<div class="item"><img src="/' + StorageUrl + '/' + v.url + '"/></div><i class="fa fa-times"></i>');
                                        $("#gallery-upload-file").sortable();
                                    });
                                }
                            }


                            $('.gallery-upload-file .r-queue-item i.fa-times').off('click').on('click', function () {
                                var $this = $(this);
                                $this.closest('.r-queue-item').removeClass('added').find('div').show().removeAttr('data');
                                $this.parent().find('.item').remove();
                                $this.parent().find('i.fa-times').remove();
                            });


                            $('#datetxt').daterangepicker({
                                autoUpdateInput: false,
                                "singleDatePicker": true,
                                "timePicker": true,
                                "timePicker24Hour": true,
                                "timePickerSeconds": true,
                                //"autoApply": true,
                                "showCustomRangeLabel": false,
                                "alwaysShowCalendars": true,
                                "startDate": moment(),
                                "endDate": R.Tour.maxDate,
                                "opens": "left",
                                locale: {
                                    format: 'DD/MM/YYYY h:mm A'
                                }
                            }, function (start, end, label) {
                                $('#datetxt').attr('data', start.format('YYYY-MM-DD h:mm A'));
                                $('#datetxt').val(start.format('DD-MM-YYYY h:mm A'));
                            });


                        } else {
                            $('.start-date,.end-date').pickadate({
                                editable: true,
                                format: 'dd/mm/yyyy',
                                formatSubmit: 'yyyy/mm/dd',
                                altFormat: "dd/MM/yyyy"
                            });



                        }



                        autosize(document.querySelectorAll('textarea#Sapo'));
                        R.ScrollAutoSize('#zone-content', function () {
                            return $(window).height() - 10;
                        }, function () {
                            if (CKEDITOR.instances["contentCkeditor"]) {
                                CKEDITOR.instances["contentCkeditor"].destroy();
                            }
                                CKEDITOR.replace('contentCkeditor', { toolbar: 'iweb' });

                                if (CKEDITOR.instances["Sapo"]) {
                                    CKEDITOR.instances["Sapo"].destroy();
                            }
                                CKEDITOR.replace('Sapo', { toolbar: 'iweb' });

                            $('#items .item').each(function (i, v) {
                                //var ckeditorId = $(v).find('.editor').attr('id');
                                //CKEDITOR.replace('' + ckeditorId + '', { toolbar: 'basic' });

                            });
                            return 'auto';
                        }, {});
                    }, 200);

                    $('body').RModuleUnBlock();

                    R.Tour.RegisterEvents();

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }
        });
    },
    XSave: function (data) {

        R.Post({
            params: data,
            module: "tour",
            ashx: 'modulerequest.ashx',
            action: "xsave",
            success: function (res) {
                if (res.Success) {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.CloseOverlayFull();
                    // R.Tour.NewsStatus = 4;
                    R.Tour.XNews();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                $('#IMSFullOverlayWrapper').RModuleUnBlock();
                R.Tour.RegisterEvents();
            }, error: function () {
                $('#IMSFullOverlayWrapper').RModuleUnBlock();
            }
        });
    },

}
$(function () {
    R.Tour.Init();
});