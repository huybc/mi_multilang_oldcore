﻿<%@ Page Title="Trang chủ" Language="C#" MasterPageFile="~/Themes/RoLux.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="GoldLuck_Remaster._default" %>

<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="BMLed.Core.Helper" %>
<%@ Import Namespace="Mi.BO.Base.Product" %>
<%@ Import Namespace="Mi.Entity.Base.Product" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
     <meta property="og:title" content=" <%=obj.TitleSeo%>" />
    <meta property="og:description" content="<%=obj.MetaDescription%>" />
    <meta property="og:image" content="<%=domainName %>/Uploads/<%=obj.Avatar%>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" runat="server">
   
    <div class="clhidden  bg-fff">
        <div class="container px-0 text-center">
            <div class="row no-gutters">
                <div class="col-lg-12 col-md-12  col-sm-12 col-12 px-xl-3">
                    <div class="bd-example">
                        <div id="slide-home" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#slide-home" data-slide-to="0" class="active"></li>
                                <li data-target="#slide-home" data-slide-to="1"></li>
                                <li data-target="#slide-home" data-slide-to="2"></li>
                            </ol>
                            <%var GetConFig = ConfigBo.AdvGetByType(7); %>
                            <div class="carousel-inner">
                                <%var i = 0; %>
                                <%foreach (var item in GetConFig)
                                    {%>

                                <%var split = item.Content.Split(';');%>
                                <div class="carousel-item <%=i == 0? "active":"" %>">
                                    <img src="/Uploads/thumb/<%=item.Thumb %>" class="d-block w-100" alt="<%=item.Name %>">
                                    <div class="carousel-caption ">

                                        <div class="lp-3">
                                            <%=item.Name %>
                                        </div>
                                        <%if (split.Count() >= 2)
                                            {%>
                                        <h2><%=split[0]%></h2>
                                        <p><%=split[1]%></p>

                                        <%} %>
                                        <a class="btn btn-outline-light text-uppercase px-5 mb-3" href="<%=item.Url %>">Xem thêm
                                        </a>
                                    </div>
                                </div>
                                <%=i++ %>
                                <%}%>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <% var list_zone_trang_chu = ZoneBo.GetAllZone(14); %>
    <% var zone_trang_chu_child = list_zone_trang_chu.Where(r => r.ParentId > 0).OrderBy(r => r.SortOrder); %>
    <!--Sp mới-->
    <%var zone_trang_chu_child_cut_1 = zone_trang_chu_child.Take(1).FirstOrDefault(); %>
    <%if (zone_trang_chu_child_cut_1 != null)
        { %>
    <section class="list-product-home py-3 py-lg-5 mb-xl-4 text-center clhidden" style="margin-bottom: 0px!important; padding-bottom: 0!important;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5 col-12">
                    <h2 class="heading-home"><%=zone_trang_chu_child_cut_1.Name %></h2>
                    <div class="color-52575C text-center mb-4">
                        <%=zone_trang_chu_child_cut_1.Description %>
                    </div>
                </div>
            </div>
            <div class="row">

                <%  var zones = zone_trang_chu_child_cut_1.Id.ToString();
                    int TotalNewRows = 0;
                    var san_phams = ProductBo.FE_Product_Search("", "", zones, 0, 0, DateTime.MinValue, DateTime.MaxValue, 0, -1, (int)EnumProductSortOrder.CreatedDateDesc, (int)EnumProductStatus.Published, 1, 8, ref TotalNewRows);
                %>
                <%foreach (var item in san_phams)
                    {
                        //var link_target = "javascript:void(0)";
                        var link_target = string.Format("/san-pham/{0}.p{1}.htm", item.Url, item.Id);
                %>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative">
                            <img src="/Uploads/thumb/<%=item.Avatar %>" class="img-fluid w-100" alt="<%=item.Title %>">
                            <%if (item.IsHot == true)
                                { %>
                            <div class="tag">
                                HOT
                            </div>
                            <%} %>
                            <%if (item.Percent > 0)
                                { %>
                            <div class="tag bg-C6832B">
                                -<%=item.Percent %>%
                            </div>
                            <%} %>
                            <div class="overlay">
                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2">
                                    <img src="/Themes/images/heart-icon.svg" class="img-fluid "></button>
                                <a href="/gio-hang" class="btn btn-product add-to-cart" data-idsp="<%=item.Id %>">
                                    <img src="/Themes/images/cart-icon.svg" data-idsp="<%=item.Id %>" class="img-fluid" style="margin-top: 10px"></a>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="javascript:void(0)" title="<%=item.Folder %>"><%=item.Folder %></a>
                            </div>
                            <h5 class="title">
                                <a href="<%=link_target %>" class="<%=item.Title %>"><%=item.Title %></a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    <%=UIHelper.FormatNumber(item.DiscountPrice > 0 ? item.DiscountPrice : item.Price) %> vnđ
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%} %>
            </div>
        </div>
    </section>
    <%} %>


    <!--Banner-->
    <%-- <section class="banner-home">
        <div class="container">
            <%var GetBanner1 = ConfigBo.AdvGetByType(1).OrderBy(x => x.SortOrder).FirstOrDefault(); %>
            <div class="col-xl-12 col-md-12 col-12">
                <div class="img-thumb-banner-giua">
                    <a href="javascript:void(0)">
                        <img src="/Uploads/thumb/<%=GetBanner1.Thumb %>" class="img-fluid w-100" />
                    </a>
                    <div class="text">
                        <div class="text-uppercase ">
                            <%=GetBanner1.Name %>
                        </div>
                        <div class="heading">
                            <a href="javascript:void(0)"><%=GetBanner1.Content %></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>--%>
    <%var zone_trang_chu_child_cut_2 = zone_trang_chu_child.Skip(1).Take(1).FirstOrDefault(); %>
    <%if (zone_trang_chu_child_cut_2 != null)
        { %>
    <section class="list-product-home py-3 py-lg-4 text-center clhidden" style="margin-top: 0px; padding-top: 0">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5 col-12">
                    <h2 class="heading-home"><%=zone_trang_chu_child_cut_2.Name %></h2>
                    <div class="color-52575C text-center mb-4">
                        <%=zone_trang_chu_child_cut_2.Description %>
                    </div>
                </div>
            </div>
            <div class="row">

                <%  var zones = zone_trang_chu_child_cut_2.Id.ToString();
                    int TotalNewRows = 0;

                    var san_phams = ProductBo.FE_Product_Search(""
                        , "", zones, 0, 0, DateTime.MinValue, DateTime.MaxValue, 0, -1, (int)EnumProductSortOrder.CreatedDateDesc, (int)EnumProductStatus.Published, 1, 8, ref TotalNewRows);
                %>
                <%foreach (var item in san_phams)
                    {
                        //var link_target = "javascript:void(0)";
                        var link_target = string.Format("/san-pham/{0}.p{1}.htm", item.Url, item.Id);
                %>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative">
                            <img src="/Uploads/thumb/<%=item.Avatar %>" class="img-fluid w-100" alt="<%=item.Title %>">
                            <%if (item.IsHot == true)
                                { %>
                            <div class="tag">
                                HOT
                            </div>
                            <%} %>
                            <%if (item.Percent > 0)
                                { %>
                            <div class="tag bg-C6832B">
                                -<%=item.Percent %>%
                            </div>
                            <%} %>
                            <div class="overlay">
                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2">
                                    <img src="/Themes/images/heart-icon.svg" class="img-fluid "></button>
                                <a href="/gio-hang" class="btn btn-product add-to-cart" data-idsp="<%=item.Id %>">
                                    <img src="/Themes/images/cart-icon.svg" data-idsp="<%=item.Id %>" class="img-fluid" style="margin-top: 10px"></a>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="javascript:void(0)" title="<%=item.Folder %>"><%=item.Folder %></a>
                            </div>
                            <h5 class="title">
                                <a href="<%=link_target %>" class="<%=item.Title %>"><%=item.Title %></a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    <%=UIHelper.FormatNumber(item.DiscountPrice > 0 ? item.DiscountPrice : item.Price) %> vnđ
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%} %>
            </div>
        </div>
    </section>
    <%} %>
    <!--Sp yêu thích-->

    <!--banner category-->
    <section class="banner-cate py-3 text-center clhidden">
        <div class="container">
            <div class="row">
                <%var GetBanner2 = ConfigBo.AdvGetByType(8).OrderBy(x => x.SortOrder).Take(2); %>
                <%foreach (var item in GetBanner2)
                    {%>
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="img-thumb-banner">
                        <a href="javascript:void(0)">
                            <img src="/Uploads/thumb/<%=item.Thumb %>" class="img-fluid imit" alt="" />
                        </a>
                        <div class="text">
                            <div class="text-uppercase ">
                                <%=item.Name %>
                            </div>
                            <div class="heading">
                                <a href="javascript:void(0)"><%=item.Content %></a>
                            </div>
                        </div>
                    </div>
                </div>
                <%}%>
            </div>
        </div>
    </section>
    <%var zone_trang_chu_child_cut_3 = zone_trang_chu_child.Skip(2); %>
    <%foreach (var ztc in zone_trang_chu_child_cut_3)
        { %>
    <section class="list-product-home py-3 py-lg-4  text-center clhidden">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5 col-12">
                    <h2 class="heading-home"><%=ztc.Name %></h2>
                    <div class="color-52575C text-center mb-4">
                        <%=ztc.Description %>
                    </div>
                </div>
            </div>
            <div class="row table-item-sp">
                <%  var zones = ztc.Id.ToString();
                    int TotalNewRows = 0;
                    var san_phams = ProductBo.FE_Product_Search("", "", zones, 0, 0, DateTime.MinValue, DateTime.MaxValue, 0, -1, (int)EnumProductSortOrder.CreatedDateDesc, (int)EnumProductStatus.Published, 1, 8, ref TotalNewRows);
                %>
                <%if (san_phams != null)
                    {%>
                <%foreach (var item in san_phams)
                    {
                        //var link_target = "javascript:void(0)";
                        var link_target = string.Format("/san-pham/{0}.p{1}.htm", item.Url, item.Id);
                %>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="item-product">
                        <div class="image position-relative">
                            <img src="/Uploads/thumb/<%=item.Avatar %>" class="img-fluid w-100" alt="<%=item.Title %>">
                            <%if (item.IsHot == true)
                                { %>
                            <div class="tag">
                                HOT
                            </div>
                            <%} %>
                            <%if (item.Percent > 0)
                                { %>
                            <div class="tag bg-C6832B">
                                -<%=item.Percent %>%
                            </div>
                            <%} %>
                            <div class="overlay">
                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2">
                                    <img src="/Themes/images/heart-icon.svg" class="img-fluid "></button>
                                <a href="/gio-hang" class="btn btn-product add-to-cart" data-idsp="<%=item.Id %>">
                                    <img src="/Themes/images/cart-icon.svg" class="img-fluid " data-idsp="<%=item.Id %>" style="margin-top: 10px"></a>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="javascript:void(0)" title="<%=item.Folder %>"><%=item.Folder %></a>
                            </div>
                            <h5 class="title">
                                <a href="<%=link_target %>" class="<%=item.Title %>"><%=item.Title %></a>
                            </h5>
                            <div class="price">
                                <div class="new color-C6832B font-weight-bold mr-3">
                                    <%=UIHelper.FormatNumber(item.DiscountPrice > 0 ? item.DiscountPrice : item.Price) %> vnđ
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%} %>
                <%}%>
                 <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6 binding"  style="display:none">
                    <div class="item-product">
                        <div class="image position-relative"> 
                                <img src="images/change/product-01.jpg" class="img-fluid w-100 Avata" alt=""> 
                            <div class="tag bg-C6832B hot">
                               
                            </div>
                            <div class="tag bg-C6832B gg">
                                
                            </div>
                            <div class="overlay">
                            </div>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2 dataid"><img src="/Themes/images/heart-icon.svg " class="img-fluid "></button>
                                <a class="btn btn-product add-to-cart dataid"><img src="/Themes/images/cart-icon.svg " class="img-fluid" style="margin-top: 10px"></a>
                            </div>
                        </div>
                        <div class="py-3">
                            <div class="cate">
                                <a href="" class="foder" title="">Nội thất</a>
                            </div>
                            <h5 class="title">
                                <a href="" class="titlesp">Bàn trà titan nảy mầm MC0036</a>
                            </h5>
                            <div class="price  ">
                                <div class="new color-C6832B font-weight-bold mr-3 piricesp">
                                    200.000 vnđ
                                </div> 
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="text-danger mb-4">
            <small class="alet "></small>
            </div>
            <div class="text-center">
                <a href="javascript:void(0)" role="button" class="btn btn-outline-viewmore xemthem px-5"data-idzone="<%=zones %>">Xem thêm +</a>
            </div>
        </div>
    </section>

    <%} %>
    <!--Sp theo yc-->

    <!--Tin tức-->
    <section class="list-product-home py-3 py-lg-4  bg-f2f2f2 text-center clhidden">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5 col-12">
                    <h2 class="heading-home">Tin tức</h2>
                    <div class="color-52575C text-center mb-4">
                        <%=UIHelper.GetConfigByName("TextTinTuc") %>
                    </div>
                </div>
            </div>
            <div class="row">
                <%var all_zone = ZoneBo.GetAllZone(1);
                    var zone_bai_viet_parents = all_zone.Where(r => r.ParentId == 0 && r.ShortUrl == "blog").FirstOrDefault();
                %>
                <%if (zone_bai_viet_parents != null)
                    { %>
                <%var total_moi_cap_nhat = 0; %>
                <%var list_news_moi_cap_nhat = NewsBo.SearchNews_ShowHome("", "", "",true, DateTime.MinValue, DateTime.MaxValue, NewsFilterFieldForUsername.CreatedBy, NewsSortExpression.CreatedDateDesc, (int)NewsStatus.Published, -1, 1, 3, ref total_moi_cap_nhat); %>
                <%var mcn_cut_1 = list_news_moi_cap_nhat.Where(x => x.IsLandingPage == true ); %>
                <%if (mcn_cut_1 != null)
                    { %>
                <%foreach (var item in mcn_cut_1)
                    {%>
                <%var l = string.Format("/bai-viet-chi-tiet/{0}.a{1}.htm", item.Url, item.Id); %>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 text-center">
                    <div class="item-news">
                        <div class="image position-relative">
                            <a href="<%=l %>">
                                <img src="/Uploads/thumb/<%=item.Avatar %>" class="img-fluid w-100" alt=""></a>
                            <div class="tag">
                                <a href="<%=l %>" title=""><%=item.CreatedDate %></a>
                            </div>
                        </div>
                        <div class="py-4 px-3">
                            <h4 class="title">
                                <a href="<%=l %>" title=""><%=item.Title %></a>
                            </h4>
                            <div class="des">
                                <%=item.Sapo %>
                                <a href="<%=l %>" class="color-C6832B text-underline ml-1 nowrap ">Đọc tiếp</a>
                            </div>
                            <div class="text-right">
                            </div>
                        </div>
                        <div>
                        </div>
                    </div>
                </div>
                <%} %>
                <%}%>
                <%} %>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script src="/Themes/js/sanphamController.js"></script>
    <script src="/Themes/js/PhanTrang.js"></script>
</asp:Content>
