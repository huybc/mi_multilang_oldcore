new WOW().init();


 

function topFunction() {
    $('body,html').animate({
        scrollTop: 0                       // Scroll to top of body
    }, 500);
}
 

function slidethumb2(){
    var galleryThumbs2 = new Swiper('.slide-product-detail-2 .gallery-thumbs-2', {
        spaceBetween: 15,
        slidesPerView: 6,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        breakpoints: {
            320: {
                slidesPerView: 3,
            },
            567: {
                slidesPerView: 3,
                spaceBetween: 5,
            },
            767: {
                slidesPerView: 4,
                spaceBetween: 10,
            },
            991: {
                slidesPerView: 4,
                spaceBetween: 10,
            },
            1023: {
                slidesPerView: 5,
                spaceBetween: 15,
            },
            1365: {
                slidesPerView: 5,
                spaceBetween: 10,
            },
        },
        navigation: {
            nextEl: '.slide-product-detail-2 .swiper-button-next',
            prevEl: '.slide-product-detail-2 .swiper-button-prev',
          },
      });
      var galleryTop2 = new Swiper('.slide-product-detail-2 .gallery-top-2', {
        spaceBetween: 10,
        thumbs: {
          swiper: galleryThumbs2
        } 
      });
  }
  slidethumb2();


 
 