﻿using HempSpa.Core.Helper;
using Mi.Entity.Base.ConFig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HempSpa
{
    public partial class _default1 : BasePages
    {
        public ConfigTitle obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                obj = new ConfigTitle();
                var titleSeo = UIHelper.GetConfigByName("TitleSeoPage");
                var MetaDescription = UIHelper.GetConfigByName("descriptionPage");
                var MetaKeywords = UIHelper.GetConfigByName("MetaKeywords");
                var Avatar = UIHelper.GetConfigByName("ImageShare");

                obj.Avatar = Avatar;
                obj.MetaDescription = MetaDescription;
                obj.TitleSeo = titleSeo;
                obj.MetaKeyword = MetaKeywords;
                Page.Title = obj.TitleSeo;
                Page.MetaKeywords = obj.MetaKeyword;
                Page.MetaDescription = obj.MetaDescription;
            }
        }
    }
}