﻿<%@ Page Title="Thư viện ảnh" Language="C#" MasterPageFile="~/Themes/HempSpaModalGallery.Master" AutoEventWireup="true" CodeBehind="gallery.aspx.cs" Inherits="HempSpa.Pages.gallery" %>

<%@ Import Namespace="HempSpa.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
     <meta property="og:title" content=" <%=obj != null? obj.MetaTitle:""%>" />
    <meta property="og:description" content="<%=obj != null?obj.MetaDescription:""%>" />
    <meta property="og:image" content="<%=domainName %>/Uploads/<%=obj != null?obj.Avatar:""%>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <section class="py-5"></section>
    <% var slider = ConfigBo.AdvGetByType(9, lang); %>

    <% var anhslihead = slider.Where(x => x.IsEnable == true).OrderBy(x => x.SortOrder).FirstOrDefault(); %>

    <section class="banner-page py-5" style="background: url(/Uploads/<%=anhslihead == null ? "":anhslihead.Thumb%>) no-repeat center center; background-size: cover; color: #fff; text-align: center;">

        <div class="container  ">
            <div class="row justify-content-center  ">
                <div class="col-xl-6 col-md-8 col-sm-10 col-12 align-self-lg-center">
                    <h1 class="title"><%=anhslihead == null? "":anhslihead.Name %></h1>
                    <%=anhslihead == null ? "":anhslihead.Content %>
                </div>
            </div>
        </div>
    </section>

    <section class="list-images py-4 py-lg-5">
        <div class="container">
            <ul class=" nav nav-tabs justify-content-center" role="tablist">
                <li class="col-xl-2 col-lg-2 col-md-3 col-6  px-2 active ">
                    <a class="btn btn-hempspa-outline text-uppercase w-100 mb-3 pills-profile-tab" data-lang="<%=lang %>" data-toggle="tab" href="#pills-1"><%=Language.Gallery %></a>

                </li>
                <li class="col-xl-2 col-lg-2 col-md-3 col-6 px-2">
                    <a class="btn btn-hempspa-outline text-uppercase w-100 mb-3 pills-profile-tab"data-alias ="xemphong"data-toggle="tab" href="#pills-2"
                        aria-controls="pills-profile" aria-selected="false"  data-lang="<%=lang %>" ><%=Language.viewroom %></a>
                </li>
                <li class="col-xl-2 col-lg-2 col-md-3 col-6 px-2">
                    <a class="btn btn-hempspa-outline text-uppercase w-100 mb-3 pills-profile-tab"data-alias ="xemsong"  data-toggle="tab" href="#pills-3"
                        aria-controls="pills-profile" aria-selected="false"  data-lang="<%=lang %>" ><%=Language.viewriver %></a>
                </li>
                <li class="col-xl-2 col-lg-2 col-md-3  col-6 px-2">
                    <a class="btn btn-hempspa-outline text-uppercase  w-100 mb-3 pills-profile-tab"data-alias ="bancong"  data-toggle="tab" href="#pills-4"
                        aria-controls="pills-profile" aria-selected="false"  data-lang="<%=lang %>" ><%=Language.reception %></a>
                </li>
            </ul>
            <div class="tab-content tab-content-hemp mb-5 " id="pills-tabContent">
                <div class="tab-pane fade in active show" id="pills-1" aria-labelledby="pills-home-tab">
                    <div class="row no-gutters BinAnh"">
                        <%var totalRow = 0; %>
                        <%var getAllNewAnh = NewsBo.GetNewsDetailWithLanguage(11, lang, 2, 0, "", 1, 6, ref totalRow);%>
                        <%foreach (var item in getAllNewAnh)
                            {%>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 px-2 imit">
                            <div class="image" >
                                <a href="javascript:;">
                                    <img src="/Uploads/<%=item.Avatar %>" class="img-fluid immd" style="width:100%;height:250px;margin:10px" data-id="<%=item.NewsId %>" data-lang="<%=lang %>" alt="" />
                                </a>

                            </div>
                        </div>

                        <% } %>
                        
                    </div>
                    <div class="col-12 mt-4 text-center text-danger ThongBao"></div>
                    <div class="text-center my-5">
                        <button class="btn btn-hempspa-outline px-lg-5 text-uppercase view_more_image" data-lang="<%=lang %>"><%=Language.VIEW_MORE %></button>
                    </div>
                </div>
               <%-- table 2--%>
                <div class="tab-pane fade" id="pills-2" role="tabpanel" aria-labelledby="pills-profile-tab2">
                    <div class="row no-gutters imit BinAnh">
                       
                    </div>
                     <div class="col-12 mt-4 text-center text-danger ThongBao"></div>
                    <div class="text-center my-5">
                        <button class="btn btn-hempspa-outline px-lg-5 text-uppercase view_more_image"data-alias ="xemphong"  data-lang="<%=lang %>"><%=Language.VIEW_MORE %></button>
                    </div>
                </div>
                <%--table 3--%>
                <div class="tab-pane fade" id="pills-3" role="tabpanel" aria-labelledby="pills-profile-tab3">
                    <div class="row no-gutters imit BinAnh"> 
                        
                    </div>
                    <div class="col-12 mt-4  text-center text-danger ThongBao" ></div>
                    <div class="text-center my-5">
                        <button class="btn btn-hempspa-outline px-lg-5 text-uppercase view_more_image"data-alias ="xemsong" data-lang="<%=lang %>"><%=Language.VIEW_MORE %></button>
                    </div>
                </div>
                <%--table 4--%>
                <div class="tab-pane fade" id="pills-4" role="tabpanel" aria-labelledby="pills-profile-tab4">
                    <div class="row no-gutters imit BinAnh" >
                        
                    </div>
                     <div class="col-12 mt-4 text-center text-danger ThongBao"></div>
                    <div class="text-center my-5">
                        <button class="btn btn-hempspa-outline px-lg-5 text-uppercase view_more_image"data-alias ="bancong"  data-lang="<%=lang %>"><%=Language.VIEW_MORE %></button>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <% var anhsllast = slider.Where(x => x.IsEnable == true).OrderByDescending(x => x.SortOrder).FirstOrDefault(); %>

    <section class="contact-home py-5" style="background: url(/Uploads/<%=anhsllast == null ? "":anhsllast.Thumb%>) no-repeat center center; background-size: cover; color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <h2 class="heading"><%=UIHelper.GetConfigByName("TitleAddress") %> </h2>
                    <ul class="list-contact mb-5">
                        <li>
                            <img src="/Themes/images/Location-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Address") %>
                        </li>
                        <li>
                            <img src="/Themes/images/call-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Phone") %>
                        </li>
                        <li>
                            <img src="/Themes/images/Letter-icon.svg" class="img-fluid mr-3 " /><%=UIHelper.GetConfigByName("Gmail") %>
                        </li>
                    </ul>
                    <div class="social">
                        <% var getconfigByName1 = ConfigBo.GetByConfigName("Fcebook"); %>
                        <div class="item">
                            <a href="<%=getconfigByName1.ConfigInitValue%>"><i class="fab fa-facebook-f mr-3"></i><%=getconfigByName1.ConfigValue %></a>
                        </div>
                        <% var getconfigByName2 = ConfigBo.GetByConfigName("Intargram"); %>
                        <div class="item">
                            <a href="<%=getconfigByName2.ConfigInitValue%>"><i class="fab fa-instagram mr-3"></i><%=getconfigByName2.ConfigValue %></a>
                        </div>
                        <% var getconfigByName3 = ConfigBo.GetByConfigName("Zalo"); %>
                        <div class="item">
                            <a href="<%=getconfigByName3.ConfigInitValue%>">
                                <img src="/Themes/images/zalo-icon.svg" class="img-fluid mr-3" /><%=getconfigByName3.ConfigValue %></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12 col-12 px-xl-5">
                    <form class="form-contact-home"id="gui"  data-type="tuvan">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" id="txtHoten" placeholder="<%=Language.Full_Name %>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" id="txtGmail" placeholder="Gmail *">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="tel" class="form-control" id="txtPhone" placeholder="<%=Language.Hotline %>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea rows="5" class="form-control" id="txtTinNhan" placeholder="<%=Language.Note_Detail %>"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 mb-0">
                                <button type="submit" class="btn btn-hempspa w-100 text-uppercase"><%=Language.Submit %></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
