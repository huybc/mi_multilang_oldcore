﻿<%@ Page Title="Blog" Language="C#" MasterPageFile="~/Themes/HempSpas.Master" AutoEventWireup="true" CodeBehind="blogs.aspx.cs" Inherits="HempSpa.Pages.blogs" %>

<%@ Register Src="~/Pages/Controls/BlogRightMenu.ascx" TagPrefix="uc1" TagName="BlogRightMenu" %>
<%@ Import Namespace="HempSpa.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
     <meta property="og:title" content=" <%=obj != null? obj.MetaTitle:""%>" />
    <meta property="og:description" content="<%=obj != null?obj.MetaDescription:""%>" />
    <meta property="og:image" content="<%=domainName %>/Uploads/<%=obj != null?obj.Avatar:""%>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <section class="py-5"></section>
    <% var slider = ConfigBo.AdvGetByType(10, lang); %>

    <% var anhslihead = slider.Where(x => x.IsEnable == true).OrderBy(x => x.SortOrder).FirstOrDefault(); %>

    <section class="banner-page py-5" style="background: url(/Uploads/<%=anhslihead == null ? "":anhslihead.Thumb%>) no-repeat center center; background-size: cover; color: #fff; text-align: center;">

        <div class="container  ">
            <div class="row justify-content-center  ">
                <div class="col-xl-6 col-md-8 col-sm-10 col-12 align-self-lg-center">
                    <h1 class="title"><%=anhslihead == null? "":anhslihead.Name %></h1>
                    <%=anhslihead == null ? "":anhslihead.Content %>
                </div>
            </div>
        </div>
    </section>

    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-7 col-12">
                    <%var totalRow = 0;%>
                    <%var getDetailBlogById = NewsBo.GetNewsDetailWithLanguage(3, lang, 2, 0, "", 1, 9, ref totalRow); %>
                    <%if (getDetailBlogById != null)
                        {%>
                          <%var sapXepListBlog = getDetailBlogById.Where(x => x.Title != "").OrderByDescending(x => x.CreatedDate).Take(3).ToList(); %>

                    <%foreach (var item in sapXepListBlog)
                        {%>
                    <% DateTime ThoiGianTao1 = item.CreatedDate;%>
                    <% var link_taget1 = string.Format("/{0}/blog-chi-tiet/{1}.{2}.htm", currentLanguage, item.Alias, item.NewsId);%>
                    <div class="item-blog">
                        <div class="label-date">
                            <div class="font-weight-bold">
                                <%=ThoiGianTao1.Day %>
                            </div>
                            <small>Month <%=ThoiGianTao1.Month %></small>
                        </div>
                        <div class="image">
                            <a href="<%=link_taget1 %>">
                                <img src="/uploads/<%=item.Avatar %>" class="w-100" alt="<%=item.Title %>" />
                            </a>
                        </div>
                        <div class="box-text">
                            <div class="btn  btn-hempspa px-xl-5" style="background: #58935B;">
                                <%=item.ZoneName %>
                            </div>
                            <h4 class="title">
                                <a href="<%=link_taget1 %>"><%=item.Title %></a>
                            </h4>
                            <div class="des mb-2 style1">
                                <%=item.Sapo%>
                            </div>
                            <a href="<%=link_taget1 %>" class="color-345F1F text-uppercase font-weight-bold"><%=Language.Continue_reading %></a>
                        </div>

                    </div>
                    <%}%>
                    <%}%>
                  
                </div>
                <div class="col-xl-4 col-lg-4 col-md-5 col-12">
                    <uc1:BlogRightMenu runat="server" ID="BlogRightMenu" />

                </div>
            </div>
        </div>
    </section>
    <% var anhsllast = slider.Where(x => x.IsEnable == true).OrderByDescending(x => x.SortOrder).FirstOrDefault(); %>

    <section class="contact-home py-5" style="background: url(/Uploads/<%=anhsllast == null ? "":anhsllast.Thumb%>) no-repeat center center; background-size: cover; color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <h2 class="heading"><%=UIHelper.GetConfigByName("TitleAddress") %> </h2>
                    <ul class="list-contact mb-5">
                        <li>
                            <img src="/Themes/images/Location-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Address") %>
                        </li>
                        <li>
                            <img src="/Themes/images/call-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Phone") %>
                        </li>
                        <li>
                            <img src="/Themes/images/Letter-icon.svg" class="img-fluid mr-3 " /><%=UIHelper.GetConfigByName("Gmail") %>
                        </li>
                    </ul>
                    <div class="social">
                        <% var getconfigByName1 = ConfigBo.GetByConfigName("Fcebook"); %>
                        <div class="item">
                            <a href="<%=getconfigByName1.ConfigInitValue%>"><i class="fab fa-facebook-f mr-3"></i><%=getconfigByName1.ConfigValue %></a>
                        </div>
                        <% var getconfigByName2 = ConfigBo.GetByConfigName("Intargram"); %>
                        <div class="item">
                            <a href="<%=getconfigByName2.ConfigInitValue%>"><i class="fab fa-instagram mr-3"></i><%=getconfigByName2.ConfigValue %></a>
                        </div>
                        <% var getconfigByName3 = ConfigBo.GetByConfigName("Zalo"); %>
                        <div class="item">
                            <a href="<%=getconfigByName3.ConfigInitValue%>">
                                <img src="/Themes/images/zalo-icon.svg" class="img-fluid mr-3" /><%=getconfigByName3.ConfigValue %></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12 col-12 px-xl-5">
                    <form class="form-contact-home" id="gui" data-type="tuvan">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" id="txtHoten" placeholder="<%=Language.Full_Name %>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" id="txtGmail" placeholder="Gmail *">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="tel" class="form-control" id="txtPhone" placeholder="<%=Language.Hotline %>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea rows="5" class="form-control" id="txtTinNhan" placeholder="<%=Language.Note %>"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 mb-0">
                                <button type="submit" class="btn btn-hempspa w-100 text-uppercase"><%=Language.Submit %></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
