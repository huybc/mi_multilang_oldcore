﻿<%@ Page Title="Lịch trình" Language="C#" MasterPageFile="~/Themes/HempSpas.Master" AutoEventWireup="true" CodeBehind="tour.aspx.cs" Inherits="HempSpa.Pages.tour" %>

<%@ Import Namespace="HempSpa.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
     <meta property="og:title" content=" <%=obj != null? obj.MetaTitle:""%>" />
    <meta property="og:description" content="<%=obj != null?obj.MetaDescription:""%>" />
    <meta property="og:image" content="<%=domainName %>/Uploads/<%=obj != null?obj.Avatar:""%>" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <section class="py-5"></section>
    <% var slider = ConfigBo.AdvGetByType(12, lang); %>

    <% var anhslihead = slider.Where(x => x.IsEnable == true).OrderBy(x => x.SortOrder).FirstOrDefault(); %>

    <section class="banner-page py-5" style="background: url(/Uploads/<%=anhslihead == null ? "":anhslihead.Thumb%>) no-repeat center center; background-size: cover; color: #fff; text-align: center;">

        <div class="container  ">
            <div class="row justify-content-center  ">
                <div class="col-xl-6 col-md-8 col-sm-10 col-12 align-self-lg-center">
                    <h1 class="title"><%=anhslihead == null? "":anhslihead.Name %></h1>
                    <%=anhslihead == null ? "":anhslihead.Content %>
                </div>
            </div>
        </div>
    </section>


    <section class="list-tour py-4 py-lg-5">
        <div class="container">
            <div class="row" id="List_tour">

                <%var totalRow = 0; %>
                <%var getZoneLichTrinh = ZoneBo.GetZoneWithLanguageByType(12, lang).Where(x => x.Alias == "tour").FirstOrDefault();%>
                <% %>
                <%if (getZoneLichTrinh != null)
                  {%>
                         <% var GetNewByZoneid = NewsBo.GetNewsDetailWithLanguage(12, lang, 2, getZoneLichTrinh.Id, "", 1, 9, ref totalRow).OrderByDescending(x => x.CreatedDate).ToList(); %>
                            <%foreach (var item in GetNewByZoneid)
                                {%>
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">

                                        <% var link_item = string.Format("/{0}/{1}/{2}.{3}.htm", currentLanguage,getZoneLichTrinh.Alias, item.Url, item.NewsId); %>

                                        <div class="item-tour">
                                            <div class="image position-relative">
                                                <a href="<%=link_item %>">
                                                    <img src="/Uploads/<%=item.Avatar %>" class="img-fluid w-100" alt="" /></a>
                                                <div class="tag">

                                                    <a href="<%=link_item %>" title=""><%=item.Title %></a>
                                                </div>
                                            </div>
                                            <%var gia_detail = Newtonsoft.Json.JsonConvert.DeserializeObject<DonGiaEntity>(item.Extras);%>

                                            <div class="py-4 px-3">
                                                <h4 class="title">
                                                    <a href="<%=link_item %>" title=""><%=item.Sapo%></a>
                                                </h4>
                                                <div class="d-flex flex-wrap">
                                                    <div class="time-tour">
                                                        <%=gia_detail.SoNgay %>
                                                    </div>
                                                    <div class="price-tour ml-auto">
                                                        <%=gia_detail.HocVitour %>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <%}%>
                            
                <% }%>
            </div>
             <div class="col-12 mt-4 text-center text-danger" id="sp4"></div>
            <div class="text-center">
                <button class="btn btn-hempspa-outline px-lg-5 text-uppercase" id="view_tour" data-curentlang="<%=currentLanguage %>" data-lang="<%=lang %>"><%=Language.VIEW_MORE %></button>
            </div>
        </div>
    </section>

    <% var anhsllast = slider.Where(x => x.IsEnable == true).OrderByDescending(x => x.SortOrder).FirstOrDefault(); %>

    <section class="contact-home py-5" style="background: url(/Uploads/<%=anhsllast == null ? "":anhsllast.Thumb%>) no-repeat center center; background-size: cover; color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <h2 class="heading"><%=UIHelper.GetConfigByName("TitleAddress") %> </h2>
                    <ul class="list-contact mb-5">
                        <li>
                            <img src="/Themes/images/Location-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Address") %>
                        </li>
                        <li>
                            <img src="/Themes/images/call-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Phone") %>
                        </li>
                        <li>
                            <img src="/Themes/images/Letter-icon.svg" class="img-fluid mr-3 " /><%=UIHelper.GetConfigByName("Gmail") %>
                        </li>
                    </ul>
                    <div class="social">
                        <% var getconfigByName1 = ConfigBo.GetByConfigName("Fcebook"); %>
                        <div class="item">
                            <a href="<%=getconfigByName1.ConfigInitValue%>"><i class="fab fa-facebook-f mr-3"></i><%=getconfigByName1.ConfigValue %></a>
                        </div>
                        <% var getconfigByName2 = ConfigBo.GetByConfigName("Intargram"); %>
                        <div class="item">
                            <a href="<%=getconfigByName2.ConfigInitValue%>"><i class="fab fa-instagram mr-3"></i><%=getconfigByName2.ConfigValue %></a>
                        </div>
                        <% var getconfigByName3 = ConfigBo.GetByConfigName("Zalo"); %>
                        <div class="item">
                            <a href="<%=getconfigByName3.ConfigInitValue%>">
                                <img src="/Themes/images/zalo-icon.svg" class="img-fluid mr-3" /><%=getconfigByName3.ConfigValue %></a>
                        </div>
                    </div>
                </div>
                 <div class="col-xl-6 col-md-6 col-sm-12 col-12 px-xl-5">
                    <form class="form-contact-home"id="gui"  data-type="tuvan">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" id="txtHoten" placeholder="<%=Language.Full_Name %> *" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" id="txtGmail" placeholder="Gmail *">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="tel" class="form-control" id="txtPhone" placeholder="<%=Language.Hotline %> *" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea rows="5" class="form-control" id="txtTinNhan" placeholder="<%=Language.Note %> *"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 mb-0">
                                <button type="submit" class="btn btn-hempspa w-100 text-uppercase">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
