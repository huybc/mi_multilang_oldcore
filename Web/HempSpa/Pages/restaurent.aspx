﻿<%@ Page Title="Nhà hàng" Language="C#" MasterPageFile="~/Themes/HempSpas.Master" AutoEventWireup="true" CodeBehind="restaurent.aspx.cs" Inherits="HempSpa.Pages.restaurent" %>

<%@ Import Namespace="HempSpa.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
     <meta property="og:title" content=" <%=obj != null? obj.MetaTitle:""%>" />
    <meta property="og:description" content="<%=obj != null?obj.MetaDescription:""%>" />
    <meta property="og:image" content="<%=domainName %>/Uploads/<%=obj != null?obj.Avatar:""%>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>

    <section class="py-5"></section>
    <% var slider = ConfigBo.AdvGetByType(7, lang); %>
    <% var anhslihead = slider.Where(x => x.IsEnable == true).OrderBy(x => x.SortOrder).FirstOrDefault(); %>
    <section class="banner-page py-5" style="background: url(/uploads/<%=anhslihead == null ? "":anhslihead.Thumb%>) no-repeat center center; background-size: cover; color: #fff; text-align: center;">
        <div class="container  ">
            <div class="row justify-content-center  ">
                <div class="col-xl-6 col-md-8 col-sm-10 col-12 align-self-lg-center">
                    <h1 class="title"><%=anhslihead != null ?anhslihead.Name:""%></h1>
                    <%=anhslihead == null?"":anhslihead.Content %>
                </div>

            </div>
        </div>
    </section>

    <section class="container py-5">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12 imit">
                <%var zone_nha_hang = ZoneBo.GetZoneWithLanguageByType(12, lang).Where(r => r.Alias == "nha-hang").FirstOrDefault(); %>
                <%var total_nha_hang = 0; %>
                <%if (zone_nha_hang != null)
                    {%>
                <%var nha_hang_detail = new NewsDetailWithLanguageEntity(); %>
                <% nha_hang_detail = NewsBo.GetNewsDetailWithLanguage(12, lang, 2, zone_nha_hang.Id, "", 1, 1, ref total_nha_hang).FirstOrDefault(); %>
                <div class="color-333D36">
                    <%=nha_hang_detail.Body %>
                </div>
                <div class="text-center mb-4 ">
                    <img src="/Uploads/<%=nha_hang_detail.Avatar %>" class="w-100 " />
                </div>
                <%}%>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12">
                <form class="form-right mb-4" id="dat-lich-chung" data-type="nha-hang">
                    <div class="text-center color-345F1F font-weight-bold mb-4">
                        <%=Language.BOOK_NOW__ONLY_2_MINUTES %>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label><%=Language.Full_Name %> *</label>
                            <input type="text" class="form-control" id="txtName" placeholder="" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label><%=Language.Hotline %>  *</label>
                            <input type="text" class="form-control" id="txtPhoneNumber" placeholder="" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Email *</label>
                            <input type="text" class="form-control" id="txtEmail" placeholder="">
                        </div>
                        <div class="form-group col-md-12">
                            <label><%=Language.checkin %>  *</label>
                            <input type="text" name="" class="form-control dpk" id="txtNgayDen" placeholder="" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label><%=Language.checkout %>  *</label>
                            <input type="text" name="" class="form-control dpk" id="txtNgayDat" placeholder="" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label><%=Language.Note %>  *</label>
                            <textarea type="text" rows="5" class="form-control" id="txtMessage"
                                placeholder="<%=Language.Note_Detail %> *"></textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-hempspa w-100 text-uppercase mb-3 "><%=Language.Book_now %> </button>
                            <div class="color-345F1F text-center">
                                <%=Language.Or_Contact_Us%> : <%=UIHelper.GetConfigByName("Phone2") %>
                            </div>
                        </div>
                    </div>
                </form>
                <%var BannerDetail = slider.Where(x => x.SortOrder == 2).FirstOrDefault(); %>
                <div class="banner-menu-right" style="color: #fff; background: url(/Themes/images/change/<%=BannerDetail == null ? "":BannerDetail.Thumb%>) no-repeat center; background-size: cover; padding: 3rem; min-height: 240px; margin-bottom: 1.5rem;">
                    <h4 class="font-weight-bold mb-3">
                        <a href="<%=BannerDetail == null? "":BannerDetail.Url %>"><%=BannerDetail == null? "":BannerDetail.Name %>
                        </a>
                    </h4>
                    <div>
                        <%=BannerDetail == null? "":BannerDetail.Content %>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <% var anhsllast = slider.Where(x => x.IsEnable == true).OrderByDescending(x => x.SortOrder).FirstOrDefault(); %>

    <section class="contact-home py-5" style="background: url(/Uploads/<%=anhsllast == null ? "":anhsllast.Thumb%>) no-repeat center center; background-size: cover; color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <h2 class="heading"><%=UIHelper.GetConfigByName("TitleAddress") %> </h2>
                    <ul class="list-contact mb-5">
                        <li>
                            <img src="/Themes/images/Location-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Address") %>
                        </li>
                        <li>
                            <img src="/Themes/images/call-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Phone") %>
                        </li>
                        <li>
                            <img src="/Themes/images/Letter-icon.svg" class="img-fluid mr-3 " /><%=UIHelper.GetConfigByName("Gmail") %>
                        </li>
                    </ul>
                    <div class="social">
                        <% var getconfigByName1 = ConfigBo.GetByConfigName("Fcebook"); %>
                        <div class="item">
                            <a href="<%=getconfigByName1.ConfigInitValue%>"><i class="fab fa-facebook-f mr-3"></i><%=getconfigByName1.ConfigValue %></a>
                        </div>
                        <% var getconfigByName2 = ConfigBo.GetByConfigName("Intargram"); %>
                        <div class="item">
                            <a href="<%=getconfigByName2.ConfigInitValue%>"><i class="fab fa-instagram mr-3"></i><%=getconfigByName2.ConfigValue %></a>
                        </div>
                        <% var getconfigByName3 = ConfigBo.GetByConfigName("Zalo"); %>
                        <div class="item">
                            <a href="<%=getconfigByName3.ConfigInitValue%>">
                                <img src="/Themes/images/zalo-icon.svg" class="img-fluid mr-3" /><%=getconfigByName3.ConfigValue %></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12 col-12 px-xl-5">
                    <form class="form-contact-home" id="gui" data-type="tuvan">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" id="txtHoten" placeholder="<%=Language.Full_Name %>  *" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" id="txtGmail" placeholder="Gmail *">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="tel" class="form-control" id="txtPhone" placeholder="<%=Language.Hotline %>  *" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea rows="5" class="form-control" id="txtTinNhan" placeholder="<%=Language.Note_Detail %> *"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 mb-0">
                                <button type="submit" class="btn btn-hempspa w-100 text-uppercase"><%=Language.Submit %> </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
