﻿<%@ Page Title="Chi tiết sản phẩm" Language="C#" MasterPageFile="~/Themes/HempSpaProduct.Master" AutoEventWireup="true" CodeBehind="product-detail.aspx.cs" Inherits="HempSpa.Pages.product_detail" %>

<%@ Import Namespace="HempSpa.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="application/ld+json">
            {
              "@context": "http://schema.org/",
              "@type": "Product",
              "name": "<%= obj.Title %>",
              "image": "https://hanuchi.com/uploads/<%= obj.Avatar %>",
              "description": "<%= Utility.SubWordInString(obj.MetaDescription, 160) %>",
              "mpn": "925872",
              "brand": {
                "@type": "Thing",
                "name": ""
              },
             "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "4.4",
               <%-- "reviewCount": "<%= obj.ViewCount %>"--%>
              },
              "offers": {
                "@type": "Offer",
                "priceCurrency": "VND",
               <%-- "price": "<%= obj.Price %>",
                "priceValidUntil": "<%= obj.DiscountPrice %>",--%>
                "itemCondition": "http://schema.org/UsedCondition",
                "availability": "http://schema.org/InStock",
                "seller": {
                  "@type": "Organization",
                  "name": "Executive Objects"
                }
              }
            }
    </script>
    <section class="py-5"></section>
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>

    <% var slider = ConfigBo.AdvGetByType(8, lang); %>
    <%var getIdSanpham = int.Parse(Page.RouteData.Values["id"].ToString()); %>
    <%var getSpById = NewsBo.GetNewsDetailWithLanguageById(getIdSanpham, lang); %>
    <%var GetPart = NewsBo.GetPartInNews(getIdSanpham, lang); %>
    <%var GetPartNew = GetPart.FirstOrDefault(); %>
    <section class="product-detail py-5">
        <div class="container">
            <div class="row">
                <%var linkTrong = "javascripts:void(0)"; %>

                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <div class="row h-100" id="AnhdetailSp">

                        <div class="col-xl-3 col-md-4 col-sm-4 col-12  d-none d-md-block imit">
                            <div class="swiper-container gallery-thumbs ">
                                <div class="swiper-wrapper " id="addAnh1">
                                    <%var _rexge = @"/.*?\.(png|jpg|svg)";%>
                                    <%if (GetPartNew != null)
                                        { %>
                                    <%var listImg = Regex.Matches(GetPartNew.Content, _rexge);%>
                                    <%foreach (var item in listImg)
                                        {%>
                                    <%string[] splipChuoi = item.ToString().Split('/');%>
                                    <div class="swiper-slide" style="background-image: url(/uploads/<%=splipChuoi.Last()%>)"></div>
                                    <%}%>
                                    <%} %>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="swiper-button-next  mr-2"><i class="fas fa-chevron-up"></i></div>
                                <div class="swiper-button-prev  "><i class="fas fa-chevron-down"></i></div>
                            </div>
                        </div>
                        <div class="col-xl-9 col-md-8 col-sm-8 col-12 imit">
                            <div class="swiper-container gallery-top">
                                <div class="swiper-wrapper" id="addAnh2">
                                    <%if (GetPartNew != null)
                                        { %>
                                    <%var listImg = Regex.Matches(GetPartNew.Content, _rexge);%>
                                    <%foreach (var item in listImg)
                                        {%>
                                    <%string[] splipChuoi = item.ToString().Split('/');%>
                                    <div class="swiper-slide" style="background-image: url(/Uploads/<%=splipChuoi.Last()%>)"></div>
                                    <%}%>
                                    <%}%>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <%var gia = ""; %>
                    <%var name = ""; %>
                    <div id="detailSp">
                        <%if (getSpById != null)
                            { %>
                        <div class="cate mb-2 ">
                            <a href="<%=linkTrong %>" title=""><%=name+=(getSpById.Title)%></a>
                        </div>
                        <h1 class="title mb-3 addcl">
                            <a href="<%=linkTrong %>"><%=getSpById.Sapo %></a>
                        </h1>
                        <%var Extra = Newtonsoft.Json.JsonConvert.DeserializeObject<DonGiaEntity>(getSpById.Extras); %>
                        <div class="price mb-2 gia">
                            <%=gia +=(Extra.KhuyenMai != ""?Extra.KhuyenMai:Extra.HocVi) %>
                        </div>
                        <% } %>
                    </div>
                   
                    <div id="btmua">
                        <button class="btn btn-hempspa px-5 text-uppercase" id="mua" data-gia="<%=gia %>"data-name="<%=name %>">buy</button>
                    </div>
                    <div class="border-bottom my-3"></div>
                    <div class="mb-3">
                        <div class="mb-2">
                            <span class="color-000 font-weight-600 ">SKU:</span> N/A
                        </div>
                        <div class="mb-2">
                            <span class="color-000 font-weight-600 ">Category:</span> Furniture
                        </div>
                        <div class="mb-2">
                            <span class="color-000 font-weight-600 mr-3"><%=Language.Share %>:</span>
                            <a href="<%=linkTrong %>">
                                <img src="/Themes/images/Facebook.svg" class="img-fluid mr-3" style="height: 15px;" />
                            </a>
                            <a href="<%=linkTrong %>">
                                <img src="/Themes/images/Instagram.svg" class="img-fluid mr-3" style="height: 15px;" />
                            </a>
                            <a href="<%=linkTrong %>">
                                <img src="/Themes/images/Zalo.svg" class="img-fluid mr-3" style="height: 15px;" />
                            </a>
                            <a href="<%=linkTrong %>">
                                <img src="/Themes/images/Email.svg" class="img-fluid " style="height: 15px;" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="pt-2 my-4" style="background: #F5F7F2;">
            <div class="container">
                <ul class="nav nav-pills nav-fill-hemp mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-1" role="tab"
                            aria-controls="pills-home" aria-selected="true"><%=Language.DESCRIBE %></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-2" role="tab"
                            aria-controls="pills-profile" aria-selected="false"><%=Language.Detail %></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="tab-content tab-content-hemp mb-5" id="pills-tabContent">
                <div class="tab-pane fade show active " id="pills-1" role="tabpanel"
                    aria-labelledby="pills-home-tab">
                    <div class="h4 mb-3 "><%=Language.DESCRIBE %></div>
                    <div id="MotaSp">
                        <%=getSpById != null?getSpById.Body:"" %>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-2" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <%var parts_2 = GetPart.Skip(1).FirstOrDefault();  %>
                    <%if (parts_2 != null)
                        { %>
                    <%=parts_2.Content %>
                    <%} %>
                </div>
            </div>
        </div>
    </section>


    <section class="list-products py-4 py-lg-5">
        <div class="container">
            <h4 class="color-333D36 mb-4"><%=Language.More %></h4>
            <div class="row" id="_sp">
                <%var totalRow = 0; %>
                <%var getZoneSanPham = ZoneBo.GetZoneWithLanguageByType(12, lang).Where(x => x.Alias == "san-pham").FirstOrDefault();%>
                <%if (getZoneSanPham != null)
                    {%>
                <%var getAllSp = NewsBo.GetNewsDetailWithLanguage(12, lang, 2, getZoneSanPham.Id, "", 1, 4, ref totalRow); %>
                <%foreach (var item in getAllSp)
                    {%>
                <%var ExTra = Newtonsoft.Json.JsonConvert.DeserializeObject<DonGiaEntity>(item.Extras);%>
                <div class="col-xl-3 col-md-4 col-sm-6 col-12 layout-product">
                    <div class="item-product imit">
                        <div class="image position-relative">
                            <a href="<%=linkTrong %>" title="" class="topcontro" data-lang="<%=lang %>" data-id="<%=item.NewsId %>">
                                <img src="/Uploads/<%=item.Avatar %>" style="width:100%;height:200px;" class="img-fluid"  alt="" />
                                <%=ExTra.KhuyenMai != ""? "<div class=\"tag\">sale</div>":"" %>
                                <div class="overlay">
                                </div>

                            </a>
                            <div class="group-btn">
                                <button class="btn btn-product mr-2">
                                    <img src="/Themes/images/heart-icon.svg" class="img-fluid " /></button>
                                <button class="btn btn-product">
                                    <img src="/Themes/images/cart-icon.svg" class="img-fluid" /></button>
                            </div>
                        </div>
                        <div class="p-3">
                            <div class="des">
                                <a href="<%=linkTrong %>" title="" class="topcontro" data-lang="<%=lang %>" data-id="<%=item.NewsId %>"><%=item.Title %></a>
                            </div>
                            <a href="<%=linkTrong %>" title="">
                                <h5 class="title topcontro" data-lang="<%=lang %>" data-id="<%=item.NewsId %>"><%=item.Sapo %>
                                </h5>
                            </a>
                            <div class="price d-flex flex-wrap">
                                <div class="new color-345F1F font-weight-bold mr-3">
                                    <%=ExTra.KhuyenMai != ""?ExTra.KhuyenMai:ExTra.HocVi %>
                                </div>
                                <div class="old">
                                    <%=ExTra.KhuyenMai != ""?ExTra.HocVi:ExTra.KhuyenMai %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <%}%>

                <%}%>
                <div class="col-12" id="ft_sp">
                </div>

            </div>
            <div class="col-12 mt-4 text-center text-danger" id="sp4"></div>
            <div class="text-center my-3">
                <button class="btn btn-hempspa-outline px-lg-5 text-uppercase" data-page="1" id="_xTsanPham" data-lang="<%=lang %>" data-curentlang="<%=currentLanguage %>"><%=Language.VIEW_MORE %></button>
            </div>

        </div>
    </section>

    <% var anhsllast = slider.Where(x => x.IsEnable == true).OrderByDescending(x => x.SortOrder).FirstOrDefault(); %>

    <section class="contact-home py-5" style="background: url(/Uploads/<%=anhsllast == null ? "":anhsllast.Thumb%>) no-repeat center center; background-size: cover; color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <h2 class="heading"><%=UIHelper.GetConfigByName("TitleAddress") %> </h2>
                    <ul class="list-contact mb-5">
                        <li>
                            <img src="/Themes/images/Location-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Address") %>
                        </li>
                        <li>
                            <img src="/Themes/images/call-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Phone") %>
                        </li>
                        <li>
                            <img src="/Themes/images/Letter-icon.svg" class="img-fluid mr-3 " /><%=UIHelper.GetConfigByName("Gmail") %>
                        </li>
                    </ul>
                    <div class="social">
                        <% var getconfigByName1 = ConfigBo.GetByConfigName("Fcebook"); %>
                        <div class="item">
                            <a href="<%=getconfigByName1.ConfigInitValue%>"><i class="fab fa-facebook-f mr-3"></i><%=getconfigByName1.ConfigValue %></a>
                        </div>
                        <% var getconfigByName2 = ConfigBo.GetByConfigName("Intargram"); %>
                        <div class="item">
                            <a href="<%=getconfigByName2.ConfigInitValue%>"><i class="fab fa-instagram mr-3"></i><%=getconfigByName2.ConfigValue %></a>
                        </div>
                        <% var getconfigByName3 = ConfigBo.GetByConfigName("Zalo"); %>
                        <div class="item">
                            <a href="<%=getconfigByName3.ConfigInitValue%>">
                                <img src="/Themes/images/zalo-icon.svg" class="img-fluid mr-3" /><%=getconfigByName3.ConfigValue %></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12 col-12 px-xl-5">
                    <form class="form-contact-home" id="gui" data-type="tuvan">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" id="txtHoten" placeholder="<%=Language.Full_Name %>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" id="txtGmail" placeholder="Gmail *">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="tel" class="form-control" id="txtPhone" placeholder="<%=Language.Hotline %>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea rows="5" class="form-control" id="txtTinNhan" placeholder="<%=Language.Note_Detail %>*"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 mb-0">
                                <button type="submit" class="btn btn-hempspa w-100 text-uppercase"><%=Language.Submit %></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
