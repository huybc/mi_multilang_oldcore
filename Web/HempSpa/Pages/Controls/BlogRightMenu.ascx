﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogRightMenu.ascx.cs" Inherits="HempSpa.Pages.Controls.BlogRightMenu" %>
<%@ Import Namespace="HempSpa.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<% 
    var lang = Current.LanguageJavaCode;
    var currentLanguage = Current.Language;
    %>
<div class="menu-right pb-3 border-bottom mb-3">
                        <h4 class="color-333D36 font-weight-600 text-uppercase h5"><%=Language.classify %></h4>
                        <%var list_danh_muc_blog = ZoneBo.GetZoneWithLanguageByType(3, lang); %>
                        <%var list_blog_con = list_danh_muc_blog.Where(r => r.ParentId > 0); %>
                        <%foreach (var item in list_blog_con)
                            { %> 
                        <%var link_target = "javascript:void(0)"; %>
                            <div class="mb-2">
                            <a href="<%=link_target %>" title="<%=item.lang_name %>"><%=item.lang_name %></a>
                        </div>
                        <%} %>
                        
                        <%var getZone = ZoneBo.GetZoneWithLanguageByType(3, lang); %>
                        <%var getDetailByprenid = getZone.Where(x => x.ParentId == 0).OrderBy(x => x.SortOrder).Take(4).ToList();%>
                        <%--<%foreach (var item in getDetailByprenid)
                            {%>
                                <div class="mb-2">
                                    <a href="" title=""><%=item.Name %></a>
                                </div>
                                
                           <% } %>--%>
                    </div>
<div class="menu-right pb-3 border-bottom pr-lg-4 mb-3">
    <h4 class="color-333D36 font-weight-600 text-uppercase h5"><%=Language.Recent_posts %></h4>
    <%var total_newest = 0; %>
    <%var sapXepListBlog2 = NewsBo.GetNewsDetailWithLanguage(3,lang,2,0,"",1,4, ref total_newest); %>
    <%foreach (var item in sapXepListBlog2)
        {%>
     <% var link_taget1 = string.Format("/{0}/blog-chi-tiet/{1}/{2}.htm", currentLanguage, item.Alias, item.NewsId);%>
    <div class="item-blog-right py-2 border-bottom ">
        <div class="row">
            <div class="col-xl-3 col-md-4 col-3 pr-0">
                <div class="image">
                    <img src="/uploads/<%=item.Avatar %>" class="img-fluid" />
                </div>
            </div>
            <div class="col-xl-9 col-md-8 col-9">
                <h5 class="title ">
                    <a href="<%=link_taget1%>""><%=item.Title %></a>
                </h5>
                <div class="d-flex small">
                    <div class="time">
                        <%=item.CreatedDate.ToString("dd/MM/yyyy") %>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%}%>
</div>
<%--<div class="instagam pb-3 border-bottom mb-4">
    <h4 class="color-333D36 font-weight-600 text-uppercase h5 mb-3"><%=UIHelper.GetConfigByName("TieuDeImgBlog") %></h4>
    <div class="row  px-2 mb-3">
        <% var getNewDetail = getDetailBlogById.Where(x => x.MetaTitle == "Anh Blog").FirstOrDefault(); %>
        <%if (getNewDetail != null)
            { %> 
            <%string _rexge = @"/.*?\.(png|jpg|svg)";%>

        <%var listImg = Regex.Matches(getNewDetail.Body, _rexge);%>

        <%foreach (var item in listImg)
            {%>
                <%string[] splipChuoi = item.ToString().Split('/');%>
                <div class="col-4 p-2 ">
                    <a href="javascripts:void(0)">
                        <img src="/Themes/images/change/<%=splipChuoi.Last() %>" class="img-fluid w-100" /></a>
                </div>
        <%} %>
        <%} %>
                            

    </div>
    <div>
        <a href="" class="color-333D36 h5"><i class="fab fa-instagram mr-2"></i>Xem hồ sơ của chúng tôi</a>
    </div>
</div>--%>
