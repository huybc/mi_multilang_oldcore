﻿<%@ Page Title="Yoga" Language="C#" MasterPageFile="~/Themes/HempSpas.Master" AutoEventWireup="true" CodeBehind="Yoga.aspx.cs" Inherits="HempSpa.Pages.Yoga" %>

<%@ Import Namespace="HempSpa.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
     <meta property="og:title" content=" <%=obj != null? obj.MetaTitle:""%>" />
    <meta property="og:description" content="<%=obj != null?obj.MetaDescription:""%>" />
    <meta property="og:image" content="<%=domainName %>/Uploads/<%=obj != null?obj.Avatar:""%>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <section class="py-5"></section>
    <% var slider = ConfigBo.AdvGetByType(6, lang); %>

    <% var anhslihead = slider.Where(x => x.IsEnable == true).OrderBy(x => x.SortOrder).FirstOrDefault(); %>

    <section class="banner-page py-5" style="background: url(/Uploads/<%=anhslihead == null ? "":anhslihead.Thumb%>) no-repeat center center; background-size: cover; color: #fff; text-align: center;">

        <div class="container  ">
            <div class="row justify-content-center  ">
                <div class="col-xl-6 col-md-8 col-sm-10 col-12 align-self-lg-center">
                    <h1 class="title"><%=anhslihead == null? "":anhslihead.Name %></h1>
                    <%=anhslihead == null ? "":anhslihead.Content %>
                </div>
            </div>
        </div>
    </section>

    <section class="intro-yoga py-4">
        <div class="container">
            <%var totalRow = 0;%>
            <%var getZoneyoga = ZoneBo.GetZoneWithLanguageByType(12, lang).Where(x => x.Alias == "yoga").FirstOrDefault();%>
            <%if (getZoneyoga != null)
                {%>
            <%var getAllYogaDetail = NewsBo.GetNewsDetailWithLanguage(12, lang, 2, getZoneyoga.Id, "", 1, 20, ref totalRow).OrderByDescending(x => x.CreatedDate).ToList(); %>
            <%for (int i = 0; i < getAllYogaDetail.Count(); i++)
                {%>
            <%var extra = Newtonsoft.Json.JsonConvert.DeserializeObject<DonGiaEntity>(getAllYogaDetail[i].Extras); %>
            <%var link_target = string.Format("/{0}/{1}/{2}.{3}.htm", currentLanguage, getAllYogaDetail[i].Alias, getAllYogaDetail[i].Url, getAllYogaDetail[i].NewsId); %>
            <div class="row no-gutters imit <%=i % 2 == 0 ? "" : "flex-md-row-reverse"%>">
                <div class="col-lg-6 col-md-6 col-12 align-self-center">
                    <a href="<%=link_target %>">
                        <img src="/Uploads/<%=getAllYogaDetail[i].Avatar%>" class="img-fluid" alt="" />
                    </a>
                </div>
                <div class="col-lg-6 col-md-6 col-12 py-3 <%=i%2 == 0? "": "align-self-center pl-xl-5"%>">
                    <div class="px-3">
                        <a href="<%=link_target %>">
                            <h3 class="heading-yoga text-uppercase mb-3"><%=getAllYogaDetail[i].Title %></h3>
                        </a>

                        <ul class="pl-3 mb-4">
                            <%=getAllYogaDetail[i].Body %>
                        </ul>
                        <%var gia = extra.KhuyenMai != "" ? extra.KhuyenMai : extra.HocVi; %>
                        <button class="btn btn-hempspa px-5 text-uppercase showmoda" data-gia="<%=gia%>"><%=Language.Book_now %></button>
                    </div>
                </div>
            </div>
            <%}%>

            <%}%>
        </div>
    </section>

    <section class="list-yoga py-5">
        <div class="container">
            <h3 class="heading-yoga text-center"><%=UIHelper.GetConfigByName("Title Yoga") %></h3>
            <div class="row">
                <%if (getZoneyoga != null)
                    {%>
                <%var getAllYogaDetail2 = NewsBo.GetNewsDetailWithLanguage(12, lang, 2, getZoneyoga.Id, "", 1, 2, ref totalRow).OrderByDescending(x => x.CreatedDate).Skip(2).Take(3).ToList(); %>
                <%foreach (var item in getAllYogaDetail2)
                    {%>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="item-yoga">
                        <div class="image">
                            <a href="javascripts:void(0)" title="" id="imgNext">
                                <img src="/Uploads/<%=item.Avatar %>" class="img-fluid"
                                    alt="" /></a>
                        </div>
                        <div class="text p-3 p-lg-4">
                            <h4 class="title">
                                <a href="javascripts:void(0)"><%=item.Title %></a>
                            </h4>
                            <div class="des">
                                <%=item.Sapo %>
                            </div>
                        </div>
                    </div>
                </div>
                <% } %>
                <%} %>
            </div>
        </div>
    </section>

    <% var anhsllast = slider.Where(x => x.IsEnable == true).OrderByDescending(x => x.SortOrder).FirstOrDefault(); %>

    <section class="contact-home py-5" style="background: url(/Uploads/<%=anhsllast == null ? "":anhsllast.Thumb%>) no-repeat center center; background-size: cover; color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <h2 class="heading"><%=UIHelper.GetConfigByName("TitleAddress") %> </h2>
                    <ul class="list-contact mb-5">
                        <li>
                            <img src="/Themes/images/Location-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Address") %>
                        </li>
                        <li>
                            <img src="/Themes/images/call-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Phone") %>
                        </li>
                        <li>
                            <img src="/Themes/images/Letter-icon.svg" class="img-fluid mr-3 " /><%=UIHelper.GetConfigByName("Gmail") %>
                        </li>
                    </ul>
                    <div class="social">
                        <% var getconfigByName1 = ConfigBo.GetByConfigName("Fcebook"); %>
                        <div class="item">
                            <a href="<%=getconfigByName1.ConfigInitValue%>"><i class="fab fa-facebook-f mr-3"></i><%=getconfigByName1.ConfigValue %></a>
                        </div>
                        <% var getconfigByName2 = ConfigBo.GetByConfigName("Intargram"); %>
                        <div class="item">
                            <a href="<%=getconfigByName2.ConfigInitValue%>"><i class="fab fa-instagram mr-3"></i><%=getconfigByName2.ConfigValue %></a>
                        </div>
                        <% var getconfigByName3 = ConfigBo.GetByConfigName("Zalo"); %>
                        <div class="item">
                            <a href="<%=getconfigByName3.ConfigInitValue%>">
                                <img src="/Themes/images/zalo-icon.svg" class="img-fluid mr-3" /><%=getconfigByName3.ConfigValue %></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12 col-12 px-xl-5">
                    <form class="form-contact-home" id="gui" data-type="tuvan">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" id="txtHoten" placeholder="<%=Language.Full_Name %> *" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" id="txtGmail" placeholder="Gmail *">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="tel" class="form-control" id="txtPhone" placeholder="<%=Language.Hotline %> *" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea rows="5" class="form-control" id="txtTinNhan" placeholder="<%=Language.Note %> *"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 mb-0">
                                <button type="submit" class="btn btn-hempspa w-100 text-uppercase"><%=Language.Submit %></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
