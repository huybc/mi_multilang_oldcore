﻿<%@ Page Title="chi tiết lich trình" Language="C#" MasterPageFile="~/Themes/HempSpas.Master" AutoEventWireup="true" CodeBehind="tour-detail.aspx.cs" Inherits="HempSpa.Pages.tour_detail" %>

<%@ Import Namespace="HempSpa.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <script type="application/ld+json">
            {
              "@context": "http://schema.org/",
              "@type": "Product",
              "name": "<%= obj.Title %>",
              "image": "https://hanuchi.com/uploads/<%= obj.Avatar %>",
              "description": "<%= Utility.SubWordInString(obj.MetaDescription, 160) %>",
              "mpn": "925872",
              "brand": {
                "@type": "Thing",
                "name": ""
              },
             "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "4.4",
               <%-- "reviewCount": "<%= obj.ViewCount %>"--%>
              },
              "offers": {
                "@type": "Offer",
                "priceCurrency": "VND",
               <%-- "price": "<%= obj.Price %>",
                "priceValidUntil": "<%= obj.DiscountPrice %>",--%>
                "itemCondition": "http://schema.org/UsedCondition",
                "availability": "http://schema.org/InStock",
                "seller": {
                  "@type": "Organization",
                  "name": "Executive Objects"
                }
              }
            }
    </script>
    <% var GetId = int.Parse(Page.RouteData.Values["id"].ToString());%>
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>

    <% var slider = ConfigBo.AdvGetByType(12, lang); %>
    <%var totalRow = 0; %>
    <%var getNewByid = NewsBo.GetNewsDetailWithLanguageById(GetId, lang); %>
    <% var anhslihead = slider.Where(x => x.IsEnable == true).OrderBy(x => x.SortOrder).FirstOrDefault(); %>

    <section class="py-5"></section>
    <%if (getNewByid != null)
        {%>
    <%var gia_detail = Newtonsoft.Json.JsonConvert.DeserializeObject<DonGiaEntity>(getNewByid.Extras);%>

    <section class="banner-page-2 py-5 " style="background: url(/Uploads/<%=anhslihead == null ? "":anhslihead.Thumb%>) no-repeat center center; background-size: cover; color: #fff; text-align: center;">
        <div class="container ">
            <div class="position-absolute" style="bottom: 1rem;">
                <h1 class="title"><%=getNewByid.Title %></h1>
                <div class="d-flex">
                    <div class="mr-4">
                        <i class="fas fa-map-marker-alt mr-2"></i><%=gia_detail.Location %>
                    </div>
                    <div class="mr-4">
                        <i class="far fa-clock mr-2"></i><%=gia_detail.SoNgay %>
                    </div>
                    <div class="mr-4">
                        <i class="fas fa-biking mr-2"></i><%=gia_detail.Vehicle %>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container py-5">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12">
                <ul class="nav nav-pills nav-fill-hemp mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-1" role="tab"
                            aria-controls="pills-home" aria-selected="true"><%=Language.DESCRIBE %></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-2" role="tab"
                            aria-controls="pills-profile" aria-selected="false"><%=Language.Departure_schedule %></a>
                    </li>
                </ul>
                <div class="tab-content tab-content-hemp mb-5" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-1" role="tabpanel"
                        aria-labelledby="pills-home-tab">
                        <div class="  h4 mb-3"><%=Language.Detail %></div>
                        <%=getNewByid.Body %>

                        <h4 class="mb-4">Related Tours
                        </h4>
                        <div class="row">
                            <%var getZoneLichTrinh = ZoneBo.GetZoneWithLanguageByType(12, lang).Where(x => x.Alias == "tour").FirstOrDefault();%>
                            <%if (getZoneLichTrinh != null)
                                {%>
                            <% var sapXepListtour = NewsBo.GetNewsDetailWithLanguage(12, lang, 2, getZoneLichTrinh.Id, "", 1, 4, ref totalRow).OrderByDescending(x => x.CreatedDate).ToList(); %>
                            <%foreach (var item in sapXepListtour)
                                {%>
                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">

                                <% var link_item = string.Format("/{0}/{1}/{2}.{3}.htm", currentLanguage, getZoneLichTrinh.Alias, item.Url, item.NewsId); %>

                                <div class="item-tour">
                                    <div class="image position-relative">
                                        <a href="<%=link_item %>">
                                            <img src="/Uploads/<%=item.Avatar %>" class="img-fluid w-100" alt="" /></a>
                                        <div class="tag">

                                            <a href="<%=link_item %>" title=""><%=item.Title %></a>
                                        </div>
                                    </div>
                                    <%var gia_detail2 = Newtonsoft.Json.JsonConvert.DeserializeObject<DonGiaEntity>(item.Extras);%>

                                    <div class="py-4 px-3">
                                        <h4 class="title">
                                            <a href="<%=link_item %>" title=""><%=item.Sapo%></a>
                                        </h4>
                                        <div class="d-flex flex-wrap">
                                            <div class="time-tour">
                                                <%=gia_detail2.SoNgay %>
                                            </div>
                                            <div class="price-tour ml-auto">
                                                <%=gia_detail2.HocVitour %>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <%}%>
                            <%}%>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="pills-2" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <%//Lay doan bai viet %>
                        <%var parts = NewsBo.GetPartInNews(getNewByid.NewsId, lang).FirstOrDefault(); %>
                        <%if (parts != null)
                        {  %>
                        <%=parts.Content %>
                        <%} %>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12">
                <form class="form-right mb-4" id="dat-lich-chung" data-type="<%=getNewByid.Alias %>">
                    <div class="text-center color-345F1F font-weight-bold mb-4">
                        <%=Language.BOOK_NOW__ONLY_2_MINUTES %>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label><%=Language.Full_Name %> *</label>
                            <input type="text" class="form-control" id="txtName" placeholder="" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label><%=Language.Hotline %> *</label>
                            <input type="text" class="form-control" id="txtPhoneNumber" placeholder="" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Email *</label>
                            <input type="text" class="form-control" id="txtEmail" placeholder="">
                        </div>
                        <div class="form-group col-md-12">
                            <label><%=Language.checkin %> *</label>
                            <input type="text" name="" class="form-control dpk" id="txtNgayDen" placeholder="" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label><%=Language.checkout %> *</label>
                            <input type="text" name="" class="form-control dpk" id="txtNgayDat" placeholder="" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label><%=Language.Note %> *</label>
                            <textarea type="text" rows="5" class="form-control" id="txtMessage"
                                placeholder="Messange*"></textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-hempspa w-100 text-uppercase mb-3  " <%--data-dismiss="modal" data-toggle="modal"--%> <%--data-target="#modal-successfull"--%>><%=Language.Book_now %></button>
                            <div class="color-345F1F text-center">
                                <%=Language.Or_Contact_Us %> :<%=UIHelper.GetConfigByName("Phone2") %>
                            </div>
                        </div>
                    </div>
                </form>
                <% var bannerRight = slider.Where(x => x.SortOrder == 2).FirstOrDefault(); %>
                <div class="banner-menu-right " style="margin-bottom: 1.5rem; padding: 3rem; min-height: 240px; background: url(/Uploads/<%=bannerRight == null ? "":bannerRight.Thumb%>) no-repeat center center; background-size: cover; color: #fff;">
                    <h4 class="font-weight-bold mb-3">
                        <a href="javácripts:void(0)"><%=bannerRight.Name %>
                        </a>
                    </h4>
                    <div>
                        <%=bannerRight.Content %>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <%}%>


    <% var anhsllast = slider.Where(x => x.IsEnable == true).OrderByDescending(x => x.SortOrder).FirstOrDefault(); %>

    <section class="contact-home py-5" style="background: url(/Uploads/<%=anhsllast == null ? "":anhsllast.Thumb%>) no-repeat center center; background-size: cover; color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <h2 class="heading"><%=UIHelper.GetConfigByName("TitleAddress") %> </h2>
                    <ul class="list-contact mb-5">
                        <li>
                            <img src="/Themes/images/Location-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Address") %>
                        </li>
                        <li>
                            <img src="/Themes/images/call-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Phone") %>
                        </li>
                        <li>
                            <img src="/Themes/images/Letter-icon.svg" class="img-fluid mr-3 " /><%=UIHelper.GetConfigByName("Gmail") %>
                        </li>
                    </ul>
                    <div class="social">
                        <% var getconfigByName1 = ConfigBo.GetByConfigName("Fcebook"); %>
                        <div class="item">
                            <a href="<%=getconfigByName1.ConfigInitValue%>"><i class="fab fa-facebook-f mr-3"></i><%=getconfigByName1.ConfigValue %></a>
                        </div>
                        <% var getconfigByName2 = ConfigBo.GetByConfigName("Intargram"); %>
                        <div class="item">
                            <a href="<%=getconfigByName2.ConfigInitValue%>"><i class="fab fa-instagram mr-3"></i><%=getconfigByName2.ConfigValue %></a>
                        </div>
                        <% var getconfigByName3 = ConfigBo.GetByConfigName("Zalo"); %>
                        <div class="item">
                            <a href="<%=getconfigByName3.ConfigInitValue%>">
                                <img src="/Themes/images/zalo-icon.svg" class="img-fluid mr-3" /><%=getconfigByName3.ConfigValue %></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12 col-12 px-xl-5">
                    <form class="form-contact-home" id="gui" data-type="tuvan">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" id="txtHoten" placeholder="<%=Language.Full_Name %> *" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" id="txtGmail" placeholder="Gmail *">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="tel" class="form-control" id="txtPhone" placeholder="<%=Language.Hotline %> *" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea rows="5" class="form-control" id="txtTinNhan" placeholder="<%=Language.Note %>*"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 mb-0">
                                <button type="submit" class="btn btn-hempspa w-100 text-uppercase"><%=Language.Submit %></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>

