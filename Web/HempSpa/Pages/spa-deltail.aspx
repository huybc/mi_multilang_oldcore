﻿<%@ Page Title="Chi tiết spa" Language="C#" MasterPageFile="~/Themes/HempSpas.Master" AutoEventWireup="true" CodeBehind="spa-deltail.aspx.cs" Inherits="HempSpa.Pages.spa_deltail" %>

<%@ Import Namespace="HempSpa.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <script type="application/ld+json">
            {
              "@context": "http://schema.org/",
              "@type": "spa",
              "name": "<%= obj.Title %>",
              "image": "https://hanuchi.com/uploads/<%= obj.Avatar %>",
              "description": "<%= Utility.SubWordInString(obj.MetaDescription, 160) %>",
              "mpn": "925872",
              "brand": {
                "@type": "Thing",
                "name": ""
              },
             "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "4.4",
               <%-- "reviewCount": "<%= obj.ViewCount %>"--%>
              },
              "offers": {
                "@type": "Offer",
                "priceCurrency": "VND",
               <%-- "price": "<%= obj.Price %>",
                "priceValidUntil": "<%= obj.DiscountPrice %>",--%>
                "itemCondition": "http://schema.org/UsedCondition",
                "availability": "http://schema.org/InStock",
                "seller": {
                  "@type": "Organization",
                  "name": "Executive Objects"
                }
              }
            }
    </script>
    <% var SpaID = int.Parse(Page.RouteData.Values["id"].ToString());%>
    <% var zoneAlias = Page.RouteData.Values["zoneAlias"].ToString();%>
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>

    <section class="py-5"></section>
    <% var slider = ConfigBo.AdvGetByType(4, lang); %>
    <%var getNewByid = NewsBo.GetNewsDetailWithLanguageById(SpaID, lang); %>
    <%if (getNewByid != null)
        {%>
    <%var gia_detail = Newtonsoft.Json.JsonConvert.DeserializeObject<DonGiaEntity>(getNewByid.Extras);%>
     <% var anhslHiead = slider.Where(x => x.IsEnable == true).OrderBy(x => x.SortOrder).FirstOrDefault(); %>
    <section class="banner-page-2 py-5" style="background: url(/Uploads/<%=anhslHiead != null? anhslHiead.Thumb:""%>) no-repeat center center; background-size: cover; color: #fff; text-align: center;">
        <div class="container ">
            <div class="position-absolute" style="bottom: 1rem;">
                <h1 class="title"><%=getNewByid.Title%></h1>
                <p class="font-weight-bold"><%=gia_detail.HocVi %> net/1 pax</p>
            </div>
        </div>
    </section>

    <section class="container py-5">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12">
                <ul class="nav nav-pills nav-fill-hemp mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-1" role="tab" aria-controls="pills-home" aria-selected="true"><%=Language.DESCRIBE %></a>
                    </li>
                </ul>
                <div class="tab-content tab-content-hemp mb-5" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-1" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="font-weight-bold"><%=getNewByid.Title %></div>
                        <div class="body-content">
                            <%=getNewByid.Body %>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-12">
                        <h5 class="color-333D36 font-weight-bold  mb-4"><%=Language.Recent_posts %></h5>
                    </div>
                    <%var totalRow = 0; %>
                    <%var list_same_spa = NewsBo.GetNewsDetailWithLanguage(12, lang, 2, getNewByid.ZoneId, "", 1, 4, ref totalRow); %>
                    <%--% var GetNewByZoneid = NewsBo.GetNewsDetailWithLanguage(2, lang, 2, 0, "", 1, 2, ref totalRow); %>
                    <%var sapXepListSpa = GetNewByZoneid.Where(x => x.Title != "").OrderByDescending(x => x.CreatedDate).ToList(); %>--%>
                    <%foreach (var item in list_same_spa)
                        {%>
                    <% var link_item = string.Format("/{0}/{1}/{2}.{3}.htm", currentLanguage, zoneAlias, item.Url, item.NewsId); %>

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="item-spa px-md-0 px-lg-3 px-xl-4">
                            <div class="image">
                                <a href="<%=link_item %>" title="">
                                    <img src="/uploads/<%=item.AvatarTron %>" class="img-fluid"
                                        alt="<%=item.Title %>" /></a>
                            </div>
                            <div class="text px-xl-4 mb-4 ">
                                <h4 class="title">
                                    <a href="<%=link_item %>"><%=item.Title %></a>
                                </h4>
                                <div class="des">
                                    <%=item.Sapo %>
                                </div>

                            </div>
                            <a href="<%=link_item%>" class="btn btn-hempspa-outline text-uppercase px-4 px-lg-5"><%=Language.More %></a>
                        </div>
                    </div>
                    <%} %>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12">
                <form class="form-right mb-4" id="dat-lich-chung" data-type="<%=getNewByid.Alias %>">
                    <div class="text-center color-345F1F font-weight-bold mb-4">
                        <%=Language.BOOK_NOW__ONLY_2_MINUTES %>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label><%=Language.Full_Name %> *</label>
                            <input type="text" class="form-control" id="txtName" placeholder="" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label><%=Language.Hotline %> *</label>
                            <input type="text" class="form-control" id="txtPhoneNumber" placeholder="" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Email *</label>
                            <input type="text" class="form-control" id="txtEmail" placeholder="">
                        </div>
                        <div class="form-group col-md-12">
                            <label><%=Language.checkin %> *</label>
                            <input type="text" name="" class="form-control dpk" id="txtNgayDen" placeholder="" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label><%=Language.checkout %> *</label>
                            <input type="text" name="" class="form-control dpk" id="txtNgayDat" placeholder="" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label><%=Language.Note %> *</label>
                            <textarea type="text" rows="5" class="form-control" id="txtMessage"
                                placeholder="Messange*"></textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-hempspa w-100 text-uppercase mb-3  " <%--data-dismiss="modal" data-toggle="modal"--%> <%--data-target="#modal-successfull"--%>><%=Language.Book_now %></button>
                            <div class="color-345F1F text-center">
                                <%=Language.Or_Contact_Us %>:<%=UIHelper.GetConfigByName("Phone2") %>
                            </div>
                        </div>
                    </div>
                </form>
                <%var BannerDetail = slider.Where(x => x.SortOrder == 2).FirstOrDefault(); %>
                <div class="banner-menu-right" style="color: #fff; background: url(/Uploads/<%=BannerDetail == null ? "":BannerDetail.Thumb%>) no-repeat center; background-size: cover; padding: 3rem; min-height: 240px; margin-bottom: 1.5rem;">
                    <h4 class="font-weight-bold mb-3">
                        <a href="/<%=BannerDetail.Url %>"><%=BannerDetail == null? "":BannerDetail.Name %>
                        </a>
                    </h4>
                    <div>
                        <%=BannerDetail == null? "":BannerDetail.Content %>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <%}%>


    <% var anhsllast = slider.Where(x => x.IsEnable == true).OrderByDescending(x => x.SortOrder).FirstOrDefault(); %>

    <section class="contact-home py-5" style="background: url(/Uploads/<%=anhsllast == null ? "":anhsllast.Thumb%>) no-repeat center center; background-size: cover; color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <h2 class="heading"><%=UIHelper.GetConfigByName("TitleAddress") %> </h2>
                    <ul class="list-contact mb-5">
                        <li>
                            <img src="/Themes/images/Location-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Address") %>
                        </li>
                        <li>
                            <img src="/Themes/images/call-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Phone") %>
                        </li>
                        <li>
                            <img src="/Themes/images/Letter-icon.svg" class="img-fluid mr-3 " /><%=UIHelper.GetConfigByName("Gmail") %>
                        </li>
                    </ul>
                    <div class="social">
                        <% var getconfigByName1 = ConfigBo.GetByConfigName("Fcebook"); %>
                        <div class="item">
                            <a href="<%=getconfigByName1.ConfigInitValue%>"><i class="fab fa-facebook-f mr-3"></i><%=getconfigByName1.ConfigValue %></a>
                        </div>
                        <% var getconfigByName2 = ConfigBo.GetByConfigName("Intargram"); %>
                        <div class="item">
                            <a href="<%=getconfigByName2.ConfigInitValue%>"><i class="fab fa-instagram mr-3"></i><%=getconfigByName2.ConfigValue %></a>
                        </div>
                        <% var getconfigByName3 = ConfigBo.GetByConfigName("Zalo"); %>
                        <div class="item">
                            <a href="<%=getconfigByName3.ConfigInitValue%>">
                                <img src="/Themes/images/zalo-icon.svg" class="img-fluid mr-3" /><%=getconfigByName3.ConfigValue %></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12 col-12 px-xl-5">
                    <form class="form-contact-home" id="gui" data-type="tuvan">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" id="txtHoten" placeholder="<%=Language.Full_Name %> *" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" id="txtGmail" placeholder="Gmail *">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="tel" class="form-control" id="txtPhone" placeholder="<%=Language.Hotline %> *" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea rows="5" class="form-control" id="txtTinNhan" placeholder="<%=Language.Note %>*"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 mb-0">
                                <button type="submit" class="btn btn-hempspa w-100 text-uppercase"><%=Language.Submit %></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
