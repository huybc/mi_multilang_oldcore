﻿using HempSpa.Core.Helper;
using Mi.BO.Base.News;
using Mi.BO.Base.Zone;
using Mi.Entity.Base.ConFig;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Zone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HempSpa.Pages
{
    public partial class Yoga : BasePages
    {
        public ZoneEntity obj;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var lang = Current.LanguageJavaCode;
              //  var zoneName = Page.RouteData.Values["url"];
             //   if(zoneName != null)
              //  {
                    obj = ZoneBo.GetZoneByAlias("yoga",lang,12);
                    if (obj != null)
                    {
                      
                        Page.Title = obj.MetaTitle;
                        Page.MetaKeywords = obj.MetaKeyword;
                        Page.MetaDescription = obj.MetaDescription;
                    }
                    else
                    {
                        obj = new ZoneEntity();
                    }
               // }

               
            }
        }

    }
}