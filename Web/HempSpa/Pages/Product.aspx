﻿<%@ Page Title="Sản phẩm" Language="C#" MasterPageFile="~/Themes/HempSpaProduct.Master" AutoEventWireup="true" CodeBehind="Product.aspx.cs" Inherits="HempSpa.Pages.Product" %>

<%@ Import Namespace="HempSpa.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
     <meta property="og:title" content=" <%=obj != null? obj.MetaTitle:""%>" />
    <meta property="og:description" content="<%=obj != null?obj.MetaDescription:""%>" />
    <meta property="og:image" content="<%=domainName %>/Uploads/<%=obj != null?obj.Avatar:""%>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <section class="py-5"></section>
    <% var slider = ConfigBo.AdvGetByType(8, lang); %>

    <% var anhslihead = slider.Where(x => x.IsEnable == true).OrderBy(x => x.SortOrder).FirstOrDefault(); %>

    <section class="banner-page py-5" style="background: url(/Uploads/<%=anhslihead == null ? "":anhslihead.Thumb%>) no-repeat center center; background-size: cover; color: #fff; text-align: center;">

        <div class="container  ">
            <div class="row justify-content-center  ">
                <div class="col-xl-6 col-md-8 col-sm-10 col-12 align-self-lg-center">
                    <h1 class="title"><%=anhslihead == null? "":anhslihead.Name %></h1>
                    <%=anhslihead == null ? "":anhslihead.Content %>
                </div>
            </div>
        </div>
    </section>

    <section class="list-products py-4 py-lg-5">
        <div class="container">
            <%--<div class="row">
                <div class="col-12 d-flex justify-content-md-end mb-4 flex-wrap">
                    <div class="pagi-product mr-md-4 mb-2 mb-md-0">
                        <span class="color-333D36 mr-2">Hiển thị:</span>
                        <a href="" class="number-page active">9</a>
                        <a href="" class="number-page">12</a>
                        <a href="" class="number-page">18</a>
                        <a href="" class="number-page">24</a>
                    </div>
                    <div class="d-flex">
                        <a href="" class="layout-option">
                            <img src="/Themes/images/3-cols.svg" class="img-fluid mr-3" /></a>
                        <a href="" class="layout-option">
                            <img src="/Themes/images/2rows-2cols.svg" class="img-fluid mr-3" /></a>
                        <a href="" class="layout-option active">
                            <img src="/Themes/images/3rows-3cols.svg" class="img-fluid mr-3" /></a>
                        <a href="" class="layout-option">
                            <img src="/Themes/images/4rows-4cols.svg" class="img-fluid mr-3" /></a>
                    </div>
                </div>
            </div>--%>
            <div class="row" id="_sp">
                <%var totalRow = 0; %>
                <%var getZoneSanPham = ZoneBo.GetZoneWithLanguageByType(12, lang).Where(x => x.Alias == "san-pham").FirstOrDefault();%>
                <%if (getZoneSanPham != null)
                    {%>
                        <%var getAllSp = NewsBo.GetNewsDetailWithLanguage(12, lang, 2, getZoneSanPham.Id, "", 1, 8, ref totalRow); %>
                            <%foreach (var item in getAllSp)
                                {%>
                                 <%var link_tar = string.Format("/{0}/{1}/{2}.{3}.htm", currentLanguage, getZoneSanPham.Alias == null ? "" : getZoneSanPham.Alias, item.Url, item.NewsId); %>
                                 <%var ExTra = Newtonsoft.Json.JsonConvert.DeserializeObject<DonGiaEntity>(item.Extras);%>
                                 <div class="col-xl-3 col-md-4 col-sm-6 col-12 layout-product imit" >
                                    <div class="item-product">
                                         <div class="image position-relative">
                                            <a href="<%=link_tar %>"" title="">
                                            <img src="/Uploads/<%=item.Avatar %>"  style="width:100%;height:200px;" class="img-fluid" alt="" />
                                            <%=ExTra.KhuyenMai != ""? "<div class=\"tag\">sale</div>":"" %>
                                            <div class="overlay">
                                            </div>
                                           
                                            </a>
                                             <div class="group-btn">
                                                <button class="btn btn-product mr-2">
                                                    <img src="/Themes/images/heart-icon.svg" class="img-fluid " /></button>
                                                <button class="btn btn-product">
                                                    <img src="/Themes/images/cart-icon.svg" class="img-fluid" /></button>
                                            </div>
                                        </div>
                                        <div class="p-3">
                                             <div class="des">
                                                <a href="<%=link_tar %>"" title=""><%=item.Title %></a>
                                            </div>
                                            <a href="<%=link_tar %>"" title=""> <h5 class="title"><%=item.Sapo %>
                                            </h5></a>
                                           
                                            <div class="price d-flex flex-wrap">
                                                <div class="new color-345F1F font-weight-bold mr-3">
                                                    <%=ExTra.KhuyenMai != ""?ExTra.KhuyenMai:ExTra.HocVi %>
                                                </div>
                                                <div class="old">
                                                    <%=ExTra.KhuyenMai != ""?ExTra.HocVi:ExTra.KhuyenMai %>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                 </div>

                            <%}%>


                <%}%>
                <div class="col-12" id="ft_sp">
                </div>
            </div>
            <div class="col-12 mt-4 text-center text-danger" id="sp4"></div>
            <div class="text-center my-3">
                <button class="btn btn-hempspa-outline px-lg-5 text-uppercase" data-obj="<%=new DonGiaEntity() %>" id="_xTsanPham" data-lang="<%=lang %>" data-curentlang="<%=currentLanguage %>"><%=Language.VIEW_MORE %></button>
            </div>
        </div>
    </section>
    <% var anhsllast = slider.Where(x => x.IsEnable == true).OrderByDescending(x => x.SortOrder).FirstOrDefault(); %>

    <section class="contact-home py-5" style="background: url(/Uploads/<%=anhsllast == null ? "":anhsllast.Thumb%>) no-repeat center center; background-size: cover; color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <h2 class="heading"><%=UIHelper.GetConfigByName("TitleAddress") %> </h2>
                    <ul class="list-contact mb-5">
                        <li>
                            <img src="/Themes/images/Location-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Address") %>
                        </li>
                        <li>
                            <img src="/Themes/images/call-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Phone") %>
                        </li>
                        <li>
                            <img src="/Themes/images/Letter-icon.svg" class="img-fluid mr-3 " /><%=UIHelper.GetConfigByName("Gmail") %>
                        </li>
                    </ul>
                    <div class="social">
                        <% var getconfigByName1 = ConfigBo.GetByConfigName("Fcebook"); %>
                        <div class="item">
                            <a href="<%=getconfigByName1.ConfigInitValue%>"><i class="fab fa-facebook-f mr-3"></i><%=getconfigByName1.ConfigValue %></a>
                        </div>
                        <% var getconfigByName2 = ConfigBo.GetByConfigName("Intargram"); %>
                        <div class="item">
                            <a href="<%=getconfigByName2.ConfigInitValue%>"><i class="fab fa-instagram mr-3"></i><%=getconfigByName2.ConfigValue %></a>
                        </div>
                        <% var getconfigByName3 = ConfigBo.GetByConfigName("Zalo"); %>
                        <div class="item">
                            <a href="<%=getconfigByName3.ConfigInitValue%>">
                                <img src="/Themes/images/zalo-icon.svg" class="img-fluid mr-3" /><%=getconfigByName3.ConfigValue %></a>
                        </div>
                    </div>
                </div>
                 <div class="col-xl-6 col-md-6 col-sm-12 col-12 px-xl-5">
                    <form class="form-contact-home"id="gui"  data-type="tuvan">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" id="txtHoten" placeholder="<%=Language.Full_Name %>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" id="txtGmail" placeholder="Gmail *">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="tel" class="form-control" id="txtPhone" placeholder="<%=Language.Hotline %>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea rows="5" class="form-control" id="txtTinNhan" placeholder="<%=Language.Note_Detail %>"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 mb-0">
                                <button type="submit" class="btn btn-hempspa w-100 text-uppercase"><%=Language.Submit %></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
