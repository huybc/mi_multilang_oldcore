﻿var pageId = 1;
modals = {
    init: function () {
        modals.RegisterEvent();
    },
    RegisterEvent: function () {
        $('.immd').off('click').off('click').on('click', function () {
            var idAnh = $(this).data('id');
            var lang = $(this).data('lang');
            modals.loadDataMd(idAnh, lang);
        });
        $('.pills-profile-tab').off('click').on('click', function () {
            var lang = $(this).data('lang');
            var alias = $(this).data('alias');
            modals.TagImageByAlias(lang, alias);
        })
        $('.view_more_image').off('click').on('click', function () {
            var lang = $(this).data('lang');
            var alias = $(this).data('alias');
            modals.ViewMore(lang, alias);
        })
    },
    loadDataMd: function (idAnh, lang) {
        R.Post({
            params: {
                Id: idAnh,
                lang: lang,
            },
            module: "ui-action",
            ashx: 'modulerequest.ashx',
            action: "get-new-byid",

            success: function (res) {
                var regex_get_picture = new RegExp(/\/[^'"/<>]+"/g);
                var ltobjax = res.Data.Body;
                var anh = ltobjax.match(regex_get_picture);
                var htm = "";
                var htm2 = '';
                $('.ThongBao').html("");

                anh.forEach(function (element) {
                    var can = element.lastIndexOf("?");
                    var chuoidung = element.slice(0, can)
                    htm += '<div class="swiper-slide swiper-slide-visible imit">' +
                        ' <div class="image">' +
                        ' <img src="/Uploads' + chuoidung + '" class="img-fluid" style="min-height:530px;max-height:530px" alt="" />' +
                        ' </div>' +
                        ' </div>';
                    htm2 += '<div class="swiper-slide swiper-slide-visible imit">' +
                        ' <div class="image">' +
                        ' <img src="/Uploads' + chuoidung + '" class="img-fluid" style="min-height:130px;max-height:130px;"  alt="" />' +
                        ' </div>' +
                        ' </div>';

                })
                $('._binAnhById').html('').html(htm);
                $('._binAnhById1').html('').html(htm2);
                //$('.myslide').html(res.Data.Body);
                //$('.myslide>p').addClass('swiper-wrapper');
                //$('.myslide-preview>p>img').addClass('my-img-slide');
                $('._Bin1>div>div>img').removeClass('swiper-slide').addClass('swiper-slide');
                $('._Bin2>div>div>img').removeClass('slide-swipe-bt').addClass('slide-swipe-bt');
                var galleryThumbs = null;
                //mySwiper.destroy(deleteInstance, cleanStyles);
                galleryThumbs = new Swiper('.gallery-thumbs', {
                    slidesPerView: 5,
                    freeMode: true,
                    watchSlidesVisibility: true,
                    watchSlidesProgress: true,
                });
                var galleryTop = null;
                galleryTop = new Swiper('.gallery-top', {
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                    thumbs: {
                        swiper: galleryThumbs
                    }
                });
                $('#modal-slide-imge').modal("show");
                modals.RegisterEvent();
            }
        })
    },
    TagImageByAlias: function (lang, Alias) {
        $('.BinAnh').html("")
        pageId = 1;
        var pageIndex = 1;
        var pageSize = 6;
        R.Post({
            params: {
                lang: lang,
                type: 11,
                alias: Alias,
                pageIndex: pageIndex,
                pageSize: pageSize,
            },
            module: "ui-action",
            ashx: 'modulerequest.ashx',
            action: "load-san-pham",
            success: function (res) {
                var htm = "";
                $('.ThongBao').html("");

                if (res.Data.length > 0) {
                    for (var i = 0; i < res.Data.length; i++) {


                        htm += ' <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 px-2 imit">' +
                            '  <a href="javascript:void(0);">' +
                            '<img src="/Uploads/' + res.Data[i].Avatar + ' " style="width:100%;height:250px;margin:10px" class="img-fluid immd" data-id="' + res.Data[i].NewsId + '" data-lang="' + lang + '" alt="" />' +
                            '   </a>' +
                            ' </div >' +
                            ' </div >';
                    }
                    $('.BinAnh').html(htm);
                    modals.RegisterEvent();
                }
                else {
                    switch (lang) {
                        case "vi-VN":
                            htm += '<small>thư viện ảnh trống !</small>'
                            break;
                        case "ko-KR":
                            htm += '<small>  빈 사진 갤러리 !</small>'

                            break;
                        case "en-US":
                            htm += '<small> Blank photo gallery !</small>'
                            break;
                        case "zh-CN":
                            htm += '<small> 空白圖片庫 !</small>'

                            break;
                    }

                    $('.ThongBao').html(htm);
                }
            }
        })
    },
    ViewMore: function (lang, Alias) {
        pageId += 1;
        var pageSize = 6;
        R.Post({
            params: {
                lang: lang,
                type: 11,
                alias: Alias,
                pageIndex: pageId,
                pageSize: pageSize,
            },
            module: "ui-action",
            ashx: 'modulerequest.ashx',
            action: "load-san-pham",
            success: function (res) {
                $('.ThongBao').html("");

                var htm = '';
                if (res.Data.length > 0) {
                    for (var i = 0; i < res.Data.length; i++) {
                        if (res.Data[i].MetaTitle === "MT3") {
                            htm +=
                                ' <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 px-2 imit">' +
                                '  <a href="javascript:void(0);">' +
                                '<img src="/Uploads/' + res.Data[i].Avatar + ' "  style="width:100%;height:250px;margin:10px" class="img-fluid immd" data-id="' + res.Data[i].NewsId + '" data-lang="' + lang + '" alt="" />' +
                                '   </a>' +
                                ' </div >' +
                                ' </div >';
                        }
                    }
                    $('.BinAnh').append(htm);
                    modals.RegisterEvent();
                }
                else {
                    switch (lang) {
                        case "vi-VN":
                            htm += '<small>thư viện ảnh trống !</small>'
                            break;
                        case "ko-KR":
                            htm += '<small>  빈 사진 갤러리 !</small>'

                            break;
                        case "en-US":
                            htm += '<small> Blank photo gallery !</small>'


                            break;
                        case "zh-CN":
                            htm += '<small> 空白圖片庫 !</small>'

                            break;
                    }

                    $('.ThongBao').html(htm);

                }
            }
        })
    },
}
modals.init();
