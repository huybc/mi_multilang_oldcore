﻿//var baseUrl = "http://localhost:8668";
var baseUrl = "http://cms.migroup.asia/";
//var baseUrl = "http://";

$('.tu-van-ngay').off('click').on('click', function () {
    //alert(1);
    $('#exampleModalScrollable').modal('show');
    var id = parseInt($(this).data('id'));
    console.log(id);
    if (id == 5193) {
        $('#cbxService').val(id);
        $('#btnSubmit').data('type', 'website');
    }
    else if (id == 5190) {
        $('#cbxService').val(id);
        $('#btnSubmit').data('type', 'crm');
    }
});

$('#btnSubmit').off('click').on('click', function () {
    var company = $('#txtCompany').val();
    var name = $('#txtName').val();
    var email = $('#txtEmail').val();
    var phone = $('#txtPhone').val();
    var service = $('#cbxService option:selected').text();
    var type = $(this).data('type');
    if (service == null && type == "tourkit")
        service = "Tourkit";
    else if (service == null && type == "lien-he")
        service = "Liên hệ";
    console.log(service);
    var obj = {
        Name: name,
        Phone: phone,
        Email: email,
        Address: company,
        hotelName: service
    }
    R.Post({
        params: {
            company: company,
            name: name,
            email: email,
            phoneNumber: phone,
            service: service,
            type: type,
        },
        module: "ui-action",
        ashx: 'modulerequest.ashx',
        action: "save",
        success: function (res) {
            if (res.Success) {
                //alert("Thanh cong");
                $('#exampleModal').modal('show');
                $('#txtCompany').val('');
                $('#txtName').val('');
                $('#txtEmail').val('');
                $('#txtPhone').val('');
                $('#exampleModalScrollable').modal('hide');
                //Bat API sang crmtravel
                $.ajax({

                    type: 'POST',
                    url: baseUrl + "/PublicAPI/BookingInSiteHandler.ashx?fn=booking",
                    data: JSON.stringify(obj),
                    dataType: "text",
                    success: function (response) {
                        //alert(response);
                        // hàm này đặt phòng ?
                        //R.Post({
                        //    params: d,
                        //    module: "ui_action",
                        //    ashx: 'modulerequest.ashx',
                        //    action: "send-mail",
                        //    success: function (res) {
                        //        //log nay an nay`
                        //        console.log('Gui mail thanh cong');
                        //        R.Post({
                        //            params: obj,
                        //            module: "ui_action",
                        //            ashx: 'modulerequest.ashx',
                        //            action: "send-mail",
                        //            success: function (res) {

                        //            }
                        //        });
                        //    }
                        //});
                        //$('#exampleModal').modal('hide');

                        //alert(response.result);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log('error');
                        alert(xhr.status + ' ' + ajaxOptions + ' ' + thrownError);
                    }
                });
            }

        }, error: function () {
            //$('#contact').RLoadingModuleComplete();
        }
    });
});
$('#btnSubmit1').off('click').on('click', function () {
    var company = $('#txtCompany').val();
    var name = $('#txtName1').val();
    var email = $('#txtEmail1').val();
    var phone = $('#txtPhone1').val();
    var note = $('#txtNote1').val()
    var service = $('#cbxService option:selected').text();
    var type = $(this).data('type');

    if (service == null && type == "tourkit")
        service = "Tourkit";
    else if (service == null && type == "lien-he")
        service = "Liên hệ";
    console.log(service);
    var obj = {
        Name: name,
        Phone: phone,
        Email: email,
        Address: company,
        hotelName: service
    }
    R.Post({
        params: {
            company: company,
            name: name,
            email: email,
            phoneNumber: phone,
            service: service,
            type: type,
            note: note
        },
        module: "ui-action",
        ashx: 'modulerequest.ashx',
        action: "save",
        success: function (res) {
            if (res.Success) {
                //alert("Thanh cong");
                $('#exampleModal').modal('show');
                $('#txtCompany').val('');
                $('#txtName').val('');
                $('#txtEmail').val('');
                $('#txtPhone').val('');
                $('#exampleModalScrollable').modal('hide');
                //Bat API sang crmtravel
                $.ajax({

                    type: 'POST',
                    url: baseUrl + "/PublicAPI/BookingInSiteHandler.ashx?fn=booking",
                    data: JSON.stringify(obj),
                    dataType: "text",
                    success: function (response) {
                        //alert(response);
                        // hàm này đặt phòng ?
                        //R.Post({
                        //    params: d,
                        //    module: "ui_action",
                        //    ashx: 'modulerequest.ashx',
                        //    action: "send-mail",
                        //    success: function (res) {
                        //        //log nay an nay`
                        //        console.log('Gui mail thanh cong');
                        //        R.Post({
                        //            params: obj,
                        //            module: "ui_action",
                        //            ashx: 'modulerequest.ashx',
                        //            action: "send-mail",
                        //            success: function (res) {

                        //            }
                        //        });
                        //    }
                        //});
                        //$('#exampleModal').modal('hide');

                        //alert(response.result);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log('error');
                        alert(xhr.status + ' ' + ajaxOptions + ' ' + thrownError);
                    }
                });
            }

        }, error: function () {
            //$('#contact').RLoadingModuleComplete();
        }
    });
});
$('#gui').off('submit').on('submit', function () {
    var name = $('#txtHoten').val();
    var email = $('#txtGmail').val();
    var phone = $('#txtPhone').val();
    var tinnhan = $('#txtTinNhan').val();
    var type = $(this).data('type');
    R.Post({
        params: {
            Name: name,
            Phone: phone,
            Gmail: email,
            TinNhan: tinnhan,
            type: type,
        },
        module: "ui-action",
        ashx: 'modulerequest.ashx',
        action: "save-thong-tin",
        success: function (res) {
            if (res.Success)
                alert(res.Message)
            $('#txtHoten').val("");
            $('#txtGmail').val("");
            $('#txtPhone').val("");
            $('#txtTinNhan').val("");
        }
    });
    return false;

});
$(document).ready(function () {
    $('.dpk').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minDate: new Date(),
        //minYear: 1901,
        //maxYear: parseInt(moment().format('YYYY'), 10),
        locale: {
            "format": "DD/MM/YYYY"
        }
        //}, function (start, end, label) {
        //    var years = moment().diff(start, 'years');
        //    alert("You are " + years + " years old!");
    });
});
$('#dat-lich-chung').off('submit').on('submit', function () {
    var name = $('#txtName').val();
    var phone = $('#txtPhoneNumber').val();
    var email = $('#txtEmail').val();
    var ngay_den = moment($('#txtNgayDen').val(), "DD/MM/YYYY").format('YYYY-MM-DD');
    var ngay_dat = moment($('#txtNgayDat').val(), "DD/MM/YYYY").format('YYYY-MM-DD');
    var mess = $('#txtMessage').val();
    var datatype = $(this).data('type');
    R.Post({
        params: {
            Name: name,
            Phone: phone,
            Gmail: email,
            TinNhan: mess,
            ngaySinh: ngay_den,
            ngayKham: ngay_dat,
            type: datatype,
        },
        module: "ui-action",
        ashx: 'modulerequest.ashx',
        action: "save-thong-tin",
        success: function (res) {
            if (res.Success)
                $('#modal-successfull').modal('show');
            $('#txtName').val("");
            $('#txtPhoneNumber').val("");
            $('#txtEmail').val("");
            $('#txtMessage').val("");
        }
    });
    return false;
})
$('.book-room').off('click').on('click', function () {
    var room_price = $(this).data('price');
    if (sessionStorage.getItem("Ngay") != null) {
        var ngay = sessionStorage.getItem("Ngay");
        var splitNgay = ngay.split('-');
        var x1 = splitNgay[0];
        var x2 = splitNgay[1];
        $('.dt1').val(moment(x1, "DD/MM/YYYY").format("MM/DD/YYYY"));
        var getdate = moment(x1, "DD/MM/YYYY").format("DD/MM/YYYY HH:mm:ss");
        var dt = new Date(getdate);
        var getmlccden = dt.getTime();
        $('.dt2').val(moment(x2, "DD/MM/YYYY").format("MM/DD/YYYY"));
        var getdate2 = moment(x2, "DD/MM/YYYY").format("DD/MM/YYYY HH:mm:ss");
        var dt2 = new Date(getdate2);
        var getmlccden2 = dt2.getTime();
        if ((getmlccden2 - getmlccden) > 5184000) {
            var tinhNgay = getmlccden2 - getmlccden;
            var cvdate = tinhNgay / 60 / 60 / 1000 / 24;
            var parseGia = "";
            for (var i = 0; i < room_price.trim().length; i++) {
                if (room_price[i] != '.' && room_price[i] != ',') {
                    parseGia += room_price[i];
                }
            }
            var tTien = (parseInt(parseGia) * cvdate).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            var timKiTu = tTien.indexOf('.');
            var ThanhTien = tTien.substr(0, timKiTu);

            $('#modal-book-rom').find('.tong-cong').text(ThanhTien);
        }

    }
    else {
        $('#modal-book-rom').find('.tong-cong').text(room_price);
        var datatype = $(this).data('nguoi');
        $('#modal-book-rom').find('#txtNguoiL').val(datatype);
    }
    if (sessionStorage.getItem("NguoiL") != null) {
        var NL = sessionStorage.getItem("NguoiL");
        $('.txtNgLon').val(NL)
    }
    if (sessionStorage.getItem("TreE") != null) {
        var TE = sessionStorage.getItem("TreE");
        $('.txtTreEm').val(TE)
    }
    $('#modal-book-rom').modal('show');
    $('#gia').val(room_price);
    //tong-cong
});
$('#boook').off('submit').on('submit', function () {
    var checkIn = $('#modal-book-rom').find('.dt1').val();
    var CheckOut = $('#modal-book-rom').find('.dt2').val();
    var ngLon = $('#modal-book-rom').find('.txtNgLon').val();
    var treEm = $('#modal-book-rom').find('.txtTreEm').val();
    var Name = $('#modal-book-rom').find('#txtname').val();
    var Phone = $('#modal-book-rom').find('#txtphone').val();
    var Gmail = $('#modal-book-rom').find('#txtemail').val();
    var datatype = $(this).data('type');
    var ngay_den = moment(checkIn, "DD/MM/YYYY").format('YYYY-MM-DD');
    var ngay_di = moment(CheckOut, "DD/MM/YYYY").format('YYYY-MM-DD');
    R.Post({
        params: {
            Name: Name,
            Phone: Phone,
            Gmail: Gmail,
            nguoilon: ngLon,
            treem: treEm,
            ngaySinh: ngay_den,
            ngayKham: ngay_di,
            type: datatype,
        },
        module: "ui-action",
        ashx: 'modulerequest.ashx',
        action: "save-thong-tin",
        success: function (res) {
            if (res.Success)
            $('#modal-book-rom').find('.txtTreEm').val("");
            $('#modal-book-rom').find('#txtname').val("");
            $('#modal-book-rom').find('#txtphone').val("");
            $('#modal-book-rom').find('#txtemail').val("");
            $('#modal-book-rom').modal('hide');
            $('#modal-successfull').modal('show');
        }
    });
    return false;
});
$('.showmoda').off('click').on('click', function () {
    var room_price = parseInt($(this).data('gia').replace(".", "").replace(",", ""));
    $('#modal-book-rom').modal('show');
    $('#modal-book-rom').find('.tong-cong').text(room_price.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    //tong-cong
});
function myFunction(obj) {
    var Date1 = $('.dt1').val();
    var getdate = moment(Date1, "DD/MM/YYYY").format("MM/DD/YYYY HH:mm:ss");
    var dt = new Date(getdate);
    var getmlccden = dt.getTime();
    var getdate2 = moment(obj, "DD/MM/YYYY").format("MM/DD/YYYY HH:mm:ss");
    var dt2 = new Date(getdate2);
    var getmlccden2 = dt2.getTime(); 
    if ((getmlccden2 - getmlccden) > 5184000) {
        var tinhNgay = getmlccden2 - getmlccden;
        var cvdate = tinhNgay / 60 / 60 / 1000 / 24;

        var room_price = $('#gia').val();
        var parseGia = "";
        for (var i = 0; i < room_price.trim().length; i++) {
            if (room_price[i] != '.' && room_price[i] != ',') {
                parseGia += room_price[i];
            }
        }

        var tTien = (parseInt(parseGia) * cvdate).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var timKiTu = tTien.indexOf('.');
        var ThanhTien = tTien.substr(0, timKiTu);
           
        $('#modal-book-rom').find('.tong-cong').text(ThanhTien);
    }
}
$('#BookPhong').on('click', function () {
    var DT = $('#txtdateDF').val();
    var NL = $('#txtNlDf').val();
    var TE = $('#txtTeDf').val();
    if (typeof (Storage) !== 'undefined') {
        sessionStorage.setItem('Ngay', DT);
        sessionStorage.setItem('NguoiL', NL);
        sessionStorage.setItem('TreE', TE);
    }
   
})