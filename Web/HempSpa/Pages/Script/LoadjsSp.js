﻿var pageIndex = 1;
var SpDetail = {
    init: function () {
        SpDetail.registerEvent();
        SpDetail.LoadthemSp();
        SpDetail.ViewSp();
        SpDetail.MuaSp();
    },
    registerEvent: function () {
       
    },
    LoadthemSp: function () {
        $('#_xTsanPham').on('click', function () {
            pageIndex += 1;
            var pagedetai = $(this).data('page');
            var pageSize = 8;
            if (pagedetai == 1) {
                pageSize = 4;
            }
            var lang = $(this).data('lang');
            var currentlang = $(this).data('curentlang');
            R.Post({
                params: {
                    lang: lang,
                    pageIndex: pageIndex,
                    pageSize: pageSize,
                    alias: "san-pham",
                    type: 12,
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "load-san-pham",
                success: function (res) {
                    var htm = '';
                    if (res.Data > 0) {
                        var linktrong = "javascripts:void(0)"
                        for (var i = 0; i < res.Data.length; i++) {

                            var extra = JSON.parse(res.Data[i].Extras);
                            var hf = "/" + currentlang + "/" + res.Data[i].Alias + "/" + res.Data[i].Url + "/" + res.Data[i].NewsId + ".htm";
                            var sale = extra.KhuyenMai != "" ? "<div class=\"tag\">sale</div>" : ""
                            var GiaGoc = extra.KhuyenMai != "" ? extra.KhuyenMai : extra.HocVi;
                            var GiaKhyenMai = extra.KhuyenMai != "" ? extra.HocVi : extra.KhuyenMai;
                            var link = pagedetai == 1 ? linktrong : hf;
                            htm +=
                                '<div class="col-xl-3 col-md-4 col-sm-6 col-12 layout-product" >' +
                                '<div class="item-product">' +
                                '<div class="image position-relative">' +
                                '<a  href="' + link + '" title="" class="topcontro" data-lang=' + lang + ' data-id="' + res.Data[i].NewsId + '">' +
                                '<img src="/Uploads/' + res.Data[i].Avatar + '" style="width:100%;height:200px; class="img-fluid" alt="" />' +
                                '' + sale + '' +
                                ' <div class="overlay">' +
                                '</div > ' +
                                '</a> ' +
                                '<div class="group-btn">' +
                                '<button class="btn btn-product mr-2">' +
                                '<img src="/Themes/images/heart-icon.svg" class="img-fluid " /></button>' +
                                '<button class="btn btn-product">' +
                                '<img src="/Themes/images/cart-icon.svg" class="img-fluid" /></button>' +
                                '</div>' +
                                '</div>' +
                                '<div class="p-3">' +
                                '<div class="des">' +
                                '<a href="' + link + '" title="" class="topcontro" data-lang=' + lang + 'data-id="' + res.Data[i].NewsId + '">' + res.Data[i].Title + '</a>' +
                                '</div>' +
                                '<a href="' + link + '"><h5 class="title topcontro" data-lang=' + lang + 'data-id="' + res.Data[i].NewsId + '">' + res.Data[i].Sapo + '' +
                                '</h5></a>' +
                                '<div class="price d-flex flex-wrap">' +
                                '<div class="new color-345F1F font-weight-bold mr-3">' + GiaGoc + '</div>' +
                                '<div class="old">' + GiaKhyenMai + '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div >' +
                                '</div >';
                        }
                        $('#_sp').append(htm)

                    }

                    if (htm === '') {
                        switch (lang) {
                            case "vi-VN":
                                htm += '<small>Hiện chưa có sản phẩm mới !</small>'
                                break;
                            case "ko-KR":
                                htm += '<small> 현재 새로운 제품이 없습니다 !</small>'
                                break;
                            case "en-US":
                                htm += '<small>There are currently no new products !</small>'

                                break;
                            case "zh-CN":
                                htm += '<small> 當前沒有新產品 !</small>'
                                break;
                        }

                        $('#sp4').html(htm);
                    }
                }
            })
        });
    },
    ViewSp: function () {
        $('.topcontro').on('click', function () {
            $('body,html').animate({
                scrollTop: 0
            }, 1000);
            var lang = $(this).data('lang');
            var Id = $(this).data('id');
            $('#addAnh1').html('');
            $('#addAnh2').html('');
            $('#detailSp').html('');
            R.Post({
                params: {
                    language_code: lang,
                    newsId: Id,
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "get-part-new-byid",

                success: function (res) {
                    var htm1 = '';
                    var htm2 = '';
                    if (res.Data.length > 0) {
                        var regex_get_picture = new RegExp(/\/[^'"/<>]+"/g);
                        var ltobjax = res.Data[0].Content;
                        var anh = ltobjax.match(regex_get_picture);
                        anh.forEach(function (element) {
                            var can = element.lastIndexOf("?");
                            var chuoidung = element.slice(0, can)

                            htm1 += '<div class="swiper-slide" style = "background-image: url(/Uploads' + chuoidung + ')" >' +
                                '</div>';

                            htm2 += '<div class="swiper-slide" style = "background-image: url(/Uploads' + chuoidung + ')" >' +
                                '</div > ';

                        })

                        $('#addAnh1').html(htm1);
                        $('#addAnh2').html(htm2);
                    }
                }
            });
            R.Post({
                params: {
                    lang: lang,
                    Id: Id,
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "get-new-byid",

                success: function (res) {
                    var htm3 = '';
                    var htm4 = '';
                    var htm5 = '';
                    var extra = JSON.parse(res.Data.Extras);
                    var GiaGoc = extra.KhuyenMai != "" ? extra.KhuyenMai : extra.HocVi;
                    var linktrong = "javascripts:void(0)"
                    htm3 += '<div class="cate mb-2">' +
                        '<a href = "' + linktrong + '">' + res.Data.Title + '' +
                        '</a > ' +
                        '</div >' +
                        '<h1 class="title mb-3">' +
                        '<a href="' + linktrong + '">' + res.Data.Sapo + '</a > ' +
                        '</h1>' +
                        '<div class="price mb-2">' +
                        '' + GiaGoc + '' +
                        '</div >';
                    htm4 += '' + res.Data.Body + ' '
                    htm5 += '<button class="btn btn-hempspa px-5 text-uppercase" id="mua" data-gia="' + GiaGoc + '" data-name="' + res.Data.Title + '">buy</button>'
                    $('#detailSp').html(htm3)
                    $('#MotaSp').html(htm4)
                    $('#btmua').html(htm5)
                }
            });
        })
    },
    MuaSp: function () {
        $('#mua').off('click').on('click', function () {
            $('#modal_MuaSp').modal('show');
            var gia = $(this).data('gia')
            var name = $(this).data('name')
            $('.gia').val(gia)
            var sl = $('.SoLuong').val();
            var regex = new RegExp(/\W/g);
            var laydaucham = gia.match(regex);
            var xoacham = gia.replace(laydaucham, '');
            $('#Thanhtien').text((parseInt(sl) * parseInt(xoacham)).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
            $('#TenSP').val(name)

        });
    }

}
SpDetail.init();
function checkInput(obj) {
    if (parseInt(obj.value)) {
        var sl = parseInt(obj.value);
        var gia = $('.gia').val();
        var regex = new RegExp(/\W/g);
        var laydaucham = gia.match(regex);
        var xoacham = gia.replace(laydaucham, '');
        $('#Thanhtien').text((sl * parseInt(xoacham)).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
    }
    else {
        $('#Thanhtien').text("")
    }
}
$('#boookSp').off('submit').on('submit', function () {
    var name = $('#TenSP').val();
    var email = $('#Gmail').val();
    var phone = $('#Sdt').val();
    var SoLuong = $('.SoLuong').val();
    var type = $(this).data('type');
    R.Post({
        params: {
            Name: name,
            Phone: phone,
            Gmail: email,
            coSo: SoLuong,
            type: type,
        },
        module: "ui-action",
        ashx: 'modulerequest.ashx',
        action: "save-thong-tin",
        success: function (res) {
            if (res.Success)
                alert(res.Message)
            $('#modal_MuaSp').modal('hide');
            $('#TenSP').val("");
            $('#Gmail').val("");
            $('#Sdt').val("");
            $('#SoLuon').val("");
            $('#Thanhtien').text("");
        }
    });
    return false;
});
