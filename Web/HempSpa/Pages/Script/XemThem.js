﻿
$(".view-more-projet").off('click').on('click', function () {
    var type = $(this).data('type');
    var lang = $(this).data('lang');
    var zone = $(this).data('zone');
    var page = $(this).data('page');
    var size = $(this).data('size');
    var search = "";
    var currentType = $(this).data('txttype');
    console.log(type, lang, zone, page, size, search, currentType);
    R.Post({
        params: {
            type: type,
            lang: lang,
            hot: 2,
            zone_id: zone,
            search: search,
            pageIndex: page,
            pageSize: size
        },
        module: "ui-action",
        ashx: 'modulerequest.ashx',
        action: "load-trang",
        success: function (res) {
            if (res.Success) {
                var result = res.Data;
                console.log(result);
                var htm = "";

                result.forEach(function (e) {
                    //var extras = JSON.parse(e.Extras);
                    var link = "/" + lang.split('-')[0] + "/" + currentType + "/" + e.Url + "." + e.NewsId + ".htm";
                    /*
                     Tu tin viet kieu moi xem nao
                     */
                    $('.item-load-trang').last().clone().insertAfter('.item-load-trang:last');
                    var el = $('.item-load-trang').last();
                    el.find('.card-img-top').attr("src", '/uploads/' + e.Avatar + '');
                    el.find('.card-img-top').attr("alt", e.Title);
                    //el.find('.avatar').attr("alt", e.Title);
                    el.find('.load-title').text(e.Title);
                    el.find('.load-title').attr('href', link);
                    //el.find('.lich-kham').text(extras.LichKham);
                    //el.find('.xem-them').attr("href", link);
                    el.find('.card-text').html(e.Sapo);

                });
                //$('.list-ck').append(htm);
                //Xu ly hau ky
                //if (result.length > 0) {
                $('.view-more-projet').data('page', page + 1);
                //var index = $('#xem-them-btn').data('index');
                //$('.page-now').html(index);
                //}

            }

        }, error: function () {
            //$('#contact').RLoadingModuleComplete();
        }
    });
})

$("#view-more-blog").off('click').on('click', function () {

    var type = $(this).data('type');
    var lang = $(this).data('lang');
    var zone = $(this).data('zone');
    var page = $(this).data('page');
    var size = $(this).data('size');
    var search = "";
    var currentType = $(this).data('txttype');
    console.log(type, lang, zone, page, size, search, currentType);
    R.Post({
        params: {
            type: type,
            lang: lang,
            hot: 2,
            zone_id: zone,
            search: search,
            pageIndex: page,
            pageSize: size
        },
        module: "ui-action",
        ashx: 'modulerequest.ashx',
        action: "load-trang",
        success: function (res) {
            if (res.Success) {
                var result = res.Data;
                console.log(result);
                var htm = "";

                result.forEach(function (e) {
                    //var extras = JSON.parse(e.Extras);
                    var link = "/" + lang.split('-')[0] + "/" + currentType + "/" + e.Url + "-" + e.NewsId + ".htm";
                    /*
                     Tu tin viet kieu moi xem nao
                     */
                    $('.item-load-trang').last().clone().insertAfter('.item-load-trang:last');
                    var el = $('.item-load-trang').last();
                    el.find('.img-fluid').attr("src", '/uploads/' + e.Avatar + '');
                    el.find('.img-fluid').attr("alt", e.Title);
                    //el.find('.avatar').attr("alt", e.Title);
                    //class="link-load-trang"
                    el.find('.link-load-trang').attr('href', link);
                    el.find('.link-load-trang-1').attr('href', link);
                    el.find('.link-load-trang-1').text(e.Title);
                    //el.find('.load-title').text(e.Title);
                    //el.find('.load-title').attr('href', link);
                    //el.find('.lich-kham').text(extras.LichKham);
                    //el.find('.xem-them').attr("href", link);
                    el.find('.des').html(e.Sapo);

                });
                //$('.list-ck').append(htm);
                //Xu ly hau ky
                //if (result.length > 0) {
                $('#view-more-blog').data('page', page + 1);
                $('.danh-so-trang').text(page);
                //var index = $('#xem-them-btn').data('index');
                //$('.page-now').html(index);
                //}

            }

        }, error: function () {
            //$('#contact').RLoadingModuleComplete();
        }
    });
})
var page = 1;
$("#view_tour").off('click').on('click', function () {
    page += 1;
    var lang = $(this).data('lang');
    var search = "";
    var currentType = $(this).data('curentlang');
    R.Post({
        params: {
            type: 12,
            lang: lang,
            alias: "tour",
            pageIndex: page,
            pageSize: 6
        },
        module: "ui-action",
        ashx: 'modulerequest.ashx',
        action: "load-san-pham",
        success: function (res) {
            htm = '';
            if (res.Data != null) {
                for (var i = 0; i < res.Data.length; i++) {
                    var extra = JSON.parse(res.Data[i].Extras);
                    var GiaTour = extra.KhuyenMai != "" ? extra.KhuyenMai : extra.HocVitour;
                    var hf = "/" + currentType + "/" + res.Data[i].Alias + "/" + res.Data[i].Url + "/" + res.Data[i].NewsId + ".htm"
                    htm += '<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">' +
                        ' <div class="item-tour">' +
                        ' <div class="image position-relative">' +
                        ' <a href="' + hf + '">' +
                        ' <img src="/Themes/images/change/' + res.Data[i].Avatar + '" class="img-fluid w-100" alt="" /></a>' +
                        ' <div class="tag">' +
                        '<a href="' + hf + '" title="">' + res.Data[i].Title + '</a>' +
                        '</div>' +
                        '</div>' +
                        '<div class="py-4 px-3">' +
                        '<h4 class="title">' +
                        '<a href="' + hf + '" title="">' + res.Data[i].Sapo + '</a>' +
                        '</h4>' +
                        '<div class="d-flex flex-wrap">' +
                        '<div class="time-tour">' +
                        '' + extra.SoNgay + '' +
                        '</div>' +
                        '<div class="price-tour ml-auto">' +
                        '' + GiaTour + '' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                }
                $('#List_tour').append(htm)
            }

            if (htm === '') {
                switch (lang) {
                    case "vi-VN":
                        htm += '<small>Hiện chưa có tour mới !</small>'
                        break;
                    case "ko-KR":
                        htm += '<small>현재 새로운 투어가 없습니다!</small>'
                        break;
                    case "en-US":
                        htm += '<small>There are currently no new tours !</small>'
                        break;
                    case "zh-CN":
                        htm += '<small>目前沒有新團 !</small>'
                        break;
                }

                $('#sp4').html(htm);
            }
        }
    })
})
$('#_viewspa').off('click').on('click', function () {
    page += 1;
    var lang = $(this).data('lang');
    var search = "";
    var currentType = $(this).data('currentlang');
    R.Post({
        params: {
            type: 12,
            lang: lang,
            alias: "spa",
            pageIndex: page,
            pageSize: 6
        },
        module: "ui-action",
        ashx: 'modulerequest.ashx',
        action: "load-san-pham",
        success: function (res) {
            htm = '';
            if (res.Data != null) {
                for (var i = 0; i < res.Data.length; i++) {
                    var extra = JSON.parse(res.Data[i].Extras);
                    var GiaSp = extra.KhuyenMai != "" ? extra.KhuyenMai : extra.HocVi;
                    var hf = "/" + currentType + "/" + res.Data[i].Alias + "/" + res.Data[i].Url + "." + res.Data[i].NewsId + ".htm"
                    htm += '<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">' +
                        '<div class="row no-gutters item-service-home" >' +
                        '<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">' +
                        '<div class="image">' +
                        '<a href="' + hf + '">' +
                        '<img src="/uploads/' + res.Data[i].Avatar + '" class="img-fluid w-100 " alt="' + res.Data[i].Title + '" />' +
                        '</a>' +
                        '<div class="row no-gutters group-btn">' +
                        '<div class="col-5">' +
                        '<div class="price">' +
                        '<small>From</small> <b>' + GiaSp + '</b>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-7">' +
                        '<div class="book-now border-left  ">' +
                        '<a href="javascript:void(0)">BOOK NOW</a>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12  ">' +
                        '<div class="box-text">' +
                        '<h3 class="title">' +
                        '<a href="' + hf + '" title="<%=item.Title %>">' + res.Data[i].Title + '</a>' +
                        '</h3>' +
                        '<div>' +
                        '' + res.Data[i].Sapo + '' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div >' +
                        '</div >';
                }
                $('#addSpa').append(htm)
            }
            if (htm === '') {
                switch (lang) {
                    case "vi-VN":
                        htm += '<small>Hiện chưa có tour mới !</small>'
                        break;
                    case "ko-KR":
                        htm += '<small>현재 새로운 투어가 없습니다 !</small>'
                        break;
                    case "en-US":
                        htm += '<small>There are currently no new tours !</small>'
                        break;
                    case "zh-CN":
                        htm += '<small>目前沒有新團 !</small>'
                        break;
                }

                $('#sp4').html(htm);
            }
        }
    })
})
$('#_viewDichVu').off('click').on('click', function () {
    page += 1;
    var lang = $(this).data('lang');
    var search = "";
    var currentType = $(this).data('currentlang');
    R.Post({
        params: {
            type: 12,
            lang: lang,
            pageIndex: page,
            pageSize: 6
        },
        module: "ui-action",
        ashx: 'modulerequest.ashx',
        action: "load-san-pham",
        success: function (res) {
            htm = '';
            if (res.Data != null) {
                for (var i = 0; i < res.Data.length; i++) {
                    var extra = JSON.parse(res.Data[i].Extras);
                    var GiaSp = extra.KhuyenMai != "" ? extra.KhuyenMai : extra.HocVi;
                    var GiaPhong = extra.KhuyenMaiPhong != "" ? extra.KhuyenMaiPhong : extra.HocViPhong;
                    var GiaTour = extra.KhuyenMai != "" ? extra.KhuyenMai : extra.HocVitour;
                    var hf = "/" + currentType + "/" + res.Data[i].Alias + "/" + res.Data[i].Url + "." + res.Data[i].NewsId + ".htm";
                    var Gia ="";
                    if (GiaSp != null) {
                        Gia += GiaSp;
                    }
                    if (GiaPhong != null) {
                        Gia += GiaPhong;
                    }
                    if (GiaTour != null && extra.HocVitour != "" ) {
                        Gia += GiaTour;
                    }
                    htm += '<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">' +
                        '<div class="row no-gutters item-service-home" >' +
                        '<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">' +
                        '<div class="image">' +
                        '<a href="' + hf + '">' +
                        '<img src="/uploads/' + res.Data[i].Avatar + '" class="img-fluid w-100 " alt="' + res.Data[i].Title + '" />' +
                        '</a>' +
                        '<div class="row no-gutters group-btn">' +
                        '<div class="col-5">' +
                        '<div class="price">' +
                        '<small>From</small> <b>' + Gia + '</b>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-7">' +
                        '<div class="book-now border-left  ">' +
                        '<a href="' + hf+'">BOOK NOW</a>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12  ">' +
                        '<div class="box-text">' +
                        '<h3 class="title">' +
                        '<a href="' + hf + '" title="<%=item.Title %>">' + res.Data[i].Title + '</a>' +
                        '</h3>' +
                        '<div>' +
                        '' + res.Data[i].Sapo + '' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div >' +
                        '</div >';
                }
                $('#addSpa').append(htm)
            }
            if (htm === '') {
                switch (lang) {
                    case "vi-VN":
                        htm += '<small>Hiện chưa có tour mới !</small>'
                        break;
                    case "ko-KR":
                        htm += '<small>현재 새로운 투어가 없습니다 !</small>'
                        break;
                    case "en-US":
                        htm += '<small>There are currently no new tours !</small>'
                        break;
                    case "zh-CN":
                        htm += '<small>目前沒有新團 !</small>'
                        break;
                }

                $('#sp4').html(htm);
            }
        }
    })
})
$('.co').off('click').on('click', function () {
    sessionStorage.clear();
    var ngonngu = $(this).data('lang');
    switch (ngonngu) {
        case "vi":
            sessionStorage.setItem("vi", "vi");
            break;
        case "en":
            sessionStorage.setItem("en", "en");
            break;
        case "zh":
            sessionStorage.setItem("zh", "zh");
            break;
        case "ko":
            sessionStorage.setItem("ko", "ko");
            break;
    }
})
$(document).ready(function () {
    var htm = '';
    if (typeof (Storage !== 'undefined')) {
        var vi = sessionStorage.getItem("vi");
        var en = sessionStorage.getItem("en");
        var ko = sessionStorage.getItem("ko");
        var zh = sessionStorage.getItem("zh");
        if (vi !== null) {
            htm += '<img src="/Themes/images/vietnamGod.svg" class="img-fluid" />'
        }
        else if (en !== null) {
            htm += '<img src="/Themes/images/eng.jpg" class="img-fluid" />'
        }
        else if (ko !== null) {
            htm += '<img src="/Themes/images/koreaChuan.svg" class="img-fluid" />'
        }
        else if (zh !== null) {
            htm += '<img src="/Themes/images/chinachuan.svg" class="img-fluid" />'
        }
    }
    $('.itemAnhCo').html(htm);
})

