﻿using HempSpa.Core.Helper;
using Mi.BO.Base.News;
using Mi.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HempSpa.Pages
{
    public partial class spa_deltail : BasePages
    {
        public NewsDetailWithLanguageEntity obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            var getIdSanpham = int.Parse(Page.RouteData.Values["id"].ToString());
            var lang = Current.LanguageJavaCode;
            obj = NewsBo.GetNewsDetailWithLanguageById(getIdSanpham, lang);
        }
    }
}