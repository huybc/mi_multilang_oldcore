﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mi_Company.Core.Helper;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.Zone;

namespace Mi_Company.CMS.Pages
{
    public partial class ZoneEdit : System.Web.UI.Page
    {
        public ZoneEntity obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            var zoneType = Page.RouteData.Values["zoneType"];
            var id = Page.RouteData.Values["Id"];
            if (id != null)
            {
                obj = ZoneBo.GetZoneById(id.ToInt32Return0());

            }
            else
            {
                obj = new ZoneEntity();
            }
            var zones = ZoneBo.GetAllZoneWithTreeView(false, zoneType.ToInt32Return0()).ToList().Where(it => it.Status == 1 && it.Id != id.ToInt32Return0());
            ZoneRpt.DataSource = zones;
            ZoneRpt.DataBind();
        }
    }
}