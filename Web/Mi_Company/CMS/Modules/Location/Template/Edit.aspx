﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Mi_Company.CMS.Modules.Location.Template.Edit" %>

<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Location" %>
<%@ Import Namespace="Mi.Common.ChannelConfig" %>
<%@ Import Namespace="Mi.Common" %>

        <%var location = new LocationEntity();
            var id = GetQueryString.GetPost("id",0);
            if(id > 0)
            {
                location = ZoneBo.GetLocationById(id);
            }
            
                %>
        
<div class="panel-flat" id="zone-edit" data-locaId="<%=location.Id %>" style="display: inline-block; width: 780px">
    <%--<h1>Location Edit</h1>--%>
    <div class="panel-title" style="height: 46px; padding: 10px 0 4px 0; margin-right: 20px">
        <div class="col-md-12 text-right">
            <button type="button" id="locaSaveBtn" class="btn ibtn-xs btn-primary " ><i class="icon-floppy-disk position-left"></i>Lưu</button>
        </div>
    </div>
    <div class="panel-form-zone panel-body" style="padding-top: 0; margin-bottom: 40px; margin-right: 20px">
        <div class="form-group">
            <label>Tên:</label>
            <input type="text" class="form-control" id="locaNameTxt" placeholder="Tên địa danh" value="<%= location.Name %>">
        </div>
        <div class="form-group">
            <label>Hiển thị trang chủ: </label>
            <input type="checkbox" value="1" <%=location.isShowInHomePage == 1 ? "checked" : "" %> class="custom-control-input" id="locaHomeCbx" >
        </div>
        <div class="form-group">
            <label>Thứ tự: </label>
            <input type="number" class="form-control" id="locaSortNum" value="<%=location.Sort %>">
        </div>
        <div class="form-group">
            <label>Địa danh cha: </label>
            <%
                var listLoca = ZoneBo.GetAllLocation();

                %>
            <select class="form-control" id="locaParentSl" >
                <option value="">--Chọn địa danh--</option>
                <%foreach (var item in listLoca)
                    { %>
                    <option value="<%=item.Id %>" <%=item.Id == location.Parent ? "selected" : "" %>><%=item.Name %></option>
                <%} %>
            </select>
        </div>
        <div class="form-group">
            <label>Tour Type: </label>
            <select class="form-control" id="locaType" >
                <option value="trong-nuoc" <%=location.Type == "trong-nuoc" ? "selected" : "" %>>Trong nuoc</option>
                <option value="nuoc-ngoai" <%=location.Type == "nuoc-ngoai" ? "selected" : "" %>>Nuoc ngoai</option>
                
            </select>
        </div>
        <%--<div class="form-group">
            <label>Code:</label>
            <input type="text" class="form-control" id="locaCodeTxt" placeholder="Tên địa danh" value="<%= location.Code %>">
        </div>--%>
        <div class="form-group" id="attack-files">
            <div class="row">
                <div class="col-lg-3" id="attack-thumb">
                    <div>
                        <label>Ảnh đại diện</label>
                    </div>
                    <div class="avatar">
                        <input accept="image/*" id="fileSingleUploadThumb" type="file" name="files[]" style="display: none" />
                        <label for="fileSingleUploadThumb">
                            <%if (location.Id > 0)
                                {%>
                            <img data-img="<%=location.Avatar_No1%>" src="/<%=!string.IsNullOrEmpty(location.Avatar_No1)? CmsChannelConfiguration.GetAppSetting("StorageUrl")+"/"+location.Avatar_No1:"CMS/Themes/Images/no-thumbnai.png" %>" />
                            <%}
                                else
                                {%>
                            <img src="/CMS/Themes/Images/no-thumbnai.png" width="60" />
                            <% } %>
                        </label>
                    </div>
                </div>
                <div class="col-lg-6" id="attack-icon">
                    <div>
                        <label>Icon</label>
                    </div>
                    <div class="avatar">
                        <input accept="image/*" id="fileSingleUploadIcon" type="file" name="files[]" style="display: none" />
                        <label for="fileSingleUploadIcon">
                            <%if (location.Id > 0)
                                {%>
                            <img style="width: 100%" data-img="<%=location.Avatar_No2%>" src="/<%=!string.IsNullOrEmpty(location.Avatar_No2)? CmsChannelConfiguration.GetAppSetting("StorageUrl")+"/"+location.Avatar_No2:"CMS/Themes/Images/no-thumbnai.png" %>" />
                            <%}
                                else
                                {%>
                            <img src="/CMS/Themes/Images/no-thumbnai.png" width="60" />
                            <% } %>
                        </label>
                    </div>
                </div>
                <div class="col-lg-6" id="class-icon" style="display: none">
                    <div>
                        <label>Icon</label>
                    </div>
                    <div>
                        <p><a target="_blank" href="https://fontawesome.com/">https://fontawesome.com/</a></p>
                        <input type="text" class="form-control" id="clasIconTxt" placeholder="address-book" value="<%= location.Avatar_No2%>">
                    </div>
                </div>
            </div>

        </div>

        <%--<div class="form-group">
            <label>Thứ tự:</label>
            <input type="text" class="form-control" id="locaSortTxt" placeholder="Thứ tự" value="<%= obj.SortOrder %>">
        </div>--%>
        <div class="form-group">
            <label>Mô tả:</label>
            <textarea rows="2" cols="5" class="form-control" id="locaDescriptionTxt" placeholder="Enter your message here"><%= location.Description%></textarea>
        </div>
        <%--<div class="form-group">
            <label>Meta title<span class="text-danger">*</span></label>
            <input type="text" name="basic" id="metaTitletxt" class="form-control" value="<%=obj.MetaTitle %>" tabindex="1" required="required" placeholder="Khai báo từ khóa vs công cụ tìm kiếm">
        </div>
        <div class="form-group">
            <label>Từ khóa:</label>
            <input class="form-control" id="metaKeywordZoneTxt" placeholder="Metakeyword, từ khóa đểo seo danh mục" value="<%=obj.MetaKeyword %>" />
        </div>
        <div class="form-group">
            <label>Mô tả trang:</label>
            <textarea rows="2" cols="5" class="form-control" id="metaDescriptionZoneTxt" placeholder="MetaDescription.. mô tả trang."><%=obj.MetaDescription %></textarea>
        </div>--%>


    </div>
</div>
