﻿R.News = {
    Init: function () {
        this.ItemSelected = [];
        this.oId = 0;
        this.PageIndex = 1;
        this.PageSize = 30;
        this.NewsStatus = 3;
        this.startDate = null;
        this.endDate = null;
        this.minDate = moment("01/01/2014").format('DD/MM/YYYY');
        this.maxDate = moment("12/31/2050").format('DD/MM/YYYY');
        R.News.RegisterEvents();
        R.News.NewsSearch();
        $('#zone-filer-ddl').fselect({
            dropDownWidth: 0,
            autoResize: false
        });
        $('#language-type-ddl').fselect({
            dropDownWidth: 0,
            autoResize: false
        });
    },
    RegisterEvents: function () {
        var delay = null;
        $('#cms-sidebar .navigation li.p-item-hot').off('click').on('click', function () {
            if ($(this).hasClass('active')) {
                return;
            }
            //R.Zone.ZoneType = parseInt($(this).attr('data-type'));
            $('#cms-sidebar .navigation li').removeClass('active');
            $(this).addClass('active');
            if (delay != null) clearTimeout(delay);
            delay = setTimeout(function () {
                R.Special.HotMain();
            }, 200);

        });
        $('#date-filter').off('apply.daterangepicker').on('apply.daterangepicker', function (ev, picker) {
            R.News.startDate = picker.startDate.format('YYYY-MM-DD');
            R.News.endDate = picker.endDate.format('YYYY-MM-DD');
            R.News.NewsSearch();
        });
        $('#titletxt').bind("keypress keyup", function (event) {
            $('#Urltxt').val(R.UnicodeToSeo($('#titletxt').val()));
        });


        $('#cms-sidebar .navigation li.n-item').off('click').on('click', function () {
            $('#sub-pnl').hide().html('');;
            if ($(this).hasClass('active')) {
                return;
            }
            $('#main-pnl').show('slow');

            $('#cms-sidebar .navigation li').removeClass('active');
            $(this).addClass('active');
            R.News.startDate = R.News.minDate;
            R.News.endDate = R.News.maxDate;
            R.News.PageIndex = 1;
            $('#date-filter span').html("Tất cả: " + R.News.minDate + ' - ' + R.News.maxDate);
            R.News.NewsStatus = $(this).attr('status');
            if (delay != null) clearTimeout(delay);
            delay = setTimeout(function () {
                R.News.NewsSearch();
            }, 200);

        });

        $('button._search').off('click').on('click', function (e) {
            R.News.NewsSearch();
        });
        $('#btn-new-reload').off('click').on('click', function (e) {
            R.News.startDate = R.News.minDate;
            R.News.endDate = R.News.maxDate;
            R.News.PageIndex = 1;
            $('#_search-data').val('');
            $('#date-filter span').html("Tất cả: " + R.News.minDate + ' - ' + R.News.maxDate);
            if (delay != null) clearTimeout(delay);
            delay = setTimeout(function () {
                R.News.NewsSearch();
            }, 200);
        });
        $('#zone-filer-ddl').off('change').on('change', function (e) {
            R.News.NewsSearch();
        }); $('#language-type-ddl').off('change').on('change', function (e) {
            R.News.NewsSearch();
        });
        $('#news-preview .btn-action button._reject').off('click').on('click', function (e) {
            if (CKEDITOR.instances['replyReject']) {
                CKEDITOR.instances['replyReject'].destroy();
            }
            CKEDITOR.replace('replyReject', { toolbar: 'basic' });
            if (CKEDITOR.instances['replyReject'].getData().length === 0) {
                $('#reject-content').show("slow");
                $('#lbMessage em').show();
                $('#news-preview .container-news').animate({
                    scrollTop: $('#body').height()
                }, 1000);
                return;
            }
            R.Post({
                params: {
                    id: R.News.ItemSelected.join(','),
                    content: CKEDITOR.instances['replyReject'].getData()
                },
                module: "news",
                ashx: 'modulerequest.ashx',
                action: "reject",
                success: function (res) {
                    if (res.Success) {
                        $.notify("Đã từ chối bài viết !", {
                            autoHideDelay: 3000, className: "success",
                            globalPosition: 'right top'
                        });
                        $('#news-preview').modal('hide');
                        $('#news-preview').on('hidden.bs.modal', function (e) {
                            R.News.NewsSearch();

                        })
                    } else {
                        $.notify(res.Message, {
                            autoHideDelay: 3000, className: "error",
                            globalPosition: 'right top'
                        });

                    }
                }
            });

        });
        $('#module-content .btn-action button._edit').off('click').on('click', function (e) {
            var id = $('#module-content .datatable-scroll table tbody tr td input[type=checkbox]:checked').attr('id');
            R.News.Edit(id);
        });
        $('#module-content .btn-action button._view').off('click').on('click', function (e) {
            $('#news-preview').RLoading();
            var id = $('#module-content .datatable-scroll table tbody tr td input[type=checkbox]:checked').attr('id');
            $('#news-preview').modal('show').off('shown.bs.modal').on('shown.bs.modal', function () {
                R.Post({
                    params: { id: id },
                    module: "news",
                    ashx: 'modulerequest.ashx',
                    action: "preview",
                    success: function (res) {
                        if (res.Success) {
                            $('#news-preview-content').html(res.Content).find('#body .detail img').css({ 'max-width': '80%' }).parent().css({
                                'text-align': 'center', 'width': '100%', 'display': 'inline-block'
                            });
                            $('#news-preview-content p img,#news-preview-content p iframe').parent().css('text-align', 'center');
                            $('#news-preview .modal-content').css('overflow-y', 'hidden');
                            $('[data-popup="tooltip"]').tooltip();
                            // check menu action in view
                            if (R.News.NewsStatus === '3' || R.News.NewsStatus === 3) {
                                $('#news-preview .btn-action ._unpublish').show("slow");
                            } else {
                                $('#news-preview .btn-action ._unpublish').hide("slow");
                            }
                            // bài lưu tạm
                            if (R.News.NewsStatus === '2' || R.News.NewsStatus === 2) {
                                $('#news-preview .btn-action ._publish').show("slow");
                                // từ chối
                                $('#news-preview .btn-action ._reject').show("slow");
                            } else {
                                // từ chối
                                $('#news-preview .btn-action ._reject').hide("slow");
                                $('#news-preview .btn-action ._publish').hide("slow");
                            }
                            // yêu cầu duyệt
                            if (R.News.NewsStatus === '1' || R.News.NewsStatus === 1) {
                                $('#news-preview .btn-action ._request_publish').show("slow");
                            } else {
                                $('#news-preview .btn-action ._request_publish').hide("slow");
                            }
                            if (R.News.NewsStatus === '6' || R.News.NewsStatus === 6) {
                                $('#news-preview .btn-action ._request_publish').hide("slow");
                            } else {
                                $('#news-preview .btn-action ._request_publish').show("slow");
                            }

                            R.ScrollAutoSize('#news-preview .container-news', function () {
                                return $(window).height() - 10;
                            }, function () {
                                return 'auto';
                            }, {});

                            var $zoneItem = $('#news-preview').find('._zone');
                            if ($zoneItem.length > 0 && $zoneItem != 'undefined') {
                                var strHtml = '';
                                var arrayId = $zoneItem.attr('data-id').split(',');
                                for (var k = 0; k < arrayId.length; k++) {
                                    $(RAllZones).each(function (i, item) {
                                        if (arrayId[k] == item.Id) {
                                            if (strHtml.length == 0) {
                                                strHtml = item.Name;
                                            } else {
                                                strHtml += ';' + item.Name;
                                            }
                                        }
                                        return;
                                    });

                                }
                                $zoneItem.text(strHtml);
                            }
                            R.Post({
                                params: { id: id },
                                module: "news",
                                ashx: 'modulerequest.ashx',
                                action: "reject_log",
                                success: function (res) {
                                    var str = '';
                                    if (res.Success) {
                                        if (parseInt(res.TotalRow) > 0) {
                                            $.each($.parseJSON(res.Data), function (i, item) {
                                                str += '<li><p><b>' + item.CreatedBy + '</b> - <time>' + item.Date + '</time></p><div id="log_content">' + item.Content + '</div></li>';
                                            });
                                            $('#reject-text').html('<div id="_log"><ul>' + str + '</ul></div>').show();

                                        }
                                    }
                                }
                            });

                            R.News.RegisterEvents();
                            $('#news-preview').RLoadingComplete();

                        } else {
                            alert(res.Message);
                        }
                    }
                });
            });
        });
        $('#module-content .btn-action button._delete').off('click').on('click', function (e) {
            $.confirm({
                title: 'Xác nhận',
                content: 'Xóa bài viết ?',
                buttons: {
                    confirm: {
                        text: 'Xóa',
                        action: function () {
                            R.News.RemoveToTrash(R.News.ItemSelected.join(','));
                        }
                    },
                    cancel: {
                        text: 'Hủy',
                        action: function () {

                        }
                    },
                }
            });


        });
        $('#module-content .btn-action button._unpublish').off('click').on('click', function (e) {
            $.confirm({
                title: 'Xác nhận',
                content: 'Hạ bài viết ?',
                buttons: {
                    confirm: {
                        text: 'Hạ bài',
                        action: function () {
                            R.News.UnPublish(R.News.ItemSelected.join(','));
                        }
                    },
                    cancel: {
                        text: 'Hủy',
                        action: function () {

                        }
                    },
                }
            });
        });

        $('#module-content .btn-action button._request_publish').off('click').on('click', function (e) {
            $.confirm({
                title: 'Xác nhận',
                content: 'Gửi yêu cầu duyệt bài ?',
                buttons: {
                    confirm: {
                        text: 'Xác nhận',
                        action: function () {

                            R.Post({
                                params: { id: R.News.ItemSelected.join(',') },
                                module: "news",
                                ashx: 'modulerequest.ashx',
                                action: "rq_publish",
                                success: function (res) {
                                    if (res.Success) {
                                        $.notify("Đã gửi yêu cầu !", {
                                            autoHideDelay: 3000, className: "success",
                                            globalPosition: 'right top'
                                        });
                                        R.News.NewsSearch();
                                    } else {
                                        $.notify(res.Message, {
                                            autoHideDelay: 3000, className: "error",
                                            globalPosition: 'right top'
                                        });

                                    }
                                }

                            });
                        }
                    },
                    cancel: {
                        text: 'Hủy',
                        action: function () {

                        }
                    },
                }
            });


        });
        $('#module-content .btn-action button._publish, #news-preview-content .btn-action button._publish').off('click').on('click', function (e) {
            $.confirm({
                title: 'Xác nhận',
                content: '<input type="text" class="form-control" id="datepublish" placeholder="Ngày xuất bản">',
                onOpen: function () {
                    $('#datepublish').attr('data', moment().format('YYYY/MM/DD h:mm A'));
                    $('#datepublish').daterangepicker({
                        "singleDatePicker": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "timePickerSeconds": true,
                        //"autoApply": true,
                        "showCustomRangeLabel": false,
                        "alwaysShowCalendars": true,
                        "startDate": moment(),
                        "endDate": R.News.maxDate,
                        "opens": "left",
                        locale: {
                            format: 'DD/MM/YYYY h:mm A'
                        }
                    }, function (start, end, label) {
                        $('#datepublish').attr('data', start.format('YYYY/MM/DD h:mm A'));
                    });
                },
                buttons: {
                    confirm: {
                        text: 'Xuất bản',
                        action: function () {
                            R.News.Publish(R.News.ItemSelected.join(','), $('#datepublish').attr('data'));
                        }
                    },
                    cancel: {
                        text: 'Hủy',
                        action: function () {

                        }
                    },
                }
            });


        });
        $('#news-preview').off('hidden.bs.modal').on('hidden.bs.modal', function () {
            $('#news-preview-content').html('');
            R.News.oId = 0;
        });
        $('#news-save-temp').off('click').on('click', function (e) {

            R.News.Save(1);
        });
        $('#news-save-request').off('click').on('click', function (e) {

            R.News.Save(3);
        });
        $('#data-list .datatable-scroll tr td input[type=checkbox]').off('click').on('click', function () {
            var id = $(this).attr('id');
            if ($(this).is(':checked')) {
                $(this).closest('tr').addClass('row-selected');
                R.News.ItemSelected.push(id);
            } else {

                var index = R.News.ItemSelected.indexOf(id);
                if (index > -1) {
                    R.News.ItemSelected.splice(index, 1);
                }
                $(this).closest('tr').removeClass('row-selected');
            }


            if (R.News.ItemSelected.length === 0) {
                $('#news-edit .btn-action').slideUp();
            }
            if (R.News.ItemSelected.length === 1) {
                $('#news-edit .btn-action').slideDown("slow");
                $('#news-edit .btn-action ._view').show("slow");
                $('#news-edit .btn-action ._edit').show("slow");
            }

            if (R.News.ItemSelected.length > 1) {
                $('#news-edit .btn-action ._view').hide("slow");
                $('#news-edit .btn-action ._edit').hide("slow");

            }
            // bài đã duyệt
            if (R.News.NewsStatus === '3' || R.News.NewsStatus === 3) {
                $('#news-edit .btn-action ._unpublish').show("slow");
                $('#news-edit .btn-action ._edit').hide();

            } else {
                $('#news-edit .btn-action ._unpublish').hide("slow");
            }
            // bài lưu tạm
            if (R.News.NewsStatus === '2' || R.News.NewsStatus === 2) {
                $('#news-edit .btn-action ._publish').show("slow");
            } else {
                $('#news-edit .btn-action ._publish').hide("slow");
            }
            //
            if (R.News.NewsStatus === '4' || R.News.NewsStatus === 4) {
                $('#news-edit .btn-action ._delete').hide("slow");
            } else {
                $('#news-edit .btn-action ._delete').show("slow");
            }
            if (R.News.NewsStatus === '1' || R.News.NewsStatus === 1) {
                $('#news-edit .btn-action ._request_publish').show("slow");
            } else {
                $('#news-edit .btn-action ._request_publish').hide("slow");
            }

            if (R.News.NewsStatus === '6' || R.News.NewsStatus === 6) {
                $('#news-edit .btn-action ._request_publish').hide("slow");
            } else {

                $('#news-edit .btn-action ._request_publish').show("slow");
            }

            $('#rows').text(R.News.ItemSelected.length);
        });
        $('#btn-new-post').off('click').on('click', function () {
            R.News.Edit(0);
        });
        $('#data-list .datatable-scroll tr td .icon-comment').off('click').on('click', function () {
            var id = $(this).closest('tr').find('input[type=checkbox]').attr('id');
            var $this = $(this);
            $this.addClass('icon-spinner3 spinner');
            var status = $(this).attr('data');
            $.confirm({
                title: 'Xác nhận',
                content: status === 'False' ? 'Mở bình luận ?' : 'Khóa bình luận',
                buttons: {
                    confirm: {
                        text: 'Xác nhận',
                        action: function () {
                            R.Post({
                                params: { id: id, status: status === 'False' ? 1 : 0 },
                                module: "news",
                                ashx: 'modulerequest.ashx',
                                action: "comment",
                                success: function (res) {
                                    if (res.Success) {
                                        $this.removeClass('icon-spinner3 spinner');
                                        if (status === 'False') {
                                            $this.removeClass('disable').addClass('allow');
                                        } else {
                                            $this.removeClass('allow').addClass('disable');
                                        }

                                        $.notify("Cập nhật thành công !", {
                                            autoHideDelay: 3000, className: "success",
                                            globalPosition: 'right top'
                                        });
                                    } else {
                                        $.notify(res.Message, {
                                            autoHideDelay: 3000, className: "error",
                                            globalPosition: 'right top'
                                        });

                                    }
                                }

                            });
                        }
                    },
                    cancel: {
                        text: 'Hủy',
                        action: function () {
                            $this.removeClass('icon-spinner3 spinner');
                        }
                    },
                }
            });


        });
        //$('#data-list .datatable-scroll tr td .icon-info22').off('click').on('click', function () {
        //    $(this).popover('show').mouseout(function () {
        //        $(this).popover('hide');
        //    });
        //});

        // news phẩm liên quan
        $('#tab-news li').off('click').on('click', function () {
            if ($(this).hasClass('block-tab')) return;
            $('#tab-news li').removeClass('active');
            var index = $(this).addClass('active').index();
            $('#nwrapper > div').hide();
            $('#nwrapper > div').eq(index).show({
                duration: 10,
                complete: function () {

                },
                done: function () {
                    //   if ($('#special.row').length > 0) return;
                    R.Special.EmbedMain();

                }


            });

        });
    },

    AttackFile: function (el) {
        if (typeof (el) != "undefined") {
            $.each(el, function (i, v) {

                $('#attack-thumb img').attr('src', '/' + StorageUrl + '/' + v + '').attr('data', v);
            });
        }
    },
    Save: function (status) {


        var title = $('#titletxt').val();
        //var sapo = $('#Sapo').val();
        var author = $('#authortxt').val();
        var source = $('#sourcetxt').val();
        var videocb = $('#videocb').is(":checked");
        var hotcb = $('#hotcb').is(":checked");
        var commentcb = $('#commentcb').is(":checked");
        var zoneddl = $('#zoneddl').val();
        var url = $('#Urltxt').val();
        var titleSeo = $('#titleSeotxt').val();
        var metakeyword = $('#metakeywordtxt').val();
        var metadescription = $('#metadescriptiontxt').val();
        // var date = $('#datetxt').val();

        var content = CKEDITOR.instances['detailCkeditor'].getData();
        var Sapo = CKEDITOR.instances['Sapo'].getData();
        var tagIdList = '';
        $('#Tagstext').parents('.tags').find('li.FTagItem span.FTagValue').each(function (i) {
            if (tagIdList.length > 0) {
                tagIdList += ';' + $(this).text();
            } else {
                tagIdList = $(this).text();
            }

        });
        $('#IMSFullOverlayWrapper').RModuleBlock();

        var data = {
            id: R.News.oId,
            title: title,
            sapo: Sapo,
            author: author,
            source: source,
            video: videocb,
            hot: hotcb,
            comment: commentcb,
            zone: zoneddl,
            //date: date,
            content: content,
            url: url,
            status: status,
            tagIdList: tagIdList,
            metakeyword: metakeyword,
            titleseo: titleSeo,
            metadescription: metadescription,
            language_code: $('#languageDdl').val(),
            avatar: $('#attack-news-thumb img').attr('data-img')
            //hours_of_work: $('#hoursOfWorkTxt').val(),
            //construction_date: $('#constructionDateTxt').attr('data'),
            //surface_area: $('#surfaceAreaTxt').val(),
            //budge: $('#budgetTxt').val(),
        }



        R.Post({
            params: data,
            module: "news",
            ashx: 'modulerequest.ashx',
            action: "save",
            success: function (res) {

                if (res.Success) {

                    R.News.oId = parseInt(res.Data);

                    $.notify("Lưu thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.News.NewsStatus = status;
                    $('#cms-sidebar .navigation li').removeClass('active');
                    $('#cms-sidebar .navigation li[status=' + status + ']').addClass('active');

                    setTimeout(function () {
                        R.News.NewsSearch();
                    }, 200)

                    //if (R.News.oId > 0) {
                    //    $('#tab-news li').removeClass('block-tab');
                    //}
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                $('#IMSFullOverlayWrapper').RModuleUnBlock();
            }, error: function () {
                $('#IMSFullOverlayWrapper').RModuleUnBlock();
            }
        });
    },
    RemoveToTrash: function (ids) {
        R.Post({
            params: { id: ids },
            module: "news",
            ashx: 'modulerequest.ashx',
            action: "remove",
            success: function (res) {
                if (res.Success) {
                    $.notify("Xóa thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.News.NewsSearch();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }

        });
    },
    Publish: function (ids, pub) {
        R.Post({
            params: { id: ids, publishedDate: pub },
            module: "news",
            ashx: 'modulerequest.ashx',
            action: "publish",
            success: function (res) {
                if (res.Success) {
                    $.notify("Xuất bản thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.News.NewsSearch();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }

        });
    },
    UnPublish: function (ids) {
        R.Post({
            params: { id: ids },
            module: "news",
            ashx: 'modulerequest.ashx',
            action: "unpublish",
            success: function (res) {
                if (res.Success) {
                    $.notify("Bài viết đã bị gỡ !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.News.NewsSearch();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }

        });
    },
    NewsSearch: function () {
        $('#main-pnl .btn-action').hide();
        R.News.ItemSelected = [];
        var el = '#data-list';
        $(el).RLoading();
        var data = {
            keyword: $('#_search-data').val(),
            zone: $('#zone-filer-ddl').val(),
            language_code: $('#language-type-ddl').val(),
            pageindex: R.News.PageIndex,
            pagesize: R.News.PageSize,
            status: R.News.NewsStatus,
            from: R.News.startDate,
            to: R.News.endDate
        }
        R.Post({
            params: data,
            module: "news",
            ashx: 'modulerequest.ashx',
            action: "search",
            success: function (res) {
                if (res.Success) {
                    $(el).find('.datatable-scroll._list').html(res.Content);

                    R.CMSMapZone('#data-list');

                    R.ScrollAutoSize('#main-pnl .datatable-scroll', function () {
                        return $(window).height() - 92;
                    }, function () {
                        return 'auto';
                    }, {}, {}, {}, true);


                    //pager
                    if ($('#data-list .datatable-scroll._list table').attr('page-info') != 'undefined') {
                        var pageInfo = $('#data-list .datatable-scroll._list table').attr('page-info');
                        if (typeof (pageInfo) != 'undefined') {
                            pageInfo = pageInfo.split('#');
                            var page = pageInfo[0];
                            var extant = pageInfo[1];
                            var totals = pageInfo[2];
                            if (parseInt(totals) < 0) {
                                $('#data-pager').hide();
                                return;
                            } else {
                                $('#data-pager').show();
                            }
                            var rowFrom = '';
                            if (R.News.PageIndex === 1) {
                                rowFrom = 'Từ 1 đến ' + extant;
                            } else {
                                rowFrom = 'Từ ' + (parseInt(R.News.PageIndex) * parseInt(R.News.PageSize) - parseInt(R.News.PageSize)) + ' đến ' + extant;
                            }

                            $('#rowInTotals').text(rowFrom + '/' + totals);

                        }


                        setTimeout(function () {

                            $('#news-edit .ipagination').jqPagination({
                                current_page: R.News.PageIndex,
                                max_page: page,
                                paged: function (page) {
                                    R.News.PageIndex = page;
                                    R.News.NewsSearch();
                                }
                            });

                        }, 200)
                    }
                    //end pager

                    $('#data-list .datatable-scroll tr td .icon-comment').each(function (i, v) {
                        if ($(v).attr('data') === 'False') {
                            $(v).addClass('disable');
                        } else {
                            $(v).addClass('allow');
                        }
                    });
                    $('[data-popup="tooltip"]').tooltip();

                    if (R.News.NewsStatus === '6' || R.News.NewsStatus === 6) {
                        var delay = null;
                        $('#data-list .datatable-scroll tr td .icon-info22').off('click').on('click', function () {
                            var $this = $(this);
                            var oid = $this.attr('o-id');
                            if (delay != null) clearTimeout(delay);
                            delay = setTimeout(function () {
                                R.Post({
                                    params: { id: oid },
                                    module: "news",
                                    ashx: 'modulerequest.ashx',
                                    action: "reject_log",
                                    success: function (res) {
                                        var str = '';
                                        if (res.Success) {
                                            if (parseInt(res.TotalRow) > 0) {
                                                $.each($.parseJSON(res.Data), function (i, item) {
                                                    str += '<li><p><b>' + item.CreatedBy + '</b> - <time>' + item.Date + '</time></p><div id="log_content">' + item.Content + '</div></li>';
                                                });

                                                $this.popover({
                                                    // title: oid,
                                                    html: 'true',
                                                    content: '<div id="_log"><ul>' + str + '</ul></div>',
                                                    placement: 'left',
                                                    trigger: 'focus'
                                                });
                                                $this.popover('show');

                                                $this.popover('show').mouseout(function () {
                                                    $(this).popover('hide');
                                                });
                                            }


                                        }
                                    }
                                });

                            }, 200);

                        });
                    }

                    R.News.RegisterEvents();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });
                }

                $(el).RLoadingComplete();
            }
        });
    },
    Edit2: function (id) {
        $('#news-form').modal('show').off('shown.bs.modal').on('shown.bs.modal', function () {
            //$('#news-form .modal-content').css('overflow-y', 'hidden');
            var el = '#news-form';
            $(el).RLoading();
            R.Post({
                params: { id: id },
                module: "news",
                ashx: 'modulerequest.ashx',
                action: "news_init_template",
                success: function (res) {
                    if (res.Success) {
                        R.News.oId = id;
                        $('#news-form .modal-body').html(res.Content);
                        CKEDITOR.replace('detailCkeditor', { toolbar: 'CosmeticV1' });
                        // title

                        $('#titletxt,#Sapo').textcounter({
                            type: "character",
                            max: 255,
                            stopInputAtMaximum: true,
                            countSpaces: true,
                            countDownText: "w: %d"
                        });
                        $('#Tagstext').FTag({
                            tagType: $('#Tagstext').attr('data-type')
                        });
                        $('#tab-news-post').tab();
                        $('#tab-news-post a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                            R.ScrollAutoSize('#news-edit-content .tab-pane.active > .row.scroll', function () {
                                return $(window).height() - 92;
                            }, function () {
                                return 'auto';
                            }, {});

                            if (R.News.oId === 0) {
                                $('#tab-news-post li:eq(0) a').tab('show');
                            }

                        });
                        R.ScrollAutoSize('#news-form .modal-content', function () {
                            return $(window).height();
                        }, function () {
                            return 'auto';
                        }, {});

                        $('#zoneddl').fselect({
                            dropDownWidth: 0,
                            autoResize: true
                        });
                        $('#zone-news-ddl').fselect({
                            dropDownWidth: 0,
                            autoResize: false
                        });
                        if (R.News.oId > 0) {
                            $('#zoneddl').val($('#zoneddl').attr('data'));
                            $('#zoneddl').trigger('fselect:updated');
                        }
                        $('#datetxt').daterangepicker({
                            "singleDatePicker": true,
                            "timePicker": true,
                            "timePicker24Hour": true,
                            "timePickerSeconds": true,
                            //"autoApply": true,
                            "showCustomRangeLabel": false,
                            "alwaysShowCalendars": true,
                            "startDate": moment(),
                            "endDate": R.News.maxDate,
                            "opens": "left",
                            locale: {
                                format: 'DD/MM/YYYY h:mm A'
                            }
                        }, function (start, end, label) {
                            $('#datetxt').attr('data', start.format('YYYY-MM-DD h:mm A'));
                        });
                        $('#add-media').off('click').on('click', function (e) {
                            R.FileManager.Open(R.News.AttackFileCkeditor);
                        });

                        R.News.FixSideBar();
                        // Collapse on click
                        $('.panel [data-action=collapse]').click(function (e) {
                            e.preventDefault();
                            var $panelCollapse = $(this).parent().parent().parent().parent().nextAll();
                            $(this).parents('.panel').toggleClass('panel-collapsed');
                            $(this).toggleClass('rotate-180');
                            containerHeight();
                            $panelCollapse.slideToggle(150);
                            setTimeout(function () {
                                R.News.FixSideBar();
                                R.ScrollAutoSize('#news-form .modal-content', function () {
                                    return $(window).height();
                                }, function () {
                                    return 'auto';
                                }, {});
                            }, 300);
                        });


                        $(el).RLoadingComplete();
                        R.News.RegisterEvents();

                    } else {
                        $.notify(res.Message, {
                            autoHideDelay: 3000,
                            className: "error",
                            globalPosition: 'right top'
                        });
                    }

                }
            });
        });
    },
    Edit: function (id) {
        //    $('#news-form').modal('show').off('shown.bs.modal').on('shown.bs.modal', function () {
        //$('#news-form .modal-content').css('overflow-y', 'hidden');
        var el = '#news-edit';
        $(el).RModuleBlock();
        R.Post({
            params: { id: id },
            module: "news",
            ashx: 'modulerequest.ashx',
            action: "news_init_template",
            success: function (res) {
                if (res.Success) {

                    R.News.oId = id;
                    $(el).RModuleUnBlock();
                    R.ShowOverlayFull({
                        content: res.Content,
                        onOpen: function () {

                        },
                        onClose: function () {
                            R.News.oId = 0;
                        }
                    });

                    $('#Urltxt').val(R.UnicodeToSeo($('#titletxt').val()));

                    // title
                    $('#titletxt,#Sapo').textcounter({
                        type: "character",
                        max: 255,
                        stopInputAtMaximum: true,
                        countSpaces: true,
                        countDownText: "w: %d"
                    });

                    $('#Tagstext').FTag({
                        tagType: $('#Tagstext').attr('data-type')
                    });


                    $('#zoneddl').fselect({
                        dropDownWidth: 0,
                        autoResize: true
                    });
                    $('#languageDdl').fselect({
                        dropDownWidth: 0,
                        autoResize: true
                    });

                    $('#zone-news-ddl').fselect({
                        dropDownWidth: 0,
                        autoResize: false
                    });
                    if (R.News.oId > 0) {
                        $('#tab-news li').removeClass('block-tab');
                        $('#zoneddl').val($('#zoneddl').attr('data'));
                        $('#languageDdl').val($('#languageDdl').attr('data'));
                        $('#zoneddl,#languageDdl').trigger('fselect:updated');
                    }
                    $('#datetxt').daterangepicker({
                        "singleDatePicker": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "timePickerSeconds": true,
                        //"autoApply": true,
                        "showCustomRangeLabel": false,
                        "alwaysShowCalendars": true,
                        "startDate": moment(),
                        "endDate": R.News.maxDate,
                        "opens": "left",
                        locale: {
                            format: 'DD/MM/YYYY h:mm A'
                        }
                    }, function (start, end, label) {
                        $('#datetxt').attr('data', start.format('YYYY-MM-DD h:mm A'));
                    });
                    $('#datetxt').attr('data', moment().format('YYYY-MM-DD h:mm A'));
                    $('#attack-news-thumb .avatar2').off('click').on('click', function () {
                        var $this = $(this);
                        R.Image.SingleUpload($this,
                            function () {
                                //process
                            }, function (response, status) {
                                $($this).RModuleUnBlock();
                                //success
                                if (!response.success) {
                                    $.confirm({
                                        title: 'Thông báo !',
                                        type: 'red',
                                        content: response.messages,
                                        animation: 'scaleY',
                                        closeAnimation: 'scaleY',
                                        buttons: {
                                            cancel: {
                                                text: 'Đóng',
                                                btnClass: 're-btn re-btn-default'
                                            },
                                            yes: {
                                                isHidden: true, // hide the button
                                                keys: ['ESC'],
                                                action: function () {

                                                }
                                            }
                                        }

                                    });
                                    return;
                                }

                                if (response.success && response.error == 200) {
                                    var img = response.path;
                                    $this.find('img').attr({
                                        'src': img + '?w=300&h=300&mode=crop',
                                        'data-img': response.name
                                    });
                                }

                            });

                    });
                    $('#document-attack').off('click').on('click', function (e) {

                        R.FileManager.Open(function (ds) {
                            $.each(ds, function (idex, value) {
                                $('#document-attack').attr('data-attack', value).text(value);
                            })

                        }, '');
                    });
                    R.ScrollAutoSize('#news-id', function () {
                        return $(window).height();
                    }, function () {
                        if (CKEDITOR.instances["detailCkeditor"]) {
                            CKEDITOR.instances["detailCkeditor"].destroy();
                        }
                        CKEDITOR.replace('detailCkeditor', { toolbar: 'iweb', height: '350px' });

                        if (CKEDITOR.instances["Sapo"]) {
                            CKEDITOR.instances["Sapo"].destroy();
                        }
                        CKEDITOR.replace('Sapo', { toolbar: 'iweb', height: '350px' });
                        return 'auto';
                    }, {});


                    R.News.RegisterEvents();

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000,
                        className: "error",
                        globalPosition: 'right top'
                    });
                }

            }
        });
        // });
    },
    FixSideBar: function () {
        $('#sidebar').css({ 'min-height': $('#_body').height() });
        var length = $('#sidebar').height() - $('#sidebar-content').height() + $('#sidebar').offset().top;
        var height = $('#sidebar-content').height() + 'px';
        var w = $('#sidebar').width() + 'px';
        var top = $('#_header').height() + 13;
        $('.modal-content').scroll(function () {
            var scroll = $(this).scrollTop();
            if (scroll < $('#_header').offset().top) {
                $('#sidebar-content').css({
                    'position': 'absolute',
                    'top': '0'
                });

            } else if (scroll > length) {
                $('#sidebar-content').css({
                    'position': 'absolute',
                    'bottom': '0',
                    'top': scroll
                });

            } else {
                $('#sidebar-content').css({
                    'position': 'fixed',
                    'top': top,
                    'height': height,
                    'width': w
                });
            }
        });

    }

}

$(function () {
    $('[data-popup="tooltip"]').tooltip();
    R.News.Init();
    function cb(start, end, label) {
        $('#date-filter span').html(label + ": " + start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
    }
    moment.locale('vi');
    $('#date-filter').daterangepicker({
        //  startDate: start,
        //endDate: end,
        "opens": "right",
        ranges: {
            'Tất cả': [new Date('01/01/2014'), new Date('12/31/2050')],
            'Hôm nay': [moment(), moment()],
            'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '7 ngày trước': [moment().subtract(6, 'days'), moment()],
            '30 ngày trước': [moment().subtract(29, 'days'), moment()],
            'Tháng này': [moment().startOf('month'), moment().endOf('month')],
            'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, function (start, end, label) {
        $('#date-filter span').html(label + ": " + start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
    });
    $('#date-filter span').html("Tất cả: " + moment("01/01/2014").format('DD/MM/YYYY') + ' - ' + moment("12/31/2050").format('DD/MM/YYYY'));

});

