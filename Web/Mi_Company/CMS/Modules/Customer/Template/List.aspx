﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Mi_Company.CMS.Modules.Customer.Template.List" %>

<%@ Import Namespace="Mi_Company.Core.Helper" %>
<table class="table datatable-show-all dataTable no-footer" total-rows="<%=TotalRows%>" page-info="<%=pageInfo%>">
    <% if (TotalRows > 0)
        { %>
    <tbody>
        <asp:repeater runat="server" id="DataRpt">
            <itemtemplate>
                <tr role="row">
                    <td class="w50">
                        <div>
                           <label class="_hot">
                                <i class="icon-pencil7 new-edit position-static" data="<%# Eval("Id") %>"></i> #<%# Eval("Id") %>
                            </label>
                        </div>
                    </td>
            
                <td>
                <p>
                   <b> <%# Eval("FullName") %></b>
                </p>
                <p>
                    <label>Điện thoại:</label><%# Eval("Mobile") %>
                  
                </p> 
                <p>
                    <label>Mail:</label><%# Eval("Email") %>
                  
                </p>
                    <p>
                    <label>Tên công ty:</label><%# Eval("location") %>
                  
                </p>
                <p>
                    <label>Ngày tạo:</label><time><%# UIHelper.GetLongDate(Eval("CreatedDate")) %></time>
                </p>
            </td>
<%--             <td>
                <p>
                    <label>Cty:</label><%# Eval("Firm") %>
                 
                </p>
                 <p>
                    <label>Dịch vụ:</label><%# Eval("Service") %>
                 
                </p>
                <p>
                    <%# Eval("Address") %>
                </p>
            </td> --%>
                    <td>
                 <p>
                    <label>Ghi chú:</label><%# Eval("Note") %>
                 
                </p>
               
            </td>
                    <td>
                        <label>Note nhân viên:<textarea class="form-control txtNoteNhanVien" data-id="<%# Eval("Id") %>" style="vertical-align: top;" cols="50" rows="3" disabled><%#Eval("NoteNhanVien")%></textarea>
                    </td>
            
        
        </tr>
           </itemtemplate>
        </asp:repeater>


    </tbody>
    <% }
        else
        { %>
    <div class="">
        <div id="NoDataSolution">Không tìm thấy</div>
    </div>
    <% } %>
</table>
<div style="float: left; padding-top: 20px"><%= UIHelper.FormatCurrency("VND", TotalRows,false) %> bản ghi.</div>
