﻿R.Customer = {
    Init: function () {
        R.Customer.RegisterEvents();
        this.Oid = 0;
        this.ZoneType = 0;
        this.ZoneNode = null;
        //----
        this.PageIndex = 1;
        this.PageSize = 10;
        this.NewsStatus = 1;
        this.startDate = null;
        this.endDate = null;
        this.ItemSelected = [];
        this.XNewsId = 0;
        this.minDate = moment("01/01/2014").format('DD/MM/YYYY');
        this.maxDate = moment("12/31/2050").format('DD/MM/YYYY');

        R.Customer.Search();
    },
    RegisterEvents: function () {
        $('#news-save .btn-primary').off('click').on('click', function (e) {

            var name = $('#namext').val();
            var mail = $('#mailtxt').val();
            var phone = $('#phonetxt').val();
            var address = $('#addresstxt').val();
            var company = $('#companytxt').val();
            var type = $('#typetxt').val();
            var service = $('#servicedll').val();
            var status = $('#statusddl').val();
            var contact = $('#contacttxt').val();
            var note = $('#notetxt').val();

            var data = {
                id: R.Customer.Oid,
                name: name,
                email: mail,
                phone: phone,
                address: address,
                company: company,
                type: type,
                //status:status,
                service: service,
                status: status,
                contact: contact,
                note: note
            }

            R.Customer.XSave(data);
        });
        var delay = null;

        $('#module-content .btn-action button._edit').off('click').on('click', function (e) {
            var id = $('#module-content .datatable-scroll table tbody tr td input[type=checkbox]:checked').attr('id');

            R.Customer.XEdit(id);
        });

        $('#btn-new-post').off('click').on('click', function () {
            R.Customer.XEdit(0);
        });

        $('.new-edit').off('click').on('click', function () {
            console.log(1);
            var el = $(this).closest('.row').find('.txtNoteNhanVien');
            console.log(el);
        })

        $('.txtNoteNhanVien').dblclick(function () {
            alert(1);
            $(this).prop('disabled', false);
        });

        $(document).on('dblclick', '.txtNoteNhanVien', function () {
            //alert(1);
            $(this).prop('disabled', false);
            //Viet viet gi do
        })
        $('.txtNoteNhanVien').on('keydown', function (e) {
            if (e.key === "Escape") {
                //Save to database
                //update-note
                R.Post({
                    params: {
                        id: $(this).data('id'),
                        note: $(this).val()
                    },
                    module: "customer",
                    ashx: 'modulerequest.ashx',
                    action: "update-note",
                    success: function (res) {
                        if (res.Success) {
                            console.log(res.Message);
                            
                        }
                        R.Customer.RegisterEvents();
                    }
                });
                $(this).prop('disabled', true);
            }

        })


        $('#btn-new-reload').off('click').on('click', function (e) {
            $('#kwNametxt').val(''),
                $('#kwPhoneTxt').val('');
            R.Customer.PageIndex = 1;

            if (delay != null) clearTimeout(delay);
            delay = setTimeout(function () {
                R.Customer.Search();
            }, 200);
        });



        $('#searchBtn').off('click').on('click', function (e) {
            R.Customer.Search();
        });

        $('#data-list .datatable-scroll._list table tr td .edit').off('click').on('click', function (e) {
            var id = $(this).attr('data');
            R.Customer.Oid = id;
            R.Customer.XEdit(id);
        });

        $('#news-sldebar .navigation li.sub-news-game').off('click').on('click',
            function () {
                $('#news-sldebar .navigation li').removeClass('active');
                $(this).addClass('active');
                R.Customer.PageIndex = 1;
                if (delay != null) clearTimeout(delay);
                delay = setTimeout(function () {
                    R.Customer.Search();
                },
                    200);

            });
    },

    XUpdateStatus: function (data) {
        R.Post({
            params: data,
            module: "customer",
            ashx: 'modulerequest.ashx',
            action: "update_status",
            success: function (res) {
                if (res.Success) {
                    $.notify("Cập nhật thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.Customer.XNews();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }

        });
    },

    Search: function () {
        var el = '#data-list';
        $(el).RLoading();
        var data = {
            keyword: $('#_search-data').val(),
            pageindex: R.Customer.PageIndex,
            pagesize: R.Customer.PageSize,
            status: R.Customer.NewsStatus,
            type: $('#news-sldebar .navigation li.sub-news-game.active').attr('data-type')
        }
        R.Post({
            params: data,
            module: "customer",
            ashx: 'modulerequest.ashx',
            action: "xsearch",
            success: function (res) {
                if (res.Success) {
                    $('#data-list').find('.datatable-scroll._list').html(res.Content);
                    R.CMSMapZone('#data-list');
                    R.Customer.RegisterEvents();
                    R.ScrollAutoSize('.datatable-scroll', function () {
                        return $(window).height() - 92;
                    }, function () {
                        return 'auto';
                    }, {}, {}, {}, true);
                    if ($('#data-list .datatable-scroll._list table').attr('page-info') != 'undefined') {
                        var pageInfo = $('#data-list .datatable-scroll._list table').attr('page-info').split('#');
                        var page = pageInfo[0];
                        var extant = pageInfo[1];
                        var totals = pageInfo[2];
                        if (parseInt(totals) < 0) {
                            $('#data-pager').hide();
                            return;
                        } else {
                            $('#data-pager').show();
                        }
                        var rowFrom = '';
                        if (R.Customer.PageIndex === 1) {
                            rowFrom = 'Từ 1 đến ' + extant;
                        } else {
                            rowFrom = 'Từ ' + (parseInt(R.Customer.PageIndex) * parseInt(R.Customer.PageSize) - parseInt(R.Customer.PageSize)) + ' đến ' + extant;
                        }

                        $('#rowInTotals').text(rowFrom + '/' + totals);
                        $('.ipagination').jqPagination({
                            current_page: R.Customer.PageIndex,
                            max_page: page,
                            paged: function (page) {
                                R.Customer.PageIndex = page;
                                R.Customer.Search();
                            }
                        });
                    }

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                $(el).RLoadingComplete();
            }
        });
    },
    XEdit: function (id) {

        $('body').RModuleBlock();
        R.Post({
            params: {
                id: id
                // zoneType: R.Customer.ZoneType
            },
            module: "customer",
            ashx: 'modulerequest.ashx',
            action: "xedit",
            success: function (res) {

                if (res.Success) {

                    R.Customer.Oid = id;

                    R.ShowOverlayFull({ content: res.Content }, function () {


                    }, function () {

                    });
                    if (id > 0) {
                        $("#servicedll").val($("#servicedll").attr('data-selected')).change();
                        $("#statusddl").val($("#statusddl").attr('data-selected')).change();
                    }
                    autosize(document.querySelectorAll('#notetxt'));
                    R.ScrollAutoSize('#zone-content', function () {
                        return $(window).height() - 10;
                    }, function () {

                        return 'auto';
                    }, {});


                    $('body').RModuleUnBlock();
                    R.Customer.RegisterEvents();

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }
        });
    },
    XSave: function (data) {
        $('#zone-edit').RModuleBlock();
        R.Post({
            params: data,
            module: "customer",
            ashx: 'modulerequest.ashx',
            action: "xsave",
            success: function (res) {
                if (res.Success) {
                    R.Customer.Oid = 0;
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.Customer.Search();
                    R.CloseOverlayFull();


                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                $('#zone-edit').RModuleUnBlock();
                R.Customer.RegisterEvents();
            }
        });
    },


}
$(function () {
    R.Customer.Init();
});