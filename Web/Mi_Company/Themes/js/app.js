
sliderLogo();
function sliderLogo() {
    var sliderLogo = new Swiper('.slide-logo .swiper-container', {
        slidesPerView: 5,
        spaceBetween: 30,
        loop: true,
        breakpoints: {
            320: {

            },
            567: {
                slidesPerView: 2,
                spaceBetween: 15,
            },
            767: {
                slidesPerView: 3,
                spaceBetween: 15,
            },
            991: {
                slidesPerView: 4,
                spaceBetween: 20,
            },
        }

    });
}
sliderLogo2();
function sliderLogo2(){
    var sliderLogo2 = new Swiper('.slide-logo-2 .swiper-container', {
        slidesPerView: 5,
        slidesPerColumn: 5,
        spaceBetween: 30,
        loop:true,
        autoplay:true,
        breakpoints: {
            320: {

            },
            567: {
                slidesPerView: 2,
                spaceBetween: 15,
            },
            767: {
                slidesPerView: 3,
                spaceBetween: 15,
            },
            991: {
                slidesPerView: 4,
                spaceBetween: 20,
            },
        }


    });
}
sliderBlog();
function sliderBlog(){
    var sliderBlog = new Swiper('.slide-blogs .swiper-container', {
        slidesPerView: 2.5,
        spaceBetween: 30,
        loop:true,
        autoplay:true,
        navigation: {
            nextEl: '.slide-blogs .swiper-button-next',
            prevEl: '.slide-blogs .swiper-button-prev',
          },
        breakpoints: {
            320: {

            },
            567: {
                slidesPerView: 2,
                spaceBetween: 15,
            },
            767: {
                slidesPerView: 3,
                spaceBetween: 15,
            },
            991: {
                slidesPerView: 4,
                spaceBetween: 20,
            },
        }


    });
}




$(window).scroll(function () {
    if ($(this).scrollTop() >= 30) {        // If page is scrolled more than 50px
        $('.header').addClass("fixed");    // Fade in the arrow
    } else {
        $('.header').removeClass("fixed");   // Else fade out the arrow
    }
});


function topFunction() {
    $('body,html').animate({
        scrollTop: 100                       // Scroll to top of body
    }, 500);
}
function topFunction_home() {
    $('body,html').animate({
        scrollTop: 0                       // Scroll to top of body
    }, 500);
}



//2-1-2020
$('.header .nav-bottom .nav-item.sub').click(function () {
    $('.header .nav-bottom .nav-item.sub').removeClass("show");
    $(this).toggleClass('show');
    
})

var swiper = new Swiper('.tourkit-video .swiper-container', {
    effect: 'coverflow',
    grabCursor: true,
    slidesPerView: 3,
    loop:true,
    autoplay:true,
    coverflowEffect: {
      rotate: 0,
      stretch: 0,
      depth: 80,
      modifier: 2,
      slideShadows : false,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
  });