﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Mi.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Mi_Company._default1" %>

<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .dich-vu-active {
            /*background: #FFFFFF;*/
            box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25) !important;
            border-radius: 10px !important;
            padding: 1.8rem !important;
            /*transition: .3s;*/
            height: 300px !important;
            position: relative !important;
            margin-bottom: 1.5rem !important;
            cursor: pointer !important;
            font-size: 14px !important;
            background: linear-gradient(215.48deg, #FE8837 3.98%, #FC4A35 96.58%), #FFFFFF !important;
            transition: .3s !important;
            color: #fff !important;
        }
        /*.full-imange img{
            height:100%;
            width:auto;
        }*/
        .mi-card .image {
            height:400px;
        }
        .card-body{
            height:300px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <%--<h1>Web Mi - Ready to code</h1>--%>
    <%--<section class="banner-home my-3 px-md-3">--%>
    <section class="banner-home my-3 container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-12 pr-md-0 ">
                <div id="carouselExampleIndicators" class="carousel slide slide-home mb-3 mb-md-0" data-ride="carousel">
                    <%var slider = ConfigBo.AdvGetByType(2, lang).ToList(); %>
                    <ol class="carousel-indicators">
                        <%for(int i = 0; i < slider.Count(); i++){ %> 
                            <li data-target="#carouselExampleIndicators" data-slide-to="<%=i %>" <%=i==0?"class=\"active\"":"" %>></li>
                        <%} %>
                        
<%--                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>--%>
                      </ol>
                    <div class="carousel-inner">
                        <%//Lay thong tin slide %>
                        
                        <%for (int i = 0; i < slider.Count(); i++)
                            { %>
                        <div class="carousel-item <%=i==0?"active":"" %>">
                            <a href="<%=slider[i].Url %>">
                                <img src="/uploads/<%=slider[i].Thumb %>" class="d-block w-100" alt="<%=domainName %>">
                            </a>
                            
                            <%--<div class="carousel-caption text-center">
                                <h1 class="text-uppercase mb-3">
                                    <%=slider[i].Name %>
                                </h1>
                                <%var listContent = new List<string>(); %>
                                <%if (slider[i].Content.Contains(';'))
                                    { %>
                                <%listContent = UIHelper.ClearHtmlTag(slider[i].Content).Split(';').ToList();  %>
                                <%}
                                    else
                                    { %>
                                <%listContent.Add(UIHelper.ClearHtmlTag(slider[i].Content)); %>
                                <%} %>
                                <p>
                                    <%=listContent[0] %>
                                </p>
                                <div class="line">
                                </div>
                                <%if (listContent.Count() > 2)
                                    { %>
                                <p>
                                    <%=listContent[1] %>
                                </p>
                                <%} %>
                                <%if (listContent.Count() > 3)
                                    { %>
                                <p>
                                    <%=listContent[2] %>
                                    <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i><i class="fas fa-star"></i><%if (listContent.Count() >= 4)
                                                                                              { %> <%=listContent[3] %> <%} %>
                                </p>
                                <%} %>

                                <a href="javascript:void(0)" class="btn btn-try">DÙng thử miễn phí</a>
                            </div>--%>
                        </div>
                        <%} %>
                    </div>
                    <%--<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <i class="fas fa-chevron-circle-left"></i>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <i class="fas fa-chevron-circle-right"></i>
                    </a>--%>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-12 box-right">
                <div class="row ">
                    <div class="col-12 ">
                        <%var slider_take_3 = ConfigBo.AdvGetByType(3, lang).ToList(); %>
                        <%for (int i = 0; i < slider_take_3.Count(); i++)
                            { %>

                        <div class="item">
                            <a href="<%=slider_take_3[i].Url %>" title="<%=domainName %>" class="link">
                                <img src="/uploads/<%=slider_take_3[i].Thumb %>" class="img-fluid " />
                            </a>
                        </div>
                        <%} %>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="services">
        <div class="container">
            <div class="heading-ss-home">
                <%=UIHelper.GetConfigByName("TieuDeDong1") %>
            </div>
            <p class="text-center mb-4">
                <%=UIHelper.GetConfigByName("NoiDungDong1") %>
            </p>
            <div class="row">
                <%var list_dich_vu = ConfigBo.AdvGetByType(4, lang); %>
                <%var list_dich_vu_1 = ZoneBo.GetZoneWithLanguageByType(7, lang).Where(r => r.ParentId > 0).ToList(); %>
                <%var flag = 0; %>
                <%foreach (var item in list_dich_vu_1)
                    { %>

                <%var total_dv = 0; %>
                <%var news_dich_vu = NewsBo.GetNewsDetailWithLanguage(7, lang, 2, item.Id, "", 1, 1, ref total_dv).FirstOrDefault(); %>
                <%var link_tar = "javascript:void(0)"; %>
                <%if (news_dich_vu != null)
                    { %>
                <%link_tar = string.Format("/{0}/{1}/{2}-{3}.htm", currentLanguage, "dich-vu", news_dich_vu.Url, news_dich_vu.NewsId); %>
                <%} %>
                <%if (item.lang_name.Contains("-"))
                        link_tar = "/"+currentLanguage+"/"+UIHelper.CutContentByMinusCharactor(item.lang_name)[1].Trim();%>
                <div class="dich-vu-config col-lg-3 col-md-3 col-sm-6 col-12 ">
                    <a href="<%=link_tar %>">
                        <div class="item <%=flag==0?"dich-vu-active":"" %>">
                        <div class="image mb-3">
                            <img src="/uploads/<%=item.Avatar %>" class="img-fluid" title="<%=item.Name %>" alt="<%=domainName %>" />
                        </div>
                        <h5><%=item.Name %>
                        </h5>
                        <p>
                            <%=item.Content %>
                        </p>
                        <a href="<%=link_tar %>" class="view-more">Xem thêm<i class="fas fa-arrow-right ml-3"></i>
                        </a>
                    </div>
                    </a>
                    
                </div>
                <%flag++; %>
                <%} %>
            </div>
            <div class="group-btn text-center my-4">
                <a href="/<%=currentLanguage %>/lien-he" class="btn btn-contact mr-md-3 mb-3">LIÊN HỆ TƯ VẤN NGAY
                    <img src="/Themes/images/contact-ic.svg" class="img-fluid ml-2" />
                </a>
                <a href="javascript:void(0)" class="btn mb-3" style="background: #FC4A35;" data-toggle="modal"
                    data-target="#exampleModalScrollable">YÊU CẦU BÁO GIÁ MIỄN PHÍ
                    <img src="/Themes/images/yeu-cau-icon.svg" class="img-fluid ml-2 " />
                </a>

            </div>
        </div>
    </section>
    <%//Lay thong tin slider_dich_vu_2 %>
    <%var slider_dv_2 = ConfigBo.AdvGetByType(5, lang).ToList(); %>
    <%var link_banner_dv_2 = slider_dv_2.Count > 0 ? slider_dv_2[0].Thumb : ""; %>
    <section class="strong-point py-4" style="background: url(/uploads/<%=link_banner_dv_2%>) no-repeat center">
        <div class="container">
            <div class="row">
                <%var slider_dv_2_cuted = slider_dv_2.Skip(1); %>
                <%foreach (var item in slider_dv_2_cuted)
                    { %>
                <div class="col-lg-3 col-md-3 col-sm-6 col-6 align-self-center">
                    <div class="d-flex py-3">
                        <div class="image align-self-center">
                            <img src="/uploads/<%=item.Thumb %>" class="img-fluid" alt="<%=item.Name %>" />
                        </div>
                        <div class="px-3 align-self-center">
                            <%=item.Content %>
                        </div>
                    </div>
                </div>
                <%} %>
            </div>
        </div>
    </section>

    <section class="mockup py-4 mb-5">

        <div class="container">
            <div class="heading-ss-home mb-5">
                <%var tieude_dong2 = UIHelper.GetConfigByName("TieuDeDong2"); %>
                <%var tieude_dong2_cutted = new List<string>();
                    if (tieude_dong2.Contains(';'))
                        tieude_dong2_cutted = tieude_dong2.Split(';').ToList();
                    else
                        tieude_dong2_cutted.Add(tieude_dong2);
                %>
                <%if (tieude_dong2_cutted.Count() > 1)
                    { %>
                <%foreach (var item in tieude_dong2_cutted)
                    { %>
                <div><%=item %></div>
                <%} %>
                <%}
                    else
                    { %>
                <div><%=tieude_dong2_cutted[0] %></div>
                <%} %>
            </div>
            <%var slider_3 = ConfigBo.AdvGetByType(6, lang).ToList(); %>
            <div class="text-center">
                <img style="background-cover" src="/uploads/thumb/<%=slider_3.Count>0? slider_3[0].Thumb:"" %>" class="img-fluid img-mockup mb-3" alt="<%=domainName %>" />
            </div>
            <%var slider_3_cutted = slider_3.Skip(1);
                var slider_3_cutted_1 = slider_3_cutted.Skip(0).Take(2);
                var slider_3_cutted_2 = slider_3_cutted.Skip(2).Take(2);
            %>
            <div class="row justify-content-between">
                <%foreach (var item in slider_3_cutted_1)
                    { %>
                <div class="col-xl-4 col-md-4 col-sm-4 col-12">
                    <div class="item text-right">
                        <div class="d-flex mb-3">
                            <h5><%=item.Name %></h5>
                            <div class="ml-3">
                                <img src="/uploads/<%=item.Thumb %>" class="img-fluid" alt="<%=item.Name %>" />
                            </div>
                        </div>
                        <p>
                            <%=item.Content %>
                        </p>
                    </div>
                </div>
                <%} %>
            </div>
            <div class="row justify-content-between">
                <%foreach (var item in slider_3_cutted_2)
                    { %>
                <div class="col-xl-4 col-md-4 col-sm-4 col-12">
                    <div class="item text-right">
                        <div class="d-flex mb-3">
                            <h5><%=item.Name %></h5>
                            <div class="ml-3">
                                <img src="/uploads/<%=item.Thumb %>" class="img-fluid" alt="<%=item.Name %>" />
                            </div>
                        </div>
                        <p>
                            <%=item.Content %>
                        </p>
                    </div>
                </div>
                <%} %>
            </div>
        </div>
    </section>

    <br />
    <br />
    <section class="projects py-4">
        <div class="container">
            <div class="heading-ss-home">
                <%=UIHelper.GetConfigByName("TieuDeDong3") %>
            </div>
            <div class="row justify-content-center">
                <%var total_1 = 0;
                %>
                <%var list_6_du_an = NewsBo.GetNewsDetailWithLanguage(1, lang, 1, 0, "", 1, 6, ref total_1); %>
                <%foreach (var item in list_6_du_an)
                    { %>
                <%var link_target = string.Format("/{0}/du-an/{1}-{2}.htm", currentLanguage, item.Url, item.NewsId);//Cho nay set link sau %>
                <div class="col-xl-4 col-lg-4 col-sm-6 col-12">
                    <div class="card mi-card mb-4">
                        <div class="image">
                            <a href="<%=link_target %>" title="">
                                <img src="/uploads/<%=item.Avatar %>" class="card-img-top" alt="<%=item.Title %>"></a>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title font-weight-bold">
                                <a href="<%=link_target %>" title=""><%=item.Title %>
                                </a>
                            </h5>
                            <p class="card-text">
                                <%=UIHelper.TrimByWord(UIHelper.ClearHtmlTag(item.Sapo),20,"...") %>
                            </p>

                        </div>
                    </div>
                </div>
                <%} %>
                <div>
                    <a href="javascript:void(0)" role="button" class="btn view-more-projet">Xem thêm các dự án khác
                    </a>
                </div>
            </div>
        </div>
    </section>




    <section>
        <%var banner_phu_home = UIHelper.GetConfigByName("BannerPhuTrangChu"); %>
        <img src="/uploads/<%=banner_phu_home %>" class="img-fluid" />
    </section>

    <section class="lastest-new-home py-4">
        <div class="container">
            <div class="heading-ss-home">
                <%=UIHelper.GetConfigByName("TieuDeDong4") %>
            </div>
            <p class="text-center mb-4">
                <%=UIHelper.GetConfigByName("NoiDungDong4") %>
            </p>
            <div class="row justify-content-center">
                <%var total_2 = 0; %>
                <%var list_tin_hot = NewsBo.GetNewsDetailWithLanguage(3, lang, 1, 0, "", 1, 4, ref total_2); %>
                <%foreach (var item in list_tin_hot)
                    { %>
                <%var link_taget = string.Format("/{0}/{1}/{2}-{3}.htm", currentLanguage, UIHelper.GetTypeUrlByMiProject(3), item.Url, item.NewsId);//Cai nay set sau %>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="item-lastest-new-home">
                        <div class="row">
                            <div class="col-xl-3 col-lg-4 col-md-4 col-sm-3 col-4 pr-0">
                                <div class="image">
                                    <a href="<%=link_taget %>" title="">
                                        <img src="/uploads/thumb/<%=item.Avatar %>" class="img-fluid" alt="<%=item.Title %>" />
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-8 col-md8 col-sm-8 col-8 pl-4">
                                <div class="row">
                                    <span class="time"><%=item.CreatedDate.ToString("dd/MM/yyyy") %>
                                </span>
                                </div>
                                <br />
                                <div class="row">
                                    <h5 class="title">
                                    <a href="<%=link_taget %>" title=""></a>
                                    <%=item.Title %>
                                </h5>
                                <div class="des">
                                    <%=item.Sapo %>
                                </div>
                                <a href="<%=link_taget %>" class="view-more">đọc tiếp<i class="fas fa-arrow-right ml-3" aria-hidden="true"></i>
                                </a>
                                </div>
                                

                            </div>
                        </div>
                    </div>
                </div>
                <%} %>
            </div>

        </div>
    </section>

    <section class="contact-home py-3">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-4 col-sm-4 col-12">
                    <div class="d-flex py-3 justify-content-center">
                        <div class="image align-self-center">
                            <img src="/Themes/images/hotline-icon-contact.svg" class="img-fluid" />
                        </div>
                        <div class="px-3 align-self-center">
                            HotLine: <%=UIHelper.GetConfigByName("SDTHotLine") %>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 col-sm-4 col-12">
                    <div class="d-flex py-3 justify-content-center">
                        <div class="image align-self-center">
                            <img src="/Themes/images/hotline-icon-contact.svg" class="img-fluid" />
                        </div>
                        <div class="px-3 align-self-center">
                            Phòng kinh doanh : <%=UIHelper.GetConfigByName("DienThoai1") %>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 col-sm-4 col-12">
                    <div class="d-flex py-3 justify-content-center">
                        <div class="image align-self-center">
                            <img src="/Themes/images/email-icon-contact.svg" class="img-fluid" />
                        </div>
                        <div class="px-3 align-self-center">
                            Email : <%=UIHelper.GetConfigByName("Email") %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <script type="text/javascript">
        //$('.dich-vu-active').addClass('hover');
        //$('.dich-vu-active').trigger('hover');
        $('.dich-vu-config').hover(function () {
            var el = $(this).parent().find('.dich-vu-active');
            $(el).removeClass('dich-vu-active');
        })
    </script>
</asp:Content>
