﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Mi.Master" AutoEventWireup="true" CodeBehind="Mi_ChiTietDichVu.aspx.cs" Inherits="Mi_Company.Pages.Mi_ChiTietDichVu" %>
<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        var s_id = Page.RouteData.Values["id"].ToString();
        var id = int.Parse(s_id);
        var newsDetail = NewsBo.GetNewsDetailWithLanguageById(id, lang);
        var delete_html = @"<[^>]*>";
        //var sapo = obj.Sapo.Replace(delete_html, "");
        var sapo = Regex.Replace(obj.Sapo, delete_html, "");
        %>
    <style>
        .des img {
            max-width: 100%;
            height: auto;
        }
        .des li{
            list-style-type:disc;
            list-style-position:inside;
        }
    </style>
    <meta property="fb:app_id" content="1426300910760607" />
    <meta property="og:url" content="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" />
    <meta property="og:type" content="article" />
    <%if (obj != null)
      { %>
        <meta property="og:title" content="<%= obj.Title %>" />
        <meta property="og:description" content="<%=sapo%>" />
        <meta property="og:image" content="<%=domainName %>/uploads/<%= obj.Avatar %>" />
    <%} %>
    <link rel="canonical" href="<%=newsDetail.Title %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <%var id = int.Parse(Page.RouteData.Values["id"].ToString()); %>
    <%var detail = NewsBo.GetNewsDetailWithLanguageById(id, lang); %>
    <%--<h1><%=detail.Title %></h1>--%>
    <%var delete_html = @"<[^>]*>";
        //var sapo = obj.Sapo.Replace(delete_html, "");
        var sapo = Regex.Replace(obj.Sapo, delete_html, ""); %>
    <script type="application/ld+json">
				            {  
                                    "@context":"http://schema.org",
                                    "@type":"Article",
                                    "mainEntityOfPage":{  
                                        "@type":"WebPage",
                                        "@id":"<%=HttpContext.Current.Request.Url.AbsoluteUri %>"
                                    },
                                   "headline":"<%=!string.IsNullOrEmpty(obj.MetaTitle)?obj.MetaTitle:obj.Title%>",
                                    "image":{  
                                        "@type":"ImageObject",
                                        "url":"<%=domainName+"/uploads/"+obj.Avatar %>",
                                        "height":600,
                                        "width":800
                                    },
                                    "datePublished":"<%=obj.CreatedDate %>",
                                    "dateModified":"<%=obj.CreatedDate %>",
                                    "author":{  
                                        "@type":"Person",
                                        "name":"migroup"
                                    },
                                    "publisher":{  
                                        "@type":"Organization",
                                        "name":"<%=domainName %>",
                                        "logo":{  
                                            "@type":"ImageObject",
                                            "url":"<%=domainName+"/uploads/"+UIHelper.GetConfigByName("LogoTop")%>",
                                            "width":35,
                                            "height":34
                                        }
                                    },
                                    "description":"<%=sapo %>"
                            }
    </script>
   <section class="services mt-4">
        <div class="container">
            <div class="service-detail mb-5">
                <h1 class="title">
                    <%=detail.Title %>
                </h1>
                <p class="des">
                    <%=detail.Sapo %>
                </p>
                <%=detail.Body %>
            </div>


            <div class="group-btn text-center my-4">
                <a href="javascript:void(0)" class="btn btn-contact mr-md-3 mb-3">
                    LIÊN HỆ TƯ VẤN NGAY <img src="/Themes/images/contact-ic.svg" class="img-fluid ml-2" />
                </a>
                <a href="javascript:void(0)" class="btn mb-3" style="background:#FC4A35;" data-toggle="modal"
                    data-target="#exampleModalScrollable">
                    YÊU CẦU BÁO GIÁ MIỄN PHÍ <img src="/Themes/images/yeu-cau-icon.svg" class="img-fluid ml-2 " />
                </a>
            </div>
            <div class="like-share-commnet">
                        <div class="fb-like" data-href="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="" data-layout="standard" data-action="like" data-size="small" data-share="true"></div>
                        <div class="fb-comments" data-href="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="" data-numposts="10"></div>
                    </div>
        </div>
    </section>

    <section class="bg-form-tu-van py-5">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-xl-7 col-lg-6 col-md-7 col-sm-6 col-12 align-self-center">
                    <h1 class="mb-3">
                        <%=UIHelper.GetConfigByName("TieuDeTuyenDungD1") %>
                        <div>
                            <%=UIHelper.GetConfigByName("TieuDeTuyenDungD2") %>
                        </div>
                    </h1>
                    <p>
                        <%=UIHelper.GetConfigByName("NoiDungTuyenDungD2") %>
                    </p>
                </div>
                <div class="col-xl-4 col-lg-5 col-md-4 col-sm-6 col-12">
                    <div class="form-tv">
                        <h5>
                            BẠN CẦN TƯ VẤN ?!
                        </h5>
                        <p>
                            Quý khách hàng vui lòng điền thông tin dưới đây
                            để được tư vấn nhanh nhất
                        </p>
                        <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="Tên doanh nghiệp của ban?">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="Họ và tên*">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="" placeholder="Email*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="Điện thoại*">
                        </div>
                       <div>
                        <button type="button" class="btn btn-submit">GỬI THÔNG TIN TƯ VẤN</button>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="contact-home py-3">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-4 col-sm-4 col-12">
                    <div class="d-flex py-3 justify-content-center">
                        <div class="image align-self-center">
                            <img src="/Themes/images/hotline-icon-contact.svg" class="img-fluid" />
                        </div>
                        <div class="px-3 align-self-center">
                            HotLine: <%=UIHelper.GetConfigByName("SDTHotLine") %>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 col-sm-4 col-12">
                    <div class="d-flex py-3 justify-content-center">
                        <div class="image align-self-center">
                            <img src="/Themes/images/hotline-icon-contact.svg" class="img-fluid" />
                        </div>
                        <div class="px-3 align-self-center">
                            Phòng kinh doanh : <%=UIHelper.GetConfigByName("DienThoai1") %>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 col-sm-4 col-12">
                    <div class="d-flex py-3 justify-content-center">
                        <div class="image align-self-center">
                            <img src="/Themes/images/email-icon-contact.svg" class="img-fluid" />
                        </div>
                        <div class="px-3 align-self-center">
                            Email : <%=UIHelper.GetConfigByName("Email") %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
