﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/MiWBanner.Master" AutoEventWireup="true" CodeBehind="Mi_ChiTietBlog.aspx.cs" Inherits="Mi_Company.Pages.Mi_ChiTietBlog" %>

<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        var s_id = Page.RouteData.Values["id"].ToString();
        var id = int.Parse(s_id);
        var newsDetail = NewsBo.GetNewsDetailWithLanguageById(id, lang);
        var delete_html = @"<[^>]*>";
        //var sapo = obj.Sapo.Replace(delete_html, "");
        var sapo = Regex.Replace(obj.Sapo, delete_html, "");
        %>
    <style>
        .news-body img {
            max-width: 100%;
            height: auto;
        }
        .news-body li{
            list-style-type:disc;
            list-style-position:inside;
        }
    </style>
    <meta property="fb:app_id" content="1426300910760607" />
    <meta property="og:url" content="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" />
    <meta property="og:type" content="article" />
    <%if (obj != null)
      { %>
        <meta property="og:title" content="<%= obj.Title %>" />
        <meta property="og:description" content="<%=sapo%>" />
        <meta property="og:image" content="<%=domainName %>/uploads/<%= obj.Avatar %>" />
    <%} %>
    <link rel="canonical" href="<%=newsDetail.Title %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
 <%
     string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
     string domainNameFull = HttpContext.Current.Request.Url.LocalPath;
    %>
    <%var id = int.Parse(Page.RouteData.Values["id"].ToString()); %>
    <%var newsDetail = NewsBo.GetNewsDetailWithLanguageById(id, lang); %>
    <%--<h1><%=newsDetail.Title %></h1>--%>
    <%var delete_html = @"<[^>]*>";
        //var sapo = obj.Sapo.Replace(delete_html, "");
        var sapo = Regex.Replace(obj.Sapo, delete_html, ""); %>
    <script type="application/ld+json">
				            {  
                                    "@context":"http://schema.org",
                                    "@type":"Article",
                                    "mainEntityOfPage":{  
                                        "@type":"WebPage",
                                        "@id":"<%=HttpContext.Current.Request.Url.AbsoluteUri %>"
                                    },
                                   "headline":"<%=!string.IsNullOrEmpty(obj.MetaTitle)?obj.MetaTitle:obj.Title%>",
                                    "image":{  
                                        "@type":"ImageObject",
                                        "url":"<%=domainName+"/uploads/"+obj.Avatar %>",
                                        "height":600,
                                        "width":800
                                    },
                                    "datePublished":"<%=obj.CreatedDate %>",
                                    "dateModified":"<%=obj.CreatedDate %>",
                                    "author":{  
                                        "@type":"Person",
                                        "name":"migroup"
                                    },
                                    "publisher":{  
                                        "@type":"Organization",
                                        "name":"<%=domainName %>",
                                        "logo":{  
                                            "@type":"ImageObject",
                                            "url":"<%=domainName+"/uploads/"+UIHelper.GetConfigByName("LogoTop")%>",
                                            "width":35,
                                            "height":34
                                        }
                                    },
                                    "description":"<%=sapo %>"
                            }
    </script>
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="detail-bl mb-3">
                        <h1 class="title"><%=newsDetail.Title %></h1>
                        <div class="des ">
                            <%=newsDetail.Sapo %>
                        </div>
                        <div class="news-body">
                            <%=newsDetail.Body %>
                        </div>

                    </div>
                    <div class="like-share-commnet">
                        <div class="fb-like" data-href="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="" data-layout="standard" data-action="like" data-size="small" data-share="true"></div>
                        <div class="fb-comments" data-href="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="" data-numposts="10"></div>
                    </div>
                    <div class="list-tags mb-3 ">
                        <label class="d-inline-block mr-3">Tags:</label>
                        <%//Lay tag %>
                        <%var tags = newsDetail.Tags.Split(','); %>
                        <%foreach (var item in tags)
                            { %>
                        <div class="item">
                            <%var link_target = "javascript:void(0)";//Cho nay set sau %>
                            <a href="<%=link_target %>"><%=item %></a>
                        </div>
                        <%} %>
                    </div>
                    <%//Lay cac bai viet lien quan %>
                    <%int total_1 = 0; %>
                    <%var list_tin_lien_quan = NewsBo.GetNewsDetailWithLanguage(3, lang, 2, newsDetail.ZoneId, "", 1, 3, ref total_1); %>
                    <%foreach (var item in list_tin_lien_quan)
                        { %> 
                    <%var link_target = string.Format("/{0}/{1}/{2}-{3}.htm", currentLanguage, UIHelper.GetTypeUrlByMiProject(3), item.Url, item.NewsId); %>
                        <div class="item-news">
                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-5 align-self-center">
                                <div class="image mb-3 mb-sm-0">
                                    <a href="<%=link_target %>" title="">
                                        <img src="/uploads/<%=item.Avatar %>" class="img-fluid" alt="<%=item.Title %>"></a>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-7 col-7 align-self-center">
                                <h3 class="title">
                                    <a href="<%=link_target %>" title=""><%=item.Title %>
                                    </a>
                                </h3>
                                <div class="time small">
                                    <%=item.CreatedDate.ToString("dd/MM/yyyy") %>
                                </div>
                                <p class="des">
                                    <%=item.Sapo %>
                                </p>
                            </div>
                        </div>

                    </div>
                    <%} %>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div class="blog-ss-right mb-3">
                        <div class="heading">
                            <a title="" href="javascript:void(0)">Tin đọc nhiều</a>
                        </div>
                        <%//Lấy danh sách tin hot %>
                        <%var total_2 = 0; %>
                        <%var list_4_tin_hot = NewsBo.GetNewsDetailWithLanguage(3, lang, 1, 0, "", 1, 4, ref total_2).ToList(); %>
                        <%for (int i = 0; i < list_4_tin_hot.Count(); i++)
                            {  %>
                            <%if (i == 0)
    { %> 
                        <div class="item-left mb-3">
                            <%var link_target = string.Format("/{0}/{1}/{2}-{3}.htm", currentLanguage, UIHelper.GetTypeUrlByMiProject(3), list_4_tin_hot[i].Url, list_4_tin_hot[i].NewsId); %>
                            <div class="image">
                                <a href="<%=link_target %>" title="">
                                    <img src="/uploads/<%=list_4_tin_hot[i].Avatar %>" class="img-fluid" alt="<%=list_4_tin_hot[i].Title %>" /></a>
                            </div>
                            <h2 class="title">
                                <a href="<%=link_target %>" title=""><%=list_4_tin_hot[i].Title %></a>
                            </h2>
                            <div class="detail">
                                <%=UIHelper.TrimByWord(list_4_tin_hot[i].Sapo, 20, "...") %>
                            </div>

                        </div>
                        <%} else {%> 
                        <%var link_target = string.Format("/{0}/{1}/{2}-{3}.htm", currentLanguage, UIHelper.GetTypeUrlByMiProject(3), list_4_tin_hot[i].Url, list_4_tin_hot[i].NewsId); %>
                            <div class="item-right d-flex">
                            <div class="image">
                                <a href="<%=link_target %>" title="">
                                    <img src="/uploads/thumb/<%=list_4_tin_hot[i].Avatar %>" class="img-fluid" alt="<%=list_4_tin_hot[i].Title %>" /></a>
                            </div>
                            <div class="text">
                                <h2 class="title">
                                    <a href="<%=link_target %>" title=""><%=list_4_tin_hot[i].Title %></a>
                                </h2>
                                <div class="time">
                                    <%=list_4_tin_hot[i].CreatedDate.ToString("dd/MM/yyyy") %>
                                </div>
                            </div>

                        </div>
                        <%} %>

                        <%} %>

                    </div>

                    <div class="blog-ss-right mb-3">
                        <div class="heading">
                            <a title="" href="javascript:void(0)">Video</a>
                        </div>
                        <%//Lay tin video %>
                        <%var total_3 = 0; %>
                        <%var list_3_video = NewsBo.GetNewsDetailWithLanguage(5, lang, 2, 0, "", 1, 3, ref total_3); %>
                        <%foreach (var item in list_3_video)
                            { %> 
                            <div class="item-video mb-3">
                            <iframe width="100%" height="215" src="<%=UIHelper.ConvertLinkYoutubeVideo(item.Body) %>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="time">
                                <%=item.CreatedDate.ToString("dd/MM/yyyy") %>
                            </div>
                        </div>
                        <%} %>
                        

                    </div>
                    <div class="blog-ss-right mb-3">
                        <div class="heading">
                            <a title="" href="javascript:void(0)">Ưu đãi</a>
                        </div>
                        <div class="font-weight-bold mb-3">
                            <%=UIHelper.GetConfigByName("TieuDeUuDaiBlog") %>
                        </div>
                        <p>
                            <%=UIHelper.GetConfigByName("NoiDungUuDaiBlog") %>
                        </p>
                        <div class="ads">
                            <a href="javascript:void(0)" title="">
                                <img src="/uploads/<%=UIHelper.GetConfigByName("BannerUuDaiBlog") %>" class="img-fluid w-100 mb-3" alt="<%=domainName %>"></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
