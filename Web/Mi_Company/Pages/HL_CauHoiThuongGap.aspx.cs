﻿using Mi_Company.Core.Helper;
using Mi.BO.Base.Zone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mi_Company.Pages
{
    public partial class HL_CauHoiThuongGap : BasePages
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Page.Title = "Câu hỏi thường gặp";
                Page.MetaDescription = UIHelper.GetConfigByName("MetaDescription");
                Page.MetaKeywords = UIHelper.GetConfigByName("MetaKeyword");
            }

        }
    }
}