﻿using Mi_Company.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mi_Company.Pages
{
    public partial class Mi_CRM : BasePages
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Page.Title = "Giải pháp CRM cho doanh nghiệp";
                Page.MetaDescription = UIHelper.GetConfigByName("MetaDescription");
                Page.MetaKeywords = UIHelper.GetConfigByName("MetaKeyword");
            }
        }
    }
}