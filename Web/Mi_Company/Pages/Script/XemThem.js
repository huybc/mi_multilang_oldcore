﻿$(".view-more-projet").off('click').on('click', function () {

    var type = $(this).data('type');
    var lang = $(this).data('lang');
    var zone = $(this).data('zone');
    var page = $(this).data('page');
    var size = $(this).data('size');
    var search = "";
    var currentType = $(this).data('txttype');
    console.log(type, lang, zone, page, size, search, currentType);
    R.Post({
        params: {
            type: type,
            lang: lang,
            hot: 2,
            zone_id: zone,
            search: search,
            pageIndex: page,
            pageSize: size
        },
        module: "ui-action",
        ashx: 'modulerequest.ashx',
        action: "load-trang",
        success: function (res) {
            if (res.Success) {
                var result = res.Data;
                console.log(result);
                var htm = "";

                result.forEach(function (e) {
                    //var extras = JSON.parse(e.Extras);
                    var link = "/" + lang.split('-')[0] + "/" + currentType + "/" + e.Url + "." + e.NewsId + ".htm";
                    /*
                     Tu tin viet kieu moi xem nao
                     */
                    $('.item-load-trang').last().clone().insertAfter('.item-load-trang:last');
                    var el = $('.item-load-trang').last();
                    el.find('.card-img-top').attr("src", '/uploads/' + e.Avatar + '');
                    el.find('.card-img-top').attr("alt", e.Title);
                    //el.find('.avatar').attr("alt", e.Title);
                    el.find('.load-title').text(e.Title);
                    el.find('.load-title').attr('href',link);
                    //el.find('.lich-kham').text(extras.LichKham);
                    //el.find('.xem-them').attr("href", link);
                    el.find('.card-text').html(e.Sapo);

                });
                //$('.list-ck').append(htm);
                //Xu ly hau ky
                //if (result.length > 0) {
                    $('.view-more-projet').data('page', page+1);
                    //var index = $('#xem-them-btn').data('index');
                    //$('.page-now').html(index);
                //}

            }

        }, error: function () {
            //$('#contact').RLoadingModuleComplete();
        }
    });
})

$("#view-more-blog").off('click').on('click', function () {

    var type = $(this).data('type');
    var lang = $(this).data('lang');
    var zone = $(this).data('zone');
    var page = $(this).data('page');
    var size = $(this).data('size');
    var search = "";
    var currentType = $(this).data('txttype');
    console.log(type, lang, zone, page, size, search, currentType);
    R.Post({
        params: {
            type: type,
            lang: lang,
            hot: 2,
            zone_id: zone,
            search: search,
            pageIndex: page,
            pageSize: size
        },
        module: "ui-action",
        ashx: 'modulerequest.ashx',
        action: "load-trang",
        success: function (res) {
            if (res.Success) {
                var result = res.Data;
                console.log(result);
                var htm = "";

                result.forEach(function (e) {
                    //var extras = JSON.parse(e.Extras);
                    var link = "/" + lang.split('-')[0] + "/" + currentType + "/" + e.Url + "-" + e.NewsId + ".htm";
                    /*
                     Tu tin viet kieu moi xem nao
                     */
                    $('.item-load-trang').last().clone().insertAfter('.item-load-trang:last');
                    var el = $('.item-load-trang').last();
                    el.find('.img-fluid').attr("src", '/uploads/' + e.Avatar + '');
                    el.find('.img-fluid').attr("alt", e.Title);
                    //el.find('.avatar').attr("alt", e.Title);
                    //class="link-load-trang"
                    el.find('.link-load-trang').attr('href',link);
                    el.find('.link-load-trang-1').attr('href', link);
                    el.find('.link-load-trang-1').text(e.Title);
                    //el.find('.load-title').text(e.Title);
                    //el.find('.load-title').attr('href', link);
                    //el.find('.lich-kham').text(extras.LichKham);
                    //el.find('.xem-them').attr("href", link);
                    el.find('.des').html(e.Sapo);

                });
                //$('.list-ck').append(htm);
                //Xu ly hau ky
                //if (result.length > 0) {
                $('#view-more-blog').data('page', page + 1);
                $('.danh-so-trang').text(page);
                //var index = $('#xem-them-btn').data('index');
                //$('.page-now').html(index);
                //}

            }

        }, error: function () {
            //$('#contact').RLoadingModuleComplete();
        }
    });
})