﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Mi.Master" AutoEventWireup="true" CodeBehind="Mi_Website.aspx.cs" Inherits="Mi_Company.Pages.Mi_Website" %>

<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <div id="carouselExample2" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <%//slider banner %>
            <%var slider_banner = ConfigBo.AdvGetByType(9, lang).ToList(); %>
            <%for (int i = 0; i < slider_banner.Count(); i++)
                { %>
            <div class="carousel-item <%=i==0?"active":"" %>">
                <img src="/uploads/<%=slider_banner[i].Thumb %>" class="d-block w-100" alt="<%=domainName %>">
            </div>
            <%} %>
            <a class="carousel-control-prev" href="#carouselExample2" role="button" data-slide="prev">
                <i class="fas fa-chevron-circle-left" aria-hidden="true"></i>
            </a>
            <a class="carousel-control-next" href="#carouselExample2" role="button" data-slide="next">
                <i class="fas fa-chevron-circle-right" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <section class="py-4">
        <div class="container">
            <div class="heading-ss-home mb-5">
                <div><%=UIHelper.GetConfigByName("TieuDeWebsite1") %></div>
            </div>


        </div>
        <div class="bg-FC4A35 pb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-9 col-md-10 col-12">
                        <div class="mockup-desktop mb-5">
                            <img src="/uploads/<%=UIHelper.GetConfigByName("AnhDaiDienWebsite1") %>" class="w-100" alt="<%=domainName %>" />
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <%var stemp_website = ConfigBo.AdvGetByType(10, lang); %>
                    <%foreach (var item in stemp_website)
                        { %>
                    <div class="col-xl-5 col-md-6 col-sm-6 col-12">
                        <div class="d-flex mb-4">
                            <img src="/uploads/<%=item.Thumb %>" class="img-fluid mr-3" alt="<%=domainName %>" style="height: 100%; width: auto;" />
                            <div>
                                <h6 class="font-weight-bold"><%=item.Name %></h6>
                                <div>
                                    <%=item.Content %>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%} %>
                </div>
            </div>
        </div>
    </section>

    <section class="mockup py-4 mb-5">

        <div class="container">
            <div class="heading-ss-home mb-5">
                <%var tieude_dong2 = UIHelper.GetConfigByName("TieuDeDong2"); %>
                <%var tieude_dong2_cutted = new List<string>();
                    if (tieude_dong2.Contains(';'))
                        tieude_dong2_cutted = tieude_dong2.Split(';').ToList();
                    else
                        tieude_dong2_cutted.Add(tieude_dong2);
                %>
                <%if (tieude_dong2_cutted.Count() > 1)
                    { %>
                <%foreach (var item in tieude_dong2_cutted)
                    { %>
                <div><%=item %></div>
                <%} %>
                <%}
                    else
                    { %>
                <div><%=tieude_dong2_cutted[0] %></div>
                <%} %>
            </div>
            <%var slider_3 = ConfigBo.AdvGetByType(6, lang).ToList(); %>
            <div class="text-center">
                <img src="/uploads/thumb/<%=slider_3.Count>0? slider_3[0].Thumb:"" %>" class="img-fluid img-mockup mb-3" alt="<%=domainName %>" />
            </div>
            <%var slider_3_cutted = slider_3.Skip(1);
                var slider_3_cutted_1 = slider_3_cutted.Skip(0).Take(2);
                var slider_3_cutted_2 = slider_3_cutted.Skip(2).Take(2);
            %>
            <div class="row justify-content-between">
                <%foreach (var item in slider_3_cutted_1)
                    { %>
                <div class="col-xl-4 col-md-4 col-sm-4 col-12">
                    <div class="item text-right">
                        <div class="d-flex mb-3">
                            <h5><%=item.Name %></h5>
                            <div class="ml-3">
                                <img src="/uploads/<%=item.Thumb %>" class="img-fluid" alt="<%=item.Name %>" />
                            </div>
                        </div>
                        <p>
                            <%=item.Content %>
                        </p>
                    </div>
                </div>
                <%} %>
            </div>
            <div class="row justify-content-between">
                <%foreach (var item in slider_3_cutted_2)
                    { %>
                <div class="col-xl-4 col-md-4 col-sm-4 col-12">
                    <div class="item text-right">
                        <div class="d-flex mb-3">
                            <h5><%=item.Name %></h5>
                            <div class="ml-3">
                                <img src="/uploads/<%=item.Thumb %>" class="img-fluid" alt="<%=item.Name %>" />
                            </div>
                        </div>
                        <p>
                            <%=item.Content %>
                        </p>
                    </div>
                </div>
                <%} %>
            </div>
        </div>
        <br />
    </section>

    <div class="container pt-2">
        <div class="line-pr">
        </div>
    </div>

    <section class="price-list py-4">
        <div class="container">
            <h2 class="text-center text-uppercase mb-4"><%=UIHelper.GetConfigByName("TieuDeDong3Webiste") %></h2>
            <%// Lay thong tin cua bao gia website %>
            <%var bao_gia_website = ZoneBo.GetZoneWithLanguageByType(4, lang).Where(r => r.Alias.Equals("bao-gia-website")).SingleOrDefault(); %>
            <%var z_id = 0; %>
            <%if (bao_gia_website != null)
                    z_id = bao_gia_website.Id;%>
            <%//Lay danh sach bai viet thuoc zone bao gia website %>
            <%var total_2 = 0; %>
            <%var list_gia_website = NewsBo.GetNewsDetailWithLanguage(4, lang, 2, z_id, "", 1, 3, ref total_2).OrderByDescending(r => r.isHot).ToList(); %>
            <div class="row justify-content-around">
                <div class="col-xl-4 col-md-4 col-sm-4 col-12 align-self-center">
                    <div class="item">
                        <div class="bg-top">
                        </div>
                        <div class="bg-image text-center">
                            <img src="/Themes/images/platium-ic.svg" class="img-fluid mb-3" />
                            <h4><%=list_gia_website[2].Title %></h4>
                            <p><%=list_gia_website[2].Sapo %></p>
                        </div>
                        <ul class="list mb-2">
                            <%//Convert list body
                                var l_3 = UIHelper.CutContentByMinusCharactor(UIHelper.ClearHtmlTag(list_gia_website[2].Body));
                            %>
                            <%foreach (var item in l_3)
                            { %>
                            <li><%=item %>
                            </li>
                            <%} %>
                            <%--<%=list_gia_website[2].Body %>--%>
                        </ul>
                        <div class="line mb-3">
                        </div>
                        <div class="price mb-3">
                            <%//Lay gia %>
                            <%var gia_3 = new BacSiEntity();%>
                            <%if (!string.IsNullOrEmpty(list_gia_website[2].Extras))
                                { %>
                            <%gia_3 = Newtonsoft.Json.JsonConvert.DeserializeObject<BacSiEntity>(list_gia_website[2].Extras); %>
                            <%} %>
                            <div><%=gia_3.HocVi %></div>
                            VNĐ
                        </div>
                        <div class="b-footer">
                            <a href="javascript:void(0)" class="btn btn-b-footer w-100 tu-van-ngay" data-id="5193">Tư vấn ngay</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 col-sm-4 col-12 align-self-center">
                    <div class="item">
                        <div class="bg-top" style="background: #F0545D;">
                        </div>
                        <div class="bg-image text-center py-md-5" style="background: #FE8837;">
                            <img src="/Themes/images/gold-ic.svg" class="img-fluid mb-3" />
                            <h4><%=list_gia_website[0].Title %></h4>
                            <p><%=list_gia_website[0].Sapo %></p>
                        </div>
                        <ul class="list mb-3">
                            <%var l_1 = UIHelper.CutContentByMinusCharactor(UIHelper.ClearHtmlTag(list_gia_website[0].Body)); %>
                            <%foreach (var item in l_1)
                                {  %>
                            <li><%=item %>
                            </li>
                            <%} %>
                            <%--<%=list_gia_website[0].Body %>--%>
                        </ul>
                        <div class="line mb-4">
                        </div>
                        <div class="price mb-3">
                            <%//Lay gia %>
                            <%var gia_1 = new BacSiEntity();%>
                            <%if (!string.IsNullOrEmpty(list_gia_website[0].Extras))
                                { %>
                            <%gia_1 = Newtonsoft.Json.JsonConvert.DeserializeObject<BacSiEntity>(list_gia_website[0].Extras); %>
                            <%} %>
                            <div><%=gia_1.HocVi %></div>
                            VNĐ
                        </div>
                        <div class="b-footer">
                            <%--<a href="javascript:void(0)" class="btn btn-b-footer w-100 tu-van-ngay" data-id="5193">Tư vấn ngay</a>--%>
                            <a href="javascript:void(0)" class="btn btn-b-footer w-100 tu-van-ngay" data-id="5193" style="background: #F0545D;">Tư vấn ngay</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 col-sm-4 col-12 align-self-center">
                    <div class="item">
                        <div class="bg-top">
                        </div>
                        <div class="bg-image text-center">
                            <img src="/Themes/images/platium-ic.svg" class="img-fluid mb-3" />
                            <h4><%=list_gia_website[1].Title %></h4>
                            <p><%=list_gia_website[1].Sapo %></p>
                        </div>
                        <ul class="list mb-2">
                            <%//Convert list body
                                var l_2 = UIHelper.CutContentByMinusCharactor(UIHelper.ClearHtmlTag(list_gia_website[1].Body));
                            %>
                            <%foreach (var item in l_2)
                            { %>
                            <li><%=item %>
                            </li>
                            <%} %>
                            <%-- <%=list_gia_website[1].Body %>--%>
                        </ul>
                        <div class="line mb-3">
                        </div>
                        <div class="price mb-3">
                            <%//Lay gia %>
                            <%var gia_2 = new BacSiEntity();%>
                            <%if (!string.IsNullOrEmpty(list_gia_website[1].Extras))
                                { %>
                            <%gia_2 = Newtonsoft.Json.JsonConvert.DeserializeObject<BacSiEntity>(list_gia_website[1].Extras); %>
                            <%} %>
                            <div><%=gia_2.HocVi %></div>
                            VNĐ
                        </div>
                        <div class="b-footer">
                            <a href="javascript:void(0)" class="btn btn-b-footer w-100 tu-van-ngay" data-id="5193">Tư vấn ngay</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="line-pr">
        </div>
    </div>

    <section class="projects py-4">
        <div class="container">
            <div class="heading-ss-home">
                <%=UIHelper.GetConfigByName("TieuDeDong3") %>
            </div>
            <div class="row <%--justify-content-center--%>">
                <%var total_1 = 0;
                %>
                <%var list_6_du_an = NewsBo.GetNewsDetailWithLanguage(1, lang, 2, 5178, "", 1, 6, ref total_1); %>
                <%foreach (var item in list_6_du_an)
                    { %>
                <%var link_target = string.Format("/{0}/du-an/{1}-{2}.htm", currentLanguage, item.Url, item.NewsId);//Cho nay set link sau %>
                <div class="col-xl-4 col-lg-4 col-sm-6 col-12 item-load-trang pull-left">
                    <div class="card mi-card mb-4">
                        <div class="image">
                            <a href="<%=link_target %>" title="" class="">
                                <img src="/uploads/<%=item.Avatar %>" class="card-img-top" alt="<%=item.Title %>"></a>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title font-weight-bold">
                                <a href="<%=link_target %>" title="" class="load-title"><%=item.Title %>
                                </a>
                            </h5>
                            <p class="card-text">
                                <%=UIHelper.TrimByWord(UIHelper.ClearHtmlTag(item.Sapo),20,"...") %>
                            </p>

                        </div>
                    </div>
                </div>
                <%} %>
            </div>
            <div class="row justify-content-center">
                <div>
                    <a href="javascript:void(0)" data-lang="<%=lang %>" data-zone="5178" data-page="2" data-size="6" data-type="1" data-txttype="du-an" role="button" class="btn view-more-projet justify-content-center">Xem thêm các dự án khác
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="try py-5">
        <div class="container text-center">
            <h4><%=UIHelper.GetConfigByName("TieuDeDong4Webiste") %></h4>
            <p><%=UIHelper.GetConfigByName("NoiDungeDong4Webiste") %></p>
            <a href="/<%=currentLanguage %>/lien-he" class=" btn btn-F0545D">LIÊN HỆ NGAY</a>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <script src="/Pages/Script/XemThem.js"></script>
    <script src="/Pages/Script/LienHe.js"></script>
</asp:Content>
