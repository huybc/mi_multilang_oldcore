﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/MiTourKit.Master" AutoEventWireup="true" CodeBehind="Mi_Tourkit.aspx.cs" Inherits="Mi_Company.Pages.Mi_Tourkit" %>
<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <section class="container">
        <%var khoi_content_1 = ConfigBo.AdvGetByType(51, lang).ToList(); %>
        <%for(int i = 0; i < khoi_content_1.Count(); i++){ %> 
            <%var sole = "flex-md-row-reverse";  %>
            <div class="row justify-content-between py-4 <%=i==1?sole:"" %>">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12 align-self-center ">
                <h4 class="color-F65D2D font-weight-bold mb-3">
                    <%=khoi_content_1[i].Name %>
                </h4>
                <%=khoi_content_1[i].Content %>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12 col-12">
                <img src="/uploads/<%=khoi_content_1[i].Thumb %>" alt="<%=domainName %>" class="img-fluid wow fadeInRight slow" />
            </div>
        </div>    
        <%} %>
    </section>

    <section class="tourkit-support py-4 py-lg-5" id="tourkit-la-gi">
        <div class="container">
            <%var khoi_content_2 = ConfigBo.AdvGetByType(52, lang);
                var khoi_content_2_tieude = khoi_content_2.FirstOrDefault();%>
            <h4 class="text-center text-uppercase font-weight-bold mb-4"><%=khoi_content_2_tieude!=null?khoi_content_2_tieude.Name:"" %></h4>
            <div class="row py-4">
                <%var khoi_content_2_nd = khoi_content_2.Skip(1); %>
                <%if(khoi_content_2_nd!=null){ %> 
                    <%foreach (var item in khoi_content_2_nd)
                        { %> 
                    <div class="col-lg-3 col-sm-6 col-12">
                    <div class="item  mb-4  ">
                        <div class="image text-center mb-3">
                            <img src="/uploads/<%=item.Thumb %>" class="img-fluid" alt="<%=item.Name %>"/>
                        </div>
                        <div class="small text-uppercase text-center">
                            <%=item.Content %>
                        </div>
                    </div>
                </div>
                <%} %>
                <%} %>
            </div>
        </div>
    </section>

    <section class="tourkit-video py-5" id="tinh-nang">
        <div class="container">
            <%var khoi_video = ConfigBo.AdvGetByType(53, lang); %>
            <%var khoi_video_td = khoi_video.FirstOrDefault(); %>
            <h4 class="text-center color-F65D2D text-uppercase font-weight-bold mb-4 mb-lg-5">
                <%=khoi_video_td!=null?khoi_video_td.Name:"" %>
            </h4>
            <%var khoi_video_nd = khoi_video.Skip(1); %>
            <div class="swiper-container mb-4">
                <div class="swiper-wrapper">
                    <%if(khoi_video_nd!=null){ %> 
                        <%foreach (var item in khoi_video_nd)
                            { %> 
                        <div class="swiper-slide">
                        <iframe width="100%" height="315" src="<%=UIHelper.ConvertLinkYoutubeVideo(item.Content) %>"
                            frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                    </div>
                    <%} %>
                    <%} %>

                </div>
                <div class="swiper-button-prev">
                    <i class="fas fa-caret-left"></i>
                </div>
                <div class="swiper-button-next">
                    <i class="fas fa-caret-right"></i>
                </div>
            </div>

<%--            <h4 class="text-center color-F65D2D text-uppercase font-weight-bold mb-2">
                Video hướng dẫn sử dụng
            </h4>
            <div class="text-center">
                Lorem Ipsum is simply dummy text of the<br/>
                printing and typesetting industry
            </div>--%>
        </div>
    </section>

    <section class="tourkit-price py-4 py-md-5" id="bang-gia">
        <div class="container">
            <h4 class="text-center color-F65D2D text-uppercase font-weight-bold mb-4 mb-lg-5">
                CHÍNH SÁCH GIÁ
            </h4>
            <div class="row justify-content-between">
                <%var goi_1 = ConfigBo.AdvGetByType(54,lang).ToList(); %>
                <%var link_tar = "javascript:void(0)"; %>
                <%if(goi_1!=null && goi_1.Count >= 6){ %> 
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 px-lg-4  align-self-lg-center">
                    <div class="item">
                        <div class="bg-top">
                            <%=goi_1[0].Name %>
                        </div>
                        <div class="block-price">
                            <div class="old-price">
                                <label><%=goi_1[1].Name %></label>
                                <div>
                                    <%=UIHelper.ClearHtmlTag(goi_1[1].Content) %>
                                </div>
                            </div>
                            <div class="new-price">
                                <label><%=goi_1[2].Name %></label>
                                <div>
                                    <%=UIHelper.ClearHtmlTag(goi_1[2].Content) %>
                                </div>
                            </div>
                        </div>
                        <div class="content px-4">
                            <div class="font-weight-bold text-uppercase mb-3">
                                <%=goi_1[3].Name %>
                            </div>
                            <div class="mb-3">
                                <%=goi_1[3].Content %>
                            </div>
                            <div class="line mb-3">

                            </div>
                            <div class="font-weight-bold font-italic mb-3">
                                <%=goi_1[4].Content %>
                            </div>
                            <div class="line mb-3">

                            </div>
                            <div class="font-weight-bold mb-4 color-FF0000">
                                <div class="text-uppercase"><%=goi_1[5].Name %></div>
                                <%=goi_1[5].Content %>
                            </div>
                        </div>
                        <div class="b-footer">
                            <a href="<%=link_tar %>" class="btn btn-b-footer btn-b-footer-2 w-100 tu-van-ngay"> Dùng thử Miễn phí</a>
                        </div>
                    </div>
                </div>
                <%} %>
                <%var goi_2 = ConfigBo.AdvGetByType(55, lang).ToList(); %>
                <%if (goi_2 != null)
                    { %> 
                <div class="col-lg-4 col-md-4 col-sm-12 col-12   align-self-lg-center">
                    <div class="item">
                        <div class="bg-top">
                            <%=goi_2[0].Name %>
                        </div>
                        <div class="block-price">
                            <div class="old-price">
                                <label><%=goi_2[1].Name %></label>
                                <div>
                                    <%=UIHelper.ClearHtmlTag(goi_2[1].Content) %>
                                </div>
                            </div>
                            <div class="new-price">
                                <label><%=goi_2[2].Name %></label>
                                <div>
                                    <%=UIHelper.ClearHtmlTag(goi_2[2].Content) %>
                                </div>
                            </div>
                        </div>
                        <div class="content px-4">
                            <div class="font-weight-bold text-uppercase mb-3">
                                <%=goi_2[3].Name %>
                            </div>
                            <div class="mb-3">
                                <%=goi_2[3].Content %>
                            </div>
                            <div class="line mb-3">

                            </div>
                            <div class="font-weight-bold font-italic mb-3">
                                <%=goi_2[4].Content %>
                            </div>
                            <div class="line mb-3">

                            </div>
                            <div class="font-weight-bold font-italic mb-3">
                                <%=goi_2[5].Content %>
                            </div>
                            <div class="line mb-3">

                            </div>
                            <div class="font-weight-bold mb-4 color-FF0000">
                                <div class="text-uppercase mb-3"><%=goi_2[6].Name %></div>
                                <div class="font-italic color-4AB139 wow heartBeat  infinite">
                                    <%=goi_2[6].Content %> <br />
                                </div>
                            </div>
                        </div>
                        <div class="b-footer">
                            <a href="<%=link_tar %>" class="btn btn-b-footer btn-b-footer-2 w-100 tu-van-ngay"> Dùng thử Miễn phí</a>
                        </div>
                    </div>
                </div>
                <%} %>
                <%var goi_3 = ConfigBo.AdvGetByType(56, lang).ToList(); %>
                <%if (goi_3 != null) { %> 
                <div class="col-lg-4 col-md-4 col-sm-12 col-12  px-lg-4  align-self-lg-center">

                    <div class="item">
                        <div class="bg-top">
                            <%=goi_3[0].Name %>
                        </div>
                        <div class="block-price">
                            <div class="old-price">
                                <label><%=goi_3[1].Name %></label>
                                <div>
                                    <%=UIHelper.ClearHtmlTag(goi_3[1].Content) %>
                                </div>
                            </div>
                            <div class="new-price">
                                <label><%=goi_3[2].Content %></label>
                                <div>
                                    <%=UIHelper.ClearHtmlTag(goi_3[2].Content) %>
                                </div>
                            </div>
                        </div>
                        <div class="content px-4">
                            <div class="font-weight-bold text-uppercase mb-3">
                                <%=goi_3[3].Name %>
                            </div>
                            <div class="mb-3">
                                <%=goi_3[3].Content %>
                            </div>
                            <div class="line mb-3">

                            </div>
                            <div class="font-weight-bold font-italic mb-3">
                                <%=goi_3[4].Content %>
                            </div>
                            <div class="line mb-3">

                            </div>
                            <div class="font-weight-bold font-italic mb-3">
                                <%=goi_3[5].Content %>
                            </div>



                        </div>
                        <div class="b-footer">
                            <a href="<%=link_tar %>" class="btn btn-b-footer btn-b-footer-2 w-100 tu-van-ngay"> Dùng thử Miễn phí</a>
                        </div>
                    </div>
                </div>
                <%} %>
                
            </div>
        </div>
    </section>

    <section class="table-baogia py-4 py-lg-5">
        <div class="container">
            <%//Get zone báo giá tourkit %>
            <%var zone_bao_gia_tourkit = ZoneBo.GetZoneWithLanguageByType(4, lang).Where(r => r.ParentId > 0).ToList(); %>
            <h4 class="text-center color-F65D2D text-uppercase font-weight-bold mb-4 mb-lg-5">
                <%var zone_1 = zone_bao_gia_tourkit[0]; %>
                <%var zone_2 = zone_bao_gia_tourkit[1]; %>
                <%=zone_1.lang_name %>
            </h4>
            <div class="table-responsive mi-table wow fadeInUp slow">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Gói dịch vụ</th>
                            <th>Đơn giá (VNĐ)</th>
                            <th>Đơn vị tính</th>
                            <th>Mô tả chi tiết</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%//Lay danh sach bai viet tai zone 1 %>
                        <%var total_1 = 0; %>
                        <%var tin_zone_1 = NewsBo.GetNewsDetailWithLanguage(4, lang, 2, zone_1.Id, "", 1, 100, ref total_1); %>
                        <%foreach (var item in tin_zone_1)
                            { %> 
                            <tr>
                                <%--<%var gia = new BacSiEntity(); %>--%>
                                <%var gia_detail = Newtonsoft.Json.JsonConvert.DeserializeObject<BacSiEntity>(item.Extras); %>
                                <%var list_gia = UIHelper.CutContentByMinusCharactor(gia_detail.HocVi);  %>
                            <th class="text-center"><%=item.Title %></th>
                            <td class="text-center">
                                <%if (list_gia.Count > 1)
                                    { %> 
                                    <div class="font-italic color-4AB139 font-weight-bold">
                                    <%=list_gia[0] %>
                                </div>
                                đến
                                <div class="font-italic color-4AB139 font-weight-bold">
                                    <%=list_gia[1] %>
                                </div>
                                <%} %>
                                <%if (list_gia.Count <= 1)
                                    { %> 
                                    <div class="font-italic color-4AB139 font-weight-bold">
                                    <%=list_gia[0] %>
                                </div>
                                <%} %>
                                
                            </td>
                            <td class="text-center">
                                Khóa
                            </td>
                            <td>
                                <%=item.Body %>
                            </td>
                        </tr>

                        <%} %>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <section class="table-baogia py-4 py-lg-5">
        <div class="container">
            <h4 class="text-center color-F65D2D text-uppercase font-weight-bold mb-4 mb-lg-5">
                <%=zone_2.lang_name %>
            </h4>
            <div class="table-responsive mi-table wow fadeInUp slow">
                <table class="table table-bordered  ">
                    <thead>
                        <tr>
                            <th>Gói dịch vụ</th>
                            <th>Đơn giá (VNĐ)</th>
                            <th>Đơn vị tính</th>
                            <th>Mô tả chi tiết</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%//Lay danh sach bai viet zone 2 %>
                        <%var total_2 = 0; %>
                        <%var list_tin_2 = NewsBo.GetNewsDetailWithLanguage(4, lang, 2, zone_2.Id, "", 1, 100, ref total_2).OrderBy(r=>r.CreatedDate); %>
                        <%foreach(var item in list_tin_2){ %> 
                        <%var gia_detail = Newtonsoft.Json.JsonConvert.DeserializeObject<BacSiEntity>(item.Extras); %>
                            <tr>
                            <th class="text-center"><%=item.Title %></th>
                            <td class="text-center">
                                <%=gia_detail.HocVi %>
                            </td>
                            <td class="text-center">

                            </td>
                            <td>
                                <%=item.Body %>
                            </td>
                        </tr>
                        <%} %>


                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <%//Lay footer %>
    <%var page_footer = ConfigBo.AdvGetByType(57, lang).ToList(); %>
    <%if (page_footer.Count >= 2)
        { %> 
    <%var page_footer_1 = page_footer[0]; %>
    <%var page_footer_2 = page_footer[1]; %>
    <section class="bg-form-tu-van py-5 "
        style="background: url(/Themes/images/silver-imac-pink.jpg) no-repeat center;background-size: cover;" id="ho-tro">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-xl-7 col-lg-6 col-md-7 col-sm-6 col-12 align-self-center">
                    <h1 class="mb-3">
                        <%=page_footer_1.Name %>
                    </h1>
                    <p>
                        <%=page_footer_1.Content %>
                    </p>
                </div>
                <div class="col-xl-4 col-lg-5 col-md-4 col-sm-6 col-12">
                    <div class="form-tv bg-fff ">
                        <h5>
                            <%=page_footer_2.Name %>
                        </h5>
                        <p>
                            <%=page_footer_2.Content %>
                        </p>
                        <div class="form-group">
                            <input type="text" class="form-control" id="txtCompany" placeholder="Tên doanh nghiệp của ban?">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="txtName" placeholder="Họ và tên*">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="txtEmail" placeholder="Email*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="txtPhone" placeholder="Điện thoại*">
                        </div>
                        <div>
                            <button type="button" class="btn btn-submit" id="btnSubmit" data-type="tourkit">GỬI THÔNG TIN TƯ VẤN</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <%} %>
    
    <section class="contact-home py-3 " style="background: #F65D2D;">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-4 col-sm-4 col-12">
                    <div class="d-flex py-3 justify-content-center">
                        <div class="image align-self-center">
                            <img src="/Themes/images/hotline-icon-contact.svg" class="img-fluid">
                        </div>
                        <div class="px-3 align-self-center">
                            HotLine: 0383.202.404
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 col-sm-4 col-12">
                    <div class="d-flex py-3 justify-content-center">
                        <div class="image align-self-center">
                            <img src="/Themes/images/hotline-icon-contact.svg" class="img-fluid">
                        </div>
                        <div class="px-3 align-self-center">
                            Phòng kinh doanh : 0902.169.757
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 col-sm-4 col-12">
                    <div class="d-flex py-3 justify-content-center">
                        <div class="image align-self-center">
                            <img src="/Themes/images/email-icon-contact.svg" class="img-fluid">
                        </div>
                        <div class="px-3 align-self-center">
                            Email : hotro@migroup.asia
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <script src="/Pages/Script/XemThem.js"></script>
    <script src="/Pages/Script/LienHe.js"></script>
</asp:Content>
