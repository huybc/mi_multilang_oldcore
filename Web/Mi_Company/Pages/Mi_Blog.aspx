﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/MiWBanner.Master" AutoEventWireup="true" CodeBehind="Mi_Blog.aspx.cs" Inherits="Mi_Company.Pages.Mi_Blog" %>

<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        string domainNameFull = HttpContext.Current.Request.Url.LocalPath;
    %>
    <%var total_list_tin_hot = 0; %>
    <%var list_4_tin_hot = NewsBo.GetNewsDetailWithLanguage(3, lang, 1, 0, "", 1, 7, ref total_list_tin_hot).ToList(); %>
    <%var top_1 = list_4_tin_hot.Take(1).SingleOrDefault(); %>
    <%var top_2 = list_4_tin_hot.Skip(1).Take(2).ToList(); %>
    <section class="blog-ss-1">
        <div class="container">
            <div class="row">
                
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="item mb-3">
                        <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByMiProject(3) %>/<%=top_1.Url %>-<%=top_1.NewsId %>.htm" title="">
                            <img src="/uploads/<%=top_1.Avatar %>" class="img-fluid" alt="<%=top_1.Title %>" /></a>
                        <h2 class="title">
                            <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByMiProject(3) %>/<%=top_1.Url %>-<%=top_1.NewsId %>.htm" title=""><%=top_1.Title %></a>

                        </h2>
                    </div>
                </div>
                
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <%foreach(var item in top_2){ %> 
                    <%var link_target = string.Format("/{0}/{1}/{2}-{3}.htm", currentLanguage, UIHelper.GetTypeUrlByMiProject(3), item.Url, item.NewsId); %>
                        <div class="item mb-3 item-right">
                        <a href="<%=link_target %>" title="">
                            <img src="/uploads/<%=item.Avatar %>" class="img-fluid" alt="<%=item.Title %>" /></a>
                        <h2 class="title">
                            <a href="<%=link_target %>" title=""><%=item.Title %></a>

                        </h2>
                    </div>
                    <%} %>
                   
                </div>

            </div>
        </div>
    </section>

    <section class="media">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="heading text-uppercase">
                        <a href="javascript:void(0)" title="">Media</a>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <%var total_3 = 0; %>
                <%var list_3_video = NewsBo.GetNewsDetailWithLanguage(5, lang, 2, 0, "", 1, 3, ref total_3); %>
                <%foreach (var item in list_3_video)
                    { %>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="item">
                        <iframe width="100%" height="200" src="<%=UIHelper.ConvertLinkYoutubeVideo(item.Body) %>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <div class="text">
                            <h3 class="title">
                                <a href="javascript:void(0)" title=""><%=item.Title %></a>
                            </h3>
                            <div class="date">
                                <span class="acc">BienTapVien</span>
                                <span class=""><%=item.CreatedDate.ToString("dd/MM/yyyy") %></span>
                            </div>
                        </div>
                    </div>
                </div>
                <%} %>
            </div>
        </div>
    </section>

    <section class="py-5">
        <div class="container">
            <div class="row">
                <%//Lay thong tin zone blog %>
                <%var list_zone_total = ZoneBo.GetZoneWithLanguageByType(3, lang).ToList(); %>
                <%var zone_parent = list_zone_total.Where(r => r.ParentId == 0).FirstOrDefault(); %>
                <%var list_zone = list_zone_total.Where(r => r.ParentId > 0).ToList(); %>
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12">
                    <%//list 1 %>
                    <div class="blog-ss-2">
                        <div class="heading ">
                            <a href="/<%=currentLanguage %>/blog/<%=list_zone[0].Alias %>" title=""><%=list_zone[0].lang_name %></a>
                        </div>
                        <div class="wp">
                            <%//Lay danh sach bai viet %>
                            <%var total_1 = 0; %>
                            <%var list_tin_1 = NewsBo.GetNewsDetailWithLanguage(3, lang, 2, list_zone[0].Id, "", 1, 5, ref total_1).ToList(); %>
                            <div class="row">
                                <%//Lay thang dau tien  %>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <%var tin_1_to = list_tin_1.FirstOrDefault(); %>
                                    <%if (tin_1_to != null)
                                        { %>
                                    <%var link = string.Format("/{0}/{1}/{2}-{3}.htm", currentLanguage, UIHelper.GetTypeUrlByMiProject(3), tin_1_to.Url, tin_1_to.NewsId); %>
                                    <div class="item-left">
                                        <div class="image">
                                            <a href="<%=link %>" title="">
                                                <img src="/uploads/<%=tin_1_to.Avatar %>" class="img-fluid" alt="<% =tin_1_to.Title  %>" /></a>
                                        </div>
                                        <h2 class="title">
                                            <a href="<%=link %>" title=""><%=tin_1_to.Title %></a>
                                        </h2>
                                        <div class="detail">
                                            <%=UIHelper.ClearHtmlTag(tin_1_to.Sapo) %>
                                        </div>

                                    </div>
                                    <%} %>
                                </div>
                                <%//Lay thang tiep theo %>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <%var tin_1_nho = list_tin_1.Skip(1).Take(4); %>
                                    <%foreach (var item in tin_1_nho)
                                        { %>
                                    <%var link = string.Format("/{0}/{1}/{2}-{3}.htm", currentLanguage, UIHelper.GetTypeUrlByMiProject(3), item.Url, item.NewsId); %>
                                    <div class="item-right d-flex">
                                        <div class="image">
                                            <a href="<%=link %>" title="">
                                                <img src="/uploads/<%=item.Avatar %>" class="img-fluid" alt="<%=item.Title %>" /></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="title">
                                                <a href="<%=link %>" title=""><%=item.Title %></a>
                                            </h2>
                                            <div class="time">
                                                <%=item.CreatedDate.ToString("dd/MM/yyyy") %>
                                            </div>
                                        </div>

                                    </div>
                                    <%} %>

                                    <%--<div class="item-right d-flex">
                                        <div class="image">
                                            <a href="" title=""><img src="/Themes/images/change/blog-1.svg" class="img-fluid" alt="" /></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="title">
                                                <a href="" title="">Cập nhật giá tham quan Nha Trang 2019 tại 15 điểm
                                                    chụp ảnh “vạn </a>
                                            </h2>
                                            <div class="time">
                                                2 giờ trước
                                            </div>
                                        </div>

                                    </div>
                                    <div class="item-right d-flex">
                                        <div class="image">
                                            <a href="" title=""><img src="/Themes/images/change/blog-1.svg" class="img-fluid" alt="" /></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="title">
                                                <a href="" title="">Check-in quán The Ylang Gardenista Coffee view đẹp
                                                    quên lối về ở Hà Nội</a>
                                            </h2>
                                            <div class="time">
                                                2 giờ trước
                                            </div>
                                        </div>

                                    </div>
                                    <div class="item-right d-flex">
                                        <div class="image">
                                            <a href="" title=""><img src="/Themes/images/change/blog-1.svg" class="img-fluid" alt="" /></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="title">
                                                <a href="" title="">Check-in quán The Ylang Gardenista Coffee view đẹp
                                                    quên lối về ở Hà Nội</a>
                                            </h2>
                                            <div class="time">
                                                2 giờ trước
                                            </div>
                                        </div>

                                    </div>--%>
                                </div>
                            </div>

                        </div>
                    </div>
                    <%//end_list_1 %>
                    <%//list 2 %>
                    <div class="blog-ss-2 mb-4">
                        <div class="heading">
                           <a href="/<%=currentLanguage %>/blog/<%=list_zone[1].Alias %>" title=""><%=list_zone[1].lang_name %></a>
                        </div>
                        <div class="wp mb-3">
                            <div class="row">
                                <%var total_list_tin_2 = 0; %>
                                <%var list_tin_2 = NewsBo.GetNewsDetailWithLanguage(3, lang, 2, list_zone[1].Id, "", 1, 6, ref total_list_tin_2); %>
                                <%var list_tin_2_cut_1 = list_tin_2.Skip(0).Take(2); %>
                                <%foreach (var item in list_tin_2_cut_1)
                                    { %>
                                <%var link_target = string.Format("/{0}/{1}/{2}-{3}.htm", currentLanguage, UIHelper.GetTypeUrlByMiProject(3), item.Url, item.NewsId); %>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="item-left">
                                        <div class="image">
                                            <a href="<%=link_target %>" title="">
                                                <img src="/uploads/<%=item.Avatar %>" class="img-fluid" alt="<%=item.Title %>" /></a>
                                        </div>
                                        <h2 class="title">
                                            <a href="<%=link_target %>" title=""><%=item.Title %></a>
                                        </h2>

                                    </div>
                                </div>
                                <%} %>
                            </div>

                        </div>
                        <%var list_tin_2_cut_2 = list_tin_2.Skip(2).Take(4); %>

                        <div class="slide-blogs px-3">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <div class="swiper-container">
                                        <!-- Additional required wrapper -->
                                        <div class="swiper-wrapper">
                                            <!-- Slides -->
                                            <%foreach (var item in list_tin_2_cut_2)
                                                { %>
                                            <%var link_target = string.Format("/{0}/{1}/{2}-{3}.htm", currentLanguage, UIHelper.GetTypeUrlByMiProject(3), item.Url, item.NewsId); %>
                                            <div class="swiper-slide">
                                                <div class="item-slide">
                                                    <div class="image">
                                                        <a href="<%=link_target %>" title="">
                                                            <img src="/uploads/thumb/<%=item.Avatar %>" alt="<%=item.Title %>" class="w-100 h-100" /></a>
                                                    </div>
                                                    <h6 class="title">
                                                        <a href="<%=link_target %>" title=""><%=item.Title %>
                                                        </a>
                                                    </h6>
                                                </div>
                                            </div>
                                            <%} %>
                                        </div>

                                        <!-- If we need navigation buttons -->

                                    </div>
                                    <div class="swiper-button-prev"><i class="fas fa-chevron-left"></i></div>
                                    <div class="swiper-button-next"><i class="fas fa-chevron-right"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%//end list 2 %>
                    <div class="row">
                        <%//list 3 %>
                        <%var total_list_tin_3 = 0; %>
                        <%var list_tin_3 = NewsBo.GetNewsDetailWithLanguage(3, lang, 2, list_zone[2].Id, "", 1, 1, ref total_list_tin_3).FirstOrDefault(); %>
                        <div class="col-lg-12 col-sm-12 col-12">
                            <div class="blog-ss-right mb-3">
                                <div class="heading-2">
                                    <a href="/<%=currentLanguage %>/blog/<%=list_zone[2].Alias %>" title=""><%=list_zone[2].lang_name %></a>
                                </div>
                                <%//design chi co 1 tin %>
                                <%if (list_tin_3 != null)
                                    { %>
                                <%var link_target = string.Format("/{0}/{1}/{2}-{3}.htm", currentLanguage, UIHelper.GetTypeUrlByMiProject(3), list_tin_3.Url, list_tin_3.NewsId); %>
                                <div class="item-left mb-3">
                                    <div class="image">
                                        <a href="<%=link_target %>" title="">
                                            <img src="/uploads/<%=list_tin_3.Avatar %>" class="img-fluid" alt="<%=list_tin_3.Title %>" /></a>
                                    </div>
                                    <h2 class="title">
                                        <a href="<%=link_target %>" title=""><%=list_tin_3.Title %></a>
                                    </h2>
                                    <div class="detail">
                                        <%=UIHelper.TrimByWord(UIHelper.ClearHtmlTag(list_tin_3.Sapo),20,"...") %>
                                    </div>

                                </div>
                                <%} %>
                            </div>
                        </div>
                        <%//end list 3 %>
                        <%//list tin 4,5 %>
                        <%var list_zone_con_lai = list_zone.Skip(3).Take(2).ToList(); %>
                        <%
                            //foreach (var zone in list_zone_con_lai)
                            for (int a = 0; a < list_zone_con_lai.Count; a++)
                            { %>
                        <%var zone = list_zone_con_lai[a]; %>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="blog-ss-right mb-3">
                                <div class="heading-2">
                                    <a href="/<%=currentLanguage %>/blog/<%=zone.Alias %>" title=""><%=zone.lang_name %></a>
                                </div>
                                <%//Lay danh sach tin %>
                                <%var total_con_lai = 0; %>
                                <%var list_tin_con_lai = NewsBo.GetNewsDetailWithLanguage(3, lang, 2, zone.Id, "", 1, 5, ref total_con_lai).ToList(); %>
                                <%for (int i = 0; i < list_tin_con_lai.Count(); i++)
                                    { %>
                                <%var link_target = string.Format("/{0}/{1}/{2}-{3}.htm", currentLanguage, UIHelper.GetTypeUrlByMiProject(3), list_tin_con_lai[i].Url, list_tin_con_lai[i].NewsId); %>
                                <%if (i == 0)
                                    { %>
                                <div class="item-left mb-3">
                                    <div class="image">
                                        <a href="<%=link_target %>" title="">
                                            <img src="/uploads/<%=list_tin_con_lai[i].Avatar %>" class="img-fluid" alt="<%=list_tin_con_lai[i].Title %>" /></a>
                                    </div>
                                    <h2 class="title">
                                        <a href="<%=link_target %>" title=""><%=list_tin_con_lai[i].Title %></a>
                                    </h2>
                                    <div class="detail">
                                        <%=UIHelper.TrimByWord(UIHelper.ClearHtmlTag(list_tin_con_lai[i].Sapo),20,"...") %>
                                    </div>

                                </div>
                                <%}
                                    else
                                    { %>
                                <div class="item-right d-flex">
                                    <div class="image">
                                        <a href="<%=link_target %>" title="">
                                            <img src="/uploads/<%=list_tin_con_lai[i].Avatar %>" class="img-fluid" alt="<%=list_tin_con_lai[i].Title %>" /></a>
                                    </div>
                                    <div class="text">
                                        <h2 class="title">
                                            <a href="<%=link_target %>" title=""><%=list_tin_con_lai[i].Title %></a>
                                        </h2>
                                        <div class="time">
                                            <%=list_tin_con_lai[i].CreatedDate.ToString("dd/MM/yyyy") %>
                                        </div>
                                    </div>

                                </div>
                                <%} %>
                                <%} %>
                            </div>
                        </div>
                        <%} %>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <%//Lay danh sach 7 tin hot %>
                    <div class="blog-ss-right mb-3">
                        <div class="heading">
                            <a title="" href="javascript:void(0)">Tin đọc nhiều</a>
                        </div>
                        <%//Lấy danh sách tin hot %>
                        <%--<%var total_list_tin_hot = 0; %>
                        <%var list_4_tin_hot = NewsBo.GetNewsDetailWithLanguage(3, lang, 1, 0, "", 1, 7, ref total_list_tin_hot).ToList(); %>--%>
                        <%for (int i = 0; i < list_4_tin_hot.Count(); i++)
                            {  %>
                        <%if (i == 0)
                            { %>
                        <div class="item-left mb-3">
                            <%var link_target = string.Format("/{0}/{1}/{2}-{3}.htm", currentLanguage, UIHelper.GetTypeUrlByMiProject(3), list_4_tin_hot[i].Url, list_4_tin_hot[i].NewsId); %>
                            <div class="image">
                                <a href="<%=link_target %>" title="">
                                    <img src="/uploads/<%=list_4_tin_hot[i].Avatar %>" class="img-fluid" alt="<%=list_4_tin_hot[i].Title %>" /></a>
                            </div>
                            <h2 class="title">
                                <a href="<%=link_target %>" title=""><%=list_4_tin_hot[i].Title %></a>
                            </h2>
                            <div class="detail">
                                <%=UIHelper.TrimByWord(UIHelper.ClearHtmlTag(list_4_tin_hot[i].Sapo), 20, "...") %>
                            </div>

                        </div>
                        <%}
                            else
                            {%>
                        <%var link_target = string.Format("/{0}/{1}/{2}-{3}.htm", currentLanguage, UIHelper.GetTypeUrlByMiProject(3), list_4_tin_hot[i].Url, list_4_tin_hot[i].NewsId); %>
                        <div class="item-right d-flex">
                            <div class="image">
                                <a href="<%=link_target %>" title="">
                                    <img src="/uploads/thumb/<%=list_4_tin_hot[i].Avatar %>" class="img-fluid" alt="<%=list_4_tin_hot[i].Title %>" /></a>
                            </div>
                            <div class="text">
                                <h2 class="title">
                                    <a href="<%=link_target %>" title=""><%=list_4_tin_hot[i].Title %></a>
                                </h2>
                                <div class="time">
                                    <%=list_4_tin_hot[i].CreatedDate.ToString("dd/MM/yyyy") %>
                                </div>
                            </div>

                        </div>
                        <%} %>

                        <%} %>
                    </div>
                    <%//end 7 tin hot %>
                    <%//lay cau hinh ads %>
                    <div class="ads">
                        <a href="javascript:void(0)" title="">
                            <img src="/uploads/<%=UIHelper.GetConfigByName("BannerUuDaiBlog") %>" class="img-fluid w-100 mb-3" alt="<%=domainName %>"></a>
                    </div>
                    <%//end ads %>
                    <%//lay 7 bai viet moi nhat %>
                    <%var total_7_tin_moi = 0; %>
                    <%var list_7_tin_moi = NewsBo.GetNewsDetailWithLanguage(3, lang, 2, 0, "", 1, 7, ref total_7_tin_moi).ToList(); %>
                    <div class="blog-ss-right mb-3">
                        <div class="heading">
                            <a href="javascript:void(0)" title="">Mới cập nhật</a>
                        </div>
                        <%for (int i = 0; i < list_7_tin_moi.Count(); i++)
                            { %>
                        <%var link_target = string.Format("/{0}/{1}/{2}-{3}.htm", currentLanguage, UIHelper.GetTypeUrlByMiProject(3), list_7_tin_moi[i].Url, list_7_tin_moi[i].NewsId); %>
                        <%if (i == 0)
                            { %>
                        <div class="item-left mb-3">
                            <div class="image">
                                <a href="<%=link_target %>" title="">
                                    <img src="/uploads/<%=list_7_tin_moi[i].Avatar %>" class="img-fluid" alt="<%=list_7_tin_moi[i].Title %>" /></a>
                            </div>
                            <h2 class="title">
                                <a href="<%=link_target %>" title=""><%=list_7_tin_moi[i].Title %></a>
                            </h2>
                            <div class="detail">
                                <%=UIHelper.TrimByWord(UIHelper.ClearHtmlTag(list_7_tin_moi[i].Sapo),20,"...") %>
                            </div>

                        </div>
                        <%}
                            else
                            { %>
                        <div class="item-right d-flex">
                            <div class="image">
                                <a href="<%=link_target %>" title="">
                                    <img src="/uploads/<%=list_7_tin_moi[i].Avatar %>" class="img-fluid" alt="<%=list_7_tin_moi[i].Title %>" /></a>
                            </div>
                            <div class="text">
                                <h2 class="title">
                                    <a href="<%=link_target %>" title=""><%=list_7_tin_moi[i].Title %></a>
                                </h2>
                                <div class="time">
                                    <%=list_7_tin_moi[i].CreatedDate.ToString("dd/MM/yyyy") %>
                                </div>
                            </div>

                        </div>
                        <%} %>
                        <%} %>
                    </div>
                    <%//end 7 tin moi %>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
