﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/MiWBanner.Master" AutoEventWireup="true" CodeBehind="Mi_TuyenDung.aspx.cs" Inherits="Mi_Company.Pages.Mi_TuyenDung" %>

<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <%--<h1>Trang tuyển dụng</h1>--%>
        <div class="container py-4">
        <div class="row">
            <div class="col-xl-8 col-md-7 col-sm-7 col-12">
                <h4 class="text-uppercase mb-4">Mới nhất</h4>
                <div class="list-recruitment mb-4">
                    <%//Lay thong tin tuyen dung %>
                    <%var total_1 = 0; %>
                    <%var list_tuyen_dung = NewsBo.GetNewsDetailWithLanguage(6, lang, 2, 0, "", 1, 10, ref total_1); %>
                    <%foreach (var item in list_tuyen_dung)
                        { %> 
                    <%var link_target = string.Format("/{0}/{1}/{2}-{3}.htm",currentLanguage,"tuyen-dung",item.Url,item.NewsId); %>
                        <div class="item-recruitment">
                        <div class="row">
                            <div class="col-xl-2 col-md-3 col-sm-3 col-3">
                                <div class="image"><img src="/uploads/<%=item.Avatar %>" class="img-fluid" alt="<%=item.Title %>"/></div>
                            </div>
                            <div class="col-xl-10 col-md-9 col-sm-9 col-9">
                                <h4 class="mb-3">
                                    <a href="<%=link_target %>" title=""><%=item.Title %></a>
                                </h4>
                                <div>
                                    <%var info = new BacSiEntity(); %>
                                     
                                    <%if (!string.IsNullOrEmpty(item.Extras))
                                        { %> 
                                    <%info = Newtonsoft.Json.JsonConvert.DeserializeObject<BacSiEntity>(item.Extras); %>
                                        <div class="d-inline-block mr-4 mb-3">
                                        <img src="/Themes/images/work1.svg" class="img-fluid mr-2" />
                                        <span class="chuc-vu"><%=info.ChuyenKhoa %></span>
                                    </div>
                                    <div class="d-inline-block mr-4 mb-3">
                                        <img src="/Themes/images/amount.svg" class="img-fluid mr-2" />
                                        <span class="so-luong"><%=info.ChuyenMon %></span>
                                    </div>
                                    <div class="d-inline-block mb-3">
                                        <img src="/Themes/images/money.svg" class="img-fluid mr-2" />
                                        <span class="muc-luong">
                                            <%=info.LichKham %>
                                        </span>
                                    </div>
                                    <%} %>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <%} %>
                </div>
            </div>
            <div class="col-xl-4 col-md-5 col-sm-5 col-12">
                <h4 class="text-uppercase mb-4">Tiêu điểm</h4>
                <%//Lay thang moi nhat co hot %>
                <%var total_2 = 0; %>
                <%var tieu_diem = NewsBo.GetNewsDetailWithLanguage(6, lang, 1, 0, "", 1, 1, ref total_2).FirstOrDefault(); %>
                <%if (tieu_diem != null)
                    {
                        var info = new BacSiEntity();

                        if (!string.IsNullOrEmpty(tieu_diem.Extras))
                        {
                            info = Newtonsoft.Json.JsonConvert.DeserializeObject<BacSiEntity>(tieu_diem.Extras);
                        }

                        %> 
                <%var link_target = string.Format("/{0}/{1}/{2}-{3}.htm",currentLanguage,"tuyen-dung",tieu_diem.Url,tieu_diem.NewsId); %>
                    <div class="border text-center p-4">
                    <h4 class="mb-3"><%=tieu_diem.Title %></h4>
                    <div class="mb-3">
                        <div class="mb-2">
                            <img src="/Themes/images/work1.svg" class="img-fluid mr-2" />
                            <span class="chuc-vu"><%=info.ChuyenKhoa %></span>
                        </div>
                        <div class="mb-2">
                            <img src="/Themes/images/amount.svg" class="img-fluid mr-2" />
                            <span class="so-luong"><%=info.ChuyenMon %></span>
                        </div>
                        <div class="mb-2">
                            <img src="/Themes/images/money.svg" class="img-fluid mr-2" />
                            <span class="muc-luong">
                                <%=info.LichKham %>
                            </span>
                        </div>
                    </div>
                    <div class="mb-4">
                        <p class="font-weight-bold">Mô tả công việc:</p>
                        <%=tieu_diem.Body %>
                    </div>
                    <a href="<%=link_target %>" class="btn btn-detail-re">
                        Xem chi tiết
                    </a>
                </div>
                <%} %>
                
            </div>
        </div>
    </div>


    <section class="map">
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.4494941266425!2d105.7857861144065!3d20.974611595001694!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acd3d8a7e509%3A0x2c132edb57f47f5c!2zUmFpbmJvdyBWxINuIFF1w6Fu!5e0!3m2!1svi!2s!4v1580795720735!5m2!1svi!2s"
            width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
