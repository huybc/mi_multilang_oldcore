﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Mi.Master" AutoEventWireup="true" CodeBehind="Mi_LienHe.aspx.cs" Inherits="Mi_Company.Pages.Mi_LienHe" %>

<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <section class="bg-lien-he">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10 col-md-11 col-12">
                        <div class="lh-box-infor mb-4">
                            <h1 class="title">Liên hệ: <%=UIHelper.GetConfigByName("TieuDeLienHe") %></h1>
                            <ul class="pl-md-5">
                                <li>
                                    <label class="font-weight-bold mr-1"><i class="fas fa-phone-volume mr-2"></i>Điện
                                        thoại:</label><%=UIHelper.GetConfigByName("Hotline") %>
                                </li>
                                <li>
                                    <label class="font-weight-bold mr-1"><i class="fas fa-map-marker-alt mr-2"></i>Trụ
                                        sở:</label> <%=UIHelper.GetConfigByName("DiaChi") %>
                                </li>
                                <li>
                                    <label class="font-weight-bold mr-1"><i
                                            class="fas fa-envelope mr-2"></i>Email:</label><%=UIHelper.GetConfigByName("Email") %>
                                </li>
                                <li>
                                    <label class="font-weight-bold mr-1"><i
                                            class="fab fa-facebook-square mr-2"></i>Facebook:</label><a href="<%=UIHelper.GetConfigByName("ConfigFacebook") %>"><%=UIHelper.GetConfigByName("ConfigFacebook") %></a>
                                </li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-md-6 col-12">
                                <div class="form-group">
                                    <input type="text" class="form-control form-lh" id="txtName1" placeholder="Họ và tên">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-lh" id="txtPhone1" placeholder="Số điện thoại">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-lh" id="txtEmail1" placeholder="Địa chỉ email">
                                </div>
    
                            </div>
                            <div class="col-lg-7 col-md-6 col-12">
                                <div class="form-group">
                                    <textarea rows="7" class="form-control form-lh" id="txtNote1"
                                        placeholder="Nội dung tin nhắn..."></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-12">
                                <div class="form-group">
                                    <a href="javascript:void(0)" class="btn form-lh" id="btnSubmit1" data-type="lien-he">Gửi đi</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">

                </div>
            </div>
        </section>

        <section class="try py-5">
        <div class="container text-center">
            <h4><%=UIHelper.GetConfigByName("TieuDeDong4Webiste") %></h4>
            <p><%=UIHelper.GetConfigByName("NoiDungeDong4Webiste") %></p>
            <a href="javascript:void(0)" class=" btn btn-F0545D tu-van-ngay">DÙNG THỬ MIỄN PHÍ</a>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <script src="/Pages/Script/LienHe.js"></script>
    <script type="text/javascript">
        $('input.form-lh').off('blur').on('blur',function () {
            //alert('out');
            if ($(this).val() != null)
                $(this).css('background', '#ddd').css('opacity','0.8').css('color','#000000');
        })
        $('textarea.form-lh').off('blur').on('blur', function () {
            //alert('out');
            if ($(this).val() != null)
                $(this).css('background', '#ddd').css('opacity', '0.8').css('color', '#000000');
        })
    </script>
</asp:Content>
