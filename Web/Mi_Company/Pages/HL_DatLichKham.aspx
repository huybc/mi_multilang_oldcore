﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/HoangLongMaster.Master" AutoEventWireup="true" CodeBehind="HL_DatLichKham.aspx.cs" Inherits="Mi_Company.Pages.HL_DatLichKham" %>

<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%--<%
        var currentLanguage = Page.RouteData.Values["languageCode"].ToString();
        ///var currentType = Page.RouteData.Values["type"].ToString();
        //var type = "";
        var lang = "";
        switch (currentLanguage)
        {
            case "vi":
                lang = "vi-VN";
                break;
            case "en":
                lang = "en-US";
                break;
        }
    %>--%>
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <div class="container">
        <section class="bg-dat-lich">


            <div class="row ">
                <div class="col-lg-11 col-md-12 col-sm-12 col-12">
                    <div class="row justify-content-center">
                        <div class="col-sm-8 col-12 offset-md-3 text-center mb-4">
                            <h4 class="text-uppercase mb-2"><%=UIHelper.GetConfigByName_v1("TieuDeDatLich",lang) %></h4>
                            <div><%=UIHelper.GetConfigByName_v1("NoiDungeDatLich_1",lang) %></div>
                            <div><%=UIHelper.GetConfigByName_v1("NoiDungeDatLich_2",lang) %></div>
                        </div>
                    </div>
                    <form class="form-dat-lich">
                        <div class="form-group row justify-content-center">
                            <label for="" class="col-lg-3 col-sm-3 col-form-label require"><%=Language.Full_Name %></label>
                            <div class="col-sm-8 col-12">
                                <input type="text" class="form-control" id="txtName" placeholder="<%=Language.Full_Name %>">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <label for="" class="col-lg-3 col-sm-3 col-form-label require"><%=Language.Date_Of_Birth %></label>
                            <div class="col-sm-8 col-12">
                                <input type="text" class="form-control dpk" id="txtBirthday" placeholder="24/07/1998">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <label for="" class="col-lg-3 col-sm-3 col-form-label require"><%=Language.Phone_Number %></label>
                            <div class="col-sm-8 col-12">
                                <input type="text" class="form-control" id="txtPhoneNumber" placeholder="0833336299">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <label for="" class=" col-lg-3 col-sm-3 col-form-label require">
                                <%=Language.Address %>
                            </label>

                            <div class="col-sm-8 col-12">
                                <input type="text" class="form-control" id="txtAddress" placeholder="<%=Language.Address %>">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <label for="" class=" col-lg-3 col-sm-3 col-form-label require">
                                <%=Language.Location %>
                            </label>

                            <div class="col-sm-8 col-12">
                                <select name="" class="form-control" id="cbxLocation">
                                    <option value="Tp.Hà Nội">
                                    Tp.Hà Nội
                                        <option value="Tp.Hải Phòng">
                                    Tp.Hải Phòng
                                    <option value="Tp.Cần Thơ">
                                    Tp.Cần Thơ
                                    <option value="Tp.Đà Nẵng">
                                    Tp.Đà Nẵng
                                    <option value="TP  HCM">
                                    TP HCM
                                    <option value="An Giang">
                                    An Giang
                                    <option value="Bà Rịa - Vũng Tàu">
                                    Bà Rịa - Vũng Tàu
                                    <option value="Bắc Giang">
                                    Bắc Giang
                                    <option value="Bắc Kạn">
                                    Bắc Kạn
                                    <option value="Bạc Liêu">
                                    Bạc Liêu
                                    <option value="Bắc Ninh">
                                    Bắc Ninh
                                    <option value="Bến Tre">
                                    Bến Tre
                                    <option value="Bình Định">
                                    Bình Định
                                    <option value="Bình Dương">
                                    Bình Dương
                                    <option value="Bình Phước">
                                    Bình Phước
                                    <option value="Bình Thuận">
                                    Bình Thuận
                                    <option value="Bình Thuận">
                                    Bình Thuận
                                    <option value="Cà Mau">
                                    Cà Mau
                                    <option value="Cao Bằng">
                                    Cao Bằng
                                    <option value="Đắk Lắk">
                                    Đắk Lắk
                                    <option value="Đắk Nông">
                                    Đắk Nông
                                    <option value="Điện Biên">
                                    Điện Biên
                                    <option value="Đồng Nai">
                                    Đồng Nai
                                    <option value="Đồng Tháp">
                                    Đồng Tháp
                                    <option value="Đồng Tháp">
                                    Đồng Tháp
                                    <option value="Gia Lai">
                                    Gia Lai
                                    <option value="Hà Giang">
                                    Hà Giang
                                    <option value="Hà Nam">
                                    Hà Nam
                                    <option value="Hà Tĩnh">
                                    Hà Tĩnh
                                    <option value="Hải Dương">
                                    Hải Dương
                                    <option value="Hậu Giang">
                                    Hậu Giang
                                    <option value="Hòa Bình">
                                    Hòa Bình
                                    <option value="Hưng Yên">
                                    Hưng Yên
                                    <option value="Khánh Hòa">
                                    Khánh Hòa
                                    <option value="Kiên Giang">
                                    Kiên Giang
                                    <option value="Kon Tum">
                                    Kon Tum
                                    <option value="Lai Châu">
                                    Lai Châu
                                    <option value="Lâm Đồng">
                                    Lâm Đồng
                                    <option value="Lạng Sơn">
                                    Lạng Sơn
                                    <option value="Lào Cai">
                                    Lào Cai
                                    <option value="Long An">
                                    Long An
                                    <option value="Nam Định">
                                    Nam Định
                                    <option value="Nghệ An">
                                    Nghệ An
                                    <option value="Ninh Bình">
                                    Ninh Bình
                                    <option value="Ninh Thuận">
                                    Ninh Thuận
                                    <option value="Phú Thọ">
                                    Phú Thọ
                                    <option value="Quảng Bình">
                                    Quảng Bình
                                    <option value="Quảng Bình">
                                    Quảng Bình
                                    <option value="Quảng Ngãi">
                                    Quảng Ngãi
                                    <option value="Quảng Ninh">
                                    Quảng Ninh
                                    <option value="Quảng Trị">
                                    Quảng Trị
                                    <option value="Sóc Trăng">
                                    Sóc Trăng
                                    <option value="Sơn La">
                                    Sơn La
                                    <option value="Tây Ninh">
                                    Tây Ninh
                                    <option value="Thái Bình">
                                    Thái Bình
                                    <option value="Thái Nguyên">
                                    Thái Nguyên
                                    <option value="Thanh Hóa">
                                    Thanh Hóa
                                    <option value="Thừa Thiên Huế">
                                    Thừa Thiên Huế
                                    <option value="Tiền Giang">
                                    Tiền Giang
                                    <option value="Trà Vinh">
                                    Trà Vinh
                                    <option value="Tuyên Quang">
                                    Tuyên Quang
                                    <option value="Vĩnh Long">
                                    Vĩnh Long
                                    <option value="Vĩnh Phúc">
                                    Vĩnh Phúc
                                    <option value="Yên Bái">
                                    Yên Bái
                                    <option value="Phú Yên">
                                    Phú Yên

                                </select>

                            </div>
                        </div>
                        <%/*

*/

                        %>
                        <div class="form-group row justify-content-center">
                            <label for="" class="col-lg-3 col-sm-3 col-form-label require"><%=Language.Medical_day %></label>
                            <div class="col-sm-8 col-12">
                                <input type="text" class="form-control dpk" id="txtNgayKham" placeholder="24/04/2019">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <label for="" class="col-lg-3 col-sm-3 col-form-label require"><%="Cơ sở" %></label>
                            <div class="col-sm-8 col-12">
                                <%//Lay thong tin cac co so %>
                                <%var list_co_so = ZoneBo.GetZoneWithLanguageByType(18, lang).Where(r => r.ParentId > 0).ToList(); %>
                                <% %>
                                <select id="cbx_CoSo" class="form-control">
                                    <%foreach (var item in list_co_so)
                                        { %> 
                                        <option value="<%=item.Id %>"><%=item.Content %></option>
                                    <%} %>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <label for="" class="col-lg-3 col-sm-3 col-form-label ">Email</label>
                            <div class="col-sm-8 col-12">
                                <input type="text" class="form-control" id="txtEmail" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <label for="" class="col-lg-3 col-sm-3 col-form-label align-self-start"><%=Language.Note %></label>
                            <div class="col-sm-8 col-12">
                                <textarea type="text" rows="6" class="form-control" id="txtNote" placeholder="<%=Language.Note_Detail %>"></textarea>
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <div class="col-sm-8 col-12 offset-md-3 text-center mt-4">
                                <a href="javascript:void(0)" id="dat-lich-kham" data-lang="<%=lang %>" role="button" class="btn btn-dat-lich mx-auto"><%=Language.Send %></a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </section>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.dpk').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: parseInt(moment().format('YYYY'), 10),
                locale: {
                    "format": "DD/MM/YYYY"
                }
                //}, function (start, end, label) {
                //    var years = moment().diff(start, 'years');
                //    alert("You are " + years + " years old!");
            });
        });
    </script>
    <script type="text/javascript">
        $('#dat-lich-kham').off('click').on('click', function () {
            var name = $('#txtName').val();
            var birthday = moment($('#txtBirthday').val(), "DD/MM/YYYY").format("YYYY-MM-DD");
            var ngaykham = moment($('#txtNgayKham').val(), "DD/MM/YYYY").format("YYYY-MM-DD");
            var phoneNumber = $('#txtPhoneNumber').val();
            var address = $('#txtAddress').val();
            var coSo = $('#cbx_CoSo').val();
            var location = $('#cbxLocation').val();
            var email = $('#txtEmail').val();
            var note = $('#txtNote').val();
            var type = "dat-lich";
            var lang = $(this).data('lang');
            console.log(name, birthday, ngaykham, phoneNumber, address, email, note, type);

            R.Post({
                params: {
                    name: name,
                    email: email,
                    phoneNumber: phoneNumber,
                    note: note,
                    type: type,
                    ngaySinh: birthday,
                    ngayKham: ngaykham,
                    address: address,
                    location: location,
                    coSo: coSo

                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "save",
                success: function (res) {
                    if (res.Success) {
                        console.log(res.Data);
                        R.Post({
                            params: {
                                name: name,
                                email: email,
                                phoneNumber: phoneNumber,
                                note: note,
                                type: type,
                                ngaySinh: birthday,
                                ngayKham: ngaykham,
                                address: address,
                                location: location,
                                coSo: coSo

                            },
                            module: "ui-action",
                            ashx: 'modulerequest.ashx',
                            action: "send_mail",
                            success: function (res) {
                                console.log('Send mail Successful!');
                            }
                        });
                        //alert("Cảm ơn Quý khách đã để lại thông tin! Chúng tôi sẽ liên hệ lại sớm!");
                        var link = "/" + lang.split("-")[0] + "/xac-nhan-dat-lich?id=" + res.Data;
                        window.location.replace(link);
                    }
                    // $('.card-header').RLoadingModuleComplete();
                }
            });
        })
    </script>
</asp:Content>
