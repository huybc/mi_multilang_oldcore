﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/MiWBanner.Master" AutoEventWireup="true" CodeBehind="Mi_ListDanhMuc.aspx.cs" Inherits="Mi_Company.Pages.Mi_ListDanhMuc" %>

<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        string domainNameFull = HttpContext.Current.Request.Url.LocalPath;
    %>

    <%//Bat dau lay thong tin %>
    <%var alias = Page.RouteData.Values["alias"]; %>
    <%var list_zone_tin_tuc = ZoneBo.GetZoneWithLanguageByType(3, lang); %>
    <%var zone_tin_tuc_parent = list_zone_tin_tuc.Where(r => r.ParentId == 0).FirstOrDefault(); %>
    <%var zone_tin_tuc_child = list_zone_tin_tuc.Where(r => r.ParentId == zone_tin_tuc_parent.Id); %>
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4 col-12">
                    <div class="menu-left">
                        <div class="heading">
                            <%=zone_tin_tuc_parent.lang_name %>
                        </div>
                        <div class="list-menu">
                            <%foreach (var item in zone_tin_tuc_child)
                                { %> 
                               <%var link_tar = string.Format("/{0}/blog/{1}", currentLanguage, item.Alias); %>
                                <div class="item">
                                <a href="<%=link_tar %>" title=""><%=item.lang_name %></a>
                            </div>
                            <%} %>
                        </div>
                    </div>

                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mi-breadcrumb">
                            <%var zone_now = zone_tin_tuc_child.Where(r => r.Alias.Equals(alias)).SingleOrDefault(); %>
                            <li class="breadcrumb-item " aria-current="page"><%=zone_tin_tuc_parent.lang_name %></li>
                            <%if(zone_now != null){ %> 
                                <li class="breadcrumb-item active" aria-current="page"><%=zone_now.lang_name %></li>
                            <%} %>
                        </ol>
                    </nav>
                    <hr>
                    <div class="list-blog-2">
                        <%//Lay danh sach bai viet thuoc zone nay %>
                        <%var total_r = 0; %>
                        <%var list_tin = NewsBo.GetNewsDetailWithLanguage(zone_now.Type, lang, 2, zone_now.Id, "", 1, 10, ref total_r); %>
                        <%var tin_dau_tien = list_tin.Skip(0).Take(1).SingleOrDefault(); %>
                        <%if (tin_dau_tien != null)
                            { %> 
                        <%var link_target = string.Format("/{0}/blog/{1}-{2}.htm", currentLanguage, tin_dau_tien.Url, tin_dau_tien.NewsId); %>
                            <div class="item item-large mb-4">
                            <div class="row">
                                <div class="col-lg-6 col-md-7 col-sm-7 col-12">
                                    <div class="image mb-3  mb-sm-0">
                                        <a href="<%=link_target %>" title="">
                                            <img src="/uploads/<%=tin_dau_tien.Avatar %>" class="img-fluid" alt="<%=tin_dau_tien.Title %>"></a>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-5 col-sm-5 col-12">
                                    <h3 class="title">
                                        <a href="<%=link_target %>" title=""><%=tin_dau_tien.Title %></a>
                                    </h3>
                                    <p class="des">
                                        <%=UIHelper.TrimByWord(UIHelper.ClearHtmlTag(tin_dau_tien.Sapo),20,"...") %>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <%} %>
                        
                        <%var list_tin_tiep_theo = list_tin.Skip(1); %>
                        <%foreach (var item in list_tin_tiep_theo)
                            { %> 
                        <%var link_target = string.Format("/{0}/blog/{1}-{2}.htm", currentLanguage, item.Url, item.NewsId); %>
                            <div class="item mb-3 item-load-trang">
                            <div class="row">
                                <div class="col-md-4 col-sm-5 col-4">
                                    <div class="image mb-3 mb-sm-0">
                                        <a class="link-load-trang" href="<%=link_target %>" title="">
                                            <img src="/uploads/<%=item.Avatar %>" class="img-fluid" alt="<%=item.Title %>"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-7 col-8 pl-0">
                                    <h3 class="title" style="text-transform:none">
                                        <a class="link-load-trang-1" href="<%=link_target %>" title=""><%=item.Title %></a>
                                    </h3>
                                    <p class="des">
                                        <%=UIHelper.TrimByWord(UIHelper.ClearHtmlTag(item.Sapo),20,"...") %>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <%} %>
                    </div>
                    <div class="pani mt-5">
                        <%var tong_so_trang = total_r / 10 + 1; %>
                        <span class="page-number mr-3">Trang <span class="danh-so-trang">1</span>/<%=tong_so_trang %></span>
                        <a href="javascript:void(0)" id="view-more-blog" data-lang="<%=lang %>" data-zone="<%=zone_now.Id %>" data-page="2" data-size="10" data-type="3" data-txttype="blog" class="btn mr-3" role="button">Xem thêm</a>
                        <%--<a href="javascripts:;" class="btn " onclick="topFunction()" role="button">Về đầu trang</a>--%>
                    </div>

                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <script src="/Pages/Script/XemThem.js"></script>
</asp:Content>
