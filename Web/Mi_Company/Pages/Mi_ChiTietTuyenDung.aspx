﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/MiWBanner.Master" AutoEventWireup="true" CodeBehind="Mi_ChiTietTuyenDung.aspx.cs" Inherits="Mi_Company.Pages.Mi_ChiTietTuyenDung" %>

<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        var s_id = Page.RouteData.Values["id"].ToString();
        var id = int.Parse(s_id);
        var newsDetail = NewsBo.GetNewsDetailWithLanguageById(id, lang);
        var delete_html = @"<[^>]*>";
        //var sapo = obj.Sapo.Replace(delete_html, "");
        var sapo = Regex.Replace(obj.Sapo, delete_html, "");
        %>
    <style>
        .content img {
            max-width: 100%;
            height: auto;
        }
        .content li{
            list-style-type:disc;
            list-style-position:inside;
        }
    </style>
    <meta property="fb:app_id" content="1426300910760607" />
    <meta property="og:url" content="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" />
    <meta property="og:type" content="article" />
    <%if (obj != null)
      { %>
        <meta property="og:title" content="<%= obj.Title %>" />
        <meta property="og:description" content="<%=sapo%>" />
        <meta property="og:image" content="<%=domainName %>/uploads/<%= obj.Avatar %>" />
    <%} %>
    <link rel="canonical" href="<%=newsDetail.Title %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <%var id = int.Parse(Page.RouteData.Values["id"].ToString()); %>

    <%var detail = NewsBo.GetNewsDetailWithLanguageById(id, lang); %>
    <%var info = new BacSiEntity(); %>
    <%if (detail != null)
        {
            if (!string.IsNullOrEmpty(detail.Extras))
            {
                info = Newtonsoft.Json.JsonConvert.DeserializeObject<BacSiEntity>(detail.Extras);
            }

        }%>
    <%--<h1><%=detail.Title %></h1>--%>
    <%var delete_html = @"<[^>]*>";
        //var sapo = obj.Sapo.Replace(delete_html, "");
        var sapo = Regex.Replace(obj.Sapo, delete_html, ""); %>
    <script type="application/ld+json">
				            {  
                                    "@context":"http://schema.org",
                                    "@type":"Article",
                                    "mainEntityOfPage":{  
                                        "@type":"WebPage",
                                        "@id":"<%=HttpContext.Current.Request.Url.AbsoluteUri %>"
                                    },
                                   "headline":"<%=!string.IsNullOrEmpty(obj.MetaTitle)?obj.MetaTitle:obj.Title%>",
                                    "image":{  
                                        "@type":"ImageObject",
                                        "url":"<%=domainName+"/uploads/"+obj.Avatar %>",
                                        "height":600,
                                        "width":800
                                    },
                                    "datePublished":"<%=obj.CreatedDate %>",
                                    "dateModified":"<%=obj.CreatedDate %>",
                                    "author":{  
                                        "@type":"Person",
                                        "name":"migroup"
                                    },
                                    "publisher":{  
                                        "@type":"Organization",
                                        "name":"<%=domainName %>",
                                        "logo":{  
                                            "@type":"ImageObject",
                                            "url":"<%=domainName+"/uploads/"+UIHelper.GetConfigByName("LogoTop")%>",
                                            "width":35,
                                            "height":34
                                        }
                                    },
                                    "description":"<%=sapo %>"
                            }
    </script>

    <div class="container py-4">
        <div class="row">
            <div class="col-xl-8 col-md-7 col-sm-7 col-12">
                <div class="detail-re">
                    <h1 class="title mb-4"><%=detail.Title %></h1>
                    <div class="mb-3">
                        <div class="d-inline-block mr-4 mb-2">
                            <img src="/Themes/images/work1.svg" class="img-fluid mr-2" />
                            <span class="chuc-vu"><%=info.ChuyenKhoa %></span>
                        </div>
                        <div class="d-inline-block mr-4 mb-2">
                            <img src="/Themes/images/amount.svg" class="img-fluid mr-2" />
                            <span class="so-luong"><%=info.ChuyenMon %></span>
                        </div>
                        <div class="d-inline-block mb-2">
                            <img src="/Themes/images/money.svg" class="img-fluid mr-2" />
                            <span class="muc-luong"><%=info.LichKham %>
                            </span>
                        </div>
                    </div>
                    <div class="content">
                        <%=detail.Body %>
                    </div>
                    <div class="like-share-commnet">
                        <div class="fb-like" data-href="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="" data-layout="standard" data-action="like" data-size="small" data-share="true"></div>
                        <div class="fb-comments" data-href="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="" data-numposts="10"></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-5 col-sm-5 col-12">
                <h4 class="font-weight-bold mb-4">Ứng tuyển</h4>
                <div class="border p-4">
                    <form class="form-re">
                        <div class="border mb-3 px-3 py-2" style="color: #009DE7;">
                            <%=detail.Title %>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="Họ và tên *">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="" placeholder="Email *">
                        </div>
                        <div class="form-group">

                            <input type="text" class="form-control" id="" placeholder="Số điện thoại *">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="Mức lương mong muốn *">
                        </div>
                        <div class="form-group">
                            <textarea type="text" rows="8" class="form-control" id="" placeholder="Thông tin thêm"></textarea>
                        </div>
                        <div class="form-group">
                            <textarea type="text" rows="5" class="form-control" id="" placeholder="Thông tin thêm"></textarea>
                        </div>
                        <div class="input-group mb-3">
                            <div class="custom-file ">
                                <input type="file" class=" custom-file-input" id="inputGroupFile01"
                                    aria-describedby="inputGroupFileAddon01">
                                <label class="custom-file-label" for="inputGroupFile01">Đính kèm cv</label>
                            </div>
                        </div>
                        <p>* File đính kèm dung lượng tối đa 5MB</p>
                        <button class="btn w-100 btn-detail-re">
                            Gia nhập
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>




    <section class="map">
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.4494941266425!2d105.7857861144065!3d20.974611595001694!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acd3d8a7e509%3A0x2c132edb57f47f5c!2zUmFpbmJvdyBWxINuIFF1w6Fu!5e0!3m2!1svi!2s!4v1580795720735!5m2!1svi!2s"
            width="100%" height="450" frameborder="0" style="border: 0;" allowfullscreen=""></iframe>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
