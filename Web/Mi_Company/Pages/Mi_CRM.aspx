﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/MiCRM.Master" AutoEventWireup="true" CodeBehind="Mi_CRM.aspx.cs" Inherits="Mi_Company.Pages.Mi_CRM" %>
<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <%var banner_crm = ConfigBo.AdvGetByType(21, lang).FirstOrDefault(); %>
    <%var khoi_content_1 = ConfigBo.AdvGetByType(22, lang).FirstOrDefault(); %>
    <%var khoi_content_2 = ConfigBo.AdvGetByType(24, lang).FirstOrDefault(); %>
    <%var khoi_content_3 = ConfigBo.AdvGetByType(25, lang).FirstOrDefault(); %>
    <%if(banner_crm != null){ %> 
        <style>
        @media (min-width: 1366px) {
            .banner-crm {
                background: url(/uploads/<%=banner_crm.Thumb%>) no-repeat bottom right;
                    background-size: 1000px 664px;
    min-height: 664px;
            }
        }
        @media (min-width: 1200px) {
            .banner-crm {
                background: url(/uploads/<%=banner_crm.Thumb%>) no-repeat bottom right;
            }
        }
        @media (min-width: 992px) {
            .banner-crm {
                background: url(/uploads/<%=banner_crm.Thumb%>) no-repeat bottom right;
            }
        }
        @media (min-width: 768px) {
            .banner-crm {
                background: url(/uploads/<%=banner_crm.Thumb%>) no-repeat bottom right;
            }
        }
    </style>
    <%if (khoi_content_1 != null)
        { %> 
        <style>
        .banner-crm-2 .bg-crm{
                background: url(/uploads/<%=khoi_content_1.Thumb%>) no-repeat center center;

        }
        .block-content ul{
            list-style-type: circle;
            list-style-position: inside;
        }
    </style>
    <%if(khoi_content_2 != null){ %> 
        <style>
            .wave-bot .bg-crm{
                background: url(/uploads/<%=khoi_content_2.Thumb%>) no-repeat center center;
                    background-size: cover;
            }
        </style>
    <%} %>
    <%if(khoi_content_3 != null){ %> 
        <style>
            .why-us .bg-crm{
                background: url(/uploads/<%=khoi_content_3.Thumb%>) no-repeat center center;
            }
        </style>
    <%} %>
    <%} %>
    
    <%} %>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <section class="banner-crm py-4">
        <div class="container ">
            <%var banner_crm = ConfigBo.AdvGetByType(21, lang).FirstOrDefault(); %>
            <%if (banner_crm != null)
                { %> 
                <div class="hero-text">
                <h1>
                    <%=banner_crm.Name %>
                </h1>
                <p>
                    <%=banner_crm.Content %>
                </p>
                <a href="javascript:void(0)" class="btn tu-van-ngay" data-id="5190">
                    LIÊN HỆ HỢP TÁC
                </a>
            </div>
            <%} %>
            
        </div>
    </section>
    <section class="banner-crm-2">
        <%var khoi_content_1 = ConfigBo.AdvGetByType(22, lang).Skip(1); %>

        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <%foreach (var item in khoi_content_1)
                        { %> 
                        <div class="block-text mb-3">
                        <h2>
<%--                            <div class="color-F65D2D"><%=item.Name %></div>
                            TẠO NÊN SỰ <span class="color-F65D2D">KHÁC BIỆT</span>--%>
                            <%=UIHelper.ConvertToTextColorByBracket(item.Name) %>
                            
                        </h2>
                            <div class="block-content">
                                <%=item.Content %>
                            </div>
                        
                    </div>
                    <%} %>
                    
<%--                    <div class="block-text mb-3">
                        <h2 class="color-F65D2D">
                            Dịch vụ IT Outsourcing
                        </h2>
                        <p>
                            Đội ngũ nhân sự chuyên môn cao sẵn sàng đáp ứng nhu cầu sản xuất phần mềm, ứng dụng, hệ
                            thống tự động hóa từ phía doanh nghiệp:
                        </p>
                        <ul>
                            <li>● Tư vấn các giải pháp công nghệ tối ưu nhất cho từng lĩnh vực kinh doanh</li>
                            <li>● Đồng hành cùng xây dựng hệ thống quản trị, bao gồm quản lý nhân sự, quản lý hoạt động
                                logistic, quản lý chung,…</li>
                            <li>
                                ● Hỗ trợ thực hiện các tính năng công nghệ như realtime, xử lý server tối ưu page
                                loading, video watching.
                            </li>
                        </ul>




                    </div>
                    <div class="block-text mb-3">
                        <h2 class="color-F65D2D">
                            Đồng hành cùng Start-up Việt
                        </h2>
                        <p>
                            Được thành lập bởi những kỹ sư công nghệ thông tin Việt Nam, <span>Migroup</span> thấu hiểu
                            sâu sắc vai trò của công nghệ trong sự phát triển của các doanh nghiệp hiện nay.
                        </p>

                    </div>--%>

                </div>
                <div class="col-xl-6 col-md-6 col-sm-12 col-12 align-self-center ">
                    <div class="bg-crm">

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="">

    </section>


    <section class="slide-logo py-4">
        <%var list_logo = ConfigBo.AdvGetByType(23, lang); %>
        
        <div class="container">
            <div class="swiper-container">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <%foreach(var item in list_logo){ %> 
                        <div class="swiper-slide">
                        <img src="/uploads/<%=item.Thumb %>" class="w-100" alt="<%=domainName %>"/>
                    </div>
                    <%} %>
                </div>

            </div>
        </div>
    </section>

    <section class="wave-bot py-4">
        <div class="row no-gutters">
            <%var khoi_content_2 = ConfigBo.AdvGetByType(24, lang).FirstOrDefault(); %>
            <div class="col-md-6 col-sm-12 col-12">
                <div class="bg-crm">
                     
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-12 p-4 p-md-5 bg-FFE1CC">
               <div class="mb-5">
                <%if(khoi_content_2!= null){ %> 
                   <%=khoi_content_2.Content %>
                   <%} %>
               </div>
            </div>
        </div>
        <div class="ocean">
            <div class="wp-wave">
                <div class="wave wave1"></div>
            </div>
        </div>
    </section>
   

    <section class="why-us py-4 py-md-5">
        <%var khoi_content_3 = ConfigBo.AdvGetByType(25, lang).FirstOrDefault(); %>
        <div class="container">
            <div class="container">
                <div class="row">
                    <%if (khoi_content_3 != null)
                        { %> 
                    <div class="col-xl-6 col-md-6 col-sm-12 col-12 align-self-center">
                        <div class="block-text mb-3">
                            <h2 class="mb-4">
                                <%=UIHelper.ConvertToTextColorByBracket(khoi_content_3.Name) %>
                            </h2>
                            <div class="block-content">
                                <%=khoi_content_3.Content %>
                            </div>
                            
                        </div>
                        
                    </div>
                    <div class="col-xl-6 col-md-6 col-sm-12 col-12 align-self-center ">
                        <div class="bg-crm">

                        </div>
                    </div>
                    <%} %>
                    
                    
                </div>
            </div>

        </div>
    </section>

    <section class="projects py-4">
        <div class="container">
            <div class="heading-ss-home">
                <%=UIHelper.GetConfigByName("TieuDeDong3") %>
            </div>
            <div class="row <%--justify-content-center--%>">
                <%var total_1 = 0;
                    %>
                <%var list_6_du_an = NewsBo.GetNewsDetailWithLanguage(1, lang, 2, 5192, "", 1, 6, ref total_1); %>
                <%foreach(var item in list_6_du_an){ %> 
                <%var link_target = string.Format("/{0}/du-an/{1}-{2}.htm",currentLanguage,item.Url,item.NewsId);//Cho nay set link sau %>
                    <div class="col-xl-4 col-lg-4 col-sm-6 col-12 item-load-trang pull-left">
                    <div class="card mi-card mb-4">
                        <div class="image">
                            <a href="<%=link_target %>" title="" class="">
                                <img src="/uploads/<%=item.Avatar %>" class="card-img-top" alt="<%=item.Title %>"></a>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title font-weight-bold">
                                <a href="<%=link_target %>" title="" class="load-title"><%=item.Title %>
                                </a>
                            </h5>
                            <p class="card-text">
                                <%=UIHelper.TrimByWord(UIHelper.ClearHtmlTag(item.Sapo),20,"...") %>
                            </p>

                        </div>
                    </div>
                </div>
                <%} %>
                
            </div>
            <div class="row justify-content-center">
                <div>
                    <a href="javascript:void(0)" data-lang="<%=lang %>" data-zone="5192" data-page="2" data-size="6" data-type="1" data-txttype="du-an" role="button" class="btn view-more-projet justify-content-center">Xem thêm các dự án khác
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
        <script src="/Pages/Script/XemThem.js"></script>
    <script src="/Pages/Script/LienHe.js"></script>
</asp:Content>
