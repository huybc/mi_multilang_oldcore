﻿using Mi.BO.Base.News;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi_Company.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mi_Company.Pages
{
    public partial class Mi_ChiTietBlog : BasePages
    {
        public NewsDetailWithLanguageEntity obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var newsId = Utility.ConvertToInt(Page.RouteData.Values["id"]);
                if (newsId > 0)
                {
                    obj = NewsBo.GetNewsDetailWithLanguageById(newsId, Current.LanguageJavaCode);
                    Page.Title = !string.IsNullOrEmpty(obj.MetaTitle) ? obj.MetaTitle : obj.Title;
                    Page.MetaKeywords = obj.MetaKeyword;
                    Page.MetaDescription = obj.MetaDescription;
                }
                else
                {
                    obj = new NewsDetailWithLanguageEntity();

                }
            }
        }
    }
}