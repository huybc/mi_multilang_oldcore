﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/HoangLongMaster.Master" AutoEventWireup="true" CodeBehind="HL_ThuVienAnhDetail.aspx.cs" Inherits="Mi_Company.Pages.HL_ThuVienAnhDetail" %>
<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
    <meta property="fb:app_id" content="1426300910760607" />
    <meta property="og:url" content="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" />
    <meta property="og:type" content="article" />
    <%if (obj != null)
      { %>
        <meta property="og:title" content="<%= obj.Title %>" />
        <meta property="og:description" content="<%= obj.Sapo%>" />
        <meta property="og:image" content="https://rbland.vn/Uploads/<%= obj.Avatar %>" />
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%--<%
        var currentLanguage = Page.RouteData.Values["languageCode"].ToString();
        //var currentType = Page.RouteData.Values["type"].ToString();
        //var type = "";
        var lang = "";
        switch (currentLanguage)
        {
            case "vi":
                lang = "vi-VN";
                break;
            case "en":
                lang = "en-US";
                break;
        }
    %>--%>
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
        %>
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <script type="application/ld+json">
				            {  
                                    "@context":"http://schema.org",
                                    "@type":"Article",
                                    "mainEntityOfPage":{  
                                        "@type":"WebPage",
                                        "@id":"<%=HttpContext.Current.Request.Url.AbsoluteUri %>"
                                    },
                                   "headline":"<%=!string.IsNullOrEmpty(obj.MetaTitle)?obj.MetaTitle:obj.Title%>",
                                    "image":{  
                                        "@type":"ImageObject",
                                        "url":"<%=domainName+"/uploads/"+obj.Avatar %>",
                                        "height":600,
                                        "width":800
                                    },
                                    "datePublished":"<%=obj.CreatedDate %>",
                                    "dateModified":"<%=obj.CreatedDate %>",
                                    "author":{  
                                        "@type":"Person",
                                        "name":"rbland"
                                    },
                                    "publisher":{  
                                        "@type":"Organization",
                                        "name":"rbland.vn",
                                        "logo":{  
                                            "@type":"ImageObject",
                                            "url":"<%=domainName+"/uploads/"+UIHelper.GetConfigByName("LogoTop")%>",
                                            "width":35,
                                            "height":34
                                        }
                                    },
                                    "description":"<%=obj.Sapo %>"
                            }
    </script>
        <section class="py-5">
        <div class="container">
            <div class="row">
               <%//Lay thong tin can thiet %>
                <%var tin_anh_id = int.Parse(Page.RouteData.Values["id"].ToString()); %>
                <%var tin_anh_detail = NewsBo.GetNewsDetailWithLanguageById(tin_anh_id, lang); %>
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-md-9 col-sm-12 col-12 pr-md-5">
                                <h4 class="text-uppercase text-center mb-5">
                                    <%=tin_anh_detail.Title %>
                                </h4>
                                <div class="row">
                                    <%//Cat anh bang regex %>
                                    <%var regex_get_picture = "src=\"(.*?)\"";  %>
                                    <%var list_anh = Regex.Matches(tin_anh_detail.Body, regex_get_picture); %>
                                    <%for (int i = 0; i < list_anh.Count; i++)
                                        { %> 
                                        <div class="col-md-6 col-sm-6 col-12">
                                        <div class="image-item">
                                            <a href="javascript:void(0)" title="">
                                                <img <%=list_anh[i].Value %> class="img-fluid" alt="<%=domainName %>"/>
                                            </a>
                                        </div>
                                    </div>
                                    <%} %>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-12  ">
                                <div class="menu-right">
                                    <div class="heading">
                                        <%=Language.Interested %>
                                    </div>
                                    <%//Lay list 6 bai viet moi nhat %>
                                    <%var more_row = 0; %>
                                    <%var list_6_newest_news = NewsBo.GetNewsDetailWithLanguage(14, lang, 2, 0, string.Empty, 1, 6, ref more_row); %>
                                    <div class="list-link">
                                        <%foreach (var item in list_6_newest_news)
                                            { %> 
                                            <div class="item-link">
                                           <h4 class="title"> <a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(item.Type) %>/<%=item.Alias %>/<%=item.Url %>.<%=item.NewsId %>.htm" title=""><%=item.Title %></a></h4>
                                           <p class="des"><%=item.Sapo %></p>
                                        </div>
                                        <%} %>
                                    </div>
                                </div>
                            </div>
                        </div>
                    

                </div>
            </div>
        </div>
    </section>
    <section class="res">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <iframe width="100%" height="280" src="<%=UIHelper.GetConfigByName_v1("Videohome",lang).Replace("watch?v=","/embed/") %>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 pl-md-5">
                    <%var c = UIHelper.GetConfigByName_v1("TieuDeDongVideo", lang); %>
                    <%var line_videos = UIHelper.CutContentByMinusCharactor(c); %>
                    <div class="heading mt-4 mt-md-0">
                        <%=line_videos[0] %>
                        <div>
                            <%=line_videos[1] %>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" id="txtName" class="form-control" placeholder="<%=Language.Full_Name %>" />
                    </div>
                    <div class="form-group">
                        <input type="text" id="txtPhone" class="form-control" placeholder="<%=Language.Phone_Number %>" />
                    </div>
                    <div class="form-group">
                        <input type="text" id="txtAddress" class="form-control" placeholder="<%=Language.Address %>" />
                    </div>
                    <div class="form-group">
                        <a href="javascript:void(0)" id="dat-lien-he" class="btn"><%=Language.Register %></a>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
</asp:Content>
