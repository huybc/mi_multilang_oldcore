﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/MiWBanner.Master" AutoEventWireup="true" CodeBehind="Mi_About.aspx.cs" Inherits="Mi_Company.Pages.Mi_About" %>

<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%//Lay thong tin about %>
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        string domainNameFull = HttpContext.Current.Request.Url.LocalPath;
        var tar = domainNameFull.Split('/').ToList();
        var banner_link = "";
        if (tar[2].Equals("gioi-thieu"))
            banner_link = UIHelper.GetConfigByName("BannerGioiThieu");
    %>
    <style>
        .banner-recruitment {
            background: url(/uploads/<%=banner_link%>);
        }

        @media(min-width:768px) {
            .banner-recruitment {
                background: url(/uploads/<%=banner_link%>) no-repeat center center;
                background-size: cover;
                min-height: 400px;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        string domainNameFull = HttpContext.Current.Request.Url.LocalPath;
    %>
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <%var banner_gt_1 = UIHelper.GetConfigByName("BannGioiThieu1"); %>
    <%--<h1><%=domainNameFull %></h1>--%>
    <%--<h1>Ready for about</h1>--%>
    <%--<h1><%=banner_gt_1 %></h1>--%>
    
    <section class="bn">
        <a href="javascript:void(0)">
            <img src="/uploads/<%=banner_gt_1 %>" class="w-100" alt="<%=domainName %>"/>
        </a>
    </section>
    <section class="py-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-10 col-md-11 col-sm-12 col-12">
                    <h3 class="font-weight-bold text-center mb-4"><%=UIHelper.GetConfigByName("TieuDeGioiThieu1") %></h3>
                </div>
                <div class="col-xl-8 col-md-9 col-sm-12 col-12">
                    <div class="h5 text-center mb-4">
                        <%=UIHelper.GetConfigByName("NoiDungGioiThieu1") %>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a href="javascript:void(0)" class=" btn btn-detail-re"><i class="fas fa-arrow-circle-down"></i>Download tài liệu về
                    Migroup</a>
            </div>
        </div>

    </section>

    <section class="intro py-4">
        <div class="container">
            <div class="row">
                <%var slider_gt_1 = ConfigBo.AdvGetByType(7, lang); %>
                <%foreach(var item in slider_gt_1){ %> 
                <div class="col-md-4 col-sm-4 col-12">
                    <div class="item text-center">
                        <h5 class="title mb-3"><%=item.Name %></h5>
                        <div class="image mb-3 ">
                            <img src="/uploads/<%=item.Thumb %>" class="img-fluid" />
                        </div>
                        <div class="h5">
                            <%=item.Content %>
                        </div>
                        <a href="<%=item.Url %>" class=" btn view-more-projet">Xem chi tiết<i class="fas fa-angle-double-right ml-2"></i></a>
                    </div>
                </div>
                <%} %>
            </div>
        </div>
    </section>

    <section class="projects py-4">
        <div class="container">
            <div class="heading-ss-home">
                <%=UIHelper.GetConfigByName("TieuDeDong3") %>
            </div>
            <div class="row ">
                <%var total_1 = 0;
                    %>
                <%var list_6_du_an = NewsBo.GetNewsDetailWithLanguage(1, lang, 2, 0, "", 1, 6, ref total_1); %>
                <%foreach(var item in list_6_du_an){ %> 
                <%var link_target = string.Format("/{0}/du-an/{1}-{2}.htm",currentLanguage,item.Url,item.NewsId);//Cho nay set link sau %>
                    <div class="col-xl-4 col-lg-4 col-sm-6 col-12 item-load-trang pull-left">
                    <div class="card mi-card mb-4">
                        <div class="image">
                            <a href="<%=link_target %>" title="" class="">
                                <img src="/uploads/<%=item.Avatar %>" class="card-img-top" alt="<%=item.Title %>"></a>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title font-weight-bold">
                                <a href="<%=link_target %>" title="" class="load-title"><%=item.Title %>
                                </a>
                            </h5>
                            <p class="card-text">
                                <%=UIHelper.TrimByWord(UIHelper.ClearHtmlTag(item.Sapo),20,"...") %>
                            </p>

                        </div>
                    </div>
                </div>
                <%} %>
                
            </div>
            <div class="row justify-content-center">
                <div>
                    <a href="javascript:void(0)" data-lang="<%=lang %>" data-zone="0" data-page="2" data-size="6" data-type="1" data-txttype="du-an" role="button" class="btn view-more-projet justify-content-center">Xem thêm các dự án khác
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="number py-5">
        <div class="container">
            <h4 class="text-center font-weight-bolder text-uppercase mb-4">Những con số ấn tượng</h4>
            <div class="row justify-content-center">
                <%var slider_gt_2 = ConfigBo.AdvGetByType(8, lang); %>
                <%foreach (var item in slider_gt_2)
                    { %> 
                    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6">
                    <div class="item">
                        <div class="image mb-3">
                            <img src="/uploads/<%=item.Thumb %>" class="img-fluid" alt="<%=domainName %>"/>
                        </div>
                        <div class="amount h4 font-weight-bolder">
                            <%=item.Name %> 
                        </div>
                        <div class="text">
                            <%=item.Content %>
                        </div>
                    </div>
                </div>
                <%} %>

            </div>
        </div>
    </section>

    <section class="py-5">
        <div class="container text-center">
            <div class="row justify-content-center">
                <div class="col-xl-8 col-lg-10 col-md-12 col-sm-12 col-12 h6">
                    <div class="color-F65D2D mb-3 h5">
                        <%=UIHelper.GetConfigByName("ChuCamGioiThieu") %>
                    </div>
                    <h4 class="font-weight-bolder mb-3"><%=UIHelper.GetConfigByName("TieuDeGioiThieu2") %>
                    </h4>
                    <div class="h5 mb-4 " style="color: #929292;">
                        <div class="mb-2">
                            <%=UIHelper.GetConfigByName("NoiDungGioiThieu2") %> 
                        </div>
                        <div>
                            <%=UIHelper.GetConfigByName("NoiDungGioiThieu2.1") %>
                        </div>
                    </div>
                    <a href="/<%=currentLanguage %>/lien-he" class=" btn btn-detail-re">Liên hệ ngay
                    </a>

                </div>
            </div>
        </div>
    </section>


    <section class="map">
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.4494941266425!2d105.7857861144065!3d20.974611595001694!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acd3d8a7e509%3A0x2c132edb57f47f5c!2zUmFpbmJvdyBWxINuIFF1w6Fu!5e0!3m2!1svi!2s!4v1580795720735!5m2!1svi!2s"
            width="100%" height="450" frameborder="0" style="border: 0;" allowfullscreen=""></iframe>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <script src="/Pages/Script/XemThem.js"></script>
    <script src="/Pages/Script/LienHe.js"></script>
</asp:Content>
