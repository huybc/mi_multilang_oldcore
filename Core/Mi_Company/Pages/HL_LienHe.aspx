﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/HoangLongMaster.Master" AutoEventWireup="true" CodeBehind="HL_LienHe.aspx.cs" Inherits="Mi_Company.Pages.HL_LienHe" %>

<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%--<%
        var currentLanguage = Page.RouteData.Values["languageCode"].ToString();
        ///var currentType = Page.RouteData.Values["type"].ToString();
        //var type = "";
        var lang = "";
        switch (currentLanguage)
        {
            case "vi":
                lang = "vi-VN";
                break;
            case "en":
                lang = "en-US";
                break;
        }


    %>--%>
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <div class="container">
        <section class="bg-lien-he">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10 col-md-11 col-12">
                        <div class="lh-box-infor mb-4">
                            <h1 class="title"><%=UIHelper.GetConfigByName_v1("TieuDeLienHe",lang) %></h1>
                            <ul class="pl-md-5">
                                <li>
                                    <label class="font-weight-bold mr-1"><i class="fas fa-phone-volume mr-2"></i><%=Language.Hotline %>:</label>
                                    <%=UIHelper.GetConfigByName_v1("DienThoai1",lang) %> - <%=UIHelper.GetConfigByName_v1("DienThoai2",lang) %>
                                </li>
                                <li>
                                    <label class="font-weight-bold mr-1"><i class="fas fa-map-marker-alt mr-2"></i><%=Language.Place %>:</label><%=UIHelper.GetConfigByName_v1("TruSo",lang) %>
                                </li>
                                <li>
                                    <label class="font-weight-bold mr-1"><i class="fas fa-envelope mr-2"></i>Email:</label>
                                    <%=UIHelper.GetConfigByName_v1("Email",lang) %>
                                </li>
                                <li>
                                    <label class="font-weight-bold mr-1"><i class="fab fa-facebook-square mr-2"></i>Facebook:</label>https://www.facebook.com/phongkhamdakhoahoanglong/
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5 col-12">
                        <div class="form-group">
                            <input type="text" id="txtName" class="form-control form-lh" placeholder="<%=Language.Full_Name%>">
                        </div>
                        <div class="form-group">
                            <input type="text" id="txtPhoneNumber" class="form-control form-lh" placeholder="<%=Language.Phone_Number %>">
                        </div>
                        <div class="form-group">
                            <input type="text" id="txtEmail" class="form-control form-lh" placeholder="Email">
                        </div>

                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="form-group">
                            <textarea type="text" rows="7" id="txtNote" class="form-control form-lh" placeholder="<%=Language.Note_Detail %>"></textarea>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-11 col-12">
                        <div class="form-group">
                            <a href="javascript:void(0)" id="dat-lien-he" class="btn form-lh"><%=Language.Send%></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <div class="heading-lh">
                    ĐỊa chỉ phòng khám
                </div>
            </div>
        </section>
        <section class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6065913241036!2d105.83520411440695!3d21.008401393850022!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac7883ec4e51%3A0x1c2aaa7ccbb2dc42!2zUGjDsm5nIGtow6FtIMSRYSBraG9hIEhvw6BuZyBMb25n!5e0!3m2!1svi!2s!4v1577282083190!5m2!1svi!2s" width="100%" height="600" frameborder="0" style="border: 0;" allowfullscreen=""></iframe>
        </section>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
    <script type="text/javascript">
        $('#dat-lien-he').off('click').on('click', function () {
            var name = $('#txtName').val();
            var phoneNumber = $('#txtPhoneNumber').val();
            var email = $('#txtEmail').val();
            var note = $('#txtNote').val();
            var type = "lien-he";
            R.Post({
                params: {
                    name: name,
                    email: email,
                    phoneNumber: phoneNumber,
                    note: note,
                    type: type
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "save",
                success: function (res) {
                    if (res.Success) {
                        console.log(1);
                        alert("Cảm ơn Quý khách đã để lại thông tin! Chúng tôi sẽ liên hệ lại sớm!");
                    }
                    // $('.card-header').RLoadingModuleComplete();
                }
            });
        })
    </script>
</asp:Content>
