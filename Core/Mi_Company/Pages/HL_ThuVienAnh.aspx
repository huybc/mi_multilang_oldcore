﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/HoangLongMaster.Master" AutoEventWireup="true" CodeBehind="HL_ThuVienAnh.aspx.cs" Inherits="Mi_Company.Pages.HL_ThuVienAnh" %>

<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%--<%
        var currentLanguage = Page.RouteData.Values["languageCode"].ToString();
        //var currentType = Page.RouteData.Values["type"].ToString();
        //var type = "";
        var lang = "";
        switch (currentLanguage)
        {
            case "vi":
                lang = "vi-VN";
                break;
            case "en":
                lang = "en-US";
                break;
        }


    %>--%>
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12 col-12 pr-md-5">
                            <%//Lay thong tin can thiet o day %>
                            <%var zone_galary = ZoneBo.GetZoneWithLanguageByType(17, lang); %>
                            <%var zone_galary_hinhanh = zone_galary.Where(r => r.Name.Equals("ẢNH")).SingleOrDefault(); %>
                            <%var zone_galary_video = zone_galary.Where(r => r.Name.Equals("VIDEO")).SingleOrDefault(); %>
                            <%var totalR = 0; %>
                            <%var page_index = 1; %>
                            <%var page_size = 1; %>
                            <%var tin_anh = NewsBo.GetNewsDetailWithLanguage(17, lang, 2, zone_galary_hinhanh.Id, string.Empty, page_index, page_size, ref totalR).SingleOrDefault(); %>

                            <div class="gallery-image">
                                <div class="py-2 border-top">
                                </div>
                                <h4 class="text-uppercase mb-3">
                                    <%=UIHelper.GetConfigByName_v1("TieuDeHinhAnh",lang) %>
                                </h4>
                                <p class="h5 mb-4 title_tin_anh">
                                    <b>
                                        <%=tin_anh.Title %>
                                    </b>
                                </p>
                                <%//Lay ra 3 hinh anh dau tien cua tin de cho len slide %>
                                <%var regex_get_picture = "src=\"(.*?)\"";  %>
                                <%var list_anh = Regex.Matches(tin_anh.Body, regex_get_picture); %>
                                <div id="slide-image" class="carousel slide slide-td mb-3" data-ride="carousel">

                                    <div class="carousel-inner" id="carousel-binding">
                                        <%for (int i = 0; i < list_anh.Count; i++)
                                            { %>
                                        <div class="carousel-item <%=i==1?"active":"" %>">
                                            <img <%=list_anh[i].Value %> class="d-block w-100" alt="<%=tin_anh.Title %>">
                                        </div>
                                        <%} %>
                                    </div>
                                    <a class="carousel-control-prev" href="#slide-image" role="button" data-slide="prev">
                                        <i class="fas fa-chevron-circle-left" aria-hidden="true"></i>
                                    </a>
                                    <a class="carousel-control-next" href="#slide-image" role="button" data-slide="next">
                                        <i class="fas fa-chevron-circle-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <div class="des mb-4">
                                    <span id="binding-sapo"><%=tin_anh.Sapo %></span>
                                    <a class="a-link" href="/<%=currentLanguage %>/thu-vien-anh/<%=tin_anh.Url %>.<%=tin_anh.NewsId %>.htm" class="btn-link"><%=Language.More %></a>
                                </div>
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination pagination-hl justify-content-center mb-5">
                                        <%for (int i = 0; i < totalR; i++)
                                            { %>
                                        <li class="page-item <%=i==0?"active":"" %>"><a class="page-link" data-page="<%=i+1 %>" data-type="<%=tin_anh.Type %>" data-lang="<%=lang %>" data-hot="2" data-zoneid="<%=zone_galary_hinhanh.Id %>" data-size="1" href="javascript:void(0)"><%=i+1 %></a></li>
                                        <%} %>
                                    </ul>
                                </nav>
                            </div>
                            <div class="gallery-video">
                                <div class="py-2 border-top">
                                </div>
                                <h4 class="text-uppercase mb-3">VIDEO
                                </h4>
                                <p class="h5 mb-2">
                                    <b>
                                        <%=UIHelper.GetConfigByName_v1("TieuDeVideo", lang) %>
                                    </b>
                                </p>
                                <p class="mb-4">
                                    <%=UIHelper.GetConfigByName_v1("NoiDungVideo",lang) %>
                                </p>

                                <div id="slide-video" class="carousel slide slide-video mb-3" data-ride="carousel">
                                    <%//Lay thong tin can thiet %>
                                    <%var totalR_1 = 0; %>
                                    <%var list_tin_video = NewsBo.GetNewsDetailWithLanguage(zone_galary_video.Type, lang, 2, zone_galary_video.Id, string.Empty, 1, 100, ref totalR_1).ToList();  %>
                                    <div class="carousel-inner">
                                        <%for (int i = 0; i < list_tin_video.Count(); i++)
                                            { %>
                                        <%
                                            string data = list_tin_video[i].Body;
                                            string reg = "\"(h.*?)\"";
                                            data = Regex.Matches(data, reg)[0].ToString().Replace("\"", "").Replace("\"", "").Replace(@"watch?v=", @"embed\");
                                        %>
                                        <div class="carousel-item <%=i==0?"active":"" %> carousel-item-left">
                                            <iframe width="100%" height="400" src="<%=data %>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        </div>
                                        <%} %>
                                    </div>
                                    <a class="carousel-control-prev" href="#slide-video" role="button" data-slide="prev">
                                        <i class="fas fa-chevron-circle-left" aria-hidden="true"></i>
                                    </a>
                                    <a class="carousel-control-next" href="#slide-video" role="button" data-slide="next">
                                        <i class="fas fa-chevron-circle-right" aria-hidden="true"></i>
                                    </a>
                                    <ol class="carousel-indicators">
                                        <%for (int i = 0; i < list_tin_video.Count(); i++)
                                            { %>
                                        <li data-target="#slide-video" data-slide-to="<%=i %>" class="">
                                            <img src="/uploads/thumb/<%=list_tin_video[i].Avatar %>" class="d-block w-100" alt="<%=list_tin_video[i].Title %>">
                                        </li>
                                        <%} %>
                                    </ol>
                                </div>

                            </div>



                        </div>
                        <div class="col-md-3 col-sm-12 col-12  ">
                            <div class="qc-left">
                                <a href="">
                                    <%string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority); %>
                                    <img src="/uploads/<%=UIHelper.GetConfigByName_v1("QuangCaoTrangTieuDe",lang) %>" alt="<%=domainName %>" class="img-fluid" /></a>
                            </div>
                            <div class="menu-right">
                                <div class="heading">
                                    <%=Language.Interested %>
                                </div>
                                <%//Lay list 6 bai viet moi nhat %>
                                <%var more_row = 0; %>
                                <%var list_6_newest_news = NewsBo.GetNewsDetailWithLanguage(14, lang, 2, 0, string.Empty, 1, 4, ref more_row); %>
                                <div class="list-link">
                                    <%foreach (var item in list_6_newest_news)
                                        { %>
                                    <div class="item-link">
                                        <h4 class="title"><a href="/<%=currentLanguage %>/<%=UIHelper.GetTypeUrlByHoangLongProject(item.Type) %>/<%=item.Alias %>/<%=item.Url %>.<%=item.NewsId %>.htm" title=""><%=item.Title %></a></h4>
                                        <p class="des"><%=item.Sapo %></p>
                                    </div>
                                    <%} %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <div class="container">
        <section class="res" style="margin-bottom:3px;">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <iframe width="100%" height="280" src="<%=UIHelper.GetConfigByName_v1("Videohome",lang).Replace("watch?v=","/embed/") %>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 pl-md-5">
                        <%var c = UIHelper.GetConfigByName_v1("TieuDeDongVideo", lang); %>
                        <%var line_videos = UIHelper.CutContentByMinusCharactor(c); %>
                        <div class="heading mt-4 mt-md-0">
                            <%=line_videos[0] %>
                            <div>
                                <%=line_videos[1] %>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" id="txtName" class="form-control" placeholder="<%=Language.Full_Name%>" />
                        </div>
                        <div class="form-group">
                            <input type="text" id="txtPhone" class="form-control" placeholder="<%=Language.Phone_Number%>" />
                        </div>
                        <div class="form-group">
                            <input type="text" id="txtAddress" class="form-control" placeholder="<%=Language.Address%>" />
                        </div>
                        <div class="form-group">
                            <a href="javascript:void(0)" id="dat-lien-he" class="btn"><%=Language.Register%></a>
                        </div>
                    </div>
                </div>
            </div>
   
    </section>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
    <script type="text/javascript">
        $('.page-link').off('click').on('click', function () {
            console.log($(this).data('page'));
            //Xoa het class active tren list
            $('.page-item').each(function (i, v) {
                $(v).removeClass('active');
            })
            $(this).parent().addClass('active');
            var lang = $(this).data('lang');
            //Lay du lieu theo phan trang
            R.Post({
                params: {
                    type: $(this).data('type'),
                    lang: $(this).data('lang'),
                    hot: 2,
                    zone_id: $(this).data('zoneid'),
                    search: "",
                    pageIndex: $(this).data('page'),
                    pageSize: 1
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "load-trang",
                success: function (res) {
                    if (res.Success) {
                        var result = res.Data[0];
                        console.log(result);
                        var regex_get_picture = /src=\"(.*?)\"/g;
                        var body = result.Body;
                        var matchs = body.match(regex_get_picture);
                        console.log(matchs);
                        var el = $('.gallery-image');
                        el.find('.title_tin_anh').text(result.Title);
                        var carousel = '';
                        for (var i = 0; i < matchs.length; i++) {
                            var ac = i == 0 ? 'active' : ''
                            carousel += '<div class="carousel-item ' + ac + '">';
                            carousel += '<img ' + matchs[i] + ' class="d-block w-100" alt="' + result.Title + '">';
                            carousel += '</div>';
                        }
                        console.log(carousel);
                        $('#carousel-binding').html(carousel);
                        el.find('#binding-sapo').html(result.Sapo);
                        //var link = "/" + lang.split('-')[0] + "/" + currentType + "/" + e.Alias + "/" + e.Url + "." + e.NewsId + ".htm";
                        var link = "/" + lang.split('-')[0] + "/" + "thu-vien-anh" + "/" + result.Url + "." + result.NewsId + ".htm";
                        console.log(link);
                        el.find(".a-link").attr('href', link);

                        //Xu ly hau ky
                        if (result.length > 0) {
                            $('#xem-them-btn').data('index', pageIndex);
                            var index = $('#xem-them-btn').data('index');
                            $('.page-now').html(index);
                        }

                    }

                }, error: function () {
                    //$('#contact').RLoadingModuleComplete();
                }
            });
        })
        $('#dat-lien-he').off('click').on('click', function () {
            var name = $('#txtName').val();
            var phoneNumber = $('#txtPhoneNumber').val();
            //var email = $('#txtEmail').val();
            var address = $('#txtAddress').val();
            var type = "lien-he";
            R.Post({
                params: {
                    name: name,
                    address: address,
                    phoneNumber: phoneNumber,
                    //note: note,
                    type: type
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "save",
                success: function (res) {
                    if (res.Success) {
                        console.log(1);
                        alert("Cảm ơn Quý khách đã để lại thông tin! Chúng tôi sẽ liên hệ lại sớm!");
                        R.Post({
                            params: {
                                name: name,
                                //email: email,
                                phoneNumber: phoneNumber,
                                //note: note,
                                type: type,
                                //ngaySinh: birthday,
                                //ngayKham: ngaykham,
                                //address: address,
                                //location: location,
                                //coSo: coSo

                            },
                            module: "ui-action",
                            ashx: 'modulerequest.ashx',
                            action: "send_mail",
                            success: function (res) {
                                console.log('Send mail Successful!');
                            }
                        });
                    }
                    // $('.card-header').RLoadingModuleComplete();
                }
            });
        })
    </script>
</asp:Content>
