﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/HoangLongMaster.Master" AutoEventWireup="true" CodeBehind="HL_XacNhanDatLich.aspx.cs" Inherits="Mi_Company.Pages.HL_XacNhanDatLich" %>

<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Customer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%
        //var currentLanguage = Page.RouteData.Values["languageCode"].ToString();
        /////var currentType = Page.RouteData.Values["type"].ToString();
        ////var type = "";
        //var lang = "";
        //switch (currentLanguage)
        //{
        //    case "vi":
        //        lang = "vi-VN";
        //        break;
        //    case "en":
        //        lang = "en-US";
        //        break;
        //}

        var customerId = int.Parse(Request.QueryString["id"].ToString());

        var khach_hang_detail = CustomerBo.GetById(customerId);

    %>
    <%
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
        //Tinh tuoi khach hang
        var tuoiKhachHang = DateTime.Now.Year - khach_hang_detail.NgaySinh.Year;
    %>
    <div class="container">
        <section class="bg-dat-lich">

            <div class="row justify-content-center">
                <div class="col-lg-11 col-md-12 col-sm-12 col-12">
                    <div class="font-24" style="font-size:14px;">
                        <div class=" mb-4">
                            <div class="text-uppercase font-weight-bold mb-2"><%=UIHelper.GetConfigByName_v1("TitleXacNhanDatLich",lang) %><%--<i class="fas fa-check-circle ml-3" style="color: #81FF03;"></i>--%></div>
                            <div><%=UIHelper.GetConfigByName_v1("NoiDungXacNhanDatLich_1",lang) %></div>
                            <div><%=UIHelper.GetConfigByName_v1("NoiDungXacNhanDatLich_2",lang) %></div>
                        </div>
                        <ul class="list-infor mb-5">
                            <li>
                                <label class="mr-1"><%=Language.Full_Name %>:</label><span class="font-weight-bold"><%=khach_hang_detail.FullName %></span>
                            </li>
                            <li>
                                <label class="mr-1"><%=Language.Date_Of_Birth %>: </label>
                                <span class="font-weight-bold"><%=UIHelper.MultiLanguageManual(lang,"Ngày","Date") %> <%=khach_hang_detail.NgayKham.ToString("dd/MM/yyyy") %></span>
                            </li>
                            <li>
                                <label class="mr-1"><%=Language.Phone_Number %>: </label>
                                <span class="font-weight-bold"><%=khach_hang_detail.Mobile %></span>
                            </li>
                            <li>
                                <label class="mr-1"><%=Language.Address %>: </label>
                                <span class="font-weight-bold"><%=khach_hang_detail.Address %></span>
                            </li>
                            <li>
                                <label class="mr-1"><%=UIHelper.MultiLanguageManual(lang,"Tỉnh thành phố","Location") %>: </label>
                                <span class="font-weight-bold"><%=khach_hang_detail.location %></span>
                            </li>
                            <li>
                                <label class="mr-1"><%=Language.HospitalDate %>: </label>
                                <span class="font-weight-bold"><%=UIHelper.MultiLanguageManual(lang,"Ngày","Date") %> <%=khach_hang_detail.NgayKham.ToString("dd/MM/yyyy") %></span>
                            </li>
                            <li>
                                <%//Lay ra co so kham cua khach hang %>
                                <%var co_so_kham = ZoneBo.GetZoneWithLanguageByType(18, lang).Where(r => r.Id == khach_hang_detail.CoSoKham).SingleOrDefault(); %>
                                <label class="mr-1"><%=UIHelper.MultiLanguageManual(lang,"Cơ sở","Location") %>: </label>
                                <span class="font-weight-bold"><%=co_so_kham.Content %></span>
                            </li>
                            <li>

                                <label class="mr-1"><%=Language.Note %>: </label>
                                <span style="color: #FF0000;" class="font-weight-bold font-italic"><%=(tuoiKhachHang >=10&&tuoiKhachHang<=70) ? UIHelper.GetConfigByName_v1("LoiNhanDatLich_LonHon18T",lang) : UIHelper.GetConfigByName_v1("LoiNhanDatLich_NhoHon18T",lang) %>
                                </span>
                            </li>
                        </ul>
                        <div class=" mb-4">
                            <a href="/<%=currentLanguage %>" class="btn btn-dat-lich mr-3" style="background: #438F8C;"><i class="fas fa-home mr-2"></i><%=Language.BackHome %></a>
                            <a href="/<%=currentLanguage %>/dat-lich-kham" class="btn btn-dat-lich"><i class="far fa-calendar-alt mr-2"></i><%=Language.Another_Booking %></a>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
</asp:Content>
