﻿using Mi_Company.Core.Helper;
using Mi.BO.Base.Zone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mi_Company.Pages
{
    public partial class HL_DSBaiVietChuyenKhoa : BasePages
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var zone_alias = Page.RouteData.Values["zone"].ToString();
            var zone_tar = ZoneBo.GetZoneByAlias(zone_alias, Current.LanguageJavaCode);

            if (!Page.IsPostBack)
            {
                Page.Title = zone_tar.Name;
                Page.MetaKeywords = zone_tar.MetaKeyword;
                Page.MetaDescription = zone_tar.MetaDescription;
            }
        }
    }
}