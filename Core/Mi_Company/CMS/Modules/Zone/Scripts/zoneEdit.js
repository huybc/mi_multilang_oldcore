﻿R.Zone = {
    Init: function () {
        R.Zone.RegisterEvents();
        this.zoneLanguageId = 0;
        this.ZoneId = $('#zone-edit').attr('zone-id');
        CKEDITOR.replace('contentTxt', { toolbar: 'iweb' });
        $('select').fselect({
            dropDownWidth: 0,
            autoResize: true
        });
        $('#_zoneddl').val($('#_zoneddl').attr('data'));
        $('#colDdl').val($('#colDdl').attr('data'));
        $('#_zoneddl,#colDdl').trigger('fselect:updated');

    },
    RegisterEvents: function () {

       
        $('#zones-language ._edit').off('click').on('click',
            function (e) {
                var $this = $(this);
                var id = $this.parent().attr('data-id');
                R.Post({
                    params: { id: id },
                    module: "zone",
                    ashx: 'modulerequest.ashx',
                    action: "zone-language-get-id",
                    success: function (res) {
                        if (res.Success) {
                            var resultJson = res.Data;
                            $('#languageDdl').val(resultJson.LanguageCode);
                            $('#languageDdl').trigger('fselect:updated');
                            $('#nameLanguageTxt').val(resultJson.Name);
                            $('#metaTitletxt').val(resultJson.MetaTitle);
                            $('#metaDescriptionTxt').val(resultJson.MetaDescription);
                            $('#metaKeywordTxt').val(resultJson.MetaKeyword);

                            CKEDITOR.instances['contentTxt'].setData(resultJson.Content);
                            R.Zone.zoneLanguageId = resultJson.Id;

                            $.smoothScroll({
                                scrollTarget: '#zone-edit',
                                speed: 100,
                                easing: 'swing',
                            });
                        } else {
                            $.notify(res.Message, {
                                autoHideDelay: 3000, className: "error",
                                globalPosition: 'right top'
                            });

                        }
                        $('#zone-edit').RModuleUnBlock();
                    }, error: function () {

                    }
                });
            });
        $('#zones-language ._delete').off('click').on('click',
            function (e) {
                var $this = $(this);
                var id = $this.parent().attr('data-id');
                $.confirm({
                    title: 'Thông báo !',
                    type: 'red',
                    content: 'Xác nhận xóa',
                    animation: 'scaleY',
                    closeAnimation: 'scaleY',
                    buttons: {
                        yes: {
                            text: 'Xóa',
                            keys: ['ESC'],
                            action: function () {
                                R.Post({
                                    params: { id: id },
                                    module: "zone",
                                    ashx: 'modulerequest.ashx',
                                    action: "zone-language-remove",
                                    success: function (res) {
                                        if (res.Success) {

                                            $this.closest('tr').remove();
                                            $.notify("Xóa thành công !", {
                                                autoHideDelay: 3000, className: "success",
                                                globalPosition: 'right top'
                                            });



                                        } else {
                                            $.notify(res.Message, {
                                                autoHideDelay: 3000, className: "error",
                                                globalPosition: 'right top'
                                            });

                                        }
                                        $('#zone-edit').RModuleUnBlock();
                                    }, error: function () {

                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Đóng',
                            btnClass: 're-btn re-btn-default'
                        }
                    }

                });

            });

        $('.zone-action ._edit').off('click').on('click', function (e) {
            var id = $(this).parent().attr('data-id');
            R.Zone.Edit(id);
        });

        $('.zone-action .status').off('click').on('click', function (e) {
            var id = $(this).parent().attr('data-id');
            var status = $(this).attr('data');
            R.Zone.XUpdateStatus({
                status: status,
                id: id
            });
        });




        var delay = 0;
        $('#cms-sidebar li').off('click').on('click',
            function () {
                $('.navigation-main li').removeClass('active');
                $('#news-sldebar .navigation li').removeClass('active');
                $(this).addClass('active');
                if (delay != null) clearTimeout(delay);
                delay = setTimeout(function () {
                    R.Zone.XNews();
                },
                    200);

            });

        $('#zoneInLanguageSaveBtn').off('click').on('click',
            function () {
                var data = {
                    id: R.Zone.zoneLanguageId,
                    zone_id: R.Zone.ZoneId,
                    name: $('#nameLanguageTxt').val(),
                    language_code: $('#languageDdl').val(),
                    content: CKEDITOR.instances['contentTxt'].getData(),
                    metakey: $('#metaKeywordTxt').val(),
                    metades: $('#metaDescriptionTxt').val(),
                    metatitle: $('#metaTitletxt').val()

                }
                if (R.Zone.ZoneId == 0) {
                    $.notify("Thông tin cần được hoàn thành!", {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });
                    return false;
                }
                if (R.Zone.zoneLanguageId == 0) {
                    var flag = 0;

                    for (var j = 0; j < $('#zone-language tr').length; j++) {

                        if ($('#languageDdl').val() == $('#zone-language tr:eq(' + j + ')').attr('data')) {
                            flag++;
                            $.notify("Đã tồn tại ngôn ngữ này!",
                                {
                                    autoHideDelay: 3000,
                                    className: "error",
                                    globalPosition: 'right top'
                                });
                            break;
                        }

                    }
                    if (flag > 0) {
                        return false;
                    }
                }
                R.Zone.SaveZoneInLanguage(data);

            });

        $('#zoneSaveBtn').off('click').on('click',
            function () {

                var data = {
                    id: R.Zone.ZoneId,
                    name: $('#zoneNameTxt').val(),
                    col: $('#colDdl').val(),
                    parent_id: $('#_zoneddl').val(),
                    sort: $('#sortZoneTxt').val(),
                    avatar: $('#attack-thumb img').attr('data-img'),
                    icon: $('#zone-edit').attr('zone-type') == 20 ?  $('#attack-icon img').attr('data-img'):'',
                    type: $('#zone-edit').attr('zone-type'),
                    isDisplayHome: $('#isShowHomaPage').is(":checked")

                }
                R.Zone.Save(data);
            });

        $('#attack-files .avatar').off('click').on('click', function () {

            var $this = $(this);
            R.Image.SingleUpload($this,
                function () {
                    //process
                }, function (response, status) {
                    $($this).RModuleUnBlock();
                    //success
                    if (!response.success) {
                        $.confirm({
                            title: 'Thông báo !',
                            type: 'red',
                            content: response.messages,
                            animation: 'scaleY',
                            closeAnimation: 'scaleY',
                            buttons: {
                                cancel: {
                                    text: 'Đóng',
                                    btnClass: 're-btn re-btn-default'
                                },
                                yes: {
                                    isHidden: true, // hide the button
                                    keys: ['ESC'],
                                    action: function () {

                                    }
                                }
                            }

                        });
                        return;
                    }

                    if (response.success && response.error == 200) {
                        var img = response.path;
                        $this.find('img').attr({
                            'src': img + '?w=150&h=100&mode=crop',
                            'data-img': response.name
                        });
                    }

                });

        });




    },
    Save: function (data) {
        $('#zone-edit').RModuleBlock();
        R.Post({
            params: data,
            module: "zone",
            ashx: 'modulerequest.ashx',
            action: "save",
            success: function (res) {
                if (res.Success) {


                    $.notify("Lưu thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });

                    var resultJson = res.Data;
                    location.href = '/cpanel/conf/zone/edit-' + resultJson.Type + '-' + resultJson.Id + '.htm';

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                $('#zone-edit').RModuleUnBlock();
            }, error: function () {

            }
        });
    },
    SaveZoneInLanguage: function (data) {
        $('#zone-edit').RModuleBlock();
        R.Post({
            params: data,
            module: "zone",
            ashx: 'modulerequest.ashx',
            action: "save-language",
            success: function (res) {
                if (res.Success) {


                    $.notify("Lưu thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    var resultJson = res.Data;
                    var htm = '<tr data=' + resultJson.LanguageCode + '>' +
                        '<td>' + $('#languageDdl option:selected').text() + '</td>' +
                        '<td>' + resultJson.Name + '</td>' +
                        '<td class="text-right" data-id=' + resultJson.Id + '>' +
                        '<a class="_edit" style="padding-right: 20px" href="javascript:void(0)"><i class="icon-pencil7"></i></a>' +
                        '<a class="_delete" href="javascript:void(0)"><i class="icon-bin"></i></a>' +
                        '</td>' +
                        '</tr>';
                    if (data.id==0) {
                        $('#zone-language').append(htm);
                    }

                    $('#nameLanguageTxt').val('');
                    CKEDITOR.instances['contentTxt'].setData('');
                    $('#metaKeywordTxt').val('');
                    $('#metaDescriptionTxt').val('');
                    $('#metaTitletxt').val('');
                    R.Zone.zoneLanguageId = 0;
                    R.Zone.RegisterEvents();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                $('#zone-edit').RModuleUnBlock();
            }, error: function () {

            }
        });
    }




}
$(function () {
    R.Zone.Init();
});