﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuLeft.ascx.cs" Inherits="Mi_Company.CMS.Modules.Zone.MenuLeft" %>
<div class="sidebar sidebar-main" id="cms-sidebar">
    <!-- Main navigation -->
    <div class="sidebar-category sidebar-category-visible">
        <div class="category-content no-padding">
            <ul class="navigation navigation-main navigation-accordion">
                <li class="navigation-header"><span>Danh mục</span> <i class="icon-menu" title="Main pages"></i></li>
                <li data="2" class="n-item"><a href="/cpanel/zone-2.htm"><i class="icon-file-presentation"></i>Trang chủ</a></li>
                <li data="1" class="n-item"><a href="/cpanel/zone-1.htm"><i class="icon-file-presentation"></i>Giới thiệu</a></li>
                <li data="3" class="n-item"><a href="/cpanel/zone-3.htm"><i class="icon-file-presentation"></i>Đội ngũ bác sĩ</a></li>
                <li data="11" class="n-item"><a href="/cpanel/zone-11.htm"><i class="icon-file-presentation"></i>Gói dịch vụ</a></li>
                <li data="12" class="n-item"><a href="/cpanel/zone-12.htm"><i class="icon-file-presentation"></i>Chuyên khoa</a></li>
                <li data="14" class="n-item"><a href="/cpanel/zone-14.htm"><i class="icon-file-presentation"></i>Tin tức</a></li>
                <li data="13" class="n-item"><a href="/cpanel/zone-13.htm"><i class="icon-file-presentation"></i>Hướng dẫn KH</a></li>
                <li data="16" class="n-item"><a href="/cpanel/zone-16.htm"><i class="icon-file-presentation"></i>Liên hệ</a></li>
                <li data="17" class="n-item"><a href="/cpanel/zone-17.htm"><i class="icon-file-presentation"></i>Thư viện ảnh và video</a></li>
                <li data="18" class="n-item"><a href="/cpanel/zone-18.htm"><i class="icon-file-presentation"></i>Hệ thống</a></li>
                <li data="19" class="n-item"><a href="/cpanel/zone-19.htm"><i class="icon-file-presentation"></i>Tuyển dụng</a></li>
                <li data="20" class="n-item"><a href="/cpanel/zone-20.htm"><i class="icon-file-presentation"></i>Tra cứu</a></li>
                <%--<li data="19" class="n-item"><a href="/cpanel/zone-19.htm"><i class="icon-file-presentation"></i>Khách sạn</a></li>--%>
            </ul>
        </div>
    </div>
    <!-- /main navigation -->
</div>