﻿R.Location = {
    Init: function () {
        R.Location.RegisterEvents();
        this.ZoneId = 0;
        this.ZoneType = 0;
        this.ZoneNode = null;
        //----
        var PageIndex = 1;
        var PageSize = 5;
        this.NewsStatus = 1;
        this.startDate = null;
        this.endDate = null;
        this.ItemSelected = [];
        this.XNewsId = 0;
        this.minDate = moment("01/01/2014").format('DD/MM/YYYY');
        this.maxDate = moment("12/31/2050").format('DD/MM/YYYY');
        R.Location.XNews(PageIndex);
    },
    RegisterEvents: function () {
        $('#zoneStatusdll,#zoneTypedll').fselect();
        $('#zoneStatusdll').off('change').on('change',
            function (e) {
                R.Location.XNews(PageIndex);
            });
        $('.zone-action ._edit').off('click').on('click', function (e) {
            var id = $(this).parent().attr('data-id');
            R.Location.Edit(id);
        });

        $('.zone-action .status').off('click').on('click', function (e) {
            var id = $(this).parent().attr('data-id');
            var status = $(this).attr('data');
            R.Location.XUpdateStatus({
                status: status,
                id: id
            });
        });
        $('.page-link').off('click').on('click', function () {
            PageIndex = $(this).data('page');
            $(this).parent().addClass('active');
            R.Location.XNews(PageIndex)

        });
        $('._search').off('click').on('click', function () {
            R.Location.XNews(1);
        })
        var delay = 0;
        $('#cms-sidebar li').off('click').on('click',
            function () {
                $('.navigation-main li').removeClass('active');
                $('#news-sldebar .navigation li').removeClass('active');
                $(this).addClass('active');
                if (delay != null) clearTimeout(delay);
                delay = setTimeout(function () {
                    R.Location.XNews(PageIndex);
                },
                    200);

            });

        $('#zoneAddBtn').off('click').on('click',
            function () {
                R.Location.Edit(0);

            });

        $('#locaSaveBtn').off('click').on('click',
            function () {
                var data = {
                    id: $('#zone-edit').attr('data-locaId'),
                    name: $('#locaNameTxt').val(),
                    code: $('#locaCodeTxt').val(),

                    avatar1: $('#attack-thumb img').attr('data-img'),
                    avatar2: $('#attack-icon img').attr('data-img'),
                    description: $('#locaDescriptionTxt').val(),
                    parentId: parseInt($('#locaParentSl').val()),
                    isShowInHomePage: $('#locaHomeCbx:checked').val(),
                    sort: $('#locaSortNum').val(),
                    type: $('#locaType').val()

                }
                console.log(data);
                R.Location.Save(data);
            });
        $('#items .item i.icon-bin').off('click').on('click',
            function () {
                var $this = $(this);
                $.confirm({
                    title: 'Thông báo !',
                    type: 'red',
                    content: 'Xác nhận xóa',
                    animation: 'scaleY',
                    closeAnimation: 'scaleY',
                    buttons: {
                        yes: {
                            text: 'Xóa',
                            keys: ['ESC'],
                            action: function () {
                                $this.closest('.item').remove();
                            }
                        },
                        cancel: {
                            text: 'Đóng',
                            btnClass: 're-btn re-btn-default'
                        }
                    }

                });

            });


        $('#items .item .avatar').off('click').on('click',
            function () {
                var $this = $(this);

                R.Image.SingleUpload($this, function () {

                    //process
                }, function (response, status) {

                    //success
                    if (!response.success) {
                        $.confirm({
                            title: 'Thông báo !',
                            type: 'red',
                            content: response.messages,
                            animation: 'scaleY',
                            closeAnimation: 'scaleY',
                            buttons: {
                                cancel: {
                                    text: 'Đóng',
                                    btnClass: 're-btn re-btn-default'
                                },
                                yes: {
                                    isHidden: true, // hide the button
                                    keys: ['ESC'],
                                    action: function () {

                                    }
                                }
                            }

                        });
                        return;
                    }

                    if (response.success && response.error == 200) {
                        var img = response.path;
                        $this.closest('.avatar').find('img').attr({
                            'src': img + '?w=150&h=100&mode=crop',
                            'data-img': response.name
                        });
                    }


                });

            });

    },



    XUpdateStatus: function (data) {
        R.Post({
            params: data,
            module: "zone",
            ashx: 'modulerequest.ashx',
            action: "update_status",
            success: function (res) {
                if (res.Success) {
                    $.notify("Cập nhật thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.Location.XNews(PageIndex);
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }

        });
    },
    XNews: function (PageIndex) {

        var el = $('#zone-wrapper');
        var filter = $('#_search-data').val();
        $(el).RLoading();
        var data = {
            keyword: $('#_search-data').val(),
            zone: 0,
            pageindex: R.Location.PageIndex,
            pagesize: R.Location.PageSize,
            status: $('#zoneStatusdll').val(),
            from: R.Location.startDate,
            to: R.Location.endDate,
            type: $('#cms-sidebar li.active').attr('data')
        }
        R.Post({
            params: {
                Filter: filter,
                PageIndex: PageIndex
            },
            module: "location",
            ashx: 'modulerequest.ashx',
            action: "filter",
            success: function (res) {
                if (res.Success) {

                    $(el).find('.datatable-scroll._list').html(res.Content);
                    //$('#locationTable').DataTable();
                    R.CMSMapZone(el);
                    $('.btn-group.btn-action').hide();

                    //if ($('#zone-wrapper .datatable-scroll._list table').attr('page-info') != 'undefined') {
                    //    var pageInfo = $('#zone-wrapper .datatable-scroll._list table').attr('page-info').split('#');
                    //    var page = pageInfo[0];
                    //    var extant = pageInfo[1];
                    //    var totals = pageInfo[2];
                    //    if (parseInt(totals) < 0) {
                    //        $('#data-pager').hide();
                    //        return;
                    //    } else {
                    //        $('#data-pager').show();
                    //    }
                    //    var rowFrom = '';
                    //    if (R.Location.PageIndex === 1) {
                    //        rowFrom = 'Từ 1 đến ' + extant;
                    //    } else {
                    //        rowFrom = 'Từ ' + (parseInt(R.Location.PageIndex) * parseInt(R.Location.PageSize) - parseInt(R.Location.PageSize)) + ' đến ' + extant;
                    //    }

                    //    $('#rowInTotals').text(rowFrom + '/' + totals);
                    //    $('.ipagination').jqPagination({
                    //        current_page: R.Location.PageIndex,
                    //        max_page: page,
                    //        paged: function (page) {
                    //            R.Location.PageIndex = page;
                    //            R.Location.XNews(PageIndex);
                    //        }
                    //    });
                    //}

                    $('.linkEdit').off('click').on('click', function () {
                        var dataId = $(this).data('location');
                        console.log(dataId);
                        R.Location.Edit(dataId);
                    })
                    $('.linkDelete').off('click').on('click', function () {
                        var id = $(this).data('location');
                        $.confirm({
                            title: 'Thông báo !',
                            type: 'red',
                            content: "Xac nhan xoa",
                            animation: 'scaleY',
                            closeAnimation: 'scaleY',
                            buttons: {
                                OK: {
                                    isHidden: false, // hide the button
                                    keys: ['ESC'],
                                    action: function () {
                                        R.Post({
                                            params: {
                                                id: id
                                            },
                                            module: "location",
                                            ashx: 'modulerequest.ashx',
                                            action: "remove",
                                            success: function (res) {
                                                $.notify("Cập nhật thành công !", {
                                                    autoHideDelay: 3000, className: "success",
                                                    globalPosition: 'right top'
                                                });
                                                R.Location.XNews(PageIndex);
                                            }
                                        })
                                    }
                                },
                                cancel: {
                                    text: 'Đóng',
                                    btnClass: 're-btn re-btn-default'
                                },
                            }

                        });
                    })
                    $('.page-link').off('click').on('click', function () {
                        $(this).parent().addClass('active');
                        PageIndex = $(this).data('page');
                        R.Location.XNews(PageIndex)

                    });
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });
                }

                $(el).RLoadingComplete();
            }
        });
        R.Location.RegisterEvents();
    },
    Save: function (data) {
        $('#zone-edit').RModuleBlock();
        R.Post({
            params: data,
            module: "location",
            ashx: 'modulerequest.ashx',
            action: "save",
            success: function (res) {
                if (res.Success) {
                    R.Location.XNews(R.Location.PageIndex);
                    R.ScrollAutoSize('#data-zones', function () {
                        return $(window).height() - 10;
                    }, function () {
                        return 'auto';
                    }, {});
                    $.notify("Lưu thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                $('#zone-edit').RModuleUnBlock();
            }
        });
    },
    Edit: function (id) {
        console.log(id);
        $('#zone-form-edit').RLoading();
        var zoneType = $('#cms-sidebar li.active').attr('data');
        R.Post({
            params: {
                id: id,
            },
            module: "location",
            ashx: 'modulerequest.ashx',
            action: "edit",
            success: function (res) {
                console.log(res)
                if (res.Success) {

                    R.ShowOverlay(res.Content, function () {

                        $("#ZoneSaveButton").off('mouseover').off('mouseout')
                            .mouseover(function () {
                                $(this).removeClass('SaveButtonNormalState');
                                $(this).addClass('SaveButtonOverState');
                            })
                            .mouseout(function () {
                                $(this).removeClass('SaveButtonOverState');
                                $(this).addClass('SaveButtonNormalState');
                            });

                        $('#ZoneSaveButton').off('click').on('click', function () {
                            return false;
                        });

                        if (zoneType == 13) {
                            $('#attack-icon').hide();
                            $('#class-icon').show();
                        }

                    }, function () {

                    });

                    if ($('#cms-sidebar li.active').attr('data') == 1) {
                        $('#isShowHomaPageEl').show();
                    }
                    R.Location.ZoneId = id;
                    if (R.Location.ZoneType === 3) {
                        $('#zone-panel').hide();
                    }
                    setTimeout(function () {
                        $('#_zoneddl').fselect({
                            dropDownWidth: 0,
                            autoResize: true
                        });
                        if (id > 0) {
                            $('#_zoneddl').val($('#_zoneddl').attr('data'));
                            $('#_zoneddl').trigger('fselect:updated');
                        }
                        //if (CKEDITOR.instances['sapoZoneTxt']) {
                        //    CKEDITOR.instances['sapoZoneTxt'].destroy();
                        //}
                        ////CKEDITOR.replace('sapoZoneTxt', { toolbar: 'iweb' });
                        R.ScrollAutoSize('#zone-edit .panel-body', function () {
                            return $(window).height() - 10;
                        }, function () {
                            return 'auto';
                        }, {});

                        $('#attack-files .avatar').off('click').on('click', function () {
                            var $this = $(this);
                            R.Image.SingleUpload($this,
                                function () {
                                    //process
                                }, function (response, status) {
                                    $($this).RModuleUnBlock();
                                    //success
                                    if (!response.success) {
                                        $.confirm({
                                            title: 'Thông báo !',
                                            type: 'red',
                                            content: response.messages,
                                            animation: 'scaleY',
                                            closeAnimation: 'scaleY',
                                            buttons: {
                                                cancel: {
                                                    text: 'Đóng',
                                                    btnClass: 're-btn re-btn-default'
                                                },
                                                yes: {
                                                    isHidden: true, // hide the button
                                                    keys: ['ESC'],
                                                    action: function () {

                                                    }
                                                }
                                            }

                                        });
                                        return;
                                    }

                                    if (response.success && response.error == 200) {
                                        var img = response.path;
                                        $this.find('img').attr({
                                            'src': img + '?w=150&h=100&mode=crop',
                                            'data-img': response.name
                                        });
                                    }

                                });

                        });

                    }, 400);

                    $('#zone-form-edit').RLoadingComplete();
                    R.Location.RegisterEvents();

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }
        });
    }



}
$(function () {
    R.Location.Init();
});