﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Serialization;
using Mi.BO.Base.News;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.ErrorCode;
using Mi.Action;
using Mi.Action.Core;

namespace Mi_Company.CMS.Modules.Config.Action
{
    public class ConfigActions : ActionBase
    {
        protected override string ResponseContentType
        {
            get { return "text/plain; charset=utf-8"; }
        }
        protected override bool IsResponseDataDirectly
        {
            get { return false; }
        }

        protected override object ProcessAction(string functionName, HttpContext context)
        {
            var responseData = new ResponseData();
            if (!PolicyProviderManager.Provider.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {
                switch (functionName)
                {
                    case "":
                        responseData.Success = false;
                        responseData.Message = Constants.MESG_CAN_NOT_FOUND_ACTION;
                        break;

                    case "save":
                        responseData = Save();
                        break;
                    case "edit":
                        responseData = Edit();
                        break;
                    case "search":
                        responseData = Search();
                        break;
                    case "page_setting":
                        responseData = PageSetting();
                        break;
                    case "search-export":
                        responseData = SearchExport();
                        break;

                }
            }

            return responseData;
        }
        private ResponseData SearchExport()
        {
            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Config\\Templates\\List.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData Search()
        {


            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Config\\Templates\\List.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData Edit()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Config\\Templates\\Edit.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData Save()
        {
            var responseData = new ResponseData();

            try
            {
                var id = GetQueryString.GetPost("id", 0);
                var languageCode = GetQueryString.GetPost("language_code", string.Empty);
                var configName = GetQueryString.GetPost("config_key", string.Empty);
                var value = GetQueryString.GetPost("value", string.Empty);
                ConfigBo.UpdateConfigLanguageById(id, value, configName);
                if (languageCode.Equals("vi-VN"))
                {
                    ConfigBo.SetValue(configName, value);
                }
                responseData.Success = true;
            }
            catch (Exception)
            {

                responseData.Success = false;
            }
            return responseData;
        }
        private ResponseData PageSetting()
        {
            var responseData = new ResponseData();

            var pjson = GetQueryString.GetPost("json", string.Empty);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var objs = serializer.Deserialize<List<PageSettingEntity>>(pjson);
            try
            {
                foreach (var item in objs)
                {
                    var obj = new ConfigEntity
                    {
                        Id = item.id,
                        ConfigLabel = item.label,
                        ConfigInitValue = item.total + "",//
                        IsEnable = item.isenable,
                        ConfigValueType = item.order,
                        ConfigValue = item.poster + "",// lưu ảnh,url

                    };
                    ConfigBo.Update(obj);
                }
                responseData.Success = true;

            }
            catch (Exception)
            {

                responseData.Success = false;
            }




            return responseData;
        }


        public class ZoneResult
        {
            public string id { get; set; }
            public string parent { get; set; }
            public string text { get; set; }
            public string path { get; set; }
        }
        public class PageSettingEntity
        {
            public int id { get; set; }
            public int order { get; set; }
            public string label { get; set; }
            public int total { get; set; }
            public string poster { get; set; }
            public bool isenable { get; set; }
        }
    }
}