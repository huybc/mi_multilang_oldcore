﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/HoangLongMaster.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Mi_Company._default1" %>

<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <%--<meta property="fb:app_id" content="<%=UIHelper.GetConfigByName("FBAppID") %>" />
    <meta property="og:url" content="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="<%=UIHelper.GetConfigByName("MetaTitle") %>" />
    <meta property="og:description" content="<%=UIHelper.GetConfigByName("MetaDescription") %>" />
    <meta property="og:image" content="<%=domainName %>/uploads/<%=UIHelper.GetConfigByName("Logo") %>" />--%>
    <%--<style>
        .full-image-link img {
            width: 100%;
            height: auto;
        }

        .home-tin-tuc {
            margin-top: 25px;
        }

        .heading-ss-home h1 {
            font-size: 22px;
            font-family: "Trajan";
        }

        .heading-ss-home h6 {
            font-size: 14px;
            font-family: "Trajan";
        }

        .banner img {
            max-width: 100%;
            height: auto;
        }
        /*.image{
            width:347px;
            height:394px
        }*/
        .menu-right .item-link {
            padding: 8px;
            padding-bottom:0px;
            border-top: 1px solid #A9A9A9;
        }
        .list-ck .item-large, .list-ck .item {
            border-bottom:none;
        }
        .menu-right{
            padding-bottom:1px;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%--<%
        var currentLanguage = Page.RouteData.Values["languageCode"].ToString();
        var lang = "";
        switch (currentLanguage)
        {
            case "vi":
                lang = "vi-VN";
                break;
            case "en":
                lang = "en-US";
                break;
        }

    %>--%>
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <h1>Main</h1>
    
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
    <script type="text/javascript">
        $(window).on('load', function () {
            //Kiem tra session
            var s = sessionStorage.getItem('show-popup');
            console.log(s);
            if (s != "show") {
                //Neu khong co session thi hien ra
                $('#myModal').modal('show');
                sessionStorage.setItem("show-popup", "show");
            }

        })
        $('#dat-lien-he').off('click').on('click', function () {
            var name = $('#txtName').val();
            var phoneNumber = $('#txtPhoneNumber').val();
            //var email = $('#txtEmail').val();
            var address = $('#txtAddress').val();
            var type = "lien-he";
            R.Post({
                params: {
                    name: name,
                    address: address,
                    phoneNumber: phoneNumber,
                    //note: note,
                    type: type
                },
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "save",
                success: function (res) {
                    if (res.Success) {
                        console.log(1);
                        alert("Cảm ơn Quý khách đã để lại thông tin! Chúng tôi sẽ liên hệ lại sớm!");
                        R.Post({
                            params: {
                                name: name,
                                //email: email,
                                phoneNumber: phoneNumber,
                                //note: note,
                                type: type,
                                //ngaySinh: birthday,
                                //ngayKham: ngaykham,
                                //address: address,
                                //location: location,
                                //coSo: coSo

                            },
                            module: "ui-action",
                            ashx: 'modulerequest.ashx',
                            action: "send_mail",
                            success: function (res) {
                                console.log('Send mail Successful!');
                                $('#txtName').val('');
                                $('#txtPhoneNumber').val('');
                                $('#txtAddress').val('');
                            }
                        });
                    }
                    // $('.card-header').RLoadingModuleComplete();
                }
            });
        })
    </script>
</asp:Content>
