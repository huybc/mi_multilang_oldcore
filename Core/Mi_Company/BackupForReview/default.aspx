﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Mi_Company.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Mi_Company._default" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi_Company.Core.Helper" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Register Src="~/Pages/Controls/Banner.ascx" TagPrefix="uc1" TagName="Banner" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
    <style>
        .select-box-price { background-color: #efeeee; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <div style="position: relative">
        <h1 style="position: absolute; top: -999px"><%=UIHelper.GetConfigByName("H1HomePage") %></h1>
    </div>
    <uc1:Banner runat="server" ID="Banner" />
    <% var currentLanguageJavaCode = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
        var currentUnit = Current.UnitMoney;
        var priceContact = Language.Contact;
        var priceLable = Language.Price;
    %>

    <section class="list-combo list-tour container">
        <%
            int totalRows = 0;
            //var tours = NewsBo.SearchByShortUrl(zone.ShortUrl, 1, 10, ref totalRows);
            var tours = NewsBo.GetTopTours(18, currentLanguageJavaCode, 1, 1, 12, ref totalRows);
        %>
        <div class="row tour-cate">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <section class="ss-name">
                    <div class="left">
                        <h3 class="tit">
                            <asp:Literal runat="server" Text="<%$ Resources:Language,TourLatest %>"></asp:Literal></h3>
                        <%--<div>Làm việc cả năm. Đến lúc tưởng thưởng</div>--%>
                    </div>
                </section>
                <section class="list row">
                    <% foreach (var tour in tours)
                        { %>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour hot">
                            <div class="image">
                                <a href="/<%=currentLanguage %>/tour/<%=tour.Url %>.<%=tour.Id %>.htm" title="<%=tour.Title %>">
                                    <img src="/Uploads/thumb/<%=tour.Avatar %>?v=1.0" alt="<%=tour.Title %>" /></a>
                                <div class="flag text">
                                    <div class="local-tour-name format-location" data-id="<%=tour.ListZoneId %>"></div>

                                </div>
                            </div>
                            <div class="content px-3 ">
                                <h2 class="tit">
                                    <a href="/<%=currentLanguage %>/tour/<%=tour.Url %>.<%=tour.Id %>.htm" title="<%=tour.Title %>"><%=tour.Title %></a>
                                </h2>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class=""><%=tour.SubTitle %></span>
                                    </div>
                                    <% if (tour.MinPrice > 0)
                                        { %>
                                    <div class="cost ml-auto"><%= UIHelper.FormatCurrency(currentUnit, tour.MinPrice, true) %></div>
                                    <% }
                                        else
                                        { %>
                                    <div class="ml-auto "><%=priceLable %>:
                                        <label class="text-danger"><%=priceContact  %></label></div>
                                    <% } %>
                                </div>
                            </div>

                        </div>
                    </div>
                    <% } %>
                    <% if (!tours.Any())
                        { %>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                        <asp:Literal runat="server" Text="<%$ Resources:Language,NoData %>"></asp:Literal>
                    </div>
                    <% } %>
                </section>
            </div>
        </div>
    </section>
    <section class="list-combo _list-news container ">
        <%
            int totalNewsRows = 0;
            var news = NewsBo.GetTopNews(1, currentLanguageJavaCode, 1, 1, 6, ref totalNewsRows);
        %>
        <div class="row tour-cate">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <section class="ss-name">
                    <div class="left">
                        <h3 class="tit">
                            <asp:Literal runat="server" Text="<%$ Resources:Language,NewsLatest %>"></asp:Literal></h3>
                        <%--<div>Làm việc cả năm. Đến lúc tưởng thưởng</div>--%>
                    </div>
                </section>
                <section class="list row">
                    <% foreach (var tour in news)
                        { %>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour hot">
                            <div class="image">
                                <a href="/<%=currentLanguage %>/blog/<%=tour.Url %>.<%=tour.Id %>.htm" title="<%=tour.Title %>">
                                    <img src="/Uploads/thumb/<%=tour.Avatar %>?v=1.0" alt="<%=tour.Title %>" />
                                </a>
                            </div>
                            <div class="content px-3 ">
                                <h2 class="tit">
                                    <a href="/<%=currentLanguage %>/blog/<%=tour.Url %>.<%=tour.Id %>.htm" title="<%=tour.Title %>"><%=tour.Title %></a>
                                </h2>
                                <div class="d-flex mb-2">
                                    <span class=""><%= Utility.SubWordInString(tour.Sapo.GetText(),30) %></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <% } %>
                    <% if (!news.Any())
                        { %>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                        <asp:Literal runat="server" Text="<%$ Resources:Language,NoData %>"></asp:Literal>
                    </div>
                    <% } %>
                </section>
            </div>
        </div>
    </section>

    <% var locations = ZoneBo.GetLocationByLanguage(currentLanguageJavaCode, true); %>
    <section class="list-location-suggest">
        <div class="container">
            <%
                var zoneLocation = ZoneBo.GetZoneByAlias("viet-nam", currentLanguageJavaCode);
                if (zoneLocation != null)
                {
            %>
            <div class="row ">
                <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                    <section class="ss-name px-2">
                        <div class="left">
                            <h3 class="tit">
                                <%--<%=UIHelper.GetConfigByName("Line2Title") %>--%>
                                <asp:Literal runat="server" Text="<%$ Resources:Language,LocalLocation %>"></asp:Literal>
                            </h3>
                            <div><%=zoneLocation.Content %></div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row no-gutters mb-3">
                <%var locationVNs = locations.Where(l => l.ParentId == zoneLocation.Id).ToList();
                    var tourLanguage = Language.Tours;
                    foreach (var lo in locationVNs)

                    { %>
                <div class="<%= lo.Col %> align-self-center">
                    <div class="px-2">
                        <div class="item-home mb-3 ">
                            <a title="<%= lo.Name %>" href="/<%=currentLanguage %>/location/<%= lo.Url %>">
                                <img src="/uploads/<%= lo.Banner %>?v=1.0" alt="<%= lo.Name %>" class="img-fluid" />
                                <div class="ovelay">
                                </div>
                            </a>
                            <div class="category">
                                <h3 class="top"><a href="/<%=currentLanguage %>/location/<%= lo.Url %>" title="<%= lo.Name %>"><%= lo.Name %></a></h3>
                                <div class="bot"><%= lo.TourCount %> <%=tourLanguage %></div>
                            </div>
                        </div>
                    </div>
                </div>
                <% } %>
                <% if (!locationVNs.Any())
                    { %>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                    <asp:Literal runat="server" Text="<%$ Resources:Language,NoData %>"></asp:Literal>
                </div>
                <% } %>
            </div>
            <% } %>
        </div>
    </section>
    <section class="list-uu-dai">
        <div class="container">
            <div class="row ">
                <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                    <section class="ss-name px-2">
                        <div class="left">
                            <h3 class="tit">
                                <asp:Literal runat="server" Text="<%$ Resources:Language,GoodwillToday %>"></asp:Literal>

                            </h3>
                            <%--<div>Nhanh tay đặt ngay. Để mai sẽ lỡ !</div>--%>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
                <% var datas = ConfigBo.AdvGetByType(6, currentLanguageJavaCode);
                    foreach (var d in datas)
                    {%>
                <div class="<%=d.Col %>">
                    <div class="item-1">
                        <a href="<%=d.Url %>" title="<%=d.Name %>" class="">
                            <img class="w-100" src="/uploads/<%=d.Thumb %>?v=1.0" alt="<%=d.Name %>">
                        </a>
                    </div>
                </div>
                <% } %>
            </div>
        </div>
    </section>
    <section class="list-location-suggest mb-5">
        <div class="container">
            <%
                var locationZone = ZoneBo.GetZoneByAlias("quoc-te", currentLanguageJavaCode);
                if (locationZone != null)
                {
            %>
            <div class="row ">
                <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                    <section class="ss-name px-2">
                        <div class="left">
                            <h3 class="tit">
                                <asp:Literal runat="server" Text="<%$ Resources:Language,InternationalLocation %>"></asp:Literal>
                            </h3>
                            <div><%=locationZone.Content %></div>
                        </div>
                    </section>
                </div>
            </div>

            <div class="row no-gutters mb-3">
                <%var locationQt = locations.Where(l => l.ParentId == locationZone.Id).ToList();
                    var tourLanguage = Language.Tours;
                    foreach (var lo in locationQt)

                    { %>
                <div class="<%= lo.Col %> align-self-center">
                    <div class="px-2">
                        <div class="item-home mb-3 ">
                            <a title="<%= lo.Name %>" href="/<%=currentLanguage %>/location/<%= lo.Url %>">
                                <img src="/uploads/<%= lo.Banner %>?v=1.0" alt="<%= lo.Name %>" class="img-fluid" />
                                <div class="ovelay">
                                </div>
                            </a>
                            <div class="category">
                                <h3 class="top"><a href="/<%=currentLanguage %>/location/<%= lo.Url %>" title="<%= lo.Name %>"><%= lo.Name %></a></h3>
                                <div class="bot"><%= lo.TourCount %> <%=tourLanguage %></div>
                            </div>
                        </div>
                    </div>
                </div>
                <% } %>
                <% if (!locationQt.Any())
                    { %>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                    <asp:Literal runat="server" Text="<%$ Resources:Language,NoData %>"></asp:Literal>
                </div>
                <% } %>
            </div>
            <% } %>
        </div>
    </section>
</asp:Content>
