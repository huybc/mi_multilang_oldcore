﻿using Mi.Common;

namespace Mi.Entity.Base.Language
{
    public class LanguageEntity:EntityBase
    {
        public int id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int sortOrder { get; set; }
        public string Description { get; set; }
        public bool IsDefault { get; set; }
        public bool IsBasic { get; set; }
        public string Icon { get; set; }

    }
}
