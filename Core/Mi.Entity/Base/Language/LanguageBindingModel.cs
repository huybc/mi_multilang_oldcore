﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mi.Entity.Base.Language
{
    public class ZoneWithLanguageByType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string lang_name { get; set; }
        public string lang_url { get; set; }
        public int ParentId { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public string LanguageCode { get; set; }
        public string Avatar { get; set; }
        public string Content { get; set; }
        public string lang_content { get; set; }
        public string MetaTitle { get; set; }
        public int SortOrder { get; set; }


    }
}
