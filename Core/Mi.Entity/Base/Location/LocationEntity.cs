﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mi.Entity.Base.Location
{
    public class LocationEntity
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string ProvinceCode { get; set; }
        public string Avatar_No1 { get; set; }
        public string Avatar_No2 { get; set; }
        public int Parent { get; set; }
        public int isShowInHomePage { get; set; }
        public int Sort { get; set; }
        public int total_hotel { get; set; }
    }
}
