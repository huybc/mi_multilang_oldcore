﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mi.Entity.Base.ConFig
{
    public class ConfigTitle
    {
        public string TitleSeo { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
        public string Avatar { get; set; }
    }
}
