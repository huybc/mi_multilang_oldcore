﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.News
{
    [DataContract]
    public class NewsContentMiPressEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string PublishedDate { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public string NewsRelation { get; set; }
        [DataMember]
        public string Tags { get; set; }
        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public int LocationType { get; set; }
        [DataMember]
        public string SourceURL { get; set; }
    }
}
