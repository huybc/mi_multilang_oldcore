﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Mi.Common;
using Mi.Entity.Base.Tag;
using Mi.Entity.Base.Zone;

namespace Mi.Entity.Base.News
{
    [DataContract]
    public class NewsGroupEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string GroupNewsName { get; set; }
        [DataMember]
        public string GroupNewsDescription { get; set; }
        [DataMember]
        public int TotalRow { get; set; }
        [DataMember]
        public bool IsFilterByZone { get; set; }
    }
}
