﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.News
{
    [DataContract]
    public enum NewsLiveStatus
    {
        [EnumMember]
        AllStatus = 0,
        [EnumMember]
        Created = 1,
        [EnumMember]
        Published = 2,
        [EnumMember]
        Deleted = 3
    }
    [DataContract]
    public class NewsLiveEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public long PublishedDateNumber { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string EventTime { get; set; }
    }

    [DataContract]
    public class NewsLiveSettingsEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public bool IsFinished { get; set; }
        [DataMember]
        public bool ShowTime { get; set; }
        [DataMember]
        public bool ShowAuthor { get; set; }
    }

    [DataContract]
    public class NewsLiveAuthorEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int AuthorId { get; set; }
        [DataMember]
        public string AuthorName { get; set; }
        [DataMember]
        public string Note { get; set; }
    }

    [DataContract]
    public class NewsLiveDetail : EntityBase
    {
        [DataMember]
        public NewsLiveEntity NewsLive { get; set; }
        [DataMember]
        public List<NewsLiveAuthorEntity> NewsAuthorList { get; set; }
        [DataMember]
        public NewsLiveSettingsEntity NewsSettings { get; set; }
    }

    [DataContract]
    public class NewsLiveForList : EntityBase
    {
        [DataMember]
        public List<NewsLiveEntity> NewsLiveEntitiesList { get; set; }
        [DataMember]
        public int TotalRow { get; set; }
        [DataMember]
        public NewsLiveSettingsEntity NewsLiveSettings { get; set; }
        [DataMember]
        public List<NewsLiveAuthorEntity> NewsAuthorList { get; set; }
    }
}
