﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.News
{
    [DataContract]
    public class NewsHistoryEntity : EntityBase
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string ActionName { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string SentBy { get; set; }
        [DataMember]
        public string ReceivedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}
