﻿using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.News
{
    [DataContract]
    public class NewsRelationEntity : EntityBase
    {
        [DataMember]
        public string NewsId { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string PublishedDate { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string AvatarCustom { get; set; }
    }
}
