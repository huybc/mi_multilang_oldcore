﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.News
{
    [DataContract]
    public class NewsPublishEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string InitSapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AvatarDesc { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string NewsRelation { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int ThreadId { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string OriginalUrl { get; set; }

        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public int DisplayPosition { get; set; }
        [DataMember]
        public int DisplayInSlide { get; set; }
        [DataMember]
        public string AvatarCustom { get; set; }
        [DataMember]
        public string TagItem { get; set; }

        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public long PublishedDate { get; set; }

        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public bool IsBreakingNews { get; set; }
        [DataMember]
        public bool IsPr { get; set; }
        [DataMember]
        public bool AdStore { get; set; }
        [DataMember]
        public string AdStoreUrl { get; set; }
        [DataMember]
        public int RollingNewsId { get; set; }
        [DataMember]
        public int TagSubTitleId { get; set; }

        [DataMember]
        public int LocationType { get; set; }

        [DataMember]
        public DateTime ExpiredDate { get; set; }

    }

    [DataContract]
    public class NewsPublishForNewsRelationEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
    }

    [DataContract]
    public class NewsPublishForObjectBoxEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
    }

    [DataContract]
    public class NewsPublishForSearchEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
    }
}
