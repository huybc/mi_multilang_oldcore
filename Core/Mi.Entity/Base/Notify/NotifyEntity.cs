﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Mi.Common;

namespace Mi.Entity.Base.Notify
{
    [DataContract]
    public class NotifyEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string ReceiveUser { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Detail { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
    [DataContract]
    public enum EnumNotifyStatus : int
    {
        [EnumMember]
        AllType = 0,
        [EnumMember]
        Read = 1,
        [EnumMember]
        NotRead = 2,
        [EnumMember]
        View = 3
    }
}
