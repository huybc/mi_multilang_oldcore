﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Media
{
    [DataContract]
    public class MediaEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Thumb { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public byte Status { get; set; }
        [DataMember]
        public byte Type { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public string Metakeyword { get; set; }
        [DataMember]
        public string MetaDescription { get; set; }
    }
}
