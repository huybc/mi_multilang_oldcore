﻿using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Zone
{
    [DataContract]
    public class ZoneDefaultTagEntity : EntityBase
    {
        [DataMember]
        public long TagId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public string ZoneUrl { get; set; }
        [DataMember]
        public string CustomUrl { get; set; }
    }
}
