﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Mi.Common;

namespace Mi.Entity.Base.Zone
{
    public class ZoneLanguageEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string LanguageName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public int zoneId { get; set; }
        [DataMember]
        public string LanguageCode { get; set; }
        [DataMember]
        public string MetaTitle { get; set; }
        [DataMember]
        public string MetaKeyword { get; set; }
        [DataMember]
        public string MetaDescription { get; set; }
    }

}
