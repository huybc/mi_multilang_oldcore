﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Mi.Common;

namespace Mi.Entity.Base.Tag
{
    [DataContract]
    public class TagProductEntity : EntityBase
    {

        [DataMember]
        public long TagId { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public Int16 TagMode { get; set; }
        [DataMember]
        public string TagProperty { get; set; }
        [DataMember]
        public int Priority { get; set; }
    }
    [DataContract]
    public class TagProductWithTagInfoEntity : EntityBase
    {
        [DataMember]
        public long TagId { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public Int16 TagMode { get; set; }
        [DataMember]
        public string TagProperty { get; set; }
        [DataMember]
        public int Priority { get; set; }

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Url { get; set; }
    }

   
}
