﻿using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Tag
{
    [DataContract]
    public class BoxThreadEmbedEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public long ThreadId { get; set; }//tagid

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
