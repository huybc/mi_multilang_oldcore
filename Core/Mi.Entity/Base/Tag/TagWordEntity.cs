﻿using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Tag
{
    [DataContract]
    public class TagWordEntity : EntityBase
    {
        [DataMember]
        public string TagName { get; set; }
        [DataMember]
        public bool IsSuggestion { get; set; }
    }
    [DataContract]
    public class TagWordCheckedEntity : EntityBase
    {
        [DataMember]
        public string TagName { get; set; }
        [DataMember]
        public bool IsChecked { get; set; }
    }
}
