﻿using System;
using System.Runtime.Serialization;
using Mi.Common;
using System.Collections.Generic;

namespace Mi.Entity.Base.Tag
{
    [DataContract]
    public class QueueUpdateTagChannelEntity : EntityBase
    {
        [DataMember]
        public long QueueUpdateTagChannelId { set; get; }
        [DataMember]
        public long TagChannelId { set; get; }
        [DataMember]
        public long TagChannelOriginalId { set; get; }
        [DataMember]
        public int ChannelId { set; get; }
        [DataMember]
        public string Name { set; get; }
        [DataMember]
        public string UnsignName { set; get; }
        [DataMember]
        public string LowerName { set; get; }
        [DataMember]
        public string Url { set; get; }
        [DataMember]
        public bool IsHotTag { set; get; }
        [DataMember]
        public int Type { set; get; }
        [DataMember]
        public string Avatar { set; get; }
        [DataMember]
        public string TagTitle { set; get; }
        [DataMember]
        public string TagInit { set; get; }
        [DataMember]
        public string TagContent { set; get; }
        [DataMember]
        public string TagMetaKeyword { set; get; }
        [DataMember]
        public string TagMetaContent { set; get; }
        [DataMember]
        public int PrimaryZoneId { set; get; }
        [DataMember]
        public string RelatedZoneIds { set; get; }
        [DataMember]
        public string LastUpdateBy { set; get; }
        [DataMember]
        public DateTime InsertQueueDate { set; get; }
        [DataMember]
        public DateTime LastProcessedDate { set; get; }
        [DataMember]
        public bool IsProcessed { set; get; }
    }
}
