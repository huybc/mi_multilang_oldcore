﻿using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Tag
{
    [DataContract]
    public class TagZoneEntity : EntityBase
    {
        [DataMember]
        public long TagId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
    }
}
