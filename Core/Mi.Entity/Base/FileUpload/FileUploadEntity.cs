﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Mi.Common;

namespace Mi.Entity.Base.FileUpload
{
    [DataContract]
    public enum EnumFileUploadStatus
    {
        [EnumMember]
        AllStatus = 2,
        [EnumMember]
        Inactived = 0,
        [EnumMember]
        Actived = 1
    }
    [DataContract]
    public enum EnumFileUploadType
    {
        [EnumMember]
        Folder = 0,
        [EnumMember]
        File = 1,
        [EnumMember]
        All = 2

    }

    [DataContract]
    public enum EnumFileSortOrder
    {
        [EnumMember]
        UploadedDateAsc = 1,
        [EnumMember]
        UploadedDateDesc = 2,
        [EnumMember]
        FileExtDesc = 3,
        [EnumMember]
        FileExtAsc = 4,
        [EnumMember]
        NameDesc = 5,
        [EnumMember]
        NameAsc = 6,
        [EnumMember]
        FileSizeDesc = 7,
        [EnumMember]
        FileSizeAsc = 8,
        [EnumMember]
        DimSumDesc = 9,
        [EnumMember]
        DimSumAsc = 10
    }

    [DataContract]
    public class FileUploadEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string FileDownloadPath { get; set; }
        [DataMember]
        public string FilePath { get; set; }
        [DataMember]
        public string Thumb { get; set; }
        [DataMember]
        public string FileExt { get; set; }
        [DataMember]
        public double FileSize { get; set; }
        [DataMember]
        public DateTime UploadedDate { get; set; }
        [DataMember]
        public string strDate { get; set; }
        [DataMember]
        public string UploadedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public string Dimensions { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public byte Type { get; set; }
    }
    [DataContract]
    public class FolderEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string path { get; set; }
    }
}
