﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Comment
{
    [DataContract]
    public class CommentEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public long ObjectId { get; set; }
        [DataMember]
        public int ObjectType { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string ObjectUrl { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public int LogParentId { get; set; }
    }


    [DataContract]
    public enum EnumCommentStatus
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Published = 1,
        [EnumMember]
        UnPublished = 2
    }

    
    public enum EnumCommentObjectType
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Solution = 1,
        [EnumMember]
        Question = 2,
        [EnumMember]
        Answer = 3
    }
    [DataContract]
    public enum EnumCommentSortOrder
    {
        [EnumMember]
        Asc = 1,
        [EnumMember]
        Desc = 0
    }
}
