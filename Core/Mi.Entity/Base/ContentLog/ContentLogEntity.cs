﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.ContentLog
{

    [DataContract]
    public class ContentLogEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public long ObjbectId { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string Date { get; set; }
        [DataMember]
        public byte ObjectType { get; set; }
    }
    [DataContract]
    public enum EnumContentLogType
    {
        [EnumMember]
        News = 1,
        [EnumMember]
        Product = 2

    }

}
