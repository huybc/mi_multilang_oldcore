﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.XData
{
    public class XDataEntity
    {

    }

    [DataContract]
    public class TopCashoutEntity : EntityBase
    {
        [DataMember]
        public string User_Name { get; set; }
        [DataMember]
        public string avatar_url { get; set; }
        [DataMember]
        public long Total_Cash_Out_Amount { get; set; }

    }

    [DataContract]
    public class TopWealthyEntity : EntityBase
    {
        [DataMember]
        public string User_Name { get; set; }
        [DataMember]
        public long xcoin { get; set; }

    }
    [DataContract]
    public class LatestSuccessfulExchangeEntity : EntityBase
    {
        [DataMember]
        public string User_Name { get; set; }
        [DataMember]
        public string name { get; set; }

    }
    [DataContract]
    public class CardChargeRateEntity : EntityBase
    {
        [DataMember]
        public long Charge_Amount { get; set; }
        [DataMember]
        public long Balance { get; set; }

    }
    [DataContract]
    public class TopEarningUserEntity : EntityBase
    {
        [DataMember]
        public string User_Name { get; set; }
        [DataMember]
        public int vip_level { get; set; }
        [DataMember]
        public decimal amount { get; set; }

    }
    [DataContract]
    public class Top10SmashJarEntity : EntityBase
    {
        [DataMember]
        public long row_numb { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int amount { get; set; }

    }
    [DataContract]
    public class TaiXiuEventEntity : EntityBase
    {
        [DataMember]
        public long row_numb { get; set; }
        [DataMember]
        public string player_name { get; set; }
        [DataMember]
        public long win_length { get; set; }
        [DataMember]
        public DateTime start_time { get; set; }
        [DataMember]
        public long total_bet { get; set; }

    }
    [DataContract]
    public class EventLuckNumberEntity : EntityBase
    {
        [DataMember]
        public long id { get; set; }
        [DataMember]
        public DateTime event_date { get; set; }
        [DataMember]
        public string lucky_number { get; set; }
        [DataMember]
        public string lottery_result { get; set; }
        [DataMember]
        public string winner { get; set; }
        [DataMember]
        public string prize { get; set; }

    }
}
