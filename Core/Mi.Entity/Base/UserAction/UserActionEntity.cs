﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.UserAction
{
    [DataContract]
    public class UserActionEntity : EntityBase
    {
        /// <summary>
        /// Người nhận
        /// </summary>
        [DataMember]
        public string UserName { get; set; }
        /// <summary>
        /// Đối tượng tác động
        /// </summary>
        [DataMember]
        public long ObjectId { get; set; }
        /// <summary>
        /// Loại đối tượng
        /// </summary>
        [DataMember]
        public int ObjectType { get; set; }
        /// <summary>
        /// Loại hành động
        /// </summary>
        [DataMember]
        public int ActionType { get; set; }
        /// <summary>
        /// Giá trị của hành động tùy theo ngữ cảnh. Ví dụ: voteUp = 1, voteDown = -1
        /// </summary>
        [DataMember]
        public int ActionValue { get; set; }
        /// <summary>
        /// Số điểm mà [UserName] đạt được khi [CreatedBy] thực hiện hành động
        /// </summary>
        [DataMember]
        public int Point { get; set; }
        /// <summary>
        /// Người gây ra hành động
        /// </summary>
        [DataMember]
        public string CreatedBy { get; set; }
        /// <summary>
        /// Ngày phát sinh hành động
        /// </summary>
        [DataMember]
        public DateTime CreatedDate { get; set; }
        /// <summary>
        /// Ngày cập nhật lại hành động. Ví dụ bỏ vote/ sửa vote
        /// </summary>
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
    }
    [DataContract]
    public enum EnumUserActionType
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        VoteUp = 1,
        [EnumMember]
        VoteDown = 2,
        [EnumMember]
        PostTutorial = 3,
        [EnumMember]
        PostQuestion = 4,
        [EnumMember]
        PostAnswer = 5
    }
    [DataContract]
    public enum EnumUserActionObjectType
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Solution = 1,
        [EnumMember]
        Question = 2,
        [EnumMember]
        Answer = 3
    }
}
