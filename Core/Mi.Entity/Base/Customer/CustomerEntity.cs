﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Customer
{
    [DataContract]
    public class CustomerEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]

        public string FullName { get; set; }
        [DataMember]

        public string Email { get; set; }
        [DataMember]

        public string Mobile { get; set; }
        [DataMember]

        public string Type { get; set; }
        [DataMember]

        public string Firm { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string Contactperson { get; set; }
        [DataMember]
        public string Service { get; set; }
        [DataMember]
        public string Address { get; set; } [DataMember]
        public DateTime CreatedDate { get; set; }
        public DateTime NgaySinh { get; set; }
        public DateTime NgayKham { get; set; }
        public string location { get; set; }
        public int CoSoKham { get; set; }
        public int NguoiLon { get; set; }
        public int TreEm { get; set; }
        public string NoteNhanVien { get; set; }

    }
    public class Customer_By_Month {
        public int year { get; set; }
        public int month { get; set; }
        public int tong_so { get; set; }
    }
    public class Customer_By_CoSo
    {
        public string Name { get; set; }
        public int total_kh { get; set; }
    }
    public class Customer_Theo_Tuoi {
        public int z1 { get; set; }
        public int z2 { get; set; }
        public int z3 { get; set; }
        public int z4 { get; set; }
        public int z5 { get; set; }
    }
}
