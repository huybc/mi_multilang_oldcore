﻿namespace Mi.Entity.Base.ProjectDetail
{
   public class TourDepartureEnitity
    {
        public int Id { get; set; }
        public long ArticleId { get; set; }
        public long ParentId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Image { get; set; }
        public decimal Price { get; set; }  public decimal ChildPrice { get; set; }
    }
}
