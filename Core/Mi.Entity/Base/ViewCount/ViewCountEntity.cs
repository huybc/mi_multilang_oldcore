﻿using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.ViewCount
{
    [DataContract]
    public class ViewCountEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public long ObjectId { get; set; }
        [DataMember]
        public int ObjectType { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
    }

    public enum EnumObjectType : int
    {
        [EnumMember]
        TblNews = 1,
        [EnumMember]
        TblAnswer = 2,
        [EnumMember]
        TblComment = 3,
        [EnumMember]
        TblShop = 4,
        [EnumMember]
        TblProduct = 5,
        [EnumMember]
        TblUser = 6 // xem profile



    }

}
