﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Document
{

    [DataContract]
    public class GovDocumentEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string SoDi { get; set; }
        [DataMember]
        public string SoKyHieu { get; set; }
        [DataMember]
        public string NguoiKy { get; set; }
        [DataMember]
        public string NguoiSoan { get; set; }
        [DataMember]
        public string DviSoanThao { get; set; }
        [DataMember]
        public string CompanyId { get; set; }
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public int LoaiVanBan { get; set; }
        [DataMember]
        public string NoiNhanNgoaiNganh { get; set; }
        [DataMember]
        public string NoiNhanTrongNganh { get; set; }
        [DataMember]
        public string NoiNhanNoiBo { get; set; }
        [DataMember]
        public string FilePath { get; set; }
        [DataMember]
        public string EDocLink { get; set; }
        [DataMember]
        public DateTime NgayPhatHanh { get; set; }
        [DataMember]
        public DateTime NgayDen { get; set; }
        [DataMember]
        public DateTime CreatedAt { get; set; }
        [DataMember]
        public DateTime LastModifiedAt { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public string Notes { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public byte Type { get; set; }
        [DataMember]
        public int SortOrder { get; set; }


    }
    [DataContract]
    public class GovDocumentBasicEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string SoDi { get; set; }
        public int ParentId { get; set; }

    }

    public class DocumentFilterEntity
    {
        public string SoDi { get; set; }
        public string SoKyHieu { get; set; }
        public string NguoiKy { get; set; }
        public string NguoiSoan { get; set; }
        public string DviSoanThao { get; set; }
        public string MST { get; set; }
        public int LoaiVanBan { get; set; }
        public string NoiNhanNgoaiNganh { get; set; }
        public string NoiNhanTrongNganh { get; set; }
        public string NoiNhanNoiBo { get; set; }
        public DateTime NgayPhatHanhFrom { get; set; }
        public DateTime NgayPhatHanhTo { get; set; }
        public DateTime NgayDenFrom { get; set; }
        public DateTime NgayDenTo { get; set; }
        public string ParentId { get; set; }
        public string Notes { get; set; }
        public int SortOrder { get; set; }
        public int Status { get; set; }
        public byte Type { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }

    }
    public class DocumentFilterBasicEntity
    {
      
        public string ParentId { get; set; }
        public int SortOrder { get; set; }
        public int Status { get; set; }
        public byte Type { get; set; }
   

    }
    [DataContract]
    public enum EnumDocumentSortOrder
    {
        [EnumMember]
        DocumentDateAsc = 0,
        [EnumMember]
        DocumentDateDesc = 1,

        NameDesc = 2,
        [EnumMember]
        NameAsc = 3,

    }
    [DataContract]
    public enum DocumentStatus
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Publish = 1,
        [EnumMember]
        UnPublish = 2,
        [EnumMember]
        Delete = 3,
        [EnumMember]
        Temp = 4

    }
    [DataContract]
    public enum DocumentType
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Document = 1,
        [EnumMember]
        Category = 2,
        [EnumMember]
        Type = 3

    }
}
