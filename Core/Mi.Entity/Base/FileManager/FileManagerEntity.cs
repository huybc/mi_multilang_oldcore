﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mi.Entity.Base.FileManager
{
    public class FileManagerEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string User { get; set; }
        public DateTime CreatedDate { get; set; }

    }
}
