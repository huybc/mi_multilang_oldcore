﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Mi.Common;

namespace Mi.Entity.Base.Departure
{
    [DataContract]
    public class DepartureEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime EndDate { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public decimal ChildPrice { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public long TourId { get; set; }
    }
}
