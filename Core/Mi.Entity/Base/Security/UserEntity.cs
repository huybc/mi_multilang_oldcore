﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Mi.Common;
using Mi.Entity.Base.Zone;

namespace Mi.Entity.Base.Security
{
    [DataContract]
    public enum UserStatus
    {
        [EnumMember]
        Unknow = -1,
        [EnumMember]
        Actived = 1,
        [EnumMember]
        Locked = 0,
        [EnumMember]
        UnActive = 2
    }
    [DataContract]
    public enum UserSortExpression
    {
        [EnumMember]
        UserNameDesc = 0,
        [EnumMember]
        UserNameAsc = 1,
        [EnumMember]
        CreateDateDesc = 2,
        [EnumMember]
        CreateDateAsc = 3,
        [EnumMember]
        LastLoginDesc = 4,
        [EnumMember]
        LastLoginAsc = 5,
        [EnumMember]
        LastChangePassDesc = 6,
        [EnumMember]
        LastChangePassAsc = 7
    }
    [DataContract]
    public enum MemberOrder
    {
        [EnumMember]
        CreatedDateDesc = 0,
        [EnumMember]
        CreatedDateAsc = 1,
        [EnumMember]
        PointDesc = 2,
        [EnumMember]
        PointAsc = 3,

    }
    [DataContract]
    public enum TimeFilter
    {
        [EnumMember]
        Week = 0,
        [EnumMember]
        Month = 1,
        [EnumMember]
        Quarter = 2,
        [EnumMember]
        Year = 3,
        [EnumMember]
        All = -1

    }
    public class XAuthEntity
    {
        public string user_name { get; set; }
        public long partner_id { get; set; }
        public string password { get; set; }
    }
    [DataContract]
    public class UserEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        //[DataMember]
        //public string EncryptId { get { return CryptonForId.EncryptId(Id); } }
        [DataMember]
        public string EncryptId { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public bool IsFullPermission { get; set; }
        [DataMember]
        public bool IsFullZone { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public DateTime Birthday { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Skype { get; set; }
        [DataMember]
        public string Website { get; set; }
        [DataMember]
        public string ZoneIdList { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public DateTime LastLogined { get; set; }
        [DataMember]
        public DateTime LastChangePass { get; set; }
        [DataMember]
        public int PermissionCount { get; set; }
        [DataMember]
        public bool IsSystem { get; set; }
        [DataMember]
        public int Point { get; set; }
        [DataMember]
        public string ActiveCode { get; set; }
        [DataMember]
        public byte LoginType { get; set; }
        [DataMember]
        public string SocialId { get; set; }
    }

    [DataContract]
    public class MemberEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int Point { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string ListIdZone { get; set; }

    }

    [DataContract]

    public class UserWithPermissionEntity : EntityBase
    {
        [DataMember]
        public UserEntity User { get; set; }
        [DataMember]
        public List<UserPermissionEntity> UserPermissions { get; set; }
    }
    [DataContract]
    public class UserWithPermissionDetailEntity : EntityBase
    {
        [DataMember]
        public UserEntity User { get; set; }
        [DataMember]
        public List<GroupPermissionDetailEntity> AllGroupPermission { get; set; }
        [DataMember]
        public List<ZoneWithSimpleFieldEntity> AllParentZone { get; set; }
        [DataMember]
        public List<UserPermissionEntity> UserPermissionList { get; set; }
    }
    [DataContract]
    public class UserStandardEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        //[DataMember]
        //public string EncryptId { get { return CryptonForId.EncryptId(Id); } }
        [DataMember]
        public string EncryptId { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public string Skype { get; set; }
        [DataMember]
        public string Website { get; set; }
        [DataMember]

        public bool IsFullPermission { get; set; }
        [DataMember]
        public bool IsFullZone { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int PermissionCount { get; set; }
    }
}
