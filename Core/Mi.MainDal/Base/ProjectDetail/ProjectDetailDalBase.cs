﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.ProjectDetail;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.ProjectDetail
{
    public abstract class ProjectDetailDalBase
    {
        #region Gets

        public List<TourDepartureEnitity> GetByArticleId(long id)
        {
            const string commandText = "CMS_ProjectDetail_GetByArticleId";
            try
            {
                List<TourDepartureEnitity> data = new List<TourDepartureEnitity>();
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ArticleId", id);
                
                data = _db.GetList<TourDepartureEnitity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
     
        #endregion

        #region Sets


        public bool Create(TourDepartureEnitity ProjectDetail,ref int id)
        {

            const string commandText = "CMS_ProjectDetail_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ArticleId", ProjectDetail.ArticleId);
                _db.AddParameter(cmd, "Title", ProjectDetail.Title);
                _db.AddParameter(cmd, "Content", ProjectDetail.Content);
                _db.AddParameter(cmd, "Image", ProjectDetail.Image);
                _db.AddParameter(cmd, "Price", ProjectDetail.Price);
                _db.AddParameter(cmd, "ParentId", ProjectDetail.ParentId);
                _db.AddParameter(cmd, "ChildPrice", ProjectDetail.ChildPrice);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(long id)
        {

            const string commandText = "CMS_ProjectDetail_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected ProjectDetailDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
