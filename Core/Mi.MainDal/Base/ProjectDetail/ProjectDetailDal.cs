﻿using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.ProjectDetail
{
    public class ProjectDetailDal : ProjectDetailDalBase
    {
        internal ProjectDetailDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
