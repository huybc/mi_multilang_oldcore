﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.Media;
using Mi.Entity.Base.Product;
using Mi.Entity.Base.Zone;
using Mi.MainDal.Common;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Media
{
    public class MediaDalBase
    {
        public bool MediaUpdate(MediaEntity obj)
        {
            const string commandText = "CMS_Media_Update";
            try
            {

                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id);
                _db.AddParameter(cmd, "Name", obj.Name);
                _db.AddParameter(cmd, "Content", obj.Content);
                _db.AddParameter(cmd, "SortOrder", obj.SortOrder);
                _db.AddParameter(cmd, "Type", obj.Type);
                _db.AddParameter(cmd, "Status", obj.Status);
                _db.AddParameter(cmd, "Thumb", obj.Thumb);
                _db.AddParameter(cmd, "MetaKeyword", obj.Metakeyword);
                _db.AddParameter(cmd, "MetaDescription", obj.MetaDescription);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool MediaInsert(MediaEntity obj, ref int id)
        {
            const string commandText = "CMS_Media_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", obj.Name);
                _db.AddParameter(cmd, "Content", obj.Content);
                _db.AddParameter(cmd, "SortOrder", obj.SortOrder);
                _db.AddParameter(cmd, "Type", obj.Type);
                _db.AddParameter(cmd, "Status", obj.Status);
                _db.AddParameter(cmd, "Thumb", obj.Thumb);
                _db.AddParameter(cmd, "MetaKeyword", obj.Metakeyword);
                _db.AddParameter(cmd, "MetaDescription", obj.MetaDescription);
                var data = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data > 0 &&id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public MediaEntity GetById(int id)
        {
            const string commandText = "CMS_Media_GetById";
            try
            {
                MediaEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<MediaEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool MediaDelete(int Id)
        {
            const string commandText = "CMS_Media_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<MediaEntity> Search(string keyword, string username, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "CMS_Media_Search";
            try
            {
                List<MediaEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                data = _db.GetList<MediaEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsMainDb _db;

        protected MediaDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
