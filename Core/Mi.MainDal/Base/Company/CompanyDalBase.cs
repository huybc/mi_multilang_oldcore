﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.Company;
using Mi.Entity.Base.News;
using Mi.MainDal.Common;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Company
{
    public class CompanyDalBase
    {
        #region Update

        public bool Insert(CompanyEntity obj, ref int id)
        {
            const string commandText = "Company_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MST", obj.MST );
                _db.AddParameter(cmd, "Name", obj.Name);
                _db.AddParameter(cmd, "Address", obj.Address);
                _db.AddParameter(cmd, "Phone", obj.Phone);

                _db.AddParameter(cmd, "CreatedAt", obj.CreatedBy);
                //  _db.AddParameter(cmd, "LastModifiedAt", obj.LastModifiedAt);
                _db.AddParameter(cmd, "CreatedBy", obj.CreatedBy);
                //    _db.AddParameter(cmd, "LastModifiedBy", obj.LastModifiedBy);
                _db.AddParameter(cmd, "Note", obj.Note);

                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(CompanyEntity obj)
        {
            const string commandText = "Company_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MST", obj.MST);
                _db.AddParameter(cmd, "Name", obj.Name);
                _db.AddParameter(cmd, "Address", obj.Address);
                _db.AddParameter(cmd, "Phone", obj.Phone);

                //_db.AddParameter(cmd, "CreatedAt", obj.CreatedBy);
                _db.AddParameter(cmd, "LastModifiedAt", obj.LastModifiedAt);
                //_db.AddParameter(cmd, "CreatedBy", obj.CreatedBy);
                _db.AddParameter(cmd, "LastModifiedBy", obj.LastModifiedBy);
                _db.AddParameter(cmd, "Note", obj.Note);

                var numberOfRow = cmd.ExecuteNonQuery();
                //   newFolderId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        #endregion

        #region Get

        public CompanyEntity GetById(int id)
        {
            const string commandText = "Company_GetById";
            try
            {
                CompanyEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<CompanyEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        public IEnumerable<CompanyEntity> Search(string keyword, int sortOrder, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "Company_Search";
            try
            {
                List<CompanyEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "MST", keyword);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<CompanyEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected CompanyDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
