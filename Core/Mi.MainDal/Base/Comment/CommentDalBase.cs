﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.Comment;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Comment
{
    public abstract class CommentDalBase
    {
        #region Gets

        public CommentEntity GetById(int id)
        {
            const string commandText = "CMS_Comment_GetById";
            try
            {
                CommentEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<CommentEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CommentEntity> Search(int status, int objectType, long objectId)
        {
            const string commandText = "CMS_Comment_Search";
            try
            {
                List<CommentEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "Status", status);
                data = _db.GetList<CommentEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CommentEntity> SearchCommentWithPaging(long objectId, int objectType,int parentId, int status,int sortOrder,  int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Comment_SearchWithPaging";
            try
            {
                List<CommentEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<CommentEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int CountComment(int status, int objectType, long objectId)
        {
            const string commandText = "CMS_Comment_Count";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "Status", status);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Sets


        public bool Insert(CommentEntity CommentEntity, ref int id)
        {

            const string commandText = "CMS_Comment_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", CommentEntity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Body", CommentEntity.Body);
                _db.AddParameter(cmd, "CreatedBy", CommentEntity.CreatedBy);
                _db.AddParameter(cmd, "ObjectId", CommentEntity.ObjectId);
                _db.AddParameter(cmd, "ObjectType", CommentEntity.ObjectType);
                _db.AddParameter(cmd, "Status", CommentEntity.Status);
                _db.AddParameter(cmd, "ObjectUrl", CommentEntity.ObjectUrl);
                _db.AddParameter(cmd, "ParentId", CommentEntity.ParentId);
                _db.AddParameter(cmd, "LogParentId", CommentEntity.LogParentId);
               var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
               

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(CommentEntity CommentEntity, ref int id)
        {

            const string commandText = "CMS_Comment_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", CommentEntity.Id);
                _db.AddParameter(cmd, "Body", CommentEntity.Body);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatus(int id, int status)
        {
            const string commandText = "CMS_Comment_ChangeStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }


        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected CommentDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
