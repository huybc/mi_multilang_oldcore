﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.Answer;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Answer
{
    public abstract class AnswerDalBase
    {
        #region Gets

        public AnswerEntity GetById(int id)
        {
            const string commandText = "CMS_Answer_GetById";
            try
            {
                AnswerEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<AnswerEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<AnswerEntity> Search(int status, int objectType, long objectId)
        {
            const string commandText = "CMS_Answer_Search";
            try
            {
                List<AnswerEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "Status", status);
                data = _db.GetList<AnswerEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int CountAnswer(int status, int objectType, long objectId)
        {
            const string commandText = "CMS_Answer_Count";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "Status", status);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Sets


        public bool Insert(AnswerEntity answerEntity, ref int id)
        {

            const string commandText = "CMS_Answer_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", answerEntity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Body", answerEntity.Body);
                _db.AddParameter(cmd, "CreatedBy", answerEntity.CreatedBy);
                _db.AddParameter(cmd, "ObjectId", answerEntity.ObjectId);
                _db.AddParameter(cmd, "ObjectType", answerEntity.ObjectType);
                _db.AddParameter(cmd, "Status", answerEntity.Status);
                _db.AddParameter(cmd, "ObjectUrl", answerEntity.ObjectUrl);
                cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(AnswerEntity answerEntity)
        {

            const string commandText = "CMS_Answer_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", answerEntity.Id);
                _db.AddParameter(cmd, "Body", answerEntity.Body);
                //_db.AddParameter(cmd, "CreatedBy", answerEntity.CreatedBy);
                //_db.AddParameter(cmd, "ObjectId", answerEntity.ObjectId);
                //_db.AddParameter(cmd, "ObjectType", answerEntity.ObjectType);
                //_db.AddParameter(cmd, "Status", answerEntity.Status);
                //_db.AddParameter(cmd, "ObjectUrl", answerEntity.ObjectUrl);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatus(int id, int status)
        {
            const string commandText = "CMS_Answer_ChangeStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool SetIsCorrect(int id, long objectId, bool isCorrect)
        {
            const string commandText = "CMS_Answer_SetIsCorrect";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "isCorrect", isCorrect);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool VoteAnswer(long id, EnumAnswerVoteDirection voteDirection)
        {
            const string commandText = "CMS_Answer_Vote";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Direction", (int)voteDirection);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected AnswerDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
