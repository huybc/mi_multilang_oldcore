﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.FileUpload;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.FileUpload
{
    public abstract class FileUploadDalBase
    {
        #region Update

        public bool InsertFileUpload(FileUploadEntity fileUpload, ref int newFileUploadId)
        {
            const string commandText = "CMS_FileUpload_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", fileUpload.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ParentId", fileUpload.ParentId);
                _db.AddParameter(cmd, "Name", fileUpload.Name);
                _db.AddParameter(cmd, "Description", fileUpload.Description);
                _db.AddParameter(cmd, "FileDownloadPath", fileUpload.FileDownloadPath);
                _db.AddParameter(cmd, "FilePath", fileUpload.FilePath);
                _db.AddParameter(cmd, "FileExt", fileUpload.FileExt);
                _db.AddParameter(cmd, "FileSize", fileUpload.FileSize);
                _db.AddParameter(cmd, "UploadedBy", fileUpload.UploadedBy);
                _db.AddParameter(cmd, "Status", fileUpload.Status);
                _db.AddParameter(cmd, "Dimensions", fileUpload.Dimensions);
                _db.AddParameter(cmd, "Type", fileUpload.Type);
                var numberOfRow = cmd.ExecuteNonQuery();
                newFileUploadId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateFileUpload(FileUploadEntity fileUpload)
        {
            const string commandText = "CMS_FileUpload_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", fileUpload.Id);
                _db.AddParameter(cmd, "ParentId", fileUpload.ParentId);
                _db.AddParameter(cmd, "Name", fileUpload.Name);
                _db.AddParameter(cmd, "Description", fileUpload.Description);
                _db.AddParameter(cmd, "FileDownloadPath", fileUpload.FileDownloadPath);
                _db.AddParameter(cmd, "FilePath", fileUpload.FilePath);
                _db.AddParameter(cmd, "LastModifiedBy", fileUpload.LastModifiedBy);
                _db.AddParameter(cmd, "Status", fileUpload.Status);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteFileUpload(int fileUploadId)
        {
            const string commandText = "CMS_FileUpload_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", fileUploadId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Get

        public FileUploadEntity GetFileUploadById(int fileUploadId)
        {
            const string commandText = "CMS_FileUpload_GetById";
            try
            {
                FileUploadEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", fileUploadId);
                data = _db.Get<FileUploadEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<FileUploadEntity> SearchFileUpload(string keyword, int parentId, string uploadedBy, EnumFileUploadType type, EnumFileUploadStatus status, string ext, EnumFileSortOrder sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_FileUpload_Search";
            try
            {
                IEnumerable<FileUploadEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "Type", (byte)type);
                _db.AddParameter(cmd, "UploadedBy", uploadedBy);
                _db.AddParameter(cmd, "Status", (byte)status);
                _db.AddParameter(cmd, "Ext", ext);
                _db.AddParameter(cmd, "SortOrder", (int)sortOrder);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<FileUploadEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
       

        public IEnumerable<FileUploadEntity> FolderCheckIsExist(int id, string name, int parentId)
        {
            const string commandText = "CMS_CheckExistFolder";
            try
            {
                IEnumerable<FileUploadEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Name", name);
                _db.AddParameter(cmd, "ParentId", parentId);
                data = _db.GetList<FileUploadEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<FolderEntity> FoldersSearch()
        {
            const string commandText = "CMS_Folders_Search";
            try
            {
                IEnumerable<FolderEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<FolderEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected FileUploadDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
