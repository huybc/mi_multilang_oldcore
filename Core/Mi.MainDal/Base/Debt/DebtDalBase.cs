﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.Debt;
using Mi.Entity.Base.Document;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Debt
{
    public class DebtDalBase
    {
        #region Update
        public bool Insert(DebtEntity obj, ref int id)
        {
            const string commandText = "CMS_Debt_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DebtID", obj.DebtID, ParameterDirection.Output);
                _db.AddParameter(cmd, "CompanyId", obj.CompanyId);
                //_db.AddParameter(cmd, "KyTinhThue", obj.KyTinhThue);
                _db.AddParameter(cmd, "KhoanThue", obj.KhoanThue);
                _db.AddParameter(cmd, "TieuMuc", obj.TieuMuc);
                //_db.AddParameter(cmd, "SoTienNoGoc", obj.SoTienNopGoc);
                //_db.AddParameter(cmd, "HanNopGoc", obj.HanNopNoGoc);
                _db.AddParameter(cmd, "Note", obj.Note);
                _db.AddParameter(cmd, "CreatedAt", obj.CreatedAt);
                _db.AddParameter(cmd, "CreatedBy", obj.CreatedBy);
                //  _db.AddParameter(cmd, "LastModifiedAt", obj.LastModifiedAt);
                //   _db.AddParameter(cmd, "LastModifiedBy", obj.LastModifiedBy);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(DebtEntity obj, ref int id)
        {
            const string commandText = "CMS_Debt_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DebtID", obj.DebtID, ParameterDirection.InputOutput);
                _db.AddParameter(cmd, "CompanyId", obj.CompanyId);
                //_db.AddParameter(cmd, "KyTinhThue", obj.KyTinhThue);
                _db.AddParameter(cmd, "KhoanThue", obj.KhoanThue);
                _db.AddParameter(cmd, "TieuMuc", obj.TieuMuc);
                //_db.AddParameter(cmd, "SoTienNoGoc", obj.SoTienNopGoc);
                //_db.AddParameter(cmd, "HanNopGoc", obj.HanNopNoGoc);
                _db.AddParameter(cmd, "Note", obj.Note);
                //  _db.AddParameter(cmd, "CreatedAt", obj.CreatedAt);
                //   _db.AddParameter(cmd, "CreatedBy", obj.CreatedBy);
                _db.AddParameter(cmd, "LastModifiedAt", obj.LastModifiedAt);
                _db.AddParameter(cmd, "LastModifiedBy", obj.LastModifiedBy);

                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Get
        public DebtEntity GetById(int id)
        {
            const string commandText = "CMS_Debt_GetById";
            try
            {
                DebtEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<DebtEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public TransEntity TransGetById(int id)
        {
            const string commandText = "CMS_DebtTransactionId_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                return  _db.Get<TransEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion



        //public IEnumerable<DebtEntity> Search(DocumentFilterEntity obj, ref int totalRows)
        //{
        //    const string commandText = "Documental_Search";
        //    try
        //    {
        //        IEnumerable<DebtEntity> data = null;
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
        //        _db.AddParameter(cmd, "SoDi", obj.SoDi);
        //        _db.AddParameter(cmd, "SoKyHieu", obj.SoKyHieu);
        //        _db.AddParameter(cmd, "NguoiKy", obj.NguoiKy);
        //        _db.AddParameter(cmd, "NguoiSoan", obj.NguoiSoan);
        //        _db.AddParameter(cmd, "DviSoanThao", obj.DviSoanThao);
        //        _db.AddParameter(cmd, "LoaiVanBan", obj.LoaiVanBan);
        //        _db.AddParameter(cmd, "NoiNhanNgoaiNganh", obj.NoiNhanNgoaiNganh);
        //        _db.AddParameter(cmd, "NoiNhanTrongNganh", obj.NoiNhanTrongNganh);
        //        _db.AddParameter(cmd, "NoiNhanNoiBo", obj.NoiNhanNoiBo);
        //        _db.AddParameter(cmd, "NgayPhatHanhFrom", obj.NgayPhatHanhFrom);
        //        _db.AddParameter(cmd, "NgayPhatHanhTo", obj.NgayPhatHanhTo);
        //        _db.AddParameter(cmd, "NgayDenFrom", obj.NgayDenFrom);
        //        _db.AddParameter(cmd, "NgayDenTo", obj.NgayDenTo);
        //        _db.AddParameter(cmd, "ParentId", obj.ParentId);
        //        _db.AddParameter(cmd, "Notes", obj.Notes);
        //        _db.AddParameter(cmd, "SortOrder", obj.SortOrder);
        //        _db.AddParameter(cmd, "Status", obj.Status);
        //        _db.AddParameter(cmd, "Type", obj.Type);
        //        _db.AddParameter(cmd, "PageIndex", obj.PageIndex);
        //        _db.AddParameter(cmd, "PageSize", obj.PageSize);

        //        data = _db.GetList<DebtEntity>(cmd);

        //        totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        #region trans
        public bool TransInsert(TransEntity obj, ref int id)
        {
            const string commandText = "CMS_DebtTransactions_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.DebtTransactionId, ParameterDirection.Output);
                _db.AddParameter(cmd, "DebtID", obj.DebtId);
                _db.AddParameter(cmd, "HanNopNoGoc", obj.HanNopNoGoc);
                _db.AddParameter(cmd, "TienNoGoc", obj.TienNoGoc);
                _db.AddParameter(cmd, "NgayChamNop1", obj.NgayChamNop1);
                _db.AddParameter(cmd, "NgayChamNop2", obj.NgayChamNop2);
                _db.AddParameter(cmd, "TyLe", obj.TyLe);
                _db.AddParameter(cmd, "SoTienChamNop", obj.SoTienChamNop);
                _db.AddParameter(cmd, "NgayNop", obj.NgayNop);
                _db.AddParameter(cmd, "SoTienNop", obj.SoTienNop);
                _db.AddParameter(cmd, "Status", obj.Status);
                _db.AddParameter(cmd, "CreatedBy", obj.CreatedBy);
                _db.AddParameter(cmd, "ParentId", obj.ParentId);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool TransUpdate(TransEntity obj, ref int id)
        {
            const string commandText = "CMS_DebtTransactions_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.DebtTransactionId, ParameterDirection.Output);
                _db.AddParameter(cmd, "DebtID", obj.DebtId);
                _db.AddParameter(cmd, "HanNopNoGoc", obj.HanNopNoGoc);
                _db.AddParameter(cmd, "TienNoGoc", obj.TienNoGoc);
                _db.AddParameter(cmd, "NgayChamNop1", obj.NgayChamNop1);
                _db.AddParameter(cmd, "NgayChamNop2", obj.NgayChamNop2);
                _db.AddParameter(cmd, "TyLe", obj.TyLe);
                _db.AddParameter(cmd, "SoTienChamNop", obj.SoTienChamNop);
                _db.AddParameter(cmd, "NgayNop", obj.NgayNop);
                _db.AddParameter(cmd, "SoTienNop", obj.SoTienNop);
                _db.AddParameter(cmd, "Status", obj.Status);
                _db.AddParameter(cmd, "CreatedAt", obj.CreatedAt);
                _db.AddParameter(cmd, "ParentId", obj.ParentId);
                _db.AddParameter(cmd, "LastModifiedBy", obj.LastModifiedBy);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public IEnumerable<TransEntity> TransSearch(TransEntity obj)
        {
            const string commandText = "CMS_DebtTransactions_Search";
            try
            {
                IEnumerable<TransEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "ParentId", obj.ParentId);
                _db.AddParameter(cmd, "DebtId", obj.DebtId);
                _db.AddParameter(cmd, "Status", obj.Status);
                data = _db.GetList<TransEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion


        #region Core members

        private readonly CmsMainDb _db;

        protected DebtDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
