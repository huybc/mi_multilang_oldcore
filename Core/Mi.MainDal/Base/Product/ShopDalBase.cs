﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.Product;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Product
{
    public abstract class ShopDalBase
    {
        #region Gets

        public ShopEntity GetById(int id)
        {
            const string commandText = "CMS_Shop_GetById";
            try
            {
                ShopEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<ShopEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<ShopEntity> GetAllByProductId(int id)
        {
            const string commandText = "FE_Shop_GetAllByProductId";
            try
            {
                List<ShopEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.GetList<ShopEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ShopEntity GetAllShopByZoneId(int id)
        {
            const string commandText = "FE_GetAllShopByZoneId";
            try
            {
                ShopEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<ShopEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ShopSimpleEntity> GetByUserName(string userName)
        {
            const string commandText = "CMS_Shop_GetWithShopSimpleFieldByUserName";
            try
            {
                List<ShopSimpleEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", userName);
                data = _db.GetList<ShopSimpleEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<LocationEntity> SearchNearLocation(int getTop, double latitude, double longitude, int distance, int zoneId, int type, EnumShopLocationOrder locationOrder, ref int totalRows, bool isLogin)
        {
            string commandText = "CMS_Search_Location";
            if (isLogin)
            {
                commandText = "CMS_Search_Location_FullProperty";

            }
            try
            {
                List<LocationEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Latitude", latitude);
                _db.AddParameter(cmd, "Longitude", longitude);
                _db.AddParameter(cmd, "distance", distance);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "SortOrder", (int)locationOrder);
                _db.AddParameter(cmd, "SelectTop", getTop);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                data = _db.GetList<LocationEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ShopEntity> Search(string keyword, string username, int status, int sortOrder, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "CMS_Shop_Search";
            try
            {
                List<ShopEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                data = _db.GetList<ShopEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<ShopEntity> FE_Shop_Search(string keyword, string username, string zoneIds, int status, int sortOrder, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "FE_Shop_Search";
            try
            {
                IEnumerable<ShopEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                data = _db.GetList<ShopEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<ShopEntity> FE_Shop_SearchSimple(string keyword, string username, string zoneIds, int status, int sortOrder, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "FE_Shop_SearchSimple";
            try
            {
                IEnumerable<ShopEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "UserName", username);
                //_db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                data = _db.GetList<ShopEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Sets

        public bool Insert(ShopEntity shop, string zoneIdList, ref int id)
        {
            const string commandText = "CMS_Shop_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", shop.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", shop.Name);
                _db.AddParameter(cmd, "UnsignName", shop.UnsignName);
                _db.AddParameter(cmd, "Logo", shop.Logo);
                _db.AddParameter(cmd, "Website", shop.Website);
                _db.AddParameter(cmd, "Address", shop.Address);
                _db.AddParameter(cmd, "Longitude", shop.Longitude);
                _db.AddParameter(cmd, "Latitude", shop.Latitude);
                _db.AddParameter(cmd, "Description", shop.Description);
                _db.AddParameter(cmd, "CreatedBy", shop.CreatedBy);
                _db.AddParameter(cmd, "Status", shop.Status);
                _db.AddParameter(cmd, "Mobile", shop.Mobile);
                _db.AddParameter(cmd, "Type", shop.Type);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "DistrictCode", shop.DistrictCode);
                _db.AddParameter(cmd, "PrimaryName", shop.PrimaryName);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0 && id > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(ShopEntity shop, string zoneIdList)
        {

            const string commandText = "CMS_Shop_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", shop.Id);
                _db.AddParameter(cmd, "Name", shop.Name);
                _db.AddParameter(cmd, "UnsignName", shop.UnsignName);
                _db.AddParameter(cmd, "Logo", shop.Logo);
                _db.AddParameter(cmd, "Website", shop.Website);
                _db.AddParameter(cmd, "Address", shop.Address);
                _db.AddParameter(cmd, "Longitude", shop.Longitude);
                _db.AddParameter(cmd, "Latitude", shop.Latitude);
                _db.AddParameter(cmd, "Description", shop.Description);
                _db.AddParameter(cmd, "Status", shop.Status);
                _db.AddParameter(cmd, "Mobile", shop.Mobile);
                _db.AddParameter(cmd, "Type", shop.Type);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "DistrictCode", shop.DistrictCode);
                _db.AddParameter(cmd, "PrimaryName", shop.PrimaryName);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeShopStatus(int id, int status)
        {
            const string commandText = "CMS_Shop_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool ChangeStatus(int id, int status)
        {
            const string commandText = "CMS_Product_ChangeStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;


            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool IsExistNamePrimary(int id, string name)
        {
            int totalRows = 0;
            const string commandText = "FE_Shop_CheckPrimaryName";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Name", name);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                _db.GetList<LocationEntity>(cmd);

                cmd.ExecuteNonQuery();
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                if (totalRows > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }


        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected ShopDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
