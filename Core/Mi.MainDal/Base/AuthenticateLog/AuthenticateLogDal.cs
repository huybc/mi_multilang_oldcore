﻿using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.AuthenticateLog
{
    public class AuthenticateLogDal : AuthenticateLogDalBase
    {
        internal AuthenticateLogDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}