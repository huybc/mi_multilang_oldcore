﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.Notify;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Notify
{
    public abstract class NotifyDalBase
    {
        #region Gets

        public List<NotifyEntity> GetNotifyByStatus(string username, int status)
        {
            const string commandText = "FE_Notify_Get";
            try
            {
                List<NotifyEntity> data = new List<NotifyEntity>();
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ReceiveUser", username);
                _db.AddParameter(cmd, "Status", status);
                data = _db.GetList<NotifyEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NotifyEntity> NotifySearch(string username, int status, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "FE_Notify_Get_WithPaging";
            try
            {
                List<NotifyEntity> data = new List<NotifyEntity>();
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ReceiveUser", username);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "SortOrder", 0);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                data = _db.GetList<NotifyEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Sets


        public bool AddNotify(NotifyEntity notify)
        {

            const string commandText = "FE_Notify_Add";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ReceiveUser", notify.ReceiveUser);
                _db.AddParameter(cmd, "Status", notify.Status);
                _db.AddParameter(cmd, "Detail", notify.Detail);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool NotifyUpdateStatus(NotifyEntity notify)
        {

            const string commandText = "FE_Notify_UpdateSatatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", notify.Id);
                _db.AddParameter(cmd, "UserName", notify.ReceiveUser);
                _db.AddParameter(cmd, "Status", notify.Status);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool NotifyUpdateStatus_SetAll(string userName, int statusInput,int statusOutput)
        {

            const string commandText = "FE_Notify_UpdateSatatus_SetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", userName);
                _db.AddParameter(cmd, "StatusInput", statusInput);
                _db.AddParameter(cmd, "StatusOutput", statusOutput);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool MarkAllRead(string userName)
        {

            const string commandText = "FE_Notify_MarkAllRead";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", userName);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int Count(string userName, int status)
        {

            const string commandText = "FE_Notify_Count";
            var data = 0;
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", userName);
                _db.AddParameter(cmd, "Status", status);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
            return data;
        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected NotifyDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
