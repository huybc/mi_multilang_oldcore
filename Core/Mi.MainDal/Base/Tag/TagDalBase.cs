﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.Tag;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Tag
{
    public abstract class TagDalBase
    {
        #region Get methods
        public List<TagEntity> GetTagByListOfId(string listTagId)
        {
            const string commandText = "CMS_Tag_GetTagByIds";
            try
            {
                List<TagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Ids", listTagId);
                data = _db.GetList<TagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TagEntity> GetTagByParentTagId(long parentTagId)
        {
            const string commandText = "CMS_Tag_GetTagByParentTagID";
            try
            {
                List<TagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentID", parentTagId);
                data = _db.GetList<TagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public TagEntity GetTagByTagId(long tagId)
        {
            const string commandText = "CMS_Tag_GetTagByTagID";
            try
            {
                TagEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                data = _db.Get<TagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public TagEntity GetTagByTagName(string name, bool getTagHasNewsOnly = false)
        {
            const string commandText = "CMS_Tag_GetByName";
            try
            {
                TagEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", name);
                _db.AddParameter(cmd, "getTagHasNewsOnly", getTagHasNewsOnly);
                data = _db.Get<TagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TagEntity> SearchTag(string keyword, long parentTagId, int isThread, int zoneId, int type, int orderBy, int isHotTag, int pageIndex, int pageSize, ref int totalRow, bool getTagHasNewsOnly = false)
        {
            const string commandText = "CMS_Tag_Search";
            try
            {
                List<TagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ParentID", parentTagId);
                _db.AddParameter(cmd, "IsThread", isThread);
                _db.AddParameter(cmd, "IsHotTag", isHotTag);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "OrderBy", orderBy);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "GetTagHasNewsOnly", getTagHasNewsOnly);
                data = _db.GetList<TagEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TagEntity> SearchAllByKeyword(string keyword, int zoneId, bool isThread)
        {
            const string commandText = "CMS_Tag_GetAllByKeyword";
            try
            {
                List<TagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "IsThread", isThread);
                data = _db.GetList<TagEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TagWithSimpleFieldEntity> SearchTagForSuggestion(int top, int zoneId, string keyword,int tagType, int isHot, int orderBy, bool getTagHasNewsOnly)
        {
            const string commandText = "CMS_Tag_SearchForSuggest";
            try
            {
                List<TagWithSimpleFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "IsHotTag", isHot);
                _db.AddParameter(cmd, "OrderBy", orderBy);
                _db.AddParameter(cmd, "Type", tagType);
                _db.AddParameter(cmd, "GetTagHasNewsOnly", getTagHasNewsOnly);
                data = _db.GetList<TagWithSimpleFieldEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<TagWithSimpleFieldEntity> GetAllCleanTag(string startByKeyword, int minWordCount, int maxWordCount)
        {
            const string commandText = "CMS_Tag_GetAllCleanTag";
            try
            {
                List<TagWithSimpleFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "StartByKeyword", startByKeyword);
                _db.AddParameter(cmd, "MinWordCount", minWordCount);
                _db.AddParameter(cmd, "MaxWordCount", maxWordCount);

                data = _db.GetList<TagWithSimpleFieldEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<TagEntity> GetListTagByNewsId(long newsId)
        {
            const string commandText = "CMS_TagNews_GetByNewsId";
            try
            {
                List<TagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "newsId", newsId);

                data = _db.GetList<TagEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Update methods
        public bool Insert(TagEntity tag, int zoneId, string zoneIdList, ref long tagId)
        {

            const string commandText = "CMS_Tag_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "ParentID", tag.ParentId);
                _db.AddParameter(cmd, "Name", tag.Name);
                _db.AddParameter(cmd, "Description", tag.Description);
                _db.AddParameter(cmd, "Url", tag.Url);
                _db.AddParameter(cmd, "Invisibled", tag.Invisibled);
                _db.AddParameter(cmd, "IsHotTag", tag.IsHotTag);
                _db.AddParameter(cmd, "Type", tag.Type);
                _db.AddParameter(cmd, "CreatedBy", tag.CreatedBy);
                _db.AddParameter(cmd, "UnsignName", tag.UnsignName);
                _db.AddParameter(cmd, "IsThread", tag.IsThread);
                _db.AddParameter(cmd, "Avatar", tag.Avatar);
                _db.AddParameter(cmd, "Priority", tag.Priority);
                _db.AddParameter(cmd, "TagContent", tag.TagContent);
                _db.AddParameter(cmd, "TagTitle", tag.TagTitle);
                _db.AddParameter(cmd, "TagInit", tag.TagInit);
                _db.AddParameter(cmd, "TagMetaKeyword", tag.TagMetaKeyword);
                _db.AddParameter(cmd, "TagMetaContent", tag.TagMetaContent);
                _db.AddParameter(cmd, "PrimaryZoneId", zoneId);
                _db.AddParameter(cmd, "OtherZoneId", zoneIdList);
                _db.AddParameter(cmd, "TemplateId", tag.TemplateId);
                _db.AddParameter(cmd, "CreatedDate", tag.CreatedDate);

                var numberOfRow = cmd.ExecuteNonQuery();

                tagId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Insert(TagEntity tag, ref long tagId)
        {
            const string commandText = "SYNCV2_CMS_Tag_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tag.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", tag.Name);
                _db.AddParameter(cmd, "Description", tag.Description);
                _db.AddParameter(cmd, "Url", tag.Url);
                _db.AddParameter(cmd, "Type", tag.Type);
                _db.AddParameter(cmd, "UnsignName", tag.UnsignName);
                _db.AddParameter(cmd, "Avatar", tag.Avatar);
                _db.AddParameter(cmd, "TagContent", tag.TagContent);
                _db.AddParameter(cmd, "TagTitle", tag.TagTitle);
                _db.AddParameter(cmd, "TagMetaKeyword", tag.TagMetaKeyword);
                _db.AddParameter(cmd, "TagMetaContent", tag.TagMetaContent);

                var numberOfRow = cmd.ExecuteNonQuery();

                tagId = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        //public static bool InsertNew(TagEntity tag, ref long tagId) 
        //{
        //    
        //    try
        //    {
        //        tagId = 0;
        //        var parameters = new[]
        //                             {
        //                                 new SqlParameter("@ParentID", tag.ParentId),
        //                                 new SqlParameter("@Name", tag.Name),
        //                                 new SqlParameter("@Description", tag.Description),
        //                                 new SqlParameter("@Url", tag.Url),
        //                                 new SqlParameter("@Invisibled", tag.Invisibled),
        //                                 new SqlParameter("@IsHotTag", tag.IsHotTag),
        //                                 new SqlParameter("@Type", tag.Type),
        //                                 new SqlParameter("@CreatedBy", tag.CreatedBy),
        //                                 new SqlParameter("@UnsignName", tag.UnsignName),
        //                                 new SqlParameter("@IsThread", tag.IsThread),
        //                                 new SqlParameter("@Avatar", tag.Avatar),
        //                                 new SqlParameter("@Priority", tag.Priority),
        //                                 new SqlParameter("@Id", 0) {Direction = ParameterDirection.Output}
        //                             };
        //        SqlHelperParameterCache.CacheParameterSet(Constants.GetConnectionString(Constants.Connection.CmsMainDb),
        //                                                    Constants.DatabaseSchema + "CMS_Tag_Insert",
        //                                                    parameters);

        //        int numberOfRow = _db.ExecuteNonQuery(
        //                                                    CommandType.StoredProcedure,
        //                                                    Constants.DatabaseSchema + "CMS_Tag_Insert",
        //                                                    parameters);
        //        return numberOfRow > 0;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format(Constants.DatabaseSchema + "CMS_Tag_Insert:{0}", ex.Message));
        //    }
        //}

        public bool Update(TagEntity tag, int zoneId, string zoneIdList)
        {

            const string commandText = "CMS_Tag_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tag.Id);
                _db.AddParameter(cmd, "ParentID", tag.ParentId);
                _db.AddParameter(cmd, "Name", tag.Name);
                _db.AddParameter(cmd, "Description", tag.Description);
                _db.AddParameter(cmd, "Url", tag.Url);
                _db.AddParameter(cmd, "Invisibled", tag.Invisibled);
                _db.AddParameter(cmd, "IsHotTag", tag.IsHotTag);
                _db.AddParameter(cmd, "Type", tag.Type);
                _db.AddParameter(cmd, "EditedBy", tag.EditedBy);
                _db.AddParameter(cmd, "UnsignName", tag.UnsignName);
                _db.AddParameter(cmd, "IsThread", tag.IsThread);
                _db.AddParameter(cmd, "Avatar", tag.Avatar);
                _db.AddParameter(cmd, "Priority", tag.Priority);
                _db.AddParameter(cmd, "TagContent", tag.TagContent);
                _db.AddParameter(cmd, "TagTitle", tag.TagTitle);
                _db.AddParameter(cmd, "TagInit", tag.TagInit);
                _db.AddParameter(cmd, "TagMetaKeyword", tag.TagMetaKeyword);
                _db.AddParameter(cmd, "TagMetaContent", tag.TagMetaContent);
                _db.AddParameter(cmd, "PrimaryZoneId", zoneId);
                _db.AddParameter(cmd, "OtherZoneId", zoneIdList);
                _db.AddParameter(cmd, "TemplateId", tag.TemplateId);
                _db.AddParameter(cmd, "CreatedDate", tag.CreatedDate);


                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateViewCountByTag(int viewCount, string url)
        {

            const string commandText = "CMS_Tag_UpdateViewCount";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "viewCount", viewCount);
                _db.AddParameter(cmd, "Url", url);


                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateListViewCountByTag(string value)
        {

            const string commandText = "CMS_Tag_UpdateListViewCount";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "value", value);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(long tagId)
        {
            const string commandText = "CMS_Tag_DeleteByTagID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool DeleteTagNewsById(string tagName, long newsId)
        {
            const string commandText = "SYNCV2_CMS_Tag_DeleteTagNewByTagID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "tagName", tagName);
                _db.AddParameter(cmd, "newsId", newsId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        //public static bool UpdateTagForNews(long newsId, string tagIds)
        //{
        //    
        //    try
        //    {
        //        var parameters = new[]
        //                             {
        //                                 new SqlParameter("@NewsId", newsId),
        //                                 new SqlParameter("@TagIDs", tagIds)
        //                             };
        //        var numberOfRow = _db.ExecuteNonQuery(
        //                                                    CommandType.StoredProcedure,
        //                                                    Constants.DatabaseSchema + "CMS_TagNews_UpdateTagForNews",
        //                                                    parameters);
        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format(Constants.DatabaseSchema + "CMS_TagNews_UpdateTagForNews:{0}", ex.Message));
        //    }
        //}
        /// <summary>
        /// Thêm mới thông tin tag vào bảng tagNews
        /// </summary>
        /// <param name="newsIds"></param>
        /// <param name="tagId"></param>
        /// <param name="tagMode"></param>
        /// <returns></returns>
        public bool AddNews(string newsIds, long tagId, int tagMode = 1)
        {
            const string commandText = "CMS_Tag_AddNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsIds", newsIds);
                _db.AddParameter(cmd, "TagId", tagId);
                _db.AddParameter(cmd, "TagMode", tagMode);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdatePriority(long tagId, long priority)
        {
            const string commandText = "CMS_Tag_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagId", tagId);
                _db.AddParameter(cmd, "Priority", priority);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool UpdateTagHot(long tagId, bool isHotTag)
        {
            const string commandText = "CMS_Tag_UpdateTagHot";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                _db.AddParameter(cmd, "IsHotTag", isHotTag);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Cập nhật tag cho bài viết
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="tagName"></param>
        /// <returns></returns>
        public bool UpdateTagNews(long newsId, string tagName)
        {
            const string commandText = "SYNCV2_CMS_Tag_UpdateTagNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "newsId", newsId);
                _db.AddParameter(cmd, "tagName", tagName);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        public List<TagWithSimpleFieldEntity> GetTagByListOfTagName(string listTagName)
        {
            const string commandText = "CMS_Tag_GetTagByListTagName";
            try
            {
                List<TagWithSimpleFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListTagName", listTagName);
                data = _db.GetList<TagWithSimpleFieldEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected TagDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

        public bool RecieveFromCloud(TagEntity tag, int zoneId, string zoneIdList, ref long tagId)
        {

            const string commandText = "CMS_Tag_RecieveFromCloud";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "ParentID", tag.ParentId);
                _db.AddParameter(cmd, "Name", tag.Name);
                _db.AddParameter(cmd, "Description", tag.Description);
                _db.AddParameter(cmd, "Url", tag.Url);
                _db.AddParameter(cmd, "Invisibled", tag.Invisibled);
                _db.AddParameter(cmd, "IsHotTag", tag.IsHotTag);
                _db.AddParameter(cmd, "Type", tag.Type);
                _db.AddParameter(cmd, "CreatedBy", tag.CreatedBy);
                _db.AddParameter(cmd, "UnsignName", tag.UnsignName);
                _db.AddParameter(cmd, "IsThread", tag.IsThread);
                _db.AddParameter(cmd, "Avatar", tag.Avatar);
                _db.AddParameter(cmd, "Priority", tag.Priority);
                _db.AddParameter(cmd, "TagContent", tag.TagContent);
                _db.AddParameter(cmd, "TagTitle", tag.TagTitle);
                _db.AddParameter(cmd, "TagInit", tag.TagInit);
                _db.AddParameter(cmd, "TagMetaKeyword", tag.TagMetaKeyword);
                _db.AddParameter(cmd, "TagMetaContent", tag.TagMetaContent);
                _db.AddParameter(cmd, "PrimaryZoneId", zoneId);
                _db.AddParameter(cmd, "OtherZoneId", zoneIdList);
                _db.AddParameter(cmd, "TemplateId", tag.TemplateId);
                _db.AddParameter(cmd, "CreatedDate", tag.CreatedDate);
                _db.AddParameter(cmd, "EditedBy", tag.EditedBy);

                var numberOfRow = cmd.ExecuteNonQuery();

                tagId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
    }
}
