﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.ContentLog;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.ContentLog
{
 public abstract class ContentLogDalBase
    {
        #region Update

        public bool Insert(ContentLogEntity obj, ref int id)
        {
            const string commandText = "CMS_ContentLogs_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ObjbectId", obj.ObjbectId);
                _db.AddParameter(cmd, "Content", obj.Content);
                _db.AddParameter(cmd, "CreatedDate", obj.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", obj.CreatedBy);
                _db.AddParameter(cmd, "ObjectType", obj.ObjectType);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        //public bool UpdateFolder(ContentLogEntity obj, ref int newFolderId)
        //{
        //    const string commandText = "CMS_Folders_Update";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", obj.Id);
        //        _db.AddParameter(cmd, "Name", obj.Name);
        //        _db.AddParameter(cmd, "ModifyBy", obj.ModifyBy);
        //        _db.AddParameter(cmd, "ParentId", obj.ParentId);
        //        _db.AddParameter(cmd, "FolderPath", obj.FolderPath);

        //        var numberOfRow = cmd.ExecuteNonQuery();
        //        //   newFolderId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}
        public bool Delete(int id)
        {
            const string commandText = "CMS_ContentLogs_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        #endregion

        #region Get

        public ContentLogEntity GetById(int FolderId)
        {
            const string commandText = "CMS_ContentLogs_GetById";
            try
            {
                ContentLogEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", FolderId);
                data = _db.Get<ContentLogEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<ContentLogEntity> Search(long objectId,byte objectType)
        {
            const string commandText = "CMS_ContentLogs_Search";
            try
            {
                IEnumerable<ContentLogEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                  _db.AddParameter(cmd, "ObjectId", objectId);
                  _db.AddParameter(cmd, "ObjectType", objectType);

                data = _db.GetList<ContentLogEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
     
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected ContentLogDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
