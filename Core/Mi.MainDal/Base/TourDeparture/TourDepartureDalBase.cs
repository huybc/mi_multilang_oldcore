﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Entity.Base.Departure;
using Mi.Entity.Base.ProjectDetail;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.TourDeparture
{
    public abstract class TourDepartureDalBase
    {
        #region Gets

        public List<DepartureEntity> GetByTourId(long id)
        {
            const string commandText = "CMS_TourDeparture_GetByTourId";
            try
            {
                List<DepartureEntity> data = new List<DepartureEntity>();
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                data = _db.GetList<DepartureEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<DepartureEntity> GetByTourIdCurentTime(long id)
        {
            const string commandText = "CMS_TourDeparture_GetByTourIdV2";
            try
            {
                List<DepartureEntity> data = new List<DepartureEntity>();
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                data = _db.GetList<DepartureEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Sets


        public bool Create(DepartureEntity obj)
        {

            const string commandText = "CMS_Departure_Insert";
            try
            {

                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "StartDate", obj.StartDate);
                _db.AddParameter(cmd, "EndDate", obj.EndDate);
                _db.AddParameter(cmd, "Price", obj.Price);
                _db.AddParameter(cmd, "Status", obj.Status);
                _db.AddParameter(cmd, "TourId", obj.TourId);
                _db.AddParameter(cmd, "ChildPrice", obj.ChildPrice);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(long id)
        {

            const string commandText = "CMS_Departure_Delete_ByTourId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected TourDepartureDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
