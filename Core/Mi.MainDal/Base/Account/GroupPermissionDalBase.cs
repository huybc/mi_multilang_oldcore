﻿using System;
using System.Collections.Generic;
using Mi.Entity.Base.Security;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Account
{
    public abstract class GroupPermissionDalBase
    {
        public List<GroupPermissionEntity> GetAllGroupPermission()
        {
            const string commandText = "CMS_GroupPermission_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var data = _db.GetList<GroupPermissionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool CheckUserInGroupPermission(int userId, int groupId)
        {
            const string commandText = "CMS_GroupPermission_CheckUserInGroupPermission";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userId);
                _db.AddParameter(cmd, "GroupId", groupId);
                var reader = cmd.ExecuteReader();

                return reader.Read() && reader["ExistsGroupPermission"].ToString() == "1";
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        #region Core members

        private readonly CmsMainDb _db;
        
        protected GroupPermissionDalBase(CmsMainDb db)
		{
			_db = db;
		}

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
