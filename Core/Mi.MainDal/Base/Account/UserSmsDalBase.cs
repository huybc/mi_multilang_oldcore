﻿using System;
using Mi.Common;
using Mi.Entity.Base.Security;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Account
{
    public abstract class UserSmsDalBase
    {
        #region Sets

        public bool AddnewSmsCode(UserSmsEntity userSms)
        {
            const string commandText = "CMS_UserSms_Addnew";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userSms.UserId);
                _db.AddParameter(cmd, "SmsCode", userSms.SmsCode);
                _db.AddParameter(cmd, "ExpiredDate", userSms.ExpiredDate);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(int userId, string smsCode, DateTime expiredDate)
        {
            const string commandText = "CMS_UserSMS_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userId);
                _db.AddParameter(cmd, "SmsCode", smsCode);
                _db.AddParameter(cmd, "ExpiredDate", expiredDate);
                _db.AddParameter(cmd, "InUsing", true);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool RemoveInUsing(int userId)
        {
            const string commandText = "CMS_UserSms_RemoveInUsing";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Gets

        public UserSmsEntity GetUserSmsByUserId(int userId)
        {
            const string commandText = "CMS_UserSms_GetByUserId";
            try
            {
                UserSmsEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userId);
                data = _db.Get<UserSmsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int CheckValidateCode(string userName, string smsCode)
        {
            const string commandText = "CMS_UserSMS_ValidCode";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", userName);
                _db.AddParameter(cmd, "SmsCode", smsCode);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool GetSmsCode(string userName, ref string smsCode, ref string phoneNumber)
        {
            const string commandText = "CMS_UserSms_GetByUserId";
            try
            {
                UserSmsEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", userName);
                data = _db.Get<UserSmsEntity>(cmd);
                if (data != null && data.UserId > 0)
                {
                    smsCode = data.SmsCode;
                    phoneNumber = data["Mobile"] != null ? data["Mobile"].ToString() : string.Empty;
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected UserSmsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
