﻿using System;
using System.Collections.Generic;
using Mi.Entity.Base;
using Mi.Entity.Base.Province;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Cadastral
{
    public abstract class CadastralDalBase
    {
        public IEnumerable<DisctrictEntity> DistrictGetByProvinceCode(string provinceCode)
        {
            const string commandText = "FE_DistrictGetByProvinceCode";
            try
            {
                List<DisctrictEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "provinceCode", provinceCode);
                data = _db.GetList<DisctrictEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<ProvinceEntity> ProvinceGetAll()
        {
            const string commandText = "FE_ProvinceGetAll";
            try
            {
                List<ProvinceEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<ProvinceEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public ProvinceEntity GetProvinceByCode(string code)
        {
            const string commandText = "FE_Province_GetByCode";
            try
            {
                ProvinceEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Code", code);
                data = _db.Get<ProvinceEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public DisctrictEntity GetDistrictByCode(string code)
        {
            const string commandText = "FE_District_GetByCode";
            try
            {
                DisctrictEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Code", code);
                data = _db.Get<DisctrictEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        #region Core members

        private readonly CmsMainDb _db;

        protected CadastralDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
