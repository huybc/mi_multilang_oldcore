﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.MainDal.Common;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.News
{
    public abstract class NewsDalBase
    {
        public bool InsertNews(NewsEntity news, int zoneId, string tagIdList, ref long newsId)
        {
            const string commandText = "CMS_News_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newsId, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Body", news.Body);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "Source", news.Source);
                _db.AddParameter(cmd, "Type", news.Type);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", news.DistributionDate);
                _db.AddParameter(cmd, "CreatedBy", news.CreatedBy);
                _db.AddParameter(cmd, "EditedBy", news.EditedBy);
                _db.AddParameter(cmd, "PublishedBy", news.PublishedBy);
                _db.AddParameter(cmd, "WordCount", news.WordCount);
                _db.AddParameter(cmd, "ViewCount", news.ViewCount);
                _db.AddParameter(cmd, "LikeCount", news.LikeCount);
                _db.AddParameter(cmd, "Status", news.Status);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                //_db.AddParameter(cmd, "TermIdList", termIdList);
                //_db.AddParameter(cmd, "Product_Tool_IdList", productTool);
                //_db.AddParameter(cmd, "Product_Replacement_IdList", productRep);
                //_db.AddParameter(cmd, "Product_Service_IdList", productService);

                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "IsOnMobile", news.IsOnMobile);
                _db.AddParameter(cmd, "SourceURL", news.SourceURL);

                _db.AddParameter(cmd, "MetaKeyword", news.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", news.MetaDescription);
                _db.AddParameter(cmd, "TitleSeo", news.TitleSeo);
                _db.AddParameter(cmd, "IsLandingPage", news.IsLandingPage);
                _db.AddParameter(cmd, "Css", news.Css);
                _db.AddParameter(cmd, "Script", news.Script);
                _db.AddParameter(cmd, "IsAllowComment", news.IsAllowComment);
                _db.AddParameter(cmd, "IsVideo", news.IsVideo);
                _db.AddParameter(cmd, "IsHot", news.IsHot);
                //_db.AddParameter(cmd, "EventStart", news.EventStart);
                //_db.AddParameter(cmd, "EventStop", news.EventStart);

                _db.AddParameter(cmd, "HoursOfWork", news.HoursOfWork);
                _db.AddParameter(cmd, "Invest", news.Invest);
                _db.AddParameter(cmd, "SurfaceArea", news.SurfaceArea);
                _db.AddParameter(cmd, "Budget", news.Budget);
                _db.AddParameter(cmd, "GroupId", news.GroupId);
                _db.AddParameter(cmd, "LanguageCode", news.LanguageCode);
                _db.AddParameter(cmd, "AvatarTron", news.AvatarTron);
                _db.AddParameter(cmd, "Banner", news.Banner);

                var numberOfRow = cmd.ExecuteNonQuery();
                newsId = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertTour(TourEntity news, string zoneIds, string tagIdList, ref long newsId)
        {
            const string commandText = "CMS_Tour_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newsId, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Body", news.Body);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "AvatarArray", news.AvatarArray);
                _db.AddParameter(cmd, "Source", news.Source);
                _db.AddParameter(cmd, "Type", news.Type);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", news.DistributionDate);
                _db.AddParameter(cmd, "CreatedBy", news.CreatedBy);
                _db.AddParameter(cmd, "EditedBy", news.EditedBy);
                _db.AddParameter(cmd, "PublishedBy", news.PublishedBy);
                _db.AddParameter(cmd, "WordCount", news.WordCount);
                _db.AddParameter(cmd, "ViewCount", news.ViewCount);
                _db.AddParameter(cmd, "LikeCount", news.LikeCount);
                _db.AddParameter(cmd, "Status", news.Status);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                //_db.AddParameter(cmd, "TermIdList", termIdList);
                //_db.AddParameter(cmd, "Product_Tool_IdList", productTool);
                //_db.AddParameter(cmd, "Product_Replacement_IdList", productRep);
                //_db.AddParameter(cmd, "Product_Service_IdList", productService);

                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "IsOnMobile", news.IsOnMobile);
                _db.AddParameter(cmd, "SourceURL", news.SourceURL);

                _db.AddParameter(cmd, "MetaKeyword", news.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", news.MetaDescription);
                _db.AddParameter(cmd, "TitleSeo", news.TitleSeo);
                _db.AddParameter(cmd, "IsLandingPage", news.IsLandingPage);
                _db.AddParameter(cmd, "Css", news.Css);
                _db.AddParameter(cmd, "Script", news.Script);
                _db.AddParameter(cmd, "IsAllowComment", news.IsAllowComment);
                _db.AddParameter(cmd, "IsVideo", news.IsVideo);
                _db.AddParameter(cmd, "IsHot", news.IsHot);

                _db.AddParameter(cmd, "SubTitle", news.SubTitle);
                _db.AddParameter(cmd, "Code", news.Code);
                _db.AddParameter(cmd, "Locations", news.Locations);
                _db.AddParameter(cmd, "Transfers", news.Transfers);
                _db.AddParameter(cmd, "Rate", news.Rate);
                _db.AddParameter(cmd, "LanguageCode", news.LanguageCode);
                var numberOfRow = cmd.ExecuteNonQuery();
                newsId = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertNewsV2(NewsEntity news, string ZoneIds, string tagIdList, ref long newsId)
        {
            const string commandText = "CMS_News_InsertV2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newsId, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Body", news.Body);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "Source", news.Source);
                _db.AddParameter(cmd, "Type", news.Type);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", news.DistributionDate);
                _db.AddParameter(cmd, "CreatedBy", news.CreatedBy);
                _db.AddParameter(cmd, "EditedBy", news.EditedBy);
                _db.AddParameter(cmd, "PublishedBy", news.PublishedBy);
                _db.AddParameter(cmd, "WordCount", news.WordCount);
                _db.AddParameter(cmd, "ViewCount", news.ViewCount);
                _db.AddParameter(cmd, "LikeCount", news.LikeCount);
                _db.AddParameter(cmd, "Status", news.Status);
                _db.AddParameter(cmd, "ZoneIds", ZoneIds);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "IsOnMobile", news.IsOnMobile);
                _db.AddParameter(cmd, "SourceURL", news.SourceURL);

                _db.AddParameter(cmd, "MetaKeyword", news.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", news.MetaDescription);
                _db.AddParameter(cmd, "TitleSeo", news.TitleSeo);
                _db.AddParameter(cmd, "IsLandingPage", news.IsLandingPage);
                _db.AddParameter(cmd, "Css", news.Css);
                _db.AddParameter(cmd, "Script", news.Script);
                _db.AddParameter(cmd, "IsAllowComment", news.IsAllowComment);
                _db.AddParameter(cmd, "IsVideo", news.IsVideo);
                _db.AddParameter(cmd, "IsHot", news.IsHot);
                //_db.AddParameter(cmd, "EventStart", news.EventStart);
                //_db.AddParameter(cmd, "EventStop", news.EventStart);
                var numberOfRow = cmd.ExecuteNonQuery();
                newsId = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SaveNewsInLanguage(NewsWithLanguageEntity entity)
        {
            const string commandText = "uspH_SaveNewsInLanguage";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", entity.NewsId);
                _db.AddParameter(cmd, "Title", entity.Title);
                _db.AddParameter(cmd, "Sapo", entity.Sapo);
                _db.AddParameter(cmd, "Body", entity.Body);
                _db.AddParameter(cmd, "Tags", entity.Tags);
                _db.AddParameter(cmd, "MetaKeyword", entity.MetaKeyword);
                _db.AddParameter(cmd, "MetaTitle", entity.MetaTitle);
                _db.AddParameter(cmd, "MetaDescription", entity.MetaDescription);
                _db.AddParameter(cmd, "Extras", entity.Extras);
                _db.AddParameter(cmd, "LanguageCode", entity.LanguageCode);
                //_db.AddParameter(cmd, "tblPart", entity.LanguageCode);
                
                var numberOfRow = cmd.ExecuteNonQuery();
                //newsId = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SaveNewsInLanguage_v1(NewsWithLanguageEntity entity)
        {
            const string commandText = "uspH_SaveNewsInLanguage";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", entity.NewsId);
                _db.AddParameter(cmd, "Title", entity.Title);
                _db.AddParameter(cmd, "Sapo", entity.Sapo);
                _db.AddParameter(cmd, "Body", entity.Body);
                _db.AddParameter(cmd, "Tags", entity.Tags);
                _db.AddParameter(cmd, "MetaKeyword", entity.MetaKeyword);
                _db.AddParameter(cmd, "MetaTitle", entity.MetaTitle);
                _db.AddParameter(cmd, "MetaDescription", entity.MetaDescription);
                _db.AddParameter(cmd, "Extras", entity.Extras);
                _db.AddParameter(cmd, "LanguageCode", entity.LanguageCode);
                _db.AddParameter(cmd, "tblPart", entity.PartOfType);

                var numberOfRow = cmd.ExecuteNonQuery();
                //newsId = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<NewsWithLanguageEntity> GetNewsInLanguage(int newsId, string languageCode)
        {
            const string commandText = "uspH_GetNewsInLanguage";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "LanguageCode", languageCode);

                var data = _db.GetList<NewsWithLanguageEntity>(cmd);
                //newsId = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<Part> GetPartInNews(int newsId, string languageCode)
        {
            const string commandText = "uspH_GetPartInNewsBYNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "id", newsId);
                _db.AddParameter(cmd, "lang_code", languageCode);

                var data = _db.GetList<Part>(cmd);
                //newsId = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        //public IEnumerable<NewsWithLanguageEntity> GetNewsInLanguage_v1(int newsId, string languageCode, out object parts)
        //{
        //    const string commandText = "uspH_GetNewsInLanguage_v1";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "NewsId", newsId);
        //        _db.AddParameter(cmd, "LanguageCode", languageCode);

        //        var data = _db.GetList<NewsWithLanguageEntity>(cmd);
        //        //newsId = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));

        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        public bool UpdateNews(NewsEntity news, int ZoneId, string tagIdList)
        {
            const string commandText = "CMS_News_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", news.Id);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Body", news.Body);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "Author", news.Author);
                _db.AddParameter(cmd, "Status", news.Status);
                _db.AddParameter(cmd, "Source", news.Source);
                _db.AddParameter(cmd, "Type", news.Type);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", news.DistributionDate);
                _db.AddParameter(cmd, "LastModifiedBy", news.LastModifiedBy);
                _db.AddParameter(cmd, "WordCount", news.WordCount);
                _db.AddParameter(cmd, "ViewCount", news.ViewCount);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "IsOnMobile", news.IsOnMobile);
                _db.AddParameter(cmd, "SourceURL", news.SourceURL);

                _db.AddParameter(cmd, "MetaKeyword", news.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", news.MetaDescription);
                _db.AddParameter(cmd, "TitleSeo", news.TitleSeo);
                _db.AddParameter(cmd, "IsLandingPage", news.IsLandingPage);
                _db.AddParameter(cmd, "Css", news.Css);
                _db.AddParameter(cmd, "Script", news.Script);
                _db.AddParameter(cmd, "IsAllowComment", news.IsAllowComment);
                _db.AddParameter(cmd, "IsVideo", news.IsVideo);
                _db.AddParameter(cmd, "IsHot", news.IsHot);

                _db.AddParameter(cmd, "HoursOfWork", news.HoursOfWork);
                _db.AddParameter(cmd, "Invest", news.Invest);
                _db.AddParameter(cmd, "SurfaceArea", news.SurfaceArea);
                _db.AddParameter(cmd, "Budget", news.Budget);
                _db.AddParameter(cmd, "GroupId", news.GroupId);
                _db.AddParameter(cmd, "LanguageCode", news.LanguageCode);
                _db.AddParameter(cmd, "AvatarTron", news.AvatarTron);
                _db.AddParameter(cmd, "Banner", news.Banner);

                //_db.AddParameter(cmd, "EventStart", news.EventStart);
                //_db.AddParameter(cmd, "EventStop", news.EventStart);
                var numberOfRow = _db.ExecuteNonQuery(cmd);

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateTour(TourEntity news, int ZoneId, string tagIdList)
        {
            const string commandText = "CMS_Tour_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", news.Id);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Body", news.Body);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "AvatarArray", news.AvatarArray);
                _db.AddParameter(cmd, "Author", news.Author);
                _db.AddParameter(cmd, "Status", news.Status);
                _db.AddParameter(cmd, "Source", news.Source);
                _db.AddParameter(cmd, "Type", news.Type);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", news.DistributionDate);
                _db.AddParameter(cmd, "LastModifiedBy", news.LastModifiedBy);
                _db.AddParameter(cmd, "WordCount", news.WordCount);
                _db.AddParameter(cmd, "ViewCount", news.ViewCount);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "IsOnMobile", news.IsOnMobile);
                _db.AddParameter(cmd, "SourceURL", news.SourceURL);

                _db.AddParameter(cmd, "MetaKeyword", news.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", news.MetaDescription);
                _db.AddParameter(cmd, "TitleSeo", news.TitleSeo);
                _db.AddParameter(cmd, "IsLandingPage", news.IsLandingPage);
                _db.AddParameter(cmd, "Css", news.Css);
                _db.AddParameter(cmd, "Script", news.Script);
                _db.AddParameter(cmd, "IsAllowComment", news.IsAllowComment);
                _db.AddParameter(cmd, "IsVideo", news.IsVideo);
                _db.AddParameter(cmd, "IsHot", news.IsHot);

                _db.AddParameter(cmd, "SubTitle", news.SubTitle);
                _db.AddParameter(cmd, "Code", news.Code);
                _db.AddParameter(cmd, "Locations", news.Locations);
                _db.AddParameter(cmd, "Transfers", news.Transfers);
                _db.AddParameter(cmd, "Rate", news.Rate);
                _db.AddParameter(cmd, "LanguageCode", news.LanguageCode);
                //_db.AddParameter(cmd, "EventStart", news.EventStart);
                //_db.AddParameter(cmd, "EventStop", news.EventStart);
                var numberOfRow = _db.ExecuteNonQuery(cmd);

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateTourv1(TourEntity news, string ZoneIds, string tagIdList)
        {
            const string commandText = "CMS_Tour_Update_v1";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", news.Id);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Address", news.Address);
                _db.AddParameter(cmd, "Body", news.Body);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "AvatarArray", news.AvatarArray);
                _db.AddParameter(cmd, "Author", news.Author);
                _db.AddParameter(cmd, "Status", news.Status);
                _db.AddParameter(cmd, "Source", news.Source);
                _db.AddParameter(cmd, "Type", news.Type);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", news.DistributionDate);
                _db.AddParameter(cmd, "LastModifiedBy", news.LastModifiedBy);
                _db.AddParameter(cmd, "WordCount", news.WordCount);
                _db.AddParameter(cmd, "ViewCount", news.ViewCount);
                //  _db.AddParameter(cmd, "ZoneId", ZoneId);
                _db.AddParameter(cmd, "ZoneIds", ZoneIds);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "IsOnMobile", news.IsOnMobile);
                _db.AddParameter(cmd, "SourceURL", news.SourceURL);

                _db.AddParameter(cmd, "MetaKeyword", news.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", news.MetaDescription);
                _db.AddParameter(cmd, "TitleSeo", news.TitleSeo);
                _db.AddParameter(cmd, "IsLandingPage", news.IsLandingPage);
                _db.AddParameter(cmd, "Css", news.Css);
                _db.AddParameter(cmd, "Script", news.Script);
                _db.AddParameter(cmd, "IsAllowComment", news.IsAllowComment);
                _db.AddParameter(cmd, "IsVideo", news.IsVideo);
                _db.AddParameter(cmd, "IsHot", news.IsHot);

                _db.AddParameter(cmd, "SubTitle", news.SubTitle);
                _db.AddParameter(cmd, "Code", news.Code);
                _db.AddParameter(cmd, "Locations", news.Locations);
                _db.AddParameter(cmd, "Transfers", news.Transfers);
                _db.AddParameter(cmd, "Rate", news.Rate);
                _db.AddParameter(cmd, "LanguageCode", news.LanguageCode);

                //_db.AddParameter(cmd, "EventStart", news.EventStart);
                //_db.AddParameter(cmd, "EventStop", news.EventStart);
                var numberOfRow = _db.ExecuteNonQuery(cmd);

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateNewsV2(NewsEntity news, string zoneIds, string tagIdList)
        {
            const string commandText = "CMS_News_UpdateV2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", news.Id);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Body", news.Body);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "Author", news.Author);
                _db.AddParameter(cmd, "Status", news.Status);
                _db.AddParameter(cmd, "Source", news.Source);
                _db.AddParameter(cmd, "Type", news.Type);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", news.DistributionDate);
                _db.AddParameter(cmd, "LastModifiedBy", news.LastModifiedBy);
                _db.AddParameter(cmd, "WordCount", news.WordCount);
                _db.AddParameter(cmd, "ViewCount", news.ViewCount);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "IsOnMobile", news.IsOnMobile);
                _db.AddParameter(cmd, "SourceURL", news.SourceURL);

                _db.AddParameter(cmd, "MetaKeyword", news.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", news.MetaDescription);
                _db.AddParameter(cmd, "TitleSeo", news.TitleSeo);
                _db.AddParameter(cmd, "IsLandingPage", news.IsLandingPage);
                _db.AddParameter(cmd, "Css", news.Css);
                _db.AddParameter(cmd, "Script", news.Script);
                _db.AddParameter(cmd, "IsAllowComment", news.IsAllowComment);
                _db.AddParameter(cmd, "IsVideo", news.IsVideo);
                _db.AddParameter(cmd, "IsHot", news.IsHot);
                //_db.AddParameter(cmd, "EventStart", news.EventStart);
                //_db.AddParameter(cmd, "EventStop", news.EventStart);
                var numberOfRow = _db.ExecuteNonQuery(cmd);

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsAvatar(long newsId, string avatar)
        {
            const string commandText = "CMS_News_UpdateAvatar";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "@NewsId", newsId);
                _db.AddParameter(cmd, "@Avatar", avatar);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsIsAnswered(long newsId, bool isAnswered)
        {
            const string commandText = "CMS_News_UpdateIsAnswered";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "@NewsId", newsId);
                _db.AddParameter(cmd, "@IsAnswered", isAnswered);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool VoteNews(long id, EnumNewsVoteDirection voteDirection)
        {
            const string commandText = "CMS_News_Vote";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Direction", (int)voteDirection);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteNews(long id)
        {
            const string commandText = "CMS_News_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusComment(long id, string modifyBy, byte status)
        {
            const string commandText = "CMS_News_ChangeStatusComment";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "LastModifiedBy", modifyBy);
                _db.AddParameter(cmd, "IsAllowComment", status);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ChangeStatusToWaitPublish(long id, string modifyBy)
        {
            const string commandText = "CMS_News_ChangeStatusToWaitPublish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "LastModifiedBy", modifyBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ChangeStatusToPublished(long id, string publishedBy, DateTime publishedDate)
        {
            const string commandText = "CMS_News_ChangeStatusToPublished";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "PublishedBy", publishedBy);
                _db.AddParameter(cmd, "PublishedDate", publishedDate);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ChangeStatusToReject(long id, string publishedBy)
        {
            const string commandText = "CMS_News_ChangeStatusToReject";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "PublishedBy", publishedBy);
                _db.AddParameter(cmd, "PublishedDate", DateTime.Now);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ChangeStatusToUnpublished(long id, string lastModifiedBy)
        {
            const string commandText = "CMS_News_ChangeStatusToUnpublished";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToMovedToTrash(long id, string lastModifiedBy)
        {
            const string commandText = "CMS_News_ChangeStatusToMovedToTrash";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToRestoreFromTrash(long id, string lastModifidedBy)
        {
            const string commandText = "CMS_News_ChangeStatusToRestoreFromTrash";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "LastModidiedBy", lastModifidedBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsForValidateEntity GetNewsForValidateById(long id)
        {
            const string commandText = "CMS_News_GetNewsForValidateByNewsId";
            try
            {
                NewsForValidateEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<NewsForValidateEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsInfoForCachedEntity GetNewsInfoForCachedById(long id)
        {
            const string commandText = "CMS_News_GetNewsInfoForCachedByNewsId";
            try
            {
                NewsInfoForCachedEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<NewsInfoForCachedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsEntity GetNewsById(long id)
        {
            const string commandText = "CMS_News_GetNewsById";
            try
            {
                NewsEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public NewsEntity GetNewsById(long id, int type)
        {
            const string commandText = "CMS_News_GetNewsByIdAndType";
            try
            {
                NewsEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Type", type);
                data = _db.Get<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public NewsEntity GetNewsByIdv2(long id)
        {
            const string commandText = "CMS_News_GetNewsById_v2";
            try
            {
                NewsEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public TourBasicEntity GetNewsByIdv1(long id)
        {
            const string commandText = "CMS_News_GetNewsById_v1";
            try
            {
                TourBasicEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<TourBasicEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public TourEntity GetTourById(long id)
        {
            const string commandText = "CMS_News_GetNewsById";
            try
            {
                TourEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<TourEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public NewsEntity CMS_News_GetNewsDetailAndZoneById(long id)
        {
            const string commandText = "CMS_News_GetNewsDetailAndZoneById";
            try
            {
                NewsEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsWithAnalyticEntity> GetNewsByListNewsId(int status, string listNewsId)
        {
            const string commandText = "CMS_News_GetNewsByListNewsId";
            try
            {
                List<NewsWithAnalyticEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "ListNewsId", listNewsId);
                data = _db.GetList<NewsWithAnalyticEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsWithDistributionDateEntity> GetNewsDistributionDateByListNewsId(string listNewsId)
        {
            const string commandText = "CMS_News_GetNewsDistributionDateByListNewsId";
            try
            {
                List<NewsWithDistributionDateEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListNewsId", listNewsId);
                data = _db.GetList<NewsWithDistributionDateEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<NewsInListEntity> FESearchNews(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int filterFieldForUsername, int sortOrder, int status, int type, int isAnswered, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "FE_News_Search";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "FilterFieldForUsername", filterFieldForUsername);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "IsAnswered", isAnswered);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<NewsInListEntity> SearchNews(string keyword,string languageCode, string username, string zoneIds, DateTime fromDate, DateTime toDate, int filterFieldForUsername, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_Search";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "languageCode", languageCode);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "FilterFieldForUsername", filterFieldForUsername);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<NewsInListEntity> SearchNewsWithDetail(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int filterFieldForUsername, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_Search_With_Detail";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "FilterFieldForUsername", filterFieldForUsername);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsInListEntity> GetNewsByTagIdWithPaging(long tagId, DateTime fromDate, DateTime toDate, int filterFieldForUsername, int sortOrder, int status, int type, int isAnswered, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchByTagId";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "TagId", tagId);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "FilterFieldForUsername", filterFieldForUsername);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "IsAnswered", isAnswered);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchNewsByUsername(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchByUsername";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        public List<NewsInListEntity> SearchNewsByStatus(string keyword, string usernameDoAction, string usernameUpdateNews, int filterFieldForUsernameUpdateNews, string zoneIds, DateTime fromDate, DateTime toDate, int type, int status, bool isGetTotalRow, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchByStatus";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "UsernameDoAction", usernameDoAction);
                _db.AddParameter(cmd, "UsernameUpdateNews", usernameUpdateNews);
                _db.AddParameter(cmd, "FilterFieldForUsernameUpdateNews", filterFieldForUsernameUpdateNews);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "IsGetTotalRow", isGetTotalRow);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsEntity> SearchNewsByTag(string tagIds, string tagNames, bool searchBytag, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchByTag";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "TagIds", tagIds);
                _db.AddParameter(cmd, "TagNames", tagNames);
                _db.AddParameter(cmd, "SearchByTag", searchBytag);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchNewsPublishExcludeNewsInTag(int zoneId, string keyword, long tagId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchNewsPublishExcludeNewsInTag";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "tagId", tagId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchNewsPublishExcludeNewsInThread(int zoneId, string keyword, long threadId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchNewsPublishExcludeNewsInThread";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ThreadId", threadId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// NANIA bổ sung hàm SearchNewsByFocusKeyword
        /// Tìm các bài có sử dụng từ khóa Focus này
        /// Created By: 22/03/2013
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        /// 
        public List<NewsEntity> SearchNewsByFocusKeyword(string keyword, int top)
        {
            const string commandText = "CMS_News_SearchByFocusKeyword";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Top", top);
                data = _db.GetList<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsEntity> GetMostedViewNews(string languageCode)
        {
            const string commandText = "uspH_Client_GetTopNewsMostView";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "languageCode", languageCode);
                data = _db.GetList<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsEntity> GetTopNewestNews(string languageCode)
        {
            const string commandText = "uspH_Client_GetTopNewestNews";
            try
            {
                List<NewsEntity> data = null;
              var cmd = _db.CreateCommand(commandText, true);
              _db.AddParameter(cmd, "languageCode", languageCode);
                data = _db.GetList<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsEntity> GetTop4VideoNews(string languageCode)
        {
            const string commandText = "uspH_Client_GetTop4VideoNews";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "languageCode", languageCode);
                data = _db.GetList<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        public List<NewsEntity> GetTop3IsHot()
        {
            const string commandText = "uspH_Client_Get3HotVideos";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, string excludeNewsIds, int newstype, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchNewsWhichPublished";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "NewsType", newstype);
                _db.AddParameter(cmd, "DisplayPosition", displayPosition);
                if (distributedDateFrom <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateFrom", distributedDateFrom);
                if (distributedDateTo <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateTo", distributedDateTo);
                _db.AddParameter(cmd, "ExcludeNewsIds", excludeNewsIds);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsInListEntity> SearchNewsForNewsPosition(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, string excludeNewsIds, int newstype, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchNewsForNewsPosition";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "NewsType", newstype);
                _db.AddParameter(cmd, "DisplayPosition", displayPosition);
                if (distributedDateFrom <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateFrom", distributedDateFrom);
                if (distributedDateTo <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateTo", distributedDateTo);
                _db.AddParameter(cmd, "ExcludeNewsIds", excludeNewsIds);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsInListEntity> SearchNewsWhichPublishedForNewsPosition(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, string excludeNewsIds, int newstype, string excludePositionTypes, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchNewsWhichPublishedForNewsPosition";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "NewsType", newstype);
                _db.AddParameter(cmd, "DisplayPosition", displayPosition);
                if (distributedDateFrom <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateFrom", distributedDateFrom);
                if (distributedDateTo <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateTo", distributedDateTo);
                _db.AddParameter(cmd, "ExcludeNewsIds", excludeNewsIds);
                _db.AddParameter(cmd, "ExcludePositionTypes", excludePositionTypes);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsEntity> SearchNewsByTagId(long tagId)
        {
            const string commandText = "CMS_News_GetNewsByTagId";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagId", tagId);
                data = _db.GetList<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsEntity> SearchNewsByTagIdWithPaging(long tagId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_GetNewsByTagIdWithPaging";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "TagId", tagId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsEntity> SearchNewsByThreadIdWithPaging(long threadId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_GetNewsByThreadIdWithPaging";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ThreadId", threadId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsCounterEntity> CounterNewsByStatus(string accountName, string zoneIds, int role, string commonStatus, bool isByZone)
        {
            const string commandText = "CMS_News_CounterInList";
            try
            {
                List<NewsCounterEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "AccountName", accountName);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "Role", role);
                _db.AddParameter(cmd, "CommonStatus", commonStatus);
                _db.AddParameter(cmd, "IsByZone", isByZone);
                data = _db.GetList<NewsCounterEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsCounterEntity> GetCounter(string accountName, int zoneId)
        {
            const string commandText = "CMS_News_GetCounter";
            try
            {
                List<NewsCounterEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "AccountName", accountName);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = _db.GetList<NewsCounterEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Select dánh sách bài viết để tính nhuận bút
        /// </summary>
        /// <param name="zoneId">Chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu</param>
        /// <param name="endDate">Ngày kết thúc</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả của bài viết</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="order">Order</param>
        /// <returns></returns>
        public List<NewsForRoyaltiesEntity> SelectRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, int pageIndex, int pageSize, int order, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            const string commandText = "CMS_News_SelectRoyalties";
            try
            {
                List<NewsForRoyaltiesEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "StartDate", startDate);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "EndDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategory", NewsCategories);
                _db.AddParameter(cmd, "OriginalId", originalId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = _db.GetList<NewsForRoyaltiesEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsForRoyaltiesEntity> SelectBusinessPrRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, int pageIndex, int pageSize, int order, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            const string commandText = "CMS_News_SelectRoyaltiesBusinessPr";
            try
            {
                List<NewsForRoyaltiesEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "StartDate", startDate);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "EndDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategory", NewsCategories);
                _db.AddParameter(cmd, "OriginalId", originalId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = _db.GetList<NewsForRoyaltiesEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsForRoyaltiesEntity> SelectPRRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, int pageIndex, int pageSize, int order, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            const string commandText = "CMS_News_SelectPRRoyalties";
            try
            {
                List<NewsForRoyaltiesEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "StartDate", startDate);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "EndDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategory", NewsCategories);
                _db.AddParameter(cmd, "OriginalId", originalId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = _db.GetList<NewsForRoyaltiesEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Thống kê nhuận bút dành cho Cafebiz
        /// </summary>
        /// <param name="zoneId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="creator"></param>
        /// <param name="author"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="order"></param>
        /// <param name="NewsCategories"></param>
        /// <param name="newsType"></param>
        /// <param name="viewCountFrom"></param>
        /// <param name="viewCountTo"></param>
        /// <returns></returns>
        public List<NewsForRoyaltiesEntity> SelectRoyaltiesV2(string zoneId, DateTime startDate, DateTime endDate, string creator, int author,
                                                                        int pageIndex, int pageSize, ref int totalRows,
                                                                        int order, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0)
        {
            const string commandText = "CMS_News_SelectRoyaltiesV2";
            try
            {
                List<NewsForRoyaltiesEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "StartDate", startDate);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "EndDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategory", NewsCategories);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = _db.GetList<NewsForRoyaltiesEntity>(cmd);

                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        public List<NewsForRoyaltiesEntity> SelectPRRoyaltiesV2(string zoneId, DateTime startDate, DateTime endDate, string creator, int author,
                                                                       int pageIndex, int pageSize, ref int totalRows,
                                                                       int order, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0)
        {
            const string commandText = "CMS_News_SelectPRRoyaltiesV2";
            try
            {
                List<NewsForRoyaltiesEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "StartDate", startDate);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "EndDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategory", NewsCategories);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = _db.GetList<NewsForRoyaltiesEntity>(cmd);

                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Count tổng số nhuận bút
        /// </summary>
        /// <param name="zoneId">Chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu</param>
        /// <param name="endDate">Ngày kết thúc</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả của bài viết</param>
        public int CountRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            const string commandText = "CMS_News_CountRoyalties";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "StartDate", startDate);
                _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategories", NewsCategories);
                _db.AddParameter(cmd, "OriginalId", originalId);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int CountBusinessPrRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            const string commandText = "CMS_News_CountBusinessPrRoyalties";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "StartDate", startDate);
                _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategories", NewsCategories);
                _db.AddParameter(cmd, "OriginalId", originalId);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int CountPRRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            const string commandText = "CMS_News_CountPRRoyalties";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "StartDate", startDate);
                _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategories", NewsCategories);
                _db.AddParameter(cmd, "OriginalId", originalId);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Get tong tien nhuận bút
        /// </summary>
        /// <param name="zoneId">Chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu</param>
        /// <param name="endDate">Ngày kết thúc</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả của bài viết</param>
        public decimal SelectTotalPrice(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, string newsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            const string commandText = "CMS_News_SelectTotalPrice";
            try
            {
                decimal data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "StartDate", startDate);
                _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategory", newsCategories);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "OriginalId", originalId);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = Utility.ConvertToDecimal(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Update giá tiền cho bài viết
        /// </summary>
        /// <param name="id">Id bài viết</param>
        /// <param name="price">Giá tiền cho bài viết</param>
        /// <returns></returns>
        public bool UpdatePrice(long id, decimal price)
        {
            const string commandText = "CMS_News_UpdatePrice";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Price", price);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Update tiền bonus cho bài viết
        /// </summary>
        /// <param name="id">Id bài viết</param>
        /// <param name="price">Tiền bonus cho bài viết</param>
        /// <returns></returns>
        public bool UpdateBonusPrice(long id, decimal bonusPrice)
        {
            const string commandText = "CMS_News_UpdateBonusPrice";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "BonusPrice", bonusPrice);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateIsOnHome(long id, bool isOnHome)
        {
            const string commandText = "CMS_News_UpdateIsOnHome";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "IsOnHome", isOnHome);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateIsFocus(long id, bool isFocus)
        {
            const string commandText = "CMS_News_UpdateIsFocus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "IsFocus", isFocus);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateDisplayInSlide(long id, int displayInSlide)
        {
            const string commandText = "CMS_News_UpdateDisplayInSlide";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "DisplayInSlide", displayInSlide);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Update ghi chú về chấm nhuận bút cho bài viết
        /// </summary>
        /// <param name="id">Id bài viết</param>
        /// <param name="note">Ghi chú cho bài viết</param>
        /// <returns></returns>
        public bool UpdateNoteRoyalties(long id, string note)
        {
            const string commandText = "CMS_News_UpdateNoteRoyalties";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "NoteRoyalties", note);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateOnlyNewsTitle(long newsId, string newsTitle, string newsUrl, string accountName)
        {
            const string commandText = "CMS_News_UpdateOnlyTitle";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "NewsTitle", newsTitle);
                _db.AddParameter(cmd, "NewsUrl", newsUrl);
                _db.AddParameter(cmd, "AccountName", accountName);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateDetail(long newsId, string newsTitle, string sapo, string body, string newsUrl, string accountName)
        {
            const string commandText = "CMS_News_UpdateDetail";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "NewsTitle", newsTitle);
                _db.AddParameter(cmd, "NewsSapo", sapo);
                _db.AddParameter(cmd, "NewsBody", body);
                _db.AddParameter(cmd, "NewsUrl", newsUrl);
                _db.AddParameter(cmd, "AccountName", accountName);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateDisplayPosition(long newsId, int displayPosition, string accountName)
        {
            const string commandText = "CMS_News_UpdateDisplayPosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "DisplayPosition", displayPosition);
                _db.AddParameter(cmd, "AccountName", accountName);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        // Update lại tag trong winservice 

        public bool UpdateTagAutoByNewsId(long newsId, string linkTag, string tagItems)
        {
            const string commandText = "CMS_News_UpdateTagById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Tag", linkTag);
                _db.AddParameter(cmd, "TagItems", tagItems);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        // Update lại tag mới strong bảng Tag 
        public bool UpdateTagAutoForNews(long newsId, string tagListId, ref string tagIdListUpdated)
        {
            const string commandText = "CMS_TagNews_UpdateTagAutoForNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "TagIdList", tagListId);
                tagIdListUpdated = Utility.ConvertToString(_db.GetFirtDataRecord(cmd));

                return !string.IsNullOrEmpty(tagIdListUpdated);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateTagUrlForNews(long newsId, string tagLink, string tagItems)
        {
            const string commandText = "CMS_News_UpdateTagUrl";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "TagLink", tagLink);
                _db.AddParameter(cmd, "TagItems", tagItems);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> GetNewsByIds(string listNewsId, int zoneId)
        {
            const string commandText = "CMS_News_GetByListId";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListId", listNewsId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = _db.GetList<NewsInListEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsForExportEntity> StatisticsExportNewsData(string zoneIds, string sourceIds, int orderBy, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_Statistics_ExportNews";
            try
            {
                List<NewsForExportEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "Sources", sourceIds);
                _db.AddParameter(cmd, "OrderBy", orderBy);
                _db.AddParameter(cmd, "DateFrom", fromDate);
                _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<NewsForExportEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsForExportEntity> StatisticsExportHotNewsData(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_Statistics_ExportHotNews";
            try
            {
                List<NewsForExportEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "Top", top);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<NewsForExportEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsForExportEntity> StatisticsExportNewsHasManyCommentsData(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_Statistics_ExportNewsHasManyComments";
            try
            {
                List<NewsForExportEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "Top", top);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<NewsForExportEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsForExportEntity> GetListNewsByDistributionDate(string zoneIds, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_News_GetByDistributionDate";
            try
            {
                List<NewsForExportEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<NewsForExportEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool PublishNewsContentOnly(long newsId, string body)
        {
            const string commandText = "CMS_News_PublishNewsContentOnly";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Body", body);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsForSuggestionEntity> GetNewsUseRollingNews(int rollingNewsId)
        {
            const string commandText = "CMS_News_GetNewsUseRollingNews";
            try
            {
                List<NewsForSuggestionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RollingNewsId", rollingNewsId);
                data = _db.GetList<NewsForSuggestionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region NewsStats By OriginalId

        public int GetViewCountByOriginalId(string zoneIds, DateTime fromDate, DateTime toDate, int originalId)
        {
            const string commandText = "CMS_News_GetViewCount_By_OriginalId";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "OriginalId", originalId);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchNewsByOriginalId(string zoneIds, DateTime fromDate, DateTime toDate, int originalId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_Search_By_OriginalId";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "OriginalId", originalId);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Update like count

        public bool UpdateLikeCount(long newsId, int likeCount)
        {
            const string commandText = "CMS_News_Update_LikeCount";
            try
            {
                bool data = false;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "LikeCount", likeCount);
                data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsEntity> SearchNewsByDateRange(DateTime startDate, DateTime endDate, int status)
        {
            const string commandText = "CMS_News_SearchNewsByDateRange";
            try
            {
                List<NewsEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DateFrom", startDate);
                _db.AddParameter(cmd, "DateTo", endDate);
                _db.AddParameter(cmd, "status", status);
                data = _db.GetList<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        public NewsEntity GetNewsByTitle(string title)
        {
            const string commandText = "CMS_NewsPublish_GetByNewsTitle";
            try
            {
                NewsEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsTitle", title);
                data = _db.Get<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int CountNewsBySource(int sourceId, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_News_CountBySource";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SourceId", sourceId);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public int CountNewsByZone(int zoneId)
        {
            const string commandText = "CMS_News_CountByZone";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public int CountNewsByType(int type)
        {
            const string commandText = "CMS_News_CountByType";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Type", type);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }




        public IEnumerable<NewsEntity> Search(string title, int sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "uspSearchNews";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Title", title);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.Output);
                data = _db.GetList<NewsEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 4));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<NewsInZoneWithLanguageEntity> GetNewsByZoneIdAndLanguage(int zoneId, string lang_code, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "uspH_GetNewsByZoneIdAndLanguage";
            try
            {
                List<NewsInZoneWithLanguageEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "zoneId", zoneId);
                _db.AddParameter(cmd, "lang_code", lang_code);
                _db.AddParameter(cmd, "pageIndex", pageIndex);
                _db.AddParameter(cmd, "pageSize", pageSize);
                _db.AddParameter(cmd, "totalRow", totalRow, ParameterDirection.Output);
                data = _db.GetList<NewsInZoneWithLanguageEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 4));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<NewsDetailWithLanguageEntity> GetNewsDetailWithLanguage(int type, string lang_code, int hot, int zoneId, string search, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "upsH_GetNewsDetailWithLanguage";
            try
            {
                List<NewsDetailWithLanguageEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "totalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "type", type);
                _db.AddParameter(cmd, "lang_code", lang_code);
                _db.AddParameter(cmd, "hot", hot);
                _db.AddParameter(cmd, "zoneId", zoneId);
                _db.AddParameter(cmd, "search", search);
                _db.AddParameter(cmd, "pageIndex", pageIndex);
                _db.AddParameter(cmd, "pageSize", pageSize);
                
                data = _db.GetList<NewsDetailWithLanguageEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<NewsDetailWithLanguageEntity> GetNewsDetailWithLanguageSameTag(int type, string lang_code, int hot, int zoneId, string search, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "upsH_GetNewsDetailWithLanguageSameTag";
            try
            {
                List<NewsDetailWithLanguageEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "totalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "type", type);
                _db.AddParameter(cmd, "lang_code", lang_code);
                _db.AddParameter(cmd, "hot", hot);
                _db.AddParameter(cmd, "zoneId", zoneId);
                _db.AddParameter(cmd, "search", search);
                _db.AddParameter(cmd, "pageIndex", pageIndex);
                _db.AddParameter(cmd, "pageSize", pageSize);

                data = _db.GetList<NewsDetailWithLanguageEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<NewsDetailWithLanguageEntity> GetNewsWithZoneParentAndLanguage(int zoneId, string lang_code,string search, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "uspH_GetNewsDetailByParentZone";
            try
            {
                List<NewsDetailWithLanguageEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "totalRow", totalRow, ParameterDirection.Output);
                //_db.AddParameter(cmd, "type", type);
                _db.AddParameter(cmd, "lang_code", lang_code);
                //_db.AddParameter(cmd, "hot", hot);
                _db.AddParameter(cmd, "zoneId", zoneId);
                _db.AddParameter(cmd, "search", search);
                _db.AddParameter(cmd, "pageIndex", pageIndex);
                _db.AddParameter(cmd, "pageSize", pageSize);

                data = _db.GetList<NewsDetailWithLanguageEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public NewsDetailWithLanguageEntity GetNewsDetailWithLanguageById(int newsId, string lang_code)
        {
            const string commandText = "uspH_GetNewsDetailWithLanguageById";
            try
            {
                NewsDetailWithLanguageEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                
                _db.AddParameter(cmd, "newsId", newsId);
                _db.AddParameter(cmd, "lang_code", lang_code);
                
                data = _db.Get<NewsDetailWithLanguageEntity>(cmd);
                

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<NewsEntity> SearchByShortUrl(string shortUrl,string languageCode, int pageNumber, int pageSize, ref int totalRow)
        {
            const string commandText = "uspSectNewsInTree";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "shortUrl", shortUrl);
                _db.AddParameter(cmd, "languageCode", languageCode);
                _db.AddParameter(cmd, "PageNumber", pageNumber);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.Output);
                data = _db.GetList<NewsEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 4));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<TourEntityV1> FilterTour(TourFilter tourFilter)
        {
            const string commandText = "uspH_Client_FilterTour";
            try
            {
                List<TourEntityV1> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "locationId", tourFilter.locationId);
                _db.AddParameter(cmd, "startDate", tourFilter.startDate);
                _db.AddParameter(cmd, "minPrice", tourFilter.minPrice);
                _db.AddParameter(cmd, "maxPrice", tourFilter.maxPrice);
                _db.AddParameter(cmd, "type", tourFilter.Type);
                data = _db.GetList<TourEntityV1>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<TourEntityV1> FilterHotel(TourFilter tourFilter)
        {
            const string commandText = "uspH_Client_FilterHotel";
            try
            {
                List<TourEntityV1> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "locationId", tourFilter.locationId);
                //_db.AddParameter(cmd, "startDate", tourFilter.startDate);
                _db.AddParameter(cmd, "minPrice", tourFilter.minPrice);
                _db.AddParameter(cmd, "maxPrice", tourFilter.maxPrice);
                _db.AddParameter(cmd, "type", tourFilter.Type);
                data = _db.GetList<TourEntityV1>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<TourEntityV1> FilterHotelName(TourFilterName tourFilter)
        {
            const string commandText = "uspH_Client_FilterHotel_vName1";
            try
            {
                List<TourEntityV1> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "name", tourFilter.name);
                //_db.AddParameter(cmd, "startDate", tourFilter.startDate);
                _db.AddParameter(cmd, "minPrice", tourFilter.minPrice);
                _db.AddParameter(cmd, "maxPrice", tourFilter.maxPrice);
                _db.AddParameter(cmd, "type", tourFilter.Type);
                data = _db.GetList<TourEntityV1>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<TourEntityV1> GetNewsByLocationType(string type, int newsType)
        {
            const string commandText = "uspH_GetNewsByLocationType";
            try
            {
                List<TourEntityV1> data = null;
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "type", type);
                _db.AddParameter(cmd, "newsType", newsType);
                data = _db.GetList<TourEntityV1>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<TourEntityV1> GetNewsByLocationId(int id)
        {
            const string commandText = "uspH_Client_GetNewSameLocation";
            try
            {
                List<TourEntityV1> data = null;
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "id", id);
                data = _db.GetList<TourEntityV1>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<TourEntityV1> GetAllTour()
        {
            const string commandText = "uspH_Client_GetAllTour";
            try
            {
                List<TourEntityV1> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<TourEntityV1>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<TourEntityV1> SearchByShortUrlTour(string shortUrl, int pageNumber, int pageSize, ref int totalRow)
        {
            const string commandText = "uspH_Client_SectNewsInTree_Tour";
            try
            {
                List<TourEntityV1> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "shortUrl", shortUrl);
                _db.AddParameter(cmd, "PageNumber", pageNumber);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.Output);
                data = _db.GetList<TourEntityV1>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 3));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<TourBasicEntity> SearchByShortUrlTourV2(string shortUrl, string languageCode, int type, int pageNumber, int pageSize, ref int totalRow)
        {
            const string commandText = "uspH_Client_SectNewsInTree_TourV2";
            try
            {
                List<TourBasicEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "shortUrl", shortUrl);
                _db.AddParameter(cmd, "languageCode", languageCode);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "PageNumber", pageNumber);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.Output);
                data = _db.GetList<TourBasicEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 5));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<TourBasicEntity> SearchByShortUrlNewsV2(string shortUrl, string languageCode, int type, int pageNumber, int pageSize, ref int totalRow)
        {
            const string commandText = "uspH_Client_SectNewsInTree_NewsV2";
            try
            {
                List<TourBasicEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "shortUrl", shortUrl);
                _db.AddParameter(cmd, "languageCode", languageCode);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "PageNumber", pageNumber);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.Output);
                data = _db.GetList<TourBasicEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 3));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<TourBasicEntity> GetTopTours(int type, string languageCode, int order, int pageNumber, int pageSize, ref int totalRow)
        {
            const string commandText = "GetTopTour";
            try
            {
                List<TourBasicEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "languageCode", languageCode);
                _db.AddParameter(cmd, "SortOrder", order);
                _db.AddParameter(cmd, "PageIndex", pageNumber);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.Output);
                data = _db.GetList<TourBasicEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 3));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }  public IEnumerable<TourBasicEntity> GetHotTours(int type, string languageCode, int order, int pageNumber, int pageSize, ref int totalRow)
        {
            const string commandText = "GetHotTour";
            try
            {
                List<TourBasicEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "languageCode", languageCode);
                _db.AddParameter(cmd, "SortOrder", order);
                _db.AddParameter(cmd, "PageIndex", pageNumber);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.Output);
                data = _db.GetList<TourBasicEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 5));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<TourBasicEntity> GetTopNews(int type, string languageCode, int order, int pageNumber, int pageSize, ref int totalRow)
        {
            const string commandText = "GetTopNews";
            try
            {
                List<TourBasicEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "languageCode", languageCode);
                _db.AddParameter(cmd, "SortOrder", order);
                _db.AddParameter(cmd, "PageIndex", pageNumber);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.Output);
                data = _db.GetList<TourBasicEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 5));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<TourBasicEntity> SearchByShortUrlTourV3(string shortUrl, int type, int pageNumber, int pageSize, ref int totalRow)
        {
            const string commandText = "uspH_Client_SectNewsInTree_TourV3";
            try
            {
                List<TourBasicEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "shortUrl", shortUrl);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "PageNumber", pageNumber);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.Output);
                data = _db.GetList<TourBasicEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 4));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<NewsEntity> uspSectNewsInTreeWithZone(string shortUrl, int pageNumber, int pageSize, ref int totalRow)
        {
            const string commandText = "uspSectNewsInTreeWithZonew";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "shortUrl", shortUrl);
                _db.AddParameter(cmd, "PageNumber", pageNumber);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.Output);
                data = _db.GetList<NewsEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 3));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<NewsEntity> uspSectNewsByProductType(string shortUrl, int pageNumber, int pageSize, ref int totalRow)
        {
            const string commandText = "uspSectNewsByProductType";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "shortUrl", shortUrl);
                _db.AddParameter(cmd, "PageNumber", pageNumber);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.Output);
                data = _db.GetList<NewsEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 3));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<NewsEntity> GetByParentId(int parentId, int type)
        {
            const string commandText = "CMS_Zone_GetByParentId";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "Type", type);

                data = _db.GetList<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public IEnumerable<NewsEntity> GetByZoneId(int zoneId,string languageCode, int numOfRow)
        {
            const string commandText = "uspH_Client_GetNumOfNewsInZone";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "numberOfNews", numOfRow);
                _db.AddParameter(cmd, "zoneId", zoneId);
                _db.AddParameter(cmd, "languageCode", languageCode);

                data = _db.GetList<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsMainDb _db;

        protected NewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
