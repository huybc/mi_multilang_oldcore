﻿using System;
using System.Collections.Generic;
using Mi.Entity.Base.Language;
using Mi.Entity.Base.News;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.News
{
    public abstract class ConfigDalBase
    {

        public List<LanguageEntity> GetAllLanguage()
        {
            const string commandText = "CMS_Language_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                List<LanguageEntity> data = _db.GetList<LanguageEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ConfigEntity GetByConfigName(string configName)
        {
            const string commandText = "CMS_Config_GetByConfigName";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConfigName", configName);
                ConfigEntity data = _db.Get<ConfigEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ConfigEntity GetConfigNameInLanguage(string configName,string languageCode)
        {
            const string commandText = "CMS_Config_GetByConfigNameInLanguage";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConfigName", configName);
                _db.AddParameter(cmd, "languageCode", languageCode);
                ConfigEntity data = _db.Get<ConfigEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ConfigEntity> GetListConfigByGroupKey(string groupConfigKey, int type)
        {
            const string commandText = "CMS_Config_GetByGroupConfigKey";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConfigGroupKey", groupConfigKey);
                _db.AddParameter(cmd, "ConfigType", type);
                List<ConfigEntity> data = _db.GetList<ConfigEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ConfigLanguageEntity> GetListConfigLanguageByConfigId( int configId)
        {
            const string commandText = "GetListConfigLanguageByConfigId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "configId", configId);
                List<ConfigLanguageEntity> data = _db.GetList<ConfigLanguageEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ConfigLanguageEntity GetListConfigLanguageByConfigName(string name, string lang_code)
        {
            const string commandText = "GetListConfigLanguageByConfigNameWithLanguage";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "name", name);
                _db.AddParameter(cmd, "lang_code", lang_code);
                ConfigLanguageEntity data = _db.Get<ConfigLanguageEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ConfigEntity> GetListConfigByPage(string page, int type)
        {
            const string commandText = "CMS_Config_GetByPage";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Page", page);
                _db.AddParameter(cmd, "ConfigType", type);
                List<ConfigEntity> data = _db.GetList<ConfigEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ConfigEntity> GetAll()
        {
            const string commandText = "CMS_Config_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                List<ConfigEntity> data = _db.GetList<ConfigEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<ConfigEntity> GetAll(string type, bool isEnable)
        {

            const string commandText = "CMS_Config_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "IsEnable", isEnable);
                List<ConfigEntity> data = _db.GetList<ConfigEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SetValue(string configName, string configValue,string Confixinvalue)
        {
            const string commandText = "CMS_Config_SetValueByType";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConfigName", configName);
                _db.AddParameter(cmd, "ConfigValue", configValue);
                _db.AddParameter(cmd, "ConfigInitValue", Confixinvalue);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateConfigLanguageById(int id, string content)
        {
            const string commandText = "UpdateConfigLanguageById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Content", content);

                return  _db.ExecuteNonQuery(cmd) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SetValue(string configName, string configValue, int configType)
        {
            const string commandText = "CMS_Config_SetValueByType";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConfigName", configName);
                _db.AddParameter(cmd, "ConfigValue", configValue);
                _db.AddParameter(cmd, "ConfigType", configType);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        public bool Update(ConfigEntity obj)
        {
            const string commandText = "CMS_Config_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id);
                _db.AddParameter(cmd, "ConfigName", obj.ConfigName);
                _db.AddParameter(cmd, "ConfigLabel", obj.ConfigLabel);
                _db.AddParameter(cmd, "ConfigValue", obj.ConfigValue);
                _db.AddParameter(cmd, "ConfigInitValue", obj.ConfigInitValue);
                _db.AddParameter(cmd, "IsEnable", obj.IsEnable);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SetValueAndLabel(string configName, string configLabel, string configValue)
        {
            const string commandText = "CMS_Config_SetValueAndLabel";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConfigName", configName);
                _db.AddParameter(cmd, "ConfigValue", configValue);
                _db.AddParameter(cmd, "ConfigLabel", configLabel);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SetValueAndLabel(string configName, string configLabel, string configValue, int configType)
        {
            const string commandText = "CMS_Config_SetValueAndLabelByType";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConfigName", configName);
                _db.AddParameter(cmd, "ConfigValue", configValue);
                _db.AddParameter(cmd, "ConfigLabel", configLabel);
                _db.AddParameter(cmd, "ConfigType", configType);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        // Adv
        public AdvEntity AdvGetById(int Id)
        {
            const string commandText = "CMS_Adv_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                AdvEntity data = _db.Get<AdvEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public IEnumerable<AdvEntity> AdvGetByType(int type,string languageCode)
        {
            const string commandText = "CMS_Adv_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "LanguageCode", languageCode);
                List<AdvEntity> data = _db.GetList<AdvEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool AdvUpdate(AdvEntity obj)
        {
            const string commandText = "CMS_Adv_Update";
            try
            {

                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id);
                _db.AddParameter(cmd, "Name", obj.Name);
                _db.AddParameter(cmd, "Content", obj.Content);
                _db.AddParameter(cmd, "SortOrder", obj.SortOrder);
                _db.AddParameter(cmd, "Type", obj.Type);
                _db.AddParameter(cmd, "Position", obj.Position);
                _db.AddParameter(cmd, "Url", obj.Url);
                _db.AddParameter(cmd, "IsEnable", obj.IsEnable);
                _db.AddParameter(cmd, "Thumb", obj.Thumb);
                _db.AddParameter(cmd, "LanguageCode", obj.LanguageCode);
                _db.AddParameter(cmd, "Col", obj.Col);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool AdvInsert(AdvEntity obj)
        {
            const string commandText = "CMS_Adv_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", obj.Name);
                _db.AddParameter(cmd, "Content", obj.Content);
                _db.AddParameter(cmd, "SortOrder", obj.SortOrder);
                _db.AddParameter(cmd, "Type", obj.Type);
                _db.AddParameter(cmd, "Position", obj.Position);
                _db.AddParameter(cmd, "Url", obj.Url);
                _db.AddParameter(cmd, "IsEnable", obj.IsEnable);
                _db.AddParameter(cmd, "Thumb", obj.Thumb);
                _db.AddParameter(cmd, "LanguageCode", obj.LanguageCode);
                _db.AddParameter(cmd, "Col", obj.Col);

                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool AdvDelete(int Id)
        {
            const string commandText = "CMS_Adv_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsMainDb _db;

        protected ConfigDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
