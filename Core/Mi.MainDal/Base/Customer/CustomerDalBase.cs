﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.Customer;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Customer
{
    public abstract class CustomerDalBase
    {
        #region Gets

        public CustomerEntity GetById(int id)
        {
            const string commandText = "CMS_Customer_GetById";
            try
            {
                CustomerEntity data ;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                data = _db.Get<CustomerEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<Customer_By_Month> CustomerByMonthChart(int year)
        {
            const string commandText = "uspH_CustomerChartByMonth";
            try
            {
                IEnumerable<Customer_By_Month> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "year_picker", year);

                data = _db.GetList<Customer_By_Month>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<Customer_By_CoSo> CustomerByCoSoChart(DateTime startDate, DateTime endDate)
        {
            const string commandText = "uspH_CustomerChatByCoSo";
            try
            {
                IEnumerable<Customer_By_CoSo> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "start_date", startDate);
                _db.AddParameter(cmd, "end_date", endDate);

                data = _db.GetList<Customer_By_CoSo>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public Customer_Theo_Tuoi CustomerByTuoi(DateTime startDate, DateTime endDate)
        {
            const string commandText = "uspH_BarChartTheoDoTuoi";
            try
            {
                Customer_Theo_Tuoi data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "start_date", startDate);
                _db.AddParameter(cmd, "end_date", endDate);

                data = _db.Get<Customer_Theo_Tuoi>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<CustomerEntity> Search(string name,string phone,string type,int pageIndex,int pageSize, ref int totalRows)
        {
            const string commandText = "CMS_Customer_SearchWithPaging";
            try
            {
                IEnumerable<CustomerEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", name);
                _db.AddParameter(cmd, "Phone", phone);
                _db.AddParameter(cmd, "type", type);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<CustomerEntity>(cmd);

                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Sets


        public bool Create(CustomerEntity obj, ref int id)
        {

            const string commandText = "CMS_Customer_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "FullName", obj.FullName);
                _db.AddParameter(cmd, "Email", obj.Email);
                _db.AddParameter(cmd, "Mobile", obj.Mobile);
                _db.AddParameter(cmd, "Type", obj.Type);
                _db.AddParameter(cmd, "Firm", obj.Firm);
                _db.AddParameter(cmd, "Status", obj.Status);
                _db.AddParameter(cmd, "Note", obj.Note);
                _db.AddParameter(cmd, "Contactperson", obj.Contactperson);
                _db.AddParameter(cmd, "Service", obj.Service);
                _db.AddParameter(cmd, "Address", obj.Address);
                _db.AddParameter(cmd, "NgayKham", obj.NgayKham);
                _db.AddParameter(cmd, "NgaySinh", obj.NgaySinh);
                _db.AddParameter(cmd, "location", obj.location);
                _db.AddParameter(cmd, "CoSoKham", obj.CoSoKham);
                _db.AddParameter(cmd, "NguoiLon", obj.NguoiLon);
                _db.AddParameter(cmd, "TreEm", obj.TreEm);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(CustomerEntity obj, ref int id)
        {
            const string commandText = "CMS_Customer_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id);
                _db.AddParameter(cmd, "FullName", obj.FullName);
                _db.AddParameter(cmd, "Email", obj.Email);
                _db.AddParameter(cmd, "Mobile", obj.Mobile);
                _db.AddParameter(cmd, "Type", obj.Type);
                _db.AddParameter(cmd, "Firm", obj.Firm);
                _db.AddParameter(cmd, "Status", obj.Status);
                _db.AddParameter(cmd, "Note", obj.Note);
                _db.AddParameter(cmd, "Contactperson", obj.Contactperson);
                _db.AddParameter(cmd, "Service", obj.Service);
                _db.AddParameter(cmd, "Address", obj.Address);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update_NoteNhanVien(int id, string note)
        {
            const string commandText = "uspH_SaveCustomerNoteNhanVien";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "id", id);
                _db.AddParameter(cmd, "note", note);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int id)
        {

            const string commandText = "";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected CustomerDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
