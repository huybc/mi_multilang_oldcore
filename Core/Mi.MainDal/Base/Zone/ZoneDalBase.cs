﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.Language;
using Mi.Entity.Base.Location;
using Mi.Entity.Base.Zone;
using Mi.MainDal.Common;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Zone
{
    public abstract class ZoneDalBase
    {
        #region Gets

        public ZoneEntity GetZoneById(int id)
        {
            const string commandText = "CMS_Zone_GetById";
            try
            {
                ZoneEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public IEnumerable<ZoneWithLanguageByType> GetZoneWithLanguageByType(int type, string lang_code)
        {
            const string commandText = "uspH_GetZoneWithLanguageByType";
            try
            {
                IEnumerable<ZoneWithLanguageByType> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "type", type);
                _db.AddParameter(cmd, "lang_code", lang_code);
                data = _db.GetList<ZoneWithLanguageByType>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public ZoneEntity CMS_Zone_LanguageGetById(int id,string languageCode)
        {
            const string commandText = "CMS_Zone_LanguageGetById";
            try
            {
                ZoneEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "LanguageCode", languageCode);
                data = _db.Get<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public ZoneLanguageEntity ZoneInLanguageGetById(int id)
        {
            const string commandText = "CMS_Zone_In_Language_GetById";
            try
            {
                ZoneLanguageEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<ZoneLanguageEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public ZoneEntity GetZoneByIdV2(int id)
        {
            const string commandText = "CMS_Zone_GetById_V2";
            try
            {
                ZoneEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public ZoneEntity GetZoneByAlias(string alias, string languageCode)
        {
            const string commandText = "CMS_Zone_GetByAlias";
            try
            {
                ZoneEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Alias", alias);
                _db.AddParameter(cmd, "LanguageCode", languageCode);
                data = _db.Get<ZoneEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        } 
        public ZoneEntity GetZoneByAlias(string alias, string languageCode,int type)
        {
            const string commandText = "CMS_Zone_GetByAliasType";
            try
            {
                ZoneEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Alias", alias);
                _db.AddParameter(cmd, "LanguageCode", languageCode);
                _db.AddParameter(cmd, "type", type);
                data = _db.Get<ZoneEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public ZoneEntity GetZoneByType(int type)
        {
            const string commandText = "CMS_Zone_GetByType";
            try
            {
                ZoneEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "type", type);
                data = _db.Get<ZoneEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<ZoneEntity> GetZoneActiveByParentId(int parentId)
        {
            const string commandText = "CMS_Zone_Active_GetByParentId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<ZoneLocationEntity> GetLocationByLanguage(string languageCode, bool isHome)
        {
            const string commandText = "GetLocationByLanguage";
            try
            {
                List<ZoneLocationEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "languageCode", languageCode);
                _db.AddParameter(cmd, "IsShowHome", isHome);
                data = _db.GetList<ZoneLocationEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        //public List<ZoneEntity> GetZoneByParentId(int parentId)
        //{

        //    const string commandText = "CMS_Zone_GetByParentId";
        //    try
        //    {
        //        List<ZoneEntity> data = null;
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "ParentId", parentId);
        //        data = _db.GetList<ZoneEntity>(cmd);

        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }


        //}
        public IEnumerable<ZoneEntity> ZoneSearch(string keyword, int status, int type)
        {
            const string commandText = "CMS_Zone_Search";
            try
            {
                IEnumerable<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "Status", status);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public IEnumerable<LocationEntity> GetAllLocation()
        {
            const string commandText = "uspH_Client_GetAllLocation";
            try
            {
                IEnumerable<LocationEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<LocationEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public IEnumerable<ZoneLanguageEntity> GetAllZoneLanguage(int zoneId)
        {
            const string commandText = "CMS_Zone_In_Language";
            try
            {
                IEnumerable<ZoneLanguageEntity> data = null;

                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneId);
                data = _db.GetList<ZoneLanguageEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public LocationEntity GetLocationById(int id)
        {
            const string commandText = "uspH_Client_GetLocationById";
            try
            {
                LocationEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                //var cmd = _db.CreateCommand(commandText, true);
                data = _db.Get<LocationEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public IEnumerable<ZoneEntity> SearchNewsWithPaging(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int filterFieldForUsername, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Zone_SearchV1";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "FilterFieldForUsername", filterFieldForUsername);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<ZoneEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public ZoneEntity GetZoneByAlias(string alias, byte type)
        {
            const string commandText = "CMS_Zone_GetByAliasAndType";
            try
            {
                ZoneEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Alias", alias);
                _db.AddParameter(cmd, "Type", type);
                data = _db.Get<ZoneEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public List<ZoneEntity> GetZoneByParentId(int parentId, int type)
        {

            const string commandText = "CMS_Zone_GetByParentId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneBasicEntity> GetZoneByParentId(int parentId, int type, string languageCode)
        {

            const string commandText = "EF_Zone_GetByParentId";
            try
            {
                List<ZoneBasicEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "languageCode ", languageCode);
                data = _db.GetList<ZoneBasicEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneAndProductCountEntity> GetZoneByParentIdAndCount(int parentId, int type)
        {

            const string commandText = "CMS_Zone_GetByParentIdAndCount";
            try
            {
                List<ZoneAndProductCountEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<ZoneAndProductCountEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneEntity> GetListZoneByAlias(string alias)
        {

            const string commandText = "FE_Zone_GetAllByParentAlias";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Alias", alias);
                //    _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneEntity> GetListPropertyByParentIdAndType(int parentId, int type)
        {

            const string commandText = "CMS_Zone_GetListPropertyByParentIdAndType";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneEntity> GetBreadCrumbByZoneId(int id)
        {

            const string commandText = "GetBreadCrumbByZoneId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CategoryId", id);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneWithSimpleFieldEntity> GetZoneActivedByParentId(int parentId)
        {

            const string commandText = "CMS_Zone_GetActivedByParentId";
            try
            {
                List<ZoneWithSimpleFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                data = _db.GetList<ZoneWithSimpleFieldEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<ZoneEntity> GetZoneByUserId(int userId)
        {

            const string commandText = "CMS_Zone_GetByUserId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userId);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneEntity> GetZoneByUsername(string username)
        {
            const string commandText = "CMS_Zone_GetByUsername";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneEntity> GetZoneByUserIdAndPermissionId(int userId, int permissionId)
        {
            const string commandText = "CMS_Zone_GetByUserIdAndPermissionId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userId);
                _db.AddParameter(cmd, "PermissionId", permissionId);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<ZoneEntity> GetZoneByUsernameAndPermissionId(string username, int permissionId)
        {
            const string commandText = "CMS_Zone_GetByUsernameAndPermissionId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "PermissionId", permissionId);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneEntity> GetZoneByUsernameAndPermissionIds(string username, string permissionIds)
        {
            const string commandText = "CMS_Zone_GetByUsernameAndPermissionIds";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "PermissionIds", permissionIds);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<ZoneWithSimpleFieldEntity> GetZoneActivedByUsernameAndPermissionIds(string username, string permissionIds)
        {
            const string commandText = "CMS_Zone_GetActivedByUsernameAndPermissionIds";
            try
            {
                List<ZoneWithSimpleFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "PermissionIds", permissionIds);
                data = _db.GetList<ZoneWithSimpleFieldEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneEntity> GetZoneByUserId(int userId, string keyword)
        {

            const string commandText = "CMS_Zone_GetByUserId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userId);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneEntity> GetZoneByNewsId(long newsId)
        {
            const string commandText = "CMS_Zone_GetByNewsId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneEntity> GetZoneByKeyword(string keyword)
        {
            const string commandText = "CMS_Zone_GetListByKeyword";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ZoneEntity GetPrimaryZoneByNewsId(long newsId)
        {
            const string commandText = "CMS_Zone_GetPrimaryZoneByNewsId";
            try
            {
                ZoneEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                data = _db.Get<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ZoneEntity> GetGroupNewsZoneByParentId(int id)
        {
            const string commandText = "CMS_Zone_GetGroupNewsZoneByParentId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RootZoneId", id);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<ZoneEntity> GetSiteMapByCategoryId(int id)
        {
            const string commandText = "GetSiteMapByCateoryId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CategoryId", id);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public IEnumerable<ZoneSimpleFieldEntity> GetDisplayHomePage(int type)
        {
            const string commandText = "FE_Zone_GetDisplayHomePage";
            try
            {
                IEnumerable<ZoneSimpleFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<ZoneSimpleFieldEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public IEnumerable<ZoneBasicEntity> GetAllByParentId(int parentId)
        {
            const string commandText = "FE_Zone_GetAllByParentId";
            try
            {
                IEnumerable<ZoneBasicEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                data = _db.GetList<ZoneBasicEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public IEnumerable<ZoneEntity> SearchHeader(string shortUrl, int pageNumber, int pageSize, ref int totalRow)
        {
            const string commandText = "uspSelectHeader";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "shortUrl", shortUrl);
                _db.AddParameter(cmd, "PageNumber", pageNumber);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRow, ParameterDirection.Output);
                data = _db.GetList<ZoneEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 3));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Sets

        public bool Insert(ZoneEntity zoneEntity)
        {

            const string commandText = "CMS_Zone_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", zoneEntity.Name);
                _db.AddParameter(cmd, "Description", zoneEntity.Description);
                _db.AddParameter(cmd, "ShortURL", zoneEntity.ShortUrl);
                _db.AddParameter(cmd, "SortOrder", zoneEntity.SortOrder);
                _db.AddParameter(cmd, "ParentId", zoneEntity.ParentId);
                _db.AddParameter(cmd, "Invisibled", zoneEntity.Invisibled);
                _db.AddParameter(cmd, "Status", zoneEntity.Status);
                _db.AddParameter(cmd, "AllowComment", zoneEntity.AllowComment);
                _db.AddParameter(cmd, "Avatar", zoneEntity.Avatar);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateLocation(LocationEntity locationEntity)
        {

            const string commandText = "uspH_Client_UpdateLocation";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", locationEntity.Id);
                _db.AddParameter(cmd, "Code", locationEntity.Code);
                _db.AddParameter(cmd, "Name", locationEntity.Name);
                _db.AddParameter(cmd, "Type", locationEntity.Type);
                _db.AddParameter(cmd, "Description", locationEntity.Description);
                _db.AddParameter(cmd, "ProvinceCode", locationEntity.ProvinceCode);
                _db.AddParameter(cmd, "Avatar_No1", locationEntity.Avatar_No1);
                _db.AddParameter(cmd, "Avatar_No2", locationEntity.Avatar_No2);
                _db.AddParameter(cmd, "Parent", locationEntity.Parent);
                _db.AddParameter(cmd, "isShowInHomePage", locationEntity.isShowInHomePage);
                _db.AddParameter(cmd, "sort", locationEntity.Sort);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(ZoneEntity zoneEntity)
        {
            const string commandText = "CMS_Zone_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", zoneEntity.Name);
                _db.AddParameter(cmd, "Description", zoneEntity.Description);
                _db.AddParameter(cmd, "ShortURL", zoneEntity.ShortUrl);
                _db.AddParameter(cmd, "SortOrder", zoneEntity.SortOrder);
                _db.AddParameter(cmd, "ParentId", zoneEntity.ParentId);
                _db.AddParameter(cmd, "Invisibled", zoneEntity.Invisibled);
                _db.AddParameter(cmd, "Status", zoneEntity.Status);
                _db.AddParameter(cmd, "AllowComment", zoneEntity.AllowComment);
                _db.AddParameter(cmd, "Avatar", zoneEntity.Avatar);
                _db.AddParameter(cmd, "Id", zoneEntity.Id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool Insert_V2(ZoneEntity zoneEntity, ref int id)
        {

            const string commandText = "CMS_Zone_Insert_V2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", zoneEntity.Name);
                _db.AddParameter(cmd, "SortOrder", zoneEntity.SortOrder);
                _db.AddParameter(cmd, "ParentId", zoneEntity.ParentId);
                _db.AddParameter(cmd, "Status", zoneEntity.Status);
                _db.AddParameter(cmd, "Avatar", zoneEntity.Avatar);
                _db.AddParameter(cmd, "Banner", zoneEntity.Banner);
                _db.AddParameter(cmd, "BannerLink", zoneEntity.BannerLink);
                _db.AddParameter(cmd, "Col", zoneEntity.Col);
                _db.AddParameter(cmd, "CreatedBy", zoneEntity.CreatedBy);
                _db.AddParameter(cmd, "CreatedDate", zoneEntity.CreatedDate);
                _db.AddParameter(cmd, "Type", zoneEntity.Type);
                _db.AddParameter(cmd, "ZoneIdList", zoneEntity.ZoneIdList);
                _db.AddParameter(cmd, "IsShowHomePage", zoneEntity.IsShowHomePage);
                _db.AddParameter(cmd, "Alias", zoneEntity.ShortUrl);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Insert_V3(ZoneEntity zoneEntity, ref int id)
        {

            const string commandText = "CMS_Zone_Insert_V3";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", zoneEntity.Name);
                _db.AddParameter(cmd, "Description", zoneEntity.Description);
                _db.AddParameter(cmd, "Content", zoneEntity.Content);
                _db.AddParameter(cmd, "ShortURL", zoneEntity.ShortUrl);
                _db.AddParameter(cmd, "SortOrder", zoneEntity.SortOrder);
                _db.AddParameter(cmd, "ParentId", zoneEntity.ParentId);
                _db.AddParameter(cmd, "Invisibled", zoneEntity.Invisibled);
                _db.AddParameter(cmd, "Status", zoneEntity.Status);
                _db.AddParameter(cmd, "AllowComment", zoneEntity.AllowComment);
                _db.AddParameter(cmd, "Avatar", zoneEntity.Avatar);
                _db.AddParameter(cmd, "Banner", zoneEntity.Banner);
                _db.AddParameter(cmd, "BannerLink", zoneEntity.BannerLink);
                _db.AddParameter(cmd, "MetaKeyword", zoneEntity.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", zoneEntity.MetaDescription);
                _db.AddParameter(cmd, "CreatedBy", zoneEntity.CreatedBy);
                _db.AddParameter(cmd, "CreatedDate", zoneEntity.CreatedDate);
                _db.AddParameter(cmd, "Type", zoneEntity.Type);
                _db.AddParameter(cmd, "MetaTitle", zoneEntity.MetaTitle);
                _db.AddParameter(cmd, "DistributionDate", zoneEntity.DistributionDate);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0 && id > 0;


            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Add_ZoneLanguage(ZoneLanguageEntity zoneEntity, ref int id)
        {
            const string commandText = "CMS_Zone_In_Language_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneEntity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneEntity.zoneId);
                _db.AddParameter(cmd, "LanguageCode", zoneEntity.LanguageCode);
                _db.AddParameter(cmd, "Name", zoneEntity.Name);
                _db.AddParameter(cmd, "Description", zoneEntity.Description);
                _db.AddParameter(cmd, "Content", zoneEntity.Content);
                _db.AddParameter(cmd, "Url", zoneEntity.Url);
                _db.AddParameter(cmd, "MetaTitle", zoneEntity.MetaTitle);
                _db.AddParameter(cmd, "MetaKeyword", zoneEntity.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", zoneEntity.MetaDescription);
                var data = cmd.ExecuteNonQuery() > 0;
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool Update_v2(ZoneEntity zoneEntity)
        {
            const string commandText = "CMS_Zone_Update_V2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneEntity.Id);
                _db.AddParameter(cmd, "Name", zoneEntity.Name);
                _db.AddParameter(cmd, "SortOrder", zoneEntity.SortOrder);
                _db.AddParameter(cmd, "ParentId", zoneEntity.ParentId);
                _db.AddParameter(cmd, "Status", zoneEntity.Status);
                _db.AddParameter(cmd, "Avatar", zoneEntity.Avatar);
                _db.AddParameter(cmd, "Banner", zoneEntity.Banner);
                _db.AddParameter(cmd, "BannerLink", zoneEntity.BannerLink);
                _db.AddParameter(cmd, "Col", zoneEntity.Col);
                _db.AddParameter(cmd, "ModifiedBy", zoneEntity.ModifiedBy);
                _db.AddParameter(cmd, "Type", zoneEntity.Type);
                _db.AddParameter(cmd, "ZoneIdList", zoneEntity.ZoneIdList);
                _db.AddParameter(cmd, "IsShowHomePage", zoneEntity.IsShowHomePage);
                _db.AddParameter(cmd, "Alias", zoneEntity.ShortUrl);
                return cmd.ExecuteNonQuery() > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool Update_ZoneLanguage(ZoneLanguageEntity zoneEntity)
        {
            const string commandText = "CMS_Zone_In_Language_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneEntity.Id);
                _db.AddParameter(cmd, "LanguageCode", zoneEntity.LanguageCode);
                _db.AddParameter(cmd, "Name", zoneEntity.Name);
                _db.AddParameter(cmd, "Description", zoneEntity.Description);
                _db.AddParameter(cmd, "Content", zoneEntity.Content);
                _db.AddParameter(cmd, "Url", zoneEntity.Url);
                _db.AddParameter(cmd, "MetaTitle", zoneEntity.MetaTitle);
                _db.AddParameter(cmd, "MetaKeyword", zoneEntity.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", zoneEntity.MetaDescription);
                return cmd.ExecuteNonQuery() > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool Update_v3(ZoneEntity zoneEntity)
        {
            const string commandText = "CMS_Zone_Update_V3";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", zoneEntity.Name);
                _db.AddParameter(cmd, "Description", zoneEntity.Description);
                _db.AddParameter(cmd, "Content", zoneEntity.Content);
                _db.AddParameter(cmd, "ShortURL", zoneEntity.ShortUrl);
                _db.AddParameter(cmd, "SortOrder", zoneEntity.SortOrder);
                _db.AddParameter(cmd, "ParentId", zoneEntity.ParentId);
                _db.AddParameter(cmd, "Invisibled", zoneEntity.Invisibled);
                _db.AddParameter(cmd, "Status", zoneEntity.Status);
                _db.AddParameter(cmd, "AllowComment", zoneEntity.AllowComment);
                _db.AddParameter(cmd, "Avatar", zoneEntity.Avatar);
                _db.AddParameter(cmd, "Banner", zoneEntity.Banner);
                _db.AddParameter(cmd, "BannerLink", zoneEntity.BannerLink);
                _db.AddParameter(cmd, "MetaKeyword", zoneEntity.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", zoneEntity.MetaDescription);
                _db.AddParameter(cmd, "ModifiedBy", zoneEntity.ModifiedBy);
                _db.AddParameter(cmd, "Type", zoneEntity.Type);
                _db.AddParameter(cmd, "MetaTitle", zoneEntity.MetaTitle);
                _db.AddParameter(cmd, "DistributionDate", zoneEntity.DistributionDate);
                _db.AddParameter(cmd, "Id", zoneEntity.Id);

                return cmd.ExecuteNonQuery() > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool MoveUp(int zoneId)
        {
            const string commandText = "CMS_Zone_MoveUp";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool MoveDown(int zoneId)
        {
            const string commandText = "CMS_Zone_MoveDown";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateInvisibled(int zoneId)
        {
            const string commandText = "CMS_Zone_UpdateInvisibled";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }

        public bool UpdateAllowComment(int zoneId)
        {
            const string commandText = "CMS_Zone_UpdateAllowComment";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool UpdateBasicSetting(ZoneBasicSettingEntity zoneEntity)
        {
            const string commandText = "CMS_Zone_UpdateBasic";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "SortOrder", zoneEntity.SortOrder);
                _db.AddParameter(cmd, "IsDisplayHomePage", zoneEntity.IsDisplayHomePage);
                _db.AddParameter(cmd, "PageSize", zoneEntity.PageSize);

                _db.AddParameter(cmd, "Id", zoneEntity.Id);
                var data = cmd.ExecuteNonQuery();
                return data > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool ChangeStatus(long id, string modifyBy, int status)
        {
            const string commandText = "CMS_Zone_ChangeStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "ModifiedBy", modifyBy);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ParentUpdate(int id, int parentId)
        {
            const string commandText = "CMS_Zone_ParentUpdate";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "ParentId", parentId);
                return cmd.ExecuteNonQuery() > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool RemoveLocation(int id)
        {
            const string commandText = "uspH_Client_RemoveLocation";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                return cmd.ExecuteNonQuery() > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool RemoveZoneLanguage(int id)
        {
            const string commandText = "CMS_Zone_In_Language_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                return cmd.ExecuteNonQuery() > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected ZoneDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
