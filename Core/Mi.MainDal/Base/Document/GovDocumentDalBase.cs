﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.Document;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Document
{
    public class GovDocumentDalBase
    {
        #region Update

        public bool Insert(GovDocumentEntity obj, ref int id)
        {
            const string commandText = "Documental_Insert_V2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "SoDi", obj.SoDi);
                _db.AddParameter(cmd, "SoKyHieu", obj.SoKyHieu);
                _db.AddParameter(cmd, "NguoiKy", obj.NguoiKy);
                _db.AddParameter(cmd, "NguoiSoan", obj.NguoiSoan);
                _db.AddParameter(cmd, "DviSoanThao", obj.DviSoanThao);
                _db.AddParameter(cmd, "MST", obj.CompanyId);
                _db.AddParameter(cmd, "LoaiVanBan", obj.LoaiVanBan);
                _db.AddParameter(cmd, "NoiNhanNgoaiNganh", obj.NoiNhanNgoaiNganh);
                _db.AddParameter(cmd, "NoiNhanTrongNganh", obj.NoiNhanTrongNganh);
                _db.AddParameter(cmd, "NoiNhanNoiBo", obj.NoiNhanNoiBo);
                _db.AddParameter(cmd, "FilePath", obj.FilePath);
                _db.AddParameter(cmd, "EDocLink", obj.EDocLink);
                _db.AddParameter(cmd, "NgayPhatHanh", obj.NgayPhatHanh);
                _db.AddParameter(cmd, "NgayDen", obj.NgayDen);
                _db.AddParameter(cmd, "CreatedAt", obj.CreatedBy);
              //  _db.AddParameter(cmd, "LastModifiedAt", obj.LastModifiedAt);
                _db.AddParameter(cmd, "Status", obj.Status);
                _db.AddParameter(cmd, "CreatedBy", obj.CreatedBy);
            //    _db.AddParameter(cmd, "LastModifiedBy", obj.LastModifiedBy);
                _db.AddParameter(cmd, "Notes", obj.Notes);
                _db.AddParameter(cmd, "ParentId", obj.ParentId);
                _db.AddParameter(cmd, "Type", obj.Type);
                _db.AddParameter(cmd, "SortOrder", obj.SortOrder);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(GovDocumentEntity obj)
        {
            const string commandText = "Documental_Update_V2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id);
                _db.AddParameter(cmd, "SoDi", obj.SoDi);
                _db.AddParameter(cmd, "SoKyHieu", obj.SoKyHieu);
                _db.AddParameter(cmd, "NguoiKy", obj.NguoiKy);
                _db.AddParameter(cmd, "NguoiSoan", obj.NguoiSoan);
                _db.AddParameter(cmd, "DviSoanThao", obj.DviSoanThao);
                _db.AddParameter(cmd, "MST", obj.CompanyId);
                _db.AddParameter(cmd, "LoaiVanBan", obj.LoaiVanBan);
                _db.AddParameter(cmd, "NoiNhanNgoaiNganh", obj.NoiNhanNgoaiNganh);
                _db.AddParameter(cmd, "NoiNhanTrongNganh", obj.NoiNhanTrongNganh);
                _db.AddParameter(cmd, "NoiNhanNoiBo", obj.NoiNhanNoiBo);
                _db.AddParameter(cmd, "FilePath", obj.FilePath);
                _db.AddParameter(cmd, "EDocLink", obj.EDocLink);
                _db.AddParameter(cmd, "NgayPhatHanh", obj.NgayPhatHanh);
                _db.AddParameter(cmd, "NgayDen", obj.NgayDen);
              //  _db.AddParameter(cmd, "CreatedAt", obj.CreatedBy);
                _db.AddParameter(cmd, "LastModifiedAt", obj.LastModifiedAt);
                _db.AddParameter(cmd, "Status", obj.Status);
                //_db.AddParameter(cmd, "CreatedBy", obj.CreatedBy);
                _db.AddParameter(cmd, "LastModifiedBy", obj.LastModifiedBy);
                _db.AddParameter(cmd, "Notes", obj.Notes);
                _db.AddParameter(cmd, "ParentId", obj.ParentId);
                _db.AddParameter(cmd, "Type", obj.Type);
                _db.AddParameter(cmd, "SortOrder", obj.SortOrder);

                var numberOfRow = cmd.ExecuteNonQuery();
                //   newFolderId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateStatus(int id,int status)
        {
            const string commandText = "Documental_ChangeStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        #endregion

        #region Get

        public GovDocumentEntity GetById(int id)
        {
            const string commandText = "Documental_GetById";
            try
            {
                GovDocumentEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<GovDocumentEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

  
        public IEnumerable<GovDocumentEntity> Search(DocumentFilterEntity obj, ref int totalRows)
        {
            const string commandText = "Documental_Search";
            try
            {
                IEnumerable<GovDocumentEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                  _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                  _db.AddParameter(cmd, "SoDi", obj.SoDi);
                  _db.AddParameter(cmd, "SoKyHieu", obj.SoKyHieu);
                  _db.AddParameter(cmd, "NguoiKy", obj.NguoiKy);
                  _db.AddParameter(cmd, "NguoiSoan", obj.NguoiSoan);
                  _db.AddParameter(cmd, "DviSoanThao", obj.DviSoanThao);
                  _db.AddParameter(cmd, "MST", obj.MST);
                  _db.AddParameter(cmd, "LoaiVanBan", obj.LoaiVanBan);
                  _db.AddParameter(cmd, "NoiNhanNgoaiNganh", obj.NoiNhanNgoaiNganh);
                  _db.AddParameter(cmd, "NoiNhanTrongNganh", obj.NoiNhanTrongNganh);
                  _db.AddParameter(cmd, "NoiNhanNoiBo", obj.NoiNhanNoiBo);
                  _db.AddParameter(cmd, "NgayPhatHanhFrom", obj.NgayPhatHanhFrom);
                  _db.AddParameter(cmd, "NgayPhatHanhTo", obj.NgayPhatHanhTo);
                  _db.AddParameter(cmd, "NgayDenFrom", obj.NgayDenFrom);
                  _db.AddParameter(cmd, "NgayDenTo", obj.NgayDenTo);
                  _db.AddParameter(cmd, "ParentId", obj.ParentId);
                  _db.AddParameter(cmd, "Notes", obj.Notes);
                  _db.AddParameter(cmd, "SortOrder", obj.SortOrder);
                  _db.AddParameter(cmd, "Status", obj.Status);
                  _db.AddParameter(cmd, "Type", obj.Type);
                  _db.AddParameter(cmd, "PageIndex", obj.PageIndex);
                  _db.AddParameter(cmd, "PageSize", obj.PageSize);

                data = _db.GetList<GovDocumentEntity>(cmd);

                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<GovDocumentBasicEntity> GetBasic(DocumentFilterBasicEntity obj)
        {
            const string commandText = "Documental_GetBasic";
            try
            {
                IEnumerable<GovDocumentBasicEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                 
                  _db.AddParameter(cmd, "ParentId", obj.ParentId);
                  _db.AddParameter(cmd, "SortOrder", obj.SortOrder);
                  _db.AddParameter(cmd, "Status", obj.Status);
                  _db.AddParameter(cmd, "Type", obj.Type);
                data = _db.GetList<GovDocumentBasicEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
      
        #region Core members

        private readonly CmsMainDb _db;

        protected GovDocumentDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
