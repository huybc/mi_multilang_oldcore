﻿using System;
using System.Collections.Generic;
using Mi.BoCached.Common;
using Mi.Common;
using Mi.Entity.Base.Point;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BoCached.CacheObjects
{
    public class PointCached : CacheObjectBase
    {
        public int GetPointByZone(string userName, int zoneId)
        {
            try
            {
                var cachedKey = string.Format("PointCached.GetById[{0}_{1}]", userName, zoneId);
                var data = Get<PointEntity>(userName, cachedKey);
                var p = 0;
                if (data == null)
                {
                    using (var db = new CmsMainDb())
                    {
                        p = db.PointMainDal.GetPointByZone(userName, zoneId);
                        Add(userName, cachedKey, new PointEntity
                        {
                            UserName = userName,
                            ZoneId = zoneId,
                            Point = p
                        });
                    }
                }
                else
                {
                    p = data.Point;
                }
                return p;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return 0;
            }
        }

        public List<PointEntity> GetTopUserByZone(int zoneId, int top, bool allowSelectExperience)
        {
            try
            {
                var cachedKey = string.Format("PointCached.GetTopUserByZone[{0}]", zoneId);
                var data = Get<List<PointEntity>>(zoneId + "", cachedKey);

                if (data == null || data.Count == 0)
                {
                    using (var db = new CmsMainDb())
                    {
                        data = db.PointMainDal.GetTopUserByZone(zoneId, top, allowSelectExperience);
                        Add(zoneId + "", cachedKey, data);
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<PointEntity>();
            }
        }

        public int GetTotalPoint(string userName)
        {
            try
            {
                var cachedKey = string.Format("PointCached.GetTotalPoint[{0}]", userName);
                var data = Get<PointEntity>(cachedKey);
                var p = 0;
                if (data == null)
                {
                    using (var db = new CmsMainDb())
                    {
                        p = db.PointMainDal.GetTotalPoint(userName);
                        Add(cachedKey, new PointEntity
                        {
                            UserName = userName,
                            Point = p
                        });
                    }
                }
                else
                {
                    p = data.Point;
                }
                return p;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return 0;
            }
        }

        public WcfActionResponse AddPoint(PointEntity point)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    db.PointMainDal.AddPoint(point);
                    //Xóa cache point cho user này
                    var cachedKey = string.Format("PointCached.GetById[{0}_{1}]", point.UserName, point.ZoneId);
                    Remove(cachedKey);
                    //xóa cache point top user
                    var cachedKey2 = string.Format("PointCached.GetTopUserByZone[{0}]", point.ZoneId);
                    Remove(cachedKey2);
                    //xóa cache point tổng điểm của user
                    var cachedKey3 = string.Format("PointCached.GetTotalPoint[{0}]", point.UserName);
                    Remove(cachedKey3);
                }
                
                return WcfActionResponse.CreateSuccessResponse();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }
    }
}
