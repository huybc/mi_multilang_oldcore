﻿using Mi.BoCached.Common;
using Mi.Entity.Base.Security;
using Mi.MainDal.Databases;

namespace Mi.BoCached.CacheObjects
{
    public class UserCached : CacheObjectBase
    {
        public UserEntity GetUserByUsername(string username)
        {
            var cachedKey = string.Format("GetUserByUsername[{0}]", username);
            var data = Get<UserEntity>(username, cachedKey);
            if (data == null)
            {
                using (var db = new CmsMainDb())
                {
                    data = db.UserMainDal.GetUserByUsername(username);
                    Add(username, cachedKey, data);
                }
            }
            return data;
        }

        public bool RemoveAllCachedByGroup(int userId)
        {
            RemoveAllCachedByGroup(userId.ToString());
            using (var db = new CmsMainDb())
            {
                var user = db.UserMainDal.GetUserById(userId);
                if (user != null)
                {
                    RemoveAllCachedByGroup(user.UserName);
                }
            }
            return true;
        }

        public override void RemoveAllCachedByGroup(string group, bool removeGroup = true)
        {
            using (var db = new CmsMainDb())
            {
                var user = db.UserMainDal.GetUserByUsername(group);
            if (user != null)
            {
                RemoveAllCachedByGroup(user.Id.ToString());
            }
            }
            base.RemoveAllCachedByGroup(group, removeGroup);
        }
    }
}
