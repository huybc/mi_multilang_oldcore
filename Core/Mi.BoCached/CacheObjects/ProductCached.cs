﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mi.BoCached.Common;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Product;
using Mi.Entity.Base.Zone;
using Mi.MainDal.Databases;

namespace Mi.BoCached.CacheObjects
{
  public  class ProductCached: CacheObjectBase
    {
       
        public List<NewsEmbedBoxOnPageListEntity> GetListProductEmbedBoxOnPage(dynamic zoneId, int type)
        {
            var cachedKey = string.Format("GetListProductEmbedBoxOnPage[{0}]", zoneId + type);
            var data = Get<List<ZoneEntity>>(zoneId + type.ToString(), cachedKey);
            if (data == null)
            {
                using (var db = new CmsMainDb())
                {
                    data = db.NewsEmbedBoxOnPageMainDal.GetListProductEmbedBoxOnPage(zoneId, type);
                    Add(zoneId.ToString(), cachedKey, data);
                }

            }
            return data;
        }
        public List<NewsEmbedBoxOnPageListEntity> EF_BoxProductEmbed_GetListByZone(dynamic zoneId, int type,int pageSize)
        {
            var cachedKey = string.Format("EF_BoxProductEmbed_GetListByZone[{0}]", zoneId + type);
            var data = Get<List<NewsEmbedBoxOnPageListEntity>>(zoneId + type.ToString(), cachedKey);
            if (data == null)
            {
                using (var db = new CmsMainDb())
                {
                    data = db.NewsEmbedBoxOnPageMainDal.EF_BoxProductEmbed_GetListByZone(zoneId, type, pageSize);
                    Add(zoneId.ToString(), cachedKey, data);
                }

            }
            return data;
        }

        //public List<ZoneEntity> GetBreadCrumbByCateoryId(int id)
        //{
        //    var cachedKey = string.Format("GetBreadCrumbByCateoryId[{0}]", id);
        //    var data = Get<List<ZoneEntity>>(id.ToString(), cachedKey);
        //    if (data == null)
        //    {
        //        using (var db = new CmsMainDb())
        //        {
        //            data = db.ZoneMainDal.GetBreadCrumbByZoneId(id);
        //            Add(id.ToString(), cachedKey, data);
        //        }

        //    }
        //    return data;
        //}

        public ProductEntity GetById(int id)
        {
            var cachedKey = string.Format("GetById[{0}]", id);
            var data = Get<ProductEntity>(id+"", cachedKey);
            if (data == null)
            {
                using (var db = new CmsMainDb())
                {
                    data = db.ProductMainDal.GetById(id);
                    Add(id+"", cachedKey, data);
                }
            }
            return data;
        }
    }
}

