﻿using Mi.BoCached.Common;
using Mi.Entity.Base.News;
using Mi.MainDal.Databases;

namespace Mi.BoCached.CacheObjects
{
  public  class ConfigCached: CacheObjectBase
    {
        public ConfigEntity GetByConfigName(string configName,string languageCode)
        {
            var cachedKey = string.Format("GetByConfigName[{0}]", configName+ languageCode);
            var data = Get<ConfigEntity>(configName+ languageCode, cachedKey);
            if (data == null)
            {
                using (var db = new CmsMainDb())
                {
                    data = db.ConfigMainDal.GetConfigNameInLanguage(configName, languageCode);
                    Add(configName+ languageCode, cachedKey, data);
                }
            }
            return data;
        }
    }
}
