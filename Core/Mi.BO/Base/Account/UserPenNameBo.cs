﻿using System.Collections.Generic;
using Mi.Entity.Base.Security;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Account
{
    public class UserPenNameBo
    {
        public static ErrorMapping.ErrorCodes EditPenName(UserPenNameEntity penName)
        {
            using (var db = new CmsMainDb())
            {
                return db.UserPenNameMainDal.EditPenName(penName) == true ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes DeletePenNameById(int id)
        {
            using (var db = new CmsMainDb())
            {
                return db.UserPenNameMainDal.DeletePenNameById(id) == true ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static List<UserPenNameEntity> SearchPenName(string email, string username, int pageIndex, int pageSize,
                                                            ref int totalRow)
        {
            using (var db = new CmsMainDb())
            {
                return db.UserPenNameMainDal.SearchPenName(email, username, pageIndex, pageSize, ref totalRow);
            }
        }
        public static UserPenNameEntity GetByVietId(long vietId)
        {
            using (var db = new CmsMainDb())
            {
                return db.UserPenNameMainDal.GetPenNameByVietId(vietId);
            }
        }
    }
}
