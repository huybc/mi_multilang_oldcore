﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.Company;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Base.Company;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Company
{
    public class CompanyBo
    {
        #region Update

        public static WcfActionResponse Insert(CompanyEntity obj, ref int id)
        {
            try
            {
                if (null == obj)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.InvalidRequest, ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]);
                }

                using (var db = new CmsMainDb())
                {
                    return db.CompanyDal.Insert(obj, ref id) ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]);
            }
        }
        public static WcfActionResponse Update(CompanyEntity obj)
        {
            try
            {
                if (null == obj)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.InvalidRequest, ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]);
                }

                using (var db = new CmsMainDb())
                {
                    return db.CompanyDal.Update(obj) ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]);
            }
        }

        #endregion
        #region Get
        public static CompanyEntity GetById(int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.CompanyDal.GetById(id);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static IEnumerable<CompanyEntity> Search(string keyword, int sortOrder, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.CompanyDal.Search(keyword, sortOrder, status, pageIndex, pageSize, ref totalRow);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<CompanyEntity>();
            }
        }

        #endregion
    }
}
