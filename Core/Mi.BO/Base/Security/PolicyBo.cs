﻿using System;
using RepairVN.BO.Base.Account;
using RepairVN.BoCached.CacheObjects;
using RepairVN.BoCached.Common;
using RepairVN.Common;
using RepairVN.Common.ChannelConfig;
using RepairVN.Entity.Base.Security;
using RepairVN.Entity.ErrorCode;

namespace RepairVN.BO.Base.Security
{
    public class PolicyBo
    {
        public static WcfActionResponse ValidAccount(string username, string password)
        {

            if (string.IsNullOrEmpty(username))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountInvalidUsername, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountInvalidUsername]);
            }

            if (string.IsNullOrEmpty(password))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountInvalidPassword, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountInvalidPassword]);
            }

            var user = UserBo.GetUserByUsername(username);
            if (null == user)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountInvalidUsername, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountInvalidUsername]);
            }
            if (user.Password != Crypton.Encrypt(password))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountInvalidPassword, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountInvalidPassword]);
            }

            if (user.Status != (int)UserStatus.Actived)
            {

                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountUserLocked, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountUserLocked]);

            }

            return UserBo.AddnewSmsCodeForUser(user.Id);
        }
        public static WcfActionResponse ValidAccountWithMd5EncryptPassword(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountInvalidUsername, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountInvalidUsername]);
            }

            if (string.IsNullOrEmpty(password))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountInvalidPassword, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountInvalidPassword]);
            }

            var user = UserBo.GetUserByUsername(username);
            if (null == user)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountInvalidUsername, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountInvalidUsername]);

            }
            if (Crypton.Md5Encrypt(Crypton.Decrypt(user.Password)).ToLower() != password.ToLower())
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountInvalidPassword, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountInvalidPassword]);

            }

            if (user.Status != (int)UserStatus.Actived)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountUserLocked, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountUserLocked]);

            }

            return UserBo.AddnewSmsCodeForUser(user.Id);

        }
        public static WcfActionResponse ValidSmsCode(string username, string smsCode)
        {
            if (string.IsNullOrEmpty(username))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountInvalidUsername, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountInvalidUsername]);
            }

            if (string.IsNullOrEmpty(smsCode))
            {

                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountInvalidSmsCode, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountInvalidSmsCode]);
            }

            var user = UserBo.GetUserByUsername(username);
            if (null == user)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountInvalidUsername, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountInvalidUsername]);
            }

            if (user.Status != (int)UserStatus.Actived)
            {

                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountUserLocked, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountUserLocked]);
            }

            var userSms = UserBo.GetUserSmsCode(user.Id);
            if (null == userSms || userSms.SmsCode != smsCode)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountInvalidSmsCode, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountInvalidSmsCode]);
            }
            return UserBo.RemoveInUsingState(user.Id);

        }
        public static WcfActionResponse ChangePassword(string username, string oldPassword, string newPassword, string accountNameLogin)
        {
            if (string.IsNullOrEmpty(username))
            {

                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
            }

            if (string.IsNullOrEmpty(oldPassword))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword]);
            }


            if (oldPassword != newPassword)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountOldAndNewPasswordNotMatch, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountOldAndNewPasswordNotMatch]);
            }

            var user = UserBo.GetUserByUsername(username);
            if (null == user)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername]);
            }

            return user.Status != (int)UserStatus.Actived ? WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserHasBeenLocked, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserHasBeenLocked]) : UserBo.ChangePassword(user.Id, oldPassword, newPassword, accountNameLogin);

        }
        public static WcfActionResponse ResetPassword(string encryptUserId, string newPassword)
        {
            var userId = CryptonForId.DecryptIdToInt(encryptUserId);
            if (userId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
            }

            if (string.IsNullOrEmpty(newPassword))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword]);
            }

            return UserBo.ResetPassword(userId, newPassword);
        }
        public static WcfActionResponse ChangeStatus(string username, UserStatus userStatus)
        {
            if (string.IsNullOrEmpty(username))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
            }

            if (userStatus == UserStatus.Unknow)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus]);
            }

            var user = UserBo.GetUserByUsername(username);
            var returnData = (null == user ? WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername]) : UserBo.ChangeStatus(user.Id, userStatus));

            if (returnData.Success)
            {
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(user.Id);
            }
            return returnData;
        }

    }
}
