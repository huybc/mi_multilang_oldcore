﻿using System;
using System.Collections.Generic;
using ICore.Common;
using ICore.DAL.Base.FileUpload;
using ICore.Entity.Base.Folder;
using ICore.Entity.ErrorCode;

namespace ICore.Bo.Base.FileUpload
{
    public class FolderBo
    {
        #region Update

        public static ErrorMapping.ErrorCodes InsertFolder(FolderEntity obj, ref int id)
        {
            try
            {
                if (null == obj)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return FolderDal.InsertFolder(obj, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateFolder(FolderEntity obj,ref int id)
        {
            try
            {
                if (null == obj)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return FolderDal.UpdateFolder(obj,ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes DeleteFolder(int id)
        {
            try
            {
                return FolderDal.DeleteFolder(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region Get

        public static FolderEntity GetFolderById(int id)
        {
            try
            {
                return FolderDal.GetFolderById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static IEnumerable<FolderEntity> Search()
        {
            try
            {
                return FolderDal.Search();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FolderEntity>();
            }
        }
        public static string FolderPathById(int id)
        {
            try
            {
                string str = string.Empty;
                var objs = FolderDal.FolderPathById(id);
                foreach (var obj in objs)
                {
                    if (!string.IsNullOrEmpty(str))
                    {
                        str += "/" + obj.Name;
                    }
                    else
                    {
                        str = obj.Name;

                    }
                }
                return str;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return "";
            }
        }

        #endregion
    }
}
