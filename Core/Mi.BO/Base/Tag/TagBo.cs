﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Mi.BO.Common;
using Mi.Common;
using Mi.Entity.Base.Tag;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Tag
{
    public class TagBo
    {
        public virtual List<TagEntity> SearchAllByKeyword(string keyword, int zoneId, bool isThread)
        {
            List<TagEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.SearchAllByKeyword(keyword, zoneId, isThread);
            }
            return returnValue;
        }
        public static List<TagEntity> SearchTag(string keyword, long parentTagId, int isThread, int zoneId, int type, EnumSearchTagOrder orderBy, int isHotTag, int pageIndex, int pageSize, ref int totalRow, bool getTagHasNewsOnly = false)
        {
            try
            {
                List<TagEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.TagMainDal.SearchTag(keyword, parentTagId, isThread, zoneId, type, (int)orderBy, isHotTag, pageIndex, pageSize, ref totalRow, getTagHasNewsOnly);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<TagEntity>();
        }

        public static List<TagWithSimpleFieldEntity> SearchTagForSuggestion(int top, int zoneId, string keyword, EnumTagType tagType, int isHot,
                                                                            EnumSearchTagOrder orderBy, bool getTagHasNewsOnly)
        {
            List<TagWithSimpleFieldEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.SearchTagForSuggestion(top, zoneId, keyword, (int)tagType, isHot, (int)orderBy, getTagHasNewsOnly);
            }
            return returnValue;
        }

        public static TagEntity GetTagByTagName(string name, bool getTagHasNewsOnly = false)
        {
            TagEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.GetTagByTagName(name, getTagHasNewsOnly);
            }
            return returnValue;
        }
        public static TagEntity GetByAlias(string name, bool getTagHasNewsOnly = false)
        {
            TagEntity returnValue=null;
            using (var db = new CmsMainDb())
            {
            //    returnValue = db.TagMainDal.GetByAlias(name, getTagHasNewsOnly);
            }
            return returnValue;
        }
        public static TagEntityDetail GetTagByTagId(long tagId)
        {
            TagEntityDetail tagDetail = null;
            TagEntity tag;
            using (var db = new CmsMainDb())
            {
                tag = db.TagMainDal.GetTagByTagId(tagId);
                if (null != tag)
                {
                    tagDetail = new TagEntityDetail()
                    {
                        Id = tag.Id,
                        ParentId = tag.ParentId,
                        Name = tag.Name,
                        Description = tag.Description,
                        Url = tag.Url,
                        Invisibled = tag.Invisibled,
                        IsHotTag = tag.IsHotTag,
                        Type = tag.Type,
                        CreatedDate = tag.CreatedDate,
                        ModifiedDate = tag.ModifiedDate,
                        CreatedBy = tag.CreatedBy,
                        EditedBy = tag.EditedBy,
                        UnsignName = tag.UnsignName,
                        IsThread = tag.IsThread,
                        Avatar = tag.Avatar,
                        Priority = tag.Priority,
                        TagContent = tag.TagContent,
                        TagTitle = tag.TagTitle,
                        TagInit = tag.TagInit,
                        TagMetaContent = tag.TagMetaContent,
                        TagMetaKeyword = tag.TagMetaKeyword,
                        TemplateId = tag.TemplateId,
                        NewsCount = tag.NewsCount
                    };
                    tagDetail.TagZone = db.TagZoneMainDal.GetTagNewsByTagId(tag.Id);
                }
            }

            return tagDetail;
        }
        public static List<TagEntity> GetTagByListOfTagId(string listOfTagId)
        {
            List<TagEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.GetTagByListOfId(listOfTagId);
            }
            return returnValue;
        }
        public static List<TagEntity> GetTagByParentTagId(long parentTagId)
        {
            List<TagEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.GetTagByParentTagId(parentTagId);
            }
            return returnValue;
        }
        public virtual WcfActionResponse InsertTag(TagEntity tag, int zoneId, string zoneIdList, ref long newTagId)
        {
            if (string.IsNullOrEmpty(tag.Name))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateTagInvalidTagName, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateTagInvalidTagName]);

            }
            if (tag.CreatedDate == DateTime.MinValue) tag.CreatedDate = DateTime.Now;
            using (var db = new CmsMainDb())
            {
                if (db.TagMainDal.Insert(tag, zoneId, zoneIdList, ref newTagId))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public static WcfActionResponse InsertTag(TagEntity tag, int zoneId, string zoneIdList)
        {
            WcfActionResponse responseData;
            long newTagId = 0;
            var errorCode = BoFactory.GetInstance<TagBo>().InsertTag(tag, zoneId, zoneIdList, ref newTagId);
            if (errorCode.Success)
            {
                tag.Id = newTagId;
                responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag), "1");
            }
            else
            {
                if (newTagId > 0)
                {
                    tag.Id = newTagId;
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag), "1");
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
            }
            return responseData;
        }
        public virtual WcfActionResponse InsertTag(TagEntity tag, ref long newTagId)
        {
            if (string.IsNullOrEmpty(tag.Name))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateTagInvalidTagName, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateTagInvalidTagName]);
            }
            if (tag.CreatedDate == DateTime.MinValue) tag.CreatedDate = DateTime.Now;
            using (var db = new CmsMainDb())
            {
                if (db.TagMainDal.Insert(tag, ref newTagId))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

            }
        }
        public virtual string UpdateTagList(string[] tagNames, int zoneId, string zoneIdList)
        {
            var tagIds = "";
            var tagId = 0L;
            using (var db = new CmsMainDb())
            {
                foreach (var tagName in tagNames)
                {
                    tagId = 0L;
                    if (!db.TagMainDal.Insert(new TagEntity
                    {
                        Name = tagName,
                        Description = tagName,
                        IsThread = false,
                        UnsignName = Utility.ConvertTextToUnsignName(tagName),
                        Url = Utility.UnicodeToKoDauAndGach(tagName),
                        CreatedDate = DateTime.Now
                    }, zoneId, zoneIdList, ref tagId)) continue;
                    if (tagId > 0)
                    {
                        tagIds += ";" + tagId;
                    }
                }
            }
            return !string.IsNullOrEmpty(tagIds) ? tagIds.Substring(1) : "";
        }
        public virtual WcfActionResponse Update(TagEntity tag, int zoneId, string zoneIdList)
        {
            if (string.IsNullOrEmpty(tag.Name))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateTagTagNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateTagTagNotFound]);
            }
            using (var db = new CmsMainDb())
            {
                var validTagEntity = db.TagMainDal.GetTagByTagName(tag.Name);
                if (null != validTagEntity && validTagEntity.Id != tag.Id && validTagEntity.IsThread == tag.IsThread)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateTagConflictTagName, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateTagConflictTagName]);
                }
                if (tag.CreatedDate == DateTime.MinValue) tag.CreatedDate = DateTime.Now;
                if (db.TagMainDal.Update(tag, zoneId, zoneIdList))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public virtual WcfActionResponse UpdateViewCountByUrl(int viewCount, string url)
        {
            using (var db = new CmsMainDb())
            {
                if (db.TagMainDal.UpdateViewCountByTag(viewCount, url))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public virtual WcfActionResponse UpdateListViewCountByUrl(string value)
        {
            using (var db = new CmsMainDb())
            {
                if (db.TagMainDal.UpdateListViewCountByTag(value))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public virtual WcfActionResponse DeleteById(long tagId)
        {
            using (var db = new CmsMainDb())
            {
                if (db.TagMainDal.DeleteById(tagId))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public virtual WcfActionResponse UpdatePriority(long tagId, long priority)
        {
            using (var db = new CmsMainDb())
            {
                if (db.TagMainDal.UpdatePriority(tagId, priority))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public virtual WcfActionResponse UpdateTagHot(long tagId, bool isHotTag)
        {
            using (var db = new CmsMainDb())
            {
                if (db.TagMainDal.UpdateTagHot(tagId, isHotTag))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public virtual WcfActionResponse AddNews(string newsIds, long tagId, int tagMode = 1)
        {
            using (var db = new CmsMainDb())
            {
                if (db.TagMainDal.AddNews(newsIds, tagId, tagMode))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public virtual WcfActionResponse UpdateNewsInTagNews(long tagId, string deleteNewsId, string addNewsId, int tagMode = 1)
        {
            using (var db = new CmsMainDb())
            {

                if (db.TagNewsMainDal.UpdateNews(tagId, deleteNewsId, addNewsId, tagMode))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public virtual List<TagWithSimpleFieldEntity> GetTagByListOfTagName(List<string> listTagName)
        {
            var listTag = listTagName.Aggregate("", (current, tagName) => current + ("^" + tagName));
            if (!string.IsNullOrEmpty(listTag)) listTag = listTag.Remove(0, 1);
            using (var db = new CmsMainDb())
            {
                return db.TagMainDal.GetTagByListOfTagName(listTag);
            }
        }

        public virtual List<TagWithSimpleFieldEntity> GetCleanTagByTagWordCount(int wordCount)
        {
            try
            {
                const int cachedExpired = 10; // In minute

                const string cachedKeyFormat = "CleanTagWithWordCount[{0}]";
                var cachedKey = string.Format(cachedKeyFormat, wordCount);

                var returnCleanTag = HttpContext.Current.Cache.Get(cachedKey) as List<TagWithSimpleFieldEntity>;
                if (returnCleanTag == null)
                {
                    using (var db = new CmsMainDb())
                    {
                        returnCleanTag = db.TagMainDal.GetAllCleanTag("", wordCount, wordCount);
                        // returnCleanTag = TagDal.GetAllCleanTag("", wordCount, wordCount);
                        HttpContext.Current.Cache.Add(cachedKey, returnCleanTag, null, DateTime.Now.AddMinutes(cachedExpired),
                                                      TimeSpan.Zero, CacheItemPriority.High, null);
                    }

                }
                return returnCleanTag;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, "GetCleanTagByTagWordCount(" + wordCount + ") error => " + ex);
                return new List<TagWithSimpleFieldEntity>();
            }
        }
        public virtual List<TagWithSimpleFieldEntity> GetCleanTagStartByKeyword(string startByKeyword, int minWordCount, int maxWordCount)
        {
            try
            {
                const int cachedExpired = 10; // In minute

                startByKeyword = startByKeyword.ToLower();

                const string cachedKeyFormat = "CleanTagStartByKeyword[{0}-{1}-{2}]";
                var cachedKey = string.Format(cachedKeyFormat, startByKeyword, minWordCount, maxWordCount);

                var returnCleanTag = HttpContext.Current.Cache.Get(cachedKey) as List<TagWithSimpleFieldEntity>;
                if (returnCleanTag == null)
                {
                    using (var db = new CmsMainDb())
                    {
                        returnCleanTag = db.TagMainDal.GetAllCleanTag(startByKeyword, minWordCount, maxWordCount);
                        HttpContext.Current.Cache.Add(cachedKey, returnCleanTag, null, DateTime.Now.AddMinutes(cachedExpired),
                                                      TimeSpan.Zero, CacheItemPriority.High, null);
                    }
                }
                return returnCleanTag;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal,
                                "GetCleanTagStartByKeyword(\"" + startByKeyword + "\", " + minWordCount + ", " + maxWordCount +
                                ") error => " + ex);
                return new List<TagWithSimpleFieldEntity>();
            }
        }
        /// <summary>
        /// Lấy dạm sách tag theo bài viết
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public virtual List<TagEntity> GetListTagByNewsId(long newsId)
        {
            List<TagEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.GetListTagByNewsId(newsId);
            }
            return returnValue;
        }
        public virtual List<TagWithSimpleFieldEntity> GetListTagAndNewsInfoByNewsId(long newsId, ref int newsStatus, ref string newsUrl, ref int zoneId)
        {
            var returnData = new List<TagWithSimpleFieldEntity>();
            using (var db = new CmsMainDb())
            {
                var news = db.NewsMainDal.GetNewsInfoForCachedById(newsId);
                if (news != null)
                {
                    newsStatus = news.Status;
                    newsUrl = news.Url;
                    zoneId = news.PrimaryZoneId;

                    var listTagNews = GetListTagByNewsId(newsId);
                    returnData = listTagNews.Select(tagNews => new TagWithSimpleFieldEntity
                    {
                        Id = tagNews.Id,
                        Name = tagNews.Name,
                        NewsCount = tagNews.NewsCount,
                        UnsignName = tagNews.UnsignName,
                        Url = tagNews.Url
                    }).ToList();
                }
            }
            return returnData;
        }
        public virtual WcfActionResponse DeleteTagNewsById(string tagName, long newsId)
        {
            using (var db = new CmsMainDb())
            {
                if (db.TagMainDal.DeleteTagNewsById(tagName, newsId))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }


        }
        public virtual WcfActionResponse UpdateTagNews(long newsId, string tagName)
        {
            using (var db = new CmsMainDb())
            {
                if (db.TagMainDal.UpdateTagNews(newsId, tagName))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }

        }

        public virtual WcfActionResponse RecieveFromCloud(TagEntity tag, int zoneId, string zoneIdList, ref long newTagId)
        {
            if (string.IsNullOrEmpty(tag.Name))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateTagInvalidTagName, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateTagInvalidTagName]);

            }
            if (tag.CreatedDate == DateTime.MinValue) tag.CreatedDate = DateTime.Now;
            //  return TagDal.RecieveFromCloud(tag, zoneId, zoneIdList, ref newTagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            using (var db = new CmsMainDb())
            {
                if (db.TagMainDal.RecieveFromCloud(tag, zoneId, zoneIdList, ref newTagId))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
    }
}
