﻿using Mi.Common;
using Mi.Entity.Base.ViewCount;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.ViewCount
{
    public class ViewCountBo
    {
        public virtual WcfActionResponse Update(ViewCountEntity obj)
        {
            using (var db = new CmsMainDb())
            {
                if (db.ViewCountMainDal.Update(obj))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }
        public virtual WcfActionResponse AddNews(ViewCountEntity obj, ref int id)
        {

            using (var db = new CmsMainDb())
            {
                if (db.ViewCountMainDal.Insert(obj, ref id))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }

            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }
        public virtual ViewCountEntity GetByObjectTypeAndObjectId(int objectType, long objectId)
        {
            ViewCountEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ViewCountMainDal.GetByObjectIdAndObjectType(objectId, objectType);
            }
            return returnValue;
        }
        public static ViewCountEntity GetByObjectIdAndObjectType(long objectId, int objectType)
        {
            ViewCountEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ViewCountMainDal.GetByObjectIdAndObjectType(objectId, objectType);
            }
            return returnValue;
        }
    }
}
