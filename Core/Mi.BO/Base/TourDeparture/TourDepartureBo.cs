﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.ProjectDetail
{
    public class TourDepartureBo
    {
        public static List<Entity.Base.Departure.DepartureEntity> GetByTourId(long id)
        {
            using (var db = new CmsMainDb())
            {
                return db.TourDepartureDal.GetByTourId(id);
            }
        }
          public static List<Entity.Base.Departure.DepartureEntity> GetByTourIdCurentTime(long id)
        {
            using (var db = new CmsMainDb())
            {
                return db.TourDepartureDal.GetByTourIdCurentTime(id);
            }
        }
        
        public static WcfActionResponse Create(Entity.Base.Departure.DepartureEntity obj)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.TourDepartureDal.Create(obj))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public static WcfActionResponse Delete( long id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.TourDepartureDal.Delete(id))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }

      
    }
}
