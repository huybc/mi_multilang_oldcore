﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.Document;
using Mi.Entity.Base.FileUpload;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Document
{
    public class GovDocumentBo
    {
        #region Update

        public static WcfActionResponse Insert(GovDocumentEntity obj, ref int id)
        {
            try
            {
                if (null == obj)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.InvalidRequest, ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]);

                }
                using (var db = new CmsMainDb())
                {
                    if (db.GovDocumentlDal.Insert(obj, ref id))
                    {
                    return WcfActionResponse.CreateSuccessResponse();

                    }
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]);
            }
        }
         public static WcfActionResponse Update(GovDocumentEntity obj)
        {
            try
            {
                if (null == obj)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.InvalidRequest, ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]);

                }
                using (var db = new CmsMainDb())
                {
                    if (db.GovDocumentlDal.Update(obj))
                    {
                    return WcfActionResponse.CreateSuccessResponse();

                    }
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]);
            }
        }
       
        public static WcfActionResponse UpdateStatus(int id, int status)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.GovDocumentlDal.UpdateStatus(id, status))
                    {
                        return WcfActionResponse.CreateSuccessResponse();

                    }
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]);
            }
        }
        #endregion
        #region Get
        public static GovDocumentEntity GetById(int id)
        {
            try
            {
                GovDocumentEntity returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.GovDocumentlDal.GetById(id);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static IEnumerable<GovDocumentEntity> Search(DocumentFilterEntity obj, ref int totalRow)
        {
            try
            {
                IEnumerable<GovDocumentEntity> returnValue;

                using (var db = new CmsMainDb())
                {
                    returnValue = db.GovDocumentlDal.Search(obj, ref totalRow);
                }


                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<GovDocumentEntity>();
            }
        }
        public static IEnumerable<GovDocumentBasicEntity> GetBasic(DocumentFilterBasicEntity obj)
        {
            try
            {
                IEnumerable<GovDocumentBasicEntity> returnValue;

                using (var db = new CmsMainDb())
                {
                    returnValue = db.GovDocumentlDal.GetBasic(obj);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<GovDocumentBasicEntity>();
            }
        }



        #endregion
    }
}
