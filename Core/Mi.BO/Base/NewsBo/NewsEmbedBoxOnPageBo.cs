﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.News
{
    public class NewsEmbedBoxOnPageBo
    {
        public static List<NewsEmbedBoxOnPageListEntity> GetListNewsEmbedBoxOnPage(long zoneId, int type)
        {
            var newsEmbedBox = new List<NewsEmbedBoxOnPageListEntity>();
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsEmbedBoxOnPageMainDal.GetListNewsEmbedBoxOnPage(zoneId, type);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsEmbedBox;

        }
        public static IEnumerable<NewsEmbedBoxOnPageListEntity> GetListProductEmbedBoxOnPage(dynamic zoneId, int type)
        {

            IEnumerable<NewsEmbedBoxOnPageListEntity> returnValue;
            try
            {
                using (var db = new CmsMainDb())
                {
                    returnValue = db.NewsEmbedBoxOnPageMainDal.GetListProductEmbedBoxOnPage(zoneId, type);
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                returnValue = new List<NewsEmbedBoxOnPageListEntity>();
            }
            return returnValue;

        }
        public static IEnumerable<NewsEmbedBoxEntity> GetListNewsEmbedByTypeAndZone(int zoneId, int type)
        {
            var newsEmbedBox = new List<NewsEmbedBoxEntity>();
            try
            {

                using (var db = new CmsMainDb())
                {
                    return db.NewsEmbedBoxOnPageMainDal.GetListNewsEmbedByTypeAndZone(zoneId, type);
                }
            }
            catch (Exception ex)
            {
                newsEmbedBox = new List<NewsEmbedBoxEntity>();
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsEmbedBox;

        }

        public static WcfActionResponse Insert(NewsEmbedBoxOnPageEntity newsEmbebBox)
        {
            try
            {
                if (null != newsEmbebBox)
                {
                    using (var db = new CmsMainDb())
                    {
                        return db.NewsEmbedBoxOnPageMainDal.Insert(newsEmbebBox) ? WcfActionResponse.CreateSuccessResponse()
                          : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                    }
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateZoneNotAllow, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateZoneNotAllow]);

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsEmbedBoxOnPageDal.Insert:{0}", ex.Message));
            }

        }

        public static WcfActionResponse Update(string listNewsId, int zoneId, int type)
        {
            try
            {
                if (null != listNewsId)
                {
                    using (var db = new CmsMainDb())
                    {
                        if (db.NewsEmbedBoxOnPageMainDal.Update(listNewsId, zoneId, type))
                        {
                            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Success,
                                ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);

                        }
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                    }
                }

                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateZoneNotAllow, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateZoneNotAllow]);
            }
            catch (Exception ex)
            {

                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }

        }

        public static WcfActionResponse Delete(long newsId, int zoneId, int type)
        {
            try
            {
                if (newsId > 0 && zoneId > 0)
                {
                    using (var db = new CmsMainDb())
                    {
                        if (db.NewsEmbedBoxOnPageMainDal.Delete(newsId, zoneId, type))
                        {
                            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Success,
                               ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                        }
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                    }
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateZoneNotAllow, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateZoneNotAllow]);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsEmbedBoxOnPageDal.Delete:{0}", ex.Message));
            }

        }
        public static WcfActionResponse DeleteAll(dynamic zoneId, int type)
        {
            try
            {

                using (var db = new CmsMainDb())
                {
                    if (db.NewsEmbedBoxOnPageMainDal.DeleteAll(zoneId, type))
                    {
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Success,
                            ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }


            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsEmbedBoxOnPageDal.DeleteAll:{0}", ex.Message));
            }

        }

    }
}
