﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.Media;
using Mi.Entity.Base.Zone;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Media
{
    public class MediaBo
    {
        public static MediaEntity GetById(int zoneId)
        {
            try
            {
                MediaEntity returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.MediaDal.GetById(zoneId);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new MediaEntity();
            }
        }
        public static WcfActionResponse Delete(int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.MediaDal.MediaDelete(id))
                    {
                        return WcfActionResponse.CreateSuccessResponse();

                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

            }
        }
        public static IEnumerable<MediaEntity> Search(string keyword, string username,
                                                       int sortOrder,
                                                       int status,
                                                       int type, int pageIndex, int pageSize,
                                                       ref int totalRow)
        {

            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.MediaDal.Search(keyword, username,
                                                      (int)sortOrder, status, type, pageIndex, pageSize,
                                                      ref totalRow);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<MediaEntity>();
            }
        }

        public static WcfActionResponse Insert(MediaEntity obj, ref int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {

                    if (db.MediaDal.MediaInsert(obj, ref id))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        public static WcfActionResponse Update(MediaEntity obj)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.MediaDal.MediaUpdate(obj))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

    }
}
