﻿using System;
using Mi.Common;
using Mi.Entity.Base.AuthenticateLog;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.AuthenticateLog
{
    public class AuthenticateLogBo
    {
        public static WcfActionResponse Insert(AuthenticateLogEntity authenticate, ref long newLogId)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.AuthenticateLogMainDal.Insert(authenticate, ref newLogId))
                    {
                        return WcfActionResponse.CreateSuccessResponse();

                    }

                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
    }
}
