﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.ContentLog;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Base.ContentLog;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.ContentLog
{
    public class ContentLogBo
    {

        public static ErrorMapping.ErrorCodes Insert(ContentLogEntity obj, ref int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ContentLogMainDal.Insert(obj, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ContentLogMainDal.Delete(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }



        #region Get

        public static ContentLogEntity GetById(int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ContentLogMainDal.GetById(id);
            }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static IEnumerable<ContentLogEntity> Search(long objId, byte objTyoe)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ContentLogMainDal.Search(objId, objTyoe);
            }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ContentLogEntity>();
            }
        }

        #endregion
    }
}
