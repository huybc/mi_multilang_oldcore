﻿using System.Collections.Generic;
using System.Configuration;
using Mi.Common;
using Mi.Common.ChannelConfig;
using Mi.WcfExtensions;

namespace Mi.BO.Common
{
    public class BoConstants
    {
        public const string NEWS_FORMAT_ID = "yyyyMMddHHmmssFFF";

        public const string VIDEO_FORMAT_ID = "yyyyMMddHHmmssFFF";

        //public const string NEWS_FORMAT_URL = "UrlFormatNews";
        //public const string NEWS_FORMAT_URL_PHOTO = "UrlFormatNewsPhoto";
        //public const string NEWS_FORMAT_URL_VIDEO = "UrlFormatNewsVideo";
        //public const string NEWS_FORMAT_URL_SLIDE_PHOTO = "UrlFormatNewsSlidePhoto";
        //public const string NEWS_FORMAT_TAG = "UrlFormatTag";
        //public const string NEWS_FORMAT_NEWSRELATION = "UrlFormatNewsRelation";
        ////public const string NEWS_FORMAT_SUBTITLE = "UrlFormatSubtitle";
        //public const string NEWS_FORMAT_AUTHOR = "UrlFormatAuthor";



        //public const string VIDEO_FORMAT_URL = "UrlFormatNews";
        //public const string VIDEO_FORMAT_TAG = "UrlFormatTag";
        //public const string VIDEO_FORMAT_NEWSRELATION = "UrlFormatNewsRelation";
        //public const string VIDEO_FORMAT_SUBTITLE = "UrlFormatSubtitle";
        //public const string VIDEO_FORMAT_AUTHOR = "UrlFormatAuthor";
        //public const string VIDEO_FORMAT_SHARELINK = "ShareLinkFormat";

        #region Video

        public static string VideoShareLink
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "ShareLinkFormat"); }
        }
        public static string VideoUrlFormatAuthor
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "UrlFormatAuthor"); }
        }
        public static string VideoUrlFormatSubtitle
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "UrlFormatSubtitle"); }
        }
        public static string VideoUrlFormatNewsRelation
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "UrlFormatNewsRelation"); }
        }
        public static string VideoUrlFormatTag
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "UrlFormatTag"); }
        }
        public static string VideoUrlFormatNews
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "UrlFormatNews"); }
        }
        public static string VideoUrlFormat
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "UrlFormatVideo"); }
        }
        public static bool IsBuildLinkVideo
        {
            get { return Utility.ConvertToBoolean(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "BuildLinkVideo")); }
        }

        #endregion

        #region Products

        public static string ProductUrlFormat
        {
            get { return CmsChannelConfiguration.GetAppSetting("UrlFormatProduct"); }
        }

        #endregion
        #region News

        public static string NewsUrlFormat
        {
            get { return CmsChannelConfiguration.GetAppSetting("UrlFormatNews"); }
        }
        public static string NewsUrlFormatForPhotoNews
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "UrlFormatNewsPhoto"); }
        }
        public static string NewsUrlFormatForVideoNews
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "UrlFormatNewsVideo"); }
        }
        public static string NewsUrlFormatForFunnyNews
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "UrlFormatFunnyNews"); }
        }
        public static string NewsUrlFormatForSlidePhotoNews
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "UrlFormatNewsSlidePhoto"); }
        }
        public static string NewsUrlFormatForTag
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "UrlFormatTag"); }
        }
        public static string NewsUrlFormatForAdStore
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "UrlFormatAdStore"); }
        }
        public static string NewsUrlFormatForAdStoreByNamespace(string channelNamespace)
        {
            return ServiceChannelConfiguration.GetAppSetting(channelNamespace, "UrlFormatAdStore");
        }
        public static string NewsUrlFormatForNewsRelation
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "UrlFormatNewsRelation"); }
        }
        public static string NewsUrlFormatForAuthor
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "UrlFormatAuthor"); }
        }

        public static string TagFormatJson
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "TagFormatJson"); }
        }

        public static string RelationFormatJson
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RelationFormatJson"); }
        }

        #endregion
    }
}
