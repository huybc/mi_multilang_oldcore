﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Gold_luck.CMS.Modules.Location.Template.List" %>

<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Location" %>
<%@ Import Namespace="Mi.Common.ChannelConfig" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Common" %>

<%
    
    var pageIndex = GetQueryString.GetPost("PageIndex", 1);
    var filter = GetQueryString.GetPost("filter", string.Empty);
    var pageSize = 10;
    

    //var listLocation = ZoneBo.GetAllLocation().OrderBy(r => r.Id).Skip((pageIndex - 1) * pageSize).Take(pageSize);
    var listLocation = ZoneBo.GetAllLocation();
    if (!string.IsNullOrEmpty(filter))
        listLocation = listLocation.Where(r => r.Name.Contains(filter));
    var totalRow = listLocation.Count();
    var pageNumber = Math.Ceiling((decimal)(totalRow / pageSize));
    listLocation = listLocation.OrderByDescending(r => r.Sort).Skip((pageIndex - 1) * pageSize).Take(pageSize);
    %>
<table class="table" id="locationTable">
    <thead>
        <tr>
            <th>STT</th>
            <th>Địa danh</th>
            <th>Mô tả</th>
            <th>Avatar</th>
            <th>Hiện trang chủ</th>
            <th>Thứ tự</th>
            <th>Type</th>
            <th>Chức năng</th>
        </tr>
    </thead>
    <tbody>
        <%  var count = 1;
            foreach (var item in listLocation)
            {
                var parentName = "";
                if (item.Parent > 0)
                    parentName = ZoneBo.GetLocationById((int)item.Parent).Name;
                %> 
        
            <tr>
                <td><%=count + (pageIndex - 1)*pageSize %></td>
                <td><%=item.Name  %> - <span style="font-size:1em; color:aqua"><%= parentName %></span></td>
                <td><%=item.Description %></td>
                <td><img src="/uploads/<%=item.Avatar_No1 %>" style="width:100px", max-height="50px" /></td>
                <td><%=item.isShowInHomePage == 0 ? "Không hiển thị" : "Hiển thị trang chủ" %></td>
                <td><%=item.Sort == -1 ?"Không đánh số" : item.Sort.ToString() %></td>
                <td><%=item.Type%></td>
                <td>
                    <a href="javascript:void(0)" class="linkEdit" data-location="<%=item.Id %>">Sửa</a>
                    <br />
                    <a href="javascript:void(0)" class="linkDelete" data-location="<%=item.Id %>">Xóa</a>
                </td>
            </tr>
        <%  count++;
            } %>
        <tr>
        </tr>
    </tbody>

</table>
    <nav aria-label="Page navigation example">
  <ul class="pagination">
      <%for (int i = 0; i < pageNumber; i++)
          { %>
            <li class="page-item <%=i+1==pageIndex ? "active" : "" %>"><a class="page-link" href="javascript:void(0)" data-page="<%=i+1 %>"><%=i+1 %></a></li>    
      
      <%} %>
  </ul>
</nav>