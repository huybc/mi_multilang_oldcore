﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mi.BO.Base.News;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.Zone;
using Mi.Entity.ErrorCode;
using Mi.BO.Base.ProjectDetail;
using Mi.Entity.Base.ProjectDetail;
using Mi.Action;
using Mi.Action.Core;
using Newtonsoft.Json;
using Gold_luck.Core.Helper;

namespace Gold_luck.CMS.Modules.Zone.Action
{
    public class ZoneActions : ActionBase
    {
        protected override string ResponseContentType
        {
            get { return "text/plain; charset=utf-8"; }
        }
        protected override bool IsResponseDataDirectly
        {
            get { return false; }
        }

        protected override object ProcessAction(string functionName, HttpContext context)
        {
            var responseData = new ResponseData();
            if (!PolicyProviderManager.Provider.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {
                switch (functionName)
                {
                    case "":
                        responseData.Success = false;
                        responseData.Message = Constants.MESG_CAN_NOT_FOUND_ACTION;
                        break;
                    case "search":
                        responseData = GetListZone();
                        break;
                    case "xsearch":
                        responseData = XGetListZone();
                        break;
                    case "main":
                        responseData = InitMain();
                        break;
                    case "xmain":
                        responseData = XInitMain();
                        break;

                    case "news_init_template":
                        responseData = NewsInitTemplate();
                        break;

                    case "update_status":

                        responseData = ChangeStatus();
                        break;

                    case "xedit":
                        responseData = XEdit();
                        break;
                    case "edit":
                        responseData = Edit();
                        break;
                    case "save":
                        responseData = Save();
                        break;
                    case "save-language":
                        responseData = SaveZoneLanguage();
                        break;
                    case "zone-language-remove":
                        responseData = ZoneLanguageRemove();
                        break;
                    case "zone-language-get-id":
                        responseData = ZoneLanguageGetById();
                        break;
                    case "move":
                        responseData = ParentUpdate();
                        break;
                }
            }

            return responseData;
        }

        private ResponseData NewsInitTemplate()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\News\\Templates\\Edit.aspx");
            responseData.Success = true;
            return responseData;
        }

        private ResponseData InitMain()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Zone\\Main.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData XInitMain()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Zone\\XMain.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData Edit()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Zone\\Template\\Edit.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData XEdit()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Zone\\Template\\XData\\Edit.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData GetListZone()
        {
            ResponseData responseData;
            var rs = new List<ZoneResult>();
            var key = GetQueryString.GetPost("keyword", string.Empty);
            var status = GetQueryString.GetPost("status", -1);
            var type = GetQueryString.GetPost("type", 0);
            var objs = ZoneBo.ZoneSearch(key, status, type);
            foreach (var obj in objs)
            {
                rs.Add(new ZoneResult
                {
                    id = obj.Id + "",
                    parent = obj.ParentId > 0 ? obj.ParentId + "" : "#",
                    text = obj.Name.Replace("+ ", "")
                });
            }
            responseData = ConvertResponseData.CreateResponseData(rs, rs.Count(), "");




            return responseData;
        }
        private ResponseData XGetListZone()
        {
            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Zone\\Template\\XData\\List.aspx");
            responseData.Success = true;
            return responseData;
        }


        private ResponseData Save()
        {
            var responseData = new ResponseData();

            if (!Policy.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {
                var id = GetQueryString.GetPost("id", 0);
                var name = GetQueryString.GetPost("name", string.Empty);
                var parentId = GetQueryString.GetPost("parent_id", 0);
                var status = GetQueryString.GetPost("status", 1);
                var isDisplayHome = GetQueryString.GetPost("isDisplayHome", false);
                var sortOrder = GetQueryString.GetPost("sort", 0);
                var type = GetQueryString.GetPost("type", 0);
                var avatar = GetQueryString.GetPost("avatar", string.Empty);
                var icon = GetQueryString.GetPost("icon", string.Empty);
                var col = GetQueryString.GetPost("col", string.Empty);
                var userName = Policy.GetAccountName();
                if (id == parentId && parentId > 0)
                {
                    responseData.Success = false;
                    responseData.Message = "Chuyên mục cha không hợp lệ !";
                    return responseData;
                }
                if (!string.IsNullOrEmpty(name))
                {
                    var shortUrl = Utility.UnicodeToKoDauAndGach(name);
                    var zone = new ZoneEntity
                    {
                        Name = name,
                        ParentId = parentId,
                        CreatedDate = DateTime.Now,
                        Status = status,
                        SortOrder = sortOrder,
                        ModifiedDate = DateTime.Now,
                        Banner = icon,
                        Avatar = avatar,
                        Col = col,
                        Type = (byte)type,
                        ZoneIdList = "",
                        IsShowHomePage = isDisplayHome,
                        ShortUrl = shortUrl
                    };
                    if (id > 0)
                    {
                        zone.Id = id;
                        responseData = ConvertResponseData.CreateResponseData(ZoneBo.Update(zone, userName));
                        responseData.Data = zone;
                    }
                    else
                    {
                        int idoutput = 0;
                        responseData = ConvertResponseData.CreateResponseData(ZoneBo.InsertV2(zone, userName, ref idoutput));
                        zone.Id = idoutput;

                        if (idoutput > 0)
                        {
                            responseData.Data = zone;
                        }
                    }
                }
                else
                {
                    responseData.Success = false;
                    responseData.Message = "Invalid zone's name";
                }
            }
            return responseData;
        }
        private ResponseData SaveZoneLanguage()
        {
            var responseData = new ResponseData();

            if (!Policy.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {

                var id = GetQueryString.GetPost("id", 0);
                var name = GetQueryString.GetPost("name", string.Empty);
                var desc = GetQueryString.GetPost("sapo", string.Empty);
                var content = GetQueryString.GetPost("content", string.Empty);
                var zoneId = GetQueryString.GetPost("zone_id", 0);
                var type = GetQueryString.GetPost("type", 0);
                var metakey = GetQueryString.GetPost("metakey", string.Empty);
                var metades = GetQueryString.GetPost("metades", string.Empty);
                var metatitle = GetQueryString.GetPost("metatitle", string.Empty);
                var languageCode = GetQueryString.GetPost("language_code", string.Empty).Trim();


                if (!string.IsNullOrEmpty(name))
                {
                    var shortUrl = "";
                    if (languageCode.Equals("ko-KR") || languageCode.Equals("zh_CN"))
                    {
                        if (type == (int)ZoneType.Location)
                        {
                            shortUrl = "location-" + zoneId;
                        }
                        else
                        {
                            shortUrl = "category-" + zoneId;
                        }
                    }
                    else
                    {
                        shortUrl = Utility.UnicodeToKoDauAndGach(name);
                    }

                    var zone = new ZoneLanguageEntity
                    {
                        Name = name,
                        zoneId = zoneId,
                        Content = content,
                        Description = desc,
                        MetaDescription = metades,
                        MetaKeyword = metakey,
                        MetaTitle = metatitle,
                        LanguageCode = languageCode,
                        Url = shortUrl

                    };
                    if (id > 0)
                    {
                        zone.Id = id;
                        responseData = ConvertResponseData.CreateResponseData(ZoneBo.UpdateZoneLanguage(zone));
                        responseData.Data = zone;
                    }
                    else
                    {
                        int idoutput = 0;
                        responseData = ConvertResponseData.CreateResponseData(ZoneBo.AddZoneLanguage(zone, ref idoutput));
                        zone.Id = idoutput;

                        if (idoutput > 0)
                        {
                            responseData.Data = zone;
                        }
                    }
                }
                else
                {
                    responseData.Success = false;
                    responseData.Message = "Invalid zone's name";
                }
            }
            return responseData;
        }

        public ResponseData ParentUpdate()
        {
            var id = GetQueryString.GetPost("id", 0);
            var parent_id = GetQueryString.GetPost("parent_id", 0);

            var data = ZoneBo.ParentUpdate(id, parent_id);
            var responseData = ConvertResponseData.CreateResponseData(data);
            responseData.Success = true;
            return responseData;
        }
        public ResponseData ZoneLanguageRemove()
        {
            var id = GetQueryString.GetPost("id", 0);


            var data = ZoneBo.RemoveZoneLanguage(id);
            var responseData = ConvertResponseData.CreateResponseData(data);
            responseData.Success = true;
            return responseData;
        }
        public ResponseData ZoneLanguageGetById()
        {
            var responseData = new ResponseData();
            var id = GetQueryString.GetPost("id", 0);
            var data = ZoneBo.ZoneInLanguageGetById(id);
            responseData.Data = data;
            responseData.Success = true;
            return responseData;
        }
        public ResponseData Reject()
        {
            int id = 0;
            var listNewsId = GetQueryString.GetPost("id", 0L);
            var content = GetQueryString.GetPost("content", string.Empty);
            var username = Policy.GetAccountName();

            var data = NewsBo.ChangeStatusToReject(listNewsId, username);
            var responseData = ConvertResponseData.CreateResponseData(data);

            if (responseData.Success)
            {
                //responseData.Data = CryptonForId.EncryptId(listNewsId);
                //ContentLogMapping.Insert(new ContentLogEntity
                //{
                //    ObjbectId = listNewsId,
                //    Content = content,
                //    CreatedBy = Policy.GetAccountName(),
                //    CreatedDate = DateTime.Now,
                //    ObjectType = (byte)EnumContentLogType.News

                //}, ref id);
            }
            return responseData;
        }
        //public ResponseData Reject_Log()
        //{
        //    var listNewsId = GetQueryString.GetPost("id", 0L);
        //    var data = ContentLogMapping.Search(listNewsId, (byte)EnumContentLogType.News);
        //    var list = new List<ContentLogEntity>();
        //    foreach (var i in data)
        //    {
        //        list.Add(new ContentLogEntity
        //        {
        //            ObjbectId = i.ObjbectId,
        //            Content = i.Content,
        //            CreatedBy = i.CreatedBy,
        //            Date = UIHelper.GetLongDate(i.CreatedDate),
        //            ObjectType = i.ObjectType
        //        });
        //    }


        //    var responseData = ConvertResponseData.CreateResponseData(list, list.Count(), "");
        //    return responseData;
        //}


        public ResponseData ChangeStatus()
        {
            var status = GetQueryString.GetPost("status", 0);
            var username = Policy.GetAccountName();

            var listNewsId = GetQueryString.GetPost("id", "");
            ResponseData responseData = null;
            var listNewsIdUpdated = "";

            var newsIds = listNewsId.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
            {
                responseData = ConvertResponseData.CreateResponseData(ZoneBo.ChangeStatus((int)id, username, status));
                if (responseData.Success)
                {
                    listNewsIdUpdated += ";" + id;

                }
                var objsNews = ZoneBo.GetZoneById(id.ToInt32Return0());
                if (objsNews != null)
                {
                    //if ((int)ZoneStatus.Publish == status)
                    //{
                    //    XMLDAL.UpdateNode(objsNews.ShortUrl, id.ToInt32Return0());

                    //}

                }
            }

            if (!string.IsNullOrEmpty(listNewsIdUpdated))
            {
                responseData = ResponseData.CreateSuccessResponseData(listNewsIdUpdated.Remove(0, 1), 1, "");
            }
            return responseData;
        }
        public class ZoneResult
        {
            public string id { get; set; }
            public string parent { get; set; }
            public string text { get; set; }
            public string path { get; set; }
        }
        public class Content
        {

            public string title { get; set; }
            public string image { get; set; }
            public string content { get; set; }
        }
    }
}