﻿using Mi.BO.Base.News;
using Mi.BO.Base.Zone;
using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Zone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mi.Action.Core;

namespace Gold_luck.CMS.Pages
{
    public partial class NewsV2Edit : System.Web.UI.Page
    {
        public NewsDetailForEditEntity _obj = new NewsDetailForEditEntity();
        protected void Page_Load(object sender, EventArgs e)
        {
            var id = int.Parse(Page.RouteData.Values["id"].ToString());
            var r = Page.RouteData.Values["id"];
            _obj = NewsBo.GetDetail(id, PolicyProviderManager.Provider.GetAccountName());
            if (id <= 0)
            {
                _obj = new NewsDetailForEditEntity
                {
                    NewsInfo = new NewsEntity()
                };
            }
            if (!Page.IsPostBack)
            {
                var zones = ZoneBo.GetAllZoneWithTreeView(false, (int)ZoneType.All).ToList().Where(it => it.Status == 1);
                ZoneRpt.DataSource = zones;
                ZoneRpt.DataBind();
            }
        }
        public List<ZoneWithSimpleField> GetAllZoneWithTreeViewSimpleFields()
        {
            List<ZoneWithSimpleField> simplaFields = new List<ZoneWithSimpleField>();

            var zones = CacheObjectBase.GetInstance<ZoneCached>().GetAllZone((int)ZoneType.All).Where(it => it.Status == 1);
            foreach (var zone in zones)
            {
                simplaFields.Add(new ZoneWithSimpleField
                {
                    Id = zone.Id,
                    Name = zone.Name.Trim(),
                    ShortURL = zone.ShortUrl,
                    //   RealName = zone.Name.Trim(),
                    ParentId = zone.ParentId
                });
            }
            return simplaFields;
        }
        public class ZoneWithSimpleField
        {
            public string Name { get; set; }
            public string ShortURL { get; set; }
            // public string RealName { get; set; }
            public int Id { get; set; }
            //  public int ZoneId { get { return this.Id; } }
            public int ParentId { get; set; }
        }
        protected string GetTags(int type)
        {
            List<string> tags = new List<string>();
            if (_obj.TagInNews != null)
            {
                foreach (var tag in _obj.TagInNews)
                {
                    //TagNameA|1|TagUrl_A
                    if (tag.TagMode == type)
                    {
                        tags.Add(string.Format("{0}|{1}|{2}", tag.Name, tag.TagId, tag.Url));
                    }
                }
            }
            return String.Join(",", tags);
        }
    }
}