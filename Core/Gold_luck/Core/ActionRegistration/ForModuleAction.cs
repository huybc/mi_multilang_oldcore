﻿using Gold_luck.CMS.Modules.Authenticate.Actions;
using Gold_luck.CMS.Modules.Config.Action;
using Gold_luck.CMS.Modules.Customer.Action;
using Gold_luck.CMS.Modules.FileManager.Action;
using Gold_luck.CMS.Modules.News.Action;
using Gold_luck.CMS.Modules.Service.Action;
using Gold_luck.CMS.Modules.Slider.Action;
using Gold_luck.CMS.Modules.Tag.Action;
using Gold_luck.Core.RequestBase;
using Gold_luck.CMS.Modules.Zone.Action;
using Gold_luck.Pages.Action;
using Gold_luck.CMS.Modules.Tour.Action;
using Gold_luck.CMS.Modules.Location.Action;
//using Gold_luck.Pages.Action;

namespace Gold_luck.Core.ActionRegistration
{
    public class ForModuleAction : CmsAsyncRequestBase
    {
        static ForModuleAction()
        {
            var className = typeof(ForModuleAction).Name;
            RegisterAction(className, "tour", typeof(TourActions), true);
            RegisterAction(className, "location", typeof(LocationActions), true);
            RegisterAction(className, "fmanager", typeof(FileManagerActions), true);
            RegisterAction(className, "ui-action", typeof(UIActions), true);
            RegisterAction(className, "service", typeof(ServiceActions), true);
            RegisterAction(className, "authenticate", typeof(AuthenticateAction), true);
            RegisterAction(className, "customer", typeof(CustomerActions), true);
            RegisterAction(className, "zone", typeof(ZoneActions), true);
            //RegisterAction(className, "charge", typeof(ChargeActions), true);
            RegisterAction(className, "config", typeof(ConfigActions), true);
            RegisterAction(className, "adv", typeof(AdvActions), true);
            RegisterAction(className, "news", typeof(NewsActions), true);
            //   RegisterAction(className, "project", typeof(ProjectActions), true);
            //RegisterAction(className, "comment", typeof(CommentActions), true);
            //RegisterAction(className, "answer", typeof(AnswerActions), true);
            //RegisterAction(className, "profile", typeof(UserActions), true);
            //RegisterAction(className, "user", typeof(UserActions), true);
            RegisterAction(className, "tag", typeof(TagActions), true);
            // RegisterAction(className, "hotel", typeof(HotelActions), true);
            //RegisterAction(className, "livecms", typeof(LiveCmsActions), true);
            //RegisterAction(className, "media", typeof(MediaActions), true);
            //RegisterAction(className, "photo", typeof(PhotoActions), true);
            //RegisterAction(className, "hanwha", typeof(HanwhaActions), true);
            //RegisterAction(className, "shop", typeof(ShopActions), true);
            //RegisterAction(className, "experience", typeof(ExperienceActions), true);
            //   RegisterAction(className, "contact", typeof(ContactActions), true);

        }
    }
}