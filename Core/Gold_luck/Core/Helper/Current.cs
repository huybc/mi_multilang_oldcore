﻿using System;
using System.Web;
using System.Web.UI;

namespace Gold_luck.Core.Helper
{
    public class Current : Page
    {
        public static string Language
        {
            get
            {
                string language = "";
                try
                {
                    string str = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
                    switch (str)
                    {
                        case "vi-VN":
                            language = "vi";
                            break;
                        case "en-US":
                            language = "en";
                            break;
                        case "zh-CN":
                            language = "zh";
                            break;
                        case "ko-KR":
                            language = "ko";
                            break;
                    }

                }
                catch (Exception ex)
                {
                    HttpContext.Current.Response.Write(ex.Message);
                }
                return language;
            }
        }
        public static string LanguageJavaCode
        {
            get
            {
                string language = "";
                try
                {
                    string str = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
                    switch (str)
                    {
                        case "vi-VN":
                            language = "vi-VN";
                            break;
                        case "en-US":
                            language = "en-US";
                            break;
                        case "zh-CN":
                            language = "zh-CN";
                            break;
                        case "ko-KR":
                            language = "ko-KR";
                            break;
                    }

                }
                catch (Exception ex)
                {
                    HttpContext.Current.Response.Write(ex.Message);
                }
                return language;
            }
        }
        public static string UnitMoney
        {
            get
            {
                string unit = "";
                try
                {
                    string str = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
                    switch (str)
                    {
                        case "vi-VN":
                            unit = "VND";
                            break;
                        case "en-US":
                            unit = "USD";
                            break;
                        case "zh-CN":
                            unit = "CNY";
                            break;
                        case "ko-KR":
                            unit = "KRW";
                            break;
                    }

                }
                catch (Exception ex)
                {
                    HttpContext.Current.Response.Write(ex.Message);
                }
                return unit;
            }
        }

        public static string Culture
        {
            get
            {
                string language = "";
                try
                {
                    language = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();

                }
                catch (Exception ex)
                {
                    HttpContext.Current.Response.Write(ex.Message);
                }
                return language;
            }
        }



    }
}