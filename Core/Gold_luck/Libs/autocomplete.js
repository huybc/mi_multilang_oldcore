﻿R.Suggestion = {
    Init: function () {
    },
    Search: function (el, callback) {
        $(el).autocomplete({
            minLength: 3,
            source: function (req, response) {
                R.Post({
                    module: "sample-tour",
                    params: { keyword: $(el).val() },
                    ashx: 'modulerequest.api',
                    dataType: 'json',
                    action: 'customer-suggestion',
                    success: function (res) {
                        if (res.Success) {
                            var data = [];
                            $.each($.parseJSON(res.Data), function (i, v) {
                                data.push({
                                    label: v.id,
                                    value: v.phone,
                                    name: v.name,
                                    email: v.email,
                                    id_number: v.id_number,
                                    birthday: v.birthday,
                                    gender: v.gender,
                                    address: v.address

                                });
                               
                            });
                            response(data);

                        }
                    }
                });
            },
            select: function (e, ui) {
              
                if (callback) {
                    callback(el,ui.item);
                }


            }
            
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var inner_html = '<div class="list_item_container">' + item.value + ' - ' + item.name +'</div>';
            return $("<li></li>")
                .data("item.autocomplete", item)
                .append(inner_html)
                .appendTo(ul);
        };;
    }

}

$(function () {
    R.Suggestion.Init();

});