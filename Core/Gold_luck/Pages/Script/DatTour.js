﻿var json = '';

if (sessionStorage.getItem('NgayThang') != null) {
    var result = sessionStorage.getItem('NgayThang');
    var r = result.split("-");
    var rs_a = r[0].trim().split('/');
    var re_a = r[1].trim().split('/');
    console.log(rs_a);
    console.log(re_a);
    //var startDate = new Date(parseInt(rs_a[2]), parseInt(rs_a[0]), parseInt(rs_a[1])).toString();
    //var endDate = new Date(parseInt(re_a[2]), parseInt(re_a[0]), parseInt(re_a[1])).toString();
    var sDate = rs_a[0] + "/" + rs_a[1] + "/" + rs_a[2];
    var eDate = re_a[0] + "/" + re_a[1] + "/" + re_a[2];
    //console.log(startDate, endDate);
    console.log('Co Session');

    $('input[name="startdate"]').val(sDate);
    $('input[name="enddate"]').val(eDate);
    $('input[name="startdate"]').daterangepicker({
        singleDatePicker: true,
        //startDate: sDate
        //endDate: endDate
    })
    $('input[name="enddate"]').daterangepicker({
        singleDatePicker: true,
        //startDate: eDate
        //endDate: endDate
    })

}
else {
    $('input[name="startdate"]').daterangepicker({
        singleDatePicker: true,
        //startDate: sDate
        //endDate: endDate
    })
    $('input[name="enddate"]').daterangepicker({
        singleDatePicker: true,
        //startDate: eDate
        //endDate: endDate
    })
}

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
}
function fomatStringToNumber(string) {
    return parseInt(string.replace(/[.]/g, ""))
}
function totalMoney() {
    var slNgLon = parseInt($('#slNguoiLon').text());
    var slTreEm = parseInt($('#slTreEm').text());
    var prNgLon = fomatStringToNumber($('#priceNguoiLon').text());
    var prTreEm = fomatStringToNumber($('#priceTreEm').text());
    var result = slNgLon * prNgLon + slTreEm * prTreEm;
    $('#sumPrice').text(formatNumber(result));

}

function totalHotelMoney() {
    var listItem = $('.price');
    console.log(listItem);
    var pricePerDay = 0;
    listItem.each(function () {
        pricePerDay += $(this).find('.pricePhong').data('gia') * parseInt($(this).find('.sluong').text());
    })
    console.log(pricePerDay, 'sumHotelPrice');

    var startDate = $('#txtHotelStartDate').val();
    var endDate = $('#txtHotelEndDate').val();
    console.log(startDate, endDate);
    var s = new Date(startDate);
    var e = new Date(endDate);
    var Difference_In_Days = 0;
    if (e > s) {

        var Difference_In_Time = e.getTime() - s.getTime();
        // To calculate the no. of days between two dates 
        var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
    }
    if (e <= s)
        Difference_In_Days = 1;
    var result = pricePerDay * Difference_In_Days;
    $('#sumHotelPrice').text(formatNumber(result));

}

$('#txtHotelStartDate').focusout(function () {
    if ($('#append-room-area').html() != null)

        totalHotelMoney();
})
$('#txtHotelEndDate').focusout(function () {
    if ($('#append-room-area').html() != null)
        totalHotelMoney();
})

$('#slNgayKhoiHanh').change(function () {
    json = $(this).val();
    console.log($(this).val());
    var p = $(this).val().split(",");
    $('.priceNguoiLon').text(formatNumber(p[0]));
    $('.priceTreEm').text(formatNumber(p[1]));
    totalMoney();

})

$('.minus').off('click').on('click', function () {
    var count = parseInt($(this).closest('.price').find(".sluong").text());
    console.log(count);
    $(this).closest('.price').find(".sluong").data('sl', count - 1);
    $(this).closest('.price').find(".sluong").text(count == 0 ? 0 : count - 1);
    totalMoney();
})

$('.plus').off('click').on('click', function () {
    var count = parseInt($(this).closest('.price').find(".sluong").text());
    console.log(count);
    $(this).closest('.price').find(".sluong").data('sl', count + 1);
    $(this).closest('.price').find(".sluong").text(count + 1);
    totalMoney();

})

$('#btnSetTour').off('click').on('click', function () {
    if ($('#slNgayKhoiHanh').val() == "")
        alert('Chọn lịch khởi hành');
    else {

        $('#exampleModal').modal('show');
        $('#modal-tour-detail').html('');
        var json = $('#slNgayKhoiHanh').val();
        var p = json.split(",");
        var startDate = moment(p[2]).format("DD/MM/YYYY");
        var endDate = moment(p[3]).format("DD/MM/YYYY");
        var htm = '';

        htm += '<tr>';
        htm += '<td>' + $('#tourCode').text() + '</td>';
        htm += '<td>' + startDate + ' - ' + endDate + '</td>';
        htm += '<td>' + $('#lnglon').text() + ': ' + $('#slNguoiLon').text() + '<br />';
        htm += $('#ltreep').text() + ': ' + $('#slTreEm').text() + '';
        htm += '</td>';
        htm += '<td>' + $('#sumPrice').text() + '</td>';
        htm += '</tr>';

        $('#modal-tour-detail').append(htm);
    }
})
$(function () {

    $('#commit-customer').off('click').on('click', function () {
        var p = json.split(",");
        var error = 0;
        $('input[type=text]').removeClass('_error')
        if ($('#txtName').val().length <= 0) {
            error++;
            $('#txtName').addClass('_error')
        }
        if ($('#txtPhone').val().length <= 0) {
            $('#txtPhone').addClass('_error')
            error++;
        }
        if (error > 0) {
            return false;
        }

        var obj = {
            Name: $('#txtName').val(),
            Phone: $('#txtPhone').val(),
            Birthday: $('#txtBirthday').val(),
            Address: $('#txtAddress').val(),
            Email: $('#txtEmail').val(),
            Gender: $('#txtGender').val(),
            Note: $('#txtNote').val(),
            TourCode: $('#tourCode').text(),
            StartDate: moment(p[2]).format("YYYY-MM-DD"),
            EndDate: moment(p[3]).format("YYYY-MM-DD"),
            NguoiLon: parseInt($('#slNguoiLon').text()),
            TreEm: parseInt($('#slTreEm').text()),
            SumPrice: parseInt($('#sumPrice').text().replace(/\./g, ''))

        };


        $('#exampleModal').RLoadingModule();

        R.Post({
            params: obj,
            module: "ui-action",
            ashx: 'modulerequest.ashx',
            action: "send_mail",
            success: function (res) {
                if (res.Success) {
                    $('#exampleModal').RLoadingModuleComplete();
                    $('#ignismyModal').modal('show').on('hidden.bs.modal', function (e) {
                        location.reload();
                    })
                }

            }, error: function () {
                $('#exampleModal').RLoadingModuleComplete();
            }
        });

    })

})

