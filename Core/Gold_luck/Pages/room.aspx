﻿<%@ Page Title="Khách sạn" Language="C#" MasterPageFile="~/Themes/HempSpaProduct.Master" AutoEventWireup="true" CodeBehind="room.aspx.cs" Inherits="Gold_luck.Pages.room" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<%@ Import Namespace="Gold_luck.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <section class="py-5"></section>
    <% var slider = ConfigBo.AdvGetByType(5, lang); %>

    <% var anhslihead = slider.Where(x => x.IsEnable == true).OrderBy(x => x.SortOrder).FirstOrDefault(); %>

    <section class="banner-page py-5" style="background: url(/Themes/images/change/<%=anhslihead == null ? "":anhslihead.Thumb%>) no-repeat center center; background-size: cover; color: #fff; text-align: center;">

        <div class="container  ">
            <div class="row justify-content-center  ">
                <div class="col-xl-6 col-md-8 col-sm-10 col-12 align-self-lg-center">
                    <h1 class="title"><%=anhslihead == null? "":anhslihead.Name %></h1>
                    <%=anhslihead == null ? "":anhslihead.Content %>
                </div>
            </div>
        </div>
    </section>
    <section class="list-rooms py-4 py-lg-5">
        <div class="container">
            <div class="row  justify-content-center">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  ">
                    <div class="item-room">
                        <%var totalRow = 0; %>
                        <%var getZonePhong = ZoneBo.GetZoneWithLanguageByType(12, lang).Where(x => x.Alias == "khach-san").FirstOrDefault(); %>
                        <%if (getZonePhong != null)
                            {%>
                        <%var sapXepListPhong = NewsBo.GetNewsDetailWithLanguage(12, lang, 2, getZonePhong.Id, "", 1, 6, ref totalRow).OrderByDescending(x => x.CreatedDate).ToList(); %>
                        <%foreach (var item in sapXepListPhong)
                            {%>
                        <%var link_target = string.Format("/{0}/{1}/{2}.{3}.htm", currentLanguage, item.Alias, item.Url, item.NewsId); %>
                        <%var extra = Newtonsoft.Json.JsonConvert.DeserializeObject<DonGiaEntity>(item.Extras);%>
                        <div class="row no-gutters">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12   ">
                                <div class="image">
                                    <a href="<%=link_target %>">
                                        <img src="/uploads/<%=item.Avatar %>" class="img-fluid" />
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12  ">
                                <div class="px-md-4 py-3  py-xl-4">
                                    <h4 class="title">
                                        <a href="<%=link_target %>"><%=item.Title %></a>
                                    </h4>
                                    <div class="price-tour price-room">
                                        <%=extra.HocViPhong%>
                                    </div>
                                    <div class="condition color-333D36 mb-3">
                                        <div class="item">
                                            <span class="color-A0A4A8 mr-2"><%=Language.Max_Occupancy %>: 
                                            </span>
                                            <span><%=extra.SoNguoi%> <%=Language.Person %>(s)
                                            </span>
                                        </div>
                                        <div class="item">
                                            <span class="color-A0A4A8 mr-2"><%=Language.Room_number %>:
                                            </span>
                                            <span><%=extra.SoPhong %>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="gift-service mb-3 mb-xl-5">
                                        <div class="color-345F1F text-uppercase mb-2">
                                            <%=Language.GIFT_SERVICE %>
                                        </div>
                                        <div class="border p-3">
                                            <div class="row ">
                                                <div class="col-xl-6 colg-6 col-md-6 col-12">
                                                    <div class="d-flex mb-2">
                                                        <div class="icon mr-3">
                                                            <img src="/Themes/images/room-service-ic-1.svg" class="" />
                                                        </div>
                                                        <div><%=extra.DichVu1 %></div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 colg-6 col-md-6 col-12">
                                                    <div class="d-flex mb-2">
                                                        <div class="icon mr-3">
                                                            <img src="/Themes/images/room-service-ic-2.svg" class="" />
                                                        </div>
                                                        <div>
                                                            <%=extra.DichVu2 %>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 colg-6 col-md-6 col-12">
                                                    <div class="d-flex mb-2 mb-md-0 ">
                                                        <div class="icon mr-3">
                                                            <img src="/Themes/images/room-service-ic-3.svg" class="" />
                                                        </div>
                                                        <div><%=extra.DichVu3 %></div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 colg-6 col-md-6 col-12">
                                                    <div class="d-flex ">
                                                        <div class="icon mr-3">
                                                            <img src="/Themes/images/room-service-ic-4.svg" class="" />
                                                        </div>
                                                        <div><%=extra.DichVu4 %></div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <button class="btn btn-hempspa-outline text-uppercase py-xl-3 px-5 book-room " data-price="<%=extra.HocViPhong %> " data-nguoi="<%=extra.SoNguoi %> ">
                                           <%=Language.Book_now %>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <br />

                        <%} %>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <% var anhsllast = slider.Where(x => x.IsEnable == true).OrderByDescending(x => x.SortOrder).FirstOrDefault(); %>

    <section class="contact-home py-5" style="background: url(/Themes/images/change/<%=anhsllast == null ? "":anhsllast.Thumb%>) no-repeat center center; background-size: cover; color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <h2 class="heading"><%=UIHelper.GetConfigByName("TitleAddress") %> </h2>
                    <ul class="list-contact mb-5">
                        <li>
                            <img src="/Themes/images/Location-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Address") %>
                        </li>
                        <li>
                            <img src="/Themes/images/call-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Phone") %>
                        </li>
                        <li>
                            <img src="/Themes/images/Letter-icon.svg" class="img-fluid mr-3 " /><%=UIHelper.GetConfigByName("Gmail") %>
                        </li>
                    </ul>
                    <div class="social">
                        <% var getconfigByName1 = ConfigBo.GetByConfigName("Fcebook"); %>
                        <div class="item">
                            <a href="<%=getconfigByName1.ConfigInitValue%>"><i class="fab fa-facebook-f mr-3"></i><%=getconfigByName1.ConfigValue %></a>
                        </div>
                        <% var getconfigByName2 = ConfigBo.GetByConfigName("Intargram"); %>
                        <div class="item">
                            <a href="<%=getconfigByName2.ConfigInitValue%>"><i class="fab fa-instagram mr-3"></i><%=getconfigByName2.ConfigValue %></a>
                        </div>
                        <% var getconfigByName3 = ConfigBo.GetByConfigName("Zalo"); %>
                        <div class="item">
                            <a href="<%=getconfigByName3.ConfigInitValue%>">
                                <img src="/Themes/images/zalo-icon.svg" class="img-fluid mr-3" /><%=getconfigByName3.ConfigValue %></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12 col-12 px-xl-5">
                    <form class="form-contact-home" id="gui" data-type="tuvan">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" id="txtHoten" placeholder="<%=Language.Full_Name %> *" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" id="txtGmail" placeholder="Gmail *">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="tel" class="form-control" id="txtPhone" placeholder="<%=Language.Hotline %> *" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea rows="5" class="form-control" id="txtTinNhan" placeholder="<%=Language.Note %>*"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 mb-0">
                                <button type="submit" class="btn btn-hempspa w-100 text-uppercase"><%=Language.Submit %></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
