﻿<%@ Page Title="Blog chi tiết" Language="C#" MasterPageFile="~/Themes/HempSpas.Master" AutoEventWireup="true" CodeBehind="blog-detail.aspx.cs" Inherits="Gold_luck.Pages.blog_detail" %>

<%@ Register Src="~/Pages/Controls/BlogRightMenu.ascx" TagPrefix="uc1" TagName="BlogRightMenu" %>
<%@ Import Namespace="Gold_luck.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <% var BlogID = int.Parse(Page.RouteData.Values["id"].ToString());%>
    <section class="py-5"></section>
    <% var slider = ConfigBo.AdvGetByType(10, lang); %>

    <% var anhslihead = slider.Where(x => x.IsEnable == true).OrderBy(x => x.SortOrder).FirstOrDefault(); %>

    <section class="banner-page py-5" style="background: url(/Themes/images/change/<%=anhslihead == null ? "":anhslihead.Thumb%>) no-repeat center center; background-size: cover; color: #fff; text-align: center;">

        <div class="container  ">
            <div class="row justify-content-center  ">
                <div class="col-xl-6 col-md-8 col-sm-10 col-12 align-self-lg-center">
                    <h1 class="title"><%=anhslihead == null ? "" : anhslihead.Name %></h1>
                    <%=anhslihead == null ? "" : anhslihead.Content %>
                </div>
            </div>
        </div>
    </section>

    <%var getNewByid = NewsBo.GetNewsDetailWithLanguageById(BlogID, lang); %>
    <%if (getNewByid != null)
        {%>
    <section class="py-5 ">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-7 col-12">
                    <div class="blog-detail">
                        <div class=" text-center ">
                            <div class="cate btn btn-hempspa px-5 mb-3" style="background: #58935B; font-size: .8rem;">
                                <%=getNewByid.ZoneName %>
                            </div>
                            <h1 class="title">
                                <div><%=getNewByid.Title %></div>
                            </h1>
                        </div>
                        <div class="mb-3 text-center">
                            <img src="/uploads/<%=getNewByid.Avatar %>" class="img-fluid" />
                        </div>
                        <div>
                            <%=getNewByid.Body %>
                        </div>
                        <div class="border-top my-4 d-flex py-3 flex-wrap justify-content-between">
                            <div class="tags mb-3">
                                <%var tags = getNewByid.Tags.Split(','); %>
                                <%foreach (var item in tags)
                                    {
                                        var links = "javascript:void(0)";%>

                                <div class="item">
                                    <a href="<%=links %>"><%=item.TrimStart() %></a>
                                    <%--<%=item %>--%>
                                </div>
                                <%} %>
                            </div>
                            <div class="social color-333D36 d-flex mb-3 ">
                                <div class="item ">
                                    <a href="">
                                        <img src="/Themes/images/Facebook.svg" class="img-fluid mr-4" style="" />
                                    </a>
                                </div>
                                <div class="item   ">
                                    <a href="">
                                        <img src="/Themes/images/Instagram.svg" class="img-fluid mr-4" style="" />
                                    </a>
                                </div>
                                <div class="item  ">
                                    <a href="">
                                        <img src="/Themes/images/Zalo.svg" class="img-fluid mr-4" style="" />
                                    </a>
                                </div>
                                <div class="item  align-self-xl-stretch ">
                                    <a href="">
                                        <img src="/Themes/images/Email.svg" class="img-fluid " style="" />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-5 col-12">
                    <uc1:BlogRightMenu runat="server" ID="BlogRightMenu" />
                </div>
            </div>
        </div>
    </section>

    <%} %>

    <% var anhsllast = slider.Where(x => x.IsEnable == true).OrderByDescending(x => x.SortOrder).FirstOrDefault(); %>

    <section class="contact-home py-5" style="background: url(/Themes/images/change/<%=anhsllast == null ? "":anhsllast.Thumb%>) no-repeat center center; background-size: cover; color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <h2 class="heading"><%=UIHelper.GetConfigByName("TitleAddress") %> </h2>
                    <ul class="list-contact mb-5">
                        <li>
                            <img src="/Themes/images/Location-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Address") %>
                        </li>
                        <li>
                            <img src="/Themes/images/call-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Phone") %>
                        </li>
                        <li>
                            <img src="/Themes/images/Letter-icon.svg" class="img-fluid mr-3 " /><%=UIHelper.GetConfigByName("Gmail") %>
                        </li>
                    </ul>
                    <div class="social">
                        <% var getconfigByName1 = ConfigBo.GetByConfigName("Fcebook"); %>
                        <div class="item">
                            <a href="<%=getconfigByName1.ConfigInitValue%>"><i class="fab fa-facebook-f mr-3"></i><%=getconfigByName1.ConfigValue %></a>
                        </div>
                        <% var getconfigByName2 = ConfigBo.GetByConfigName("Intargram"); %>
                        <div class="item">
                            <a href="<%=getconfigByName2.ConfigInitValue%>"><i class="fab fa-instagram mr-3"></i><%=getconfigByName2.ConfigValue %></a>
                        </div>
                        <% var getconfigByName3 = ConfigBo.GetByConfigName("Zalo"); %>
                        <div class="item">
                            <a href="<%=getconfigByName3.ConfigInitValue%>">
                                <img src="/Themes/images/zalo-icon.svg" class="img-fluid mr-3" /><%=getconfigByName3.ConfigValue %></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12 col-12 px-xl-5">
                    <form class="form-contact-home" id="gui" data-type="tuvan">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" id="txtHoten" placeholder="<%=Language.Full_Name %>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" id="txtGmail" placeholder="Gmail *">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="tel" class="form-control" id="txtPhone" placeholder="<%=Language.Hotline %>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea rows="5" class="form-control" id="txtTinNhan" placeholder="<%=Language.Note_Detail %>*"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 mb-0">
                                <button type="submit" class="btn btn-hempspa w-100 text-uppercase"><%=Language.Submit %></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
