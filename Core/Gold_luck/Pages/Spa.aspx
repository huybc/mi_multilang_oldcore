﻿<%@ Page Title="Spa" Language="C#" MasterPageFile="~/Themes/HempSpas.Master" AutoEventWireup="true" CodeBehind="Spa.aspx.cs" Inherits="Gold_luck.Pages.Spa" %>

<%@ Import Namespace="Gold_luck.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>

    <section class="py-5"></section>
    <% var slider = ConfigBo.AdvGetByType(4,lang); %>

    <% var anhslihead = slider.Where(x => x.IsEnable == true).OrderBy(x => x.SortOrder).FirstOrDefault(); %>

    <section class="banner-page py-5" style="background: url(/Themes/images/change/<%=anhslihead == null ? "":anhslihead.Thumb%>) no-repeat center center; background-size: cover; color: #fff; text-align: center;">

        <div class="container  ">
            <div class="row justify-content-center  ">
                <div class="col-xl-6 col-md-8 col-sm-10 col-12 align-self-lg-center">
                    <h1 class="title"><%=anhslihead == null? "":anhslihead.Name %></h1>
                    <%=anhslihead == null ? "":anhslihead.Content %>
                </div>
            </div>
        </div>
    </section>

    <section class="list-spa-service py-5">
        <div class="container">
            <div class="row">
                <%var zone_spa = ZoneBo.GetZoneWithLanguageByType(12, lang).Where(r => r.Alias == "spa").FirstOrDefault(); %>
                <%var total_row = 0; %>
                <%--<%var list_spa = NewsBo.GetNewsDetailWithLanguage(2, lang, 1, 0, "", 1, 6, ref total_row).Skip(2); %>--%>
                <%if (zone_spa != null)
                    {%>
                <%var list_spa_1 = NewsBo.GetNewsDetailWithLanguage(12, lang, 2, zone_spa.Id, "", 1, 9, ref total_row); %>
                <%foreach (var item in list_spa_1)
                    {%>
                <%--<% var link_taget1 = string.Format("/{0}/{1}/{2}/{3}.htm", currentLanguage, "spa-chi-tiet", item.Alias, item.NewsId);%>--%>
                <%var link_tar = string.Format("/{0}/{1}/{2}.{3}.htm", currentLanguage, zone_spa.Alias == null ? "" : zone_spa.Alias, item.Url, item.NewsId); %>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-spa">
                        <div class="image">
                            <a href="<%=link_tar %>" title="">
                                <img src="/uploads/<%=item.AvatarTron %>" class="img-fluid"
                                    alt="<%= item.Title %>" /></a>
                        </div>
                        <div class="text px-xl-4 mb-4 ">
                            <h4 class="title">
                                <a href="<%=link_tar %>"><%= item.Title %></a>
                            </h4>
                            <div class="des">
                                <%=item.Sapo %>
                            </div>

                        </div>
                        <a href="<%=link_tar%>" class="btn btn-hempspa-outline text-uppercase px-4 px-lg-5"><%=Language.More %></a>
                    </div>
                </div>

                <%}%>

                <%} %>
            </div>
        </div>
    </section>
    <section class="menu-spa pb-5">
        <div class="container">
            <h3 class="heading text-center"><%=UIHelper.GetConfigByName("TextBodySpa") %></h3>
            <div class="text-center mb-4">
                <button class="btn btn-hempspa text-uppercase ">WELLNESS SPA</button>
                <button class="btn btn-hempspa-outline text-uppercase  ">PACKAGE</button>
            </div>
            <div class="row">
                <%var phanGia = slider.Where(x => x.SortOrder == 3 && x.LanguageCode.Trim() == lang).ToList(); %>

                <%var _regex = @"\[(.*?)]"; %>
                <%var CtaRegex = new Regex(_regex);%>
                <%foreach (var item in phanGia)
                    {%>
                <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                    <div class="item-menu-spa-ser">
                        <div class="service-name">
                            <%=item.Name %>
                        </div>
                        <div class="detail-price">

                            <%var chuoi = Regex.Matches(item.Content, _regex).Cast<Match>()
                                                    .Select(m => m.Value)
                                                    .ToList();%>
                            <%foreach (var ii in chuoi)
                                { %>
                            <% var spl = ii.Replace("[", "").Replace("]", "").Split(';'); %>
                            <div class="item  d-flex">
                                <%if (spl.Count() >= 1)
                                    { %>
                                <div class="time">
                                    <%=  spl[0] %>
                                </div>
                                <%} %>
                                <%if (spl.Count() >= 2)
                                    { %>
                                <div class="price  ml-auto border-right">
                                    <%=  spl[1] %>
                                </div>
                                <%} %>
                                <%if (spl.Count() >= 3)
                                    { %>
                                <div class="price">
                                    <%=  spl[2] %>
                                </div>
                                <%} %>
                            </div>

                            <%} %>
                        </div>

                    </div>
                </div>
                <%}%>
            </div>
        </div>
    </section>
    <% var anhsllast = slider.Where(x => x.IsEnable == true).OrderByDescending(x => x.SortOrder).FirstOrDefault(); %>

    <section class="contact-home py-5" style="background: url(/Themes/images/change/<%=anhsllast == null ? "":anhsllast.Thumb%>) no-repeat center center; background-size: cover; color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <h2 class="heading"><%=UIHelper.GetConfigByName("TitleAddress") %> </h2>
                    <ul class="list-contact mb-5">
                        <li>
                            <img src="/Themes/images/Location-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Address") %>
                        </li>
                        <li>
                            <img src="/Themes/images/call-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Phone") %>
                        </li>
                        <li>
                            <img src="/Themes/images/Letter-icon.svg" class="img-fluid mr-3 " /><%=UIHelper.GetConfigByName("Gmail") %>
                        </li>
                    </ul>
                    <div class="social">
                        <% var getconfigByName1 = ConfigBo.GetByConfigName("Fcebook"); %>
                        <div class="item">
                            <a href="<%=getconfigByName1.ConfigInitValue%>"><i class="fab fa-facebook-f mr-3"></i><%=getconfigByName1.ConfigValue %></a>
                        </div>
                        <% var getconfigByName2 = ConfigBo.GetByConfigName("Intargram"); %>
                        <div class="item">
                            <a href="<%=getconfigByName2.ConfigInitValue%>"><i class="fab fa-instagram mr-3"></i><%=getconfigByName2.ConfigValue %></a>
                        </div>
                        <% var getconfigByName3 = ConfigBo.GetByConfigName("Zalo"); %>
                        <div class="item">
                            <a href="<%=getconfigByName3.ConfigInitValue%>">
                                <img src="/Themes/images/zalo-icon.svg" class="img-fluid mr-3" /><%=getconfigByName3.ConfigValue %></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12 col-12 px-xl-5">
                    <form class="form-contact-home" id="gui" data-type="tuvan">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" id="txtHoten" placeholder="<%=Language.Full_Name %> *" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" id="txtGmail" placeholder="Gmail *">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="tel" class="form-control" id="txtPhone" placeholder="<%=Language.Hotline %> *" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea rows="5" class="form-control" id="txtTinNhan" placeholder="<%=Language.Note %>  *"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 mb-0">
                                <button type="submit" class="btn btn-hempspa w-100 text-uppercase"><%=Language.Submit %></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
