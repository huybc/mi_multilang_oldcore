﻿<%@ Page Title="Giới thiệu" Language="C#" MasterPageFile="~/Themes/HempSpas.Master" AutoEventWireup="true" CodeBehind="about.aspx.cs" Inherits="Gold_luck.Pages.about" %>

<%@ Import Namespace="Gold_luck.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <section class="py-5"></section>
    <% var slider = ConfigBo.AdvGetByType(11, lang); %>

    <% var anhslihead = slider.Where(x => x.IsEnable == true).OrderBy(x => x.SortOrder).FirstOrDefault(); %>

    <section class="banner-page py-5" style="background: url(/Themes/images/change/<%=anhslihead == null ? "":anhslihead.Thumb%>) no-repeat center center; background-size: cover; color: #fff; text-align: center;">

        <div class="container  ">
            <div class="row justify-content-center  ">
                <div class="col-xl-6 col-md-8 col-sm-10 col-12 align-self-lg-center">
                    <h1 class="title"><%=anhslihead == null? "":anhslihead.Name %></h1>
                    <%=anhslihead == null ? "":anhslihead.Content %>
                </div>
            </div>
        </div>
    </section>
    <section class="des-about py-5">
        <div class="container">
            <%var totalRow = 0; %>
            <%var GetAbao = NewsBo.GetNewsDetailWithLanguage(5, lang, 2, 0, "", 1, 1, ref totalRow); %>
            <%var getAaoDetail = GetAbao.Where(x => x.Body != "").FirstOrDefault(); %>
            <%if (getAaoDetail != null)
              {%>
                     <h2 class="heading-about-us text-left mb-3"><%=getAaoDetail.Title %>
                    </h2>
                    <div class="row">
                        <%=getAaoDetail.Body %>
                    </div>
            <%}%>
        </div>
    </section>

    <section class="video-about py-5">
        <div class="container">
            <h2 class="heading-about-us">
                <%=UIHelper.GetConfigByName("TextBlog2") %>
            </h2>
            <div class="row justify-content-center">
                <div class="col-xl-6 col-md-8 col-12 text-center">
                    <div class="mb-4">
                        <%=UIHelper.GetConfigByName("TextBlog3") %>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <% var Video = slider.Where(x => x.IsEnable == true).Skip(1).Take(2).ToList(); %>
                <%foreach (var item in Video)
                    {%>
                <div class="col-xl-5 col-lg-6 col-md-6 col-12">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="<%=item.Url %>" allowfullscreen></iframe>
                    </div>
                    <div class="color-333D36 text-center">
                        <%=item.Name %>
                    </div>
                </div>
                <% } %>
            </div>
        </div>
    </section>

    <section class="team py-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-md-8 col-12 text-center">
                    <h2 class="heading-about-us">
                        <%=UIHelper.GetConfigByName("TextBlog4") %>
                    </h2>
                    <div class="mb-4">
                        <%=UIHelper.GetConfigByName("TextBlog5") %>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <% var anhNhanVien = slider.Where(x => x.SortOrder == 4).ToList(); %>
                <%foreach (var item in anhNhanVien)
                    {%>
                          <div class="col-xl-3 col-lg-3 col-md-5 col-12">
                            <div class="item text-center mb-4">
                                <div class="image mb-3">
                                    <a href="">
                                        <img src="/Themes/images/change/<%=item.Thumb %>" class="img-fluid" alt="" /></a>
                                </div>
                                <div class="name h5 mb-1 font-weight-bold">
                                    <a href="javascript:void(0)"><%=item.Name %></a>
                                </div>
                                <div class="color-345F1F small text-uppercase">
                                   <%=item.Content %>
                                </div>
                            </div>
                        </div>
                   <% } %>
            </div>
        </div>
    </section>
    <% var anhsllast = slider.Where(x => x.IsEnable == true).OrderByDescending(x => x.SortOrder).FirstOrDefault(); %>

    <section class="contact-home py-5" style="background: url(/Themes/images/change/<%=anhsllast == null ? "":anhsllast.Thumb%>) no-repeat center center; background-size: cover; color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <h2 class="heading"><%=UIHelper.GetConfigByName("TitleAddress") %> </h2>
                    <ul class="list-contact mb-5">
                        <li>
                            <img src="/Themes/images/Location-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Address") %>
                        </li>
                        <li>
                            <img src="/Themes/images/call-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Phone") %>
                        </li>
                        <li>
                            <img src="/Themes/images/Letter-icon.svg" class="img-fluid mr-3 " /><%=UIHelper.GetConfigByName("Gmail") %>
                        </li>
                    </ul>
                    <div class="social">
                        <% var getconfigByName1 = ConfigBo.GetByConfigName("Fcebook"); %>
                        <div class="item">
                            <a href="<%=getconfigByName1.ConfigInitValue%>"><i class="fab fa-facebook-f mr-3"></i><%=getconfigByName1.ConfigValue %></a>
                        </div>
                        <% var getconfigByName2 = ConfigBo.GetByConfigName("Intargram"); %>
                        <div class="item">
                            <a href="<%=getconfigByName2.ConfigInitValue%>"><i class="fab fa-instagram mr-3"></i><%=getconfigByName2.ConfigValue %></a>
                        </div>
                        <% var getconfigByName3 = ConfigBo.GetByConfigName("Zalo"); %>
                        <div class="item">
                            <a href="<%=getconfigByName3.ConfigInitValue%>">
                                <img src="/Themes/images/zalo-icon.svg" class="img-fluid mr-3" /><%=getconfigByName3.ConfigValue %></a>
                        </div>
                    </div>
                </div>
                 <div class="col-xl-6 col-md-6 col-sm-12 col-12 px-xl-5">
                    <form class="form-contact-home"id="gui"  data-type="tuvan">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" id="txtHoten" placeholder="<%=Language.Full_Name %>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" id="txtGmail" placeholder="Gmail *">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="tel" class="form-control" id="txtPhone" placeholder="<%=Language.Hotline %>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea rows="5" class="form-control" id="txtTinNhan" placeholder="<%=Language.Note_Detail %>*"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 mb-0">
                                <button type="submit" class="btn btn-hempspa w-100 text-uppercase"><%=Language.Submit %></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
