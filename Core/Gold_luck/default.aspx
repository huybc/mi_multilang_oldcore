﻿<%@ Page Title="Trang chủ" Language="C#" MasterPageFile="~/Themes/HempSpaBG.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Gold_luck._default1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%@ Import Namespace="Gold_luck.Core.Helper" %>
    <%@ Import Namespace="Newtonsoft.Json" %>
    <%@ Import Namespace="Resources" %>
    <%@ Import Namespace="Mi.BO.Base.Zone" %>
    <%@ Import Namespace="Mi.BO.Base.News" %>
    <%@ Import Namespace="Mi.Entity.Base.News" %>
    <% 
        var lang = Current.LanguageJavaCode;
        var currentLanguage = Current.Language;
    %>
    <% var slider = ConfigBo.AdvGetByType(2, lang); %>

    <div class="position-relative">
        <div id="carousel-banner-home" class="carousel slide slide-home" data-ride="carousel">
            <div class="carousel-inner">
                <% var sliderhead = slider.Where(x => x.SortOrder == 1).ToList(); %>
                <%for (int i = 0; i < sliderhead.Count(); i++)
                    {%>
                <div class="carousel-item <%=i == 0?"active":"" %>">
                    <img src="/Themes/images/change/<%=sliderhead[i].Thumb %>" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block ">
                        <h1><%=sliderhead[i].Name %></h1>
                        <p><%=sliderhead[i].Content %> </p>
                    </div>
                </div>
                <%  } %>
            </div>
            <a class="carousel-control-prev" href="#carousel-banner-home" role="button" data-slide="prev">
                <img src="/Themes/images/change/btn-left.svg" class="img-fluid  " alt="...">
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-banner-home" role="button" data-slide="next">
                <img src="/Themes/images/change/btn-right.svg" class="img-fluid  " alt="...">
                <span class="sr-only">Next</span>
            </a>

        </div>
        <div class="menu-banner py-4">
            <div class="container">
                <div class="row no-gutters text-center justify-content-center">
                    <%var getZoneByIsHome = ZoneBo.GetLocationByLanguage(lang, true);%>
                    <%var getZoneDeltail = getZoneByIsHome.Where(x => x.ParentId == 0).OrderBy(x => x.SortOrder).Take(6).ToArray(); %>


                    <%--                    <%if (getZoneDeltail != null)
                        {%>
                    <%for (int i = 0; i < getZoneDeltail.Count(); i++)
                        {%>
                    <% var link_taget1 = string.Format("/{0}/{1}/{2}/{3}.htm", currentLanguage, UIHelper.GetTypeUrlByHempSpaProject(getZoneDeltail[i].Type), "Gold_luck", getZoneDeltail[i].Id);%>
                    <div class="col-lg-2 col-md-4 col-sm-4 col-4 ">
                        <a href="<%=link_taget1 %>" class="btn item-menu spin circle">
                            <div class="image">
                                <img src="/Themes/images/<%=getZoneDeltail[i].Avatar%>" class="img-fluid" />
                            </div>
                            <div class="text">
                                <%=getZoneDeltail[i].Name %>
                            </div>
                        </a>
                    </div>
                    <%}%>

                    <%}%>--%>
                    <%//Lay danh sach thuoc danh muc dich vu %>
                    <%var list_dich_vu = ZoneBo.GetZoneWithLanguageByType(12, lang);%>
                    <%var list_dich_vu_child = list_dich_vu.Where(x => x.ParentId > 0).OrderBy(x => x.SortOrder).Take(6); %>
                    <%foreach (var item in list_dich_vu_child)
                        { %>
                    <% var link_taget1 = string.Format("/{0}/{1}.htm", currentLanguage, item.Alias);%>
                    <div class="col-lg-2 col-md-4 col-sm-4 col-4 ">
                        <a href="<%=link_taget1 %>" class="btn item-menu spin circle">
                            <div class="image">
                                <img src="/uploads/<%=item.Avatar %>" class="img-fluid" />
                            </div>
                            <div class="text">
                                <%=item.lang_name %>
                            </div>
                        </a>
                    </div>
                    <%} %>
                </div>

            </div>
        </div>
    </div>

    <section class="form-booking">
        <div class="container">
            <form>
                <div class="row no-gutters">
                    <div class="col-lg-3 col-md-12 col-sm-12 col-12 mb-2 mb-lg-0 px-md-2">
                        <label for=" "><%=Language.Check_in___Check_out %></label>
                        <input type="text" name="dates" class="form-control" id=" " placeholder=" ">
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-12 mb-2 mb-lg-0 px-md-2">
                        <label for=" "><%=Language.Adults %></label>
                        <input type=" text" class="form-control" id=" " placeholder=" ">
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-12 mb-2 mb-lg-0 px-md-2">
                        <label for=" "><%=Language.Children %></label>
                        <input type=" text" class="form-control" id=" " placeholder=" ">
                    </div>
                    <%var random = new Random(); %>
                    <%var rn = random.Next(10, 100); %>
                    <% var link_taget = string.Format("/{0}/{1}/{2}/{3}.htm", currentLanguage, "room","hempspa",rn);%>
                    <div class="col-lg-3 col-md-12 col-sm-12 col-12 align-self-end my-3 my-lg-0 px-md-2">
                        <a href="<%=link_taget %>" class="btn btn-hempspa w-100 text-uppercase"><%=Language.Book_now %></a>
                    </div>
                </div>
            </form>
        </div>
    </section>

    <section class="service-home py-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-8 col-md-10 col-12">
                    <h2 class="heading"><%=UIHelper.GetConfigByName("Text1Content1") %></h2>
                    <div class="text-center mb-5">
                        <%=UIHelper.GetConfigByName("Text2Content1") %>
                    </div>
                </div>
            </div>
            <%//code Thang %>
            <%var getZoneByPrent = getZoneByIsHome.Where(x => x.ParentId != 0).OrderBy(s => s.SortOrder).ToList(); %>
            <%//end code Thang %>
            <%//Lay ra danh sach bai viet thuoc spa %>
            <%var total_spa = 0; %>
            <%var bai_viet_hot_spa = NewsBo.GetNewsDetailWithLanguage(12, lang, 1, 0, "", 1, 6, ref total_spa); %>
            <div class="row" id="addSpa">
                <%foreach (var item in bai_viet_hot_spa)
                    { %>
                <%var link_target = string.Format("/{0}/{1}/{2}.{3}.htm", currentLanguage, item.Alias, item.Url, item.NewsId); %>
                <%var gia_dich_vu = Newtonsoft.Json.JsonConvert.DeserializeObject<DonGiaEntity>(item.Extras); %>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="row no-gutters item-service-home">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="image">
                                <a href="<%=link_target %>">
                                    <img src="/uploads/<%=item.Avatar %>" class="img-fluid w-100 " alt="<%=item.Title %>" />
                                </a>
                                <div class="row no-gutters group-btn">
                                    <div class="col-5">
                                        <div class="price">
                                            <small>From</small> <b><%=gia_dich_vu.HocVi %></b>
                                        </div>
                                    </div>
                                    <div class="col-7">
                                        <div class="book-now border-left  ">
                                            <a href="<%=link_target %>"><%=Language.Book_now %></a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12  ">
                            <div class="box-text">
                                <h3 class="title">
                                    <a href="<%=link_target %>" title="<%=item.Title %>"><%=item.Title %></a>
                                </h3>
                                <div>
                                    <%=item.Sapo %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%} %>
            </div>



            <%-- <%if (getZoneByPrent != null)
                {%>
            <%for (int i = 0; i < getZoneByPrent.Count(); i++)
                {%>
            <%var list_spa = NewsBo.GetNewsDetailWithLanguage(getZoneByPrent[i].Type, lang, 2, getZoneByPrent[i].Id, "", 1, 1, ref total_row); %>
            <%foreach (var item in list_spa)
                {%>
            <% var link_item = string.Format("/{0}/{1}/{2}/{3}.htm", currentLanguage, UIHelper.GetTypeUrlByHempSpaProject(getZoneByPrent[i].Type), item.Url, item.ZoneId); %>
            <%var gia_detail = Newtonsoft.Json.JsonConvert.DeserializeObject<DonGiaEntity>(item.Extras); %>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="row no-gutters item-service-home">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="image">
                            <a href="<%=link_item %>">
                                <img src="/Themes/images/change/<%=item.Avatar %>" class="img-fluid w-100 " />
                            </a>
                            <div class="row no-gutters group-btn">
                                <div class="col-5">
                                    <div class="price   ">
                                        <small>From</small> <b><%=gia_detail.HocVi %></b>
                                    </div>
                                </div>
                                <div class="col-7">
                                    <div class="book-now border-left  ">
                                        <a href="<%=link_item %>">BOOK NOW</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12  ">
                        <div class="box-text">
                            <h3 class="title">
                                <a href="<%=link_item %>" title=""><%=item.Title %></a>
                            </h3>
                            <div>
                                <%=item.Sapo %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%}%>
            <%}%>
            <%}%>--%>
        </div>
        <div class="col-12 mt-4 text-center text-danger" id="sp4"></div>
        <div class="text-center my-3">
            <button class="btn btn-hempspa-outline px-5 " data-lang="<%=lang %>" id="_viewDichVu" data-currentlang="<%=currentLanguage %>"><%=Language.VIEW_MORE %></button>
        </div>

    </section>
    <% var anhsllast = slider.Where(x => x.IsEnable == true).OrderByDescending(x => x.SortOrder).FirstOrDefault(); %>
    <section class="contact-home py-5" style="background: url(/Themes/images/change/<%=anhsllast == null ? "":anhsllast.Thumb%>) no-repeat center center; background-size: cover; color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12 col-12">
                    <h2 class="heading"><%=UIHelper.GetConfigByName("TitleAddress") %> </h2>
                    <ul class="list-contact mb-5">
                        <li>
                            <img src="/Themes/images/Location-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Address") %>
                        </li>
                        <li>
                            <img src="/Themes/images/call-icon.svg" class="img-fluid mr-3" /><%=UIHelper.GetConfigByName("Phone") %>
                        </li>
                        <li>
                            <img src="/Themes/images/Letter-icon.svg" class="img-fluid mr-3 " /><%=UIHelper.GetConfigByName("Gmail") %>
                        </li>
                    </ul>
                    <div class="social">
                        <% var getconfigByName1 = ConfigBo.GetByConfigName("Fcebook"); %>
                        <div class="item">
                            <a href="<%=getconfigByName1.ConfigInitValue%>"><i class="fab fa-facebook-f mr-3"></i><%=getconfigByName1.ConfigValue %></a>
                        </div>
                        <% var getconfigByName2 = ConfigBo.GetByConfigName("Intargram"); %>
                        <div class="item">
                            <a href="<%=getconfigByName2.ConfigInitValue%>"><i class="fab fa-instagram mr-3"></i><%=getconfigByName2.ConfigValue %></a>
                        </div>
                        <% var getconfigByName3 = ConfigBo.GetByConfigName("Zalo"); %>
                        <div class="item">
                            <a href="<%=getconfigByName3.ConfigInitValue%>">
                                <img src="/Themes/images/zalo-icon.svg" class="img-fluid mr-3" /><%=getconfigByName3.ConfigValue %></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12 col-12 px-xl-5">
                     <form class="form-contact-home" id="gui" data-type="tuvan">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" id="txtHoten" placeholder="<%=Language.Full_Name %> *" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" id="txtGmail" placeholder="Gmail *">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="tel" class="form-control" id="txtPhone" placeholder="<%=Language.Hotline %> *" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea rows="5" class="form-control" id="txtTinNhan" placeholder="<%=Language.Note %>  *"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 mb-0">
                                <button type="submit" class="btn btn-hempspa w-100 text-uppercase"><%=Language.Submit %></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
